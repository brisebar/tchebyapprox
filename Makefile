

all: lib examples
	

.PHONY: lib
lib:
	$(MAKE) -C src/


.PHONY: examples
examples:
	$(MAKE) -C examples/

