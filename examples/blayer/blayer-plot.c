/*
Example for the TchebyApprox library: boundary layer from Olver and Townsend
change the parameter eps to observe the almost banded parameters
of the approx inverse
*/



#include <stdio.h>
#include <stdlib.h>

#include <gmp.h>
#include <mpfr.h>
#include <mpfi.h>

#include "chebmodel_intop_newton.h"

/*
void double_chebpoly_evaluate1(double_t y, const double_chebpoly_t P)
{
  long i;
  long n = P->degree;
  *y = 0;

  for (i = 0 ; i <= n ; i++)
    *y += P->coeffs[i];
}

void double_chebpoly_evaluate_1(double_t y, const double_chebpoly_t P)
{
  long i;
  long n = P->degree;
  *y = 0;

  for (i = 0 ; i <= n ; i++)
    if (i%2)
      *y -= P->coeffs[i];
    else
      *y += P->coeffs[i];
}
*/

void double_chebpoly_evaluate1_fr(mpfr_t y, const double_chebpoly_t P)
{
  long i;
  long n = P->degree;
  mpfr_set_zero(y, 1);

  for (i = 0 ; i <= n ; i++)
    mpfr_add_d(y, y, P->coeffs[i], MPFR_RNDN);
}

void double_chebpoly_evaluate_1_fr(mpfr_t y, const double_chebpoly_t P)
{
  long i;
  long n = P->degree;
  mpfr_set_zero(y, 1);

  for (i = 0 ; i <= n ; i++)
    if (i%2)
      mpfr_sub_d(y, y, P->coeffs[i], MPFR_RNDN);
    else
      mpfr_add_d(y, y, P->coeffs[i], MPFR_RNDN);
}

void mpfr_chebpoly_evaluate1_fr(mpfr_t y, const mpfr_chebpoly_t P)
{
  long i;
  long n = P->degree;
  mpfr_set_zero(y, 1);

  for (i = 0 ; i <= n ; i++)
    mpfr_add(y, y, P->coeffs + i, MPFR_RNDN);
}

void mpfr_chebpoly_evaluate_1_fr(mpfr_t y, const mpfr_chebpoly_t P)
{
  long i;
  long n = P->degree;
  mpfr_set_zero(y, 1);

  for (i = 0 ; i <= n ; i++)
    if (i%2)
      mpfr_sub(y, y, P->coeffs + i, MPFR_RNDN);
    else
      mpfr_add(y, y, P->coeffs + i, MPFR_RNDN);
}

/*
void chebmodel_evaluate1(mpfi_t y, const chebmodel_t P)
{
  long i;
  long n = P->poly->degree;
  mpfi_set_si(y, 0);

  for (i = 0 ; i <= n ; i++)
    mpfi_add(y, y, P->poly->coeffs + i);

  mpfr_add(&(y->right), &(y->right), P->rem, MPFR_RNDU);
  mpfr_sub(&(y->left), &(y->left), P->rem, MPFR_RNDD);
}
*/


// numerical computation of the basis
// floating-point type: [double]
// F10(-1) = 1 and F10'(-1) = 0
// F01(-1) = 0 and F01'(-1) = 1
void blayer_basis_d(double_chebpoly_t F10, double_chebpoly_t F01, const mpfr_t eps, long p, long n)
{
  long i;

  // Cos
  double_chebpoly_lode_t L0;
  double_chebpoly_lode_init(L0);
  double_chebpoly_lode_set_order(L0, 2);
  double_chebpoly_set_degree(L0->a + 0, 0);
  double_set_si(L0->a[0].coeffs + 0, 1, MPFR_RNDN);
  double_chebpoly_t g;
  double_chebpoly_init(g); // g=0
  double I[2];
  I[0] = cos(-1);
  I[1] = -sin(-1);
  double_chebpoly_struct Cos[3];
  double_chebpoly_init(Cos + 0);
  double_chebpoly_init(Cos + 1);
  double_chebpoly_init(Cos + 2);
  double_chebpoly_lode_intop_approxsolve(Cos, L0, g, I, p);

  // D^2 - 2x/eps*(cos(x)-8/10) D + 1/eps*(cos(x)-8/10)
  double_chebpoly_lode_t L;
  double_chebpoly_lode_init(L);
  double_chebpoly_lode_set_order(L, 2);
  double_chebpoly_set(L->a + 0, Cos + 0);
  L->a[0].coeffs[0] -= (double) 8/10;
  double_chebpoly_scalar_div_fr(L->a + 0, L->a + 0, eps); // = 1/eps*(cos(x)-8/10)
  double_chebpoly_set_degree(L->a + 1, 1);
  double_set_si(L->a[1].coeffs + 1, -2, MPFR_RNDN);
  double_chebpoly_mul(L->a + 1, L->a + 1, L->a + 0);

  // approx solve
  double_chebpoly_struct FF10[3];
  double_chebpoly_struct FF01[3];
  for (i = 0 ; i <= 2 ; i++) {
    double_chebpoly_init(FF10 + i);
    double_chebpoly_init(FF01 + i);
  }
  I[0] = 1; I[1] = 0;
  double_chebpoly_lode_intop_approxsolve(FF10, L, g, I, n);
  I[0] = 0; I[1] = 1;
  double_chebpoly_lode_intop_approxsolve(FF01, L, g, I, n);
  double_chebpoly_set(F10, FF10 + 0);
  double_chebpoly_set(F01, FF01 + 0);

  // clear variables
  double_chebpoly_lode_clear(L0);
  double_chebpoly_lode_clear(L);
  double_chebpoly_clear(g);
  for (i = 0 ; i <= 2 ; i++) {
    double_chebpoly_clear(Cos + i);
    double_chebpoly_clear(FF10 + i);
    double_chebpoly_clear(FF01 + i);
  }
}

// numerical computation of the basis
// floating-point type: [mpfr]
// F10(-1) = 1 and F10'(-1) = 0
// F01(-1) = 0 and F01'(-1) = 1
void blayer_basis_fr(mpfr_chebpoly_t F10, mpfr_chebpoly_t F01, const mpfr_t eps, long p, long n)
{
  long i;

  // Cos
  mpfr_chebpoly_lode_t L0;
  mpfr_chebpoly_lode_init(L0);
  mpfr_chebpoly_lode_set_order(L0, 2);
  mpfr_chebpoly_set_degree(L0->a + 0, 0);
  mpfr_set_si(L0->a[0].coeffs + 0, 1, MPFR_RNDN);
  mpfr_chebpoly_t g;
  mpfr_chebpoly_init(g); // g=0
  __mpfr_struct I[2]; mpfr_init(I + 0); mpfr_init(I + 1);
  mpfr_set_si(I + 0, -1, MPFR_RNDN);
  mpfr_cos(I + 0, I + 0, MPFR_RNDN);
  mpfr_set_si(I + 1, -1, MPFR_RNDN);
  mpfr_sin(I + 1, I + 1, MPFR_RNDN);
  mpfr_neg(I + 1, I + 1, MPFR_RNDN);
  mpfr_chebpoly_struct Cos[3];
  mpfr_chebpoly_init(Cos + 0);
  mpfr_chebpoly_init(Cos + 1);
  mpfr_chebpoly_init(Cos + 2);
  mpfr_chebpoly_lode_intop_approxsolve(Cos, L0, g, I, p);

  // D^2 - 2x/eps*(cos(x)-8/10) D + 1/eps*(cos(x)-8/10)
  mpfr_chebpoly_lode_t L;
  mpfr_chebpoly_lode_init(L);
  mpfr_chebpoly_lode_set_order(L, 2);
  mpfr_chebpoly_set_degree(L->a + 0, 0);
  mpfr_set_si(L->a[0].coeffs + 0, 8, MPFR_RNDN);
  mpfr_div_si(L->a[0].coeffs + 0, L->a[0].coeffs + 0, 10, MPFR_RNDN);
  mpfr_chebpoly_sub(L->a + 0, Cos + 0, L->a + 0);
  mpfr_chebpoly_scalar_div_fr(L->a + 0, L->a + 0, eps); // = 1/eps*(cos(x)-8/10)
  mpfr_chebpoly_set_degree(L->a + 1, 1);
  mpfr_set_si(L->a[1].coeffs + 1, -2, MPFR_RNDN);
  mpfr_chebpoly_mul(L->a + 1, L->a + 1, L->a + 0); // -2x/eps*(cos(x)-8/10)

  // approx solve
  mpfr_chebpoly_struct FF10[3];
  mpfr_chebpoly_struct FF01[3];
  for (i = 0 ; i <= 2 ; i++) {
    mpfr_chebpoly_init(FF10 + i);
    mpfr_chebpoly_init(FF01 + i);
  }
  mpfr_set_si(I + 0, 1, MPFR_RNDN); mpfr_set_si(I + 1, 0, MPFR_RNDN);
  mpfr_chebpoly_lode_intop_approxsolve(FF10, L, g, I, n);
  mpfr_set_si(I + 0, 0, MPFR_RNDN); mpfr_set_si(I + 1, 1, MPFR_RNDN);
  mpfr_chebpoly_lode_intop_approxsolve(FF01, L, g, I, n);
  mpfr_chebpoly_set(F10, FF10 + 0);
  mpfr_chebpoly_set(F01, FF01 + 0);

  // clear variables
  mpfr_chebpoly_lode_clear(L0);
  mpfr_chebpoly_lode_clear(L);
  mpfr_chebpoly_clear(g);
  for (i = 0 ; i <= 2 ; i++) {
    mpfr_chebpoly_clear(Cos + i);
    mpfr_chebpoly_clear(FF10 + i);
    mpfr_chebpoly_clear(FF01 + i);
  }
}



// certified computation of the basis
// F10(-1) = 1 and F10'(-1) = 0
// F01(-1) = 0 and F01'(-1) = 1
// returns: N, h2, d2, the parameters for the Newton-like operator
void blayer_basis_fi(chebmodel_t F10, chebmodel_t F01, const mpfr_t eps, long *N, long *h2, long *d2, long p, long n)
{
  long i;

  // Cos
  chebmodel_lode_t L0;
  chebmodel_lode_init(L0);
  chebmodel_lode_set_order(L0, 2);
  chebmodel_set_degree(L0->a + 0, 0);
  mpfi_set_si(L0->a[0].poly->coeffs + 0, 1);
  chebmodel_t g;
  chebmodel_init(g); // g=0
  __mpfi_struct I[2]; mpfi_init(I + 0); mpfi_init(I + 1);
  mpfi_set_si(I + 0, -1);
  mpfi_cos(I + 0, I + 0);
  mpfi_set_si(I + 1, -1);
  mpfi_sin(I + 1, I + 1);
  mpfi_neg(I + 1, I + 1);
  chebmodel_struct Cos[3];
  chebmodel_init(Cos + 0);
  chebmodel_init(Cos + 1);
  chebmodel_init(Cos + 2);
  chebmodel_lode_intop_newton_solve(Cos, L0, g, I, p);

  // D^2 - 2x/eps*(cos(x)-8/10) D + 1/eps*(cos(x)-8/10)
  chebmodel_lode_t L;
  chebmodel_lode_init(L);
  chebmodel_lode_set_order(L, 2);
  chebmodel_set_degree(L->a + 0, 0);
  mpfi_set_si(L->a[0].poly->coeffs + 0, 8);
  mpfi_div_si(L->a[0].poly->coeffs + 0, L->a[0].poly->coeffs + 0, 10);
  chebmodel_sub(L->a + 0, Cos + 0, L->a + 0);
  chebmodel_scalar_div_fr(L->a + 0, L->a + 0, eps); // = 1/eps*(cos(x)-8/10)
  chebmodel_set_degree(L->a + 1, 1);
  mpfi_set_si(L->a[1].poly->coeffs + 1, -2);
  chebmodel_mul(L->a + 1, L->a + 1, L->a + 0); // -2x/eps*(cos(x)-8/10)

  // contracting op
  mpfr_t Tbound; mpfr_init(Tbound);
  mpfr_bandmatrix_t MKinv; mpfr_bandmatrix_init(MKinv);
  chebmodel_intop_t K; chebmodel_intop_init(K);
  chebmodel_intop_set_lode(K, L);
  chebmodel_intop_newton_contract_fr(Tbound, MKinv, K, 2*chebmodel_intop_Dwidth(K));
  *N = MKinv->dim-1;
  *h2 = MKinv->Hwidth;
  *d2 = MKinv->Dwidth;

  // approx sol and validation
  mpfr_chebpoly_intop_t Kfr; mpfr_chebpoly_intop_init(Kfr);
  chebmodel_intop_get_mpfr_chebpoly_intop(Kfr, K);
  mpfr_chebpoly_t FF10, FF01; mpfr_chebpoly_init(FF10); mpfr_chebpoly_init(FF01);
  chebmodel_t RHS; chebmodel_init(RHS);
  mpfr_chebpoly_t RHSfr; mpfr_chebpoly_init(RHSfr);
    // F10
  mpfi_set_si(I + 0, 1); mpfi_set_si(I + 1, 0);
  chebmodel_intop_rhs(RHS, L, g, I);
  chebmodel_get_mpfr_chebpoly(RHSfr, RHS);
  mpfr_chebpoly_intop_approxsolve(FF10, Kfr, RHSfr, n);
  chebmodel_set_mpfr_chebpoly(F10, FF10);
  chebmodel_intop_newton_validate_sol_aux_fr(F10->rem, K, Tbound, MKinv, RHS, FF10);
  chebmodel_antiderivative_1(F10, F10);
  chebmodel_antiderivative_1(F10, F10);
  mpfi_add_si(F10->poly->coeffs + 0, F10->poly->coeffs + 0, 1);
    // F01
  mpfi_set_si(I + 0, 0); mpfi_set_si(I + 1, 1);
  chebmodel_intop_rhs(RHS, L, g, I);
  chebmodel_get_mpfr_chebpoly(RHSfr, RHS);
  mpfr_chebpoly_intop_approxsolve(FF01, Kfr, RHSfr, n);
  chebmodel_set_mpfr_chebpoly(F01, FF01);
  chebmodel_intop_newton_validate_sol_aux_fr(F01->rem, K, Tbound, MKinv, RHS, FF01);
  chebmodel_antiderivative_1(F01, F01);
  mpfi_add_si(F01->poly->coeffs + 0, F01->poly->coeffs + 0, 1);
  chebmodel_antiderivative_1(F01, F01);

  // clear variables
  chebmodel_lode_clear(L0);
  chebmodel_clear(g);
  chebmodel_clear(Cos + 0); chebmodel_clear(Cos + 1); chebmodel_clear(Cos + 2);
  chebmodel_lode_clear(L);
  mpfr_clear(Tbound);
  mpfr_bandmatrix_clear(MKinv);
  chebmodel_intop_clear(K);
  mpfr_chebpoly_intop_clear(Kfr);
  mpfr_chebpoly_clear(FF10); mpfr_chebpoly_clear(FF01);
  chebmodel_clear(RHS); mpfr_chebpoly_clear(RHSfr);

}


// recombine numerical basis of blayer_basis_d to obtain solution to BVP
// floating-point type: [double]
void blayer_bvp_d(double_chebpoly_t Sol, const double_chebpoly_t F10, const double_chebpoly_t F01)
{
  mpfr_t lambda, F101, F011;
  mpfr_init(lambda); mpfr_init(F101); mpfr_init(F011);
  double_chebpoly_evaluate1_fr(F101, F10);
  double_chebpoly_evaluate1_fr(F011, F01);
  mpfr_si_sub(lambda, 1, F101, MPFR_RNDN);
  mpfr_div(lambda, lambda, F011, MPFR_RNDN);
  mpfr_chebpoly_t F10fr, F01fr;
  mpfr_chebpoly_init(F10fr); mpfr_chebpoly_init(F01fr);
  mpfr_chebpoly_set_double_chebpoly(F10fr, F10); mpfr_chebpoly_set_double_chebpoly(F01fr, F01);
  mpfr_chebpoly_scalar_mul_fr(F01fr, F01fr, lambda);
  mpfr_chebpoly_add(F10fr, F10fr, F01fr);
  mpfr_chebpoly_get_double_chebpoly(Sol, F10fr);

  // clear variables
  mpfr_clear(lambda); mpfr_clear(F101); mpfr_clear(F011);
  mpfr_chebpoly_clear(F10fr); mpfr_chebpoly_clear(F01fr);
}



// recombine numerical basis of blayer_basis_fr to obtain solution to BVP
// floating-point type: [mpfr]
void blayer_bvp_fr(mpfr_chebpoly_t Sol, const mpfr_chebpoly_t F10, const mpfr_chebpoly_t F01)
{
  mpfr_t lambda, F101, F011;
  mpfr_init(lambda); mpfr_init(F101); mpfr_init(F011);
  mpfr_chebpoly_t Temp; mpfr_chebpoly_init(Temp);
  mpfr_chebpoly_evaluate1_fr(F101, F10);
  mpfr_chebpoly_evaluate1_fr(F011, F01);
  mpfr_si_sub(lambda, 1, F101, MPFR_RNDN);
  mpfr_div(lambda, lambda, F011, MPFR_RNDN);
  mpfr_chebpoly_scalar_mul_fr(Temp, F01, lambda);
  mpfr_chebpoly_add(Sol, F10, Temp);

  // clear variables
  mpfr_clear(lambda); mpfr_clear(F101); mpfr_clear(F011);
  mpfr_chebpoly_clear(Temp);
}


// recombine certified basis of blayer_basis_fi to obtain solution to BVP
void blayer_bvp_fi(chebmodel_t Sol, const chebmodel_t F10, const chebmodel_t F01)
{
  mpfi_t lambda, F101, F011;
  mpfi_init(lambda); mpfi_init(F101); mpfi_init(F011);
  chebmodel_t Temp; chebmodel_init(Temp);
  chebmodel_evaluate1(F101, F10);
  chebmodel_evaluate1(F011, F01);
  mpfi_si_sub(lambda, 1, F101);
  mpfi_div(lambda, lambda, F011);
  chebmodel_scalar_mul_fi(Temp, F01, lambda);
  chebmodel_add(Sol, F10, Temp);

  // clear variables
  mpfi_clear(lambda); mpfi_clear(F101); mpfi_clear(F011);
  chebmodel_clear(Temp);
}




/*










// determine n, h2 and d2 to obtain a validation operator
// underlying floating-point type: [double]
void blayer_validate_d(long *n, long *h2, long *d2, const mpfr_t eps, long p)
{

  long i, j, k;
  long jmax = 0;

  mpfi_t temp;
  mpfi_init(temp);

  // Boundary layer problem
  chebmodel_lode_t L0;
  chebmodel_lode_init(L0);
  chebmodel_lode_set_order(L0, 2);
  mpfi_chebpoly_set_degree(L0->a[0].poly, 0);
  mpfi_set_si(L0->a[0].poly->coeffs + 0, 1);
  chebmodel_t g;
  chebmodel_init(g); // g=0
  __mpfi_struct I[2];
  mpfi_init(I + 0);
  mpfi_init(I + 1);
  mpfi_set_si(temp, 1);
  mpfi_cos(I + 0, temp);
  mpfi_sin(I + 1, temp);
  chebmodel_struct Cos[3];
  chebmodel_init(Cos + 0);
  chebmodel_init(Cos + 1);
  chebmodel_init(Cos + 2);
  chebmodel_lode_intop_newton_solve(Cos, L0, g, I, p);

  // D^2 - 2x/eps*(cos(x)-8/10) D + 1/eps*(cos(x)-8/10)
  chebmodel_lode_t L;
  chebmodel_lode_init(L);
  mpfi_set_si(temp, 8);
  mpfi_div_si(temp, temp, 10);
  chebmodel_lode_set_order(L, 2);
  chebmodel_set(L->a + 0, Cos + 0);
  mpfi_sub(L->a[0].poly->coeffs + 0, L->a[0].poly->coeffs + 0, temp);
  chebmodel_scalar_div_fr(L->a + 0, L->a + 0, eps); // = 1/eps*(cos(x)-8/10)
  mpfi_chebpoly_set_degree(L->a[1].poly, 1);
  mpfi_set_si(L->a[1].poly->coeffs + 1, -2);
  chebmodel_mul(L->a + 1, L->a + 1, L->a + 0);


  // integral operator
  chebmodel_intop_t K;
  chebmodel_intop_init(K);
  chebmodel_intop_set_lode(K, L);
  long h = chebmodel_intop_Hwidth(K);
  long d = chebmodel_intop_Dwidth(K);
  double_bandmatrix_t M_K_inv;
  double_bandmatrix_init(M_K_inv);
  double_t bound;
  double_init(bound);

  // get a contracting operator
  chebmodel_intop_newton_contract_d(bound, M_K_inv, K, h+d);
  *n = M_K_inv->dim - 1;
  *h2 = M_K_inv->Hwidth;
  *d2 = M_K_inv->Dwidth;

  // clear variables
  mpfi_clear(temp);
  chebmodel_lode_clear(L0);
  chebmodel_clear(g);
  mpfi_clear(I + 0);
  mpfi_clear(I + 1);
  chebmodel_clear(Cos + 0);
  chebmodel_clear(Cos + 1);
  chebmodel_clear(Cos + 2);
  chebmodel_lode_clear(L);
  chebmodel_intop_clear(K);
  double_bandmatrix_clear(M_K_inv);
  double_clear(bound);

}


// determine n, h2 and d2 to obtain a validation operator
// underlying floating-point type: [mpfr]
void blayer_validate_fr(long *n, long *h2, long *d2, const mpfr_t eps, long p)
{

  long i, j, k;
  long jmax = 0;

  mpfi_t temp;
  mpfi_init(temp);

  // Boundary layer problem
  chebmodel_lode_t L0;
  chebmodel_lode_init(L0);
  chebmodel_lode_set_order(L0, 2);
  mpfi_chebpoly_set_degree(L0->a[0].poly, 0);
  mpfi_set_si(L0->a[0].poly->coeffs + 0, 1);
  chebmodel_t g;
  chebmodel_init(g); // g=0
  __mpfi_struct I[2];
  mpfi_init(I + 0);
  mpfi_init(I + 1);
  mpfi_set_si(temp, 1);
  mpfi_cos(I + 0, temp);
  mpfi_sin(I + 1, temp);
  chebmodel_struct Cos[3];
  chebmodel_init(Cos + 0);
  chebmodel_init(Cos + 1);
  chebmodel_init(Cos + 2);
  chebmodel_lode_intop_newton_solve(Cos, L0, g, I, p);

  // D^2 - 2x/eps*(cos(x)-8/10) D + 1/eps*(cos(x)-8/10)
  chebmodel_lode_t L;
  chebmodel_lode_init(L);
  mpfi_set_si(temp, 8);
  mpfi_div_si(temp, temp, 10);
  chebmodel_lode_set_order(L, 2);
  chebmodel_set(L->a + 0, Cos + 0);
  mpfi_sub(L->a[0].poly->coeffs + 0, L->a[0].poly->coeffs + 0, temp);
  chebmodel_scalar_div_fr(L->a + 0, L->a + 0, eps); // = 1/eps*(cos(x)-8/10)
  mpfi_chebpoly_set_degree(L->a[1].poly, 1);
  mpfi_set_si(L->a[1].poly->coeffs + 1, -2);
  chebmodel_mul(L->a + 1, L->a + 1, L->a + 0);


  // integral operator
  chebmodel_intop_t K;
  chebmodel_intop_init(K);
  chebmodel_intop_set_lode(K, L);
  long h = chebmodel_intop_Hwidth(K);
  long d = chebmodel_intop_Dwidth(K);
  mpfr_bandmatrix_t M_K_inv;
  mpfr_bandmatrix_init(M_K_inv);
  mpfr_t bound;
  mpfr_init(bound);

  // get a contracting operator
  chebmodel_intop_newton_contract_fr(bound, M_K_inv, K, h+d);
  *n = M_K_inv->dim - 1;
  *h2 = M_K_inv->Hwidth;
  *d2 = M_K_inv->Dwidth;

  // clear variables
  mpfi_clear(temp);
  chebmodel_lode_clear(L0);
  chebmodel_clear(g);
  mpfi_clear(I + 0);
  mpfi_clear(I + 1);
  chebmodel_clear(Cos + 0);
  chebmodel_clear(Cos + 1);
  chebmodel_clear(Cos + 2);
  chebmodel_lode_clear(L);
  chebmodel_intop_clear(K);
  mpfr_bandmatrix_clear(M_K_inv);
  mpfr_clear(bound);

}


// numerical solving without validation
// floating-point type: [mpfr]
void blayer_approxsolve_fr(mpfr_chebpoly_t P, const mpfr_t eps, long p, long n)
{
  long i;
  mpfr_t temp;
  mpfr_init(temp);

  // Boundary layer problem
  mpfr_chebpoly_lode_t L0;
  mpfr_chebpoly_lode_init(L0);
  mpfr_chebpoly_lode_set_order(L0, 2);
  mpfr_chebpoly_set_degree(L0->a + 0, 0);
  mpfr_set_si(L0->a[0].coeffs + 0, 1, MPFR_RNDN);
  mpfr_chebpoly_t g;
  mpfr_chebpoly_init(g); // g=0
  __mpfr_struct I[2];
  mpfr_inits(I + 0, I + 1, NULL);
  mpfr_set_si(temp, 1, MPFR_RNDN);
  mpfr_cos(I + 0, temp, MPFR_RNDN);
  mpfr_sin(I + 1, temp, MPFR_RNDN);
  mpfr_chebpoly_struct Cos[3];
  mpfr_chebpoly_init(Cos + 0);
  mpfr_chebpoly_init(Cos + 1);
  mpfr_chebpoly_init(Cos + 2);
  mpfr_chebpoly_lode_intop_approxsolve(Cos, L0, g, I, p);

  // D^2 - 2x/eps*(cos(x)-8/10) D + 1/eps*(cos(x)-8/10)
  mpfr_chebpoly_lode_t L;
  mpfr_chebpoly_lode_init(L);
  mpfr_chebpoly_lode_set_order(L, 2);
  mpfr_chebpoly_set(L->a + 0, Cos + 0);
  mpfr_set_si(temp, 8, MPFR_RNDN);
  mpfr_div_si(temp, temp, 10, MPFR_RNDN);
  mpfr_sub(L->a[0].coeffs + 0, L->a[0].coeffs + 0, temp, MPFR_RNDN);
  mpfr_chebpoly_scalar_div_fr(L->a + 0, L->a + 0, eps); // = 1/eps*(cos(x)-8/10)
  mpfr_chebpoly_set_degree(L->a + 1, 1);
  mpfr_set_si(L->a[1].coeffs + 1, -2, MPFR_RNDN);
  mpfr_chebpoly_mul(L->a + 1, L->a + 1, L->a + 0);

  // approx solve
  mpfr_chebpoly_struct F1[3];
  mpfr_chebpoly_struct F2[3];
  for (i = 0 ; i <= 2 ; i++) {
    mpfr_chebpoly_init(F1 + i);
    mpfr_chebpoly_init(F2 + i);
  }
  mpfr_set_si(I + 0, 1, MPFR_RNDN);
  mpfr_set_si(I + 1, 0, MPFR_RNDN);
  mpfr_chebpoly_lode_intop_approxsolve(F1, L, g, I, n);
  mpfr_set_si(I + 0, 0, MPFR_RNDN);
  mpfr_set_si(I + 1, 1, MPFR_RNDN);
  mpfr_chebpoly_lode_intop_approxsolve(F2, L, g, I, n);

  // recombine to solve BVP
  mpfr_t lambda, mu, F1a, F1b, F2a, F2b, det;
  mpfr_inits(lambda, mu, F1a, F1b, F2a, F2b, det, NULL);
  mpfr_chebpoly_evaluate_1_fr(F1a, F1 + 0);
  mpfr_chebpoly_evaluate1_fr(F1b, F1 + 0);
  mpfr_chebpoly_evaluate_1_fr(F2a, F2 + 0);
  mpfr_chebpoly_evaluate1_fr(F2b, F2 + 0);
  mpfr_mul(det, F1a, F2b, MPFR_RNDN);
  mpfr_mul(lambda, F1b, F2a, MPFR_RNDN);
  mpfr_sub(det, det, lambda, MPFR_RNDN);
  mpfr_sub(lambda, F2b, F2a, MPFR_RNDN);
  mpfr_div(lambda, lambda, det, MPFR_RNDN);
  mpfr_sub(mu, F1a, F1b, MPFR_RNDN);
  mpfr_div(mu, mu, det, MPFR_RNDN);
  // mpfr_set_si(lambda, 0, MPFR_RNDN); mpfr_set_si(mu, 1, MPFR_RNDN);
  mpfr_chebpoly_scalar_mul_fr(F1 + 0, F1 + 0, lambda);
  mpfr_chebpoly_scalar_mul_fr(F2 + 0, F2 + 0, mu);
  mpfr_chebpoly_add(P, F1 + 0, F2 + 0);

  // clear variables
  mpfr_chebpoly_lode_clear(L0);
  mpfr_chebpoly_lode_clear(L);
  mpfr_chebpoly_clear(g);
  for (i = 0 ; i <= 2 ; i++) {
    mpfr_chebpoly_clear(Cos + i);
    mpfr_chebpoly_clear(F1 + i);
    mpfr_chebpoly_clear(F2 + i);
  }
  mpfr_clears(temp, I + 0, I + 1, lambda, mu, F1a, F1b, F2a, F2b, det, NULL);

}


// validated basis of the solution space
void blayer_certified_basis(chebmodel_t F1, chebmodel_t F2, const mpfr_t eps, long p, long n)
{
  long i, j, k;

  mpfi_t temp;
  mpfi_init(temp);

  // Boundary layer problem
  chebmodel_lode_t L0;
  chebmodel_lode_init(L0);
  chebmodel_lode_set_order(L0, 2);
  mpfi_chebpoly_set_degree(L0->a[0].poly, 0);
  mpfi_set_si(L0->a[0].poly->coeffs + 0, 1);
  chebmodel_t g;
  chebmodel_init(g); // g=0
  __mpfi_struct I[2];
  mpfi_init(I + 0);
  mpfi_init(I + 1);
  mpfi_set_si(temp, 1);
  mpfi_cos(I + 0, temp);
  mpfi_sin(I + 1, temp);
  chebmodel_struct Cos[3];
  chebmodel_init(Cos + 0);
  chebmodel_init(Cos + 1);
  chebmodel_init(Cos + 2);
  chebmodel_lode_intop_newton_solve(Cos, L0, g, I, p);

  // D^2 - 2x/eps*(cos(x)-8/10) D + 1/eps*(cos(x)-8/10)
  chebmodel_lode_t L;
  chebmodel_lode_init(L);
  mpfi_set_si(temp, 8);
  mpfi_div_si(temp, temp, 10);
  chebmodel_lode_set_order(L, 2);
  chebmodel_set(L->a + 0, Cos + 0);
  mpfi_sub(L->a[0].poly->coeffs + 0, L->a[0].poly->coeffs + 0, temp);
  chebmodel_scalar_div_fr(L->a + 0, L->a + 0, eps); // = 1/eps*(cos(x)-8/10)
  mpfi_chebpoly_set_degree(L->a[1].poly, 1);
  mpfi_set_si(L->a[1].poly->coeffs + 1, -2);
  chebmodel_mul(L->a + 1, L->a + 1, L->a + 0);

  __mpfi_struct I1[2]; __mpfi_struct I2[2];
  mpfi_init(I1 + 0); mpfi_init(I1 + 1); mpfi_init(I2 + 0); mpfi_init(I2 + 1);
  mpfi_set_si(I1 + 0, 1); mpfi_set_si(I1 + 1, 0);
  mpfi_set_si(I2 + 0, 0); mpfi_set_si(I2 + 1, 1);

  // solve
  chebmodel_struct Sols1[3]; chebmodel_struct Sols2[3];
  chebmodel_init(Sols1 + 0); chebmodel_init(Sols1 + 1); chebmodel_init(Sols1 + 2);
  chebmodel_init(Sols2 + 0); chebmodel_init(Sols2 + 1); chebmodel_init(Sols2 + 2);
  chebmodel_lode_intop_newton_solve(Sols1, L, g, I1, n);
  chebmodel_lode_intop_newton_solve(Sols2, L, g, I2, n);
  chebmodel_set(F1, Sols1 + 0); chebmodel_set(F2, Sols2 + 0);

  // clear variables
  mpfi_clear(temp);
  chebmodel_lode_clear(L0);
  chebmodel_clear(g);
  mpfi_clear(I + 0); mpfi_clear(I + 1);
  mpfi_clear(I1 + 0); mpfi_clear(I1 + 1);
  mpfi_clear(I2 + 0); mpfi_clear(I2 + 1);
  chebmodel_clear(Cos + 0);
  chebmodel_clear(Cos + 1);
  chebmodel_clear(Cos + 2);
  chebmodel_lode_clear(L);
  chebmodel_clear(Sols1 + 0); chebmodel_clear(Sols1 + 1), chebmodel_clear(Sols1 + 2);
  chebmodel_clear(Sols2 + 0); chebmodel_clear(Sols2 + 1), chebmodel_clear(Sols2 + 2);

}


int blayer_solve_prec(const mpfr_t eps, long p, long n, const mpfr_t max_err)
{

  chebmodel_t F1, F2; chebmodel_init(F1); chebmodel_init(F2);
  blayer_certified_basis(F1, F2, eps, p, n);

  printf("basis errors = %.10e -- %.10e\n\n", mpfr_get_d(F1->rem, MPFR_RNDU), mpfr_get_d(F2->rem, MPFR_RNDU));

  // recombine
  chebmodel_t F; chebmodel_init(F);
  mpfi_t lambda, F1_1, F2_1;
  mpfi_init(lambda); mpfi_init(F1_1); mpfi_init(F2_1);
  chebmodel_evaluate1(F1_1, F1); chebmodel_evaluate1(F2_1, F2);
  printf("F1(1) and F2(1) = ");
  mpfi_out_str(stdout, 10, 10, F1_1); printf("  "); mpfi_out_str(stdout, 10, 10, F2_1); printf("\n");
  mpfi_si_sub(lambda, 1, F1_1);
  mpfi_div(lambda, lambda, F2_1);
  chebmodel_scalar_mul_fi(F, F2, lambda);
  chebmodel_add(F, F, F1);

  printf("initial error after recombining: %.10e\n\n", mpfr_get_d(F->rem, MPFR_RNDU));

  // find optimal degree
  while (mpfr_cmp(F->rem, max_err) <= 0)
    chebmodel_truncate(F, F->poly->degree - 1);
  long best_degree = F->poly->degree + 1;

  printf("optimal degree = %ld\n\n", best_degree);

  // clear variables
  chebmodel_clear(F); chebmodel_clear(F1); chebmodel_clear(F2);
  mpfi_clear(lambda); mpfi_clear(F1_1); mpfi_clear(F2_1);

  return best_degree;
}



// create gnuplot file to plot P with n equidistant points from -1 to 1
void double_chebpoly_plot(const double_chebpoly_t P, long n, FILE * filename)
{
  double step = (double) 2 / (n-1);
  long i;
  mpfr_t x, y;
  mpfr_init(x);
  mpfr_init(y);
  for (i = 0 ; i < n ; i++) {
    mpfr_set_d(x, -1 + i * step, MPFR_RNDN);
    double_chebpoly_evaluate_fr(y, P, x);
    fprintf(filename, "%f\t%f\n", mpfr_get_d(x, MPFR_RNDN), mpfr_get_d(y, MPFR_RNDN));
  }
}


// create gnuplot file to plot P with n equidistant points from -1 to 1
void mpfr_chebpoly_plot(const mpfr_chebpoly_t P, long n, FILE * filename)
{
  double step = (double) 2 / (n-1);
  long i;
  mpfr_t x, y;
  mpfr_init(x);
  mpfr_init(y);
  for (i = 0 ; i < n ; i++) {
    mpfr_set_d(x, -1 + i * step, MPFR_RNDN);
    mpfr_chebpoly_evaluate_fr(y, P, x);
    fprintf(filename, "%f\t%f\n", mpfr_get_d(x, MPFR_RNDN), mpfr_get_d(y, MPFR_RNDN));
  }
  mpfr_clear(x);
  mpfr_clear(y);
}


*/


int main()
{

/*
  char a;
  char b;
  scanf(" %c", &a);
  scanf(" %c", &b);
  printf("a = %c, b = %c.\n", a, b);
*/


  // Numeric or certified
  int bool_certified;
  char char_certified;
  printf("Certified result? (y/n) ");
  scanf(" %c", &char_certified);
  switch (char_certified) {
  case 'n': // numeric only
    bool_certified = 0;
    break;
  case 'y': // certified version
    bool_certified = 1;
    break;
  default:
    fprintf(stderr, "Error: invalid input.\n");
    exit(1);
  }

  // double or mpfr in the case "only nuemric" (always mpfr and mpfi for certified)
  int bool_mpfr = 1;
  if (!bool_certified) {
  char char_mpfr;
    printf("Use mpfr instead of double? (y/n) ");
    scanf(" %c", &char_mpfr);
    switch (char_mpfr) {
    case 'n': // use double
      bool_mpfr = 0;
      break;
    case 'y': // use mpfr
      bool_mpfr = 1;
      break;
    default:
      fprintf(stderr, "Error: invalid input.\n");
      exit(1);
    }
  }

  // if mpfr: precision
  mpfr_prec_t prec = 53;
  long long_prec;
  if (bool_mpfr) {
    printf("mpfr precision in bits? prec = ");
    scanf("%ld", &long_prec);
    prec = long_prec;
    mpfr_set_default_prec(prec);
  }

  // Parameter eps
  mpfr_t eps;
  mpfr_init(eps);
  printf("epsilon = ");
  mpfr_inp_str(eps, stdin, 10, MPFR_RNDN);

  // Approximation degree N
  long N;
  printf("Approximation degree? N = ");
  scanf("%ld", &N);

  // Approximation degree p for cosine
  long p;
  printf("Approximation degree for cosine? p = ");
  scanf("%ld", &p);

  // creating filename
  char filename[1000];
  strcpy(filename, "plots/blayer-plot-");
    // numeric or certified
  if (bool_certified)
    strcat(filename, "certified-");
  else
    strcat(filename, "numeric-");
    // eps
  strcat(filename, "eps");
  char str_eps[100];
  char str_exp_eps[100];
  mpfr_exp_t exp_eps;
  mpfr_get_str(str_eps, &exp_eps, 10, 5, eps, MPFR_RNDN);
  sprintf(str_exp_eps, "e%ld-", (long) exp_eps);
  strcat(str_eps, str_exp_eps);
  strcat(filename, str_eps);
    // prec
  char str_prec[100];
  if (bool_mpfr)
    sprintf(str_prec, "prec%ld-", (long) prec);
  else
    strcpy(str_prec, "precdouble-");
  strcat(filename, str_prec);
    // N
  char str_N[100];
  sprintf(str_N, "N%ld-", N);
  strcat(filename, str_N);
    // p
  char str_p[100];
  sprintf(str_p, "p%ld", p);
  strcat(filename, str_p);
    // suffix
  strcat(filename, ".dat");


  printf("bool_certified = %d.\n", bool_certified);
  printf("bool_mpfr = %d.\n", bool_mpfr);
  printf("prec = %ld.\n", prec);
  printf("eps = "); mpfr_out_str(stdout, 10, 10, eps, MPFR_RNDN); printf(".\n");
  printf("N = %ld.\n", N);
  printf("p = %ld.\n", p);

  printf("\nfilename: %s.\n", filename);

  long Nval, h2, d2;
  long plotfreq = 1000;


  // numeric with double
  if (!bool_certified && !bool_mpfr) {

    // compute
    double_chebpoly_t F10, F01, Sol;
    double_chebpoly_init(F10); double_chebpoly_init(F01); double_chebpoly_init(Sol);
    blayer_basis_d(F10, F01, eps, p, N);
    blayer_bvp_d(Sol, F10, F01);

    // plot in file
    FILE * plotfile = fopen(filename, "w+");
      // header
    fprintf(plotfile, "#t\tF10\tF01\tSol\n");
      // plot
    long i;
    double t, F10t, F01t, Solt;
    for (i = -plotfreq ; i <= plotfreq ; i++) {
      t = ((double) i) / plotfreq;
      double_chebpoly_evaluate_d(&F10t, F10, &t);
      double_chebpoly_evaluate_d(&F01t, F01, &t);
      double_chebpoly_evaluate_d(&Solt, Sol, &t);
      fprintf(plotfile, "%.10e\t%.10e\t%.10e\t%.10e\n", t, F10t, F01t, Solt);
    }
    fclose(plotfile);

  }


  // numeric with mpfr
  if (!bool_certified && bool_mpfr) {

    // compute
    mpfr_chebpoly_t F10, F01, Sol;
    mpfr_chebpoly_init(F10); mpfr_chebpoly_init(F01); mpfr_chebpoly_init(Sol);
    blayer_basis_fr(F10, F01, eps, p, N);
    blayer_bvp_fr(Sol, F10, F01);

    // plot in file
    FILE * plotfile = fopen(filename, "w+");
      // header
    fprintf(plotfile, "#t\tF10\tF01\tSol\n");
      // plot
    long i;
    mpfr_t t, F10t, F01t, Solt;
    mpfr_init(t); mpfr_init(F10t); mpfr_init(F01t); mpfr_init(Solt);
    for (i = -plotfreq ; i <= plotfreq ; i++) {
      mpfr_set_si(t, i, MPFR_RNDN);
      mpfr_div_si(t, t, plotfreq, MPFR_RNDN);
      mpfr_chebpoly_evaluate_fr(F10t, F10, t);
      mpfr_chebpoly_evaluate_fr(F01t, F01, t);
      mpfr_chebpoly_evaluate_fr(Solt, Sol, t);
      fprintf(plotfile, "%.10e\t%.10e\t%.10e\t%.10e\n", mpfr_get_d(t, MPFR_RNDN),
        mpfr_get_d(F10t, MPFR_RNDN), mpfr_get_d(F01t, MPFR_RNDN), mpfr_get_d(Solt, MPFR_RNDN));
    }
    fclose(plotfile);

  }

  // TODO: certified











  mpfr_t max_err; mpfr_init(max_err);
  mpfr_set_si(max_err, 1, MPFR_RNDN);
  mpfr_mul_2si(max_err, max_err, -53, MPFR_RNDN);

/*
  long p = 100; long n = 200;
  blayer_solve_prec(eps, p, n, max_err);
*/


// obtain contracting op
/*
  long p = 30;

  long n, h2, d2;

  blayer_validate_d(&n, &h2, &d2, eps, p);

  printf("--------------\neps = %f, p = %ld, n = %ld, h2 = %ld, d2 = %ld\n", mpfr_get_d(eps, MPFR_RNDN), p, n, h2, d2);
*/

// plot sol (with double)
/*
  long p = 50;
  long n = 1630;
  mpfr_chebpoly_t P;
  mpfr_chebpoly_init(P);
  blayer_approxsolve_fr(P, eps, p, n);
  FILE * file_plot = fopen("blayer_plot_false.dat", "w+");
  fprintf(file_plot, "# plot for boundary layer example with inaccurate approx\n# eps = %f, p = %ld, n = %ld\n", mpfr_get_d(eps, MPFR_RNDN), p, n);
  long N = 1000;
  mpfr_chebpoly_plot(P, N, file_plot);
  fprintf(stderr, "P: OK!\n");
  fclose(file_plot);
  mpfr_chebpoly_clear(P);
  mpfr_clear(eps);
*/

// plot sol (with mpfr)
/*
  long p = 50;
  long n = 1000;
  mpfr_chebpoly_t P;
  mpfr_chebpoly_init(P);
  blayer_approxsolve_fr(P, eps, p, n);
  FILE * file_plot = fopen("blayer_plot_basis_2.dat", "w+");
  fprintf(file_plot, "# plot for boundary layer example\n# eps = 0.00001, u(-1) = 2, u'(-1) = 1\n");
  long N = 1000;
  mpfr_chebpoly_plot(P, N, file_plot);
  fprintf(stderr, "P: OK!\n");
  fclose(file_plot);
  mpfr_chebpoly_clear(P);
  mpfr_clear(eps);
*/





  return 0;
}
