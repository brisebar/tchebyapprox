/*
Example for the TchebyApprox library: boundary layer from Olver and Townsend
change the parameter eps to observe the almost banded parameters
of the approx inverse
*/



#include <stdio.h>
#include <stdlib.h>

#include <gmp.h>
#include <mpfr.h>
#include <mpfi.h>

#include "chebmodel_intop_newton.h"

/*
void double_chebpoly_evaluate1(double_t y, const double_chebpoly_t P)
{
  long i;
  long n = P->degree;
  *y = 0;

  for (i = 0 ; i <= n ; i++)
    *y += P->coeffs[i];
}

void double_chebpoly_evaluate_1(double_t y, const double_chebpoly_t P)
{
  long i;
  long n = P->degree;
  *y = 0;

  for (i = 0 ; i <= n ; i++)
    if (i%2)
      *y -= P->coeffs[i];
    else
      *y += P->coeffs[i];
}
*/

void double_chebpoly_evaluate1_fr(mpfr_t y, const double_chebpoly_t P)
{
  long i;
  long n = P->degree;
  mpfr_set_zero(y, 1);

  for (i = 0 ; i <= n ; i++)
    mpfr_add_d(y, y, P->coeffs[i], MPFR_RNDN);
}

void double_chebpoly_evaluate_1_fr(mpfr_t y, const double_chebpoly_t P)
{
  long i;
  long n = P->degree;
  mpfr_set_zero(y, 1);

  for (i = 0 ; i <= n ; i++)
    if (i%2)
      mpfr_sub_d(y, y, P->coeffs[i], MPFR_RNDN);
    else
      mpfr_add_d(y, y, P->coeffs[i], MPFR_RNDN);
}

void mpfr_chebpoly_evaluate1_fr(mpfr_t y, const mpfr_chebpoly_t P)
{
  long i;
  long n = P->degree;
  mpfr_set_zero(y, 1);

  for (i = 0 ; i <= n ; i++)
    mpfr_add(y, y, P->coeffs + i, MPFR_RNDN);
}

void mpfr_chebpoly_evaluate_1_fr(mpfr_t y, const mpfr_chebpoly_t P)
{
  long i;
  long n = P->degree;
  mpfr_set_zero(y, 1);

  for (i = 0 ; i <= n ; i++)
    if (i%2)
      mpfr_sub(y, y, P->coeffs + i, MPFR_RNDN);
    else
      mpfr_add(y, y, P->coeffs + i, MPFR_RNDN);
}


/*
void chebmodel_evaluate1(mpfi_t y, const chebmodel_t P)
{
  long i;
  long n = P->poly->degree;
  mpfi_set_si(y, 0);

  for (i = 0 ; i <= n ; i++)
    mpfi_add(y, y, P->poly->coeffs + i);

  mpfr_add(&(y->right), &(y->right), P->rem, MPFR_RNDU);
  mpfr_sub(&(y->left), &(y->left), P->rem, MPFR_RNDD);
}
*/


// determine n, h2 and d2 to obtain a validation operator
// underlying floating-point type: [double]
void blayer_validate_d(long *n, long *h2, long *d2, const mpfr_t eps, long p)
{

  long i, j, k;
  long jmax = 0;

  mpfi_t temp;
  mpfi_init(temp);

  // Boundary layer problem
  chebmodel_lode_t L0;
  chebmodel_lode_init(L0);
  chebmodel_lode_set_order(L0, 2);
  mpfi_chebpoly_set_degree(L0->a[0].poly, 0);
  mpfi_set_si(L0->a[0].poly->coeffs + 0, 1);
  chebmodel_t g;
  chebmodel_init(g); // g=0
  __mpfi_struct I[2];
  mpfi_init(I + 0);
  mpfi_init(I + 1);
  mpfi_set_si(temp, 1);
  mpfi_cos(I + 0, temp);
  mpfi_sin(I + 1, temp);
  chebmodel_struct Cos[3];
  chebmodel_init(Cos + 0);
  chebmodel_init(Cos + 1);
  chebmodel_init(Cos + 2);
  chebmodel_lode_intop_newton_solve(Cos, L0, g, I, p);

  // D^2 - 2x/eps*(cos(x)-8/10) D + 1/eps*(cos(x)-8/10)
  chebmodel_lode_t L;
  chebmodel_lode_init(L);
  mpfi_set_si(temp, 8);
  mpfi_div_si(temp, temp, 10);
  chebmodel_lode_set_order(L, 2);
  chebmodel_set(L->a + 0, Cos + 0);
  mpfi_sub(L->a[0].poly->coeffs + 0, L->a[0].poly->coeffs + 0, temp);
  chebmodel_scalar_div_fr(L->a + 0, L->a + 0, eps); // = 1/eps*(cos(x)-8/10)
  mpfi_chebpoly_set_degree(L->a[1].poly, 1);
  mpfi_set_si(L->a[1].poly->coeffs + 1, -2);
  chebmodel_mul(L->a + 1, L->a + 1, L->a + 0);


  // integral operator
  chebmodel_intop_t K;
  chebmodel_intop_init(K);
  chebmodel_intop_set_lode(K, L);
  long h = chebmodel_intop_Hwidth(K);
  long d = chebmodel_intop_Dwidth(K);
  double_bandmatrix_t M_K_inv;
  double_bandmatrix_init(M_K_inv);
  double_t bound;
  double_init(bound);

  // get a contracting operator
  chebmodel_intop_newton_contract_d(bound, M_K_inv, K, h+d);
  *n = M_K_inv->dim - 1;
  *h2 = M_K_inv->Hwidth;
  *d2 = M_K_inv->Dwidth;

  // clear variables
  mpfi_clear(temp);
  chebmodel_lode_clear(L0);
  chebmodel_clear(g);
  mpfi_clear(I + 0);
  mpfi_clear(I + 1);
  chebmodel_clear(Cos + 0);
  chebmodel_clear(Cos + 1);
  chebmodel_clear(Cos + 2);
  chebmodel_lode_clear(L);
  chebmodel_intop_clear(K);
  double_bandmatrix_clear(M_K_inv);
  double_clear(bound);

}


// determine n, h2 and d2 to obtain a validation operator
// underlying floating-point type: [mpfr]
void blayer_validate_fr(long *n, long *h2, long *d2, const mpfr_t eps, long p)
{

  long i, j, k;
  long jmax = 0;

  mpfi_t temp;
  mpfi_init(temp);

  // Boundary layer problem
  chebmodel_lode_t L0;
  chebmodel_lode_init(L0);
  chebmodel_lode_set_order(L0, 2);
  mpfi_chebpoly_set_degree(L0->a[0].poly, 0);
  mpfi_set_si(L0->a[0].poly->coeffs + 0, 1);
  chebmodel_t g;
  chebmodel_init(g); // g=0
  __mpfi_struct I[2];
  mpfi_init(I + 0);
  mpfi_init(I + 1);
  mpfi_set_si(temp, 1);
  mpfi_cos(I + 0, temp);
  mpfi_sin(I + 1, temp);
  chebmodel_struct Cos[3];
  chebmodel_init(Cos + 0);
  chebmodel_init(Cos + 1);
  chebmodel_init(Cos + 2);
  chebmodel_lode_intop_newton_solve(Cos, L0, g, I, p);

  // D^2 - 2x/eps*(cos(x)-8/10) D + 1/eps*(cos(x)-8/10)
  chebmodel_lode_t L;
  chebmodel_lode_init(L);
  mpfi_set_si(temp, 8);
  mpfi_div_si(temp, temp, 10);
  chebmodel_lode_set_order(L, 2);
  chebmodel_set(L->a + 0, Cos + 0);
  mpfi_sub(L->a[0].poly->coeffs + 0, L->a[0].poly->coeffs + 0, temp);
  chebmodel_scalar_div_fr(L->a + 0, L->a + 0, eps); // = 1/eps*(cos(x)-8/10)
  mpfi_chebpoly_set_degree(L->a[1].poly, 1);
  mpfi_set_si(L->a[1].poly->coeffs + 1, -2);
  chebmodel_mul(L->a + 1, L->a + 1, L->a + 0);


  // integral operator
  chebmodel_intop_t K;
  chebmodel_intop_init(K);
  chebmodel_intop_set_lode(K, L);
  long h = chebmodel_intop_Hwidth(K);
  long d = chebmodel_intop_Dwidth(K);
  mpfr_bandmatrix_t M_K_inv;
  mpfr_bandmatrix_init(M_K_inv);
  mpfr_t bound;
  mpfr_init(bound);

  // get a contracting operator
  chebmodel_intop_newton_contract_fr(bound, M_K_inv, K, h+d);
  *n = M_K_inv->dim - 1;
  *h2 = M_K_inv->Hwidth;
  *d2 = M_K_inv->Dwidth;

  // clear variables
  mpfi_clear(temp);
  chebmodel_lode_clear(L0);
  chebmodel_clear(g);
  mpfi_clear(I + 0);
  mpfi_clear(I + 1);
  chebmodel_clear(Cos + 0);
  chebmodel_clear(Cos + 1);
  chebmodel_clear(Cos + 2);
  chebmodel_lode_clear(L);
  chebmodel_intop_clear(K);
  mpfr_bandmatrix_clear(M_K_inv);
  mpfr_clear(bound);

}


// numerical solving without validation
// floating-point type: [double]
void blayer_approxsolve_d(double_chebpoly_t P, const mpfr_t eps, long p, long n)
{
  long i;

  // Boundary layer problem
  double_chebpoly_lode_t L0;
  double_chebpoly_lode_init(L0);
  double_chebpoly_lode_set_order(L0, 2);
  double_chebpoly_set_degree(L0->a + 0, 0);
  double_set_si(L0->a[0].coeffs + 0, 1, MPFR_RNDN);
  double_chebpoly_t g;
  double_chebpoly_init(g); // g=0
  double I[2];
  I[0] = cos(1);
  I[1] = sin(1);
  double_chebpoly_struct Cos[3];
  double_chebpoly_init(Cos + 0);
  double_chebpoly_init(Cos + 1);
  double_chebpoly_init(Cos + 2);
  double_chebpoly_lode_intop_approxsolve(Cos, L0, g, I, p);

  // D^2 - 2x/eps*(cos(x)-8/10) D + 1/eps*(cos(x)-8/10)
  double_chebpoly_lode_t L;
  double_chebpoly_lode_init(L);
  double_chebpoly_lode_set_order(L, 2);
  double_chebpoly_set(L->a + 0, Cos + 0);
  L->a[0].coeffs[0] -= (double) 8/10;
  double_chebpoly_scalar_div_fr(L->a + 0, L->a + 0, eps); // = 1/eps*(cos(x)-8/10)
  double_chebpoly_set_degree(L->a + 1, 1);
  double_set_si(L->a[1].coeffs + 1, -2, MPFR_RNDN);
  double_chebpoly_mul(L->a + 1, L->a + 1, L->a + 0);

  // approx solve
  double_chebpoly_struct F1[3];
  double_chebpoly_struct F2[3];
  for (i = 0 ; i <= 2 ; i++) {
    double_chebpoly_init(F1 + i);
    double_chebpoly_init(F2 + i);
  }
  I[0] = 1; I[1] = 0;
  double_chebpoly_lode_intop_approxsolve(F1, L, g, I, n);
  I[0] = 0; I[1] = 1;
  double_chebpoly_lode_intop_approxsolve(F2, L, g, I, n);

  // recombine to solve BVP
  mpfr_t lambda, mu, F1a, F1b, F2a, F2b, det;
  mpfr_inits(lambda, mu, F1a, F1b, F2a, F2b, det, NULL);
  double_chebpoly_evaluate_1_fr(F1a, F1 + 0);
  double_chebpoly_evaluate1_fr(F1b, F1 + 0);
  double_chebpoly_evaluate_1_fr(F2a, F2 + 0);
  double_chebpoly_evaluate1_fr(F2b, F2 + 0);
  mpfr_mul(det, F1a, F2b, MPFR_RNDN);
  mpfr_mul(lambda, F1b, F2a, MPFR_RNDN);
  mpfr_sub(det, det, lambda, MPFR_RNDN);
  mpfr_sub(lambda, F2b, F2a, MPFR_RNDN);
  mpfr_div(lambda, lambda, det, MPFR_RNDN);
  mpfr_sub(mu, F1a, F1b, MPFR_RNDN);
  mpfr_div(mu, mu, det, MPFR_RNDN);
  //mpfr_set_si(lambda, 0, MPFR_RNDN); mpfr_set_si(mu, 1, MPFR_RNDN);
  mpfr_chebpoly_t G1, G2;
  mpfr_chebpoly_init(G1); mpfr_chebpoly_init(G2);
  mpfr_chebpoly_set_double_chebpoly(G1, F1 + 0);
  mpfr_chebpoly_set_double_chebpoly(G2, F2 + 0);
  mpfr_chebpoly_scalar_mul_fr(G1, G1, lambda);
  mpfr_chebpoly_scalar_mul_fr(G2, G2, mu);
  mpfr_chebpoly_add(G1, G1, G2);
  mpfr_chebpoly_get_double_chebpoly(P, G1);

  // clear variables
  double_chebpoly_lode_clear(L0);
  double_chebpoly_lode_clear(L);
  double_chebpoly_clear(g);
  for (i = 0 ; i <= 2 ; i++) {
    double_chebpoly_clear(Cos + i);
    double_chebpoly_clear(F1 + i);
    double_chebpoly_clear(F2 + i);
  }
  mpfr_clears(lambda, mu, F1a, F1b, F2a, F2b, det, NULL);
  mpfr_chebpoly_clear(G1); mpfr_chebpoly_clear(G2);

}


// numerical solving without validation
// floating-point type: [mpfr]
void blayer_approxsolve_fr(mpfr_chebpoly_t P, const mpfr_t eps, long p, long n)
{
  long i;
  mpfr_t temp;
  mpfr_init(temp);

  // Boundary layer problem
  mpfr_chebpoly_lode_t L0;
  mpfr_chebpoly_lode_init(L0);
  mpfr_chebpoly_lode_set_order(L0, 2);
  mpfr_chebpoly_set_degree(L0->a + 0, 0);
  mpfr_set_si(L0->a[0].coeffs + 0, 1, MPFR_RNDN);
  mpfr_chebpoly_t g;
  mpfr_chebpoly_init(g); // g=0
  __mpfr_struct I[2];
  mpfr_inits(I + 0, I + 1, NULL);
  mpfr_set_si(temp, 1, MPFR_RNDN);
  mpfr_cos(I + 0, temp, MPFR_RNDN);
  mpfr_sin(I + 1, temp, MPFR_RNDN);
  mpfr_chebpoly_struct Cos[3];
  mpfr_chebpoly_init(Cos + 0);
  mpfr_chebpoly_init(Cos + 1);
  mpfr_chebpoly_init(Cos + 2);
  mpfr_chebpoly_lode_intop_approxsolve(Cos, L0, g, I, p);

  // D^2 - 2x/eps*(cos(x)-8/10) D + 1/eps*(cos(x)-8/10)
  mpfr_chebpoly_lode_t L;
  mpfr_chebpoly_lode_init(L);
  mpfr_chebpoly_lode_set_order(L, 2);
  mpfr_chebpoly_set(L->a + 0, Cos + 0);
  mpfr_set_si(temp, 8, MPFR_RNDN);
  mpfr_div_si(temp, temp, 10, MPFR_RNDN);
  mpfr_sub(L->a[0].coeffs + 0, L->a[0].coeffs + 0, temp, MPFR_RNDN);
  mpfr_chebpoly_scalar_div_fr(L->a + 0, L->a + 0, eps); // = 1/eps*(cos(x)-8/10)
  mpfr_chebpoly_set_degree(L->a + 1, 1);
  mpfr_set_si(L->a[1].coeffs + 1, -2, MPFR_RNDN);
  mpfr_chebpoly_mul(L->a + 1, L->a + 1, L->a + 0);

  // approx solve
  mpfr_chebpoly_struct F1[3];
  mpfr_chebpoly_struct F2[3];
  for (i = 0 ; i <= 2 ; i++) {
    mpfr_chebpoly_init(F1 + i);
    mpfr_chebpoly_init(F2 + i);
  }
  mpfr_set_si(I + 0, 1, MPFR_RNDN);
  mpfr_set_si(I + 1, 0, MPFR_RNDN);
  mpfr_chebpoly_lode_intop_approxsolve(F1, L, g, I, n);
  mpfr_set_si(I + 0, 0, MPFR_RNDN);
  mpfr_set_si(I + 1, 1, MPFR_RNDN);
  mpfr_chebpoly_lode_intop_approxsolve(F2, L, g, I, n);

  // recombine to solve BVP
  mpfr_t lambda, mu, F1a, F1b, F2a, F2b, det;
  mpfr_inits(lambda, mu, F1a, F1b, F2a, F2b, det, NULL);
  mpfr_chebpoly_evaluate_1_fr(F1a, F1 + 0);
  mpfr_chebpoly_evaluate1_fr(F1b, F1 + 0);
  mpfr_chebpoly_evaluate_1_fr(F2a, F2 + 0);
  mpfr_chebpoly_evaluate1_fr(F2b, F2 + 0);
  mpfr_mul(det, F1a, F2b, MPFR_RNDN);
  mpfr_mul(lambda, F1b, F2a, MPFR_RNDN);
  mpfr_sub(det, det, lambda, MPFR_RNDN);
  mpfr_sub(lambda, F2b, F2a, MPFR_RNDN);
  mpfr_div(lambda, lambda, det, MPFR_RNDN);
  mpfr_sub(mu, F1a, F1b, MPFR_RNDN);
  mpfr_div(mu, mu, det, MPFR_RNDN);
  // mpfr_set_si(lambda, 0, MPFR_RNDN); mpfr_set_si(mu, 1, MPFR_RNDN);
  mpfr_chebpoly_scalar_mul_fr(F1 + 0, F1 + 0, lambda);
  mpfr_chebpoly_scalar_mul_fr(F2 + 0, F2 + 0, mu);
  mpfr_chebpoly_add(P, F1 + 0, F2 + 0);

  // clear variables
  mpfr_chebpoly_lode_clear(L0);
  mpfr_chebpoly_lode_clear(L);
  mpfr_chebpoly_clear(g);
  for (i = 0 ; i <= 2 ; i++) {
    mpfr_chebpoly_clear(Cos + i);
    mpfr_chebpoly_clear(F1 + i);
    mpfr_chebpoly_clear(F2 + i);
  }
  mpfr_clears(temp, I + 0, I + 1, lambda, mu, F1a, F1b, F2a, F2b, det, NULL);

}


// validated basis of the solution space
void blayer_certified_basis(chebmodel_t F1, chebmodel_t F2, const mpfr_t eps, long p, long n)
{
  long i, j, k;

  mpfi_t temp;
  mpfi_init(temp);

  // Boundary layer problem
  chebmodel_lode_t L0;
  chebmodel_lode_init(L0);
  chebmodel_lode_set_order(L0, 2);
  mpfi_chebpoly_set_degree(L0->a[0].poly, 0);
  mpfi_set_si(L0->a[0].poly->coeffs + 0, 1);
  chebmodel_t g;
  chebmodel_init(g); // g=0
  __mpfi_struct I[2];
  mpfi_init(I + 0);
  mpfi_init(I + 1);
  mpfi_set_si(temp, 1);
  mpfi_cos(I + 0, temp);
  mpfi_sin(I + 1, temp);
  chebmodel_struct Cos[3];
  chebmodel_init(Cos + 0);
  chebmodel_init(Cos + 1);
  chebmodel_init(Cos + 2);
  chebmodel_lode_intop_newton_solve(Cos, L0, g, I, p);

  // D^2 - 2x/eps*(cos(x)-8/10) D + 1/eps*(cos(x)-8/10)
  chebmodel_lode_t L;
  chebmodel_lode_init(L);
  mpfi_set_si(temp, 8);
  mpfi_div_si(temp, temp, 10);
  chebmodel_lode_set_order(L, 2);
  chebmodel_set(L->a + 0, Cos + 0);
  mpfi_sub(L->a[0].poly->coeffs + 0, L->a[0].poly->coeffs + 0, temp);
  chebmodel_scalar_div_fr(L->a + 0, L->a + 0, eps); // = 1/eps*(cos(x)-8/10)
  mpfi_chebpoly_set_degree(L->a[1].poly, 1);
  mpfi_set_si(L->a[1].poly->coeffs + 1, -2);
  chebmodel_mul(L->a + 1, L->a + 1, L->a + 0);

  __mpfi_struct I1[2]; __mpfi_struct I2[2];
  mpfi_init(I1 + 0); mpfi_init(I1 + 1); mpfi_init(I2 + 0); mpfi_init(I2 + 1);
  mpfi_set_si(I1 + 0, 1); mpfi_set_si(I1 + 1, 0);
  mpfi_set_si(I2 + 0, 0); mpfi_set_si(I2 + 1, 1);

  // solve
  chebmodel_struct Sols1[3]; chebmodel_struct Sols2[3];
  chebmodel_init(Sols1 + 0); chebmodel_init(Sols1 + 1); chebmodel_init(Sols1 + 2);
  chebmodel_init(Sols2 + 0); chebmodel_init(Sols2 + 1); chebmodel_init(Sols2 + 2);
  chebmodel_lode_intop_newton_solve(Sols1, L, g, I1, n);
  chebmodel_lode_intop_newton_solve(Sols2, L, g, I2, n);
  chebmodel_set(F1, Sols1 + 0); chebmodel_set(F2, Sols2 + 0);

  // clear variables
  mpfi_clear(temp);
  chebmodel_lode_clear(L0);
  chebmodel_clear(g);
  mpfi_clear(I + 0); mpfi_clear(I + 1);
  mpfi_clear(I1 + 0); mpfi_clear(I1 + 1);
  mpfi_clear(I2 + 0); mpfi_clear(I2 + 1);
  chebmodel_clear(Cos + 0);
  chebmodel_clear(Cos + 1);
  chebmodel_clear(Cos + 2);
  chebmodel_lode_clear(L);
  chebmodel_clear(Sols1 + 0); chebmodel_clear(Sols1 + 1), chebmodel_clear(Sols1 + 2);
  chebmodel_clear(Sols2 + 0); chebmodel_clear(Sols2 + 1), chebmodel_clear(Sols2 + 2);

}


int blayer_solve_prec(const mpfr_t eps, long p, long n, const mpfr_t max_err)
{

  chebmodel_t F1, F2; chebmodel_init(F1); chebmodel_init(F2);
  blayer_certified_basis(F1, F2, eps, p, n);

  printf("basis errors = %.10e -- %.10e\n\n", mpfr_get_d(F1->rem, MPFR_RNDU), mpfr_get_d(F2->rem, MPFR_RNDU));

  // recombine
  chebmodel_t F; chebmodel_init(F);
  mpfi_t lambda, F1_1, F2_1;
  mpfi_init(lambda); mpfi_init(F1_1); mpfi_init(F2_1);
  chebmodel_evaluate1(F1_1, F1); chebmodel_evaluate1(F2_1, F2);
  printf("F1(1) and F2(1) = ");
  mpfi_out_str(stdout, 10, 10, F1_1); printf("  "); mpfi_out_str(stdout, 10, 10, F2_1); printf("\n");
  mpfi_si_sub(lambda, 1, F1_1);
  mpfi_div(lambda, lambda, F2_1);
  chebmodel_scalar_mul_fi(F, F2, lambda);
  chebmodel_add(F, F, F1);

  printf("initial error after recombining: %.10e\n\n", mpfr_get_d(F->rem, MPFR_RNDU));

  // find optimal degree
  while (mpfr_cmp(F->rem, max_err) <= 0)
    chebmodel_truncate(F, F->poly->degree - 1);
  long best_degree = F->poly->degree + 1;

  printf("optimal degree = %ld\n\n", best_degree);

  // clear variables
  chebmodel_clear(F); chebmodel_clear(F1); chebmodel_clear(F2);
  mpfi_clear(lambda); mpfi_clear(F1_1); mpfi_clear(F2_1);

  return best_degree;
}



// create gnuplot file to plot P with n equidistant points from -1 to 1
void double_chebpoly_plot(const double_chebpoly_t P, long n, FILE * filename)
{
  double step = (double) 2 / (n-1);
  long i;
  mpfr_t x, y;
  mpfr_init(x);
  mpfr_init(y);
  for (i = 0 ; i < n ; i++) {
    mpfr_set_d(x, -1 + i * step, MPFR_RNDN);
    double_chebpoly_evaluate_fr(y, P, x);
    fprintf(filename, "%f\t%f\n", mpfr_get_d(x, MPFR_RNDN), mpfr_get_d(y, MPFR_RNDN));
  }
}


// create gnuplot file to plot P with n equidistant points from -1 to 1
void mpfr_chebpoly_plot(const mpfr_chebpoly_t P, long n, FILE * filename)
{
  double step = (double) 2 / (n-1);
  long i;
  mpfr_t x, y;
  mpfr_init(x);
  mpfr_init(y);
  for (i = 0 ; i < n ; i++) {
    mpfr_set_d(x, -1 + i * step, MPFR_RNDN);
    mpfr_chebpoly_evaluate_fr(y, P, x);
    fprintf(filename, "%f\t%f\n", mpfr_get_d(x, MPFR_RNDN), mpfr_get_d(y, MPFR_RNDN));
  }
  mpfr_clear(x);
  mpfr_clear(y);
}





int main()
{

  mpfr_prec_t prec = 500;
  mpfr_set_default_prec(prec);

  mpfr_t eps;
  mpfr_init(eps);
  mpfr_set_d(eps, 0.001, MPFR_RNDN);

  mpfr_t max_err; mpfr_init(max_err);
  mpfr_set_si(max_err, 1, MPFR_RNDN);
  mpfr_mul_2si(max_err, max_err, -53, MPFR_RNDN);

/*
  long p = 100; long n = 200;
  blayer_solve_prec(eps, p, n, max_err);
*/


// obtain contracting op
/*
  long p = 30;

  long n, h2, d2;

  blayer_validate_d(&n, &h2, &d2, eps, p);

  printf("--------------\neps = %f, p = %ld, n = %ld, h2 = %ld, d2 = %ld\n", mpfr_get_d(eps, MPFR_RNDN), p, n, h2, d2);
*/

// plot sol (with double)

  long p = 50;
  long n = 1630;
  mpfr_chebpoly_t P;
  mpfr_chebpoly_init(P);
  blayer_approxsolve_fr(P, eps, p, n);
  FILE * file_plot = fopen("blayer_plot_false.dat", "w+");
  fprintf(file_plot, "# plot for boundary layer example with inaccurate approx\n# eps = %f, p = %ld, n = %ld\n", mpfr_get_d(eps, MPFR_RNDN), p, n);
  long N = 1000;
  mpfr_chebpoly_plot(P, N, file_plot);
  fprintf(stderr, "P: OK!\n");
  fclose(file_plot);
  mpfr_chebpoly_clear(P);
  mpfr_clear(eps);


// plot sol (with mpfr)
/*
  long p = 50;
  long n = 1000;
  mpfr_chebpoly_t P;
  mpfr_chebpoly_init(P);
  blayer_approxsolve_fr(P, eps, p, n);
  FILE * file_plot = fopen("blayer_plot_basis_2.dat", "w+");
  fprintf(file_plot, "# plot for boundary layer example\n# eps = 0.00001, u(-1) = 2, u'(-1) = 1\n");
  long N = 1000;
  mpfr_chebpoly_plot(P, N, file_plot);
  fprintf(stderr, "P: OK!\n");
  fclose(file_plot);
  mpfr_chebpoly_clear(P);
  mpfr_clear(eps);
*/





  return 0;
}
