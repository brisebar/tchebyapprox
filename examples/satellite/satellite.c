/*
Example for the TchebyApprox library: relative linearized Keplerian motion of a satellite
certified trajectories
*/

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include <gmp.h>
#include <mpfr.h>
#include <mpfi.h>

#include <chebmodel_intop_newton.h>


void chebmodel_evaluate1(mpfi_t y, const chebmodel_t P)
{
  long i;
  long n = P->poly->degree;
  mpfi_set_si(y, 0);

  for (i = 0 ; i <= n ; i++)
    mpfi_add(y, y, P->poly->coeffs + i);

  mpfr_add(&(y->right), &(y->right), P->rem, MPFR_RNDU);
  mpfr_sub(&(y->left), &(y->left), P->rem, MPFR_RNDD);
}



void Cos_Sin(chebmodel_struct Sol[3], const mpfr_t t0, const mpfr_t tf, const __mpfi_struct I[2], const mpfi_t omega, long N)
{
  chebmodel_lode_t L;
  chebmodel_lode_init(L);
  chebmodel_lode_set_order(L, 2);

  mpfi_t t;
  mpfi_init(t);
  mpfi_set_fr(t, tf);
  mpfi_sub_fr(t, t, t0);
  mpfi_mul_2si(t, t, -1);
  mpfi_mul(t, t, omega);
  mpfi_sqr(t, t);
 
  mpfi_chebpoly_set_coeff_fi(L->a[0].poly, 0, t);

  __mpfi_struct J[2];
  mpfi_init(J + 0);
  mpfi_set(J + 0, I + 0);
  mpfi_init(J + 1);
  mpfi_set_fr(t, tf);
  mpfi_sub_fr(t, t, t0);
  mpfi_mul_2si(t, t, -1);
  mpfi_mul(J + 1, t, I + 1);

  chebmodel_t g;
  chebmodel_init(g); // g = 0
  chebmodel_lode_intop_newton_solve(Sol, L, g, J, N);

  chebmodel_scalar_div_fi(Sol + 1, Sol + 1, t);
  mpfi_sqr(t, t);
  chebmodel_scalar_div_fi(Sol + 2, Sol + 2, t);

  // clear variables
  chebmodel_lode_clear(L);
  mpfi_clear(t);
  mpfi_clear(J + 0);
  mpfi_clear(J + 1);
  chebmodel_clear(g);
}


// I = (x(nu_0), x'(nu_0), z(nu_0), z'(nu_0))
// p = approximation degree for coefficient
// n = approximation degree
void Traj_solve(chebmodel_t Z, chebmodel_t X, chebmodel_t Z2, const mpfr_t nu_0, const mpfr_t nu_f, const mpfi_t e, const __mpfi_struct I[4], long p, long n)
{

  mpfi_t t;
  mpfi_init(t);

  // prepare to compute approx of cos on [nu_0 ; nu_f]
  __mpfi_struct I0[2];
  mpfi_init(I0 + 0);
  mpfi_init(I0 + 1);
  mpfi_set_fr(t, nu_0);
  mpfi_cos(I0 + 0, t);
  mpfi_sin(I0 + 1, t);
  mpfi_neg(I0 + 1, I0 + 1);

  mpfi_t omega;
  mpfi_init(omega);
  mpfi_set_si(omega, 1);

  chebmodel_struct F[3];
  chebmodel_init(F + 0);
  chebmodel_init(F + 1);
  chebmodel_init(F + 2);
  Cos_Sin(F, nu_0, nu_f, I0, omega, p);

  // compute in F[0] approx of ((nu_f-nu_0)/2)^2 (4-3/(1+e*cos(nu))) 
  chebmodel_scalar_mul_fi(F + 0, F + 0, e);
  mpfi_add_si(F[0].poly->coeffs + 0, F[0].poly->coeffs + 0, 1);
  chebmodel_inverse(F + 0, F + 0, p);
  chebmodel_scalar_mul_si(F + 0, F + 0, -3);
  mpfi_add_si(F[0].poly->coeffs + 0, F[0].poly->coeffs + 0, 4);
  mpfi_set_fr(t, nu_f);
  mpfi_sub_fr(t, t, nu_0);
  mpfi_mul_2si(t, t, -1);
  mpfi_sqr(t, t);
  chebmodel_scalar_mul_fi(F + 0, F + 0, t);

  // differential equation related to z
  chebmodel_lode_t L;
  chebmodel_lode_init(L);
  chebmodel_lode_set_order(L, 2);
  chebmodel_set(L->a + 0, F + 0);
  mpfr_chebpoly_lode_t L_fr;
  mpfr_chebpoly_lode_init(L_fr);
  chebmodel_lode_get_mpfr_chebpoly_lode(L_fr, L);

  // t = (nu_f-nu_0)/2
  mpfi_set_fr(t, nu_f);
  mpfi_sub_fr(t, t, nu_0);
  mpfi_mul_2si(t, t, -1);

  // right hand side: g = ((nu_f-nu_0)/2)^2*(4*z(nu_0)-2*x'(nu_0))
  chebmodel_t g;
  chebmodel_init(g);
  chebmodel_set_degree(g, 0);
  mpfi_mul_2si(g->poly->coeffs + 0, I + 2, 1);
  mpfi_sub(g->poly->coeffs + 0, g->poly->coeffs + 0, I + 1);
  mpfi_mul_2si(g->poly->coeffs + 0, g->poly->coeffs + 0, 1);
  mpfi_mul(g->poly->coeffs + 0, g->poly->coeffs + 0, t);
  mpfi_mul(g->poly->coeffs + 0, g->poly->coeffs + 0, t);
  mpfr_chebpoly_t g_fr;
  mpfr_chebpoly_init(g_fr);
  chebmodel_get_mpfr_chebpoly(g_fr, g);


  // initial conditions rescaled
  mpfi_set(I0 + 0, I + 2);
  mpfi_mul(I0 + 1, t, I + 3);
  __mpfr_struct I0_fr[2]; mpfr_inits(I0_fr + 0, I0_fr + 1, NULL);
  mpfi_mid(I0_fr + 0, I0 + 0); mpfi_mid(I0_fr + 1, I0 + 1);

  // approximate sol for z"
  mpfr_chebpoly_struct F_fr[3];
  mpfr_chebpoly_init(F_fr + 0); mpfr_chebpoly_init(F_fr + 1);
  mpfr_chebpoly_init(F_fr + 2);
  mpfr_chebpoly_lode_intop_approxsolve(F_fr, L_fr, g_fr, I0_fr, 2*n);
  mpfr_chebpoly_set_degree(F_fr + 2, n);

  // validation
  chebmodel_lode_intop_newton_validate_sol_d(F[2].rem, L, g, I0, F_fr + 2);
  mpfi_chebpoly_set_mpfr_chebpoly(F[2].poly, F_fr + 2);
  chebmodel_set(Z2, F + 2);
  chebmodel_antiderivative_1(F + 1, F + 2);
  mpfi_add(F[1].poly->coeffs + 0, F[1].poly->coeffs + 0, I0 + 1);
  chebmodel_antiderivative_1(F + 0, F + 1);
  mpfi_add(F[0].poly->coeffs + 0, F[0].poly->coeffs + 0, I0 + 0);

  // deduce X and Z
  chebmodel_set(Z, F + 0);
  chebmodel_zero(X);
  chebmodel_set_degree(X, 1);
  mpfi_mul_2si(X->poly->coeffs + 0, I + 2, 1);
  mpfi_sub(X->poly->coeffs + 0, X->poly->coeffs + 0, I + 1);
  mpfi_neg(X->poly->coeffs + 1, X->poly->coeffs + 0);
  mpfi_mul_fr(X->poly->coeffs + 0, X->poly->coeffs + 0, nu_0);
  mpfi_add(X->poly->coeffs + 0, X->poly->coeffs + 0, I + 0);
  chebmodel_antiderivative_1(F + 0, F + 0);
  chebmodel_scalar_mul_fi(F + 0, F + 0, t);
  chebmodel_scalar_mul_2si(F + 0, F + 0, 1);
  chebmodel_add(X, X, F + 0);

   // clear variables
  mpfi_clear(t);
  mpfi_clear(I0 + 0);
  mpfi_clear(I0 + 1);
  mpfi_clear(omega);
  chebmodel_clear(F + 0);
  chebmodel_clear(F + 1);
  chebmodel_clear(F + 2);
  chebmodel_lode_clear(L);
  chebmodel_clear(g);
  
  mpfr_chebpoly_lode_clear(L_fr);
  mpfr_chebpoly_clear(g_fr);
  mpfr_clears(I0_fr + 0, I0_fr + 1, NULL);
  mpfr_chebpoly_clear(F_fr + 0); mpfr_chebpoly_clear(F_fr + 1);
  mpfr_chebpoly_clear(F_fr + 2);


}




// plot P with n equidistant points from -1 to 1
void double_chebpoly_plot(const double_chebpoly_t P, long n, FILE * filename)
{
  double step = (double) 2 / (n-1);
  long i;
  mpfr_t x, y;
  mpfr_init(x);
  mpfr_init(y);
  for (i = 0 ; i < n ; i++) {
    mpfr_set_d(x, -1 + i * step, MPFR_RNDN);
    double_chebpoly_evaluate_fr(y, P, x);
    fprintf(filename, "%f\t%f\n", mpfr_get_d(x, MPFR_RNDN), mpfr_get_d(y, MPFR_RNDN));
  }
}


// create gnuplot file to plot everything 
void rdv_plot(mpfi_t e, mpfr_t nu_0, mpfr_t nu_f, __mpfi_struct I[4], long n, long p, long n_prec, long p_prec,
  chebmodel_t X, chebmodel_t Z, chebmodel_t Z2, chebmodel_t Xprec, chebmodel_t Zprec, chebmodel_t Z2prec, long N, FILE * filename)
{

  fprintf(filename, "#Plot RDV example with e = %f, nu_0 = %f rad, nu_f = %f rad\n#x(nu_0) = %f m, z(nu_0) = %f m, x'(nu_0) = %f m.rad^-1, z'(nu_0) = %f m.rad^-1\n#approximation parameters: n = %ld, p = %ld, n_prec = %ld, p_prec = %ld\n#t\tnu\tx\tz\tz2\tx_prec\tz_prec\tz2_prec\n#Che1-error:\t\t%f\t%f\t%f\t%f\t%f\t%f\n",
  mpfi_get_d(e), mpfr_get_d(nu_0, MPFR_RNDN), mpfr_get_d(nu_f, MPFR_RNDN),
  mpfi_get_d(I + 0), mpfi_get_d(I + 2), mpfi_get_d(I + 1), mpfi_get_d(I + 3),
  n, p, n_prec, p_prec,
  mpfr_get_d(X->rem, MPFR_RNDN), mpfr_get_d(Z->rem, MPFR_RNDN), mpfr_get_d(Z2->rem, MPFR_RNDN),
  mpfr_get_d(Xprec->rem, MPFR_RNDN), mpfr_get_d(Zprec->rem, MPFR_RNDN), mpfr_get_d(Z2prec->rem, MPFR_RNDN));

  double_chebpoly_t X_d, Z_d, Z2_d, Xprec_d, Zprec_d, Z2prec_d;
  double_chebpoly_init(X_d); double_chebpoly_init(Z_d); double_chebpoly_init(Z2_d);
  double_chebpoly_init(Xprec_d); double_chebpoly_init(Zprec_d); double_chebpoly_init(Z2prec_d);
  chebmodel_get_double_chebpoly(X_d, X);
  chebmodel_get_double_chebpoly(Z_d, Z);
  chebmodel_get_double_chebpoly(Z2_d, Z2);
  chebmodel_get_double_chebpoly(Xprec_d, Xprec);
  chebmodel_get_double_chebpoly(Zprec_d, Zprec);
  chebmodel_get_double_chebpoly(Z2prec_d, Z2prec);

  double step = (double) 2 / (N-1);
  long i;
  double t, nu, x, z, z2, x_prec, z_prec, z2_prec;
  for (i = 0 ; i < N ; i++) {
    t = -1 + i * step;
    nu = ((1-t) * mpfr_get_d(nu_0, MPFR_RNDN) + (1+t) * mpfr_get_d(nu_f, MPFR_RNDN)) / 2;
    double_chebpoly_evaluate_d(&x, X_d, &t);
    double_chebpoly_evaluate_d(&z, Z_d, &t);
    double_chebpoly_evaluate_d(&z2, Z2_d, &t);
    double_chebpoly_evaluate_d(&x_prec, Xprec_d, &t);
    double_chebpoly_evaluate_d(&z_prec, Zprec_d, &t);
    double_chebpoly_evaluate_d(&z2_prec, Z2prec_d, &t);
    fprintf(filename, "%f\t%f\t%f\t%f\t%f\t%f\t%f\t%f\n", t, nu, x, z, z2, x_prec, z_prec, z2_prec);
  }

  double_chebpoly_clear(X_d); double_chebpoly_clear(Z_d); double_chebpoly_clear(Z2_d);
  double_chebpoly_clear(Xprec_d); double_chebpoly_clear(Zprec_d); double_chebpoly_clear(Z2prec_d);

}



int main()
{

  mpfr_prec_t prec = 100;
  mpfr_set_default_prec(prec);

  mpfr_t nu_0, nu_f;
  mpfr_inits(nu_0, nu_f, NULL);
  mpfi_t e;
  mpfi_init(e);
  __mpfi_struct I[4]; 
  mpfi_init(I + 0); mpfi_init(I + 1); mpfi_init(I + 2); mpfi_init(I + 3);

  mpfr_set_si(nu_0, 0, MPFR_RNDN);
  mpfr_const_pi(nu_f, MPFR_RNDN);
  mpfr_mul_si(nu_f, nu_f, 6, MPFR_RNDN);
  mpfi_set_d(e, 0.5);
  mpfi_set_si(I + 0, -30000); // x(nu_0)
  mpfi_set_si(I + 1, 9000); // x'(nu_0)
  mpfi_set_si(I + 2, 5000); // z(nu_0)
  mpfi_set_si(I + 3, 4000); // z'(nu_0)

  long p = 300;
  long n = 18;

  chebmodel_t X, Z, Xprec, Zprec, Z2, Z2prec;
  chebmodel_init(X); chebmodel_init(Z);
  chebmodel_init(Xprec); chebmodel_init(Zprec);
  chebmodel_init(Z2); chebmodel_init(Z2prec);

  Traj_solve(Z, X, Z2, nu_0, nu_f, e, I, p, n);

  long p_prec = 300;
  long n_prec = 300;
  
  Traj_solve(Zprec, Xprec, Z2prec, nu_0, nu_f, e, I, p_prec, n_prec);

/*
  mpfi_chebpoly_t D;
  mpfi_chebpoly_init(D);
  mpfr_t bound;
  mpfr_init(bound);
  mpfi_chebpoly_sub(D, Z2->poly, Z2prec->poly);
  mpfi_chebpoly_1norm_ubound(bound, D);
  printf("<<<!! (Z2) %f, (Z2prec) %f, (Z2-Z2prec) %f >>>\n", mpfr_get_d(Y->rem, MPFR_RNDN), mpfr_get_d(Yprec->rem, MPFR_RNDN), mpfr_get_d(bound, MPFR_RNDN));

  double_chebpoly_t X_d, Xprec_d, Z_d, Zprec_d, 
*/

  long N = 1000;
  FILE * file_plot = fopen("rdv_plot.dat", "w+");
  rdv_plot(e, nu_0, nu_f, I, n, p, n_prec, p_prec, X, Z, Z2, Xprec, Zprec, Z2prec, N, file_plot);
  fclose(file_plot);


/*
  double_chebpoly_t Z_d, X_d, Z_d_prec;
  double_chebpoly_init(Z_d);
  double_chebpoly_init(X_d);
  double_chebpoly_init(Z_d_prec);
  chebmodel_get_double_chebpoly(X_d, X);
  chebmodel_get_double_chebpoly(Z_d, Z);
  chebmodel_get_double_chebpoly(Z_d_prec, Zprec);
  FILE * file_plot_X = fopen("rdv_plot_x.dat", "w+");
  fprintf(file_plot_X, "#RDV with e = %f, nu_0 = %f rad, nu_f = %f rad\n#x(nu_0) = %f m.rad^-1, z(nu_0) = %f m.rad^-1, x'(nu_0) = %f m.rad^-1, z'(nu_0) = %f m.rad^-1\n#plot x(nu) with approximation degree %ld\n#Che1-error = %f\n", mpfi_get_d(e), mpfr_get_d(nu_0, MPFR_RNDN), mpfr_get_d(nu_f, MPFR_RNDN), mpfi_get_d(I + 0), mpfi_get_d(I + 2), mpfi_get_d(I + 1), mpfi_get_d(I + 3), n, mpfr_get_d(X->rem, MPFR_RNDN));
  double_chebpoly_plot(X_d, 1000, file_plot_X);
  fclose(file_plot_X);
  FILE * file_plot_Z = fopen("rdv_plot_z.dat", "w+");
  fprintf(file_plot_Z, "#RDV with e = %f, nu_0 = %f rad, nu_f = %f rad\n#x(nu_0) = %f m.rad^-1, z(nu_0) = %f m.rad^-1, x'(nu_0) = %f m.rad^-1, z'(nu_0) = %f m.rad^-1\n#plot z(nu) with approximation degree %ld\n#Che1-error = %f\n", mpfi_get_d(e), mpfr_get_d(nu_0, MPFR_RNDN), mpfr_get_d(nu_f, MPFR_RNDN), mpfi_get_d(I + 0), mpfi_get_d(I + 2), mpfi_get_d(I + 1), mpfi_get_d(I + 3), n, mpfr_get_d(Z->rem, MPFR_RNDN));
  double_chebpoly_plot(Z_d, 1000, file_plot_Z);
  fclose(file_plot_Z);

  FILE * file_plot_Zprec = fopen("rdv_plot_zprec.dat", "w+");
  fprintf(file_plot_Zprec, "#RDV with e = %f, nu_0 = %f rad, nu_f = %f rad\n#x(nu_0) = %f m.rad^-1, z(nu_0) = %f m.rad^-1, x'(nu_0) = %f m.rad^-1, z'(nu_0) = %f m.rad^-1\n#plot z(nu) with approximation degree %ld\n#Che1-error = %f\n", mpfi_get_d(e), mpfr_get_d(nu_0, MPFR_RNDN), mpfr_get_d(nu_f, MPFR_RNDN), mpfi_get_d(I + 0), mpfi_get_d(I + 2), mpfi_get_d(I + 1), mpfi_get_d(I + 3), n_prec, mpfr_get_d(Zprec->rem, MPFR_RNDN));
  double_chebpoly_plot(Z_d_prec, 1000, file_plot_Zprec);
  fclose(file_plot_Zprec);

*/



  return 0;

}



