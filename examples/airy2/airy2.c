/*
Example for the TchebyApprox library: Airy function
evolution of validation parameters and certified solutions
*/

#include <stdio.h>
#include <stdlib.h>

#include <gmp.h>
#include <mpfr.h>
#include <mpfi.h>

#include "mpfi_chebpoly_vec_intop_newton.h"




int main()
{
  mpfr_prec_t prec = 500;
  mpfr_set_default_prec(prec);
  
  mpfi_t a, temp;
  mpfi_init(a); mpfi_init(temp);
  mpfi_set_si(a, 10);


  // integral operator
  mpfi_chebpoly_vec_intop_t K;
  mpfi_chebpoly_vec_intop_init(K);
  mpfi_chebpoly_vec_intop_set_dim(K, 2);
  mpfi_chebpoly_intop_set_order(K->intop[0] + 0, 0);
  mpfi_chebpoly_intop_set_order(K->intop[0] + 1, 1);
  mpfi_chebpoly_set_degree(K->intop[0][1].alpha + 0, 0);
  mpfi_mul_2si(temp, a, -1);
  mpfi_neg(K->intop[0][1].alpha[0].coeffs + 0, temp);
  mpfi_chebpoly_intop_set_order(K->intop[1] + 0, 2);
  mpfi_chebpoly_set_degree(K->intop[1][0].alpha + 0, 0);
  mpfi_chebpoly_set_degree(K->intop[1][0].alpha + 1, 0);
  mpfi_sqr(temp, a); mpfi_mul_2si(temp, temp, -2);
  mpfi_set(K->intop[1][0].alpha[0].coeffs + 0, temp);
  mpfi_set(K->intop[1][0].alpha[1].coeffs + 0, temp);
  mpfi_chebpoly_intop_set_order(K->intop[1] + 1, 0);
  // mpfr integral operator
  mpfr_chebpoly_vec_intop_t Kfr;
  mpfr_chebpoly_vec_intop_init(Kfr);
  mpfi_chebpoly_vec_intop_get_mpfr_chebpoly_vec_intop(Kfr, K);

  // initial conditions
  __mpfi_struct IV[2];
  mpfi_init(IV + 0); mpfi_init(IV + 1);
  mpfi_set_si(temp, 2); mpfi_div_si(temp, temp, 3);
  mpfr_ui_pow(&(IV[0].left), 3, &(temp->left), MPFR_RNDD);
  mpfr_ui_pow(&(IV[0].right), 3, &(temp->right), MPFR_RNDU);
  mpfi_set_si(temp, 2); mpfi_div_si(temp, temp, 3);
  mpfr_swap(&(temp->left), &(temp->right));
  mpfr_gamma(&(temp->left), &(temp->left), MPFR_RNDD);
  mpfr_gamma(&(temp->right), &(temp->right), MPFR_RNDU);
  mpfi_mul(IV + 0, IV + 0, temp);
  mpfi_si_div(IV + 0, 1, IV + 0); // IV[0] = 1/(3^(2/3)*Gamma(2/3))
  mpfi_set_si(temp, 1); mpfi_div_si(temp, temp, 3);
  mpfr_ui_pow(&(IV[1].left), 3, &(temp->left), MPFR_RNDD);
  mpfr_ui_pow(&(IV[1].right), 3, &(temp->right), MPFR_RNDU);
  mpfi_set_si(temp, 1); mpfi_div_si(temp, temp, 3);
  mpfr_swap(&(temp->left), &(temp->right));
  mpfr_gamma(&(temp->left), &(temp->left), MPFR_RNDD);
  mpfr_gamma(&(temp->right), &(temp->right), MPFR_RNDU);
  mpfi_mul(IV + 1, IV + 1, temp);
  mpfi_si_div(IV + 1, -1, IV + 1); // IV[1] = -1/(3^(1/3)*Gamma(1/3))
  // mpfr initial conditions
  __mpfr_struct IVfr[2];
  mpfr_inits(IVfr + 0, IVfr + 1, NULL);
  mpfi_mid(IVfr + 0, IV + 0); mpfi_mid(IVfr + 1, IV + 1);

  // right hand side
  mpfi_chebpoly_vec_t G;
  mpfi_chebpoly_vec_init(G);
  mpfi_chebpoly_vec_set_dim(G, 2);
  mpfi_chebpoly_set_degree(G->poly + 0, 0);
  mpfi_set(G->poly[0].coeffs + 0, IV + 0);
  mpfi_chebpoly_set_degree(G->poly + 1, 0);
  mpfi_set(G->poly[1].coeffs + 0, IV + 1);
  // mpfr right hand side
  mpfr_chebpoly_vec_t Gfr;
  mpfr_chebpoly_vec_init(Gfr);
  mpfi_chebpoly_vec_get_mpfr_chebpoly_vec(Gfr, G);


  // approximate solution
  long N = 14;
  mpfr_chebpoly_vec_t Y;
  mpfr_chebpoly_vec_init(Y);
  mpfr_chebpoly_vec_intop_approxsolve(Y, Kfr, Gfr, N);
  mpfr_t t, y;
  mpfr_init(t); mpfr_init(y);

  // plot the solution
  long nb_points = 1000;
  long i, j, k, l; 
  FILE * plotfile = fopen("airy2-plot-a10-N14.dat", "w+");
  fprintf(plotfile, "# a = %.10e\tN = %ld\n# t\tx\tAi(x)\tAi'(x)\n", mpfi_get_d(a), N);
  for (i = 0 ; i <= nb_points ; i++) {
    mpfr_set_si(t, 2*i, MPFR_RNDN);
    mpfr_div_si(t, t, nb_points, MPFR_RNDN);
    mpfr_sub_si(t, t, 1, MPFR_RNDN);
    fprintf(plotfile, "%.10e\t%.10e\t", mpfr_get_d(t, MPFR_RNDN), -(1+mpfr_get_d(t, MPFR_RNDN))/2*mpfi_get_d(a));
    mpfr_chebpoly_evaluate_fr(y, Y->poly + 0, t);
    fprintf(plotfile, "%.10e\t", mpfr_get_d(y, MPFR_RNDN));
    mpfr_chebpoly_evaluate_fr(y, Y->poly + 1, t);
    fprintf(plotfile, "%.10e\n", mpfr_get_d(y, MPFR_RNDN));
  }

  fclose(plotfile);

  // print almost-banded matrices of K (!! signs)
  mpfr_bandmatrix_struct ** M = malloc(2 * sizeof(mpfr_bandmatrix_struct *));
  M[0] = malloc(2 * sizeof(mpfr_bandmatrix_t));
  M[1] = malloc(2 * sizeof(mpfr_bandmatrix_t));
  for (i = 0 ; i < 2 ; i++)
    for (j = 0 ; j < 2 ; j++) {
      mpfr_bandmatrix_init(M[i] + j);
      mpfr_chebpoly_intop_get_bandmatrix(M[i] + j, Kfr->intop[i] + j, N);
    }
  mpfr_bandmatrix_t Mbig; mpfr_bandmatrix_init(Mbig);
  mpfr_bandmatrix_merge(Mbig, M, 2);

  long h, d;
  FILE * KijHfile[2][2];
  FILE * KijDfile[2][2];
  KijHfile[0][0] = fopen("airy-K00-H-a10-N14.dat", "w+");
  KijDfile[0][0] = fopen("airy-K00-D-a10-N14.dat", "w+");
  KijHfile[0][1] = fopen("airy-K01-H-a10-N14.dat", "w+");
  KijDfile[0][1] = fopen("airy-K01-D-a10-N14.dat", "w+");
  KijHfile[1][0] = fopen("airy-K10-H-a10-N14.dat", "w+");
  KijDfile[1][0] = fopen("airy-K10-D-a10-N14.dat", "w+");
  KijHfile[1][1] = fopen("airy-K11-H-a10-N14.dat", "w+");
  KijDfile[1][1] = fopen("airy-K11-D-a10-N14.dat", "w+");
  FILE * KHfile = fopen("airy-K-H-a10-N14.dat", "w+");
  FILE * KDfile = fopen("airy-K-D-a10-N14.dat", "w+");

  for (i = 0 ; i < 2 ; i++)
    for (j = 0 ; j < 2 ; j++) {
      
      h = M[i][j].Hwidth;
      d = M[i][j].Dwidth;
      fprintf(KijHfile[i][j], "# a = %.10e\tN = %ld\tHwidth = %ld\n", mpfi_get_d(a), N, h);
      fprintf(KijDfile[i][j], "# a = %.10e\tN = %ld\tDwidth = %ld\n", mpfi_get_d(a), N, d);
      for (k = 0 ; k < h ; k++) {
        for (l = 0 ; l <= N ; l++)
          fprintf(KijHfile[i][j], "%.10e\t", -mpfr_get_d(M[i][j].H[k] + l, MPFR_RNDN));
        fprintf(KijHfile[i][j], "\n");
      }
      for (k = 0 ; k <= N ; k++) {
        for (l = 0 ; l <= 2*d ; l++)
          fprintf(KijDfile[i][j], "%.10e\t", -mpfr_get_d(M[i][j].D[k] + l, MPFR_RNDN));
        fprintf(KijDfile[i][j], "\n");
      }
      fclose(KijHfile[i][j]);
      fclose(KijDfile[i][j]);

    }

  h = Mbig->Hwidth;
  d = Mbig->Dwidth;
  fprintf(KHfile, "# a = %.10e\tN = %ld\tHwidth = %ld\n", mpfi_get_d(a), N, h);
  fprintf(KDfile, "# a = %.10e\tN = %ld\tDwidth = %ld\n", mpfi_get_d(a), N, d);
  for (k = 0 ; k < h ; k++) {
    for (l = 0 ; l <= 2*N+1 ; l++)
      fprintf(KHfile, "%.10e\t", -mpfr_get_d(Mbig->H[k] + l, MPFR_RNDN));
    fprintf(KHfile, "\n");
  }
  for (k = 0 ; k <= 2*N+1 ; k++) {
    for (l = 0 ; l <= 2*d ; l++)
      fprintf(KDfile, "%.10e\t", -mpfr_get_d(Mbig->D[k] + l, MPFR_RNDN));
    fprintf(KDfile, "\n");
  }
  fclose(KHfile);
  fclose(KDfile);


  // print obtained polynomials
  printf("Y_1 = ");
  for (i = 0 ; i <= N ; i++) {
    double coeff = mpfr_get_d(Y->poly[0].coeffs + i, MPFR_RNDN);
    if (coeff >= 0)
      printf("+%.3f T_%ld ", coeff, i);
    else
      printf("%.3f T_%ld ", coeff, i);
  }
  printf("\n");
  printf("Y_2 = ");
  for (i = 0 ; i <= N ; i++) {
    double coeff = mpfr_get_d(Y->poly[1].coeffs + i, MPFR_RNDN);
    if (coeff >= 0)
      printf("+%.3f T_%ld ", coeff, i);
    else
      printf("%.3f T_%ld ", coeff, i);
  }
  printf("\n");


  return 0;
}
