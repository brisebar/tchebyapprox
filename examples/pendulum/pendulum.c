/*
Example for the TchebyApprox library: parametric pendulum with constant variation of the cord's length
evolution of validation parameters and certified solutions
*/


#include <stdio.h>
#include <stdlib.h>

#include <gmp.h>
#include <mpfr.h>
#include <mpfi.h>

#include "chebmodel_intop_newton.h"


// Chebmodel F for 1/(1+zeta*t) of degree d
void pendulum_fractapprox(chebmodel_t F, const mpfr_t zeta, long p)
{
  chebmodel_zero(F);
  chebmodel_set_degree(F, 1);
  mpfi_set_si(F->poly->coeffs + 0, 1);
  mpfi_set_fr(F->poly->coeffs + 1, zeta);
  chebmodel_inverse(F, F, p);
}

// approximation (double_chebpoly) for 1/(1+zeta*t) of degree d
void pendulum_fractapprox_d(double_chebpoly_t F, const mpfr_t zeta, long p)
{
  double_chebpoly_zero(F);
  double_chebpoly_set_degree(F, 1);
  double_set_si(F->coeffs + 0, 1, MPFR_RNDN);
  double_set_fr(F->coeffs + 1, zeta, MPFR_RNDN);
  double_chebpoly_inverse_approx(F, F, p);
}



// obtaining validation parameters: n (truncation order), h2 and d2 (parameters of the almost-banded structure of the approximate inverse)
void pendulum_validate_d(long *n, long *h2, long *d2, const mpfr_t l0, const mpfr_t zeta, long p)
{

  mpfi_t temp;
  mpfi_init(temp);

  // approx F of 1/(1+zeta*t)
  chebmodel_t F;
  chebmodel_init(F);
  pendulum_fractapprox(F, zeta, p);

  // parametric pendulum equation
  chebmodel_lode_t L;
  chebmodel_lode_init(L);
  chebmodel_lode_set_order(L, 2);
  mpfi_set_d(temp, 9.81);
  mpfi_div_fr(temp, temp, l0);
  chebmodel_scalar_mul_fi(L->a + 0, F, temp);
  mpfi_set_fr(temp, zeta);
  mpfi_mul_2si(temp, temp, 1);
  chebmodel_scalar_mul_fi(L->a + 1, F, temp);

  // integral operator
  chebmodel_intop_t K;
  chebmodel_intop_init(K);
  chebmodel_intop_set_lode(K, L);
  long h = chebmodel_intop_Hwidth(K);
  long d = chebmodel_intop_Dwidth(K);  
  double_bandmatrix_t M_K_inv;
  double_bandmatrix_init(M_K_inv);
  double_t bound;
  double_init(bound);

  // get a contracting operator
  chebmodel_intop_newton_contract_d(bound, M_K_inv, K, h+d);
  *n = M_K_inv->dim - 1;
  *h2 = M_K_inv->Hwidth;
  *d2 = M_K_inv->Dwidth;

  // clear variables
  chebmodel_clear(F);
  mpfi_clear(temp);
  chebmodel_lode_clear(L);
  chebmodel_intop_clear(K);
  double_bandmatrix_clear(M_K_inv);
  double_clear(bound);

}


// numerical solving, without validation
void pendulum_approxsolve_d(double_chebpoly_t P, const mpfr_t l0, const mpfr_t zeta, long p, long n)
{
  double_t temp;
  double_init(temp);

  // approx F of 1/(1+zeta*t)
  double_chebpoly_t F;
  double_chebpoly_init(F);
  pendulum_fractapprox_d(F, zeta, p);

  // parametric pendulum equation
  double_chebpoly_lode_t L;
  double_chebpoly_lode_init(L);
  double_chebpoly_lode_set_order(L, 2);
  *temp = 9.81 / mpfr_get_d(l0, MPFR_RNDN);
  double_chebpoly_scalar_mul_d(L->a + 0, F, temp);
  *temp = 2 * mpfr_get_d(zeta, MPFR_RNDN);
  double_chebpoly_scalar_mul_d(L->a + 1, F, temp);

  // approx solve
  double_chebpoly_struct Sols[3];
  double_chebpoly_init(Sols + 0);
  double_chebpoly_init(Sols + 1);
  double_chebpoly_init(Sols + 2);
  double_chebpoly_t g;
  double_chebpoly_init(g); // g = 0
  double I[2];
  I[0] = 1; I[1] = 0;
  double_chebpoly_lode_intop_approxsolve(Sols, L, g, I, n);
  double_chebpoly_set(P, Sols + 0);

  // clear variables
  double_clear(temp);
  double_chebpoly_clear(F);
  double_chebpoly_lode_clear(L);
  double_chebpoly_clear(Sols + 0);
  double_chebpoly_clear(Sols + 1);
  double_chebpoly_clear(Sols + 2);
  double_chebpoly_clear(g);

}


// validated solution
void pendulum_solve(chebmodel_t P, const mpfr_t l0, const mpfr_t zeta, long p, long n)
{
  mpfi_t temp;
  mpfi_init(temp);

  // approx F of 1/(1+zeta*t)
  chebmodel_t F;
  chebmodel_init(F);
  pendulum_fractapprox(F, zeta, p);

  // parametric pendulum equation
  chebmodel_lode_t L;
  chebmodel_lode_init(L);
  chebmodel_lode_set_order(L, 2);
  mpfi_set_d(temp, 9.81);
  mpfi_div_fr(temp, temp, l0);
  chebmodel_scalar_mul_fi(L->a + 0, F, temp);
  mpfi_set_fr(temp, zeta);
  mpfi_mul_2si(temp, temp, 1);
  chebmodel_scalar_mul_fi(L->a + 1, F, temp);

  // solve
  chebmodel_struct Sols[3];
  chebmodel_init(Sols + 0);
  chebmodel_init(Sols + 1);
  chebmodel_init(Sols + 2);
  chebmodel_t g;
  chebmodel_init(g); // g = 0
  __mpfi_struct I[2]; mpfi_init(I + 0); mpfi_init(I + 1);
  mpfi_set_si(I + 0, 1); mpfi_set_si(I + 1, 0);
  chebmodel_lode_intop_newton_solve(Sols, L, g, I, n);
  chebmodel_set(P, Sols + 0);

  // clear variables
  mpfi_clear(temp); mpfi_clear(I + 0); mpfi_clear(I + 1);
  chebmodel_clear(F);
  chebmodel_lode_clear(L);
  chebmodel_clear(Sols + 0);
  chebmodel_clear(Sols + 1);
  chebmodel_clear(Sols + 2);
  chebmodel_clear(g);

}






// create gnuplot file to plot P with n equidistant points from -1 to 1
void double_chebpoly_plot(const double_chebpoly_t P, long n, FILE * filename)
{
  double step = (double) 2 / (n-1);
  long i;
  mpfr_t x, y;
  mpfr_init(x);
  mpfr_init(y);
  for (i = 0 ; i < n ; i++) {
    mpfr_set_d(x, -1 + i * step, MPFR_RNDN);
    double_chebpoly_evaluate_fr(y, P, x);
    fprintf(filename, "%f\t%f\n", mpfr_get_d(x, MPFR_RNDN), mpfr_get_d(y, MPFR_RNDN));
  }
}


int main()
{

  mpfr_prec_t prec = 100;
  mpfr_set_default_prec(prec);

  mpfr_t zeta, l0;
  mpfr_init(zeta);
  mpfr_init(l0);
 
 
 // plot approx error of 1/(1+zeta*t)
/* 
  double min_err = 10e-50;

  double zeta_val[5] = {0.1, 0.5, 0.75, 0.9, 0.99};
  long p_val[15] = {3, 4, 5, 6, 8, 10, 15, 20, 25, 30, 40, 50, 60, 80, 100};

  chebmodel_t F;
  chebmodel_init(F);

  long i, j, p;

  FILE *fp = fopen("pendulum_fractapprox.dat", "w+");
  fprintf(fp, "# approx error of 1/(1+zeta*t), in function of zeta and degree p \n");
  fprintf(fp, "# zeta =\t0.1\t0.5\t0.75\t0.9\t0.99\n# p =\n");

  for (i = 0 ; i < 15 ; i++) { 
    p = p_val[i];
    fprintf(fp, "%ld", p);
    for (j = 0 ; j < 5 ; j++) {
      mpfr_set_d(zeta, zeta_val[j], MPFR_RNDN);
      pendulum_fractapprox(F, zeta, p);
      fprintf(fp, "\t");
      if (mpfr_cmp_d(F->rem, min_err) >= 0 && mpfr_cmp_si(F->rem, 1) < 0)
	mpfr_out_str(fp, 10, 5, F->rem, MPFR_RNDU);
      else
	fprintf(fp, "...");
    }
    fprintf(fp, "\n");
  }

  fclose(fp);
  chebmodel_clear(F);
*/

// contract (find parameters for validation)
/*
  long n, h2, d2, p;
  p = 500;
  mpfr_set_d(zeta, -0.998, MPFR_RNDN);
  mpfr_set_d(l0, 1, MPFR_RNDN);

  pendulum_validate_d(&n, &h2, &d2, l0, zeta, p);

  printf("-----------\nn = %ld, h' = %ld, d' = %ld\n", n, h2, d2);
*/


// plot solution
  long p = 65;
  long n = 65;
  long N = 1000;
  mpfr_set_d(l0, 0.1, MPFR_RNDN);
  mpfr_set_d(zeta, -0.9, MPFR_RNDN);
  double_chebpoly_t P;
  double_chebpoly_init(P);
  chebmodel_t F;
  chebmodel_init(F);
  
  pendulum_solve(F, l0, zeta, p, n);
  chebmodel_get_double_chebpoly(P, F);

  FILE * plot_file = fopen("pendulum_plot_zetaneg.dat", "w+");
  fprintf(plot_file, "#Plot parametric pendulum example\n#l0 = %f, zeta = %f, certified error = %f\n", mpfr_get_d(l0, MPFR_RNDN), mpfr_get_d(zeta, MPFR_RNDN), mpfr_get_d(F->rem, MPFR_RNDU));
  double_chebpoly_plot(P, N, plot_file);
  fclose(plot_file);
 
  double_chebpoly_clear(P);
  chebmodel_clear(F);





  mpfr_clear(zeta);
  mpfr_clear(l0);

  return 0;
}




























