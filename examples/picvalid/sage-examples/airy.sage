from ore_algebra import OreAlgebra
Pols.<z> = PolynomialRing(QQ)
DiffOps.<Dz> = OreAlgebra(Pols)

Airy = Dz^2-z

def airy_ini(bitprec):
  ARBT = RealBallField(precision=bitprec)
  Ai0 = 1/(3^(2/3)*gamma(2/3))
  v0 = ARBT(Ai0)
  AiD0 = -1/(3^(1/3)*gamma(1/3))
  v1 = ARBT(AiD0)
  return [v0, v1]

#v = airy_ini(1e-16)
#Airy.numerical_solution(ini=v, path=[0, 5], eps=1e-16)
#timeit('Airy.numerical_solution(ini=v, path=[0, 5], eps=1e-16)', preparse=True, number=100)
