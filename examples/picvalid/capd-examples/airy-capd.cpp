
#include "capd/mpcapdlib.h"
#include <time.h>
using namespace capd;
using namespace std;
using namespace multiPrec;


//inline MpReal::MpReal(unsigned long int i, RoundingMode rnd, PrecisionType prec) {

// inline multiPrec::MpReal::MpReal(long long int i, multiPrec::MpReal::RoundingMode rnd, multiPrec::MpReal::PrecisionType prec)
// {
//   return MpReal((long) i, rnd, prec);
// }

// inline MpInterval operator*(long long int c, MpInterval i) {
//   return MpInterval((long) c) * i;
// }
//
// inline MpInterval operator*(double c, MpInterval i) {
//   return MpInterval(c) * i;
// }
//
// inline MpInterval operator*(long c, MpInterval i) {
//   return MpInterval(c) * i;
// }
//
// inline MpInterval operator*(int c, MpInterval i) {
//   return MpInterval(c) * i;
// }


int main()
{
  cout.precision(12);
  MpFloat::setDefaultPrecision(1024);
  long N = 65;
  double prec = 1e-125;
  MpInterval T(15);
  try{

    // Airy initial conditions
    MpIVector c(3);
    c[0] = MpInterval("0.");
    mpfr_t vlo, vhi;
    mpfr_init(vlo);
    mpfr_init(vhi);
    mpfr_set_si(vlo, 2, MPFR_RNDU);
    mpfr_div_si(vlo, vlo, 3, MPFR_RNDU);
    mpfr_gamma(vlo, vlo, MPFR_RNDD);
    mpfr_set_si(vhi, 2, MPFR_RNDD);
    mpfr_div_si(vhi, vhi, 3, MPFR_RNDD);
    mpfr_gamma(vhi, vhi, MPFR_RNDU);
    c[1] = MpInterval(MpReal(vlo), MpReal(vhi)); // Γ(2/3)
    c[1] *= power(MpInterval("3"), MpInterval("2") / MpInterval("3"));
    c[1] = MpInterval("1") / c[1]; // c[1] = 1/(3^(2/3) * Γ(2/3)) = Ai(0)
    mpfr_set_si(vlo, 1, MPFR_RNDU);
    mpfr_div_si(vlo, vlo, 3, MPFR_RNDU);
    mpfr_gamma(vlo, vlo, MPFR_RNDD);
    mpfr_set_si(vhi, 1, MPFR_RNDD);
    mpfr_div_si(vhi, vhi, 3, MPFR_RNDD);
    mpfr_gamma(vhi, vhi, MPFR_RNDU);
    c[2] = MpInterval(MpReal(vlo), MpReal(vhi)); // Γ(1/3)
    c[2] *= power(MpInterval("3"), MpInterval("1") / MpInterval("3"));
    c[2] = -MpInterval("1") / c[2]; // c[2] = -1/(3^(1/3) * Γ(1/3)) = Ai'(0)
    // define a doubleton representation of the interval vector c
    MpC0HORect2Set s(c);
    // we integrate the set s over the time T


    cout << "\ninitial set: " << c;
    cout << "\ndiam(initial set): " << diam(c) << endl;

    // compute + timing
    // MpIMap vectorField("var:t,x,y;fun:1,y,t*x;");
    MpIMap vectorField("var:t,x,y;fun:1,y,(t+1/1000000*(t^2))*x;");
    MpIOdeSolver solver(vectorField,N);
    solver.setAbsoluteTolerance(prec);
    MpITimeMap timeMap(solver);
    clock_t start, end;
    start = clock();
    MpIVector result = timeMap(T,s);
    end = clock();

    cout << "\n\nafter time=" << T << " the image is: " << result;
    cout << "\ndiam(image): " << diam(result) << endl << endl;
    cout << "\nelapsed time = " << ((double) end - start) / CLOCKS_PER_SEC << endl;

  }catch(exception& e)
  {
    cout << "\n\nException caught!\n" << e.what() << endl << endl;
  }
} // END
