

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include <gmp.h>
#include <mpfr.h>
#include <mpfi.h>

#include "chebmodel_newtonpicard.h"



// read ODE coefficients from file fp
// !! the input data is supposed to be given in monomial basis
// (therefore we need to convert)
int mpfi_chebpoly_lode_fscanf(mpfi_chebpoly_lode_t L, FILE * fp)
{
  long r, di, i, j;
  char strcoeff[1000];
  mpfi_poly_t p;
  mpfi_poly_init(p);
  mpfi_t leadcoeff;
  mpfi_init(leadcoeff);

  // read order r
  fscanf(fp, "%ld", &r);
  mpfi_chebpoly_lode_set_order(L, r);

  // read polynomial coefficients
  for (i = 0 ; i < r ; i++) {

    // read degree of i-th polynomial coefficient
    fscanf(fp, "%ld", &di);
    mpfi_poly_set_degree(p, di);

    // read its coefficients
    for (j = 0 ; j <= di ; j++) {
      fscanf(fp, "%s", strcoeff);
      mpfi_set_str(p->coeffs + j, strcoeff, 10);
    }

    // convert to the Chebyshev basis
    mpfi_chebpoly_set_mpfi_poly(L->a + i, p);
  }

  // read head coefficient, check if constant, and normalize
  fscanf(fp, "%ld", &di);
  if (di != 0) {
    fprintf(stderr, "singular operator. Abort.\n");
    return -1;
  }
  fscanf(fp, "%s", strcoeff);
  mpfi_set_str(leadcoeff, strcoeff, 10);
  for (i = 0 ; i < r ; i++)
    mpfi_chebpoly_scalar_div_fi(L->a + i, L->a + i, leadcoeff);

  // clear temp variables
  mpfi_poly_clear(p);
  mpfi_clear(leadcoeff);

  return 0;
}

// read ODE coefficients and initial conditions from file fp
// v must be allocated of size r * sizeof(mpfi_t)
// !! the input data is supposed to be given in monomial basis
// (therefore we need to convert)
int mpfi_chebpoly_lode_initvals_fscanf(mpfi_chebpoly_lode_t L, mpfi_ptr v, FILE * fp)
{
  // first read ODE
  if (mpfi_chebpoly_lode_fscanf(L, fp))
    return -1;
  long r = L->order;

  // read initial conditions
  long i;
  char strcoeff[1000];
  for (i = 0 ; i < r ; i++) {
    fscanf(fp, "%s", strcoeff);
    mpfi_set_str(v + i, strcoeff, 10);
  }

  return 0;
}



int main()
{

  srand(time(0));

  mpfr_prec_t prec = 1024;
  mpfr_set_default_prec(prec);

  long i, j;
  mpfi_chebpoly_lode_t L;
  mpfi_chebpoly_lode_init(L);
  mpfi_ptr v = malloc(100 * sizeof(mpfi_t));
  for (i = 0 ; i < 100 ; i++)
    mpfi_init(v + i);

  FILE * fp = fopen("long-diffeq-data/long-trigo-diffeq.dat", "r");
  if (mpfi_chebpoly_lode_initvals_fscanf(L, v, fp))
    return -1;
  fclose(fp);
  long r = L->order;

  // print the Che-1 norm of L
  mpfr_t Lnorm, Lnorm_aux;
  mpfr_init(Lnorm);
  mpfr_init(Lnorm_aux);
  mpfr_set_si(Lnorm, 0, MPFR_RNDN);
  for (i = 0 ; i < r ; i++) {
    mpfi_chebpoly_1norm_ubound(Lnorm_aux, L->a + i);
    if (mpfr_cmp(Lnorm_aux, Lnorm) > 0)
      mpfr_set(Lnorm, Lnorm_aux, MPFR_RNDU);
  }
  printf("L norm = ");
  mpfr_out_str(stdout, 10, 10, Lnorm, MPFR_RNDU);
  printf("\n\n");

  //return 0;


  // resolvent kernel approximation = phi and psi bases, of chosen degree N
  long N = 10000;
  mpfr_chebpoly_lode_t Lfr;
  mpfr_chebpoly_lode_init(Lfr);
  mpfi_chebpoly_lode_get_mpfr_chebpoly_lode(Lfr, L);
  mpfr_chebpoly_ptr phi = malloc(r * sizeof(mpfr_chebpoly_t));
  mpfr_chebpoly_ptr psi = malloc(r * sizeof(mpfr_chebpoly_t));
  for (i = 0 ; i < r ; i++) {
    mpfr_chebpoly_init(phi + i);
    mpfr_chebpoly_init(psi + i);
  }
  //mpfr_chebpoly_resolventkernel(phi, psi, Lfr->a, r, N);

  // print resolvent kernel
  mpfr_t c;
  mpfr_init(c);
  // FILE * fpout = fopen("long-trigo-diffeq-out.dat", "w+");
  // fprintf(fpout, "%ld\t%ld\n", r, N);
  // for (j = 0 ; j <= N ; j++) {
  //   for (i = 0 ; i < r ; i++) {
  //     mpfr_chebpoly_get_coeff(c, phi + i, j);
  //     mpfr_out_str(fpout, 10, 10, c, MPFR_RNDN);
  //     fprintf(fpout, "\t");
  //   }
  //   for (i = 0 ; i < r ; i++) {
  //     mpfr_chebpoly_get_coeff(c, psi + i, j);
  //     mpfr_out_str(fpout, 10, 10, c, MPFR_RNDN);
  //     fprintf(fpout, "\t");
  //   }
  //   fprintf(fpout, "\n");
  // }
  // fclose(fpout);


  // test
  // for (i = 0 ; i < r ; i++)
  //   mpfi_set_si(v + i, i==0);

  // integral equation
  mpfi_chebpoly_lrintop_t K;
  mpfi_chebpoly_lrintop_init(K);
  mpfi_chebpoly_lrintop_set_lode_xD(K, L->a, r);
  // mpfi_chebpoly_lrintop_set_lode_Dx(K, L->a, r);
  mpfi_chebpoly_t g;
  mpfi_chebpoly_init(g);
  mpfi_chebpoly_lrintop_set_lode_xD_initvals(g, L->a, r, v);
  // mpfi_chebpoly_lrintop_set_lode_Dx_initvals(g, r, v);

  // approx solve
  N = 10000;
  mpfr_chebpoly_lrintop_t Kfr;
  mpfr_chebpoly_lrintop_init(Kfr);
  mpfi_chebpoly_lrintop_get_mpfr_chebpoly_lrintop(Kfr, K);
  mpfr_chebpoly_t gfr;
  mpfr_chebpoly_init(gfr);
  mpfi_chebpoly_get_mpfr_chebpoly(gfr, g);
  mpfr_chebpoly_t f;
  mpfr_chebpoly_init(f);
  mpfr_chebpoly_lrintop_approxsolve(f, Kfr, gfr, N);
  mpfr_t t;
  mpfr_init(t);
  for (i = r-1 ; i >= 0 ; i--) {
    mpfr_chebpoly_antiderivative_1(f, f);
    mpfi_mid(t, v + i);
    mpfr_add(f->coeffs + 0, f->coeffs + 0, t, MPFR_RNDN);
  }

  // for (i = 0 ; i <= f->degree ; i++)
  //   printf("%.10e\n", mpfr_get_d(f->coeffs+i, MPFR_RNDN));

  FILE * fpout2 = fopen("long-trigo-diffeq-out2.dat", "w+");
  fprintf(fpout2, "%ld\n", N+r);
  for (j = 0 ; j <= N+r ; j++) {
    mpfr_chebpoly_get_coeff(c, f, j);
    mpfr_out_str(fpout2, 10, 10, c, MPFR_RNDN);
    fprintf(fpout2, "\n");
  }
















/*


  mpfr_t Tbound;
  mpfr_init(Tbound);
  mpfi_chebpoly_lrintop_t R;
  mpfi_chebpoly_lrintop_init(R);
  long d;

  // read and process all random equations
  for (r = rmin ; r <= rmax ; r++) {

    printf("\n\nr = %ld\n\n", r);

    // read ODE of order r
    sprintf(filename, "long-diffeq-data/long-rand-diffeq-%ld.dat", r);
    fp = fopen(filename, "r");
    if (mpfi_chebpoly_lode_fscanf(L, fp))
      return -1;
    fclose(fp);

    // construct Newton-Picard operator
    mpfi_chebpoly_newtonpicard_xD_Tbound(Tbound, R, L->a, r);

    // read and print degree
    d = -1;
    for (i = 0 ; i < r ; i++) {
      if (R->lpoly[i].degree > d)
        d = R->lpoly[i].degree;
      if (R->rpoly[i].degree > d)
        d = R->rpoly[i].degree;
    }
    fprintf(fpout, "%ld\t%ld\n", r, d);
  }

  fclose(fpout);
  */

  return 0;
}
