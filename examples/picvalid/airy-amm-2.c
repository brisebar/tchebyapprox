

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include <gmp.h>
#include <mpfr.h>
#include <mpfi.h>

#include "mpfi_chebpoly_lrintop.h"


void mpfi_chebpoly_lrintop_amm_valid(mpfr_t bound, const mpfi_chebpoly_lrintop_t K, const mpfi_chebpoly_t h, const mpfr_chebpoly_t f)
{
  mpfr_t Kbound;
  mpfr_init(Kbound);
  mpfi_chebpoly_lrintop_1norm(Kbound, K);
  long n = 0;
  mpfr_t Tbound;
  mpfr_init(Tbound);
  mpfr_set_si(Tbound, 1, MPFR_RNDN); // Tbound = 1
  mpfi_chebpoly_t g;
  mpfi_chebpoly_init(g);
  mpfi_chebpoly_set_mpfr_chebpoly(g, f); // g = f
  while (mpfr_cmp_d(Tbound, 0.1) > 0) {
    n++;
    fprintf(stderr, "n = %ld\n", n);
    // update g
    // mpfi_chebpoly_lrintop_evaluate_fi(g, K, g);
    // mpfi_chebpoly_sub(g, h, g);
    // update Tbound
    mpfr_div_si(Tbound, Tbound, n, MPFR_RNDU);
    mpfr_mul(Tbound, Tbound, Kbound, MPFR_RNDU);
    fprintf(stderr, "    -> Tbound = %.10e (%ld)\n", mpfr_get_d(Tbound, MPFR_RNDU), mpfr_get_exp(Tbound));
    // mpfi_chebpoly_1norm_ubound(bound, g);
    // fprintf(stderr, "    -> bound = %.10e\n", mpfr_get_d(bound, MPFR_RNDU));
  }
  mpfi_chebpoly_t gg;
  mpfi_chebpoly_init(gg);
  mpfi_chebpoly_set_mpfr_chebpoly(gg, f);
  mpfi_chebpoly_sub(gg, gg, g);
  mpfi_chebpoly_1norm_ubound(bound, gg);
  mpfr_si_sub(Tbound, 1, Tbound, MPFR_RNDD);
  mpfr_div(bound, bound, Tbound, MPFR_RNDU);

  // clear variables
  mpfi_chebpoly_clear(gg);
  mpfr_clear(Kbound);
  mpfr_clear(Tbound);
  mpfi_chebpoly_clear(g);
}




int main()
{

  srand(time(0));

  mpfr_prec_t prec = 1024;
  mpfr_set_default_prec(prec);
  long N = 100;

  mpfi_t c; // Ai(c*x)
  mpfi_init(c);
  mpfi_set_si(c, 15);
  mpfi_t c3; // c3 = c^3
  mpfi_init(c3);
  mpfi_mul(c3, c, c);
  mpfi_mul(c3, c3, c);

  long r = 2;

  long i, j, k;


  // Airy differential equationnewton
  // y'' - a^2 * x * y = 0
  mpfi_chebpoly_lode_t L;
  mpfi_chebpoly_lode_init(L);
  mpfi_chebpoly_lode_set_order(L, 2);
  mpfi_chebpoly_set_degree(L->a + 0, 1);
  mpfi_neg(L->a[0].coeffs + 1, c3);

  // initial conditions
  mpfi_ptr v = malloc(r * sizeof(mpfi_t));
  mpfi_t vtemp;
  mpfi_init(vtemp);
  mpfi_init(v + 0);
  mpfi_set_si(v + 0, 2);
  mpfi_div_si(v + 0, v + 0, 3); // v[0] = 2/3
  mpfr_swap(&v[0].left, &v[0].right);
  mpfr_gamma(&v[0].left, &v[0].left, MPFR_RNDD);
  mpfr_gamma(&v[0].right, &v[0].right, MPFR_RNDU); // v[0] = Γ(2/3)
  mpfi_set_si(vtemp, 3);
  mpfi_cbrt(vtemp, vtemp);
  mpfi_sqr(vtemp, vtemp); // vtemp = 3^(2/3)
  mpfi_mul(v + 0, vtemp, v + 0);
  mpfi_inv(v + 0, v + 0); // v[0] = 1/(3^(2/3) * Γ(2/3)) = Ai(0)
  mpfi_init(v + 1);
  mpfi_set_si(v + 1, 1);
  mpfi_div_si(v + 1, v + 1, 3); // v[1] = 1/3
  mpfr_swap(&v[1].left, &v[1].right);
  mpfr_gamma(&v[1].left, &v[1].left, MPFR_RNDD);
  mpfr_gamma(&v[1].right, &v[1].right, MPFR_RNDU); // v[1] = Γ(1/3)
  mpfi_set_si(vtemp, 3);
  mpfi_cbrt(vtemp, vtemp); // vtemp = 3^(1/3)
  mpfi_mul(v + 1, vtemp, v + 1);
  mpfi_inv(v + 1, v + 1);
  mpfi_neg(v + 1, v + 1); // v[1] = -1/(3^(1/3) * Γ(1/3)) = Ai'(0)

  // rhs = 0
  mpfi_chebpoly_t h;
  mpfi_chebpoly_init(h);

  // compute RPA for Ai'' over [-a,a]
  mpfi_chebpoly_lrintop_t K;
  mpfi_chebpoly_lrintop_init(K);
  mpfi_chebpoly_lrintop_set_lode_xD(K, L->a, r);
  mpfi_chebpoly_lrintop_set_lode_xD_rhs(h, L->a, r, h, v);
  mpfr_chebpoly_lrintop_t Kfr;
  mpfr_chebpoly_lrintop_init(Kfr);
  mpfi_chebpoly_lrintop_get_mpfr_chebpoly_lrintop(Kfr, K);
  mpfr_chebpoly_t hfr;
  mpfr_chebpoly_init(hfr);
  mpfi_chebpoly_get_mpfr_chebpoly(hfr, h);
  mpfr_chebpoly_t f;
  mpfr_chebpoly_init(f);
  mpfr_t bound;
  mpfr_init(bound);
  long iter = 1;
  clock_t start, end;
  start = clock();
  for (i = 0 ; i < iter ; i++) {
    mpfr_chebpoly_lrintop_approxsolve(f, Kfr, hfr, N);
    mpfi_chebpoly_lrintop_amm_valid(bound, K, h, f);
  }
  end = clock();
  fprintf(stderr, "bound = %.10e\n", mpfr_get_d(bound, MPFR_RNDU));
  fprintf(stderr, "ellapsed time = %f\n", ((double) end - start) / CLOCKS_PER_SEC / iter);



  return 0;
}
