

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include <gmp.h>
#include <mpfr.h>
#include <mpfi.h>

#include "mpfi_chebpoly_newtonpicard.h"




int main()
{

  srand(time(0));

  mpfr_prec_t prec = 1024;
  mpfr_set_default_prec(prec);

  long i, j, k;
  long rmax = 10;
  long degmax = 10;
  long M = 10 ;

  // random differential equation:
  // y^(r) + a[r-1] * y^(r-1) + ... + a[1] * y' + a[0] * y = 0
  // 1 <= r <= rmax
  long r = 1 + rand() % rmax;
  fprintf(stderr, "r = %ld\n", r);
  mpfr_chebpoly_ptr a = malloc(r * sizeof(mpfr_chebpoly_t));
  mpfr_ptr v = malloc(r * sizeof(mpfr_t));
  for (i = 0 ; i < r ; i++) {
    mpfr_chebpoly_init(a + i);
    long deg = rand() % (degmax + 2) - 1;
    mpfr_chebpoly_set_degree(a + i, deg);
    for (j = 0 ; j <= deg ; j++)
      mpfr_chebpoly_set_coeff_si(a + i, j, rand() % (2 * M + 1) - M);
    mpfr_chebpoly_normalise(a + i);
    mpfr_init(v + i);
    mpfr_set_si(v + i, rand() % (2 * M + 1) - M, MPFR_RNDN);
  }

  // random right hand side
  mpfr_chebpoly_t h;
  mpfr_chebpoly_init(h);
  mpfr_chebpoly_set_degree(h, rand() % (degmax + 2) - 1);
  for (i = 0 ; i <= h->degree ; i++)
    mpfr_chebpoly_set_coeff_si(h, i, rand() % (2 * M + 1) - M);


  // same equation in D-x form + D-x initial conditions
  // (-D)^r + (-D)^(r-1) b_{r-1} + ... + b_0 = hh
  mpfr_chebpoly_ptr b = malloc(r * sizeof(mpfr_chebpoly_t));
  mpfr_ptr w = malloc(r * sizeof(mpfr_t));
  for (i = 0 ; i < r ; i++) {
    mpfr_chebpoly_init(b + i);
    mpfr_init(w + i);
  }
  mpfr_chebpoly_lode_transpose(b, a, r);
  mpfr_chebpoly_lode_transpose_init_xDtoDx(w, v, b, r);
  // hh = (-1)^r * h
  mpfr_chebpoly_t hh;
  mpfr_chebpoly_init(hh);
  if (r % 2)
    mpfr_chebpoly_neg(hh, h);
  else
    mpfr_chebpoly_set(hh, h);


  // compare diff eqs
  mpfr_chebpoly_t Y1, Y2, Y1i, Temp;
  mpfr_chebpoly_init(Y1);
  mpfr_chebpoly_init(Y2);
  mpfr_chebpoly_init(Y1i);
  mpfr_chebpoly_init(Temp);

  mpfr_chebpoly_t rhstest, rrhstest;
  mpfr_chebpoly_init(rhstest);
  mpfr_chebpoly_init(rrhstest);
  mpfr_chebpoly_mul(rhstest, h, h);
  mpfr_chebpoly_mul(rhstest, rhstest, rhstest);
  mpfr_chebpoly_mul(rhstest, rhstest, h); // h^5
  if (r % 2)
    mpfr_chebpoly_neg(rrhstest, rhstest);
  else
    mpfr_chebpoly_set(rrhstest, rhstest);
  mpfr_chebpoly_set(Y1i, rhstest);
  mpfr_chebpoly_zero(Y1);
  mpfr_chebpoly_set(Y2, rrhstest);
  for (i = 0 ; i < r ; i++) {
    mpfr_chebpoly_mul(Temp, a + i, Y1i);
    mpfr_chebpoly_add(Y1, Y1, Temp);
    mpfr_chebpoly_derivative(Y1i, Y1i);
    mpfr_chebpoly_mul(Temp, b + r-1-i, rrhstest);
    mpfr_chebpoly_derivative(Y2, Y2);
    mpfr_chebpoly_sub(Y2, Temp, Y2);
  }
  mpfr_chebpoly_add(Y1, Y1, Y1i);
  // compare Y1 and Y2
  mpfr_t bound, err;
  mpfr_init(bound);
  mpfr_init(err);
  mpfr_chebpoly_1norm(bound, Y1);
  mpfr_chebpoly_sub(Y2, Y1, Y2);
  mpfr_chebpoly_1norm(err, Y2);
  mpfr_div(err, err, bound, MPFR_RNDU);
  fprintf(stderr, "rel error = %.10e\n", mpfr_get_d(err, MPFR_RNDN));

  // reconvert initial conditions
  mpfr_ptr vv = malloc(r * sizeof(mpfr_t));
  for (i = 0 ; i < r ; i++)
    mpfr_init(vv + i);
  mpfr_chebpoly_lode_transpose_init_DxtoxD(vv, w, b, r);
  for (i = 0 ; i < r ; i++) {
    mpfr_sub(vv + i, vv + i, v + i, MPFR_RNDN);
    mpfr_div(vv + i, vv + i, v + i, MPFR_RNDN);
    fprintf(stderr, "(vv/v, i=%ld) %.10e\n", i, mpfr_get_d(vv + i, MPFR_RNDN));
  }




  // solve integral equations
  mpfr_chebpoly_lrintop_t K;
  mpfr_chebpoly_lrintop_init(K);
  mpfr_chebpoly_t g, cst;
  mpfr_chebpoly_init(g);
  mpfr_chebpoly_init(cst);
  mpfr_chebpoly_set_degree(cst, 0);
  long N = 500;

  // solve x-D form
  //mpfr_chebpoly_zero(h);
  mpfr_chebpoly_t P;
  mpfr_chebpoly_init(P);
  mpfr_chebpoly_lrintop_set_lode_xD(K, a, r);
  mpfr_chebpoly_lrintop_set_lode_xD_rhs(g, a, r, h, v);
  mpfr_chebpoly_lrintop_approxsolve(P, K, g, N);
  // test
  mpfr_chebpoly_lrintop_evaluate_fr(Y1, K, P);
  mpfr_chebpoly_add(Y1, P, Y1);
  mpfr_chebpoly_sub(Y1, Y1, g);
  mpfr_chebpoly_1norm(bound, Y1);
  fprintf(stderr, "(x-D int eq defect) %.10e\n", mpfr_get_d(bound, MPFR_RNDN));
  // re-integrate
  for (i = r-1 ; i >= 0 ; i--) {
    mpfr_chebpoly_antiderivative_1(P, P);
    mpfr_chebpoly_set_coeff_fr(cst, 0, v + i);
    mpfr_chebpoly_add(P, P, cst);
  }
  // check defect
  mpfr_chebpoly_set(Y1i, P);
  mpfr_chebpoly_zero(Y1);
  for (i = 0 ; i < r ; i++) {
    mpfr_chebpoly_mul(Temp, a + i, Y1i);
    mpfr_chebpoly_add(Y1, Y1, Temp);
    mpfr_chebpoly_derivative(Y1i, Y1i);
  }
  mpfr_chebpoly_add(Y1, Y1, Y1i);
  mpfr_chebpoly_sub(Y1, Y1, h);
  mpfr_chebpoly_1norm(bound, Y1);
  fprintf(stderr, "(x-D defect) %.10e\n", mpfr_get_d(bound, MPFR_RNDN));
  // check initial conditions
  mpfr_chebpoly_set(Y1, P);
  for (i = 0 ; i < r ; i++) {
    mpfr_chebpoly_evaluate_1(vv + i, Y1);
    mpfr_sub(vv + i, vv + i, v + i, MPFR_RNDN);
    mpfr_div(vv + i, vv + i, v + i, MPFR_RNDN);
    fprintf(stderr, "(x-D init cond) %.10e\n", mpfr_get_d(vv + i, MPFR_RNDN));
    mpfr_chebpoly_derivative(Y1, Y1);
  }

  fprintf(stderr, "\n\n");

  // solve D-x form
  mpfr_chebpoly_t Q;
  mpfr_chebpoly_init(Q);
  mpfr_chebpoly_lrintop_set_lode_Dx(K, b, r);
  mpfr_chebpoly_lrintop_set_lode_Dx_rhs(g, r, hh, w);
  mpfr_chebpoly_lrintop_approxsolve(Q, K, g, N);
  // check defect
  mpfr_chebpoly_set(Y2, Q);
  for (i = 0 ; i < r ; i++) {
    mpfr_chebpoly_mul(Temp, b + r-1-i, Q);
    mpfr_chebpoly_derivative(Y2, Y2);
    mpfr_chebpoly_sub(Y2, Temp, Y2);
  }
  mpfr_chebpoly_sub(Y2, Y2, hh);
  mpfr_chebpoly_1norm(bound, Y2);
  fprintf(stderr, "(D-x defect) %.10e\n", mpfr_get_d(bound, MPFR_RNDN));
  // check initial conditions
  mpfr_chebpoly_set(Y2, Q);
  for (i = 0 ; i < r ; i++) {
    mpfr_chebpoly_evaluate_1(vv + i, Y2);
    mpfr_sub(vv + i, vv + i, w + i, MPFR_RNDN);
    mpfr_div(vv + i, vv + i, w + i, MPFR_RNDN);
    fprintf(stderr, "(D-x init cond) %.10e\n", mpfr_get_d(vv + i, MPFR_RNDN));
    mpfr_chebpoly_derivative(Y2, Y2);
    mpfr_chebpoly_mul(Temp, b + r-1-i, Q);
    mpfr_chebpoly_sub(Y2, Temp, Y2);
  }
  // check defect wrt x-D eq
  mpfr_chebpoly_set(Y1i, Q);
  mpfr_chebpoly_zero(Y1);
  for (i = 0 ; i < r ; i++) {
    mpfr_chebpoly_mul(Temp, a + i, Y1i);
    mpfr_chebpoly_add(Y1, Y1, Temp);
    mpfr_chebpoly_derivative(Y1i, Y1i);
  }
  mpfr_chebpoly_add(Y1, Y1, Y1i);
  mpfr_chebpoly_sub(Y1, Y1, h);
  mpfr_chebpoly_1norm(bound, Y1);
  fprintf(stderr, "(x-D defect of D-x sol) %.10e\n", mpfr_get_d(bound, MPFR_RNDN));
  // compare init cond in x-D form
  mpfr_chebpoly_set(Y2, Q);
  for (i = 0 ; i < r ; i++) {
    mpfr_chebpoly_evaluate_1(vv + i, Y2);
    mpfr_sub(vv + i, vv + i, v + i, MPFR_RNDN);
    mpfr_div(vv + i, vv + i, v + i, MPFR_RNDN);
    fprintf(stderr, "(x-D init cond of D-x eq) %.10e\n", mpfr_get_d(vv + i, MPFR_RNDN));
    mpfr_chebpoly_derivative(Y2, Y2);
  }

  // compare solutions
  mpfr_chebpoly_1norm(bound, P);
  mpfr_chebpoly_sub(Q, P, Q);
  mpfr_chebpoly_1norm(err, Q);
  fprintf(stderr, "rel error = %.10e\n", mpfr_get_d(err, MPFR_RNDN) / mpfr_get_d(bound, MPFR_RNDN));


/*
  // Tbound
  fprintf(stderr, "x-D form:\n");
  mpfi_chebpoly_lrintop_t R;
  mpfi_chebpoly_lrintop_init(R);
  mpfr_t Tbound;
  mpfr_init(Tbound);
  mpfi_chebpoly_newtonpicard_xD_Tbound(Tbound, R, L->a, r);

  // convert to D-x form
  mpfi_chebpoly_ptr b = malloc(r * sizeof(mpfi_chebpoly_t));
  for (i = 0 ; i < r ; i++)
    mpfi_chebpoly_init(b + i);
  mpfi_chebpoly_lode_transpose_forward(b, L->a, r);

  // Tbound for D-x form
  fprintf(stderr, "\nD-x form:\n");
  mpfi_chebpoly_newtonpicard_Dx_Tbound(Tbound, R, b, r);
*/


  return 0;
}
