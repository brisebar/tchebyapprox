

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include <gmp.h>
#include <mpfr.h>
#include <mpfi.h>

#include "mpfi_chebpoly_newtonpicard.h"




int main()
{

  srand(time(0));

  mpfr_prec_t prec = 1024;
  mpfr_set_default_prec(prec);

  long N;
  long i, j, k;
  long rmax = 10;
  long degmax = 10;
  long M = 10 ;

  // random differential equation:
  // y^(r) + a[r-1] * y^(r-1) + ... + a[1] * y' + a[0] * y = 0
  // 1 <= r <= rmax
  long r = 1 + rand() % rmax;
  fprintf(stderr, "r = %ld\n", r);
  mpfi_chebpoly_ptr a = malloc(r * sizeof(mpfi_chebpoly_t));
  for (i = 0 ; i < r ; i++) {
    mpfi_chebpoly_init(a + i);
    long deg = rand() % (degmax + 2) - 1;
    mpfi_chebpoly_set_degree(a + i, deg);
    for (j = 0 ; j <= deg ; j++)
      mpfi_set_si(a[i].coeffs + j, rand() % (2 * M + 1) - M);
    mpfi_chebpoly_normalise(a + i);
  }


/*
  // test resolvent kernel
  mpfr_chebpoly_ptr phi = malloc(r * sizeof(mpfr_chebpoly_t));
  mpfr_chebpoly_ptr psi = malloc(r * sizeof(mpfr_chebpoly_t));
  mpfr_chebpoly_ptr afr = malloc(r * sizeof(mpfr_chebpoly_t));
  for (i = 0 ; i < r ; i++) {
    mpfr_chebpoly_init(afr + i);
    mpfi_chebpoly_get_mpfr_chebpoly(afr + i, a + i);
    mpfr_chebpoly_init(phi + i);
    mpfr_chebpoly_init(psi + i);
  }
  mpfr_chebpoly_resolventkernel(phi, psi, afr, r, 1000);
  // tests
  mpfr_chebpoly_t y, yj, Temp, cst;
  mpfr_chebpoly_init(y);
  mpfr_chebpoly_init(yj);
  mpfr_chebpoly_init(Temp);
  mpfr_chebpoly_init(cst);
  mpfr_chebpoly_set_degree(cst, 0);
  mpfr_t bound;
  mpfr_init(bound);
  // check phi
  for (i = 0 ; i < r ; i++) {
    mpfr_chebpoly_set(yj, phi + i);
    mpfr_chebpoly_set(y, yj);
    for (j = r-1 ; j >= 0 ; j--) {
      // yj = phi_i^(j)
      mpfr_chebpoly_antiderivative_1(yj, yj);
      mpfr_chebpoly_set_coeff_si(cst, 0, i==j);
      mpfr_chebpoly_add(yj, yj, cst);
      // update y = partial sum
      mpfr_chebpoly_mul(Temp, afr + j, yj);
      mpfr_chebpoly_add(y, y, Temp);
    }
    mpfr_chebpoly_1norm(bound, y);
    fprintf(stderr, "(phi[%ld] x-D defect) %.10e\n", i, mpfr_get_d(bound, MPFR_RNDN));
  }
  // check psi
  for (i = 0 ; i < r ; i++) {
    mpfr_chebpoly_set(y, psi + i);
    for (j = 0 ; j < r ; j++) {
      // evaluate and print initial condition
      mpfr_chebpoly_evaluate_1(bound, y);
      mpfr_sub_si(bound, bound, i==j, MPFR_RNDN);
      fprintf(stderr, "(psi[%ld]^[%ld](-1) error) %.10e\n", i, j, mpfr_get_d(bound, MPFR_RNDN));
      // update y^[j] => y^[j+1]
      mpfr_chebpoly_mul(Temp, afr + r-1-j, psi + i);
      mpfr_chebpoly_derivative(y, y);
      mpfr_chebpoly_sub(y, Temp, y);
    }
    mpfr_chebpoly_1norm(bound, y);
    fprintf(stderr, "(psi[%ld] D-x defect) %.10e\n", i, mpfr_get_d(bound, MPFR_RNDN));
  }

  return 0;
*/

  // Tbound
  fprintf(stderr, "x-D form:\n");
  mpfi_chebpoly_lrintop_t R;
  mpfi_chebpoly_lrintop_init(R);
  mpfr_t Tbound;
  mpfr_init(Tbound);
  mpfi_chebpoly_newtonpicard_xD_Tbound(Tbound, R, a, r);

  // convert to D-x form
  mpfi_chebpoly_ptr b = malloc(r * sizeof(mpfi_chebpoly_t));
  for (i = 0 ; i < r ; i++)
    mpfi_chebpoly_init(b + i);
  mpfi_chebpoly_lode_transpose(b, a, r);

  // Tbound for D-x form
  fprintf(stderr, "\nD-x form:\n");
  mpfi_chebpoly_newtonpicard_Dx_Tbound(Tbound, R, b, r);



  return 0;
}
