

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include <gmp.h>
#include <mpfr.h>
#include <mpfi.h>

#include "mpfr_chebpoly_lrintop.h"
#include "mpfi_chebpoly_intop_newton.h"



int main()
{

  srand(time(0));

  mpfr_prec_t prec = 256;
  mpfr_set_default_prec(prec);

  long N = 10;

  mpfr_t a; // Ai(a*x)
  mpfr_init(a);
  mpfr_t a3; // a3 = a^3
  mpfr_init(a3);
  long r = 2;
  long ai, i, j, k;
  mpfr_t bound;
  mpfr_init(bound);
  // Airy differential equation
  mpfr_chebpoly_lode_t L;
  mpfr_chebpoly_lode_init(L);
  mpfr_chebpoly_lode_set_order(L, 2);
  mpfr_chebpoly_set_degree(L->a + 0, 1);
  // integral operator
  mpfr_chebpoly_intop_t K;
  mpfr_chebpoly_intop_init(K);
  mpfi_chebpoly_intop_t Kfi;
  mpfi_chebpoly_intop_init(Kfi);
  mpfr_bandmatrix_t M_K_inv;
  mpfr_bandmatrix_init(M_K_inv);

  for (ai = 1 ; ai <= 15 ; ai++) {

    printf("\n\n\na = %ld\n\n\n", ai);

    mpfr_set_si(a, ai, MPFR_RNDN);
    mpfr_mul(a3, a, a, MPFR_RNDN);
    mpfr_mul(a3, a3, a, MPFR_RNDN);

    // Airy differential equation
    mpfr_neg(L->a[0].coeffs + 1, a3, MPFR_RNDN);

    // test with TOMS method
    mpfr_chebpoly_intop_set_lode(K, L);
    mpfi_chebpoly_intop_set_mpfr_chebpoly_intop(Kfi, K);
    mpfi_chebpoly_intop_newton_contract_fr(bound, M_K_inv, Kfi, N);

    printf("N = %ld\n\n\n", M_K_inv->dim - 1);

  }


  return 0;
}
