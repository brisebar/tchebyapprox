

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include <gmp.h>
#include <mpfr.h>
#include <mpfi.h>

#include "chebmodel_newtonpicard.h"



// read ODE coefficients from file fp
// !! the input data is supposed to be given in monomial basis
// (therefore we need to convert)
int mpfi_chebpoly_lode_fscanf(mpfi_chebpoly_lode_t L, FILE * fp)
{
  long r, di, i, j;
  char strcoeff[1000];
  mpfi_poly_t p;
  mpfi_poly_init(p);
  mpfi_t leadcoeff;
  mpfi_init(leadcoeff);

  // read order r
  fscanf(fp, "%ld", &r);
  mpfi_chebpoly_lode_set_order(L, r);

  // read polynomial coefficients
  for (i = 0 ; i < r ; i++) {

    // read degree of i-th polynomial coefficient
    fscanf(fp, "%ld", &di);
    mpfi_poly_set_degree(p, di);

    // read its coefficients
    for (j = 0 ; j <= di ; j++) {
      fscanf(fp, "%s", strcoeff);
      mpfi_set_str(p->coeffs + j, strcoeff, 10);
    }

    // convert to the Chebyshev basis
    mpfi_chebpoly_set_mpfi_poly(L->a + i, p);
  }

  // read head coefficient, check if constant, and normalize
  fscanf(fp, "%ld", &di);
  if (di != 0) {
    fprintf(stderr, "singular operator. Abort.\n");
    return -1;
  }
  fscanf(fp, "%s", strcoeff);
  mpfi_set_str(leadcoeff, strcoeff, 10);
  for (i = 0 ; i < r ; i++)
    mpfi_chebpoly_scalar_div_fi(L->a + i, L->a + i, leadcoeff);

  // clear temp variables
  mpfi_poly_clear(p);
  mpfi_clear(leadcoeff);

  return 0;
}

// read ODE coefficients and initial conditions from file fp
// v must be allocated of size r * sizeof(mpfi_t)
// !! the input data is supposed to be given in monomial basis
// (therefore we need to convert)
int mpfi_chebpoly_lode_initvals_fscanf(mpfi_chebpoly_lode_t L, mpfi_ptr v, FILE * fp)
{
  // first read ODE
  if (mpfi_chebpoly_lode_fscanf(L, fp))
    return -1;
  long r = L->order;

  // read initial conditions
  long i;
  char strcoeff[1000];
  for (i = 0 ; i < r ; i++) {
    fscanf(fp, "%s", strcoeff);
    mpfi_set_str(v + i, strcoeff, 10);
  }

  return 0;
}



int main()
{

  srand(time(0));

  mpfr_prec_t prec = 512;
  mpfr_set_default_prec(prec);

  mpfi_chebpoly_lode_t L;
  mpfi_chebpoly_lode_init(L);

  long rmin = 10;
  long rmax = 30;
  long r, i;
  char filename[1000];
  FILE * fp;
  FILE * fpout = fopen("long-rand-diffeq-out.dat", "w+");

  mpfr_t Tbound;
  mpfr_init(Tbound);
  mpfi_chebpoly_lrintop_t R;
  mpfi_chebpoly_lrintop_init(R);
  long d;

  // read and process all random equations
  for (r = rmin ; r <= rmax ; r++) {

    printf("\n\nr = %ld\n\n", r);

    // read ODE of order r
    sprintf(filename, "long-diffeq-data/long-rand-diffeq-%ld.dat", r);
    fp = fopen(filename, "r");
    if (mpfi_chebpoly_lode_fscanf(L, fp))
      return -1;
    fclose(fp);

    // construct Newton-Picard operator
    mpfi_chebpoly_newtonpicard_xD_Tbound(Tbound, R, L->a, r);

    // read and print degree
    d = -1;
    for (i = 0 ; i < r ; i++) {
      if (R->lpoly[i].degree > d)
        d = R->lpoly[i].degree;
      if (R->rpoly[i].degree > d)
        d = R->rpoly[i].degree;
    }
    fprintf(fpout, "%ld\t%ld\n", r, d);
  }

  fclose(fpout);

  return 0;
}
