

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include <gmp.h>
#include <mpfr.h>
#include <mpfi.h>

#include "chebmodel_newtonpicard.h"




int main()
{

  srand(time(0));

  mpfr_prec_t prec = 1024;
  mpfr_set_default_prec(prec);

  // N, Ncos
  long N = 500;
  long Ncos = 200;

  // eps
  mpfi_t eps;
  mpfi_init(eps);
  mpfi_set_si(eps, 1);
  mpfi_div_si(eps, eps, 10000000);

  // compute cos(x) over [-1,1]
  // (-D)^2 + 1
  // Dx init cond :
  // v[0] = cos(-1)
  // v[1] = -cos'(-1) + 0 = sin'(-1)
  chebmodel_ptr a = malloc(2 * sizeof(chebmodel_t));
  chebmodel_init(a + 0);
  chebmodel_init(a + 1);
  mpfi_chebpoly_set_coeff_si(a[0].poly, 0, 1);
  mpfi_ptr v = malloc(2 * sizeof(mpfi_t));
  mpfi_init(v + 0);
  mpfi_init(v + 1);
  mpfi_set_si(v + 0, -1);
  mpfi_cos(v + 0, v + 0);
  mpfi_set_si(v + 1, -1);
  mpfi_sin(v + 1, v + 1);
  chebmodel_t Cos, Zero;
  chebmodel_init(Cos);
  chebmodel_init(Zero);
  chebmodel_newtonpicard_Dx_approxvalid(Cos, a, 2, Zero, v, Ncos);

  // test Cos
  printf("bound = %.10e\n", mpfr_get_d(Cos->rem, MPFR_RNDU));
  printf("cos(1) = ");
  mpfi_t temp;
  mpfi_init(temp);
  chebmodel_evaluate0(temp, Cos);
  mpfi_out_str(stdout, 10, 10, temp);
  printf("\n\n");

  // compute (cos(x)-8/10)/eps over [-1,1]
  mpfi_set_si(temp, 8);
  mpfi_div_si(temp, temp, 10);
  mpfi_sub(Cos->poly->coeffs + 0, Cos->poly->coeffs + 0, temp);
  chebmodel_scalar_div_fi(Cos, Cos, eps);

  // boundary layer diff eq
  chebmodel_set(a + 0, Cos);
  chebmodel_zero(a + 1);
  mpfi_chebpoly_set_coeff_si(a->poly, 1, -2);
  chebmodel_mul(a + 1, a + 1, Cos);

  // solve
  mpfi_set_si(v + 0, 1);
  mpfi_set_si(v + 1, 0);
  chebmodel_t f;
  chebmodel_init(f);
  chebmodel_newtonpicard_xD_approxvalid(f, a, 2, Zero, v, N);

  printf("f bound = %.10e\n", mpfr_get_d(f->rem, MPFR_RNDU));


/*


  mpfi_t c; // Ai(c*x)
  mpfi_init(c);
  mpfi_set_si(c, 15);
  mpfi_t c3; // c3 = c^3
  mpfi_init(c3);
  mpfi_mul(c3, c, c);
  mpfi_mul(c3, c3, c);

  long r = 2;

  long i, j, k;


  // Airy differential equation
  // y'' - a^2 * x * y = 0
  mpfi_chebpoly_ptr a = malloc(r * sizeof(mpfi_chebpoly_t));
  for (i = 0 ; i < r ; i++)
    mpfi_chebpoly_init(a + i);
  mpfi_chebpoly_set_degree(a + 0, 1);
  mpfi_neg(a[0].coeffs + 1, c3);

  // initial conditions
  mpfi_ptr v = malloc(r * sizeof(mpfi_t));
  mpfi_t vtemp;
  mpfi_init(vtemp);
  mpfi_init(v + 0);
  mpfi_set_si(v + 0, 2);
  mpfi_div_si(v + 0, v + 0, 3); // v[0] = 2/3
  mpfr_swap(&v[0].left, &v[0].right);
  mpfr_gamma(&v[0].left, &v[0].left, MPFR_RNDD);
  mpfr_gamma(&v[0].right, &v[0].right, MPFR_RNDU); // v[0] = Γ(2/3)
  mpfi_set_si(vtemp, 3);
  mpfi_cbrt(vtemp, vtemp);
  mpfi_sqr(vtemp, vtemp); // vtemp = 3^(2/3)
  mpfi_mul(v + 0, vtemp, v + 0);
  mpfi_inv(v + 0, v + 0); // v[0] = 1/(3^(2/3) * Γ(2/3)) = Ai(0)
  mpfi_init(v + 1);
  mpfi_set_si(v + 1, 1);
  mpfi_div_si(v + 1, v + 1, 3); // v[1] = 1/3
  mpfr_swap(&v[1].left, &v[1].right);
  mpfr_gamma(&v[1].left, &v[1].left, MPFR_RNDD);
  mpfr_gamma(&v[1].right, &v[1].right, MPFR_RNDU); // v[1] = Γ(1/3)
  mpfi_set_si(vtemp, 3);
  mpfi_cbrt(vtemp, vtemp); // vtemp = 3^(1/3)
  mpfi_mul(v + 1, vtemp, v + 1);
  mpfi_inv(v + 1, v + 1);
  mpfi_neg(v + 1, v + 1); // v[1] = -1/(3^(1/3) * Γ(1/3)) = Ai'(0)

  // rhs = 0
  mpfi_chebpoly_t h;
  mpfi_chebpoly_init(h);

  // compute RPA for Ai over [-a,a]
  mpfr_chebpoly_t f;
  mpfr_chebpoly_init(f);
  mpfr_t bound;
  mpfr_init(bound);
  long N = 300;
  long iter = 1;
  clock_t start, end;
  start = clock();
  for (i = 0 ; i < iter ; i++)
    mpfi_chebpoly_newtonpicard_xD_approxvalid(f, bound, a, r, h, v, N);
  end = clock();
  fprintf(stderr, "bound = %.10e\n", mpfr_get_d(bound, MPFR_RNDU));
  fprintf(stderr, "ellapsed time = %f\n", ((double) end - start) / CLOCKS_PER_SEC / iter);

*/

  return 0;
}
