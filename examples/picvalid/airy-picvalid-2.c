

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include <gmp.h>
#include <mpfr.h>
#include <mpfi.h>

#include "mpfi_chebpoly_newtonpicard.h"




int main()
{

  srand(time(0));

  mpfr_prec_t prec = 256;
  mpfr_set_default_prec(prec);

  long N;


  mpfi_t c; // Ai(c*x)
  mpfi_init(c);
  mpfi_set_si(c, 15);
  mpfi_t c3; // c3 = c^3
  mpfi_init(c3);
  mpfi_mul(c3, c, c);
  mpfi_mul(c3, c3, c);

  long r = 2;

  long i, j, k;

  mpfr_t bound;
  mpfr_init(bound);

  // Airy differential equation
  // y'' - a^2 * x * y = 0
  mpfi_chebpoly_ptr a = malloc(r * sizeof(mpfi_chebpoly_t));
  for (i = 0 ; i < r ; i++)
    mpfi_chebpoly_init(a + i);
  mpfi_chebpoly_set_degree(a + 0, 1);
  mpfi_neg(a[0].coeffs + 1, c3);

  // Tbound for x-D form
  fprintf(stderr, "x-D form:\n");
  mpfi_chebpoly_lrintop_t R;
  mpfi_chebpoly_lrintop_init(R);
  mpfr_t Tbound;
  mpfr_init(Tbound);
  mpfi_chebpoly_newtonpicard_xD_Tbound(Tbound, R, a, r);

  // convert to D-x form
  mpfi_chebpoly_ptr b = malloc(r * sizeof(mpfi_chebpoly_t));
  for (i = 0 ; i < r ; i++)
    mpfi_chebpoly_init(b + i);
  mpfi_chebpoly_lode_transpose(b, a, r);

  // Tbound for D-x form
  fprintf(stderr, "\nD-x form:\n");
  mpfi_chebpoly_newtonpicard_Dx_Tbound(Tbound, R, b, r);




  return 0;
}
