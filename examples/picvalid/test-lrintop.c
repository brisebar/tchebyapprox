

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include <gmp.h>
#include <mpfr.h>
#include <mpfi.h>

#include "double_chebpoly_lrintop.h"




int main()
{

  srand(time(0));

  mpfr_prec_t prec = 256;
  mpfr_set_default_prec(prec);

  long r = 10;
  long n = 50;
  long M = 1000;
  long i, j, k, nk, mk;


  // random operator K
  double_chebpoly_lrintop_t K;
  double_chebpoly_lrintop_init(K);
  // 1 <= r1 <= r
  long r1 = 1 + rand() % r;
  double_chebpoly_lrintop_set_length(K, r1);
  for (k = 0 ; k < r1 ; k++) {
    // -1 <= nk, mk <= n
    nk = rand() % (n + 2) - 1;
    mk = rand() % (n + 2) - 1;
    double_chebpoly_set_degree(K->lpoly + k, nk);
    double_chebpoly_set_degree(K->rpoly + k, mk);
    for (i = 0 ; i <= nk ; i++)
      double_set_si(K->lpoly[k].coeffs + i, rand() % (2 * M + 1) - M, MPFR_RNDN);
    for (j = 0 ; j <= mk ; j++)
      double_set_si(K->rpoly[k].coeffs + j, rand() % (2 * M + 1) - M, MPFR_RNDN);
  }

  // random operator L
  double_chebpoly_lrintop_t L;
  double_chebpoly_lrintop_init(L);
  // 1 <= r2 <= r
  long r2 = 1 + rand() % r;
  double_chebpoly_lrintop_set_length(L, r2);
  for (k = 0 ; k < r2 ; k++) {
    // -1 <= nk, mk <= n
    nk = rand() % (n + 2) - 1;
    mk = rand() % (n + 2) - 1;
    double_chebpoly_set_degree(L->lpoly + k, nk);
    double_chebpoly_set_degree(L->rpoly + k, mk);
    for (i = 0 ; i <= nk ; i++)
      double_set_si(L->lpoly[k].coeffs + i, rand() % (2 * M + 1) - M, MPFR_RNDN);
    for (j = 0 ; j <= mk ; j++)
      double_set_si(L->rpoly[k].coeffs + j, rand() % (2 * M + 1) - M, MPFR_RNDN);
  }

  // random polynomial P
  double_chebpoly_t P;
  double_chebpoly_init(P);
  // -1 <= nk <= n
  nk = rand() % (n + 2) - 1;
  double_chebpoly_set_degree(P, nk);
  for (i = 0 ; i <= nk ; i++)
    double_set_si(P->coeffs + i, rand() % (2 * M + 1) - M, MPFR_RNDN);

  // compose K and L
  double_chebpoly_t Q1, Q2;
  double_chebpoly_init(Q1);
  double_chebpoly_init(Q2);

  // Q1 = K(L(P))
  double_chebpoly_lrintop_evaluate_d(Q1, L, P);
  double_chebpoly_lrintop_evaluate_d(Q1, K, Q1);

  // Q2 = (K∘L)(P)
  double_chebpoly_lrintop_t K_L;
  double_chebpoly_lrintop_init(K_L);
  double_chebpoly_lrintop_comp(K_L, K, L);
  double_chebpoly_lrintop_evaluate_d(Q2, K_L, P);

  // subtract and compute bound (ratio to the average)
  double_chebpoly_t R1, R2;
  double_chebpoly_init(R1);
  double_chebpoly_init(R2);
  double_chebpoly_sub(R1, Q1, Q2);
  double_chebpoly_add(R2, Q1, Q2);
  double_chebpoly_scalar_mul_2si(R2, R2, -1);
  double bound1, bound2;
  double_chebpoly_1norm(&bound1, R1);
  double_chebpoly_1norm(&bound2, R2);
  printf("\nratio = %e\n", bound1 / bound2);

  // test operations on lrintop + 1norm
  double_chebpoly_lrintop_t T1, T2;
  double_chebpoly_lrintop_init(T1);
  double_chebpoly_lrintop_init(T2);
  double_chebpoly_lrintop_comp(T1, K, L);
  double_chebpoly_lrintop_1norm(&bound1, T1);
  double_chebpoly_lrintop_neg(K, K);
  double_chebpoly_lrintop_comp(T2, K, L);
  double_chebpoly_lrintop_add(T2, T1, T2);
  double_chebpoly_lrintop_1norm(&bound2, T2);
  printf("ratio = %e\n", bound2 / bound1);



  return 0;
}
