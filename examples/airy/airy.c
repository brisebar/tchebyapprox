/*
Example for the TchebyApprox library: Airy function
evolution of validation parameters and certified solutions
*/

#include <stdio.h>
#include <stdlib.h>

#include <gmp.h>
#include <mpfr.h>
#include <mpfi.h>

#include "mpfi_chebpoly_intop_newton.h"
#include "chebmodel_intop_newton.h"


// rescaled Airy function over [a,0] or [0,a] (depending on the sign of a)
// determine validation parameters n (truncation order), h2 and d2 (parameters of the almost-banded structure of the approximate inverse)
// underlying floating-point type: [double]
void airy_validate_d(long *n, long *h2, long *d2, const mpfr_t a)
{

  mpfi_t temp;
  mpfi_init(temp);
  mpfi_set_fr(temp, a);
  mpfi_sqr(temp, temp);
  mpfi_mul_fr(temp, temp, a);
  mpfi_mul_2si(temp, temp, -3); 
  mpfi_neg(temp, temp); // temp = -a^3/8

  // Airy equation
  mpfi_chebpoly_lode_t L;
  mpfi_chebpoly_lode_init(L);
  mpfi_chebpoly_lode_set_order(L, 2);
  mpfi_chebpoly_set_degree(L->a + 0, 1);
  mpfi_set(L->a[0].coeffs + 0, temp);
  mpfi_set(L->a[0].coeffs + 1, temp);

  // integral operator
  mpfi_chebpoly_intop_t K;
  mpfi_chebpoly_intop_init(K);
  mpfi_chebpoly_intop_set_lode(K, L);
  long d = mpfi_chebpoly_intop_Hwidth(K);
  long h = mpfi_chebpoly_intop_Dwidth(K);
  double_bandmatrix_t M_K_inv;
  double_bandmatrix_init(M_K_inv);
  double_t bound;
  double_init(bound);

  // get contracting operator
  mpfi_chebpoly_intop_newton_contract(bound, M_K_inv, K, h+d);
  *n = M_K_inv->dim - 1;
  *h2 = M_K_inv->Hwidth;
  *d2 = M_K_inv->Dwidth;

  // clear variables
  mpfi_clear(temp);
  mpfi_chebpoly_lode_clear(L);
  mpfi_chebpoly_intop_clear(K);
  double_bandmatrix_clear(M_K_inv);
  double_clear(bound);

  return;
}


// rescaled Airy function over [a,0] or [0,a] (depending on the sign of a)
// determine validation parameters n (truncation order), h2 and d2 (parameters of the almost-banded structure of the approximate inverse)
// underlying floating-point type: [mpfr]
void airy_validate_fr(long *n, long *h2, long *d2, const mpfr_t a)
{

  mpfi_t temp;
  mpfi_init(temp);
  mpfi_set_fr(temp, a);
  mpfi_sqr(temp, temp);
  mpfi_mul_fr(temp, temp, a);
  mpfi_mul_2si(temp, temp, -3);
  mpfi_neg(temp, temp); // temp = -a^3/8

  // Airy equation
  mpfi_chebpoly_lode_t L;
  mpfi_chebpoly_lode_init(L);
  mpfi_chebpoly_lode_set_order(L, 2);
  mpfi_chebpoly_set_degree(L->a + 0, 1);
  mpfi_set(L->a[0].coeffs + 0, temp);
  mpfi_set(L->a[0].coeffs + 1, temp);

  // integral operator
  mpfi_chebpoly_intop_t K;
  mpfi_chebpoly_intop_init(K);
  mpfi_chebpoly_intop_set_lode(K, L);
  long d = mpfi_chebpoly_intop_Hwidth(K);
  long h = mpfi_chebpoly_intop_Dwidth(K);
  mpfr_bandmatrix_t M_K_inv;
  mpfr_bandmatrix_init(M_K_inv);
  mpfr_t bound;
  mpfr_init(bound);

  // get contracting operator
  mpfi_chebpoly_intop_newton_contract_fr(bound, M_K_inv, K, h+d);
  *n = M_K_inv->dim - 1;
  *h2 = M_K_inv->Hwidth;
  *d2 = M_K_inv->Dwidth;

  // clear variables
  mpfi_clear(temp);
  mpfi_chebpoly_lode_clear(L);
  mpfi_chebpoly_intop_clear(K);
  mpfr_bandmatrix_clear(M_K_inv);
  mpfr_clear(bound);

  return;
}


// rescaled Airy function over [a,0] or [0,a] (depending on the sign of a)
// validated solution, returned as a [chebmodel]
void airy_solve(chebmodel_t F, const mpfr_t a, long n)
{

  mpfi_t temp;
  mpfi_init(temp);
  mpfi_set_fr(temp, a);
  mpfi_sqr(temp, temp);
  mpfi_mul_fr(temp, temp, a);
  mpfi_mul_2si(temp, temp, -3);
  mpfi_neg(temp, temp); // temp = -a^3/8

  // Airy equation
  mpfi_chebpoly_lode_t L;
  mpfi_chebpoly_lode_init(L);
  mpfi_chebpoly_lode_set_order(L, 2);
  mpfi_chebpoly_set_degree(L->a + 0, 1);
  mpfi_set(L->a[0].coeffs + 0, temp);
  mpfi_set(L->a[0].coeffs + 1, temp);
  mpfr_chebpoly_lode_t L_fr;
  mpfr_chebpoly_lode_init(L_fr);
  mpfi_chebpoly_lode_get_mpfr_chebpoly_lode(L_fr, L);

  // initial conditions
  __mpfi_struct I[2];
  mpfi_init(I + 0); mpfi_init(I + 1);
  mpfi_set_si(temp, 2); mpfi_div_si(temp, temp, 3);
  mpfr_ui_pow(&(I[0].left), 3, &(temp->left), MPFR_RNDD);
  mpfr_ui_pow(&(I[0].right), 3, &(temp->right), MPFR_RNDU);
  mpfi_set_si(temp, 2); mpfi_div_si(temp, temp, 3);
  mpfr_swap(&(temp->left), &(temp->right));
  mpfr_gamma(&(temp->left), &(temp->left), MPFR_RNDD);
  mpfr_gamma(&(temp->right), &(temp->right), MPFR_RNDU);
  mpfi_mul(I + 0, I + 0, temp);
  mpfi_si_div(I + 0, 1, I + 0); // I[0] = 1/(3^(2/3)*Gamma(2/3))
  mpfi_set_si(temp, 1); mpfi_div_si(temp, temp, 3);
  mpfr_ui_pow(&(I[1].left), 3, &(temp->left), MPFR_RNDD);
  mpfr_ui_pow(&(I[1].right), 3, &(temp->right), MPFR_RNDU);
  mpfi_set_si(temp, 1); mpfi_div_si(temp, temp, 3);
  mpfr_swap(&(temp->left), &(temp->right));
  mpfr_gamma(&(temp->left), &(temp->left), MPFR_RNDD);
  mpfr_gamma(&(temp->right), &(temp->right), MPFR_RNDU);
  mpfi_mul(I + 1, I + 1, temp);
  mpfi_fr_div(I + 1, a, I + 1);
  mpfi_mul_2si(I + 1, I + 1, -1); // I[1] = (a/2)/(3^(1/3)*Gamma(1/3))
  __mpfr_struct I_fr[2];
  mpfr_inits(I_fr + 0, I_fr + 1, NULL);
  mpfi_mid(I_fr + 0, I + 0); mpfi_mid(I_fr + 1, I + 1);

  // rhs g
  mpfi_chebpoly_t g;
  mpfi_chebpoly_init(g); // g = 0
  mpfr_chebpoly_t g_fr;
  mpfr_chebpoly_init(g_fr); // g_fr = 0

  // approx solve
  mpfr_chebpoly_struct Sols[3];
  mpfr_chebpoly_init(Sols + 0); mpfr_chebpoly_init(Sols + 1);
  mpfr_chebpoly_init(Sols + 2);
  mpfr_chebpoly_lode_intop_approxsolve(Sols, L_fr, g_fr, I_fr, 2*n);
  mpfr_chebpoly_set_degree(Sols + 2, n);

  // validate
  mpfi_chebpoly_lode_intop_newton_validate_sol_d(F->rem, L, g, I, Sols + 2);
  mpfi_chebpoly_set_mpfr_chebpoly(F->poly, Sols + 2);
  chebmodel_antiderivative_1(F, F);
  mpfi_add(F->poly->coeffs + 0, F->poly->coeffs + 0, I + 1);
  chebmodel_antiderivative_1(F, F);
  mpfi_add(F->poly->coeffs + 0, F->poly->coeffs + 0, I + 0);

  // clear variables
  mpfi_clear(temp);
  mpfi_chebpoly_lode_clear(L);
  mpfr_chebpoly_lode_clear(L_fr);
  mpfi_clear(I + 0); mpfi_clear(I + 1);
  mpfr_clears(I_fr + 0, I_fr + 1, NULL);
  mpfi_chebpoly_clear(g);
  mpfr_chebpoly_clear(g_fr);
  mpfr_chebpoly_clear(Sols + 0); mpfr_chebpoly_clear(Sols + 1);
  mpfr_chebpoly_clear(Sols + 2);

  return;
}



// create gnuplot file to plot P with n equidistant points from -1 to 1
void double_chebpoly_plot(const double_chebpoly_t P, long n, FILE * filename)
{
  double step = (double) 2 / (n-1);
  long i;
  mpfr_t x, y;
  mpfr_init(x);
  mpfr_init(y);
  for (i = 0 ; i < n ; i++) {
    mpfr_set_d(x, -1 + i * step, MPFR_RNDN);
    double_chebpoly_evaluate_fr(y, P, x);
    fprintf(filename, "%f\t%f\n", mpfr_get_d(x, MPFR_RNDN), mpfr_get_d(y, MPFR_RNDN));
  }
}




int main()
{
  mpfr_prec_t prec = 100;
  mpfr_set_default_prec(prec);
  
  mpfr_t a;
  mpfr_init(a);
  mpfr_set_si(a, -10, MPFR_RNDN);
  

// get validation parameters
/*

  long n, h2, d2;  
  airy_validate_d(&n, &h2, &d2, a);

  printf("--------------\nn = %ld, h' = %ld, d' = %ld\n", n, h2, d2);
*/

// chebmodel for Ai

  long n, h2, d2;  
  n = 48;
  chebmodel_t F;
  chebmodel_init(F);
  double_chebpoly_t P;
  double_chebpoly_init(P);
  
  airy_solve(F, a, n);  
  chebmodel_get_double_chebpoly(P, F);
  
  FILE * file_plot = fopen("airy_plot_Ai.dat", "w+");
  fprintf(file_plot, "#Plot Ai(x) over [a,0] or [0,a]\n#a = %f, deg = %ld, err = ", mpfr_get_d(a, MPFR_RNDN), P->degree);
  mpfr_out_str(file_plot, 10, 5, F->rem, MPFR_RNDU);
  fprintf(file_plot, "\n");
  double_chebpoly_plot(P, 1000, file_plot);
  fclose(file_plot);

  chebmodel_clear(F);
  double_chebpoly_clear(P);


  double_vec_t V;
  double_vec_init(V);
  double_vec_is_zero(V); 
 
 
  
  mpfr_clear(a);

  return 0;
}
