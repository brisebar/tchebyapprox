
/* test interpolation */


#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>

#include <gmp.h>
#include <mpfr.h>
#include <mpfi.h>
#include <mpfi_io.h>

#include "chebmodel_vec_intop_newton.h"


void interpolate_poly(mpfr_chebpoly_t P, const mpfr_chebpoly_t Q, long n)
{
  mpfr_t t;
  mpfr_init(t);
  long k;

  // F contains the evaluation of the cosine series at the Chebyshev nodes
  mpfr_vec_t F;
  mpfr_vec_init(F);
  mpfr_vec_set_length(F, n+1);

  for (k = 0 ; k <= n ; k++) {

    mpfr_const_pi(t, MPFR_RNDN);
    mpfr_mul_si(t, t, 2*k+1, MPFR_RNDN);
    mpfr_div_si(t, t, 2*n+2, MPFR_RNDN);
    mpfr_cos(t, t, MPFR_RNDN);

    mpfr_chebpoly_evaluate_fr(F->coeffs + k, Q, t);
  
  }

  // interpolate
  mpfr_chebpoly_interpolate(P, F);

  // clear variables
  mpfr_clear(t);
  mpfr_vec_clear(F);
}


int main()
{

  mpfr_prec_t prec = 53;
  mpfr_set_default_prec(prec);

  gmp_randstate_t randstate;
  gmp_randinit_default(randstate);
  gmp_randseed_ui(randstate, time(NULL));
  srand(time(NULL));

  long max_degree = 10;
  long max_coeff = 100;
  long i;

  mpfr_chebpoly_t P, Q;
  mpfr_chebpoly_init(P);
  mpfr_chebpoly_init(Q);

  mpfr_chebpoly_set_degree(Q, rand() % (max_degree + 1));
  for (i = 0 ; i <= Q->degree ; i++) {
    mpfr_urandomb(Q->coeffs + i, randstate);
    mpfr_mul_si(Q->coeffs + i, Q->coeffs + i, 2*max_coeff, MPFR_RNDN);
    mpfr_sub_si(Q->coeffs + i, Q->coeffs + i, max_coeff, MPFR_RNDN);
  }


  interpolate_poly(P, Q, max_degree+1);

  mpfr_chebpoly_sub(P, P, Q);

  mpfr_t err;
  mpfr_init(err);
  mpfr_chebpoly_1norm(err, P);

  printf("\nerr = %.10e\n", mpfr_get_d(err, MPFR_RNDN));

  return 0;

}



