
/* Linearized dynamics with J2 */


#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include <gmp.h>
#include <mpfr.h>
#include <mpfi.h>
#include <mpfi_io.h>

#include "chebmodel_vec_intop_newton.h"


// t0, tf, n useless because constant matrix
void kepler_matrix(chebmodel_ptr_ptr A, chebmodel_vec_t D, const mpfi_t t0, const mpfi_t tf, long n, const mpfi_t mu_earth, const mpfi_t a_sk, const mpfi_t omega_earth)
{
  // 0 par défaut
  long i, j;
  for (i = 0 ; i < 6 ; i++)
    for (j = 0 ; j < 6 ; j++)
      chebmodel_zero(A[i] + j);
  chebmodel_vec_set_dim(D, 6);
  chebmodel_vec_zero(D);
  mpfi_t temp;
  mpfi_init(temp);

  // coefficient of A
  mpfi_sqr(temp, a_sk);
  //mpfi_sqr(temp, temp);
  mpfi_mul(temp, temp, a_sk);
  mpfi_div(temp, mu_earth, temp);
  mpfi_sqrt(temp, temp);
  mpfi_mul_si(temp, temp, -3);
  mpfi_mul_2si(temp, temp, -1);
  mpfi_chebpoly_set_coeff_fi(A[5][0].poly, 0, temp);

  // coefficient of D
  mpfi_sqr(temp, a_sk);
  mpfi_mul(temp, temp, a_sk);
  mpfi_div(temp, mu_earth, temp);
  mpfi_sqrt(temp, temp);
  mpfi_sub(temp, temp, omega_earth);
  mpfi_chebpoly_set_coeff_fi(D->poly[5].poly, 0, temp);

  // rescale A and D
  mpfi_sub(temp, tf, t0);
  mpfi_mul_2si(temp, temp, -1);
  for (i = 0 ; i < 6 ; i++)
    for (j = 0 ; j < 6 ; j++)
      chebmodel_scalar_mul_fi(A[i] + j, A[i] + j, temp);
  chebmodel_vec_scalar_mul_fi(D, D, temp);

  // clear variables
  mpfi_clear(temp);
}


void Sin_Cos(chebmodel_t Sin, chebmodel_t Cos, const mpfi_t omega, const mpfi_t phi, const mpfi_t t0, const mpfi_t tf, long n)
{
  mpfi_t scal, temp;
  mpfi_init(scal); mpfi_init(temp);

  // LODE structure
  chebmodel_lode_t L;
  chebmodel_lode_init(L);
  chebmodel_lode_set_order(L, 2);
  mpfi_chebpoly_set_degree(L->a[0].poly, 0);
  mpfi_sub(scal, tf, t0);
  mpfi_mul_2si(scal, scal, -1);
  mpfi_mul(scal, scal, omega); // scal = omega * (tf - t0) / 2
  mpfi_sqr(L->a[0].poly->coeffs + 0, scal);

  chebmodel_t RHS;
  chebmodel_init(RHS); // RHS = 0
  __mpfi_struct IV[2];
  mpfi_init(IV + 0); mpfi_init(IV + 1);
  mpfi_mul(temp, omega, t0);
  mpfi_add(temp, temp, phi);
  mpfi_sin(IV + 0, temp); // IV[0] = sin(omega*t0+phi)
  mpfi_cos(IV + 1, temp);
  mpfi_mul(IV + 1, IV + 1, scal); // IV[1] = omega*(tf-t0)/2 * cos(omega*t0+phi)

  chebmodel_struct Sols[3];
  chebmodel_init(Sols + 0); chebmodel_init(Sols + 1); chebmodel_init(Sols + 2);
  chebmodel_lode_intop_newton_solve_fr(Sols, L, RHS, IV, n);

  // extract Sin
  mpfi_sqr(temp, scal);
  mpfi_neg(temp, temp);
  chebmodel_scalar_div_fi(Sin, Sols + 2, temp);

  // change IV for cos
  mpfi_mul(temp, omega, t0);
  mpfi_add(temp, temp, phi);
  mpfi_cos(IV + 0, temp); // IV[0] = cos(omega*t0+phi)
  mpfi_sin(IV + 1, temp);
  mpfi_neg(IV + 1, IV + 1);
  mpfi_mul(IV + 1, IV + 1, scal); // IV[1] = -omega*(tf-t0)/2 * sin(omega*t0+phi)

  chebmodel_lode_intop_newton_solve_fr(Sols, L, RHS, IV, n);

  // extract Cos
  mpfi_sqr(temp, scal);
  mpfi_neg(temp, temp);
  chebmodel_scalar_div_fi(Cos, Sols + 2, temp);


  // clear variables
  mpfi_clear(scal); mpfi_clear(temp);
  chebmodel_lode_clear(L); chebmodel_clear(RHS);
  mpfi_clear(IV + 0); mpfi_clear(IV + 1);
  chebmodel_clear(Sols + 0); chebmodel_clear(Sols + 1); chebmodel_clear(Sols + 2);
}



// J2 perturbation dynamics
void J2_matrix(chebmodel_ptr_ptr A, chebmodel_vec_t D, const mpfi_t t0, const mpfi_t tf, long n, const mpfi_t a_sk, const mpfi_t mu_earth, const mpfi_t theta0, const mpfi_t omega_earth, const mpfi_t lMTheta_sk, const mpfi_t alpha20)
{
  long i, j;
 
  // trigonemetric quantities
  chebmodel_t S, C;
  chebmodel_init(S); chebmodel_init(C);
  mpfi_t phi, temp;
  mpfi_init(phi); mpfi_init(temp);
  mpfi_add(phi, lMTheta_sk, theta0);
  Sin_Cos(S, C, omega_earth, phi, t0, tf, n);
  chebmodel_t S2, C2, SC;
  chebmodel_init(S2); chebmodel_init(C2); chebmodel_init(SC);
  chebmodel_mul(S2, S, S); chebmodel_truncate(S2, n);
  chebmodel_mul(C2, C, C); chebmodel_truncate(C2, n);
  chebmodel_mul(SC, S, C); chebmodel_truncate(SC, n);

  // coefficients of A
  
  // A11
  chebmodel_zero(A[0] + 0);

  // A12
  mpfi_mul_si(temp, a_sk, -6);
  chebmodel_scalar_mul_fi(A[0] + 1, S, temp);

  // A13
  mpfi_mul_si(temp, a_sk, 6);
  chebmodel_scalar_mul_fi(A[0] + 2, C, temp);

  // A14
  chebmodel_zero(A[0] + 3);

  // A15
  chebmodel_zero(A[0] + 4);

  // A16
  chebmodel_zero(A[0] + 5);

  // A21
  mpfi_mul_si(temp, a_sk, 21);
  mpfi_mul_2si(temp, temp, -1);
  chebmodel_scalar_mul_fi(A[1] + 0, S, temp);

  // A22
  mpfi_mul_si(temp, a_sk, -6);
  chebmodel_scalar_mul_fi(A[1] + 1, SC, temp);

  // A23
  mpfi_mul_si(temp, a_sk, -6);
  chebmodel_scalar_mul_fi(A[1] + 2, S2, temp);
  mpfi_add(A[1][2].poly->coeffs + 0, A[1][2].poly->coeffs + 0, temp);

  // A24
  chebmodel_zero(A[1] + 3);

  // A25
  chebmodel_zero(A[1] + 4);

  // A26
  mpfi_mul_si(temp, a_sk, -3);
  chebmodel_scalar_mul_fi(A[1] + 5, C, temp);

  // A31
  mpfi_mul_si(temp, a_sk, -21);
  mpfi_mul_2si(temp, temp, -1);
  chebmodel_scalar_mul_fi(A[2] + 0, C, temp);

  // A32
  mpfi_mul_si(temp, a_sk, 6);
  chebmodel_scalar_mul_fi(A[2] + 1, C2, temp);
  mpfi_add(A[2][1].poly->coeffs + 0, A[2][1].poly->coeffs + 0, temp);

  // A33
  mpfi_mul_si(temp, a_sk,  6);
  chebmodel_scalar_mul_fi(A[2] + 2, SC, temp);

  // A34
  chebmodel_zero(A[2] + 3);

  // A35
  chebmodel_zero(A[2] + 4);

  // A36
  mpfi_mul_si(temp, a_sk, -3);
  chebmodel_scalar_mul_fi(A[2] + 5, S, temp);

  // A41
  chebmodel_zero(A[3] + 0);

  // A42
  chebmodel_zero(A[3] + 1);

  // A43
  chebmodel_zero(A[3] + 2);

  // A44
  mpfi_mul_si(temp, a_sk, -6);
  chebmodel_scalar_mul_fi(A[3] + 3, SC, temp);

  // A45
  mpfi_mul_si(temp, a_sk, 6);
  chebmodel_scalar_mul_fi(A[3] + 4, C2, temp);

  // A46
  chebmodel_zero(A[3] + 5);

  // A51
  chebmodel_zero(A[4] + 0);

  // A52
  chebmodel_zero(A[4] + 1);

  // A53
  chebmodel_zero(A[4] + 2);

  // A54
  mpfi_mul_si(temp, a_sk, -6);
  chebmodel_scalar_mul_fi(A[4] + 3, S2, temp);

  // A55
  mpfi_mul_si(temp, a_sk, 6);
  chebmodel_scalar_mul_fi(A[4] + 4, SC, temp);

  // A56
  chebmodel_zero(A[4] + 5);

  // A61
  mpfi_mul_si(temp, a_sk, -21);
  chebmodel_zero(A[5] + 0);
  mpfi_chebpoly_set_degree(A[5][0].poly, 0);
  mpfi_set(A[5][0].poly->coeffs + 0, temp);

  // A62
  mpfi_mul_si(temp, a_sk, 39);
  mpfi_mul_2si(temp, temp, -1);
  chebmodel_scalar_mul_fi(A[5] + 1, C, temp);

  // A63
  mpfi_mul_si(temp, a_sk, 39);
  mpfi_mul_2si(temp, temp, -1);
  chebmodel_scalar_mul_fi(A[5] + 2, S, temp);

  // A64
  chebmodel_zero(A[5] + 3);

  // A65
  chebmodel_zero(A[5] + 4);

  // A66
  chebmodel_zero(A[5] + 5);

  // factor in front of matrix A
  mpfi_sqr(temp, a_sk);
  mpfi_sqr(temp, temp);
  mpfi_sqr(temp, temp);
  mpfi_mul(temp, temp, a_sk);
  mpfi_mul(temp, temp, mu_earth);
  mpfi_sqrt(temp, temp);
  mpfi_div(temp, alpha20, temp);
  for (i = 0 ; i < 6 ; i++)
    for (j = 0 ; j < 6 ; j++)
      chebmodel_scalar_mul_fi(A[i] + j, A[i] + j, temp);
 

  // coefficients of D
  chebmodel_vec_set_dim(D, 6);
  chebmodel_vec_zero(D);
  mpfi_sqr(temp, a_sk);
  mpfi_mul(temp, temp, a_sk);
  mpfi_sqr(temp, temp);
  mpfi_mul(temp, temp, a_sk);
  mpfi_mul(temp, temp, mu_earth);
  mpfi_sqrt(temp, temp);
  mpfi_div(temp, alpha20, temp);
  mpfi_mul_si(temp, temp, 3);
  mpfi_chebpoly_set_degree(D->poly[5].poly, 0);
  mpfi_mul_2si(D->poly[5].poly->coeffs + 0, temp, 1);
  chebmodel_scalar_mul_fi(D->poly + 2, C, temp);
  mpfi_neg(temp, temp);
  chebmodel_scalar_mul_fi(D->poly + 1, S, temp);


  // rescale A and D with (tf-t0)/2
  mpfi_sub(temp, tf, t0);
  mpfi_mul_2si(temp, temp, -1);
  for (i = 0 ; i < 6 ; i++)
    for (j = 0 ; j < 6 ; j++)
      chebmodel_scalar_mul_fi(A[i] + j, A[i] + j, temp);
  chebmodel_vec_scalar_mul_fi(D, D, temp);


  // clear variables
  chebmodel_clear(S); chebmodel_clear(C);
  mpfi_clear(phi); mpfi_clear(temp);
  chebmodel_clear(S2); chebmodel_clear(C2); chebmodel_clear(SC);

}





int main()
{

  mpfr_prec_t prec = 100;
  mpfr_set_default_prec(prec);

  // physical quantities
  mpfi_t a_sk, mu_earth, theta0, omega_earth, lMTheta_sk, alpha20, r_earth, c20;
  mpfi_init(a_sk); mpfi_init(mu_earth); mpfi_init(theta0); mpfi_init(omega_earth);
  mpfi_init(lMTheta_sk); mpfi_init(alpha20); mpfi_init(r_earth); mpfi_init(c20);
  mpfi_set_si(a_sk, 421658); mpfi_div_si(a_sk, a_sk, 10);
  mpfi_set_si(mu_earth, 29755); mpfi_mul_d(mu_earth, mu_earth, pow(10, 11));
  mpfi_set_si(theta0, 17579); mpfi_div_si(theta0, theta0, 10000);
  mpfi_set_si(omega_earth, 63004); mpfi_div_si(omega_earth, omega_earth, 10000);
  mpfi_set_si(lMTheta_sk, 206); mpfi_div_si(lMTheta_sk, lMTheta_sk, 100);
  mpfi_set_si(r_earth, 6378137); mpfi_div_si(r_earth, r_earth, 1000);
  mpfi_set_si(c20, -1083); mpfi_div_si(c20, c20, 1000000);
  mpfi_sqr(alpha20, r_earth);
  mpfi_mul(alpha20, alpha20, mu_earth);
  mpfi_mul(alpha20, alpha20, c20);
  mpfi_mul_2si(alpha20, alpha20, -1);
  
  // time of interest
  mpfi_t t0, tf;
  mpfi_init(t0); mpfi_init(tf);
  mpfi_set_si(t0, 0);
  mpfi_set_si(tf, 7);

  long i, j;
  

  // matrix and rhs for LODE
  long p = 70; // approximation degree of coefficients // 45
  chebmodel_ptr_ptr A_Kepler = malloc(6 * sizeof(chebmodel_ptr));
  chebmodel_ptr_ptr A_J2 = malloc(6 * sizeof(chebmodel_ptr));
  chebmodel_ptr_ptr A = malloc(6 * sizeof(chebmodel_ptr));
  for (i = 0 ; i < 6 ; i++) {
    A_Kepler[i] = malloc(6 * sizeof(chebmodel_t));
    A_J2[i] = malloc(6 * sizeof(chebmodel_t));
    A[i] = malloc(6 * sizeof(chebmodel_t));
    for (j = 0 ; j < 6 ; j++) {
      chebmodel_init(A_Kepler[i] + j);
      chebmodel_init(A_J2[i] + j);
      chebmodel_init(A[i] + j);
    }
  }
  chebmodel_vec_t D_Kepler, D_J2, D;
  chebmodel_vec_init(D_Kepler);
  chebmodel_vec_init(D_J2);
  chebmodel_vec_init(D);

  kepler_matrix(A_Kepler, D_Kepler, t0, tf, p, mu_earth, a_sk, omega_earth);
  J2_matrix(A_J2, D_J2, t0, tf, p, a_sk, mu_earth, theta0, omega_earth, lMTheta_sk, alpha20);

  // add them
  for (i = 0 ; i < 6 ; i++)
    for (j = 0 ; j < 6 ; j++)
      chebmodel_add(A[i] + j, A_Kepler[i] + j, A_J2[i] + j);
  chebmodel_vec_add(D, D_Kepler, D_J2);

  // error of the coefficients
  printf("errors of the coefficients:\n");
  for (i = 0 ; i < 6 ; i++)
    for (j = 0 ; j < 6 ; j++)
      printf("[%ld,%ld] %.5e\n", i, j, mpfr_get_d(A[i][j].rem, MPFR_RNDN));
  printf("\n");


  // plot coeffs A and D in file J2_plot_coeffs.dat
  printf("Plotting coeffs of Aand D in J2_plot_coeffs.dat...\n");
  FILE * J2_plot_coeffs = fopen("J2_plot_coeffs.dat", "w+");
  double tt0 = mpfi_get_d(t0);
  double ttf = mpfi_get_d(tf);
  double tt, value;
  double scal_d = (ttf-tt0)/2;
  double_chebpoly_t PP;
  double_chebpoly_init(PP);

  long n_points = 1000;
  long k;
  fprintf(J2_plot_coeffs, "#t");
  for (i = 0 ; i < 6 ; i++) {
    for (j = 0 ; j < 6 ; j++)
      fprintf(J2_plot_coeffs, "\tA[%ld,%ld](t)", i, j);
    fprintf(J2_plot_coeffs, "\tD[%ld](t)", i);
  }
  fprintf(J2_plot_coeffs, "\n\n");

  for (k = 0 ; k < n_points ; k++) {
    
    tt = k*2/((double) (n_points-1))-1;
    fprintf(J2_plot_coeffs, "%.10e", (1-tt)/2*tt0 + (1+tt)/2*ttf);

    for (i = 0 ; i < 6 ; i++) {
      for (j = 0 ; j < 6 ; j++) {
	chebmodel_get_double_chebpoly(PP, A[i] + j);
        double_chebpoly_evaluate_d(&value, PP, &tt);
	fprintf(J2_plot_coeffs, "\t%.10e", value/scal_d);
      }
      chebmodel_get_double_chebpoly(PP, D->poly + i);
      double_chebpoly_evaluate_d(&value, PP, &tt);
      fprintf(J2_plot_coeffs, "\t%.10e", value/scal_d);
    }

    fprintf(J2_plot_coeffs, "\n");
  }
	
  fclose(J2_plot_coeffs);


  // create LODE
  chebmodel_vec_lode_t L;
  chebmodel_vec_lode_init(L);
  chebmodel_vec_lode_set_order_dim(L, 1, 6);
  for (i = 0 ; i < 6 ; i++)
    for (j = 0 ; j < 6 ; j++)
      chebmodel_neg(L->A[0][i] + j, A[i] + j);

  // initial conditions
  mpfi_ptr_ptr I = malloc(6 * sizeof(mpfi_ptr));
  for (i = 0 ; i < 6 ; i++) {
    I[i] = malloc(1 * sizeof(mpfi_t));
    mpfi_init(I[i] + 0);
  }
  mpfr_ptr_ptr Ifr = malloc(6 * sizeof(mpfr_ptr));
  for (i = 0 ; i < 6 ; i++) {
    Ifr[i] = malloc(1 * sizeof(mpfr_t));
    mpfr_init(Ifr[i] + 0);
  }

  // integral operator
  chebmodel_vec_intop_t K;
  chebmodel_vec_intop_init(K);
  chebmodel_vec_intop_set_lode(K, L);
  mpfr_chebpoly_vec_intop_t Kfr;
  mpfr_chebpoly_vec_intop_init(Kfr);
  chebmodel_vec_intop_get_mpfr_chebpoly_vec_intop(Kfr, K);
  chebmodel_vec_t RHS;
  chebmodel_vec_init(RHS);
  mpfr_chebpoly_vec_t RHS_fr;
  mpfr_chebpoly_vec_init(RHS_fr);

  // contracting op
  mpfr_ptr_ptr T_norm = malloc(6 * sizeof(mpfr_ptr));
  for (i = 0 ; i < 6 ; i++) {
    T_norm[i] = malloc(6 * sizeof(mpfr_t));
    for (j = 0 ; j < 6 ; j++)
      mpfr_init(T_norm[i] + j);
  }
  mpfr_bandmatrix_struct ** M_K_inv = malloc(6 * sizeof(mpfr_bandmatrix_struct*));
  for (i = 0 ; i < 6 ; i++) {
    M_K_inv[i] = malloc(6 * sizeof(mpfr_bandmatrix_t));
    for (j = 0 ; j < 6 ; j++)
      mpfr_bandmatrix_init(M_K_inv[i] + j);
  }
  long init_N = 200; // 250
  chebmodel_vec_intop_newton_contract_fr(T_norm, M_K_inv, K, init_N); // !!!!

  // solve LODE
  long n = 100; // approximation degree for the solution - 300
  chebmodel_vec_t Sol;
  chebmodel_vec_init(Sol);
  mpfr_chebpoly_vec_t Sol_fr;
  mpfr_chebpoly_vec_init(Sol_fr);
  mpfr_ptr bound = malloc(6 * sizeof(mpfr_t));
  for (i = 0 ; i < 6 ; i++)
    mpfr_init(bound + i);

  // transition matrix
  chebmodel_ptr_ptr Phi = malloc(6 * sizeof(chebmodel_ptr));
  for (i = 0 ; i < 6 ; i++) {
    Phi[i] = malloc(7 * sizeof(chebmodel_t));
    for (j = 0 ; j <= 6 ; j++)
      chebmodel_init(Phi[i] + j);
  }

  chebmodel_vec_t Const;
  chebmodel_vec_init(Const);
  chebmodel_vec_set_dim(Const, 6);
  for (i = 0 ; i < 6 ; i++)
    mpfi_chebpoly_set_degree(Const->poly[i].poly, 0);

  for (j = 0 ; j <= 6 ; j++) {fprintf(stderr, "<j=%ld>\n", j);
  
    // canonical initial conditions and rhs
    if (j < 6) {
      for (i = 0 ; i < 6 ; i++) {
        mpfi_set_si(I[i] + 0, i==j);
        mpfr_set_si(Ifr[i] + 0, i==j, MPFR_RNDN);
      }
      chebmodel_vec_intop_initvals(RHS, L, I);
    }
    else {
      for (i = 0 ; i < 6 ; i++) {
        mpfi_set_si(I[i] + 0, 0);
	mpfr_set_si(Ifr[i] + 0, 0, MPFR_RNDN);
      }
      chebmodel_vec_intop_rhs(RHS, L, D, I);
    }
    chebmodel_vec_get_mpfr_chebpoly_vec(RHS_fr, RHS);

    // approx solve and validation
    mpfr_chebpoly_vec_intop_approxsolve(Sol_fr, Kfr, RHS_fr, n);
    chebmodel_vec_set_mpfr_chebpoly_vec(Sol, Sol_fr);
    chebmodel_vec_intop_newton_validate_sol_aux_fr(bound, K, T_norm, M_K_inv, RHS, Sol_fr); // !!!!
    for (i = 0 ; i < 6 ; i++) {
      mpfr_set(Sol->poly[i].rem, bound + i, MPFR_RNDU);
      mpfi_set(Const->poly[i].poly->coeffs + 0, I[i] + 0);
    }
    chebmodel_vec_antiderivative_1(Sol, Sol);
    chebmodel_vec_add(Sol, Sol, Const);

    for (i = 0 ; i < 6 ; i++) {
      chebmodel_set(Phi[i] + j, Sol->poly + i);
      chebmodel_1norm_ubound(bound + i, Phi[i] + j);
      fprintf(stderr, "[j=%ld, i=%ld, err=%.5e, 1norm=%.5e]\n", j, i, mpfr_get_d(Phi[i][j].rem, MPFR_RNDU), mpfr_get_d(bound + i, MPFR_RNDU));
    }


  fprintf(stderr, "</j=%ld>\n", j);
  }

  // print degrees and errors for Phi
  printf("\nDegrees and errors:\n\n");
  for (i = 0 ; i < 6 ; i++) {
    for (j = 0 ; j <= 6 ; j++)
      printf("[%ld -- %.5e]\t", Phi[i][j].poly->degree, mpfr_get_d(Phi[i][j].rem, MPFR_RNDU));
    printf("\n");
  }
  printf("\n");


  // plot Phi in file
  printf("Plotting in J2_plot.dat...\n");
  FILE * J2_plot = fopen("J2_plot.dat", "w+");
  tt0 = mpfi_get_d(t0);
  ttf = mpfi_get_d(tf);

  n_points = 1000;
  fprintf(J2_plot, "#t");
  for (i = 0 ; i < 6 ; i++)
    for (j = 0 ; j <= 6 ; j++)
      fprintf(J2_plot, "\tPhi[%ld,%ld](t)", i, j);
  fprintf(J2_plot, "\n\n");

  for (k = 0 ; k < n_points ; k++) {
    
    tt = k*2/((double) (n_points-1))-1;
    fprintf(J2_plot, "%.10e", (1-tt)/2*tt0 + (1+tt)/2*ttf);

    for (i = 0 ; i < 6 ; i++) 
      for (j = 0 ; j <= 6 ; j++) {
	chebmodel_get_double_chebpoly(PP, Phi[i] + j);
        double_chebpoly_evaluate_d(&value, PP, &tt);
	fprintf(J2_plot, "\t%.10e", value);
      }

    fprintf(J2_plot, "\n");

  }
	
  fclose(J2_plot);

  // optimal degrees
  printf("Truncating at optimal degrees...\n");

  // maximal error accepted
  double e7 = pow(10, -7);
  double e10 = pow(10, -10);
  double e11 = pow(10, -11);
  //e7 = pow(10,-2); e10 = e7; e11 = e7;
  double err_max[6][7] = {
    {e10, e7, e10, e10, e10, e7, e11},
    {e7, e7, e7, e7, e7, e7, e11},
    {e10, e7, e10, e10, e10, e7, e11},
    {e10, e7, e10, e11, e11, e7, e11},
    {e10, e7, e10, e11, e11, e7, e11},
    {e7, e7, e7, e7, e7, e7, e11}
  };

  // transition matrix with optimal degrees
  chebmodel_ptr_ptr PhiOpt = malloc(6 * sizeof(chebmodel_ptr));
  for (i = 0 ; i < 6 ; i++) {
    PhiOpt[i] = malloc(7 * sizeof(chebmodel_t));
    for (j = 0 ; j <= 6 ; j++)
      chebmodel_init(PhiOpt[i] + j);
  }

  chebmodel_t P1, P2;
  chebmodel_init(P1); chebmodel_init(P2);
  long deg;

  for (i = 0 ; i < 6 ; i++)
    for (j = 0 ; j <= 6 ; j++) {
      chebmodel_set(P1, Phi[i] + j);
      deg = P1->poly->degree;
      chebmodel_set(P2, P1);
      chebmodel_truncate(P2, deg-1);
      while ((mpfr_cmp_d(P2->rem, err_max[i][j]) <= 0) && (deg >= 0)) {
        chebmodel_set(P1, P2);
        chebmodel_truncate(P2, deg-1);
        deg--;
      }
      chebmodel_set(PhiOpt[i] + j, P1);
    }

  // print degrees and errors for PhiOpt
  printf("\nDegrees and errors for optimal degrees:\n\n");
  for (i = 0 ; i < 6 ; i++) {
    for (j = 0 ; j <= 6 ; j++)
      printf("[%ld -- %.5e]\t", PhiOpt[i][j].poly->degree, mpfr_get_d(PhiOpt[i][j].rem, MPFR_RNDU));
    printf("\n");
  }
  printf("\n");


  // plot PhiOpt in file
  printf("Plotting in J2_plot_opt.dat...\n");
  FILE * J2_plot_opt = fopen("J2_plot_opt.dat", "w+");

  fprintf(J2_plot_opt, "#t");
  for (i = 0 ; i < 6 ; i++)
    for (j = 0 ; j <= 6 ; j++)
      fprintf(J2_plot_opt, "\tPhiOpt[%ld,%ld](t)", i, j);
  fprintf(J2_plot_opt, "\n\n");

  for (k = 0 ; k < n_points ; k++) {
    
    tt = k*2/((double) (n_points-1))-1;
    fprintf(J2_plot_opt, "%.10e", (1-tt)/2*tt0 + (1+tt)/2*ttf);

    for (i = 0 ; i < 6 ; i++) 
      for (j = 0 ; j <= 6 ; j++) {
	chebmodel_get_double_chebpoly(PP, PhiOpt[i] + j);
        double_chebpoly_evaluate_d(&value, PP, &tt);
	fprintf(J2_plot_opt, "\t%.10e", value);
      }

    fprintf(J2_plot_opt, "\n");

  }
	
  fclose(J2_plot_opt);









  return 0;

}



