
/* Linearized dynamics with J2 */


#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include <gmp.h>
#include <mpfr.h>
#include <mpfi.h>
#include <mpfi_io.h>

#include "chebmodel_vec_intop_newton.h"


// t0, tf, n useless because constant matrix
void kepler_matrix(chebmodel_ptr_ptr A, chebmodel_vec_t D, const mpfi_t t0, const mpfi_t tf, long n, const mpfi_t mu_earth, const mpfi_t a_sk, const mpfi_t omega_earth)
{
  // 0 par défaut
  long i, j;
  for (i = 0 ; i < 6 ; i++)
    for (j = 0 ; j < 6 ; j++)
      chebmodel_zero(A[i] + j);
  chebmodel_vec_set_dim(D, 6);
  chebmodel_vec_zero(D);
  mpfi_t temp;
  mpfi_init(temp);

  // coefficient of A
  mpfi_sqr(temp, a_sk);
  mpfi_sqr(temp, temp);
  mpfi_mul(temp, temp, a_sk);
  mpfi_div(temp, omega_earth, temp);
  mpfi_sqrt(temp, temp);
  mpfi_mul_si(temp, temp, -3);
  mpfi_mul_2si(temp, temp, -1);
  mpfi_chebpoly_set_coeff_fi(A[5][0].poly, 0, temp);

  // coefficient of D
  mpfi_sqr(temp, a_sk);
  mpfi_mul(temp, temp, a_sk);
  mpfi_div(temp, mu_earth, temp);
  mpfi_sqrt(temp, temp);
  mpfi_sub(temp, temp, omega_earth);
  mpfi_chebpoly_set_coeff_fi(D->poly[5].poly, 0, temp);

  // rescale A and D
  mpfi_sub(temp, tf, t0);
  mpfi_mul_2si(temp, temp, -1);
  for (i = 0 ; i < 6 ; i++)
    for (j = 0 ; j < 6 ; j++)
      chebmodel_scalar_mul_fi(A[i] + j, A[i] + j, temp);
  chebmodel_vec_scalar_mul_fi(D, D, temp);

  // clear variables
  mpfi_clear(temp);
}


void Sin_Cos(chebmodel_t Sin, chebmodel_t Cos, const mpfi_t omega, const mpfi_t phi, const mpfi_t t0, const mpfi_t tf, long n)
{
  mpfi_t scal, temp;
  mpfi_init(scal); mpfi_init(temp);

  // LODE structure
  chebmodel_lode_t L;
  chebmodel_lode_init(L);
  chebmodel_lode_set_order(L, 2);
  mpfi_chebpoly_set_degree(L->a[0].poly, 0);
  mpfi_sub(scal, tf, t0);
  mpfi_mul_2si(scal, scal, -1);
  mpfi_mul(scal, scal, omega); // scal = omega * (tf - t0) / 2
  mpfi_sqr(L->a[0].poly->coeffs + 0, scal);

  chebmodel_t RHS;
  chebmodel_init(RHS); // RHS = 0
  __mpfi_struct IV[2];
  mpfi_init(IV + 0); mpfi_init(IV + 1);
  mpfi_mul(temp, omega, t0);
  mpfi_add(temp, temp, phi);
  mpfi_sin(IV + 0, temp); // IV[0] = sin(omega*t0+phi)
  mpfi_cos(IV + 1, temp);
  mpfi_mul(IV + 1, IV + 1, scal); // IV[1] = omega*(tf-t0)/2 * cos(omega*t0+phi)

  chebmodel_struct Sols[3];
  chebmodel_init(Sols + 0); chebmodel_init(Sols + 1); chebmodel_init(Sols + 2);
  chebmodel_lode_intop_newton_solve_fr(Sols, L, RHS, IV, n);

  // extract Sin
  mpfi_sqr(temp, scal);
  mpfi_neg(temp, temp);
  chebmodel_scalar_div_fi(Sin, Sols + 2, temp);

  // change IV for cos
  mpfi_mul(temp, omega, t0);
  mpfi_add(temp, temp, phi);
  mpfi_cos(IV + 0, temp); // IV[0] = cos(omega*t0+phi)
  mpfi_sin(IV + 1, temp);
  mpfi_neg(IV + 1, IV + 1);
  mpfi_mul(IV + 1, IV + 1, scal); // IV[1] = -omega*(tf-t0)/2 * sin(omega*t0+phi)

  chebmodel_lode_intop_newton_solve_fr(Sols, L, RHS, IV, n);

  // extract Cos
  mpfi_sqr(temp, scal);
  mpfi_neg(temp, temp);
  chebmodel_scalar_div_fi(Cos, Sols + 2, temp);


  // clear variables
  mpfi_clear(scal); mpfi_clear(temp);
  chebmodel_lode_clear(L); chebmodel_clear(RHS);
  mpfi_clear(IV + 0); mpfi_clear(IV + 1);
  chebmodel_clear(Sols + 0); chebmodel_clear(Sols + 1); chebmodel_clear(Sols + 2);
}



// J2 perturbation dynamics
void J2_matrix(chebmodel_ptr_ptr A, chebmodel_vec_t D, const mpfi_t t0, const mpfi_t tf, long n, const mpfi_t a_sk, const mpfi_t mu_earth, const mpfi_t theta0, const mpfi_t omega_earth, const mpfi_t lMTheta_sk, const mpfi_t alpha20)
{
  long i, j;
 
  // trigonemetric quantities
  chebmodel_t S, C;
  chebmodel_init(S); chebmodel_init(C);
  mpfi_t phi, temp;
  mpfi_init(phi); mpfi_init(temp);
  mpfi_add(phi, lMTheta_sk, theta0);
  Sin_Cos(S, C, omega_earth, phi, t0, tf, n);
  chebmodel_t S2, C2, SC;
  chebmodel_init(S2); chebmodel_init(C2); chebmodel_init(SC);
  chebmodel_mul(S2, S, S);
  chebmodel_mul(C2, C, C);
  chebmodel_mul(SC, S, C);

  // coefficients of A
  
  // A11
  chebmodel_zero(A[0] + 0);

  // A12
  mpfi_mul_si(temp, a_sk, -6);
  chebmodel_scalar_mul_fi(A[0] + 1, S, temp);

  // A13
  mpfi_mul_si(temp, a_sk, 6);
  chebmodel_scalar_mul_fi(A[0] + 2, C, temp);

  // A14
  chebmodel_zero(A[0] + 3);

  // A15
  chebmodel_zero(A[0] + 4);

  // A16
  chebmodel_zero(A[0] + 5);

  // A21
  mpfi_mul_si(temp, a_sk, 21);
  mpfi_mul_2si(temp, temp, -1);
  chebmodel_scalar_mul_fi(A[1] + 0, S, temp);

  // A22
  mpfi_mul_si(temp, a_sk, -6);
  chebmodel_scalar_mul_fi(A[1] + 1, SC, temp);

  // A23
  mpfi_mul_si(temp, a_sk, -6);
  chebmodel_scalar_mul_fi(A[1] + 2, S2, temp);
  mpfi_add(A[1][2].poly->coeffs + 0, A[1][2].poly->coeffs + 0, temp);

  // A24
  chebmodel_zero(A[1] + 3);

  // A25
  chebmodel_zero(A[1] + 4);

  // A26
  mpfi_mul_si(temp, a_sk, -3);
  chebmodel_scalar_mul_fi(A[1] + 5, C, temp);

  // A31
  mpfi_mul_si(temp, a_sk, -21);
  mpfi_mul_2si(temp, temp, -1);
  chebmodel_scalar_mul_fi(A[2] + 0, C, temp);

  // A32
  mpfi_mul_si(temp, a_sk, 6);
  chebmodel_scalar_mul_fi(A[2] + 1, C2, temp);
  mpfi_add(A[2][1].poly->coeffs + 0, A[2][1].poly->coeffs + 0, temp);

  // A33
  mpfi_mul_si(temp, a_sk,  6);
  chebmodel_scalar_mul_fi(A[2] + 2, SC, temp);

  // A34
  chebmodel_zero(A[2] + 3);

  // A35
  chebmodel_zero(A[2] + 4);

  // A36
  mpfi_mul_si(temp, a_sk, -3);
  chebmodel_scalar_mul_fi(A[2] + 5, S, temp);

  // A41
  chebmodel_zero(A[3] + 0);

  // A42
  chebmodel_zero(A[3] + 1);

  // A43
  chebmodel_zero(A[3] + 2);

  // A44
  mpfi_mul_si(temp, a_sk, -6);
  chebmodel_scalar_mul_fi(A[3] + 3, SC, temp);

  // A45
  mpfi_mul_si(temp, a_sk, 6);
  chebmodel_scalar_mul_fi(A[3] + 4, C2, temp);

  // A46
  chebmodel_zero(A[3] + 5);

  // A51
  chebmodel_zero(A[4] + 0);

  // A52
  chebmodel_zero(A[4] + 1);

  // A53
  chebmodel_zero(A[4] + 2);

  // A54
  mpfi_mul_si(temp, a_sk, -6);
  chebmodel_scalar_mul_fi(A[4] + 3, S2, temp);

  // A55
  mpfi_mul_si(temp, a_sk, 6);
  chebmodel_scalar_mul_fi(A[4] + 4, SC, temp);

  // A56
  chebmodel_zero(A[4] + 5);

  // A61
  mpfi_mul_si(temp, a_sk, -21);
  chebmodel_zero(A[5] + 0);
  mpfi_chebpoly_set_degree(A[5][0].poly, 0);
  mpfi_set(A[5][0].poly->coeffs + 0, temp);

  // A62
  mpfi_mul_si(temp, a_sk, 39);
  mpfi_mul_2si(temp, temp, -1);
  chebmodel_scalar_mul_fi(A[5] + 1, C, temp);

  // A63
  mpfi_mul_si(temp, a_sk, 39);
  mpfi_mul_2si(temp, temp, -1);
  chebmodel_scalar_mul_fi(A[5] + 2, S, temp);

  // A64
  chebmodel_zero(A[5] + 3);

  // A65
  chebmodel_zero(A[5] + 4);

  // A66
  chebmodel_zero(A[5] + 5);

  // factor in front of matrix A
  mpfi_sqr(temp, a_sk);
  mpfi_sqr(temp, temp);
  mpfi_sqr(temp, temp);
  mpfi_mul(temp, temp, a_sk);
  mpfi_mul(temp, temp, mu_earth);
  mpfi_sqrt(temp, temp);
  mpfi_div(temp, alpha20, temp);
  for (i = 0 ; i < 6 ; i++)
    for (j = 0 ; j < 6 ; j++)
      chebmodel_scalar_mul_fi(A[i] + j, A[i] + j, temp);
 

  // coefficients of D
  chebmodel_vec_set_dim(D, 6);
  chebmodel_vec_zero(D);
  mpfi_sqr(temp, a_sk);
  mpfi_mul(temp, temp, a_sk);
  mpfi_sqr(temp, temp);
  mpfi_mul(temp, temp, a_sk);
  mpfi_mul(temp, temp, mu_earth);
  mpfi_sqrt(temp, temp);
  mpfi_div(temp, alpha20, temp);
  mpfi_mul_si(temp, temp, 3);
  chebmodel_scalar_mul_fi(D->poly + 2, C, temp);
  mpfi_neg(temp, temp);
  chebmodel_scalar_mul_fi(D->poly + 1, S, temp);


  // rescale A and D with (tf-t0)/2
  mpfi_sub(temp, tf, t0);
  mpfi_mul_2si(temp, temp, -1);
  for (i = 0 ; i < 6 ; i++)
    for (j = 0 ; j < 6 ; j++)
      chebmodel_scalar_mul_fi(A[i] + j, A[i] + j, temp);
  chebmodel_vec_scalar_mul_fi(D, D, temp);


  // clear variables
  chebmodel_clear(S); chebmodel_clear(C);
  mpfi_clear(phi); mpfi_clear(temp);
  chebmodel_clear(S2); chebmodel_clear(C2); chebmodel_clear(SC);

}





int main()
{

  mpfr_prec_t prec = 53;
  mpfr_set_default_prec(prec);

  // physical quantities
  mpfi_t a_sk, mu_earth, theta0, omega_earth, lMTheta_sk, alpha20, r_earth, c20;
  mpfi_init(a_sk); mpfi_init(mu_earth); mpfi_init(theta0); mpfi_init(omega_earth);
  mpfi_init(lMTheta_sk); mpfi_init(alpha20); mpfi_init(r_earth); mpfi_init(c20);
  mpfi_set_si(a_sk, 421658); mpfi_div_si(a_sk, a_sk, 10);
  mpfi_set_si(mu_earth, 29755); mpfi_mul_d(mu_earth, mu_earth, pow(10, 11));
  mpfi_set_si(theta0, 17579); mpfi_div_si(theta0, theta0, 10000);
  mpfi_set_si(omega_earth, 63004); mpfi_div_si(omega_earth, omega_earth, 10000);
  mpfi_set_si(lMTheta_sk, 206); mpfi_div_si(lMTheta_sk, lMTheta_sk, 100);
  mpfi_set_si(r_earth, 6378137); mpfi_div_si(r_earth, r_earth, 1000);
  mpfi_set_si(c20, -1083); mpfi_div_si(c20, c20, 1000000);
  mpfi_sqr(alpha20, r_earth);
  mpfi_mul(alpha20, alpha20, mu_earth);
  mpfi_mul(alpha20, alpha20, c20);
  mpfi_mul_2si(alpha20, alpha20, -1);
  
  // time of interest
  mpfi_t t0, tf;
  mpfi_init(t0); mpfi_init(tf);
  mpfi_set_si(t0, 0);
  mpfi_set_si(tf, 7);

  long i, j;
  

  // matrix and rhs for LODE
  long p = 40; // approximation degree of coefficients
  chebmodel_ptr_ptr A_Kepler = malloc(6 * sizeof(chebmodel_ptr));
  chebmodel_ptr_ptr A_J2 = malloc(6 * sizeof(chebmodel_ptr));
  chebmodel_ptr_ptr A = malloc(6 * sizeof(chebmodel_ptr));
  for (i = 0 ; i < 6 ; i++) {
    A_Kepler[i] = malloc(6 * sizeof(chebmodel_t));
    A_J2[i] = malloc(6 * sizeof(chebmodel_t));
    A[i] = malloc(6 * sizeof(chebmodel_t));
    for (j = 0 ; j < 6 ; j++) {
      chebmodel_init(A_Kepler[i] + j);
      chebmodel_init(A_J2[i] + j);
      chebmodel_init(A[i] + j);
    }
  }
  chebmodel_vec_t D_Kepler, D_J2, D;
  chebmodel_vec_init(D_Kepler);
  chebmodel_vec_init(D_J2);
  chebmodel_vec_init(D);

  kepler_matrix(A_Kepler, D_Kepler, t0, tf, p, mu_earth, a_sk, omega_earth);
  J2_matrix(A_J2, D_J2, t0, tf, p, a_sk, mu_earth, theta0, omega_earth, lMTheta_sk, alpha20);

  // add them
  for (i = 0 ; i < 6 ; i++)
    for (j = 0 ; j < 6 ; j++)
      chebmodel_add(A[i] + j, A_Kepler[i] + j, A_J2[i] + j);
  chebmodel_vec_add(D, D_Kepler, D_J2);

  // error of the coefficients
  printf("errors of the coefficients:\n");
  for (i = 0 ; i < 6 ; i++)
    for (j = 0 ; j < 6 ; j++)
      printf("[%ld,%ld] %.5e\n", i, j, mpfr_get_d(A[i][j].rem, MPFR_RNDN));
  printf("\n");

  // create LODE
  chebmodel_vec_lode_t L;
  chebmodel_vec_lode_init(L);
  chebmodel_vec_lode_set_order_dim(L, 1, 6);
  for (i = 0 ; i < 6 ; i++)
    for (j = 0 ; j < 6 ; j++)
      chebmodel_neg(L->A[0][i] + j, A[i] + j);

  // initial conditions
  mpfi_ptr_ptr I = malloc(6 * sizeof(mpfi_ptr));
  for (i = 0 ; i < 6 ; i++) {
    I[i] = malloc(1 * sizeof(mpfi_t));
    mpfi_init(I[i] + 0);
  } 

  for (i = 0 ; i < 6 ; i++)
    mpfi_set_si(I[i] + 0, 1);


  // solve LODE
  long n = 250; // approximation degree for the solution
  chebmodel_vec_ptr Sols = malloc(2 * sizeof(chebmodel_vec_t));
  chebmodel_vec_init(Sols + 0); chebmodel_vec_init(Sols + 1);
  chebmodel_vec_lode_intop_newton_solve_fr(Sols, L, D, I, n);

  // print errors
  printf("\nerrors:\n");
  for (i = 0 ; i < 6 ; i++)
    printf("err[%ld] = %.5e\n", i, mpfr_get_d(Sols[0].poly[i].rem, MPFR_RNDU));
  printf("\n");


  return 0;

}



