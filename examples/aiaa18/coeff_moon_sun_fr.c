
/* Interpolate Moon and Sun position */


#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include <gmp.h>
#include <mpfr.h>
#include <mpfi.h>
#include <mpfi_io.h>

#include "chebmodel_vec_intop_newton.h"



void cosseries_eval_x(mpfr_t x, mpfr_ptr_ptr X, long N, const mpfr_t t)
{
  mpfr_t res, temp;
  mpfr_init(res);
  mpfr_init(temp);
  mpfr_set_zero(res, 1);
  long i;

  for (i = 0 ; i < N ; i++) {
    mpfr_const_pi(temp, MPFR_RNDN);
    mpfr_mul(temp, temp, X[0] + i, MPFR_RNDN);
    mpfr_mul(temp, temp, t, MPFR_RNDN);
    mpfr_mul_2si(temp, temp, 1, MPFR_RNDN);
    mpfr_add(temp, temp, X[2] + i, MPFR_RNDN);
    mpfr_cos(temp, temp, MPFR_RNDN);
    mpfr_mul(temp, temp, X[1] + i, MPFR_RNDN);
    mpfr_add(res, res, temp, MPFR_RNDN);
  }

  // get result and clear variables
  mpfr_set(x, res, MPFR_RNDN);
  mpfr_clear(res);
  mpfr_clear(temp);
}


void cosseries_eval_y(mpfr_t y, mpfr_ptr_ptr X, long N, const mpfr_t t)
{
  mpfr_t res, temp;
  mpfr_init(res);
  mpfr_init(temp);
  mpfr_set_zero(res, 1);
  long i;

  for (i = 0 ; i < N ; i++) {
    mpfr_const_pi(temp, MPFR_RNDN);
    mpfr_mul(temp, temp, X[0] + i, MPFR_RNDN);
    mpfr_mul(temp, temp, t, MPFR_RNDN);
    mpfr_mul_2si(temp, temp, 1, MPFR_RNDN);
    mpfr_add(temp, temp, X[4] + i, MPFR_RNDN);
    mpfr_cos(temp, temp, MPFR_RNDN);
    mpfr_mul(temp, temp, X[3] + i, MPFR_RNDN);
    mpfr_add(res, res, temp, MPFR_RNDN);
  }

  // get result and clear variables
  mpfr_set(y, res, MPFR_RNDN);
  mpfr_clear(res);
  mpfr_clear(temp);
}


void cosseries_eval_z(mpfr_t z, mpfr_ptr_ptr X, long N, const mpfr_t t)
{
  mpfr_t res, temp;
  mpfr_init(res);
  mpfr_init(temp);
  mpfr_set_zero(res, 1);
  long i;

  for (i = 0 ; i < N ; i++) {
    mpfr_const_pi(temp, MPFR_RNDN);
    mpfr_mul(temp, temp, X[0] + i, MPFR_RNDN);
    mpfr_mul(temp, temp, t, MPFR_RNDN);
    mpfr_mul_2si(temp, temp, 1, MPFR_RNDN);
    mpfr_add(temp, temp, X[6] + i, MPFR_RNDN);
    mpfr_cos(temp, temp, MPFR_RNDN);
    mpfr_mul(temp, temp, X[5] + i, MPFR_RNDN);
    mpfr_add(res, res, temp, MPFR_RNDN);
  }

  // get result and clear variables
  mpfr_set(z, res, MPFR_RNDN);
  mpfr_clear(res);
  mpfr_clear(temp);
}



// plot x, y and z using Fourier decomposition in X, from t0 to tf, with n equidistant points, in [file]
void plot_position(mpfr_ptr_ptr X, long N, const mpfr_t t0, const mpfr_t tf, long n, FILE * file)
{
  mpfr_t t, tt, x, y, z;
  mpfr_init(t);
  mpfr_init(tt);
  mpfr_init(x);
  mpfr_init(y);
  mpfr_init(z);
  long i;

  for (i = 0 ; i < n ; i++) {

    mpfr_mul_si(t, t0, n-1-i, MPFR_RNDN);
    mpfr_mul_si(tt, tf, i, MPFR_RNDN);
    mpfr_add(t, t, tt, MPFR_RNDN);
    mpfr_div_si(t, t, n-1, MPFR_RNDN);

    cosseries_eval_x(x, X, N, t);
    cosseries_eval_y(y, X, N, t);
    cosseries_eval_z(z, X, N, t);

    fprintf(file, "%.10e\t%.10e\t%.10e\t%.10e\n", mpfr_get_d(t, MPFR_RNDN), mpfr_get_d(x, MPFR_RNDN), mpfr_get_d(y, MPFR_RNDN), mpfr_get_d(z, MPFR_RNDN));

  }

  // clear variables
  mpfr_clear(t);
  mpfr_clear(tt);
  mpfr_clear(x);
  mpfr_clear(y);
  mpfr_clear(z);
}


// interpolate Moon or Sun
// N = size of X
// n = degree of interpolant P, between t0 and tf

void cosseries_interpolate_x(mpfr_chebpoly_t Px, mpfr_ptr_ptr X, long N, const mpfr_t t0, const mpfr_t tf, long n)
{
  mpfr_t t, t2, t3;
  mpfr_init(t);
  mpfr_init(t2);
  mpfr_init(t3);
  long k;

  // F contains the evaluation of the cosine series at the Chebyshev nodes
  mpfr_vec_t F;
  mpfr_vec_init(F);
  mpfr_vec_set_length(F, n+1);

  for (k = 0 ; k <= n ; k++) {

    mpfr_const_pi(t3, MPFR_RNDN);
    mpfr_mul_si(t3, t3, 2*k+1, MPFR_RNDN);
    mpfr_div_si(t3, t3, 2*n+2, MPFR_RNDN);
    mpfr_cos(t3, t3, MPFR_RNDN);

    mpfr_si_sub(t, 1, t3, MPFR_RNDN);
    mpfr_mul(t, t, t0, MPFR_RNDN);
    mpfr_mul_2si(t, t, -1, MPFR_RNDN);
    mpfr_add_si(t2, t3, 1, MPFR_RNDN);
    mpfr_mul(t2, t2, tf, MPFR_RNDN);
    mpfr_mul_2si(t2, t2, -1, MPFR_RNDN);
    mpfr_add(t, t, t2, MPFR_RNDN);

    cosseries_eval_x(F->coeffs + k, X, N, t);
  
  }

  // interpolate
  mpfr_chebpoly_interpolate(Px, F);

  // clear variables
  mpfr_clear(t);
  mpfr_clear(t2);
  mpfr_clear(t3);
  mpfr_vec_clear(F);
}


void cosseries_interpolate_y(mpfr_chebpoly_t Py, mpfr_ptr_ptr X, long N, const mpfr_t t0, const mpfr_t tf, long n)
{
  mpfr_t t, t2, t3;
  mpfr_init(t);
  mpfr_init(t2);
  mpfr_init(t3);
  long k;

  // F contains the evaluation of the cosine series at the Chebyshev nodes
  mpfr_vec_t F;
  mpfr_vec_init(F);
  mpfr_vec_set_length(F, n+1);

  for (k = 0 ; k <= n ; k++) {

    mpfr_const_pi(t3, MPFR_RNDN);
    mpfr_mul_si(t3, t3, 2*k+1, MPFR_RNDN);
    mpfr_div_si(t3, t3, 2*n+2, MPFR_RNDN);
    mpfr_cos(t3, t3, MPFR_RNDN);

    mpfr_si_sub(t, 1, t3, MPFR_RNDN);
    mpfr_mul(t, t, t0, MPFR_RNDN);
    mpfr_mul_2si(t, t, -1, MPFR_RNDN);
    mpfr_add_si(t2, t3, 1, MPFR_RNDN);
    mpfr_mul(t2, t2, tf, MPFR_RNDN);
    mpfr_mul_2si(t2, t2, -1, MPFR_RNDN);
    mpfr_add(t, t, t2, MPFR_RNDN);

    cosseries_eval_y(F->coeffs + k, X, N, t);
  
  }

  // interpolate
  mpfr_chebpoly_interpolate(Py, F);

  // clear variables
  mpfr_clear(t);
  mpfr_clear(t2);
  mpfr_clear(t3);
  mpfr_vec_clear(F);
}


void cosseries_interpolate_z(mpfr_chebpoly_t Pz, mpfr_ptr_ptr X, long N, const mpfr_t t0, const mpfr_t tf, long n)
{
  mpfr_t t, t2, t3;
  mpfr_init(t);
  mpfr_init(t2);
  mpfr_init(t3);
  long k;

  // F contains the evaluation of the cosine series at the Chebyshev nodes
  mpfr_vec_t F;
  mpfr_vec_init(F);
  mpfr_vec_set_length(F, n+1);

  for (k = 0 ; k <= n ; k++) {

    mpfr_const_pi(t3, MPFR_RNDN);
    mpfr_mul_si(t3, t3, 2*k+1, MPFR_RNDN);
    mpfr_div_si(t3, t3, 2*n+2, MPFR_RNDN);
    mpfr_cos(t3, t3, MPFR_RNDN);

    mpfr_si_sub(t, 1, t3, MPFR_RNDN);
    mpfr_mul(t, t, t0, MPFR_RNDN);
    mpfr_mul_2si(t, t, -1, MPFR_RNDN);
    mpfr_add_si(t2, t3, 1, MPFR_RNDN);
    mpfr_mul(t2, t2, tf, MPFR_RNDN);
    mpfr_mul_2si(t2, t2, -1, MPFR_RNDN);
    mpfr_add(t, t, t2, MPFR_RNDN);

    cosseries_eval_z(F->coeffs + k, X, N, t);
  
  }

  // interpolate
  mpfr_chebpoly_interpolate(Pz, F);

  // clear variables
  mpfr_clear(t);
  mpfr_clear(t2);
  mpfr_clear(t3);
  mpfr_vec_clear(F);
}



// plot chebpoly with n equidistant points
void plot_interpolates(const mpfr_chebpoly_t Px, const mpfr_chebpoly_t Py, const mpfr_chebpoly_t Pz, const mpfr_t t0, const mpfr_t tf, long n, FILE * file)
{
  mpfr_t t, x, y, z;
  mpfr_init(t);
  mpfr_init(x);
  mpfr_init(y);
  mpfr_init(z);
  double tt;  
  long i;

  for (i = 0 ; i < n ; i++) {

    mpfr_set_si(t, 2*i, MPFR_RNDN);
    mpfr_div_si(t, t, n-1, MPFR_RNDN);
    mpfr_sub_si(t, t, 1, MPFR_RNDN);
    tt = (1-mpfr_get_d(t, MPFR_RNDN))/2*mpfr_get_d(t0, MPFR_RNDN) + (1+mpfr_get_d(t, MPFR_RNDN))/2*mpfr_get_d(tf, MPFR_RNDN);

    mpfr_chebpoly_evaluate_fr(x, Px, t);
    mpfr_chebpoly_evaluate_fr(y, Py, t);
    mpfr_chebpoly_evaluate_fr(z, Pz, t);

    fprintf(file, "%.10e\t%.10e\t%.10e\t%.10e\n", tt, mpfr_get_d(x, MPFR_RNDN), mpfr_get_d(y, MPFR_RNDN), mpfr_get_d(z, MPFR_RNDN));

  }

  // clear variables
  mpfr_clear(t);
  mpfr_clear(x);
  mpfr_clear(y);
  mpfr_clear(z);
}


int main()
{

  mpfr_prec_t prec = 53;
  mpfr_set_default_prec(prec);

  long N_moon = 21816;
  //N_moon = 100;

  long N_sun = 21907 / 100;

  long i, j;

  // read input file

  double ** X_moon = malloc(7 * sizeof(double*));
  for (j = 0 ; j < 7 ; j++)
    X_moon[j] = malloc(N_moon * sizeof(double));

  FILE * input_moon = fopen("Horizons-Moon-5a-1h._freq.txt", "r");

  for (i = 0 ; i < N_moon ; i++)
    for (j = 0 ; j < 7 ; j++)
      fscanf(input_moon, "%lf", X_moon[j] + i);

  double ** X_sun = malloc(7 * sizeof(double*));
  for (j = 0 ; j < 7 ; j++)
    X_sun[j] = malloc(N_sun * sizeof(double));

  FILE * input_sun = fopen("Horizons-Sun-5a-1h._freq.txt", "r");

  for (i = 0 ; i < N_sun ; i++)
    for (j = 0 ; j < 7 ; j++)
      fscanf(input_sun, "%lf", X_sun[j] + i);

  fclose(input_moon);
  fclose(input_sun);

  // convert into mpfr: X_moon_fr and X_sun_fr

  mpfr_ptr_ptr X_moon_fr = malloc(7 * sizeof(mpfr_ptr));
  for (j = 0 ; j < 7 ; j++) {
    X_moon_fr[j] = malloc(N_moon * sizeof(mpfr_t));
    for (i = 0 ; i < N_moon ; i++) {
      mpfr_init(X_moon_fr[j] + i);
      mpfr_set_d(X_moon_fr[j] + i, X_moon[j][i], MPFR_RNDN);
    }
  }

  mpfr_ptr_ptr X_sun_fr = malloc(7 * sizeof(mpfr_ptr));
  for (j = 0 ; j < 7 ; j++) {
    X_sun_fr[j] = malloc(N_sun * sizeof(mpfr_t));
    for (i = 0 ; i < N_sun ; i++) {
      mpfr_init(X_sun_fr[j] + i);
      mpfr_set_d(X_sun_fr[j] + i, X_sun[j][i], MPFR_RNDN);
    }
  }


  // plot position using cosine series

  FILE * moon_plot = fopen("moon_plot_fr.dat", "w+");
  FILE * sun_plot = fopen("sun_plot_fr.dat", "w+");

  long n = 100;
  mpfr_t t0, tf;
  mpfr_init(t0);
  mpfr_init(tf);
  mpfr_set_d(t0, 0, MPFR_RNDN);
  mpfr_set_d(tf, 50, MPFR_RNDN);

  plot_position(X_moon_fr, N_moon, t0, tf, n, moon_plot);
  plot_position(X_sun_fr, N_sun, t0, tf, n, sun_plot);

  fclose(moon_plot);
  fclose(sun_plot);


  // interpolate!

  FILE * moon_interpolate_plot = fopen("moon_interpolate_plot_fr.dat", "w+");
  FILE * sun_interpolate_plot = fopen("sun_interpolate_plot_fr.dat", "w+");

  long p = 20; // approximation degree
  mpfr_chebpoly_t Px_moon, Py_moon, Pz_moon, Px_sun, Py_sun, Pz_sun;
  mpfr_chebpoly_init(Px_moon);
  mpfr_chebpoly_init(Py_moon);
  mpfr_chebpoly_init(Pz_moon);    
  mpfr_chebpoly_init(Px_sun);
  mpfr_chebpoly_init(Py_sun);
  mpfr_chebpoly_init(Pz_sun);    

  cosseries_interpolate_x(Px_moon, X_moon_fr, N_moon, t0, tf, p);
  cosseries_interpolate_y(Py_moon, X_moon_fr, N_moon, t0, tf, p);
  cosseries_interpolate_z(Pz_moon, X_moon_fr, N_moon, t0, tf, p);

  cosseries_interpolate_x(Px_sun, X_sun_fr, N_sun, t0, tf, p);
  cosseries_interpolate_y(Py_sun, X_sun_fr, N_sun, t0, tf, p);
  cosseries_interpolate_z(Pz_sun, X_sun_fr, N_sun, t0, tf, p);

  plot_interpolates(Px_moon, Py_moon, Pz_moon, t0, tf, n, moon_interpolate_plot);
  plot_interpolates(Px_sun, Py_sun, Pz_sun, t0, tf, n, sun_interpolate_plot);


  // Check result at interpolation nodes
  printf("\n");

  mpfr_t t, t2, t3, val, val2, diff;
  mpfr_init(t);
  mpfr_init(t2);
  mpfr_init(t3);
  mpfr_init(val);
  mpfr_init(val2);
  mpfr_init(diff);
  long k;

  for (k = 0 ; k <= p ; k++) {

    mpfr_const_pi(t3, MPFR_RNDN);
    mpfr_mul_si(t3, t3, 2*k+1, MPFR_RNDN);
    mpfr_div_si(t3, t3, 2*p+2, MPFR_RNDN);
    mpfr_cos(t3, t3, MPFR_RNDN);

    mpfr_si_sub(t, 1, t3, MPFR_RNDN);
    mpfr_mul(t, t, t0, MPFR_RNDN);
    mpfr_mul_2si(t, t, -1, MPFR_RNDN);
    mpfr_add_si(t2, t3, 1, MPFR_RNDN);
    mpfr_mul(t2, t2, tf, MPFR_RNDN);
    mpfr_mul_2si(t2, t2, -1, MPFR_RNDN);
    mpfr_add(t, t, t2, MPFR_RNDN);

    cosseries_eval_x(val, X_moon_fr, N_moon, t);
    mpfr_chebpoly_evaluate_fr(val2, Px_moon, t3);
    mpfr_sub(diff, val, val2, MPFR_RNDN);
    printf("%.5e\t%.5e\t%.5e\t", mpfr_get_d(val, MPFR_RNDN), mpfr_get_d(val2, MPFR_RNDN), mpfr_get_d(diff, MPFR_RNDN));

    cosseries_eval_y(val, X_moon_fr, N_moon, t);
    mpfr_chebpoly_evaluate_fr(val2, Py_moon, t3);
    mpfr_sub(diff, val, val2, MPFR_RNDN);
    printf("%.5e\t%.5e\t%.5e\t", mpfr_get_d(val, MPFR_RNDN), mpfr_get_d(val2, MPFR_RNDN), mpfr_get_d(diff, MPFR_RNDN));

    cosseries_eval_z(val, X_moon_fr, N_moon, t);
    mpfr_chebpoly_evaluate_fr(val2, Pz_moon, t3);
    mpfr_sub(diff, val, val2, MPFR_RNDN);
    printf("%.5e\t%.5e\t%.5e\n", mpfr_get_d(val, MPFR_RNDN), mpfr_get_d(val2, MPFR_RNDN), mpfr_get_d(diff, MPFR_RNDN));

  }

  printf("\n");









  fclose(moon_interpolate_plot);
  fclose(sun_interpolate_plot);


  return 0;

}



