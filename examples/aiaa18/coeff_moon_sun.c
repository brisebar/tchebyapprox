
/* Interpolate Moon and Sun position */


#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include <gmp.h>
#include <mpfr.h>
#include <mpfi.h>
#include <mpfi_io.h>

#include "chebmodel_vec_intop_newton.h"



double cosseries_eval_x(double ** X, long N, double t)
{
  double res = 0;
  long i;

  for (i = 0 ; i < N ; i++)
    res += X[1][i] * cos(2 * M_PI * X[0][i] * t + X[2][i]);

  return res;
}

double cosseries_eval_y(double ** X, long N, double t)
{
  double res = 0;
  long i;

  for (i = 0 ; i < N ; i++)
    res += X[3][i] * cos(2 * M_PI * X[0][i] * t + X[4][i]);

  return res;
}

double cosseries_eval_z(double ** X, long N, double t)
{
  double res = 0;
  long i;

  for (i = 0 ; i < N ; i++)
    res += X[5][i] * cos(2 * M_PI * X[0][i] * t + X[6][i]);

  return res;
}


// plot x, y and z using Fourier decomposition in X, from t0 to tf, with n equidistant points, in [file]
void plot_position(double ** X, long N, double t0, double tf, long n, FILE * file)
{
  double t, x, y, z;
  long i;

  for (i = 0 ; i < n ; i++) {

    t = ((n-1-i)*t0 + i*tf) / (n-1);
    x = cosseries_eval_x(X, N, t);
    y = cosseries_eval_y(X, N, t);
    z = cosseries_eval_z(X, N, t);
  
    fprintf(file, "%.10e\t%.10e\t%.10e\t%.10e\n", t, x, y, z);

  }

}


// interpolate Moon or Sun
// N = size of X
// n = degree of interpolant P, between t0 and tf

void cosseries_interpolate_x(double_chebpoly_t Px, double ** X, long N, double t0, double tf, long n)
{
  long k;
  double t;

  // F contains the evaluation of the cosine series at the Chebyshev nodes
  double_vec_t F;
  double_vec_init(F);
  double_vec_set_length(F, n+1);
  for (k = 0 ; k <= n ; k++) {
    t = cos(M_PI * (2*k+1)/(2*n+2));
    t = (1-t)/2*t0 + (1+t)/2*tf;
    F->coeffs[k] = cosseries_eval_x(X, N, t);
  }

  // interpolate
  double_chebpoly_interpolate(Px, F);

  // free variables
  double_vec_clear(F);
}


void cosseries_interpolate_y(double_chebpoly_t Py, double ** X, long N, double t0, double tf, long n)
{
  long k;
  double t;

  // F contains the evaluation of the cosine series at the Chebyshev nodes
  double_vec_t F;
  double_vec_init(F);
  double_vec_set_length(F, n+1);
  for (k = 0 ; k <= n ; k++) {
    t = cos(M_PI * (2*k+1)/(2*n+2));
    t = (1-t)/2*t0 + (1+t)/2*tf;
    F->coeffs[k] = cosseries_eval_y(X, N, t);
  }

  // interpolate
  double_chebpoly_interpolate(Py, F);

  // free variables
  double_vec_clear(F);
}


void cosseries_interpolate_z(double_chebpoly_t Pz, double ** X, long N, double t0, double tf, long n)
{
  long k;
  double t;

  // F contains the evaluation of the cosine series at the Chebyshev nodes
  double_vec_t F;
  double_vec_init(F);
  double_vec_set_length(F, n+1);
  for (k = 0 ; k <= n ; k++) {
    t = cos(M_PI * (2*k+1)/(2*n+2));
    t = (1-t)/2*t0 + (1+t)/2*tf;
    F->coeffs[k] = cosseries_eval_z(X, N, t);
  }

  // interpolate
  double_chebpoly_interpolate(Pz, F);

  // free variables
  double_vec_clear(F);
}




// plot chebpoly with n equidistant points
void plot_interpolates(const double_chebpoly_t Px, const double_chebpoly_t Py, const double_chebpoly_t Pz, double t0, double tf, long n, FILE * file)
{
  double t, x, y, z;
  long i;

  for (i = 0 ; i < n ; i++) {

    t = 2*i/((double) n-1) - 1;
    double_chebpoly_evaluate_d(&x, Px, &t);
    double_chebpoly_evaluate_d(&y, Py, &t);
    double_chebpoly_evaluate_d(&z, Pz, &t);

    fprintf(file, "%.10e\t%.10e\t%.10e\t%.10e\n", (1-t)/2*t0 + (1+t)/2*tf, x, y, z);

  }

}


int main()
{

  long N_moon = 21816;
  long N_sun = 21907;

  long i, j;

  double ** X_moon = malloc(7 * sizeof(double*));
  for (j = 0 ; j < 7 ; j++)
    X_moon[j] = malloc(N_moon * sizeof(double));

  FILE * input_moon = fopen("Horizons-Moon-5a-1h._freq.txt", "r");

  for (i = 0 ; i < N_moon ; i++)
    for (j = 0 ; j < 7 ; j++)
      fscanf(input_moon, "%lf", X_moon[j] + i);

  double ** X_sun = malloc(7 * sizeof(double*));
  for (j = 0 ; j < 7 ; j++)
    X_sun[j] = malloc(N_sun * sizeof(double));

  FILE * input_sun = fopen("Horizons-Sun-5a-1h._freq.txt", "r");

  for (i = 0 ; i < N_sun ; i++)
    for (j = 0 ; j < 7 ; j++)
      fscanf(input_sun, "%lf", X_sun[j] + i);

  fclose(input_moon);
  fclose(input_sun);

  FILE * moon_plot = fopen("moon_plot.dat", "w+");
  FILE * sun_plot = fopen("sun_plot.dat", "w+");

  long n = 1000;
  long t0 = 0;
  long tf = 50;
  plot_position(X_moon, N_moon, t0, tf, n, moon_plot);
  plot_position(X_sun, N_sun, t0, tf, n, sun_plot);

  fclose(moon_plot);
  fclose(sun_plot);


  FILE * moon_interpolate_plot = fopen("moon_interpolate_plot.dat", "w+");
  FILE * sun_interpolate_plot = fopen("sun_interpolate_plot.dat", "w+");

  // approximation degree
  long p = 20;
  double_chebpoly_t Px_moon, Py_moon, Pz_moon, Px_sun, Py_sun, Pz_sun;
  double_chebpoly_init(Px_moon);
  double_chebpoly_init(Py_moon);
  double_chebpoly_init(Pz_moon);    
  double_chebpoly_init(Px_sun);
  double_chebpoly_init(Py_sun);
  double_chebpoly_init(Pz_sun);    

  cosseries_interpolate_x(Px_moon, X_moon, N_moon, t0, tf, p);
  cosseries_interpolate_y(Py_moon, X_moon, N_moon, t0, tf, p);
  cosseries_interpolate_z(Pz_moon, X_moon, N_moon, t0, tf, p);

  cosseries_interpolate_x(Px_sun, X_sun, N_sun, t0, tf, p);
  cosseries_interpolate_y(Py_sun, X_sun, N_sun, t0, tf, p);
  cosseries_interpolate_z(Pz_sun, X_sun, N_sun, t0, tf, p);

  plot_interpolates(Px_moon, Py_moon, Pz_moon, t0, tf, n, moon_interpolate_plot);
  plot_interpolates(Px_sun, Py_sun, Pz_sun, t0, tf, n, sun_interpolate_plot);

  fclose(moon_interpolate_plot);
  fclose(sun_interpolate_plot);


  return 0;

}



