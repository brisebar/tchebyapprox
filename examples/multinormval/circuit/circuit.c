
/* Linearized dynamics with J2 */


#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include <gmp.h>
#include <mpfr.h>
#include <mpfi.h>
#include <mpfi_io.h>

#include "chebmodel_vec_intop_newton.h"



int main()
{

  mpfr_prec_t prec = 100;
  mpfr_set_default_prec(prec);

  long i, j, k, l;

  mpfi_t R01, R02, tau1, tau2, alpha, C1, C2, E;
  mpfi_init(R01); mpfi_init(R02); mpfi_init(C1); mpfi_init(C2);
  mpfi_init(alpha); mpfi_init(tau1); mpfi_init(tau2); mpfi_init(E);

  // set values
  mpfi_set_str(R01, "5", 10); // = 5 kOhm
  mpfi_set_str(R02, "3", 10); // = 3 kOhm
  mpfi_set_str(C1, "1", 10);
  mpfi_set_str(C2, "1", 10); // = 1 mF = 1000 uF
  mpfi_set_str(tau1, "0", 10);
  mpfi_set_str(tau2, "-0.5", 10);
  mpfi_set_str(alpha, "100", 10);
  mpfi_set_str(E, "5", 10);
  // KOhm and mF give V and mA

  // 1/R1(t) and 1/R2(t)
  chebmodel_t G1, G2;
  chebmodel_init(G1); chebmodel_init(G2);
  // G1(t) = 1/R1(t)
  chebmodel_zero(G1);
  mpfi_chebpoly_set_degree(G1->poly, 1);
  mpfi_set_si(G1->poly->coeffs + 1, 1);
  mpfi_neg(G1->poly->coeffs + 0, tau1);
  chebmodel_mul(G1, G1, G1);
  chebmodel_scalar_mul_fi(G1, G1, alpha);
  mpfi_add_si(G1->poly->coeffs + 0, G1->poly->coeffs + 0, 1);
  chebmodel_scalar_div_fi(G1, G1, R01);
  // G2(t) = 1/R2(t)
  chebmodel_zero(G2);
  mpfi_chebpoly_set_degree(G2->poly, 1);
  mpfi_set_si(G2->poly->coeffs + 1, 1);
  mpfi_neg(G2->poly->coeffs + 0, tau2);
  chebmodel_mul(G2, G2, G2);
  chebmodel_scalar_mul_fi(G2, G2, alpha);
  mpfi_add_si(G2->poly->coeffs + 0, G2->poly->coeffs + 0, 1);
  chebmodel_scalar_div_fi(G2, G2, R02);

  // LODE
  chebmodel_vec_lode_t L;
  chebmodel_vec_lode_init(L);
  chebmodel_vec_lode_set_order_dim(L, 1, 2);
  // (0,0) => (G1+G2)/C1
  chebmodel_add(L->A[0][0] + 0, G1, G2);
  chebmodel_scalar_div_fi(L->A[0][0] + 0, L->A[0][0] + 0, C1);
  // (0,1) => -G2/C1
  chebmodel_scalar_div_fi(L->A[0][0] + 1, G2, C1);
  chebmodel_neg(L->A[0][0] + 1, L->A[0][0] + 1);
  // (1,0) => -G2/C2
  chebmodel_scalar_div_fi(L->A[0][1] + 0, G2, C2);
  chebmodel_neg(L->A[0][1] + 0, L->A[0][1] + 0);
  // (1,1) => G2/C2
  chebmodel_scalar_div_fi(L->A[0][1] + 1, G2, C2);

  // RHS
  chebmodel_vec_t RHS;
  chebmodel_vec_init(RHS);
  chebmodel_vec_set_dim(RHS, 2);
  chebmodel_scalar_div_fi(RHS->poly + 0, G1, C1);
  chebmodel_scalar_mul_fi(RHS->poly + 0, RHS->poly + 0, E);

  // null initial conditions
  mpfi_ptr_ptr IV = malloc(2 * sizeof(mpfi_ptr));
  IV[0] = malloc(1 * sizeof(mpfi_t));
  mpfi_init(IV[0] + 0); mpfi_set_str(IV[0] + 0, "0", 10); 
  IV[1] = malloc(1 * sizeof(mpfi_t));
  mpfi_init(IV[1] + 0); mpfi_set_str(IV[1] + 0, "0", 10); 

/*
  // OPTION 1: solve and plot

  // solution
  long N = 100; // approximation degree
  chebmodel_vec_ptr Sols = malloc(2 * sizeof(chebmodel_vec_t));
  chebmodel_vec_init(Sols + 0); chebmodel_vec_init(Sols + 1);
  chebmodel_vec_lode_intop_newton_solve_fr(Sols, L, RHS, IV, N);

  // print errors
  printf("errors:\n");
  printf("U1 -> %.5e\n", mpfr_get_d(Sols[0].poly[0].rem, MPFR_RNDU));
  printf("U2 -> %.5e\n", mpfr_get_d(Sols[0].poly[1].rem, MPFR_RNDU));
  printf("I1 -> %.5e\n", mpfi_get_d(C1) * mpfr_get_d(Sols[1].poly[0].rem, MPFR_RNDU));
  printf("I2 -> %.5e\n", mpfi_get_d(C2) * mpfr_get_d(Sols[1].poly[1].rem, MPFR_RNDU));


  // plot U1, U2, I1, I2 in file
  long nb_points = 1000;
  printf("\nPlotting U1, U2, i1, i2 in circuit_plot.dat...\n");
  FILE * circuit_plot = fopen("circuit_plot.dat", "w+");
  double t, u1, u2, i1, i2;
  double C1_d = mpfi_get_d(C1); 
  double C2_d = mpfi_get_d(C2);
  double_chebpoly_t U1_d, U2_d, I1_d, I2_d;
  double_chebpoly_init(U1_d); chebmodel_get_double_chebpoly(U1_d, Sols[0].poly + 0);
  double_chebpoly_init(U2_d); chebmodel_get_double_chebpoly(U2_d, Sols[0].poly + 1);
  double_chebpoly_init(I1_d); chebmodel_get_double_chebpoly(I1_d, Sols[1].poly + 0);
  double_chebpoly_scalar_mul_d(I1_d, I1_d, &C1_d);
  double_chebpoly_init(I2_d); chebmodel_get_double_chebpoly(I2_d, Sols[1].poly + 1);
  double_chebpoly_scalar_mul_d(I2_d, I2_d, &C2_d);
  // header
  fprintf(circuit_plot, "#t\tU1\tU2\ti1\ti2\n\n");
  // loop
  for (i = 0 ; i < nb_points ; i++) {
    t = 2 * i / ((double) nb_points-1) - 1;
    double_chebpoly_evaluate_d(&u1, U1_d, &t);
    double_chebpoly_evaluate_d(&u2, U2_d, &t);
    double_chebpoly_evaluate_d(&i1, I1_d, &t);
    double_chebpoly_evaluate_d(&i2, I2_d, &t);
    fprintf(circuit_plot, "%.10e\t%.10e\t%.10e\t%.10e\t%.10e\n", t, u1, u2, i1, i2);
  }

  fclose(circuit_plot);
*/


  // OPTION 2: T norm in function of truncation order N

  long Ntrunc = 1000;

  // initializing
  chebmodel_vec_intop_t Kcheb;
  chebmodel_vec_intop_init(Kcheb);
  chebmodel_vec_intop_set_lode(Kcheb, L);
  mpfi_chebpoly_vec_intop_t K;
  mpfi_chebpoly_vec_intop_init(K);
  chebmodel_vec_intop_get_mpfi_chebpoly_vec_intop(K, Kcheb);

  mpfr_ptr_ptr T_norm = malloc(2 * sizeof(mpfr_ptr));
  T_norm[0] = malloc(2 * sizeof(mpfr_t));
  T_norm[1] = malloc(2 * sizeof(mpfr_t));
  mpfr_init(T_norm[0] + 0); mpfr_init(T_norm[0] + 1);
  mpfr_init(T_norm[1] + 0); mpfr_init(T_norm[1] + 1);

  mpfr_ptr_ptr T_norm1 = malloc(2 * sizeof(mpfr_ptr));
  T_norm1[0] = malloc(2 * sizeof(mpfr_t));
  T_norm1[1] = malloc(2 * sizeof(mpfr_t));
  mpfr_init(T_norm1[0] + 0); mpfr_init(T_norm1[0] + 1);
  mpfr_init(T_norm1[1] + 0); mpfr_init(T_norm1[1] + 1);

  mpfr_ptr_ptr T_norm2 = malloc(2 * sizeof(mpfr_ptr));
  T_norm2[0] = malloc(2 * sizeof(mpfr_t));
  T_norm2[1] = malloc(2 * sizeof(mpfr_t));
  mpfr_init(T_norm2[0] + 0); mpfr_init(T_norm2[0] + 1);
  mpfr_init(T_norm2[1] + 0); mpfr_init(T_norm2[1] + 1);

  mpfr_ptr_ptr T_norm3 = malloc(2 * sizeof(mpfr_ptr));
  T_norm3[0] = malloc(2 * sizeof(mpfr_t));
  T_norm3[1] = malloc(2 * sizeof(mpfr_t));
  mpfr_init(T_norm3[0] + 0); mpfr_init(T_norm3[0] + 1);
  mpfr_init(T_norm3[1] + 0); mpfr_init(T_norm3[1] + 1);

  mpfr_ptr_ptr T_norm4 = malloc(2 * sizeof(mpfr_ptr));
  T_norm4[0] = malloc(2 * sizeof(mpfr_t));
  T_norm4[1] = malloc(2 * sizeof(mpfr_t));
  mpfr_init(T_norm4[0] + 0); mpfr_init(T_norm4[0] + 1);
  mpfr_init(T_norm4[1] + 0); mpfr_init(T_norm4[1] + 1);

  mpfr_ptr_ptr T_norm5 = malloc(2 * sizeof(mpfr_ptr));
  T_norm5[0] = malloc(2 * sizeof(mpfr_t));
  T_norm5[1] = malloc(2 * sizeof(mpfr_t));
  mpfr_init(T_norm5[0] + 0); mpfr_init(T_norm5[0] + 1);
  mpfr_init(T_norm5[1] + 0); mpfr_init(T_norm5[1] + 1);

  mpfi_bandmatrix_t M_K;
  mpfi_bandmatrix_init(M_K);
  mpfi_bandmatrix_struct ** M_Ks = malloc(2 * sizeof(mpfi_bandmatrix_struct*));
  M_Ks[0] = malloc(2 * sizeof(mpfi_bandmatrix_t));
  M_Ks[1] = malloc(2 * sizeof(mpfi_bandmatrix_t));
  mpfi_bandmatrix_init(M_Ks[0] + 0); mpfi_bandmatrix_init(M_Ks[0] + 1);
  mpfi_bandmatrix_init(M_Ks[1] + 0); mpfi_bandmatrix_init(M_Ks[1] + 1);

  mpfr_bandmatrix_t M_K_fr;
  mpfr_bandmatrix_init(M_K_fr);
  mpfr_bandmatrix_struct ** M_Ks_fr = malloc(2 * sizeof(mpfr_bandmatrix_struct*));
  M_Ks_fr[0] = malloc(2 * sizeof(mpfr_bandmatrix_t));
  M_Ks_fr[1] = malloc(2 * sizeof(mpfr_bandmatrix_t));
  mpfr_bandmatrix_init(M_Ks_fr[0] + 0); mpfr_bandmatrix_init(M_Ks_fr[0] + 1);
  mpfr_bandmatrix_init(M_Ks_fr[1] + 0); mpfr_bandmatrix_init(M_Ks_fr[1] + 1);

  mpfr_bandmatrix_QRdecomp_t N_K;
  mpfr_bandmatrix_QRdecomp_init(N_K);
  mpfr_bandmatrix_t M_K_inv_glob;
  mpfr_bandmatrix_init(M_K_inv_glob);
  mpfr_bandmatrix_struct ** M_K_inv = malloc(2 * sizeof(mpfr_bandmatrix_struct *));
  M_K_inv[0] = malloc(2 * sizeof(mpfr_bandmatrix_t));
  M_K_inv[1] = malloc(2 * sizeof(mpfr_bandmatrix_t));
  mpfr_bandmatrix_init(M_K_inv[0] + 0); mpfr_bandmatrix_init(M_K_inv[0] + 1);
  mpfr_bandmatrix_init(M_K_inv[1] + 0); mpfr_bandmatrix_init(M_K_inv[1] + 1);


  // matrix representations of K
  mpfi_chebpoly_intop_get_bandmatrix(M_Ks[0] + 0, K->intop[0] + 0, Ntrunc);
  mpfi_bandmatrix_normalise(M_Ks[0] + 0);
  mpfi_chebpoly_intop_get_bandmatrix(M_Ks[0] + 1, K->intop[0] + 1, Ntrunc);
  mpfi_bandmatrix_normalise(M_Ks[0] + 1);
  mpfi_chebpoly_intop_get_bandmatrix(M_Ks[1] + 0, K->intop[1] + 0, Ntrunc);
  mpfi_bandmatrix_normalise(M_Ks[1] + 0);
  mpfi_chebpoly_intop_get_bandmatrix(M_Ks[1] + 1, K->intop[1] + 1, Ntrunc);
  mpfi_bandmatrix_normalise(M_Ks[1] + 1);
  //mpfi_bandmatrix_merge(M_K, M_Ks, 2);
 
  // matrix representation of Id-K
  for (i = 0 ; i <= Ntrunc ; i++) {
    mpfi_sub_si(M_Ks[0][0].D[i] + M_Ks[0][0].Dwidth, M_Ks[0][0].D[i] + M_Ks[0][0].Dwidth, 1);
    mpfi_sub_si(M_Ks[1][1].D[i] + M_Ks[1][1].Dwidth, M_Ks[1][1].D[i] + M_Ks[1][1].Dwidth, 1);
  }
  mpfi_bandmatrix_neg(M_Ks[0] + 0, M_Ks[0] + 0); mpfi_bandmatrix_neg(M_Ks[0] + 1, M_Ks[0] + 1);   
  mpfi_bandmatrix_neg(M_Ks[1] + 0, M_Ks[1] + 0); mpfi_bandmatrix_neg(M_Ks[1] + 1, M_Ks[1] + 1);   

  // global matrix of Id-K
  mpfi_bandmatrix_merge(M_K, M_Ks, 2);

  // mpfr matrices
  mpfi_bandmatrix_get_mpfr_bandmatrix(M_Ks_fr[0] + 0, M_Ks[0] + 0);
  mpfi_bandmatrix_get_mpfr_bandmatrix(M_Ks_fr[0] + 1, M_Ks[0] + 1);
  mpfi_bandmatrix_get_mpfr_bandmatrix(M_Ks_fr[1] + 0, M_Ks[1] + 0);
  mpfi_bandmatrix_get_mpfr_bandmatrix(M_Ks_fr[1] + 1, M_Ks[1] + 1);
  mpfi_bandmatrix_get_mpfr_bandmatrix(M_K_fr, M_K);

  // upper triangular decomposition
  mpfr_bandmatrix_get_QRdecomp(N_K, M_K_fr);

  // approx inverse
  mpfr_bandmatrix_QRdecomp_approx_band_inverse(M_K_inv_glob, N_K, 100, 100);

  // small inverse matrices
  mpfr_bandmatrix_split(M_K_inv, M_K_inv_glob, 2);

  // compute the bounds of the operator
  mpfi_chebpoly_vec_intop_newton_bounds_bound1_fr(T_norm1, K, M_Ks);
  mpfi_chebpoly_vec_intop_newton_bounds_bound2_fr(T_norm2, K, M_Ks, M_K_inv);
  mpfi_chebpoly_vec_intop_newton_bounds_bound34_fr(T_norm3, T_norm4, K, M_Ks, M_K_inv);
  mpfi_chebpoly_vec_intop_newton_bounds_bound5_fr(T_norm5, K, M_Ks, M_K_inv);
  printf("\n[%.5e][%.5e][%.5e][%.5e][%.5e]\n",
    mpfr_get_d(T_norm1[0]+0,MPFR_RNDN)+mpfr_get_d(T_norm1[0]+1,MPFR_RNDN)+mpfr_get_d(T_norm1[1]+0,MPFR_RNDN)+mpfr_get_d(T_norm1[1]+1,MPFR_RNDN),
    mpfr_get_d(T_norm2[0]+0,MPFR_RNDN)+mpfr_get_d(T_norm2[0]+1,MPFR_RNDN)+mpfr_get_d(T_norm2[1]+0,MPFR_RNDN)+mpfr_get_d(T_norm2[1]+1,MPFR_RNDN),
    mpfr_get_d(T_norm3[0]+0,MPFR_RNDN)+mpfr_get_d(T_norm3[0]+1,MPFR_RNDN)+mpfr_get_d(T_norm3[1]+0,MPFR_RNDN)+mpfr_get_d(T_norm3[1]+1,MPFR_RNDN),
    mpfr_get_d(T_norm4[0]+0,MPFR_RNDN)+mpfr_get_d(T_norm4[0]+1,MPFR_RNDN)+mpfr_get_d(T_norm4[1]+0,MPFR_RNDN)+mpfr_get_d(T_norm4[1]+1,MPFR_RNDN),
    mpfr_get_d(T_norm5[0]+0,MPFR_RNDN)+mpfr_get_d(T_norm5[0]+1,MPFR_RNDN)+mpfr_get_d(T_norm5[1]+0,MPFR_RNDN)+mpfr_get_d(T_norm5[1]+1,MPFR_RNDN));

  // merge bounds
  for (i = 0 ; i < 2 ; i++)
    for (j = 0 ; j < 2 ; j++) {
      mpfr_add(T_norm[i] + j, T_norm3[i] + j, T_norm4[i] + j, MPFR_RNDU);
      if (mpfr_cmp(T_norm[i] + j, T_norm1[i] + j) < 0)
        mpfr_set(T_norm[i] + j, T_norm1[i] + j, MPFR_RNDU);
      if (mpfr_cmp(T_norm[i] + j, T_norm2[i] + j) < 0)
        mpfr_set(T_norm[i] + j, T_norm2[i] + j, MPFR_RNDU);
      mpfr_add(T_norm[i] + j, T_norm[i] + j, T_norm5[i] + j, MPFR_RNDU);
    }
/*
  // alternative: use contract routine
  mpfi_chebpoly_vec_intop_newton_contract_fr(T_norm, M_K_inv, K, Ntrunc);
*/
  // compute spectral radius
  mpfr_t rho, delta, temp;
  mpfr_init(rho); mpfr_init(delta); mpfr_init(temp);
  if (mpfr_cmp(T_norm[0] + 0, T_norm[1] + 1) >= 0)
    mpfr_sub(delta, T_norm[0] + 0, T_norm[1] + 1, MPFR_RNDU);
  else
    mpfr_sub(delta, T_norm[1] + 1, T_norm[0] + 0, MPFR_RNDU);
  mpfr_mul(delta, delta, delta, MPFR_RNDU);
  mpfr_mul(temp, T_norm[0] + 1, T_norm[1] + 0, MPFR_RNDU);
  mpfr_mul_2si(temp, temp, 2, MPFR_RNDU);
  mpfr_add(delta, delta, temp, MPFR_RNDU);
  mpfr_sqrt(delta, delta, MPFR_RNDU); // delta=(a-d)^2+4*b*c
  mpfr_add(rho, T_norm[0] + 0, T_norm[1] + 1, MPFR_RNDU);
  mpfr_add(rho, rho, delta, MPFR_RNDU);
  mpfr_mul_2si(rho, rho, -1, MPFR_RNDU); 

  
  // output result
  printf("\n\n=======================\n\nN=%ld\nT_norm =\n", M_K_inv[0][0].dim-1);
  printf("%.5e\t%.5e\n%.5e\t%.5e\n",
    mpfr_get_d(T_norm[0] + 0, MPFR_RNDU), mpfr_get_d(T_norm[0] + 1, MPFR_RNDU),
    mpfr_get_d(T_norm[1] + 0, MPFR_RNDU), mpfr_get_d(T_norm[1] + 1, MPFR_RNDU));
  printf("rho=%.5e\n\n", mpfr_get_d(rho, MPFR_RNDU));


  // validate a solution
 
  mpfi_chebpoly_vec_lode_t Lfi; mpfi_chebpoly_vec_lode_init(Lfi);
  chebmodel_vec_lode_get_mpfi_chebpoly_vec_lode(Lfi, L); 
  mpfi_chebpoly_vec_t RHSfi; mpfi_chebpoly_vec_init(RHSfi);
  chebmodel_vec_get_mpfi_chebpoly_vec(RHSfi, RHS);
  mpfr_chebpoly_vec_intop_t Kfr;
  mpfr_chebpoly_vec_intop_init(Kfr);
  mpfi_chebpoly_vec_intop_get_mpfr_chebpoly_vec_intop(Kfr, K);
  mpfi_chebpoly_vec_t G; mpfi_chebpoly_vec_init(G);
  mpfi_chebpoly_vec_intop_rhs(G, Lfi, RHSfi, IV);
  mpfr_chebpoly_vec_t Gfr; mpfr_chebpoly_vec_init(Gfr);
  mpfi_chebpoly_vec_get_mpfr_chebpoly_vec(Gfr, G);
  mpfr_chebpoly_vec_t I, Iref; mpfr_chebpoly_vec_init(I); mpfr_chebpoly_vec_init(Iref);  
  __mpfr_struct eps[2]; mpfr_inits(eps + 0, eps + 1, NULL);
  __mpfr_struct eps_ref[2]; mpfr_inits(eps_ref + 0, eps_ref + 1, NULL);
  mpfr_t C1fr, C2fr; mpfr_inits(C1fr, C2fr, NULL);
  mpfi_mid(C1fr, C1); mpfi_mid(C2fr, C2);

  // solution
  long Nref = 100; // reference approximation degree
  long N1 = 40;
  long N2 = 25;
  mpfr_chebpoly_vec_intop_approxsolve(Iref, Kfr, Gfr, Nref);
  mpfr_chebpoly_vec_set(I, Iref);
  mpfr_chebpoly_set_degree(I->poly + 0, N1);
  mpfr_chebpoly_set_degree(I->poly + 1, N2);
  mpfr_chebpoly_vec_sub(Iref, Iref, I);
  mpfr_chebpoly_1norm(eps_ref + 0, Iref->poly + 0);
  mpfr_chebpoly_1norm(eps_ref + 1, Iref->poly + 1);
 
  // validation
  mpfi_chebpoly_vec_intop_newton_validate_sol_aux_fr(eps, K, T_norm, M_K_inv, G, I);

  // rescale to get the current (in mA)
  mpfr_chebpoly_scalar_mul_fr(I->poly + 0, I->poly + 0, C1fr);
  mpfr_chebpoly_scalar_mul_fr(I->poly + 1, I->poly + 1, C2fr);
  mpfr_mul(eps + 0, eps + 0, C1fr, MPFR_RNDN);
  mpfr_mul(eps + 1, eps + 1, C2fr, MPFR_RNDN);
  mpfr_mul(eps_ref + 0, eps_ref + 0, C1fr, MPFR_RNDN);
  mpfr_mul(eps_ref + 1, eps_ref + 1, C2fr, MPFR_RNDN);
  
  // print results
  printf("\n\n============\n");
  printf("Ntrunc=%ld\tNref=%ld\tN1=%ld\tN2=%ld\n", Ntrunc, Nref, N1, N2);
  printf("eps[1] = %.5e\teps_ref[1] = %.5e\teps[1]/eps_ref[1] = %.5e\n",
    mpfr_get_d(eps + 0, MPFR_RNDN), mpfr_get_d(eps_ref + 0, MPFR_RNDN),
    mpfr_get_d(eps + 0, MPFR_RNDN) / mpfr_get_d(eps_ref + 0, MPFR_RNDN));
  printf("eps[2] = %.5e\teps_ref[2] = %.5e\teps[2]/eps_ref[2] = %.5e\n\n",
    mpfr_get_d(eps + 1, MPFR_RNDN), mpfr_get_d(eps_ref + 1, MPFR_RNDN),
    mpfr_get_d(eps + 1, MPFR_RNDN) / mpfr_get_d(eps_ref + 1, MPFR_RNDN)); 


  return 0;

}



