
/*
This file is part of the TchebyApprox library.

The type [mpfi_bandmatrix_t] implements ([Hwidth],[Dwidth])
almost-banded square matrices of [mpfi_t] of order [dim].
Non-zero coefficients are located:
  - in rows between indicies from 0 to Hwidth-1 (initial coefficients):
    H[i][0], ... , H[i][dim-1]  i = 0 ... Hwidth-1
  - at each row of index [i], between indicies from [max(i-Dwidth,0)] to [min(ind+Dwidth,dim-1)] (diagonal coefficients):
    D[i][0], ..., D[i][Dwidth], ... , D[i][2*Dwidth]   i = 0 ... dim-1
Normal form of the matrix: the entries in the initial band overlapped
by entries from the diagonal band are ignored (considered to be zero).
[mpfi_bandmatrix_normalise]: convert to normal form
by adding the overlapped initial entries to the corresponding
diagonal entries and set them to zero.
*/


#ifndef MPFI_BANDMATRIX_H
#define MPFI_BANDMATRIX_H


#include <stdio.h>
#include <stdlib.h>

#include <gmp.h>
#include <mpfr.h>
#include <mpfi.h>
#include <mpfi_io.h>

#include "double_operations.h"

#include "mpfr_vec.h"
#include "mpfi_vec.h"
#include "mpfr_bandvec.h"
#include "mpfi_bandvec.h"
#include "mpfr_bandmatrix.h"

#include "double_bandmatrix.h"

typedef mpfi_ptr * mpfi_ptr_ptr;

typedef mpfi_ptr ** mpfi_ptr_ptr_ptr;


typedef struct {
  long dim;
  long Hwidth;
  long Dwidth;
  mpfi_ptr_ptr H;
  mpfi_ptr_ptr D;
} mpfi_bandmatrix_struct;

typedef mpfi_bandmatrix_struct mpfi_bandmatrix_t[1];

/* basic manipulations */

void mpfi_bandmatrix_init(mpfi_bandmatrix_t M);

void mpfi_bandmatrix_clear(mpfi_bandmatrix_t M);

void mpfi_bandmatrix_set(mpfi_bandmatrix_t M, const mpfi_bandmatrix_t N);

void mpfi_bandmatrix_swap(mpfi_bandmatrix_t M, mpfi_bandmatrix_t N);

void mpfi_bandmatrix_set_params(mpfi_bandmatrix_t M, long dim, long Hwidth, long Dwidth);

void mpfi_bandmatrix_zero(mpfi_bandmatrix_t M);

void mpfi_bandmatrix_get_double_bandmatrix(double_bandmatrix_t N, const mpfi_bandmatrix_t M);

void mpfi_bandmatrix_get_mpfr_bandmatrix(mpfr_bandmatrix_t N, const mpfi_bandmatrix_t M);

void mpfi_bandmatrix_set_double_bandmatrix(mpfi_bandmatrix_t M, const double_bandmatrix_t N);

void mpfi_bandmatrix_set_mpfr_bandmatrix(mpfi_bandmatrix_t M, const mpfr_bandmatrix_t N);

void mpfi_bandmatrix_normalise(mpfi_bandmatrix_t M);

void mpfi_bandmatrix_get_column(mpfi_bandvec_t V, const mpfi_bandmatrix_t M, long i);

void mpfi_bandmatrix_set_column(mpfi_bandmatrix_t M, const mpfi_bandvec_t V);

void mpfi_bandmatrix_set_column_fr(mpfi_bandmatrix_t M, const mpfr_bandvec_t V);


/* evaluation on vectors */

// on dense vectors

void _mpfi_bandmatrix_evaluate_fr(mpfi_ptr V, const mpfi_bandmatrix_t M, mpfr_srcptr W);

void mpfi_bandmatrix_evaluate_fr(mpfi_vec_t V, const mpfi_bandmatrix_t M, const mpfr_vec_t W);

void _mpfi_bandmatrix_evaluate_fi(mpfi_ptr V, const mpfi_bandmatrix_t M, mpfi_srcptr W);

void mpfi_bandmatrix_evaluate_fi(mpfi_vec_t V, const mpfi_bandmatrix_t M, const mpfi_vec_t W);

// on sparse banded vectors

// all mpfr_t must be distinct !
void _mpfi_bandmatrix_evaluate_band_fr(mpfi_ptr VH, mpfi_ptr VD, long VHwidth, long VDwidth, const mpfi_bandmatrix_t M, mpfr_srcptr WH, mpfr_srcptr WD, long WHwidth, long WDwidth, long ind);

void mpfi_bandmatrix_evaluate_band_fr(mpfi_bandvec_t V, const mpfi_bandmatrix_t M, const mpfr_bandvec_t W);

// all mpfi_t must be distinct !
void _mpfi_bandmatrix_evaluate_band_fi(mpfi_ptr VH, mpfi_ptr VD, long VHwidth, long VDwidth, const mpfi_bandmatrix_t M, mpfi_srcptr WH, mpfi_srcptr WD, long WHwidth, long WDwidth, long ind);

void mpfi_bandmatrix_evaluate_band_fi(mpfi_bandvec_t V, const mpfi_bandmatrix_t M, const mpfi_bandvec_t W);


/* norm 1 computations */

void mpfi_bandmatrix_1norm_ubound(mpfr_t y, const mpfi_bandmatrix_t M);

void mpfi_bandmatrix_1norm_lbound(mpfr_t y, const mpfi_bandmatrix_t M);

void mpfi_bandmatrix_1norm_fi(mpfi_t y, const mpfi_bandmatrix_t M);


/* arithmetic operations */

void mpfi_bandmatrix_add(mpfi_bandmatrix_t M, const mpfi_bandmatrix_t N, const mpfi_bandmatrix_t P);

void mpfi_bandmatrix_sub(mpfi_bandmatrix_t M, const mpfi_bandmatrix_t N, const mpfi_bandmatrix_t P);

void mpfi_bandmatrix_neg(mpfi_bandmatrix_t M, const mpfi_bandmatrix_t N);

void mpfi_bandmatrix_mul(mpfi_bandmatrix_t M, const mpfi_bandmatrix_t N, const mpfi_bandmatrix_t P);


// Compute an upper bound of ||I - N^-1*M||_1 in linear time
void mpfr_bandmatrix_QRdecomp_inverse_certif(mpfr_t bound, const mpfi_bandmatrix_t M, const mpfr_bandmatrix_QRdecomp_t N);


/* split and marge matrices of matrices */


// merge n*n a-b matrices of same size N  into a 4*N a-b matrix
void mpfi_bandmatrix_merge(mpfi_bandmatrix_t A, mpfi_bandmatrix_struct ** B, long n);

// split an n*N order a-b matrix into n*n a-b matrices
void mpfi_bandmatrix_split(mpfi_bandmatrix_struct ** A, const mpfi_bandmatrix_t B, long n);











#endif
