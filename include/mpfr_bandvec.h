
/*
This file is part of the TchebyApprox library.

The type [mpfr_bandvec_t] implements ([Hwidth],[Dwidth])
almost-banded vectors of [mpfr_t] around index [ind].
The dimension (finite or infinite) of the underlying vector space
is not specified.
Non-zero coefficients are located:
  - between indicies from 0 to Hwidth-1 (initial coefficients):
    H[0], ... , H[Hwidth-1]
  - between indicies from max(ind-Dwidth,0) to ind+Dwidth (diagonal coefficients):
    D[0], ..., D[Dwidth], ... , D[2*Dwidth]
*/


#ifndef MPFR_BANDVEC_H
#define MPFR_BANDVEC_H

#include <stdio.h>
#include <stdlib.h>

#include <mpfr.h>
#include <mpfi.h>
#include <mpfi_io.h>

#include "mpfr_vec.h"

typedef struct mpfr_bandvec_struct {
  long Hwidth;
  long Dwidth;
  long ind;
  mpfr_ptr H;
  mpfr_ptr D;
} mpfr_bandvec_struct;

typedef mpfr_bandvec_struct mpfr_bandvec_t[1];
typedef mpfr_bandvec_struct * mpfr_bandvec_ptr;
typedef const mpfr_bandvec_struct * mpfr_bandvec_srcptr;


/* memory management */


// init with a zero vector (Hwidth = 0, Dwidth = 0, D[0] = 0)
void mpfr_bandvec_init(mpfr_bandvec_t V);

// clear V
void mpfr_bandvec_clear(mpfr_bandvec_t V);

// set parameters for V
void mpfr_bandvec_set_params(mpfr_bandvec_t V, long Hwidth, long Dwidth, long ind);


/* basic manipulations and assignments */

void _mpfr_bandvec_set(mpfr_ptr VH, mpfr_ptr VD, long VHwidth, long VDwidth, mpfr_srcptr WH, mpfr_srcptr WD, long WHwidth, long WDwidth, long ind);

void mpfr_bandvec_set(mpfr_bandvec_t V, const mpfr_bandvec_t W);

void mpfr_bandvec_swap(mpfr_bandvec_t V, mpfr_bandvec_t W);

void _mpfr_bandvec_zero(mpfr_ptr VH, mpfr_ptr VD, long Hwidth, long Dwidth);

void mpfr_bandvec_zero(mpfr_bandvec_t V);

// normalise the H and D components if they overlap

void _mpfr_bandvec_normalise(mpfr_ptr VH, mpfr_ptr VD, long VHwidth, long VDwidth, long ind);

void mpfr_bandvec_normalise(mpfr_bandvec_t V);

// set_coeff: set the n-th coeff of V (or (VH,VD,Hwidth,Dwidth,ind)) to c

void _mpfr_bandvec_set_coeff_si(mpfr_ptr VH, mpfr_ptr VD, long Hwidth, long Dwidth, long ind, long n, long c);

void mpfr_bandvec_set_coeff_si(mpfr_bandvec_t V, long n, long c);

void _mpfr_bandvec_set_coeff_z(mpfr_ptr VH, mpfr_ptr VD, long Hwidth, long Dwidth, long ind, long n, const mpz_t c);

void mpfr_bandvec_set_coeff_z(mpfr_bandvec_t V, long n, const mpz_t c);

void _mpfr_bandvec_set_coeff_q(mpfr_ptr VH, mpfr_ptr VD, long Hwidth, long Dwidth, long ind, long n, const mpq_t c);

void mpfr_bandvec_set_coeff_q(mpfr_bandvec_t V, long n, const mpq_t c);

void _mpfr_bandvec_set_coeff_fr(mpfr_ptr VH, mpfr_ptr VD, long Hwidth, long Dwidth, long ind, long n, const mpfr_t c);

void mpfr_bandvec_set_coeff_fr(mpfr_bandvec_t V, long n, const mpfr_t c);

void _mpfr_bandvec_get_mpfr_vec(mpfr_ptr V, long length, mpfr_srcptr WH, mpfr_srcptr WD, long WHwidth, long WDwidth, long ind);

// convert a bandvec into a vec, by using the length of V
void mpfr_bandvec_get_mpfr_vec(mpfr_vec_t V, const mpfr_bandvec_t W);


/* norm 1 */

void _mpfr_bandvec_1norm_ubound(mpfr_t norm, mpfr_srcptr VH, mpfr_srcptr VD, long Hwidth, long Dwidth, long ind);

void mpfr_bandvec_1norm_ubound(mpfr_t norm, const mpfr_bandvec_t V);

void _mpfr_bandvec_1norm_lbound(mpfr_t norm, mpfr_srcptr VH, mpfr_srcptr VD, long Hwidth, long Dwidth, long ind);

void mpfr_bandvec_1norm_lbound(mpfr_t norm, const mpfr_bandvec_t V);

void _mpfr_bandvec_1norm_fi(mpfi_t norm, mpfr_srcptr VH, mpfr_srcptr VD, long Hwidth, long Dwidth, long ind);

void mpfr_bandvec_1norm_fi(mpfi_t norm, const mpfr_bandvec_t V);


/* addition and substraction */
// W and Z must have the same ind, but not necessarily the same Hiwdht and Dwidth

// for add and sub, one must have VHwidth >= max(WHwidth,ZHwidth) and VDwidth >= max(WDwidth,ZDwidth)

void _mpfr_bandvec_add(mpfr_ptr VH, mpfr_ptr VD, long VHwidth, long VDwidth, mpfr_srcptr WH, mpfr_srcptr WD, long WHwidth, long WDwidth, mpfr_srcptr ZH, mpfr_srcptr ZD, long ZHwidth, long ZDwidth, long ind);

void mpfr_bandvec_add(mpfr_bandvec_t V, const mpfr_bandvec_t W, const mpfr_bandvec_t Z);


void _mpfr_bandvec_sub(mpfr_ptr VH, mpfr_ptr VD, long VHwidth, long VDwidth, mpfr_srcptr WH, mpfr_srcptr WD, long WHwidth, long WDwidth, mpfr_srcptr ZH, mpfr_srcptr ZD, long ZHwidth, long ZDwidth, long ind);

void mpfr_bandvec_sub(mpfr_bandvec_t V, const mpfr_bandvec_t W, const mpfr_bandvec_t Z);

// for neg, one must have VHwidth >= WHwidth and VDwidth >= WDwidth

void _mpfr_bandvec_neg(mpfr_ptr VH, mpfr_ptr VD, long VHwidth, long VDwidth, mpfr_srcptr WH, mpfr_srcptr WD, long WHwidth, long WDwidth, long ind);

void mpfr_bandvec_neg(mpfr_bandvec_t V, const mpfr_bandvec_t W);


/* scalar multiplication and division */

void _mpfr_bandvec_scalar_mul_si(mpfr_ptr VH, mpfr_ptr VD, long VHwidth, long Vdwidth, mpfr_srcptr WH, mpfr_srcptr WD, long WHwidth, long WDwidth, long ind, long c);

void mpfr_bandvec_scalar_mul_si(mpfr_bandvec_t V, const mpfr_bandvec_t W, long c); 

void _mpfr_bandvec_scalar_mul_z(mpfr_ptr VH, mpfr_ptr VD, long VHwidth, long Vdwidth, mpfr_srcptr WH, mpfr_srcptr WD, long WHwidth, long WDwidth, long ind, const mpz_t c);

void mpfr_bandvec_scalar_mul_z(mpfr_bandvec_t V, const mpfr_bandvec_t W, const mpz_t c);

void _mpfr_bandvec_scalar_mul_q(mpfr_ptr VH, mpfr_ptr VD, long VHwidth, long Vdwidth, mpfr_srcptr WH, mpfr_srcptr WD, long WHwidth, long WDwidth, long ind, const mpq_t c);

void mpfr_bandvec_scalar_mul_q(mpfr_bandvec_t V, const mpfr_bandvec_t W, const mpq_t c);

void _mpfr_bandvec_scalar_mul_fr(mpfr_ptr VH, mpfr_ptr VD, long VHwidth, long Vdwidth, mpfr_srcptr WH, mpfr_srcptr WD, long WHwidth, long WDwidth, long ind, const mpfr_t c);

void mpfr_bandvec_scalar_mul_fr(mpfr_bandvec_t V, const mpfr_bandvec_t W, const mpfr_t c);

void _mpfr_bandvec_scalar_mul_2si(mpfr_ptr VH, mpfr_ptr VD, long VHwidth, long VDwidth, mpfr_srcptr WH, mpfr_srcptr WD, long WHwidth, long WDwidth, long ind, long k);

void mpfr_bandvec_scalar_mul_2si(mpfr_bandvec_t V, const mpfr_bandvec_t W, long k);


void _mpfr_bandvec_scalar_div_si(mpfr_ptr VH, mpfr_ptr VD, long VHwidth, long Vdwidth, mpfr_srcptr WH, mpfr_srcptr WD, long WHwidth, long WDwidth, long ind, long c);

void mpfr_bandvec_scalar_div_si(mpfr_bandvec_t V, const mpfr_bandvec_t W, long c); 

void _mpfr_bandvec_scalar_div_z(mpfr_ptr VH, mpfr_ptr VD, long VHwidth, long Vdwidth, mpfr_srcptr WH, mpfr_srcptr WD, long WHwidth, long WDwidth, long ind, const mpz_t c);

void mpfr_bandvec_scalar_div_z(mpfr_bandvec_t V, const mpfr_bandvec_t W, const mpz_t c);

void _mpfr_bandvec_scalar_div_q(mpfr_ptr VH, mpfr_ptr VD, long VHwidth, long Vdwidth, mpfr_srcptr WH, mpfr_srcptr WD, long WHwidth, long WDwidth, long ind, const mpq_t c);

void mpfr_bandvec_scalar_div_q(mpfr_bandvec_t V, const mpfr_bandvec_t W, const mpq_t c);

void _mpfr_bandvec_scalar_div_fr(mpfr_ptr VH, mpfr_ptr VD, long VHwidth, long Vdwidth, mpfr_srcptr WH, mpfr_srcptr WD, long WHwidth, long WDwidth, long ind, const mpfr_t c);

void mpfr_bandvec_scalar_div_fr(mpfr_bandvec_t V, const mpfr_bandvec_t W, const mpfr_t c);

void _mpfr_bandvec_scalar_div_2si(mpfr_ptr VH, mpfr_ptr VD, long VHwidth, long VDwidth, mpfr_srcptr WH, mpfr_srcptr WD, long WHwidth, long WDwidth, long ind, long k);

void mpfr_bandvec_scalar_div_2si(mpfr_bandvec_t V, const mpfr_bandvec_t W, long k);


/* display function */


void _mpfr_bandvec_print(mpfr_srcptr VH, mpfr_srcptr VD, long VHwidth, long VDwidth, long ind, size_t digits);

// display the vector V
void mpfr_bandvec_print(const mpfr_bandvec_t V, size_t digits);


#endif
