
/*
This file is part of the TchebyApprox library.

The type [mpfr_chebpoly_lode_t] represents a (scalar)
linear differential operator of order [order]:
  - normalized (leading coeff = 1)
  - with coefficients a[0], ..., a[order-1] represented
    by [mpfr_chebpoly_t].
*/


#ifndef MPFR_CHEBPOLY_LODE_H
#define MPFR_CHEBPOLY_LODE_H


#include<stdio.h>
#include<stdlib.h>

#include <gmp.h>
#include <mpfr.h>
#include <mpfi.h>
#include <mpfi_io.h>

#include "mpfr_poly.h"
#include "mpfi_poly.h"
#include "mpfr_chebpoly.h"
#include "mpfi_chebpoly.h"

// structure for a linear differential equation with polynomial coefficients
// represent the equation y^(r) + a[r-1].y^(r-1) + ... + a[1].y' + a[0].y = 0
// where r = order
typedef struct {
  long order;
  mpfr_chebpoly_ptr a;
} mpfr_chebpoly_lode_struct;

typedef mpfr_chebpoly_lode_struct mpfr_chebpoly_lode_t[1];

typedef mpfr_chebpoly_lode_struct * mpfr_chebpoly_lode_ptr;

typedef const mpfr_chebpoly_lode_struct * mpfr_chebpoly_lode_srcptr;


/* basic manipulations */

void mpfr_chebpoly_lode_init(mpfr_chebpoly_lode_t L);

void mpfr_chebpoly_lode_clear(mpfr_chebpoly_lode_t L);

void mpfr_chebpoly_lode_set(mpfr_chebpoly_lode_t L, const mpfr_chebpoly_lode_t M);

void mpfr_chebpoly_lode_swap(mpfr_chebpoly_lode_t L, mpfr_chebpoly_lode_t M);

void mpfr_chebpoly_lode_set_order(mpfr_chebpoly_lode_t L, long order);


/* evaluation on polynomials : compute P = L.Q */

void mpfr_chebpoly_lode_evaluate_fr(mpfr_chebpoly_t P, const mpfr_chebpoly_lode_t L, const mpfr_chebpoly_t Q);

void mpfr_chebpoly_lode_evaluate_fi(mpfi_chebpoly_t P, const mpfr_chebpoly_lode_t L, const mpfi_chebpoly_t Q);


/* print */

void mpfr_chebpoly_lode_print(const mpfr_chebpoly_lode_t L, const char * var, const char * Cheb_var, size_t digits);



/* transpose from x-D form to D-x form and conversely */
// x-D form : L = D^r + a_{r-1} * D^(r-1) + ... + a_1 * D + a_0
// D-x form : L' = (-D)^r + (-D)^(r-1) * b_{r-1} + ... - D * b_1 + b_0
// this transpose function maps a to b (and b to a) st L = (-1)^r L'
// note: involutive function, can be used in both directions
void mpfr_chebpoly_lode_transpose(mpfr_chebpoly_ptr b, mpfr_chebpoly_srcptr a, long r);

/* transpose initial conditions */
// x-D initial conditions: y^(i) = v[i]
// D-x initial conditions: y^[i] = w[i]
// where y^[0] = y and y^[i+1] = -Dy^[i] + a_{r-1-i} y

// v -> w
// take as input the coefficients b[i] of the D-x form differential operator
void mpfr_chebpoly_lode_transpose_init_xDtoDx(mpfr_ptr w, mpfr_srcptr v, mpfr_chebpoly_srcptr b, long r);

// w -> v
// take as input the coefficients b[i] of the D-x form differential operator
void mpfr_chebpoly_lode_transpose_init_DxtoxD(mpfr_ptr v, mpfr_srcptr w, mpfr_chebpoly_srcptr b, long r);

#endif
