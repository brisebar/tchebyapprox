
/*
This file is part of the TchebyApprox library.

The type [double_chebpoly_t] implements polynomials in Chebyshev basis
with coefficients of type [double].
[degree] = degree of the polynomial (-1 = null polynomial).
coeffs[0], ... , coeffs[degree-1] = coefficients in Chebyshev basis.
*/


#ifndef DOUBLE_CHEBPOLY_H
#define DOUBLE_CHEBPOLY_H

#include <stdio.h>
#include <stdlib.h>

#include <gmp.h>
#include <mpfr.h>

#include "double_operations.h"

#include "double_poly.h"
#include "double_vec.h"


// deg is always the exact degree of the polynomial
// by convention, deg(0) = -1

typedef struct double_chebpoly_struct {
  long degree;
  double_ptr coeffs;
} double_chebpoly_struct;

typedef double_chebpoly_struct double_chebpoly_t[1];
typedef double_chebpoly_struct * double_chebpoly_ptr;
typedef const double_chebpoly_struct * double_chebpoly_srcptr;
typedef double_chebpoly_ptr * double_chebpoly_ptr_ptr;
typedef double_chebpoly_ptr_ptr * double_chebpoly_ptr_ptr_ptr;


/* memory management */

// init with P = 0
void double_chebpoly_init(double_chebpoly_t P);

// clear P
void double_chebpoly_clear(double_chebpoly_t P);

// set n as degree for P
void double_chebpoly_set_degree(double_chebpoly_t P, long n);

// set P in canonical form (exact degree)
void double_chebpoly_normalise(double_chebpoly_t P);



/* basic manipulations and assignments */

#define _double_chebpoly_set _double_poly_set

// copy Q into P
void double_chebpoly_set(double_chebpoly_t P, const double_chebpoly_t Q);

// swap P and Q efficiently
void double_chebpoly_swap(double_chebpoly_t P, double_chebpoly_t Q);

// set the n-th coefficient of P to c
void double_chebpoly_set_coeff_si(double_chebpoly_t P, long n, long c);

// set the n-th coefficient of P to c
void double_chebpoly_set_coeff_z(double_chebpoly_t P, long n, const mpz_t c);

// set the n-th coefficient of P to c
void double_chebpoly_set_coeff_q(double_chebpoly_t P, long n, const mpq_t c);

// set the n-th coefficient of P to c
void double_chebpoly_set_coeff_d(double_chebpoly_t P, long n, const double_t c);

// set the n-th coefficient of P to c
void double_chebpoly_set_coeff_fr(double_chebpoly_t P, long n, const mpfr_t c);

// get the degree of P
long double_chebpoly_degree(const double_chebpoly_t P);

// get the n-th coefficient of P
void double_chebpoly_get_coeff(double_t c, const double_chebpoly_t P, long n);

// get a pointer to the n-th coefficient of P
// return NULL if n < 0 or if n > degree(P)
#define double_chebpoly_get_coeff_ptr(P, n) (((n) <= (P)->degree) && (n >= 0) ? (P)->coeffs + (n) : NULL)

// set P to 0
void double_chebpoly_zero(double_chebpoly_t P);

#define _double_chebpoly_monic _double_poly_monic

// set P to (1/c)*Q where c is the leading coefficient of P in the Chebyshev basis
void double_chebpoly_monic(double_chebpoly_t P, const double_chebpoly_t Q);



/* shifting, reverse */

#define _double_chebpoly_shift_left _double_poly_shift_left

// set P to Q*X^k
void double_chebpoly_shift_left(double_chebpoly_t P, const double_chebpoly_t Q, unsigned long k);

#define _double_chebpoly_shift_right _double_poly_shift_right

// set P to Q/X^k
void double_chebpoly_shift_right(double_chebpoly_t P, const double_chebpoly_t Q, unsigned long k);

// reverse a list of double of size (n+1)
#define _double_chebpoly_reverse _double_poly_reverse

// set P to Q(X^-1)*X^(degree(Q))
void double_chebpoly_reverse(double_chebpoly_t P, const double_chebpoly_t Q);


/* norm 1 in the Chebyshev basis */

#define _double_chebpoly_1norm _double_poly_1norm

void double_chebpoly_1norm(double_t y, const double_chebpoly_t P);

#define _double_chebpoly_1norm_ubound _double_poly_1norm_ubound

void double_chebpoly_1norm_ubound(double_t y, const double_chebpoly_t P);

#define _double_chebpoly_1norm_lbound _double_poly_1norm_lbound

void double_chebpoly_1norm_lbound(double_t y, const double_chebpoly_t Q);

#define _double_chebpoly_1norm_fi _double_poly_1norm_fi

void double_chebpoly_1norm_fi(mpfi_t y, const double_chebpoly_t Q);


/* test functions */

#define double_chebpoly_is_zero(P) ((P)->degree < 0)

#define double_chebpoly_is_constant(P) ((P)->degree == 0)

#define double_chebpoly_is_monic(P) (double_cmp_si((P)->coeffs + (P)->degree), 1) == 0)

#define _double_chebpoly_equal _double_poly_equal

int double_chebpoly_equal(const double_chebpoly_t P, const double_chebpoly_t Q);


/* evaluation */

void _double_chebpoly_evaluate_si(double_t y, double_srcptr P, long n, long x);

// set y to P(x)
void double_chebpoly_evaluate_si(double_t y, const double_chebpoly_t P, long x);

void _double_chebpoly_evaluate_z(double_t y, double_srcptr P, long n, const mpz_t x);

// set y to P(x)
void double_chebpoly_evaluate_z(double_t y, const double_chebpoly_t P, const mpz_t x);

void _double_chebpoly_evaluate_q(double_t y, double_srcptr P, long n, const mpq_t x);

// set y to P(x)
void double_chebpoly_evaluate_q(double_t y, const double_chebpoly_t P, const mpq_t x);

void _double_chebpoly_evaluate_d(double_t y, double_srcptr P, long n, const double_t x);

// set y to P(x)
void double_chebpoly_evaluate_d(double_t y, const double_chebpoly_t P, const double_t x);

void _double_chebpoly_evaluate_fr(mpfr_t y, double_srcptr P, long n, const mpfr_t x);

// set y to P(x)
void double_chebpoly_evaluate_fr(mpfr_t y, const double_chebpoly_t P, const mpfr_t x);

void _double_chebpoly_evaluate_fi(mpfi_t y, double_srcptr P, long n, const mpfi_t x);

// set y to P(x)
void double_chebpoly_evaluate_fi(mpfi_t y, const double_chebpoly_t P, const mpfi_t x);

// set y to P(-1)
void double_chebpoly_evaluate_1(double_t y, const double_chebpoly_t P);

// set y to P(0)
void double_chebpoly_evaluate0(double_t y, const double_chebpoly_t P);

// set y to P(1)
void double_chebpoly_evaluate1(double_t y, const double_chebpoly_t P);


/* addition and substraction */

#define _double_chebpoly_add _double_poly_add

// set P := Q + R
void double_chebpoly_add(double_chebpoly_t P, const double_chebpoly_t Q, const double_chebpoly_t R);

#define _double_chebpoly_sub _double_poly_sub

// set P := Q - R
void double_chebpoly_sub(double_chebpoly_t P, const double_chebpoly_t Q, const double_chebpoly_t R);

#define _double_chebpoly_neg _double_poly_neg

// set P := -Q
void double_chebpoly_neg(double_chebpoly_t P, const double_chebpoly_t Q);


/* scalar multiplication and division */

#define _double_chebpoly_scalar_mul_si _double_poly_scalar_mul_si

// set P := c*Q
void double_chebpoly_scalar_mul_si(double_chebpoly_t P, const double_chebpoly_t Q, long c);

#define _double_chebpoly_scalar_mul_z _double_poly_scalar_mul_z

// set P := c*Q
void double_chebpoly_scalar_mul_z(double_chebpoly_t P, const double_chebpoly_t Q, const mpz_t c);

#define _double_chebpoly_scalar_mul_q _double_poly_scalar_mul_q

// set P := c*Q
void double_chebpoly_scalar_mul_q(double_chebpoly_t P, const double_chebpoly_t Q, const mpq_t c);

#define _double_chebpoly_scalar_mul_d _double_poly_scalar_mul_d

// set P := c*Q
void double_chebpoly_scalar_mul_d(double_chebpoly_t P, const double_chebpoly_t Q, const double_t c);

#define _double_chebpoly_scalar_mul_fr _double_poly_scalar_mul_fr

// set P := c*Q
void double_chebpoly_scalar_mul_fr(double_chebpoly_t P, const double_chebpoly_t Q, const mpfr_t c);

#define _double_chebpoly_scalar_mul_2si _double_poly_scalar_mul_2si

// set P := 2^k*Q
void double_chebpoly_scalar_mul_2si(double_chebpoly_t P, const double_chebpoly_t Q, long k);


#define _double_chebpoly_scalar_div_si _double_poly_scalar_div_si

// set P := (1/c)*Q
void double_chebpoly_scalar_div_si(double_chebpoly_t P, const double_chebpoly_t Q, long c);

#define _double_chebpoly_scalar_div_z _double_poly_scalar_div_z

// set P := (1/c)*Q
void double_chebpoly_scalar_div_z(double_chebpoly_t P, const double_chebpoly_t Q, const mpz_t c);

#define _double_chebpoly_scalar_div_q _double_poly_scalar_div_q

// set P := (1/c)*Q
void double_chebpoly_scalar_div_q(double_chebpoly_t P, const double_chebpoly_t Q, const mpq_t c);

#define _double_chebpoly_scalar_div_d _double_poly_scalar_div_d

// set P := (1/c)*Q
void double_chebpoly_scalar_div_d(double_chebpoly_t P, const double_chebpoly_t Q, const double_t c);

#define _double_chebpoly_scalar_div_fr _double_poly_scalar_div_fr

// set P := (1/c)*Q
void double_chebpoly_scalar_div_fr(double_chebpoly_t P, const double_chebpoly_t Q, const mpfr_t c);

#define _double_chebpoly_scalar_div_2si _double_poly_scalar_div_2si

// set P := 2^-k*Q
void double_chebpoly_scalar_div_2si(double_chebpoly_t P, const double_chebpoly_t Q, long k);




/* multiplication */

void _double_chebpoly_mul(double_ptr P, double_srcptr Q, long n, double_srcptr R, long m);

// set P := Q * R
void double_chebpoly_mul(double_chebpoly_t P, const double_chebpoly_t Q, const double_chebpoly_t R);


/* exponentiation */

// set P := Q^e
void double_chebpoly_pow(double_chebpoly_t P, const double_chebpoly_t Q, unsigned long e);


/* composition */

// set P = Q(R)
void double_chebpoly_comp(double_chebpoly_t P, const double_chebpoly_t Q, const double_chebpoly_t R);


/* derivative and antiderivative */

void _double_chebpoly_derivative(double_ptr P, double_srcptr Q, long n);

// set P := Q'
void double_chebpoly_derivative(double_chebpoly_t P, const double_chebpoly_t Q);

void _double_chebpoly_antiderivative(double_ptr P, double_srcptr Q, long n);

// set P st P' = Q and the 0-th coeff of P is 0
void double_chebpoly_antiderivative(double_chebpoly_t P, const double_chebpoly_t Q);

// set P st P' = Q and P(0) = 0
void double_chebpoly_antiderivative0(double_chebpoly_t P, const double_chebpoly_t Q);

// set P st P' = Q and P(-1) = 0
void double_chebpoly_antiderivative_1(double_chebpoly_t P, const double_chebpoly_t Q);

// set P st P' = Q and P(1) = 0
void double_chebpoly_antiderivative1(double_chebpoly_t P, const double_chebpoly_t Q);


/* approximate inverse */
/* note : validation functions can be found in mpfi_chebpoly.h */

// computes F of degree N st F ~ 1/Q
void double_chebpoly_inverse_approx(double_chebpoly_t F, const double_chebpoly_t Q, long N);

// computes F of degree N st F ~ P/Q
void double_chebpoly_fract_approx(double_chebpoly_t F, const double_chebpoly_t P, const double_chebpoly_t Q, long N);

// compute F of degree N st F ~ P/Q and G of degree N st G ~ 1/Q
void double_chebpoly_fract_inverse_approx(double_chebpoly_t F, double_chebpoly_t G, const double_chebpoly_t P, const double_chebpoly_t Q, long N);


/* approximate square root and inverse square root */

// compute F of degree N st F ~ sqrt(G)
void double_chebpoly_sqrt_approx(double_chebpoly_t F, const double_chebpoly_t G, long N);

// compute F of degree N st F ~ 1/sqrt(G)
void double_chebpoly_inv_sqrt_approx(double_chebpoly_t F, const double_chebpoly_t G, long N);

// compute F1 and F2 of degree N st F1 ~ sqrt(G) and F2 ~ 1/sqrt(G)
void double_chebpoly_sqrt_inv_sqrt_approx(double_chebpoly_t F1, double_chebpoly_t F2, const double_chebpoly_t G, long N);



/* conversion to and from the monomial basis */

void _double_chebpoly_get_double_poly(double_ptr P, double_srcptr Q, long n);

// set in P the conversion of Q (in the Chebyshev basis) into the monomial basis
void double_chebpoly_get_double_poly(double_poly_t P, const double_chebpoly_t Q);

void _double_chebpoly_set_double_poly(double_ptr P, double_srcptr Q, long n);

// set in P the conversion of Q (in the monomial basis) into the Chebyshev basis
void double_chebpoly_set_double_poly(double_chebpoly_t P, const double_poly_t Q);


/* Chebyshev interpolation */

// Chebyshev interpolation
// F[k] contains the value of the function to be interpolated, at x_k = cos(pi * (2k+1)/(2n+2))
// for 0 <= k <= n
// the interpolant is sum_(0 <= i <= n) P[i] T_i(x)
void _double_chebpoly_interpolate(double_ptr P, double_srcptr F, long n);

// Cheyshev interpolation
// vector F of length n+1 => polynomial P of degree n
void double_chebpoly_interpolate(double_chebpoly_t P, const double_vec_t F);


/* display function */


void _double_chebpoly_print(double_srcptr P, long n, const char * var);

// display the polynomial P with 'var' as indeterminate symbol
// 'var' is a 0-terminated string
void double_chebpoly_print(const double_chebpoly_t P, const char * var);




#endif
