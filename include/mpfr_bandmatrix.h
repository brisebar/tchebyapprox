
/*
This file is part of the TchebyApprox library.

The type [mpfr_bandmatrix_t] implements ([Hwidth],[Dwidth])
almost-banded square matrices of [mpfr_t of order [dim].
Non-zero coefficients are located:
  - in rows between indicies from 0 to Hwidth-1 (initial coefficients):
    H[i][0], ... , H[i][dim-1]  i = 0 ... Hwidth-1
  - at each row of index [i], between indicies from [max(i-Dwidth,0)] to [min(ind+Dwidth,dim-1)] (diagonal coefficients):
    D[i][0], ..., D[i][Dwidth], ... , D[i][2*Dwidth]   i = 0 ... dim-1
Normal form of the matrix: the entries in the initial band overlapped
by entries from the diagonal band are ignored (considered to be zero).
[mpfr_bandmatrix_normalise]: convert to normal form
by adding the overlapped initial entries to the corresponding
diagonal entries and set them to zero.

The type [mpfr_bandmatrix_QRdecomp_t] implements QR-decomposition
of an almost-banded matrix as used in Olver and Townsend's algorithm.
Implementation details are not necessary to use this decomposition
in linear algebra procedures provided by the library.
*/


#ifndef MPFR_BANDMATRIX_H
#define MPFR_BANDMATRIX_H


#include <stdio.h>
#include <stdlib.h>

#include <gmp.h>
#include <mpfr.h>
#include <mpfi.h>
#include <mpfi_io.h>

#include "double_operations.h"

#include "mpfr_vec.h"
#include "mpfi_vec.h"
#include "mpfr_bandvec.h"
#include "mpfi_bandvec.h"

#include "double_bandmatrix.h"

typedef mpfr_ptr * mpfr_ptr_ptr;

typedef mpfr_ptr ** mpfr_ptr_ptr_ptr;


typedef struct {
  long dim;
  long Hwidth;
  long Dwidth;
  mpfr_ptr_ptr H;
  mpfr_ptr_ptr D;
} mpfr_bandmatrix_struct;

typedef mpfr_bandmatrix_struct mpfr_bandmatrix_t[1];

/* basic manipulations */

void mpfr_bandmatrix_init(mpfr_bandmatrix_t M);

void mpfr_bandmatrix_clear(mpfr_bandmatrix_t M);

void mpfr_bandmatrix_set(mpfr_bandmatrix_t M, const mpfr_bandmatrix_t N);

void mpfr_bandmatrix_set_double_bandmatrix(mpfr_bandmatrix_t M, const double_bandmatrix_t N);

void mpfr_bandmatrix_get_double_bandmatrix(double_bandmatrix_t M, const mpfr_bandmatrix_t N);

void mpfr_bandmatrix_swap(mpfr_bandmatrix_t M, mpfr_bandmatrix_t N);

void mpfr_bandmatrix_set_params(mpfr_bandmatrix_t M, long dim, long Hwidth, long Dwidth);

void mpfr_bandmatrix_zero(mpfr_bandmatrix_t M);

void mpfr_bandmatrix_normalise(mpfr_bandmatrix_t M);

void mpfr_bandmatrix_get_column(mpfr_bandvec_t V, const mpfr_bandmatrix_t M, long i);

void mpfr_bandmatrix_set_column(mpfr_bandmatrix_t M, const mpfr_bandvec_t V);


/* evaluation on vectors */

// on dense vectors

void _mpfr_bandmatrix_evaluate_fr(mpfr_ptr V, const mpfr_bandmatrix_t M, mpfr_srcptr W);

void mpfr_bandmatrix_evaluate_fr(mpfr_vec_t V, const mpfr_bandmatrix_t M, const mpfr_vec_t W);

void _mpfr_bandmatrix_evaluate_fi(mpfi_ptr V, const mpfr_bandmatrix_t M, mpfi_srcptr W);

void mpfr_bandmatrix_evaluate_fi(mpfi_vec_t V, const mpfr_bandmatrix_t M, const mpfi_vec_t W);

// on sparse banded vectors

// all mpfr_t must be distinct !
void _mpfr_bandmatrix_evaluate_band_fr(mpfr_ptr VH, mpfr_ptr VD, long VHwidth, long VDwidth, const mpfr_bandmatrix_t M, mpfr_srcptr WH, mpfr_srcptr WD, long WHwidth, long WDwidth, long ind);

void mpfr_bandmatrix_evaluate_band_fr(mpfr_bandvec_t V, const mpfr_bandmatrix_t M, const mpfr_bandvec_t W);

// all mpfi_t must be distinct !
void _mpfr_bandmatrix_evaluate_band_fi(mpfi_ptr VH, mpfi_ptr VD, long VHwidth, long VDwidth, const mpfr_bandmatrix_t M, mpfi_srcptr WH, mpfi_srcptr WD, long WHwidth, long WDwidth, long ind);

void mpfr_bandmatrix_evaluate_band_fi(mpfi_bandvec_t V, const mpfr_bandmatrix_t M, const mpfi_bandvec_t W);



/* norm 1 computations */


void mpfr_bandmatrix_1norm_ubound(mpfr_t norm, const mpfr_bandmatrix_t M);

void mpfr_bandmatrix_1norm_lbound(mpfr_t norm, const mpfr_bandmatrix_t M);

void mpfr_bandmatrix_1norm_fi(mpfi_t norm, const mpfr_bandmatrix_t M);



/* arithmetic operations */


void mpfr_bandmatrix_add(mpfr_bandmatrix_t M, const mpfr_bandmatrix_t N, const mpfr_bandmatrix_t P);

void mpfr_bandmatrix_sub(mpfr_bandmatrix_t M, const mpfr_bandmatrix_t N, const mpfr_bandmatrix_t P);

void mpfr_bandmatrix_neg(mpfr_bandmatrix_t M, const mpfr_bandmatrix_t N);

void mpfr_bandmatrix_mul(mpfr_bandmatrix_t M, const mpfr_bandmatrix_t N, const mpfr_bandmatrix_t P);

//void mpfr_bandmatrix_mul_bis(mpfr_bandmatrix_t P, const mpfr_bandmatrix_t M, const mpfr_bandmatrix_t N);


/* split and marge matrices of matrices */


// merge n*n a-b matrices of same size N  into a 4*N a-b matrix
void mpfr_bandmatrix_merge(mpfr_bandmatrix_t A, mpfr_bandmatrix_struct ** B, long n);

// split an n*N order a-b matrix into n*n a-b matrices
void mpfr_bandmatrix_split(mpfr_bandmatrix_struct ** A, const mpfr_bandmatrix_t B, long n);










/* QR decomposition of a banded matrix to solve a linear system */


typedef struct {
  long dim;
  long Hwidth;
  long Dwidth;
  mpfr_ptr_ptr H;
  mpfr_ptr_ptr D;
  mpfr_ptr_ptr B;
  long * XCH;
  mpfr_ptr_ptr_ptr GR;
} mpfr_bandmatrix_QRdecomp_struct;

typedef mpfr_bandmatrix_QRdecomp_struct mpfr_bandmatrix_QRdecomp_t[1];


/* basic manipulations */


void mpfr_bandmatrix_QRdecomp_init(mpfr_bandmatrix_QRdecomp_t M);

void mpfr_bandmatrix_QRdecomp_clear(mpfr_bandmatrix_QRdecomp_t M);

void mpfr_bandmatrix_QRdecomp_set(mpfr_bandmatrix_QRdecomp_t M, const mpfr_bandmatrix_QRdecomp_t N);

void mpfr_bandmatrix_QRdecomp_swap(mpfr_bandmatrix_QRdecomp_t M, mpfr_bandmatrix_QRdecomp_t N);

void mpfr_bandmatrix_QRdecomp_set_params(mpfr_bandmatrix_QRdecomp_t M, long dim, long Hwidth, long Dwidth);

void mpfr_bandmatrix_QRdecomp_zero(mpfr_bandmatrix_QRdecomp_t M);


/* get the QR decomposition of a banded matrix */

//void _mpfr_bandmatrix_get_QRdecomp_GR(mpfr_bandmatrix_QRdecomp_t N, long i, long j);

int mpfr_bandmatrix_get_QRdecomp(mpfr_bandmatrix_QRdecomp_t N, const mpfr_bandmatrix_t M);


/* solve the linear system using the QR decomposition */

void _mpfr_bandmatrix_QRdecomp_solve_fr(mpfr_ptr V, const mpfr_bandmatrix_QRdecomp_t N, mpfr_srcptr W);

void mpfr_bandmatrix_QRdecomp_solve_fr(mpfr_vec_t V, const mpfr_bandmatrix_QRdecomp_t N, const mpfr_vec_t W);

void _mpfr_bandmatrix_QRdecomp_solve_fi(mpfi_ptr V, const mpfr_bandmatrix_QRdecomp_t N, mpfi_srcptr W);

void mpfr_bandmatrix_QRdecomp_solve_fi(mpfi_vec_t V, const mpfr_bandmatrix_QRdecomp_t N, const mpfi_vec_t W);

/* norm 1 computations for the inverse */

// 1-norm of the inverse, in quadratic time

void mpfr_bandmatrix_QRdecomp_inv1norm_ubound(mpfr_t norm, const mpfr_bandmatrix_QRdecomp_t N);

void mpfr_bandmatrix_QRdecomp_inv1norm_lbound(mpfr_t norm, const mpfr_bandmatrix_QRdecomp_t N);

void mpfr_bandmatrix_QRdecomp_inv1norm_fi(mpfi_t norm, const mpfr_bandmatrix_QRdecomp_t N);



// 1-norm of the inverse, overestimated but in linear time
void mpfr_bandmatrix_QRdecomp_inv1norm_overestimate(mpfr_t bound, const mpfr_bandmatrix_QRdecomp_t N);


// approximate the inverse of a banded matrix (given under its QR decomposition) with a banded matrix
void mpfr_bandmatrix_QRdecomp_approx_band_inverse(mpfr_bandmatrix_t P, const mpfr_bandmatrix_QRdecomp_t N, long Hwidth, long Dwidth);

/*
// old version
void mpfr_bandmatrix_QRdecomp_approx_band_inverse_old(mpfr_bandmatrix_t P, const mpfr_bandmatrix_QRdecomp_t N, long Hwidth, long Dwidth);
*/



#endif
