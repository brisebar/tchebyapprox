
/*
This file is part of the TchebyApprox library.

Here are the routines to rigorously solve a vectorial integral equation
described by a [chebmodel_vec_intop_t], using a Newton-like method.

This generalizes the scalar case described in [chebmodel_intop_newton.h]
*/


#ifndef CHEBMODEL_VEC_INTOP_NEWTON_H
#define CHEBMODEL_VEC_INTOP_NEWTON_H

#include <stdio.h>
#include <stdlib.h>

#include <gmp.h>
#include <mpfr.h>
#include <mpfi.h>
#include <mpfi_io.h>

#include "chebmodel_vec.h"
#include "double_bandmatrix.h"
#include "mpfr_bandmatrix.h"
#include "mpfi_bandmatrix.h"
#include "chebmodel_vec_intop.h"
#include "mpfi_chebpoly_vec_intop_newton.h"
#include "chebmodel_intop_newton.h"

/* bound for the Newton Operator */
// 1 = N too small  2 = imprecise numerical inverse  3 = the cumulated error of the coefficients of K is too big
//int chebmodel_vec_intop_newton_bound_d(double_ptr_ptr bound, const chebmodel_vec_intop_t K, const mpfi_chebpoly_vec_intop_t Ktrunc, mpfi_bandmatrix_struct ** M_Ktrunc, const mpfi_bandmatrix_t M_Ktrunc_glob, double_bandmatrix_struct ** M_Ktrunc_inv, const double_bandmatrix_t M_Ktrunc_inv);
int chebmodel_vec_intop_newton_bound_fr(mpfr_ptr_ptr bound, const chebmodel_vec_intop_t K, const mpfi_chebpoly_vec_intop_t Ktrunc, mpfi_bandmatrix_struct ** M_Ktrunc, const mpfi_bandmatrix_t M_Ktrunc_glob, mpfr_bandmatrix_struct ** M_Ktrunc_inv, const mpfr_bandmatrix_t M_Ktrunc_inv_glob);
#define chebmodel_vec_intop_newton_bound chebmodel_vec_intop_newton_bound_fr


// start from N = init_N and double N the paramaters of the approx inverse until the Newton Operator is contractant

//void chebmodel_vec_intop_newton_contract_d(double_ptr_ptr bound, double_bandmatrix_struct ** M_K_inv, const chebmodel_vec_intop_t K, long init_N);
void chebmodel_vec_intop_newton_contract_fr(mpfr_ptr_ptr bound, mpfr_bandmatrix_struct ** M_K_inv, const chebmodel_vec_intop_t K, long init_N);
#define chebmodel_vec_intop_newton_contract chebmodel_vec_intop_newton_contract_fr

//void chebmodel_intop_newton_certify_d(double_t bound, double_bandmatrix_t M_K_inv, const chebmodel_intop_t K, long N, long h2, long d2);
//void chebmodel_intop_newton_certify_fr(mpfr_t bound, mpfr_bandmatrix_t M_K_inv, const chebmodel_intop_t K, long N, long h2, long d2);

//void chebmodel_vec_intop_newton_validate_sol_aux_d(mpfr_ptr bound, const chebmodel_vec_intop_t K, double_ptr_ptr T_norm, double_bandmatrix_struct ** M_K_inv, const chebmodel_vec_t G, const mpfr_chebpoly_vec_t P);
void chebmodel_vec_intop_newton_validate_sol_aux_fr(mpfr_ptr bound, const chebmodel_vec_intop_t K, mpfr_ptr_ptr T_norm, mpfr_bandmatrix_struct ** M_K_inv, const chebmodel_vec_t G, const mpfr_chebpoly_vec_t P);
#define chebmodel_vec_intop_newton_validate_sol_aux chebmodel_vec_intop_newton_validate_sol_d

//void chebmodel_vec_intop_newton_validate_sol_d(mpfr_ptr bound, const chebmodel_vec_intop_t K, const chebmodel_vec_t G, const mpfr_chebpoly_vec_t P, long init_N);
void chebmodel_vec_intop_newton_validate_sol_fr(mpfr_ptr bound, const chebmodel_vec_intop_t K, const chebmodel_vec_t G, const mpfr_chebpoly_vec_t P, long init_N);
#define chebmodel_vec_intop_newton_validate_sol chebmodel_vec_intop_newton_validate_sol_fr

//void chebmodel_vec_lode_intop_newton_validate_sol_d(mpfr_ptr bound, const chebmodel_vec_lode_t L, const chebmodel_vec_t g, mpfi_ptr_ptr I, const mpfr_chebpoly_vec_t P);
void chebmodel_vec_lode_intop_newton_validate_sol_fr(mpfr_ptr bound, const chebmodel_vec_lode_t L, const chebmodel_vec_t g, mpfi_ptr_ptr I, const mpfr_chebpoly_vec_t P);
#define chebmodel_vec_lode_intop_newton_validate_sol chebmodel_vec_lode_intop_newton_validate_sol_fr


//void chebmodel_vec_intop_newton_solve_d(chebmodel_vec_t P, const chebmodel_vec_intop_t K, const chebmodel_vec_t G, long N);
void chebmodel_vec_intop_newton_solve_fr(chebmodel_vec_t P, const chebmodel_vec_intop_t K, const chebmodel_vec_t G, long N);
#define chebmodel_vec_intop_newton_solve chebmodel_vec_intop_newton_solve_fr

//void chebmodel_vec_lode_intop_newton_solve_d(chebmodel_vec_ptr Sols, const chebmodel_vec_lode_t L, const chebmodel_vec_t g, mpfi_ptr_ptr I, long N);
void chebmodel_vec_lode_intop_newton_solve_fr(chebmodel_vec_ptr Sols, const chebmodel_vec_lode_t L, const chebmodel_vec_t g, mpfi_ptr_ptr I, long N);
#define chebmodel_vec_lode_intop_newton_solve chebmodel_vec_lode_intop_newton_solve_fr


#endif
