
/*
This file is part of the TchebyApprox library.

The type [mpfr_poly_t] implements polynomials in monomial basis
with coefficients of type [mpfr_t].
[degree] = degree of the polynomial (-1 = null polynomial).
coeffs[0], ... , coeffs[degree-1] = coefficients in the monomial basis.
*/


#ifndef MPFR_POLY_H
#define MPFR_POLY_H

#include <stdio.h>
#include <stdlib.h>

#include <gmp.h>
#include <mpfr.h>

#include "double_poly.h"

// deg is always the exact degree of the polynomial
// by convention, deg(0) = -1

typedef struct mpfr_poly_struct {
  long degree;
  mpfr_ptr coeffs;
} mpfr_poly_struct;

typedef mpfr_poly_struct mpfr_poly_t[1];
typedef mpfr_poly_struct * mpfr_poly_ptr;
typedef const mpfr_poly_struct * mpfr_poly_srcptr;



/* memory management */

// init with P = 0
void mpfr_poly_init(mpfr_poly_t P);

// clear P
void mpfr_poly_clear(mpfr_poly_t P);

// set n as degree for P
void mpfr_poly_set_degree(mpfr_poly_t P, long n);

// set P in canonical form (exact degree)
void mpfr_poly_normalise(mpfr_poly_t P);



/* basic manipulations and assignments */

void _mpfr_poly_set(mpfr_ptr P, mpfr_srcptr Q, long n);

// copy Q into P
void mpfr_poly_set(mpfr_poly_t P, const mpfr_poly_t Q);

void _mpfr_poly_set_double_poly(mpfr_ptr P, double_srcptr Q, long n);

// get P from Q with double coefficients
void mpfr_poly_set_double_poly(mpfr_poly_t P, const double_poly_t Q);

void _mpfr_poly_get_double_poly(double_ptr P, mpfr_srcptr Q, long n);

// obtain Q (with double coefficients) from P
void mpfr_poly_get_double_poly(double_poly_t P, const mpfr_poly_t Q);

// swap P and Q efficiently
void mpfr_poly_swap(mpfr_poly_t P, mpfr_poly_t Q);

// set the n-th coefficient of P to c
void mpfr_poly_set_coeff_si(mpfr_poly_t P, long n, long c);

// set the n-th coefficient of P to c
void mpfr_poly_set_coeff_z(mpfr_poly_t P, long n, const mpz_t c);

// set the n-th coefficient of P to c
void mpfr_poly_set_coeff_q(mpfr_poly_t P, long n, const mpq_t c);

// set the n-th coefficient of P to c
void mpfr_poly_set_coeff_fr(mpfr_poly_t P, long n, const mpfr_t c);

// get the degree of P
long mpfr_poly_degree(const mpfr_poly_t P);

// get the n-th coefficient of P
void mpfr_poly_get_coeff(mpfr_t c, const mpfr_poly_t P, long n);

// get a pointer to the n-th coefficient of P
// return NULL if n < 0 or if n > degree(P)
#define mpfr_poly_get_coeff_ptr(P, n) (((n) <= (P)->degree) && (n >= 0) ? (P)->coeffs + (n) : NULL)

// set P to 0
void mpfr_poly_zero(mpfr_poly_t P);

// set P to (1/c)*Q where c is the leading coefficient of P
void _mpfr_poly_monic(mpfr_ptr P, const mpfr_srcptr Q, long n);

// set P to (1/c)*Q where c is the leading coefficient of P
void mpfr_poly_monic(mpfr_poly_t P, const mpfr_poly_t Q);


/* shifting, reverse */

void _mpfr_poly_shift_left(mpfr_ptr P, mpfr_srcptr Q, long n, unsigned long k);

// set P to Q*X^k
void mpfr_poly_shift_left(mpfr_poly_t P, const mpfr_poly_t Q, unsigned long k);

void _mpfr_poly_shift_right(mpfr_ptr P, mpfr_srcptr Q, long n, unsigned long k);

// set P to Q/X^k
void mpfr_poly_shift_right(mpfr_poly_t P, const mpfr_poly_t Q, unsigned long k);

// reverse a list of mpfr of size (n+1)
void _mpfr_poly_reverse(mpfr_ptr P, mpfr_srcptr Q, long n);

// set P to Q(X^-1)*X^(degree(Q))
void mpfr_poly_reverse(mpfr_poly_t P, const mpfr_poly_t Q);


/* norm 1 */

void _mpfr_poly_1norm(mpfr_t y, mpfr_srcptr P, long n);

void mpfr_poly_1norm(mpfr_t y, const mpfr_poly_t P);


/* test functions */

#define mpfr_poly_is_zero(P) ((P)->degree < 0)

#define mpfr_poly_is_constant(P) ((P)->degree == 0)

#define mpfr_poly_is_monic(P) (mpfr_cmp_si((P)->coeffs + (P)->degree, 1) == 0)

int _mpfr_poly_equal(mpfr_srcptr P, mpfr_srcptr Q, long n);

int mpfr_poly_equal(const mpfr_poly_t P, const mpfr_poly_t Q);


/* evaluation */

void _mpfr_poly_evaluate_si(mpfr_t y, mpfr_srcptr P, long n, long x);

// set y to P(x)
void mpfr_poly_evaluate_si(mpfr_t y, const mpfr_poly_t P, long x);

void _mpfr_poly_evaluate_z(mpfr_t y, mpfr_srcptr P, long n, const mpz_t x);

// set y to P(x)
void mpfr_poly_evaluate_z(mpfr_t y, const mpfr_poly_t P, const mpz_t x);

void _mpfr_poly_evaluate_q(mpfr_t y, mpfr_srcptr P, long n, const mpq_t x);

// set y to P(x)
void mpfr_poly_evaluate_q(mpfr_t y, const mpfr_poly_t P, const mpq_t x);

void _mpfr_poly_evaluate_fr(mpfr_t y, mpfr_srcptr P, long n, const mpfr_t x);

// set y to P(x)
void mpfr_poly_evaluate_fr(mpfr_t y, const mpfr_poly_t P, const mpfr_t x);


/* addition and substraction */

void _mpfr_poly_add(mpfr_ptr P, mpfr_srcptr Q, long n, mpfr_srcptr R, long m);

// set P := Q + R
void mpfr_poly_add(mpfr_poly_t P, const mpfr_poly_t Q, const mpfr_poly_t R);

void _mpfr_poly_sub(mpfr_ptr P, mpfr_srcptr Q, long n, mpfr_srcptr R, long m);

// set P := Q - R
void mpfr_poly_sub(mpfr_poly_t P, const mpfr_poly_t Q, const mpfr_poly_t R);

void _mpfr_poly_neg(mpfr_ptr P, mpfr_srcptr Q, long n);

// set P := -Q
void mpfr_poly_neg(mpfr_poly_t P, const mpfr_poly_t Q);


/* scalar multiplication and division */

void _mpfr_poly_scalar_mul_si(mpfr_ptr P, mpfr_srcptr Q, long n, long c);

// set P := c*Q
void mpfr_poly_scalar_mul_si(mpfr_poly_t P, const mpfr_poly_t Q, long c);

void _mpfr_poly_scalar_mul_z(mpfr_ptr P, mpfr_srcptr Q, long n, const mpz_t c);

// set P := c*Q
void mpfr_poly_scalar_mul_z(mpfr_poly_t P, const mpfr_poly_t Q, const mpz_t c);

void _mpfr_poly_scalar_mul_q(mpfr_ptr P, mpfr_srcptr Q, long n, const mpq_t c);

// set P := c*Q
void mpfr_poly_scalar_mul_q(mpfr_poly_t P, const mpfr_poly_t Q, const mpq_t c);

void _mpfr_poly_scalar_mul_d(mpfr_ptr P, mpfr_srcptr Q, long n, const double_t c);

// set P := c*Q
void mpfr_poly_scalar_mul_d(mpfr_poly_t P, const mpfr_poly_t Q, const double_t c);

void _mpfr_poly_scalar_mul_fr(mpfr_ptr P, mpfr_srcptr Q, long n, const mpfr_t c);

// set P := c*Q
void mpfr_poly_scalar_mul_fr(mpfr_poly_t P, const mpfr_poly_t Q, const mpfr_t c);

void _mpfr_poly_scalar_mul_2si(mpfr_ptr P, mpfr_srcptr Q, long n, long k);

// set P := 2^k*Q
void mpfr_poly_scalar_mul_2si(mpfr_poly_t P, const mpfr_poly_t Q, long k);


void _mpfr_poly_scalar_div_si(mpfr_ptr P, mpfr_srcptr Q, long n, long c);

// set P := (1/c)*Q
void mpfr_poly_scalar_div_si(mpfr_poly_t P, const mpfr_poly_t Q, long c);

void _mpfr_poly_scalar_div_z(mpfr_ptr P, mpfr_srcptr Q, long n, const mpz_t c);

// set P := (1/c)*Q
void mpfr_poly_scalar_div_z(mpfr_poly_t P, const mpfr_poly_t Q, const mpz_t c);

void _mpfr_poly_scalar_div_q(mpfr_ptr P, mpfr_srcptr Q, long n, const mpq_t c);

// set P := (1/c)*Q
void mpfr_poly_scalar_div_q(mpfr_poly_t P, const mpfr_poly_t Q, const mpq_t c);

void _mpfr_poly_scalar_div_d(mpfr_ptr P, mpfr_srcptr Q, long n, const double_t c);

// set P := (1/c)*Q
void mpfr_poly_scalar_div_d(mpfr_poly_t P, const mpfr_poly_t Q, const double_t c);

void _mpfr_poly_scalar_div_fr(mpfr_ptr P, mpfr_srcptr Q, long n, const mpfr_t c);

// set P := (1/c)*Q
void mpfr_poly_scalar_div_fr(mpfr_poly_t P, const mpfr_poly_t Q, const mpfr_t c);

void _mpfr_poly_scalar_div_2si(mpfr_ptr P, mpfr_srcptr Q, long n, long k);

// set P := 2^-k*Q
void mpfr_poly_scalar_div_2si(mpfr_poly_t P, const mpfr_poly_t Q, long k);




/* multiplication */

void _mpfr_poly_mul(mpfr_ptr P, mpfr_srcptr Q, long n, mpfr_srcptr R, long m);

// set P := Q * R
void mpfr_poly_mul(mpfr_poly_t P, const mpfr_poly_t Q, const mpfr_poly_t R);


/* exponentiation */

// set P := Q^e
void mpfr_poly_pow(mpfr_poly_t P, const mpfr_poly_t Q, unsigned long e);


/* derivative and antiderivative */

void _mpfr_poly_derivative(mpfr_ptr P, mpfr_srcptr Q, long n);

// set P := Q'
void mpfr_poly_derivative(mpfr_poly_t P, const mpfr_poly_t Q);

void _mpfr_poly_antiderivative(mpfr_ptr P, mpfr_srcptr Q, long n);

// set P st P' = Q and P(0) = 0
void mpfr_poly_antiderivative(mpfr_poly_t P, const mpfr_poly_t Q);


/* display function */


void _mpfr_poly_print(mpfr_srcptr P, long n, const char * var, size_t digits);

// display the polynomial P with 'var' as indeterminate symbol
// 'var' is a 0-terminated string
void mpfr_poly_print(const mpfr_poly_t P, const char * var, size_t digits);




#endif
