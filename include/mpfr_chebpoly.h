
/*
This file is part of the TchebyApprox library.

The type [mpfr_chebpoly_t] implements polynomials in Chebyshev basis
with coefficients of type [mpfr_t].
[degree] = degree of the polynomial (-1 = null polynomial).
coeffs[0], ... , coeffs[degree-1] = coefficients in Chebyshev basis.
*/


#ifndef MPFR_CHEBPOLY_H
#define MPFR_CHEBPOLY_H

#include <stdio.h>
#include <stdlib.h>

#include <gmp.h>
#include <mpfr.h>

#include "mpfr_poly.h"
#include "double_chebpoly.h"
#include "mpfr_vec.h"


// deg is always the exact degree of the polynomial
// by convention, deg(0) = -1

typedef struct mpfr_chebpoly_struct {
  long degree;
  mpfr_ptr coeffs;
} mpfr_chebpoly_struct;

typedef mpfr_chebpoly_struct mpfr_chebpoly_t[1];
typedef mpfr_chebpoly_struct * mpfr_chebpoly_ptr;
typedef const mpfr_chebpoly_struct * mpfr_chebpoly_srcptr;
typedef mpfr_chebpoly_ptr * mpfr_chebpoly_ptr_ptr;
typedef mpfr_chebpoly_ptr_ptr * mpfr_chebpoly_ptr_ptr_ptr;


/* memory management */

// init with P = 0
void mpfr_chebpoly_init(mpfr_chebpoly_t P);

// clear P
void mpfr_chebpoly_clear(mpfr_chebpoly_t P);

// set n as degree for P
void mpfr_chebpoly_set_degree(mpfr_chebpoly_t P, long n);

// set P in canonical form (exact degree)
void mpfr_chebpoly_normalise(mpfr_chebpoly_t P);



/* basic manipulations and assignments */

#define _mpfr_chebpoly_set _mpfr_poly_set

// copy Q into P
void mpfr_chebpoly_set(mpfr_chebpoly_t P, const mpfr_chebpoly_t Q);

#define _mpfr_chebpoly_set_double_chebpoly _mpfr_poly_set_double_poly

// get P from Q with double coefficients
void mpfr_chebpoly_set_double_chebpoly(mpfr_chebpoly_t P, const double_chebpoly_t Q);

#define _mpfr_chebpoly_get_double_chebpoly _mpfr_poly_get_double_poly

// obtain Q (with double coefficients) from P
void mpfr_chebpoly_get_double_chebpoly(double_chebpoly_t P, const mpfr_chebpoly_t Q);

// swap P and Q efficiently
void mpfr_chebpoly_swap(mpfr_chebpoly_t P, mpfr_chebpoly_t Q);

// set the n-th coefficient of P to c
void mpfr_chebpoly_set_coeff_si(mpfr_chebpoly_t P, long n, long c);

// set the n-th coefficient of P to c
void mpfr_chebpoly_set_coeff_z(mpfr_chebpoly_t P, long n, const mpz_t c);

// set the n-th coefficient of P to c
void mpfr_chebpoly_set_coeff_q(mpfr_chebpoly_t P, long n, const mpq_t c);

// set the n-th coefficient of P to c
void mpfr_chebpoly_set_coeff_fr(mpfr_chebpoly_t P, long n, const mpfr_t c);

// get the degree of P
long mpfr_chebpoly_degree(const mpfr_chebpoly_t P);

// get the n-th coefficient of P
void mpfr_chebpoly_get_coeff(mpfr_t c, const mpfr_chebpoly_t P, long n);

// get a pointer to the n-th coefficient of P
// return NULL if n < 0 or if n > degree(P)
#define mpfr_chebpoly_get_coeff_ptr(P, n) (((n) <= (P)->degree) && (n >= 0) ? (P)->coeffs + (n) : NULL)

// set P to 0
void mpfr_chebpoly_zero(mpfr_chebpoly_t P);

#define _mpfr_chebpoly_monic _mpfr_poly_monic

// set P to (1/c)*Q where c is the leading coefficient of P in the Chebyshev basis
void mpfr_chebpoly_monic(mpfr_chebpoly_t P, const mpfr_chebpoly_t Q);



/* shifting, reverse */

#define _mpfr_chebpoly_shift_left _mpfr_poly_shift_left

// set P to Q*X^k
void mpfr_chebpoly_shift_left(mpfr_chebpoly_t P, const mpfr_chebpoly_t Q, unsigned long k);

#define _mpfr_chebpoly_shift_right _mpfr_poly_shift_right

// set P to Q/X^k
void mpfr_chebpoly_shift_right(mpfr_chebpoly_t P, const mpfr_chebpoly_t Q, unsigned long k);

// reverse a list of mpfr of size (n+1)
#define _mpfr_chebpoly_reverse _mpfr_poly_reverse

// set P to Q(X^-1)*X^(degree(Q))
void mpfr_chebpoly_reverse(mpfr_chebpoly_t P, const mpfr_chebpoly_t Q);


/* norm 1 in the Chebyshev basis */

#define _mpfr_chebpoly_1norm _mpfr_poly_1norm

void mpfr_chebpoly_1norm(mpfr_t y, const mpfr_chebpoly_t P);


/* test functions */

#define mpfr_chebpoly_is_zero(P) ((P)->degree < 0)

#define mpfr_chebpoly_is_constant(P) ((P)->degree == 0)

#define mpfr_chebpoly_is_monic(P) (mpfr_cmp_si((P)->coeffs + (P)->degree), 1) == 0)

#define _mpfr_chebpoly_equal _mpfr_poly_equal

int mpfr_chebpoly_equal(const mpfr_chebpoly_t P, const mpfr_chebpoly_t Q);


/* evaluation */

void _mpfr_chebpoly_evaluate_si(mpfr_t y, mpfr_srcptr P, long n, long x);

// set y to P(x)
void mpfr_chebpoly_evaluate_si(mpfr_t y, const mpfr_chebpoly_t P, long x);

void _mpfr_chebpoly_evaluate_z(mpfr_t y, mpfr_srcptr P, long n, const mpz_t x);

// set y to P(x)
void mpfr_chebpoly_evaluate_z(mpfr_t y, const mpfr_chebpoly_t P, const mpz_t x);

void _mpfr_chebpoly_evaluate_q(mpfr_t y, mpfr_srcptr P, long n, const mpq_t x);

// set y to P(x)
void mpfr_chebpoly_evaluate_q(mpfr_t y, const mpfr_chebpoly_t P, const mpq_t x);

void _mpfr_chebpoly_evaluate_d(mpfr_t y, mpfr_srcptr P, long n, const double_t x);

// set y to P(x)
void mpfr_chebpoly_evaluate_d(mpfr_t y, const mpfr_chebpoly_t P, const double_t x);

void _mpfr_chebpoly_evaluate_fr(mpfr_t y, mpfr_srcptr P, long n, const mpfr_t x);

// set y to P(x)
void mpfr_chebpoly_evaluate_fr(mpfr_t y, const mpfr_chebpoly_t P, const mpfr_t x);

void _mpfr_chebpoly_evaluate_fi(mpfi_t y, mpfr_srcptr P, long n, const mpfi_t x);

// set y to P(x)
void mpfr_chebpoly_evaluate_fi(mpfi_t y, const mpfr_chebpoly_t P, const mpfi_t x);

// set y to P(-1)
void mpfr_chebpoly_evaluate_1(mpfr_t y, const mpfr_chebpoly_t P);

// set y to P(0)
void mpfr_chebpoly_evaluate0(mpfr_t y, const mpfr_chebpoly_t P);

// set y to P(1)
void mpfr_chebpoly_evaluate1(mpfr_t y, const mpfr_chebpoly_t P);



/* addition and substraction */

#define _mpfr_chebpoly_add _mpfr_poly_add

// set P := Q + R
void mpfr_chebpoly_add(mpfr_chebpoly_t P, const mpfr_chebpoly_t Q, const mpfr_chebpoly_t R);

#define _mpfr_chebpoly_sub _mpfr_poly_sub

// set P := Q - R
void mpfr_chebpoly_sub(mpfr_chebpoly_t P, const mpfr_chebpoly_t Q, const mpfr_chebpoly_t R);

#define _mpfr_chebpoly_neg _mpfr_poly_neg

// set P := -Q
void mpfr_chebpoly_neg(mpfr_chebpoly_t P, const mpfr_chebpoly_t Q);


/* scalar multiplication and division */

#define _mpfr_chebpoly_scalar_mul_si _mpfr_poly_scalar_mul_si

// set P := c*Q
void mpfr_chebpoly_scalar_mul_si(mpfr_chebpoly_t P, const mpfr_chebpoly_t Q, long c);

#define _mpfr_chebpoly_scalar_mul_z _mpfr_poly_scalar_mul_z

// set P := c*Q
void mpfr_chebpoly_scalar_mul_z(mpfr_chebpoly_t P, const mpfr_chebpoly_t Q, const mpz_t c);

#define _mpfr_chebpoly_scalar_mul_q _mpfr_poly_scalar_mul_q

// set P := c*Q
void mpfr_chebpoly_scalar_mul_q(mpfr_chebpoly_t P, const mpfr_chebpoly_t Q, const mpq_t c);

#define _mpfr_chebpoly_scalar_mul_d _mpfr_poly_scalar_mul_d

// set P := c*Q
void mpfr_chebpoly_scalar_mul_d(mpfr_chebpoly_t P, const mpfr_chebpoly_t Q, const double_t c);

#define _mpfr_chebpoly_scalar_mul_fr _mpfr_poly_scalar_mul_fr

// set P := c*Q
void mpfr_chebpoly_scalar_mul_fr(mpfr_chebpoly_t P, const mpfr_chebpoly_t Q, const mpfr_t c);

#define _mpfr_chebpoly_scalar_mul_2si _mpfr_poly_scalar_mul_2si

// set P := 2^k*Q
void mpfr_chebpoly_scalar_mul_2si(mpfr_chebpoly_t P, const mpfr_chebpoly_t Q, long k);


#define _mpfr_chebpoly_scalar_div_si _mpfr_poly_scalar_div_si

// set P := (1/c)*Q
void mpfr_chebpoly_scalar_div_si(mpfr_chebpoly_t P, const mpfr_chebpoly_t Q, long c);

#define _mpfr_chebpoly_scalar_div_z _mpfr_poly_scalar_div_z

// set P := (1/c)*Q
void mpfr_chebpoly_scalar_div_z(mpfr_chebpoly_t P, const mpfr_chebpoly_t Q, const mpz_t c);

#define _mpfr_chebpoly_scalar_div_q _mpfr_poly_scalar_div_q

// set P := (1/c)*Q
void mpfr_chebpoly_scalar_div_q(mpfr_chebpoly_t P, const mpfr_chebpoly_t Q, const mpq_t c);

#define _mpfr_chebpoly_scalar_div_d _mpfr_poly_scalar_div_d

// set P := (1/c)*Q
void mpfr_chebpoly_scalar_div_d(mpfr_chebpoly_t P, const mpfr_chebpoly_t Q, const double_t c);

#define _mpfr_chebpoly_scalar_div_fr _mpfr_poly_scalar_div_fr

// set P := (1/c)*Q
void mpfr_chebpoly_scalar_div_fr(mpfr_chebpoly_t P, const mpfr_chebpoly_t Q, const mpfr_t c);

#define _mpfr_chebpoly_scalar_div_2si _mpfr_poly_scalar_div_2si

// set P := 2^-k*Q
void mpfr_chebpoly_scalar_div_2si(mpfr_chebpoly_t P, const mpfr_chebpoly_t Q, long k);




/* multiplication */

void _mpfr_chebpoly_mul(mpfr_ptr P, mpfr_srcptr Q, long n, mpfr_srcptr R, long m);

// set P := Q * R
void mpfr_chebpoly_mul(mpfr_chebpoly_t P, const mpfr_chebpoly_t Q, const mpfr_chebpoly_t R);


/* exponentiation */

// set P := Q^e
void mpfr_chebpoly_pow(mpfr_chebpoly_t P, const mpfr_chebpoly_t Q, unsigned long e);


/* composition */

// set P = Q(R)
void mpfr_chebpoly_comp(mpfr_chebpoly_t P, const mpfr_chebpoly_t Q, const mpfr_chebpoly_t R);


/* derivative and antiderivative */

void _mpfr_chebpoly_derivative(mpfr_ptr P, mpfr_srcptr Q, long n);

// set P := Q'
void mpfr_chebpoly_derivative(mpfr_chebpoly_t P, const mpfr_chebpoly_t Q);

void _mpfr_chebpoly_antiderivative(mpfr_ptr P, mpfr_srcptr Q, long n);

// set P st P' = Q and the 0-th coeff of P is 0
void mpfr_chebpoly_antiderivative(mpfr_chebpoly_t P, const mpfr_chebpoly_t Q);

// set P st P' = Q and P(0) = 0
void mpfr_chebpoly_antiderivative0(mpfr_chebpoly_t P, const mpfr_chebpoly_t Q);

// set P st P' = Q and P(-1) = 0
void mpfr_chebpoly_antiderivative_1(mpfr_chebpoly_t P, const mpfr_chebpoly_t Q);

// set P st P' = Q and P(1) = 0
void mpfr_chebpoly_antiderivative1(mpfr_chebpoly_t P, const mpfr_chebpoly_t Q);


/* approximate inverse */
/* note : validation functions can be found in mpfi_chebpoly.h */

// computes F of degree N st F ~ 1/Q
void mpfr_chebpoly_inverse_approx(mpfr_chebpoly_t F, const mpfr_chebpoly_t Q, long N);

// computes F of degree N st F ~ P/Q
void mpfr_chebpoly_fract_approx(mpfr_chebpoly_t F, const mpfr_chebpoly_t P, const mpfr_chebpoly_t Q, long N);

// compute F of degree N st F ~ P/Q and G of degree N st G ~ 1/Q
void mpfr_chebpoly_fract_inverse_approx(mpfr_chebpoly_t F, mpfr_chebpoly_t G, const mpfr_chebpoly_t P, const mpfr_chebpoly_t Q, long N);


/* approximate square root and inverse square root */

// compute F of degree N st F ~ sqrt(G)
void mpfr_chebpoly_sqrt_approx(mpfr_chebpoly_t F, const mpfr_chebpoly_t G, long N);

// compute F of degree N st F ~ 1/sqrt(G)
void mpfr_chebpoly_inv_sqrt_approx(mpfr_chebpoly_t F, const mpfr_chebpoly_t G, long N);

// compute F1 and F2 of degree N st F1 ~ sqrt(G) and F2 ~ 1/sqrt(G)
void mpfr_chebpoly_sqrt_inv_sqrt_approx(mpfr_chebpoly_t F1, mpfr_chebpoly_t F2, const mpfr_chebpoly_t G, long N);



/* conversion to and from the monomial basis */

void _mpfr_chebpoly_get_mpfr_poly(mpfr_ptr P, mpfr_srcptr Q, long n);

// set in P the conversion of Q (in the Chebyshev basis) into the monomial basis
void mpfr_chebpoly_get_mpfr_poly(mpfr_poly_t P, const mpfr_chebpoly_t Q);

void _mpfr_chebpoly_set_mpfr_poly(mpfr_ptr P, mpfr_srcptr Q, long n);

// set in P the conversion of Q (in the monomial basis) into the Chebyshev basis
void mpfr_chebpoly_set_mpfr_poly(mpfr_chebpoly_t P, const mpfr_poly_t Q);


/* Chebyshev interpolation */

// Chebyshev interpolation
// F[k] contains the value of the function to be interpolated, at x_k = cos(pi * (2k+1)/(2n+2))
// for 0 <= k <= n
// the interpolant is sum_(0 <= i <= n) P[i] T_i(x)
void _mpfr_chebpoly_interpolate(mpfr_ptr P, mpfr_srcptr F, long n);

// Cheyshev interpolation
// vector F of length n+1 => polynomial P of degree n
void mpfr_chebpoly_interpolate(mpfr_chebpoly_t P, const mpfr_vec_t F);


/* display function */


void _mpfr_chebpoly_print(mpfr_srcptr P, long n, const char * var, size_t digits);

// display the polynomial P with 'var' as indeterminate symbol
// 'var' is a 0-terminated string
void mpfr_chebpoly_print(const mpfr_chebpoly_t P, const char * var, size_t digits);




#endif
