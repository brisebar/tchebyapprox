
/*
This file is part of the TchebyApprox library.

The type [chebmodel_intop_t] represents a vectorial 
linear Volterra integral operator of the second kind
of order [order] and dimension [dim]
derived from a [chebmodel_vec_lode_t].
The new unknown of this intagral equation is the last derivative
of the unknown in the differential equation.

The vectorial operator is represented by blocks,
where each intop[i][j] (0 <= i,j <= [dim]-1)
is a [chebmodel_intop_t].

Provided solving procedures for the integral equation
use Olver and Townsend's algorithm on almost-banded matrices.
*/


#ifndef CHEBMODEL_VEC_INTOP_H
#define CHEBMODEL_VEC_INTOP_H


#include <stdio.h>
#include <stdlib.h>

#include <gmp.h>
#include <mpfr.h>
#include <mpfi.h>
#include <mpfi_io.h>

#include "double_operations.h"

#include "mpfi_bandvec.h"
#include "mpfi_bandmatrix.h"
#include "double_chebpoly_vec.h"
#include "mpfr_chebpoly_vec.h"
#include "mpfi_chebpoly_vec.h"
#include "chebmodel_vec.h"
#include "chebmodel_vec_lode.h"
#include "chebmodel_intop.h"

#include "double_chebpoly_vec_intop.h"
#include "mpfr_chebpoly_vec_intop.h"
#include "mpfi_chebpoly_vec_intop.h"


typedef struct {
  long dim;
  chebmodel_intop_struct ** intop;
} chebmodel_vec_intop_struct;

typedef chebmodel_vec_intop_struct chebmodel_vec_intop_t[1];

typedef chebmodel_vec_intop_struct * chebmodel_vec_intop_ptr;

typedef const chebmodel_vec_intop_struct * chebmodel_vec_intop_srcptr;




void chebmodel_vec_intop_init(chebmodel_vec_intop_t K);

void chebmodel_vec_intop_clear(chebmodel_vec_intop_t K);

void chebmodel_vec_intop_set(chebmodel_vec_intop_t K, const chebmodel_vec_intop_t N);

void chebmodel_vec_intop_set_double_chebpoly_vec_intop(chebmodel_vec_intop_t K, const double_chebpoly_vec_intop_t N);

void chebmodel_vec_intop_set_mpfr_chebpoly_vec_intop(chebmodel_vec_intop_t K, const mpfr_chebpoly_vec_intop_t N);

void chebmodel_vec_intop_set_mpfi_chebpoly_vec_intop(chebmodel_vec_intop_t K, const mpfi_chebpoly_vec_intop_t N);

void chebmodel_vec_intop_get_double_chebpoly_vec_intop(double_chebpoly_vec_intop_t K, const chebmodel_vec_intop_t N);

void chebmodel_vec_intop_get_mpfr_chebpoly_vec_intop(mpfr_chebpoly_vec_intop_t K, const chebmodel_vec_intop_t N);

void chebmodel_vec_intop_get_mpfi_chebpoly_vec_intop(mpfi_chebpoly_vec_intop_t K, const chebmodel_vec_intop_t N);

void chebmodel_vec_intop_swap(chebmodel_vec_intop_t K, chebmodel_vec_intop_t N);

void chebmodel_vec_intop_set_dim(chebmodel_vec_intop_t K, long dim);

long chebmodel_vec_intop_Hwidth(const chebmodel_vec_intop_t K);

long chebmodel_vec_intop_Dwidth(const chebmodel_vec_intop_t K);


void chebmodel_vec_intop_set_lode(chebmodel_vec_intop_t K, const chebmodel_vec_lode_t L);

void chebmodel_vec_intop_initvals(chebmodel_vec_t G, const chebmodel_vec_lode_t L, mpfi_ptr_ptr I);

void chebmodel_vec_intop_rhs(chebmodel_vec_t G, const chebmodel_vec_lode_t L, const chebmodel_vec_t g, mpfi_ptr_ptr I);

// evaluate on a polynomial ; use integration from -1

void chebmodel_vec_intop_evaluate_mpfr_chebpoly(chebmodel_vec_t P, const chebmodel_vec_intop_t K, const mpfr_chebpoly_vec_t Q);

void chebmodel_vec_intop_evaluate_mpfi_chebpoly(chebmodel_vec_t P, const chebmodel_vec_intop_t K, const mpfi_chebpoly_vec_t Q);

void chebmodel_vec_intop_evaluate_chebmodel(chebmodel_vec_t P, const chebmodel_vec_intop_t K, const chebmodel_vec_t Q);

/*
// get the bandmatrix_t associated to the N-th truncation of K

void mpfi_chebpoly_vec_intop_get_bandmatrix(mpfi_bandmatrix_t M, const mpfi_chebpoly_vec_intop_t K, long N);

*/
/* print */

//void mpfi_chebpoly_intop_print(const mpfi_chebpoly_intop_t K, const char * Cheb_var, size_t digits);


#endif
