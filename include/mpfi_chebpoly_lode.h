
/*
This file is part of the TchebyApprox library.

The type [mpfi_chebpoly_lode_t] represents a (scalar)
linear differential operator of order [order]:
  - normalized (leading coeff = 1)
  - with coefficients a[0], ..., a[order-1] represented
    by [mpfi_chebpoly_t].
*/


#ifndef MPFI_CHEBPOLY_LODE_H
#define MPFI_CHEBPOLY_LODE_H


#include<stdio.h>
#include<stdlib.h>

#include <gmp.h>
#include <mpfr.h>
#include <mpfi.h>
#include <mpfi_io.h>

#include "mpfr_poly.h"
#include "mpfi_poly.h"
#include "mpfr_chebpoly.h"
#include "mpfi_chebpoly.h"
#include "mpfr_chebpoly_lode.h"

// structure for a linear differential equation with polynomial coefficients
// represent the equation y^(r) + a[r-1].y^(r-1) + ... + a[1].y' + a[0].y = 0
// where r = order
typedef struct {
  long order;
  mpfi_chebpoly_ptr a;
} mpfi_chebpoly_lode_struct;

typedef mpfi_chebpoly_lode_struct mpfi_chebpoly_lode_t[1];

typedef mpfi_chebpoly_lode_struct * mpfi_chebpoly_lode_ptr;

typedef const mpfi_chebpoly_lode_struct * mpfi_chebpoly_lode_srcptr;


/* basic manipulations */

void mpfi_chebpoly_lode_init(mpfi_chebpoly_lode_t L);

void mpfi_chebpoly_lode_clear(mpfi_chebpoly_lode_t L);

void mpfi_chebpoly_lode_set(mpfi_chebpoly_lode_t L, const mpfi_chebpoly_lode_t M);

void mpfi_chebpoly_lode_set_mpfr_chebpoly_lode(mpfi_chebpoly_lode_t L, const mpfr_chebpoly_lode_t M);

void mpfi_chebpoly_lode_get_mpfr_chebpoly_lode(mpfr_chebpoly_lode_t L, const mpfi_chebpoly_lode_t M);

void mpfi_chebpoly_lode_swap(mpfi_chebpoly_lode_t L, mpfi_chebpoly_lode_t M);

void mpfi_chebpoly_lode_set_order(mpfi_chebpoly_lode_t L, long order);


/* evaluation on polynomials : compute P = L.Q */

void mpfi_chebpoly_lode_evaluate_fi(mpfi_chebpoly_t P, const mpfi_chebpoly_lode_t L, const mpfi_chebpoly_t Q);

void mpfi_chebpoly_lode_evaluate_fr(mpfi_chebpoly_t P, const mpfi_chebpoly_lode_t L, const mpfr_chebpoly_t Q);


/* print */

void mpfi_chebpoly_lode_print(const mpfi_chebpoly_lode_t L, const char * var, const char * Cheb_var, size_t digits);


/* transpose from x-D form to D-x form and conversely */
// x-D form : L = D^r + a_{r-1} * D^(r-1) + ... + a_1 * D + a_0
// D-x form : L' = (-D)^r + (-D)^(r-1) * b_{r-1} + ... - D * b_1 + b_0
// this transpose function maps a to b (and b to a) st L = (-1)^r L'
// note: involutive function, can be used in both directions
void mpfi_chebpoly_lode_transpose(mpfi_chebpoly_ptr b, mpfi_chebpoly_srcptr a, long r);

/* transpose initial conditions */
// x-D initial conditions: y^(i) = v[i]
// D-x initial conditions: y^[i] = w[i]
// where y^[0] = y and y^[i+1] = -Dy^[i] + a_{r-1-i} y

// v -> w
// take as input the coefficients b[i] of the D-x form differential operator
void mpfi_chebpoly_lode_transpose_init_xDtoDx(mpfi_ptr w, mpfi_srcptr v, mpfi_chebpoly_srcptr b, long r);

// w -> v
// take as input the coefficients b[i] of the D-x form differential operator
void mpfi_chebpoly_lode_transpose_init_DxtoxD(mpfi_ptr v, mpfi_srcptr w, mpfi_chebpoly_srcptr b, long r);


#endif
