
/*
This file is part of the TchebyApprox library.

The type [mpfi_chebpoly_intop_t] represents a (scalar)
linear Volterra integral operator of the second kind
of order [order] derived from a [mpfi_chebpoly_lode_t].
The new unknown of this intagral equation is the last derivative
of the unknown in the differential equation.
kernel k(t,s) = alpha0(t)*T0(s) + ... + alpha[order-1](t)*T[order-1](s)
*/


#ifndef MPFI_CHEBPOLY_INTOP_H
#define MPFI_CHEBPOLY_INTOP_H


#include <stdio.h>
#include <stdlib.h>

#include <gmp.h>
#include <mpfr.h>
#include <mpfi.h>
#include <mpfi_io.h>

#include "double_operations.h"

#include "mpfi_bandvec.h"
#include "mpfi_poly.h"
#include "mpfi_chebpoly.h"
#include "mpfi_chebpoly_lode.h"
#include "double_bandmatrix.h"
#include "mpfr_bandmatrix.h"
#include "mpfi_bandmatrix.h"

#include "double_chebpoly_intop.h"
#include "mpfr_chebpoly_intop.h"


typedef struct {
  long order;
  mpfi_chebpoly_ptr alpha;
} mpfi_chebpoly_intop_struct;

typedef mpfi_chebpoly_intop_struct mpfi_chebpoly_intop_t[1];

typedef mpfi_chebpoly_intop_struct * mpfi_chebpoly_intop_ptr;

typedef const mpfi_chebpoly_intop_struct * mpfi_chebpoly_intop_srcptr;




void mpfi_chebpoly_intop_init(mpfi_chebpoly_intop_t K);

void mpfi_chebpoly_intop_clear(mpfi_chebpoly_intop_t K);

void mpfi_chebpoly_intop_set(mpfi_chebpoly_intop_t K, const mpfi_chebpoly_intop_t N);

void mpfi_chebpoly_intop_set_double_chebpoly_intop(mpfi_chebpoly_intop_t K, const double_chebpoly_intop_t L);

void mpfi_chebpoly_intop_set_mpfr_chebpoly_intop(mpfi_chebpoly_intop_t K, const mpfr_chebpoly_intop_t L);

void mpfi_chebpoly_intop_get_double_chebpoly_intop(double_chebpoly_intop_t K, const mpfi_chebpoly_intop_t L);

void mpfi_chebpoly_intop_get_mpfr_chebpoly_intop(mpfr_chebpoly_intop_t K, const mpfi_chebpoly_intop_t L);

void mpfi_chebpoly_intop_swap(mpfi_chebpoly_intop_t K, mpfi_chebpoly_intop_t N);

void mpfi_chebpoly_intop_set_order(mpfi_chebpoly_intop_t K, long order);

long mpfi_chebpoly_intop_Hwidth(const mpfi_chebpoly_intop_t K);

long mpfi_chebpoly_intop_Dwidth(const mpfi_chebpoly_intop_t K);


void mpfi_chebpoly_intop_set_lode(mpfi_chebpoly_intop_t K, const mpfi_chebpoly_lode_t L);

void mpfi_chebpoly_intop_initvals(mpfi_chebpoly_t G, const mpfi_chebpoly_lode_t L, mpfi_srcptr I);

void mpfi_chebpoly_intop_rhs(mpfi_chebpoly_t G, const mpfi_chebpoly_lode_t L, const mpfi_chebpoly_t g, mpfi_srcptr I);


// evaluate on a polynomial ; use integration from -1

void mpfi_chebpoly_intop_evaluate_fr(mpfi_chebpoly_t P, const mpfi_chebpoly_intop_t K, const mpfr_chebpoly_t Q);

void mpfi_chebpoly_intop_evaluate_fi(mpfi_chebpoly_t P, const mpfi_chebpoly_intop_t K, const mpfi_chebpoly_t Q);

void mpfi_chebpoly_intop_evaluate_Ti(mpfi_bandvec_t V, const mpfi_chebpoly_intop_t K, long i);


// get the bandmatrix_t associated to the N-th truncation of K

void mpfi_chebpoly_intop_get_bandmatrix(mpfi_bandmatrix_t M, const mpfi_chebpoly_intop_t K, long N);



/* print */

void mpfi_chebpoly_intop_print(const mpfi_chebpoly_intop_t K, const char * Cheb_var, size_t digits);


#endif
