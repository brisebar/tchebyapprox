
/*
This file is part of the TchebyApprox library.

The type [double_poly_t] implements polynomials in monomial basis
with coefficients of type [double].
[degree] = degree of the polynomial (-1 = null polynomial).
coeffs[0], ... , coeffs[degree-1] = coefficients in the monomial basis.
*/


#ifndef DOUBLE_POLY_H
#define DOUBLE_POLY_H

#include <stdio.h>
#include <stdlib.h>

#include <gmp.h>
#include <mpfr.h>
#include <mpfi.h>
#include <mpfi_io.h>

#include "double_operations.h"


// deg is always the exact degree of the polynomial
// by convention, deg(0) = -1

typedef struct double_poly_struct {
  long degree;
  double_ptr coeffs;
} double_poly_struct;

typedef double_poly_struct double_poly_t[1];
typedef double_poly_struct * double_poly_ptr;
typedef const double_poly_struct * double_poly_srcptr;



/* memory management */

// init with P = 0
void double_poly_init(double_poly_t P);

// clear P
void double_poly_clear(double_poly_t P);

// set n as degree for P
void double_poly_set_degree(double_poly_t P, long n);

// set P in canonical form (exact degree)
void double_poly_normalise(double_poly_t P);



/* basic manipulations and assignments */

void _double_poly_set(double_ptr P, double_srcptr Q, long n);

// copy Q into P
void double_poly_set(double_poly_t P, const double_poly_t Q);

// swap P and Q efficiently
void double_poly_swap(double_poly_t P, double_poly_t Q);

// set the n-th coefficient of P to c
void double_poly_set_coeff_si(double_poly_t P, long n, long c);

// set the n-th coefficient of P to c
void double_poly_set_coeff_z(double_poly_t P, long n, const mpz_t c);

// set the n-th coefficient of P to c
void double_poly_set_coeff_q(double_poly_t P, long n, const mpq_t c);

// set the n-th coefficient of P to c
void double_poly_set_coeff_d(double_poly_t P, long n, const double_t c);

// set the n-th coefficient of P to c
void double_poly_set_coeff_fr(double_poly_t P, long n, const mpfr_t c);

// get the degree of P
long double_poly_degree(const double_poly_t P);

// get the n-th coefficient of P
void double_poly_get_coeff(double_t c, const double_poly_t P, long n);

// get a pointer to the n-th coefficient of P
// return NULL if n < 0 or if n > degree(P)
#define double_poly_get_coeff_ptr(P, n) (((n) <= (P)->degree) && (n >= 0) ? (P)->coeffs + (n) : NULL)

// set P to 0
void double_poly_zero(double_poly_t P);

// set P to (1/c)*Q where c is the leading coefficient of P
void _double_poly_monic(double_ptr P, const double_srcptr Q, long n);

// set P to (1/c)*Q where c is the leading coefficient of P
void double_poly_monic(double_poly_t P, const double_poly_t Q);


/* shifting, reverse */

void _double_poly_shift_left(double_ptr P, double_srcptr Q, long n, unsigned long k);

// set P to Q*X^k
void double_poly_shift_left(double_poly_t P, const double_poly_t Q, unsigned long k);

void _double_poly_shift_right(double_ptr P, double_srcptr Q, long n, unsigned long k);

// set P to Q/X^k
void double_poly_shift_right(double_poly_t P, const double_poly_t Q, unsigned long k);

// reverse a list of double of size (n+1)
void _double_poly_reverse(double_ptr P, double_srcptr Q, long n);

// set P to Q(X^-1)*X^(degree(Q))
void double_poly_reverse(double_poly_t P, const double_poly_t Q);


/* norm 1 */

void _double_poly_1norm(double_t y, double_srcptr P, long n);

void double_poly_1norm(double_t y, const double_poly_t P);

void _double_poly_1norm_ubound(double_t y, double_srcptr P, long n);

void double_poly_1norm_ubound(double_t y, const double_poly_t P);

void _double_poly_1norm_lbound(double_t y, double_srcptr P, long n);

void double_poly_1norm_lbound(double_t y, const double_poly_t P);

void _double_poly_1norm_fi(mpfi_t y, double_srcptr P, long n);

void double_poly_1norm_fi(mpfi_t y, const double_poly_t P);





/* test functions */

#define double_poly_is_zero(P) ((P)->degree < 0)

#define double_poly_is_constant(P) ((P)->degree == 0)

#define double_poly_is_monic(P) (double_cmp_si((P)->coeffs + (P)->degree, 1) == 0)

int _double_poly_equal(double_srcptr P, double_srcptr Q, long n);

int double_poly_equal(const double_poly_t P, const double_poly_t Q);


/* evaluation */

void _double_poly_evaluate_si(double_t y, double_srcptr P, long n, long x);

// set y to P(x)
void double_poly_evaluate_si(double_t y, const double_poly_t P, long x);

void _double_poly_evaluate_z(double_t y, double_srcptr P, long n, const mpz_t x);

// set y to P(x)
void double_poly_evaluate_z(double_t y, const double_poly_t P, const mpz_t x);

void _double_poly_evaluate_q(double_t y, double_srcptr P, long n, const mpq_t x);

// set y to P(x)
void double_poly_evaluate_q(double_t y, const double_poly_t P, const mpq_t x);

void _double_poly_evaluate_d(double_t y, double_srcptr P, long n, const double_t x);

// set y to P(x)
void double_poly_evaluate_d(double_t y, const double_poly_t P, const double_t x);

void _double_poly_evaluate_fr(mpfr_t y, double_srcptr P, long n, const mpfr_t x);

// set y to P(x)
void double_poly_evaluate_fr(mpfr_t y, const double_poly_t P, const mpfr_t x);


/* addition and substraction */

void _double_poly_add(double_ptr P, double_srcptr Q, long n, double_srcptr R, long m);

// set P := Q + R
void double_poly_add(double_poly_t P, const double_poly_t Q, const double_poly_t R);

void _double_poly_sub(double_ptr P, double_srcptr Q, long n, double_srcptr R, long m);

// set P := Q - R
void double_poly_sub(double_poly_t P, const double_poly_t Q, const double_poly_t R);

void _double_poly_neg(double_ptr P, double_srcptr Q, long n);

// set P := -Q
void double_poly_neg(double_poly_t P, const double_poly_t Q);


/* scalar multiplication and division */

void _double_poly_scalar_mul_si(double_ptr P, double_srcptr Q, long n, long c);

// set P := c*Q
void double_poly_scalar_mul_si(double_poly_t P, const double_poly_t Q, long c);

void _double_poly_scalar_mul_z(double_ptr P, double_srcptr Q, long n, const mpz_t c);

// set P := c*Q
void double_poly_scalar_mul_z(double_poly_t P, const double_poly_t Q, const mpz_t c);

void _double_poly_scalar_mul_q(double_ptr P, double_srcptr Q, long n, const mpq_t c);

// set P := c*Q
void double_poly_scalar_mul_q(double_poly_t P, const double_poly_t Q, const mpq_t c);

void _double_poly_scalar_mul_d(double_ptr P, double_srcptr Q, long n, const double_t c);

// set P := c*Q
void double_poly_scalar_mul_d(double_poly_t P, const double_poly_t Q, const double_t c);

void _double_poly_scalar_mul_fr(double_ptr P, double_srcptr Q, long n, const mpfr_t c);

// set P := c*Q
void double_poly_scalar_mul_fr(double_poly_t P, const double_poly_t Q, const mpfr_t c);

void _double_poly_scalar_mul_2si(double_ptr P, double_srcptr Q, long n, long k);

// set P := 2^k*Q
void double_poly_scalar_mul_2si(double_poly_t P, const double_poly_t Q, long k);


void _double_poly_scalar_div_si(double_ptr P, double_srcptr Q, long n, long c);

// set P := (1/c)*Q
void double_poly_scalar_div_si(double_poly_t P, const double_poly_t Q, long c);

void _double_poly_scalar_div_z(double_ptr P, double_srcptr Q, long n, const mpz_t c);

// set P := (1/c)*Q
void double_poly_scalar_div_z(double_poly_t P, const double_poly_t Q, const mpz_t c);

void _double_poly_scalar_div_q(double_ptr P, double_srcptr Q, long n, const mpq_t c);

// set P := (1/c)*Q
void double_poly_scalar_div_q(double_poly_t P, const double_poly_t Q, const mpq_t c);

void _double_poly_scalar_div_d(double_ptr P, double_srcptr Q, long n, const double_t c);

// set P := (1/c)*Q
void double_poly_scalar_div_d(double_poly_t P, const double_poly_t Q, const double_t c);

void _double_poly_scalar_div_fr(double_ptr P, double_srcptr Q, long n, const mpfr_t c);

// set P := (1/c)*Q
void double_poly_scalar_div_fr(double_poly_t P, const double_poly_t Q, const mpfr_t c);

void _double_poly_scalar_div_2si(double_ptr P, double_srcptr Q, long n, long k);

// set P := 2^-k*Q
void double_poly_scalar_div_2si(double_poly_t P, const double_poly_t Q, long k);




/* multiplication */

void _double_poly_mul(double_ptr P, double_srcptr Q, long n, double_srcptr R, long m);

// set P := Q * R
void double_poly_mul(double_poly_t P, const double_poly_t Q, const double_poly_t R);


/* exponentiation */

// set P := Q^e
void double_poly_pow(double_poly_t P, const double_poly_t Q, unsigned long e);


/* derivative and antiderivative */

void _double_poly_derivative(double_ptr P, double_srcptr Q, long n);

// set P := Q'
void double_poly_derivative(double_poly_t P, const double_poly_t Q);

void _double_poly_antiderivative(double_ptr P, double_srcptr Q, long n);

// set P st P' = Q and P(0) = 0
void double_poly_antiderivative(double_poly_t P, const double_poly_t Q);


/* display function */


void _double_poly_print(double_srcptr P, long n, const char * var);

// display the polynomial P with 'var' as indeterminate symbol
// 'var' is a 0-terminated string
void double_poly_print(const double_poly_t P, const char * var);




#endif
