
/*
This file is part of the TchebyApprox library.

The type [double_vec_t] implements usual vectors of [double].
[length] = total length
coeffs[0], ... , coeffs[length-1] = vector entries
*/


#ifndef DOUBLE_VEC_H
#define DOUBLE_VEC_H

#include <stdio.h>
#include <stdlib.h>

#include <gmp.h>
#include <mpfr.h>
#include <mpfi.h>
#include <mpfi_io.h>

#include "double_operations.h"


typedef struct double_vec_struct {
  long length;
  double_ptr coeffs;
} double_vec_struct;

typedef double_vec_struct double_vec_t[1];
typedef double_vec_struct * double_vec_ptr;
typedef const double_vec_struct * double_vec_srcptr;
/* memory management */


// init with an empty vector 
void double_vec_init(double_vec_t V);

// clear V
void _double_vec_clear(double_ptr V, long n);

void double_vec_clear(double_vec_t V);

// set n as length for V
void double_vec_set_length(double_vec_t V, long n);


/* basic manipulations and assignments */

void _double_vec_set(double_ptr V, double_srcptr W, long n);

// copy W into V
void double_vec_set(double_vec_t V, const double_vec_t W);

// swap V and W efficiently
void double_vec_swap(double_vec_t P, double_vec_t Q);

// set the n-th coefficient of V to c
void double_vec_set_coeff_si(double_vec_t V, long n, long c);

// set the n-th coefficient of V to c
void double_vec_set_coeff_z(double_vec_t V, long n, const mpz_t c);

// set the n-th coefficient of V to c
void double_vec_set_coeff_q(double_vec_t V, long n, const mpq_t c);

// set the n-th coefficient of V to c
void double_vec_set_coeff_d(double_vec_t V, long n, const double_t c);

// set the n-th coefficient of V to c
void double_vec_set_coeff_fr(double_vec_t V, long n, const mpfr_t c);

// get the length of V
long double_vec_length(const double_vec_t V);

void _double_vec_zero(double_ptr V, long n);

// set all coefficients of V to 0
void double_vec_zero(double_vec_t V);


/* reverse */

void _double_vec_shift_left(double_ptr V, double_srcptr W, long n, unsigned long k);

void double_vec_shift_left(double_vec_t V, const double_vec_t W, unsigned long k);

void _double_vec_shift_right(double_ptr V, double_srcptr W, long n, unsigned long k);

void double_vec_shift_right(double_vec_t V, const double_vec_t W, unsigned long k);

void _double_vec_reverse(double_ptr V, double_srcptr W, long n);

void double_vec_reverse(double_vec_t V, const double_vec_t W);


/* norm 1 */

void _double_vec_1norm(double_t norm, double_srcptr V, long n);

void _double_vec_1norm_ubound(double_t norm, double_srcptr V, long n);

void _double_vec_1norm_lbound(double_t norm, double_srcptr V, long n);

void _double_vec_1norm_fi(mpfi_t norm, double_srcptr V, long n);

void double_vec_1norm(double_t norm, const double_vec_t V);

void double_vec_1norm_ubound(double_t norm, const double_vec_t V);

void double_vec_1norm_lbound(double_t norm, const double_vec_t V);

void double_vec_1norm_fi(mpfi_t norm, const double_vec_t V);


/* test functions */

int _double_vec_is_zero(double_srcptr V, long n);

int double_vec_is_zero(const double_vec_t V);

int _double_vec_equal(double_srcptr V, double_srcptr W, long n);

int double_vec_equal(const double_vec_t V, const double_vec_t W);


/* addition and substraction */

void _double_vec_add(double_ptr V, double_srcptr W, double_srcptr Z, long n); 

// set V := W + Z
void double_vec_add(double_vec_t V, const double_vec_t W, const double_vec_t Z);

void _double_vec_sub(double_ptr V, double_srcptr W, double_srcptr Z, long n);

// set V := W - Z
void double_vec_sub(double_vec_t V, const double_vec_t W, const double_vec_t Z);

void _double_vec_neg(double_ptr V, double_srcptr W, long n);

// set V := -W
void double_vec_neg(double_vec_t V, const double_vec_t W);


/* scalar multiplication and division */

void _double_vec_scalar_mul_si(double_ptr V, double_srcptr W, long n, long c);

// set V := c*W
void double_vec_scalar_mul_si(double_vec_t V, const double_vec_t W, long c);

void _double_vec_scalar_mul_z(double_ptr V, double_srcptr W, long n, const mpz_t c);

// set V := c*W
void double_vec_scalar_mul_z(double_vec_t V, const double_vec_t W, const mpz_t c);

void _double_vec_scalar_mul_q(double_ptr V, double_srcptr W, long n, const mpq_t c);

// set V := c*W
void double_vec_scalar_mul_q(double_vec_t V, const double_vec_t W, const mpq_t c);

void _double_vec_scalar_mul_d(double_ptr V, double_srcptr W, long n, const double_t c);

// set V := c*W
void double_vec_scalar_mul_d(double_vec_t V, const double_vec_t W, const double_t c);

void _double_vec_scalar_mul_fr(double_ptr V, double_srcptr W, long n, const mpfr_t c);

// set V := c*W
void double_vec_scalar_mul_fr(double_vec_t V, const double_vec_t W, const mpfr_t c);

void _double_vec_scalar_mul_2si(double_ptr V, double_srcptr W, long n, long k);

// set V := 2^k*W
void double_vec_scalar_mul_2si(double_vec_t P, const double_vec_t Q, long k);


void _double_vec_scalar_div_si(double_ptr V, double_srcptr W, long n, long c);

// set V := (1/c)*W
void double_vec_scalar_div_si(double_vec_t V, const double_vec_t W, long c);

void _double_vec_scalar_div_z(double_ptr V, double_srcptr W, long n, const mpz_t c);

// set V := (1/c)*W
void double_vec_scalar_div_z(double_vec_t V, const double_vec_t W, const mpz_t c);

void _double_vec_scalar_div_q(double_ptr V, double_srcptr W, long n, const mpq_t c);

// set V := c*W
void double_vec_scalar_div_q(double_vec_t V, const double_vec_t W, const mpq_t c);

void _double_vec_scalar_div_d(double_ptr V, double_srcptr W, long n, const double_t c);

// set V := c*W
void double_vec_scalar_div_d(double_vec_t V, const double_vec_t W, const double_t c);

void _double_vec_scalar_div_fr(double_ptr V, double_srcptr W, long n, const mpfr_t c);

// set V := c*W
void double_vec_scalar_div_fr(double_vec_t V, const double_vec_t W, const mpfr_t c);

void _double_vec_scalar_div_2si(double_ptr V, double_srcptr W, long n, long k);

// set V := 2^-k*W
void double_vec_scalar_div_2si(double_vec_t V, const double_vec_t W, long k);


/* scalar product */

void _double_vec_scalar_prod(double_t prod, double_srcptr V, double_srcptr W, long n);

void double_vec_scalar_prod(double_t prod, const double_vec_t V, const double_vec_t W);

void _double_vec_scalar_prod_fi(mpfi_t prod, double_srcptr V, double_srcptr W, long n);

void double_vec_scalar_prod_fi(mpfi_t prod, const double_vec_t V, const double_vec_t W);



/* display function */


void _double_vec_print(double_srcptr V, long n);

// display the vector V
void double_vec_print(const double_vec_t V);




#endif
