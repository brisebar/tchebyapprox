
/*
This file is part of the TchebyApprox library.

The type [mpfi_chebpoly_vec_lode_t] represents a vectorial 
linear differential operator of order [order] and dimension [dim]:
  - normalized (leading coeff = 1)
  - with matricial coefficients A[0][i][j], ..., A[order-1][i][j]
    (0 <= i,j <= [dim]-1) represented by [mpfi_chebpoly_t].
*/


#ifndef MPFI_CHEBPOLY_VEC_LODE_H
#define MPFI_CHEBPOLY_VEC_LODE_H


#include <stdio.h>
#include <stdlib.h>

#include <gmp.h>
#include <mpfr.h>
#include <mpfi.h>
#include <mpfi_io.h>

#include "double_operations.h"

#include "mpfi_chebpoly.h"
#include "double_chebpoly_vec.h"
#include "mpfr_chebpoly_vec.h"
#include "mpfi_chebpoly_vec.h"

#include "mpfi_chebpoly_lode.h"
#include "double_chebpoly_vec_lode.h"
#include "mpfr_chebpoly_vec_lode.h"


// structure for a linear differential equation with polynomial coefficients
// represent the equation Y^(r) + A[r-1].Y^(r-1) + ... + A[1].Y' + A[0].Y = 0
// where r = order
typedef struct {
  long order;
  long dim;
  mpfi_chebpoly_ptr_ptr_ptr A;
} mpfi_chebpoly_vec_lode_struct;

typedef mpfi_chebpoly_vec_lode_struct mpfi_chebpoly_vec_lode_t[1];

typedef mpfi_chebpoly_vec_lode_struct * mpfi_chebpoly_vec_lode_ptr;

typedef const mpfi_chebpoly_vec_lode_struct * mpfi_chebpoly_vec_lode_srcptr;


/* basic manipulations */

void mpfi_chebpoly_vec_lode_init(mpfi_chebpoly_vec_lode_t L);

void mpfi_chebpoly_vec_lode_clear(mpfi_chebpoly_vec_lode_t L);

void mpfi_chebpoly_vec_lode_set(mpfi_chebpoly_vec_lode_t L, const mpfi_chebpoly_vec_lode_t M);

void mpfi_chebpoly_vec_lode_set_double_chebpoly_vec_lode(mpfi_chebpoly_vec_lode_t L, const double_chebpoly_vec_lode_t M);

void mpfi_chebpoly_vec_lode_set_mpfr_chebpoly_vec_lode(mpfi_chebpoly_vec_lode_t L, const mpfr_chebpoly_vec_lode_t M);

void mpfi_chebpoly_vec_lode_get_double_chebpoly_vec_lode(double_chebpoly_vec_lode_t L, const mpfi_chebpoly_vec_lode_t M);

void mpfi_chebpoly_vec_lode_get_mpfr_chebpoly_vec_lode(mpfr_chebpoly_vec_lode_t L, const mpfi_chebpoly_vec_lode_t M);

void mpfi_chebpoly_vec_lode_get_mpfi_chebpoly_lode(mpfi_chebpoly_lode_t Lij, const mpfi_chebpoly_vec_lode_t L, long i, long j);

void mpfi_chebpoly_vec_lode_swap(mpfi_chebpoly_vec_lode_t L, mpfi_chebpoly_vec_lode_t M);

void mpfi_chebpoly_vec_lode_set_order_dim(mpfi_chebpoly_vec_lode_t L, long order, long dim);


/* evaluation on vectors of polynomials : compute P = L.Q */

void mpfi_chebpoly_vec_lode_evaluate_d(mpfi_chebpoly_vec_t P, const mpfi_chebpoly_vec_lode_t L, const double_chebpoly_vec_t Q);

void mpfi_chebpoly_vec_lode_evaluate_fr(mpfi_chebpoly_vec_t P, const mpfi_chebpoly_vec_lode_t L, const mpfr_chebpoly_vec_t Q);

void mpfi_chebpoly_vec_lode_evaluate_fi(mpfi_chebpoly_vec_t P, const mpfi_chebpoly_vec_lode_t L, const mpfi_chebpoly_vec_t Q);


/* print */
// useful?
// void mpfi_chebpoly_vec_lode_print(const mpfi_chebpoly_vec_lode_t L, const char * var, const char * Cheb_var, size_t digits);

#endif
