
/*
This file is part of the TchebyApprox library.

The type [double_chebpoly_intop_t] represents a (scalar)
linear Volterra integral operator of the second kind
of order [order] derived from a [double_chebpoly_lode_t].
The new unknown of this intagral equation is the last derivative
of the unknown in the differential equation.
kernel k(t,s) = alpha0(t)*T0(s) + ... + alpha[order-1](t)*T[order-1](s)

Provided solving procedures for the integral equation
use Olver and Townsend's algorithm on almost-banded matrices.
*/


#ifndef DOUBLE_CHEBPOLY_INTOP_H
#define DOUBLE_CHEBPOLY_INTOP_H


#include <stdio.h>
#include <stdlib.h>

#include <gmp.h>
#include <mpfr.h>
#include <mpfi.h>
#include <mpfi_io.h>

#include "double_operations.h"

#include "double_bandvec.h"
#include "mpfr_bandvec.h"
#include "mpfi_bandvec.h"
#include "double_poly.h"
#include "mpfr_poly.h"
#include "mpfi_poly.h"
#include "double_chebpoly.h"
#include "mpfr_chebpoly.h"
#include "mpfi_chebpoly.h"
#include "double_chebpoly_lode.h"
#include "double_bandmatrix.h"


typedef struct {
  long order;
  double_chebpoly_ptr alpha;
} double_chebpoly_intop_struct;

typedef double_chebpoly_intop_struct double_chebpoly_intop_t[1];

typedef double_chebpoly_intop_struct * double_chebpoly_intop_ptr;

typedef const double_chebpoly_intop_struct * double_chebpoly_intop_srcptr;




void double_chebpoly_intop_init(double_chebpoly_intop_t K);

void double_chebpoly_intop_clear(double_chebpoly_intop_t K);

void double_chebpoly_intop_set(double_chebpoly_intop_t K, const double_chebpoly_intop_t N);

void double_chebpoly_intop_swap(double_chebpoly_intop_t K, double_chebpoly_intop_t N);

void double_chebpoly_intop_set_order(double_chebpoly_intop_t K, long order);

long double_chebpoly_intop_Hwidth(const double_chebpoly_intop_t K);

long double_chebpoly_intop_Dwidth(const double_chebpoly_intop_t K);


void double_chebpoly_intop_set_lode(double_chebpoly_intop_t K, const double_chebpoly_lode_t L);

void double_chebpoly_intop_initvals(double_chebpoly_t G, const double_chebpoly_lode_t L, double_srcptr I);

void double_chebpoly_intop_rhs(double_chebpoly_t G, const double_chebpoly_lode_t L, const double_chebpoly_t g, double_srcptr I);

// evaluate on a polynomial ; use integration from -1

void double_chebpoly_intop_evaluate_d(double_chebpoly_t P, const double_chebpoly_intop_t K, const double_chebpoly_t Q);

void double_chebpoly_intop_evaluate_fr(mpfr_chebpoly_t P, const double_chebpoly_intop_t K, const mpfr_chebpoly_t Q);

void double_chebpoly_intop_evaluate_fi(mpfi_chebpoly_t P, const double_chebpoly_intop_t K, const mpfi_chebpoly_t Q);

void double_chebpoly_intop_evaluate_Ti(double_bandvec_t V, const double_chebpoly_intop_t K, long i);

void double_chebpoly_intop_evaluate_Ti_fi(mpfi_bandvec_t V, const double_chebpoly_intop_t K, long i);


// get the bandmatrix_t associated to the N-th truncation of K

void double_chebpoly_intop_get_bandmatrix(double_bandmatrix_t M, const double_chebpoly_intop_t K, long N);


/* approximate solutions */

void double_chebpoly_intop_approxsolve(double_chebpoly_t F, const double_chebpoly_intop_t K, const double_chebpoly_t G, long N);

void double_chebpoly_lode_intop_approxsolve(double_chebpoly_ptr Sols, const double_chebpoly_lode_t L, const double_chebpoly_t g, double_srcptr I, long N);


void double_chebpoly_intop_approxsolve_check(double_t bound, double_chebpoly_srcptr Sols, const double_chebpoly_lode_t L, const double_chebpoly_t g, double_srcptr I);

/* print */

void double_chebpoly_intop_print(const double_chebpoly_intop_t K, const char * Cheb_var);


#endif
