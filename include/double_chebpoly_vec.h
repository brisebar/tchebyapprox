
/*
This file is part of the TchebyApprox library.

The type [double_chebpoly_vec_t] implements a vector of
[double_chebpoly_t] of length [dim]:
poly[0], ... , poly[dim-1]
*/


#ifndef DOUBLE_CHEBPOLY_VEC_H
#define DOUBLE_CHEBPOLY_VEC_H

#include <stdio.h>
#include <stdlib.h>

#include <gmp.h>
#include <mpfr.h>
#include "double_operations.h"

#include "double_vec.h"
#include "mpfr_vec.h"
#include "mpfi_vec.h"

#include "double_chebpoly.h"


typedef struct double_chebpoly_vec_struct {
  long dim;
  double_chebpoly_ptr poly;
} double_chebpoly_vec_struct;

typedef double_chebpoly_vec_struct double_chebpoly_vec_t[1];
typedef double_chebpoly_vec_struct * double_chebpoly_vec_ptr;
typedef const double_chebpoly_vec_struct * double_chebpoly_vec_srcptr;


/* memory management */

// init P with dim = 0
void double_chebpoly_vec_init(double_chebpoly_vec_t P);

// clear P
void double_chebpoly_vec_clear(double_chebpoly_vec_t P);

// set dim
void double_chebpoly_vec_set_dim(double_chebpoly_vec_t P, long n);


/* basic manipulations and assignments */

void double_chebpoly_vec_set(double_chebpoly_vec_t P, const double_chebpoly_vec_t Q);

void double_chebpoly_vec_swap(double_chebpoly_vec_t P, double_chebpoly_vec_t Q);

// max degree of the P[i]
long double_chebpoly_vec_degree(const double_chebpoly_vec_t P);

// set all P[i] to 0 (without changing dim)
void double_chebpoly_vec_zero(double_chebpoly_vec_t P);


/* componentwise evaluation */

void double_chebpoly_vec_evaluate_d(double_vec_t Y, const double_chebpoly_vec_t P, const double_t x);

void double_chebpoly_vec_evaluate_fr(mpfr_vec_t Y, const double_chebpoly_vec_t P, const mpfr_t x);

void double_chebpoly_vec_evaluate_fi(mpfi_vec_t Y, const double_chebpoly_vec_t P, const mpfi_t x);


/* componentwise addition and subtraction */

void double_chebpoly_vec_add(double_chebpoly_vec_t P, const double_chebpoly_vec_t Q, const double_chebpoly_vec_t R);

void double_chebpoly_vec_sub(double_chebpoly_vec_t P, const double_chebpoly_vec_t Q, const double_chebpoly_vec_t R);

void double_chebpoly_vec_neg(double_chebpoly_vec_t P, const double_chebpoly_vec_t Q);


/* componentwise scalar multiplication and division */

void double_chebpoly_vec_scalar_mul_si(double_chebpoly_vec_t P, const double_chebpoly_vec_t Q, long c);

void double_chebpoly_vec_scalar_mul_z(double_chebpoly_vec_t P, const double_chebpoly_vec_t Q, const mpz_t c);

void double_chebpoly_vec_scalar_mul_q(double_chebpoly_vec_t P, const double_chebpoly_vec_t Q, const mpq_t c);

void double_chebpoly_vec_scalar_mul_d(double_chebpoly_vec_t P, const double_chebpoly_vec_t Q, const double_t c);

void double_chebpoly_vec_scalar_mul_fr(double_chebpoly_vec_t P, const double_chebpoly_vec_t Q, const mpfr_t c);

void double_chebpoly_vec_scalar_mul_2si(double_chebpoly_vec_t P, const double_chebpoly_vec_t Q, long k);


void double_chebpoly_vec_scalar_div_si(double_chebpoly_vec_t P, const double_chebpoly_vec_t Q, long c);

void double_chebpoly_vec_scalar_div_z(double_chebpoly_vec_t P, const double_chebpoly_vec_t Q, const mpz_t c);

void double_chebpoly_vec_scalar_div_q(double_chebpoly_vec_t P, const double_chebpoly_vec_t Q, const mpq_t c);

void double_chebpoly_vec_scalar_div_d(double_chebpoly_vec_t P, const double_chebpoly_vec_t Q, const double_t c);

void double_chebpoly_vec_scalar_div_fr(double_chebpoly_vec_t P, const double_chebpoly_vec_t Q, const mpfr_t c);

void double_chebpoly_vec_scalar_div_2si(double_chebpoly_vec_t P, const double_chebpoly_vec_t Q, long k);


/* componentwise multiplication */

void double_chebpoly_vec_mul(double_chebpoly_vec_t P, const double_chebpoly_vec_t Q, const double_chebpoly_vec_t R);


/* scalar product */

void double_chebpoly_vec_scalar_prod(double_chebpoly_t P, const double_chebpoly_vec_t Q, const double_chebpoly_vec_t R);


/* componentwise exponentiation */

void double_chebpoly_vec_pow(double_chebpoly_vec_t P, const double_chebpoly_vec_t Q, unsigned long e);


/* componentwise derivative and antiderivative */

void double_chebpoly_vec_derivative(double_chebpoly_vec_t P, const double_chebpoly_vec_t Q);

void double_chebpoly_vec_antiderivative(double_chebpoly_vec_t P, const double_chebpoly_vec_t Q);

void double_chebpoly_vec_antiderivative0(double_chebpoly_vec_t P, const double_chebpoly_vec_t Q);

void double_chebpoly_vec_antiderivative_1(double_chebpoly_vec_t P, const double_chebpoly_vec_t Q);

void double_chebpoly_vec_antiderivative1(double_chebpoly_vec_t P, const double_chebpoly_vec_t Q);





 
#endif
