
/*
This file is part of the TchebyApprox library.

The type [chebmodel_chebpoly_lode_t] represents a (scalar) 
linear differential operator of order [order]:
  - normalized (leading coeff = 1)
  - with coefficients a[0], ..., a[order-1] represented
    by [chebmodel_chebpoly_t].
*/


#ifndef CHEBMODEL_LODE_H
#define CHEBMODEL_LODE_H


#include<stdio.h>
#include<stdlib.h>

#include <gmp.h>
#include <mpfr.h>
#include <mpfi.h>
#include <mpfi_io.h>

#include "mpfr_poly.h"
#include "mpfi_poly.h"
#include "mpfr_chebpoly.h"
#include "mpfi_chebpoly.h"
#include "chebmodel.h"

#include "mpfr_chebpoly_lode.h"
#include "mpfi_chebpoly_lode.h"

// structure for a linear differential equation with polynomial coefficients
// represent the equation y^(r) + a[r-1].y^(r-1) + ... + a[1].y' + a[0].y = 0
// where r = order
typedef struct {
  long order;
  chebmodel_ptr a;
} chebmodel_lode_struct;

typedef chebmodel_lode_struct chebmodel_lode_t[1];

typedef chebmodel_lode_struct * chebmodel_lode_ptr;

typedef const chebmodel_lode_struct * chebmodel_lode_srcptr;


/* basic manipulations */

void chebmodel_lode_init(chebmodel_lode_t L);

void chebmodel_lode_clear(chebmodel_lode_t L);

void chebmodel_lode_set(chebmodel_lode_t L, const chebmodel_lode_t M);

void chebmodel_lode_set_mpfr_chebpoly_lode(chebmodel_lode_t L, const mpfr_chebpoly_lode_t M);

void chebmodel_lode_set_mpfi_chebpoly_lode(chebmodel_lode_t L, const mpfi_chebpoly_lode_t M);

void chebmodel_lode_get_mpfr_chebpoly_lode(mpfr_chebpoly_lode_t L, const chebmodel_lode_t M);

void chebmodel_lode_get_mpfi_chebpoly_lode(mpfi_chebpoly_lode_t L, const chebmodel_lode_t M);

void chebmodel_lode_swap(chebmodel_lode_t L, chebmodel_lode_t M);

void chebmodel_lode_set_order(chebmodel_lode_t L, long order);


#if 0 // cannot derivate a chebmodel
/* evaluation on polynomials : compute P = L.Q */

void chebmodel_lode_evaluate_mpfr_chebpoly(chebmodel_t P, const chebmodel_lode_t L, const mpfr_chebpoly_t Q);

void chebmodel_lode_evaluate_mpfi_chebpoly(chebmodel_t P, const chebmodel_lode_t L, const mpfi_chebpoly_t Q);

void chebmodel_lode_evaluate_chebmodel(chebmodel_t P, const chebmodel_lode_t L, const chebmodel_t Q);
#endif


/* print */

void chebmodel_lode_print(const chebmodel_lode_t L, const char * var, const char * Cheb_var, size_t digits);



#endif
