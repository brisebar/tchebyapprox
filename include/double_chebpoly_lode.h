
/*
This file is part of the TchebyApprox library.

The type [double_chebpoly_lode_t] represents a (scalar)
linear differential operator of order [order]:
  - normalized (leading coeff = 1)
  - with coefficients a[0], ..., a[order-1] represented
    by [double_chebpoly_t].
*/


#ifndef DOUBLE_CHEBPOLY_LODE_H
#define DOUBLE_CHEBPOLY_LODE_H


#include<stdio.h>
#include<stdlib.h>

#include <gmp.h>
#include <mpfr.h>
#include <mpfi.h>
#include <mpfi_io.h>

#include "double_operations.h"

#include "double_poly.h"
#include "mpfr_poly.h"
#include "mpfi_poly.h"
#include "double_chebpoly.h"
#include "mpfr_chebpoly.h"
#include "mpfi_chebpoly.h"

// structure for a linear differential equation with polynomial coefficients
// represent the equation y^(r) + a[r-1].y^(r-1) + ... + a[1].y' + a[0].y = 0
// where r = order
typedef struct {
  long order;
  double_chebpoly_ptr a;
} double_chebpoly_lode_struct;

typedef double_chebpoly_lode_struct double_chebpoly_lode_t[1];

typedef double_chebpoly_lode_struct * double_chebpoly_lode_ptr;

typedef const double_chebpoly_lode_struct * double_chebpoly_lode_srcptr;


/* basic manipulations */

void double_chebpoly_lode_init(double_chebpoly_lode_t L);

void double_chebpoly_lode_clear(double_chebpoly_lode_t L);

void double_chebpoly_lode_set(double_chebpoly_lode_t L, const double_chebpoly_lode_t M);

void double_chebpoly_lode_swap(double_chebpoly_lode_t L, double_chebpoly_lode_t M);

void double_chebpoly_lode_set_order(double_chebpoly_lode_t L, long order);


/* evaluation on polynomials : compute P = L.Q */

void double_chebpoly_lode_evaluate_d(double_chebpoly_t P, const double_chebpoly_lode_t L, const double_chebpoly_t Q);

void double_chebpoly_lode_evaluate_fr(mpfr_chebpoly_t P, const double_chebpoly_lode_t L, const mpfr_chebpoly_t Q);

void double_chebpoly_lode_evaluate_fi(mpfi_chebpoly_t P, const double_chebpoly_lode_t L, const mpfi_chebpoly_t Q);


/* print */

void double_chebpoly_lode_print(const double_chebpoly_lode_t L, const char * var, const char * Cheb_var);


/* transpose from x-D form to D-x form and conversely */
// x-D form : L = D^r + a_{r-1} * D^(r-1) + ... + a_1 * D + a_0
// D-x form : L' = (-D)^r + (-D)^(r-1) * b_{r-1} + ... - D * b_1 + b_0
// this transpose function maps a to b (and b to a) st L = (-1)^r L'
// note: involutive function, can be used in both directions
void double_chebpoly_lode_transpose(double_chebpoly_ptr b, double_chebpoly_srcptr a, long r);

/* transpose initial conditions */
// x-D initial conditions: y^(i) = v[i]
// D-x initial conditions: y^[i] = w[i]
// where y^[0] = y and y^[i+1] = -Dy^[i] + a_{r-1-i} y

// v -> w
// take as input the coefficients b[i] of the D-x form differential operator
void double_chebpoly_lode_transpose_init_xDtoDx(double_ptr w, double_srcptr v, double_chebpoly_srcptr b, long r);

// w -> v
// take as input the coefficients b[i] of the D-x form differential operator
void double_chebpoly_lode_transpose_init_DxtoxD(double_ptr v, double_srcptr w, double_chebpoly_srcptr b, long r);



#endif
