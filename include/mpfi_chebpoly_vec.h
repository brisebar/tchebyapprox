
/*
This file is part of the TchebyApprox library.

The type [mpfi_chebpoly_vec_t] implements a vector of
[mpfi_chebpoly_t] of length [dim]:
poly[0], ... , poly[dim-1]
*/


#ifndef MPFI_CHEBPOLY_VEC_H
#define MPFI_CHEBPOLY_VEC_H

#include <stdio.h>
#include <stdlib.h>

#include <gmp.h>
#include <mpfr.h>
#include <mpfi.h>
#include <mpfi_io.h>
#include "double_operations.h"

#include "mpfi_vec.h"

#include "mpfi_chebpoly.h"
#include "mpfr_chebpoly_vec.h"
#include "double_chebpoly_vec.h"


typedef struct mpfi_chebpoly_vec_struct {
  long dim;
  mpfi_chebpoly_ptr poly;
} mpfi_chebpoly_vec_struct;

typedef mpfi_chebpoly_vec_struct mpfi_chebpoly_vec_t[1];
typedef mpfi_chebpoly_vec_struct * mpfi_chebpoly_vec_ptr;
typedef const mpfi_chebpoly_vec_struct * mpfi_chebpoly_vec_srcptr;


/* memory management */

// init P with dim = 0
void mpfi_chebpoly_vec_init(mpfi_chebpoly_vec_t P);

// clear P
void mpfi_chebpoly_vec_clear(mpfi_chebpoly_vec_t P);

// set dim
void mpfi_chebpoly_vec_set_dim(mpfi_chebpoly_vec_t P, long n);


/* basic manipulations and assignments */

void mpfi_chebpoly_vec_set(mpfi_chebpoly_vec_t P, const mpfi_chebpoly_vec_t Q);

void mpfi_chebpoly_vec_set_double_chebpoly_vec(mpfi_chebpoly_vec_t P, const double_chebpoly_vec_t Q);

void mpfi_chebpoly_vec_set_mpfr_chebpoly_vec(mpfi_chebpoly_vec_t P, const mpfr_chebpoly_vec_t Q);

void mpfi_chebpoly_vec_get_double_chebpoly_vec(double_chebpoly_vec_t P, const mpfi_chebpoly_vec_t Q);

void mpfi_chebpoly_vec_get_mpfr_chebpoly_vec(mpfr_chebpoly_vec_t P, const mpfi_chebpoly_vec_t Q);

void mpfi_chebpoly_vec_swap(mpfi_chebpoly_vec_t P, mpfi_chebpoly_vec_t Q);

// max degree of the P[i]
long mpfi_chebpoly_vec_degree(const mpfi_chebpoly_vec_t P);

// set all P[i] to 0 (without changing dim)
void mpfi_chebpoly_vec_zero(mpfi_chebpoly_vec_t P);


/* componentwise evaluation */

void mpfi_chebpoly_vec_evaluate_d(mpfi_vec_t Y, const mpfi_chebpoly_vec_t P, const double_t x);

void mpfi_chebpoly_vec_evaluate_fr(mpfi_vec_t Y, const mpfi_chebpoly_vec_t P, const mpfr_t x);

void mpfi_chebpoly_vec_evaluate_fi(mpfi_vec_t Y, const mpfi_chebpoly_vec_t P, const mpfi_t x);


/* componentwise addition and subtraction */

void mpfi_chebpoly_vec_add(mpfi_chebpoly_vec_t P, const mpfi_chebpoly_vec_t Q, const mpfi_chebpoly_vec_t R);

void mpfi_chebpoly_vec_sub(mpfi_chebpoly_vec_t P, const mpfi_chebpoly_vec_t Q, const mpfi_chebpoly_vec_t R);

void mpfi_chebpoly_vec_neg(mpfi_chebpoly_vec_t P, const mpfi_chebpoly_vec_t Q);


/* componentwise scalar multiplication and division */

void mpfi_chebpoly_vec_scalar_mul_si(mpfi_chebpoly_vec_t P, const mpfi_chebpoly_vec_t Q, long c);

void mpfi_chebpoly_vec_scalar_mul_z(mpfi_chebpoly_vec_t P, const mpfi_chebpoly_vec_t Q, const mpz_t c);

void mpfi_chebpoly_vec_scalar_mul_q(mpfi_chebpoly_vec_t P, const mpfi_chebpoly_vec_t Q, const mpq_t c);

void mpfi_chebpoly_vec_scalar_mul_fr(mpfi_chebpoly_vec_t P, const mpfi_chebpoly_vec_t Q, const mpfr_t c);

void mpfi_chebpoly_vec_scalar_mul_fi(mpfi_chebpoly_vec_t P, const mpfi_chebpoly_vec_t Q, const mpfi_t c);

void mpfi_chebpoly_vec_scalar_mul_2si(mpfi_chebpoly_vec_t P, const mpfi_chebpoly_vec_t Q, long k);


void mpfi_chebpoly_vec_scalar_div_si(mpfi_chebpoly_vec_t P, const mpfi_chebpoly_vec_t Q, long c);

void mpfi_chebpoly_vec_scalar_div_z(mpfi_chebpoly_vec_t P, const mpfi_chebpoly_vec_t Q, const mpz_t c);

void mpfi_chebpoly_vec_scalar_div_q(mpfi_chebpoly_vec_t P, const mpfi_chebpoly_vec_t Q, const mpq_t c);

void mpfi_chebpoly_vec_scalar_div_fr(mpfi_chebpoly_vec_t P, const mpfi_chebpoly_vec_t Q, const mpfr_t c);

void mpfi_chebpoly_vec_scalar_div_fi(mpfi_chebpoly_vec_t P, const mpfi_chebpoly_vec_t Q, const mpfi_t c);

void mpfi_chebpoly_vec_scalar_div_2si(mpfi_chebpoly_vec_t P, const mpfi_chebpoly_vec_t Q, long k);


/* componentwise multiplication */

void mpfi_chebpoly_vec_mul(mpfi_chebpoly_vec_t P, const mpfi_chebpoly_vec_t Q, const mpfi_chebpoly_vec_t R);


/* scalar product */

void mpfi_chebpoly_vec_scalar_prod(mpfi_chebpoly_t P, const mpfi_chebpoly_vec_t Q, const mpfi_chebpoly_vec_t R);


/* componentwise exponentiation */

void mpfi_chebpoly_vec_pow(mpfi_chebpoly_vec_t P, const mpfi_chebpoly_vec_t Q, unsigned long e);


/* componentwise derivative and antiderivative */

void mpfi_chebpoly_vec_derivative(mpfi_chebpoly_vec_t P, const mpfi_chebpoly_vec_t Q);

void mpfi_chebpoly_vec_antiderivative(mpfi_chebpoly_vec_t P, const mpfi_chebpoly_vec_t Q);

void mpfi_chebpoly_vec_antiderivative0(mpfi_chebpoly_vec_t P, const mpfi_chebpoly_vec_t Q);

void mpfi_chebpoly_vec_antiderivative_1(mpfi_chebpoly_vec_t P, const mpfi_chebpoly_vec_t Q);

void mpfi_chebpoly_vec_antiderivative1(mpfi_chebpoly_vec_t P, const mpfi_chebpoly_vec_t Q);





 
#endif
