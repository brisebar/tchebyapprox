
/*
This file is part of the TchebyApprox library.

Here are the routines to rigorously solve an integral equation
described by a [chebmodel_intop_t], using a Newton-like method.

[chebmodel_intop_newton_bound]: bound a Newton-like operator
[chebmodel_intop_newton_bound]: create and bound a Newton-like operator
[chebmodel_intop_newton_validate_sol_aux]: validate a candidate approximation, having already a contracting Newton-like operator
[chebmodel_intop_newton_validate]: validate a candidate approximation by creating a contracting Newton-like operator
[chebmodel_intop_newton_solve]: compute and validate an approximate solution => output a Chebmodel
*/


#ifndef CHEBMODEL_INTOP_NEWTON_H
#define CHEBMODEL_INTOP_NEWTON_H

#include <stdio.h>
#include <stdlib.h>

#include <gmp.h>
#include <mpfr.h>
#include <mpfi.h>
#include <mpfi_io.h>

#include "chebmodel.h"
#include "mpfr_bandmatrix.h"
#include "mpfi_bandmatrix.h"
#include "chebmodel_intop.h"
#include "mpfi_chebpoly_intop_newton.h"

/* bound for the Newton Operator */
// 1 = N too small  2 = imprecise numerical inverse  3 = the cumulated error of the coefficients of K is too big
int chebmodel_intop_newton_bound_d(double_t bound, const chebmodel_intop_t K, const mpfi_chebpoly_intop_t Ktrunc, const mpfi_bandmatrix_t M_Ktrunc, const double_bandmatrix_t M_Ktrunc_inv);
int chebmodel_intop_newton_bound_fr(mpfr_t bound, const chebmodel_intop_t K, const mpfi_chebpoly_intop_t Ktrunc, const mpfi_bandmatrix_t M_Ktrunc, const mpfr_bandmatrix_t M_Ktrunc_inv);
#define chebmodel_intop_newton_bound chebmodel_intop_newton_bound_d


// start from N = init_N and double N the paramaters of the approx inverse until the Newton Operator is contractant

void chebmodel_intop_newton_contract_d(double_t bound, double_bandmatrix_t M_K_inv, const chebmodel_intop_t K, long init_N);
void chebmodel_intop_newton_contract_fr(mpfr_t bound, mpfr_bandmatrix_t M_K_inv, const chebmodel_intop_t K, long init_N);
#define chebmodel_intop_newton_contract chebmodel_intop_newton_contract_d

void chebmodel_intop_newton_certify_d(double_t bound, double_bandmatrix_t M_K_inv, const chebmodel_intop_t K, long N, long h2, long d2);
void chebmodel_intop_newton_certify_fr(mpfr_t bound, mpfr_bandmatrix_t M_K_inv, const chebmodel_intop_t K, long N, long h2, long d2);

void chebmodel_intop_newton_validate_sol_aux_d(mpfr_t bound, const chebmodel_intop_t K, const double_t T_norm, const double_bandmatrix_t M_K_inv, const chebmodel_t G, const mpfr_chebpoly_t P);
void chebmodel_intop_newton_validate_sol_aux_fr(mpfr_t bound, const chebmodel_intop_t K, const mpfr_t T_norm, const mpfr_bandmatrix_t M_K_inv, const chebmodel_t G, const mpfr_chebpoly_t P);
#define chebmodel_intop_newton_validate_sol_aux chebmodel_intop_newton_validate_sol_aux_d

void chebmodel_intop_newton_validate_sol_d(mpfr_t bound, const chebmodel_intop_t K, const chebmodel_t G, const mpfr_chebpoly_t P, long init_N);
void chebmodel_intop_newton_validate_sol_fr(mpfr_t bound, const chebmodel_intop_t K, const chebmodel_t G, const mpfr_chebpoly_t P, long init_N);
#define chebmodel_intop_newton_validate_sol chebmodel_intop_newton_validate_sol_d

void chebmodel_lode_intop_newton_validate_sol_d(mpfr_t bound, const chebmodel_lode_t L, const chebmodel_t g, mpfi_srcptr I, const mpfr_chebpoly_t P);
void chebmodel_lode_intop_newton_validate_sol_fr(mpfr_t bound, const chebmodel_lode_t L, const chebmodel_t g, mpfi_srcptr I, const mpfr_chebpoly_t P);
#define chebmodel_lode_intop_newton_validate_sol chebmodel_lode_intop_newton_validate_sol_d


void chebmodel_intop_newton_solve_d(chebmodel_t P, const chebmodel_intop_t K, const chebmodel_t G, long N);
void chebmodel_intop_newton_solve_fr(chebmodel_t P, const chebmodel_intop_t K, const chebmodel_t G, long N);
#define chebmodel_intop_newton_solve chebmodel_intop_newton_solve_d

void chebmodel_lode_intop_newton_solve_d(chebmodel_ptr Sols, const chebmodel_lode_t L, const chebmodel_t g, mpfi_srcptr I, long N);
void chebmodel_lode_intop_newton_solve_fr(chebmodel_ptr Sols, const chebmodel_lode_t L, const chebmodel_t g, mpfi_srcptr I, long N);
#define chebmodel_lode_intop_newton_solve chebmodel_lode_intop_newton_solve_d


#endif
