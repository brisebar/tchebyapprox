
/*
This file is part of the TchebyApprox library.

The type [chebmodel_vec_lode_t] represents a vectorial 
linear differential operator of order [order] and dimension [dim]:
  - normalized (leading coeff = 1)
  - with matricial coefficients A[0][i][j], ..., A[order-1][i][j]
    (0 <= i,j <= [dim]-1) represented by [chebmodel_t].
*/


#ifndef CHEBMODEL_VEC_LODE_H
#define CHEBMODEL_VEC_LODE_H


#include <stdio.h>
#include <stdlib.h>

#include <gmp.h>
#include <mpfr.h>
#include <mpfi.h>
#include <mpfi_io.h>

#include "double_operations.h"

#include "chebmodel.h"
#include "double_chebpoly_vec.h"
#include "mpfr_chebpoly_vec.h"
#include "mpfi_chebpoly_vec.h"
#include "chebmodel_vec.h"

#include "chebmodel_lode.h"
#include "double_chebpoly_vec_lode.h"
#include "mpfr_chebpoly_vec_lode.h"
#include "mpfi_chebpoly_vec_lode.h"


// structure for a linear differential equation with polynomial coefficients
// represent the equation Y^(r) + A[r-1].Y^(r-1) + ... + A[1].Y' + A[0].Y = 0
// where r = order
typedef struct {
  long order;
  long dim;
  chebmodel_ptr_ptr_ptr A;
} chebmodel_vec_lode_struct;

typedef chebmodel_vec_lode_struct chebmodel_vec_lode_t[1];

typedef chebmodel_vec_lode_struct * chebmodel_vec_lode_ptr;

typedef const chebmodel_vec_lode_struct * chebmodel_vec_lode_srcptr;


/* basic manipulations */

void chebmodel_vec_lode_init(chebmodel_vec_lode_t L);

void chebmodel_vec_lode_clear(chebmodel_vec_lode_t L);

void chebmodel_vec_lode_set(chebmodel_vec_lode_t L, const chebmodel_vec_lode_t M);

void chebmodel_vec_lode_set_double_chebpoly_vec_lode(chebmodel_vec_lode_t L, const double_chebpoly_vec_lode_t M);

void chebmodel_vec_lode_set_mpfr_chebpoly_vec_lode(chebmodel_vec_lode_t L, const mpfr_chebpoly_vec_lode_t M);

void chebmodel_vec_lode_set_mpfi_chebpoly_vec_lode(chebmodel_vec_lode_t L, const mpfi_chebpoly_vec_lode_t M);

void chebmodel_vec_lode_get_double_chebpoly_vec_lode(double_chebpoly_vec_lode_t L, const chebmodel_vec_lode_t M);

void chebmodel_vec_lode_get_mpfr_chebpoly_vec_lode(mpfr_chebpoly_vec_lode_t L, const chebmodel_vec_lode_t M);

void chebmodel_vec_lode_get_mpfi_chebpoly_vec_lode(mpfi_chebpoly_vec_lode_t L, const chebmodel_vec_lode_t M);

void chebmodel_vec_lode_get_chebmodel_lode(chebmodel_lode_t Lij, const chebmodel_vec_lode_t L, long i, long j);

void chebmodel_vec_lode_swap(chebmodel_vec_lode_t L, chebmodel_vec_lode_t M);

void chebmodel_vec_lode_set_order_dim(chebmodel_vec_lode_t L, long order, long dim);


// non sense
/* evaluation on vectors of polynomials : compute P = L.Q */
/*
void mpfi_chebpoly_vec_lode_evaluate_d(mpfi_chebpoly_vec_t P, const mpfi_chebpoly_vec_lode_t L, const double_chebpoly_vec_t Q);

void mpfi_chebpoly_vec_lode_evaluate_fr(mpfi_chebpoly_vec_t P, const mpfi_chebpoly_vec_lode_t L, const mpfr_chebpoly_vec_t Q);

void mpfi_chebpoly_vec_lode_evaluate_fi(mpfi_chebpoly_vec_t P, const mpfi_chebpoly_vec_lode_t L, const mpfi_chebpoly_vec_t Q);
*/

/* print */
// useful?
// void mpfi_chebpoly_vec_lode_print(const mpfi_chebpoly_vec_lode_t L, const char * var, const char * Cheb_var, size_t digits);

#endif
