
/*
This file is part of the TchebyApprox library.

The type [chebmodel_vec_t] implements a vector of
[chebmodel_chebpoly_t] of length [dim]:
poly[0], ... , poly[dim-1]
*/


#ifndef CHEBMODEL_VEC_H
#define CHEBMODEL_VEC_H

#include <stdio.h>
#include <stdlib.h>

#include <gmp.h>
#include <mpfr.h>
#include <mpfi.h>
#include <mpfi_io.h>
#include "double_operations.h"

#include "mpfi_vec.h"

#include "chebmodel.h"
#include "mpfi_chebpoly_vec.h"
#include "mpfr_chebpoly_vec.h"
#include "double_chebpoly_vec.h"


typedef struct chebmodel_vec_struct {
  long dim;
  chebmodel_ptr poly;
} chebmodel_vec_struct;

typedef chebmodel_vec_struct chebmodel_vec_t[1];
typedef chebmodel_vec_struct * chebmodel_vec_ptr;
typedef const chebmodel_vec_struct * chebmodel_vec_srcptr;


/* memory management */

// init P with dim = 0
void chebmodel_vec_init(chebmodel_vec_t P);

// clear P
void chebmodel_vec_clear(chebmodel_vec_t P);

// set dim
void chebmodel_vec_set_dim(chebmodel_vec_t P, long n);


/* basic manipulations and assignments */

void chebmodel_vec_set(chebmodel_vec_t P, const chebmodel_vec_t Q);

void chebmodel_vec_set_double_chebpoly_vec(chebmodel_vec_t P, const double_chebpoly_vec_t Q);

void chebmodel_vec_set_mpfr_chebpoly_vec(chebmodel_vec_t P, const mpfr_chebpoly_vec_t Q);

void chebmodel_vec_set_mpfi_chebpoly_vec(chebmodel_vec_t P, const mpfi_chebpoly_vec_t Q);

void chebmodel_vec_get_double_chebpoly_vec(double_chebpoly_vec_t P, const chebmodel_vec_t Q);

void chebmodel_vec_get_mpfr_chebpoly_vec(mpfr_chebpoly_vec_t P, const chebmodel_vec_t Q);

void chebmodel_vec_get_mpfi_chebpoly_vec(mpfi_chebpoly_vec_t P, const chebmodel_vec_t Q);

void chebmodel_vec_swap(chebmodel_vec_t P, chebmodel_vec_t Q);

// max degree of the P[i]
long chebmodel_vec_degree(const chebmodel_vec_t P);

// set all P[i] to 0 (without changing dim)
void chebmodel_vec_zero(chebmodel_vec_t P);


/* componentwise evaluation */

void chebmodel_vec_evaluate_d(mpfi_vec_t Y, const chebmodel_vec_t P, const double_t x);

void chebmodel_vec_evaluate_fr(mpfi_vec_t Y, const chebmodel_vec_t P, const mpfr_t x);

void chebmodel_vec_evaluate_fi(mpfi_vec_t Y, const chebmodel_vec_t P, const mpfi_t x);


/* componentwise addition and subtraction */

void chebmodel_vec_add(chebmodel_vec_t P, const chebmodel_vec_t Q, const chebmodel_vec_t R);

void chebmodel_vec_sub(chebmodel_vec_t P, const chebmodel_vec_t Q, const chebmodel_vec_t R);

void chebmodel_vec_neg(chebmodel_vec_t P, const chebmodel_vec_t Q);


/* componentwise scalar multiplication and division */

void chebmodel_vec_scalar_mul_si(chebmodel_vec_t P, const chebmodel_vec_t Q, long c);

void chebmodel_vec_scalar_mul_z(chebmodel_vec_t P, const chebmodel_vec_t Q, const mpz_t c);

void chebmodel_vec_scalar_mul_q(chebmodel_vec_t P, const chebmodel_vec_t Q, const mpq_t c);

void chebmodel_vec_scalar_mul_fr(chebmodel_vec_t P, const chebmodel_vec_t Q, const mpfr_t c);

void chebmodel_vec_scalar_mul_fi(chebmodel_vec_t P, const chebmodel_vec_t Q, const mpfi_t c);

void chebmodel_vec_scalar_mul_2si(chebmodel_vec_t P, const chebmodel_vec_t Q, long k);


void chebmodel_vec_scalar_div_si(chebmodel_vec_t P, const chebmodel_vec_t Q, long c);

void chebmodel_vec_scalar_div_z(chebmodel_vec_t P, const chebmodel_vec_t Q, const mpz_t c);

void chebmodel_vec_scalar_div_q(chebmodel_vec_t P, const chebmodel_vec_t Q, const mpq_t c);

void chebmodel_vec_scalar_div_fr(chebmodel_vec_t P, const chebmodel_vec_t Q, const mpfr_t c);

void chebmodel_vec_scalar_div_fi(chebmodel_vec_t P, const chebmodel_vec_t Q, const mpfi_t c);

void chebmodel_vec_scalar_div_2si(chebmodel_vec_t P, const chebmodel_vec_t Q, long k);


/* componentwise multiplication */

void chebmodel_vec_mul(chebmodel_vec_t P, const chebmodel_vec_t Q, const chebmodel_vec_t R);


/* scalar product */

void chebmodel_vec_scalar_prod(chebmodel_t P, const chebmodel_vec_t Q, const chebmodel_vec_t R);


/* componentwise exponentiation */

void chebmodel_vec_pow(chebmodel_vec_t P, const chebmodel_vec_t Q, unsigned long e);


/* componentwise derivative and antiderivative */

// void chebmodel_vec_derivative(chebmodel_vec_t P, const chebmodel_vec_t Q);

void chebmodel_vec_antiderivative(chebmodel_vec_t P, const chebmodel_vec_t Q);

void chebmodel_vec_antiderivative0(chebmodel_vec_t P, const chebmodel_vec_t Q);

void chebmodel_vec_antiderivative_1(chebmodel_vec_t P, const chebmodel_vec_t Q);

void chebmodel_vec_antiderivative1(chebmodel_vec_t P, const chebmodel_vec_t Q);





 
#endif
