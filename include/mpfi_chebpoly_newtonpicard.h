
/*
This file is part of the TchebyApprox library.

Here are the routines to rigorously solve an integral equation
described by a [mpfi_chebpoly_intop_t], using a Newton-like method.

[mpfi_chebpoly_intop_newton_bound]: bound a Newton-like operator
[mpfi_chebpoly_intop_newton_bound]: create and bound a Newton-like operator
[mpfi_chebpoly_intop_newton_validate_sol_aux]: validate a candidate approximation, having already a contracting Newton-like operator
[mpfi_chebpoly_intop_newton_validate]: validate a candidate approximation by creating a contracting Newton-like operator
*/


#ifndef MPFI_CHEBPOLY_NEWTONPICARD_H
#define MPFI_CHEBPOLY_NEWTONPICARD_H

#include <stdio.h>
#include <stdlib.h>

#include <gmp.h>
#include <mpfr.h>
#include <mpfi.h>
#include <mpfi_io.h>

#include "double_operations.h"

#include "mpfi_chebpoly.h"
#include "mpfr_bandmatrix.h"
#include "mpfi_chebpoly_intop.h"
#include "mpfi_chebpoly_lrintop.h"


/* resolvent kernel approximation */
// r = LODE order
// a = LODE coefficients:
// L = D^r + a_{r-1} D^{r-1} + ... + a_0
// L* = (-D)^r + (-D)^{r-1} a_{r-1} + ... + a_0
// returns the phi's and psi's
// phi[i] = phi^(r) with L(phi) = 0 and phi^(j)(-1) = delta_ij
// psi[i] = psi with L(psi) = 0 and psi^[j](-1) = delta_ij
void mpfr_chebpoly_resolventkernel(mpfr_chebpoly_ptr phi, mpfr_chebpoly_ptr psi, mpfr_chebpoly_srcptr a, long r, long N);

/* computing and bounding Newton-Picard fixed-point operator */
// for x-D form and D-x forms
// a = coeffs
// r = LODE order
// return R and Tbound = bound for ||I-(I+R)(I+K)||
void mpfi_chebpoly_newtonpicard_xD_Tbound(mpfr_t Tbound, mpfi_chebpoly_lrintop_t R, mpfi_chebpoly_srcptr a, long r);
void mpfi_chebpoly_newtonpicard_Dx_Tbound(mpfr_t Tbound, mpfi_chebpoly_lrintop_t R, mpfi_chebpoly_srcptr a, long r);

/* validating a candidate solution */
// x-D and D-x forms

// K = associated integral operator
// R = resolvent kernel approximation
// Tbound = rigorous bound for Newton-Picard operator computed by
//       mpfi_chebpoly_newtonpicard_xD_Tbound
// g = right hand side
// f = approximate solution to be validated
// bound = rigorously computed error bound
void mpfi_chebpoly_newtonpicard_valid_aux(mpfr_t bound, const mpfi_chebpoly_lrintop_t K, const mpfr_t Tbound, const mpfi_chebpoly_lrintop_t R, const mpfi_chebpoly_t g, const mpfr_chebpoly_t f);


// a = LODE coefficients
// r = LODE order
// K = associated integral operator
// g = right hand side
// f = approximate solution to be validated
void mpfi_chebpoly_newtonpicard_xD_valid(mpfr_t bound, mpfi_chebpoly_srcptr a, long r, const mpfi_chebpoly_t g, const mpfr_chebpoly_t f);
void mpfi_chebpoly_newtonpicard_Dx_valid(mpfr_t bound, mpfi_chebpoly_srcptr a, long r, const mpfi_chebpoly_t g, const mpfr_chebpoly_t f);


/* compute approximation and validate it */

// a = LODE coefficients
// r = LODE order
// h = rhs
// v = initial conditions
// N = approximation degree
// return :
// f = approximate solution
// bound = rigorously computed error bound

// compute polynomial approx f and error bound for y^(r) with y solution of
// y^(r) + a_{r-1} y^(r-1) + ... + a_0 y = h
// y^(i)(-1) = v[i]  0 <= i < r
void mpfi_chebpoly_newtonpicard_xD_approxvalid(mpfr_chebpoly_t f, mpfr_t bound, mpfi_chebpoly_srcptr a, long r, const mpfi_chebpoly_t h, mpfi_srcptr v, long N);

// compute polynomial approx f and error bound for y solution of
// (-D)^(r)y + (-D)^(r-1)(a_{r-1} y) + ... + a_0 y = h
// y^[i](-1) = w[i]  0 <= i < r
// where y^[0] = y and y^[i+1] = a_{r-1-i} y - Dy^[i]
void mpfi_chebpoly_newtonpicard_Dx_approxvalid(mpfr_chebpoly_t f, mpfr_t bound, mpfi_chebpoly_srcptr a, long r, const mpfi_chebpoly_t h, mpfi_srcptr w, long N);





#endif
