
/*
This file is part of the TchebyApprox library.

The type [mpfi_poly_t] implements polynomials in monomial basis
with coefficients of type [mpfi_t].
[degree] = degree of the polynomial (-1 = null polynomial).
coeffs[0], ... , coeffs[degree-1] = coefficients in the monomial basis.
*/


#ifndef MPFI_POLY_H
#define MPFI_POLY_H

#include <stdio.h>
#include <stdlib.h>

#include <gmp.h>
#include <mpfr.h>
#include <mpfi.h>
#include <mpfi_io.h>

#include "double_poly.h"
#include "mpfr_poly.h"


// deg is always the exact degree of the polynomial
// by convention, deg(0) = -1

typedef struct mpfi_poly_struct {
  long degree;
  mpfi_ptr coeffs;
} mpfi_poly_struct;

typedef mpfi_poly_struct mpfi_poly_t[1];
typedef mpfi_poly_struct * mpfi_poly_ptr;
typedef const mpfi_poly_struct * mpfi_poly_srcptr;



/* memory management */

// init with P = 0
void mpfi_poly_init(mpfi_poly_t P);

// clear P
void mpfi_poly_clear(mpfi_poly_t P);

// set n as degree for P
void mpfi_poly_set_degree(mpfi_poly_t P, long n);

// set P in canonical form (exact degree)
void mpfi_poly_normalise(mpfi_poly_t P);



/* basic manipulations and assignments */

void _mpfi_poly_set(mpfi_ptr P, mpfi_srcptr Q, long n);

// copy Q into P
void mpfi_poly_set(mpfi_poly_t P, const mpfi_poly_t Q);

void _mpfi_poly_set_double_poly(mpfi_ptr P, double_srcptr Q, long n);

// copy Q (with double coefficients) into P
void mpfi_poly_set_double_poly(mpfi_poly_t P, const double_poly_t Q);

void _mpfi_poly_set_mpfr_poly(mpfi_ptr P, mpfr_srcptr Q, long n);

// copy Q (with mpfr_coefficients) into P
void mpfi_poly_set_mpfr_poly(mpfi_poly_t P, const mpfr_poly_t Q);

void _mpfi_poly_get_mpfr_poly(mpfr_ptr P, mpfi_srcptr Q, long n);

// get P (with double coefficients) from Q
void mpfi_poly_get_double_poly(double_poly_t P, const mpfi_poly_t Q);

void _mpfi_poly_get_double_poly(double_ptr P, mpfi_srcptr Q, long n);

// get P (with mpfr coefficients) from Q
void mpfi_poly_get_mpfr_poly(mpfr_poly_t P, const mpfi_poly_t Q);

// swap P and Q efficiently
void mpfi_poly_swap(mpfi_poly_t P, mpfi_poly_t Q);

// set the n-th coefficient of P to c
void mpfi_poly_set_coeff_si(mpfi_poly_t P, long n, long c);

// set the n-th coefficient of P to c
void mpfi_poly_set_coeff_z(mpfi_poly_t P, long n, const mpz_t c);

// set the n-th coefficient of P to c
void mpfi_poly_set_coeff_q(mpfi_poly_t P, long n, const mpq_t c);

// set the n-th coefficient of P to c
void mpfi_poly_set_coeff_fr(mpfi_poly_t P, long n, const mpfr_t c);

// set the n-th coefficient of P to c
void mpfi_poly_set_coeff_fi(mpfi_poly_t P, long n, const mpfi_t c);

// get the degree of P
long mpfi_poly_degree(const mpfi_poly_t P);

// get the n-th coefficient of P
void mpfi_poly_get_coeff(mpfi_t c, const mpfi_poly_t P, long n);

// get a pointer to the n-th coefficient of P
// return NULL if n < 0 or if n > degree(P)
#define mpfi_poly_get_coeff_ptr(P, n) (((n) <= (P)->degree) && (n >= 0) ? (P)->coeffs + (n) : NULL)

// set P to 0
void mpfi_poly_zero(mpfi_poly_t P);

// set P to (1/c)*Q where c is the leading coefficient of P
void _mpfi_poly_monic(mpfi_ptr P, const mpfi_srcptr Q, long n);

// set P to (1/c)*Q where c is the leading coefficient of P
void mpfi_poly_monic(mpfi_poly_t P, const mpfi_poly_t Q);


/* shifting, reverse */

void _mpfi_poly_shift_left(mpfi_ptr P, mpfi_srcptr Q, long n, unsigned long k);

// set P to Q*X^k
void mpfi_poly_shift_left(mpfi_poly_t P, const mpfi_poly_t Q, unsigned long k);

void _mpfi_poly_shift_right(mpfi_ptr P, mpfi_srcptr Q, long n, unsigned long k);

// set P to Q/X^k
void mpfi_poly_shift_right(mpfi_poly_t P, const mpfi_poly_t Q, unsigned long k);

// reverse a list of mpfi of size (n+1)
void _mpfi_poly_reverse(mpfi_ptr P, mpfi_srcptr Q, long n);

// set P to Q(X^-1)*X^(degree(Q))
void mpfi_poly_reverse(mpfi_poly_t P, const mpfi_poly_t Q);


/* norm 1 */

void _mpfi_poly_1norm(mpfi_t y, mpfi_srcptr P, long n);

void _mpfi_poly_1norm_ubound(mpfr_t y, mpfi_srcptr P, long n);

void _mpfi_poly_1norm_lbound(mpfr_t y, mpfi_srcptr P, long n);

void mpfi_poly_1norm(mpfi_t y, const mpfi_poly_t P);

void mpfi_poly_1norm_ubound(mpfr_t y, const mpfi_poly_t P);

void mpfi_poly_1norm_lbound(mpfr_t y, const mpfi_poly_t P);


/* test functions */

#define mpfi_poly_is_zero(P) ((P)->degree < 0)

#define mpfi_poly_is_constant(P) ((P)->degree == 0)

#define mpfi_poly_is_monic(P) (mpfr_cmp_si(&(((P)->coeffs + (P)->degree)->left), 1) == 0 && mpfr_cmp_si(&(((P)->coeffs + (P)->degree)->right), 1) == 0)

int _mpfi_poly_equal(mpfi_srcptr P, mpfi_srcptr Q, long n);

int mpfi_poly_equal(const mpfi_poly_t P, const mpfi_poly_t Q);


/* evaluation */

void _mpfi_poly_evaluate_si(mpfi_t y, mpfi_srcptr P, long n, long x);

// set y to P(x)
void mpfi_poly_evaluate_si(mpfi_t y, const mpfi_poly_t P, long x);

void _mpfi_poly_evaluate_z(mpfi_t y, mpfi_srcptr P, long n, const mpz_t x);

// set y to P(x)
void mpfi_poly_evaluate_z(mpfi_t y, const mpfi_poly_t P, const mpz_t x);

void _mpfi_poly_evaluate_q(mpfi_t y, mpfi_srcptr P, long n, const mpq_t x);

// set y to P(x)
void mpfi_poly_evaluate_q(mpfi_t y, const mpfi_poly_t P, const mpq_t x);

void _mpfi_poly_evaluate_fr(mpfi_t y, mpfi_srcptr P, long n, const mpfr_t x);

// set y to P(x)
void mpfi_poly_evaluate_fr(mpfi_t y, const mpfi_poly_t P, const mpfr_t x);

void _mpfi_poly_evaluate_fi(mpfi_t y, mpfi_srcptr P, long n, const mpfi_t x);

// set y to P(x)
void mpfi_poly_evaluate_fi(mpfi_t y, const mpfi_poly_t P, const mpfi_t x);


/* addition and substraction */

void _mpfi_poly_add(mpfi_ptr P, mpfi_srcptr Q, long n, mpfi_srcptr R, long m);

// set P := Q + R
void mpfi_poly_add(mpfi_poly_t P, const mpfi_poly_t Q, const mpfi_poly_t R);

void _mpfi_poly_sub(mpfi_ptr P, mpfi_srcptr Q, long n, mpfi_srcptr R, long m);

// set P := Q - R
void mpfi_poly_sub(mpfi_poly_t P, const mpfi_poly_t Q, const mpfi_poly_t R);

void _mpfi_poly_neg(mpfi_ptr P, mpfi_srcptr Q, long n);

// set P := -Q
void mpfi_poly_neg(mpfi_poly_t P, const mpfi_poly_t Q);


/* scalar multiplication and division */

void _mpfi_poly_scalar_mul_si(mpfi_ptr P, mpfi_srcptr Q, long n, long c);

// set P := c*Q
void mpfi_poly_scalar_mul_si(mpfi_poly_t P, const mpfi_poly_t Q, long c);

void _mpfi_poly_scalar_mul_z(mpfi_ptr P, mpfi_srcptr Q, long n, const mpz_t c);

// set P := c*Q
void mpfi_poly_scalar_mul_z(mpfi_poly_t P, const mpfi_poly_t Q, const mpz_t c);

void _mpfi_poly_scalar_mul_q(mpfi_ptr P, mpfi_srcptr Q, long n, const mpq_t c);

// set P := c*Q
void mpfi_poly_scalar_mul_q(mpfi_poly_t P, const mpfi_poly_t Q, const mpq_t c);

void _mpfi_poly_scalar_mul_d(mpfi_ptr P, mpfi_srcptr Q, long n, const double_t c);

// set P := c*Q
void mpfi_poly_scalar_mul_d(mpfi_poly_t P, const mpfi_poly_t Q, const double_t c);

void _mpfi_poly_scalar_mul_fr(mpfi_ptr P, mpfi_srcptr Q, long n, const mpfr_t c);

// set P := c*Q
void mpfi_poly_scalar_mul_fr(mpfi_poly_t P, const mpfi_poly_t Q, const mpfr_t c);

void _mpfi_poly_scalar_mul_fi(mpfi_ptr P, mpfi_srcptr Q, long n, const mpfi_t c);

// set P := c*Q
void mpfi_poly_scalar_mul_fi(mpfi_poly_t P, const mpfi_poly_t Q, const mpfi_t c);

void _mpfi_poly_scalar_mul_2si(mpfi_ptr P, mpfi_srcptr Q, long n, long k);

// set P := 2^k*Q
void mpfi_poly_scalar_mul_2si(mpfi_poly_t P, const mpfi_poly_t Q, long k);


void _mpfi_poly_scalar_div_si(mpfi_ptr P, mpfi_srcptr Q, long n, long c);

// set P := (1/c)*Q
void mpfi_poly_scalar_div_si(mpfi_poly_t P, const mpfi_poly_t Q, long c);

void _mpfi_poly_scalar_div_z(mpfi_ptr P, mpfi_srcptr Q, long n, const mpz_t c);

// set P := (1/c)*Q
void mpfi_poly_scalar_div_z(mpfi_poly_t P, const mpfi_poly_t Q, const mpz_t c);

void _mpfi_poly_scalar_div_q(mpfi_ptr P, mpfi_srcptr Q, long n, const mpq_t c);

// set P := (1/c)*Q
void mpfi_poly_scalar_div_q(mpfi_poly_t P, const mpfi_poly_t Q, const mpq_t c);

void _mpfi_poly_scalar_div_d(mpfi_ptr P, mpfi_srcptr Q, long n, const double_t c);

// set P := (1/c)*Q
void mpfi_poly_scalar_div_d(mpfi_poly_t P, const mpfi_poly_t Q, const double_t c);

void _mpfi_poly_scalar_div_fr(mpfi_ptr P, mpfi_srcptr Q, long n, const mpfr_t c);

// set P := (1/c)*Q
void mpfi_poly_scalar_div_fr(mpfi_poly_t P, const mpfi_poly_t Q, const mpfr_t c);

void _mpfi_poly_scalar_div_fi(mpfi_ptr P, mpfi_srcptr Q, long n, const mpfi_t c);

// set P := (1/c)*Q
void mpfi_poly_scalar_div_fi(mpfi_poly_t P, const mpfi_poly_t Q, const mpfi_t c);

void _mpfi_poly_scalar_div_2si(mpfi_ptr P, mpfi_srcptr Q, long n, long k);

// set P := 2^-k*Q
void mpfi_poly_scalar_div_2si(mpfi_poly_t P, const mpfi_poly_t Q, long k);




/* multiplication */

void _mpfi_poly_mul(mpfi_ptr P, mpfi_srcptr Q, long n, mpfi_srcptr R, long m);

// set P := Q * R
void mpfi_poly_mul(mpfi_poly_t P, const mpfi_poly_t Q, const mpfi_poly_t R);


/* exponentiation */

// set P := Q^e
void mpfi_poly_pow(mpfi_poly_t P, const mpfi_poly_t Q, unsigned long e);


/* derivative and antiderivative */

void _mpfi_poly_derivative(mpfi_ptr P, mpfi_srcptr Q, long n);

// set P := Q'
void mpfi_poly_derivative(mpfi_poly_t P, const mpfi_poly_t Q);

void _mpfi_poly_antiderivative(mpfi_ptr P, mpfi_srcptr Q, long n);

// set P st P' = Q and P(0) = 0
void mpfi_poly_antiderivative(mpfi_poly_t P, const mpfi_poly_t Q);


/* display function */


void _mpfi_poly_print(mpfi_srcptr P, long n, const char * var, size_t digits);

// display the polynomial P with 'var' as indeterminate symbol
// 'var' is a 0-terminated string
void mpfi_poly_print(const mpfi_poly_t P, const char * var, size_t digits);




#endif
