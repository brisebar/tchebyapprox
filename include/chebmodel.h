
/*
This file is part of the TchebyApprox library.

The type [chebmodel_t] implements Chebyshev models for the Che1 norm
as a pair ([poly],[rem]).
Call n = [poly->degree].
It rigorously represents a function f in Che1 if:
  - there is p = a0*T0 + a1*T1 + ... + an*Tn with
      ai belongs to interval [poly->coeffs[i]] for i = 0 ... n.
  - such that ||f-p||_{Che1} <= [rem].
*/


#ifndef CHEBMODEL_H
#define CHEBMODEL_H

#include <stdio.h>
#include <stdlib.h>

#include <gmp.h>
#include <mpfr.h>
#include <mpfi.h>
#include <mpfi_io.h>

#include "double_operations.h"

#include "double_chebpoly.h"
#include "mpfr_chebpoly.h"
#include "mpfi_chebpoly.h"


typedef struct chebmodel_struct {
  mpfi_chebpoly_t poly;
  mpfr_t rem;
} chebmodel_struct;

typedef chebmodel_struct chebmodel_t[1];
typedef chebmodel_struct * chebmodel_ptr;
typedef const chebmodel_struct * chebmodel_srcptr;
typedef chebmodel_ptr * chebmodel_ptr_ptr;
typedef chebmodel_ptr_ptr * chebmodel_ptr_ptr_ptr;


/* memory management */

// init with P = 0
void chebmodel_init(chebmodel_t P);

// clear P
void chebmodel_clear(chebmodel_t P);

// set n as degree for P
void chebmodel_set_degree(chebmodel_t P, long n);

// set P in canonical form (exact degree)
void chebmodel_normalise(chebmodel_t P);


/* basic manipulations and assignments */

// copy Q into P
void chebmodel_set(chebmodel_t P, const chebmodel_t Q);

void chebmodel_set_double_chebpoly(chebmodel_t P, const double_chebpoly_t Q);

void chebmodel_set_mpfr_chebpoly(chebmodel_t P, const mpfr_chebpoly_t Q);

void chebmodel_set_mpfi_chebpoly(chebmodel_t P, const mpfi_chebpoly_t Q);

void chebmodel_get_double_chebpoly(double_chebpoly_t P, const chebmodel_t Q);

void chebmodel_get_mpfr_chebpoly(mpfr_chebpoly_t P, const chebmodel_t Q);

void chebmodel_get_mpfi_chebpoly(mpfi_chebpoly_t P, const chebmodel_t Q);

// swap P and Q efficiently
void chebmodel_swap(chebmodel_t P, chebmodel_t Q);

// get the degree of P->poly
long chebmodel_degree(const chebmodel_t P);

// get the n-th coefficient of the function represented by P, estimated as the n-th coefficient of P->poly enlarged with P->rem
void chebmodel_get_coeff(mpfi_t c, const chebmodel_t P, long n);

// get a pointer to the n-th coefficient of P
// return NULL if n < 0 or if n > degree(P)
#define chebmodel_get_coeff_ptr(P, n) (((n) <= (P)->poly->degree) && (n >= 0) ? (P)->poly->coeffs + (n) : NULL)

// set P to 0
void chebmodel_zero(chebmodel_t P);

// take the midpoint of all interval coefficients and add the radii to rem
void chebmodel_flatten(chebmodel_t P);

// truncate the chebmodel and put the erased part into the rem
void chebmodel_truncate(chebmodel_t P, long n);

/* norm 1 in the Chebyshev basis */

void chebmodel_1norm(mpfi_t norm, const chebmodel_t P);

void chebmodel_1norm_ubound(mpfr_t norm, const chebmodel_t P);

void chebmodel_1norm_lbound(mpfr_t norm, const chebmodel_t P);


/* evaluation */

// set y to P(x)

void chebmodel_evaluate_si(mpfi_t y, const chebmodel_t P, long x);

void chebmodel_evaluate_z(mpfi_t y, const chebmodel_t P, const mpz_t x);

void chebmodel_evaluate_q(mpfi_t y, const chebmodel_t P, const mpq_t x);

void chebmodel_evaluate_d(mpfi_t y, const chebmodel_t P, const double_t x);

void chebmodel_evaluate_fr(mpfi_t y, const chebmodel_t P, const mpfr_t x);

void chebmodel_evaluate_fi(mpfi_t y, const chebmodel_t P, const mpfi_t x);

// set y to P(-1)
void chebmodel_evaluate_1(mpfi_t y, const chebmodel_t P);

// set y to P(0)
void chebmodel_evaluate0(mpfi_t y, const chebmodel_t P);

// set y to P(1)
void chebmodel_evaluate1(mpfi_t y, const chebmodel_t P);


/* addition and substraction */

// set P := Q + R
void chebmodel_add(chebmodel_t P, const chebmodel_t Q, const chebmodel_t R);

// set P := Q - R
void chebmodel_sub(chebmodel_t P, const chebmodel_t Q, const chebmodel_t R);

// set P := -Q
void chebmodel_neg(chebmodel_t P, const chebmodel_t Q);


/* scalar multiplication and division */

// set P := c*Q
void chebmodel_scalar_mul_si(chebmodel_t P, const chebmodel_t Q, long c);

// set P := c*Q
void chebmodel_scalar_mul_z(chebmodel_t P, const chebmodel_t Q, const mpz_t c);

// set P := c*Q
void chebmodel_scalar_mul_q(chebmodel_t P, const chebmodel_t Q, const mpq_t c);

// set P := c*Q
void chebmodel_scalar_mul_d(chebmodel_t P, const chebmodel_t Q, const double_t c);

// set P := c*Q
void chebmodel_scalar_mul_fr(chebmodel_t P, const chebmodel_t Q, const mpfr_t c);

// set P := c*Q
void chebmodel_scalar_mul_fi(chebmodel_t P, const chebmodel_t Q, const mpfi_t c);

// set P := 2^k*Q
void chebmodel_scalar_mul_2si(chebmodel_t P, const chebmodel_t Q, long k);


// set P := (1/c)*Q
void chebmodel_scalar_div_si(chebmodel_t P, const chebmodel_t Q, long c);

// set P := (1/c)*Q
void chebmodel_scalar_div_z(chebmodel_t P, const chebmodel_t Q, const mpz_t c);

// set P := (1/c)*Q
void chebmodel_scalar_div_q(chebmodel_t P, const chebmodel_t Q, const mpq_t c);

// set P := (1/c)*Q
void chebmodel_scalar_div_d(chebmodel_t P, const chebmodel_t Q, const double_t c);

// set P := (1/c)*Q
void chebmodel_scalar_div_fr(chebmodel_t P, const chebmodel_t Q, const mpfr_t c);

// set P := (1/c)*Q
void chebmodel_scalar_div_fi(chebmodel_t P, const chebmodel_t Q, const mpfi_t c);

// set P := 2^-k*Q
void chebmodel_scalar_div_2si(chebmodel_t P, const chebmodel_t Q, long k);


/* multiplication */

// set P := Q * R
void chebmodel_mul(chebmodel_t P, const chebmodel_t Q, const chebmodel_t R);


/* exponentiation */

// set P := Q^e
void chebmodel_pow(chebmodel_t P, const chebmodel_t Q, unsigned long e);


/* antiderivative */

// set P st P' = Q and 0-th coeff of P = 0
void chebmodel_antiderivative(chebmodel_t P, const chebmodel_t Q);

// set P st P' = Q and P(0) = 0
void chebmodel_antiderivative0(chebmodel_t P, const chebmodel_t Q);

// set P st P' = Q and P(-1) = 0
void chebmodel_antiderivative_1(chebmodel_t P, const chebmodel_t Q);

// set P st P' = Q and P(1) = 0
void chebmodel_antiderivative1(chebmodel_t P, const chebmodel_t Q);


/* inverse and division */

// N = degree of polynomial approximation
void chebmodel_inverse(chebmodel_t P, const chebmodel_t Q, long N);

// N = degree of polynomial approximation
void chebmodel_div(chebmodel_t R, const chebmodel_t P, const chebmodel_t Q, long N);


/* square root and inverse square root */

// N = degree of polynomial approximation
void chebmodel_sqrt(chebmodel_t P, const chebmodel_t Q, long N);


// N = degree of polynomial approximation
void chebmodel_inv_sqrt(chebmodel_t P, const chebmodel_t Q, long N);


/* display function */

// display the polynomial P with 'var' as indeterminate symbol
// 'var' is a 0-terminated string
void chebmodel_print(const chebmodel_t P, const char * var, size_t digits);




#endif
