
/*
This file is part of the TchebyApprox library.

The type [mpfr_chebpoly_lrintop_t] represents a (scalar)
linear Volterra integral operator of the second kind
with a bivariate polynomial kernel given as a low rank decomposition with [mpfr_chebpoly_t], that is
K(t,s) = sum_{0 <= i < length} lpoly[t] * rpoly[s]
*/


#ifndef MPFR_CHEBPOLY_LRINTOP_H
#define MPFR_CHEBPOLY_LRINTOP_H


#include <stdio.h>
#include <stdlib.h>

#include <gmp.h>
#include <mpfr.h>
#include <mpfi.h>
#include <mpfi_io.h>

#include "double_operations.h"

#include "double_chebpoly.h"
#include "mpfr_chebpoly.h"
#include "mpfi_chebpoly.h"
#include "chebmodel.h"

#include "mpfr_chebpoly_lode.h"
#include "mpfr_chebpoly_intop.h"
#include "mpfr_bandmatrix.h"

#include "double_chebpoly_lrintop.h"

typedef struct {
  long length;
  mpfr_chebpoly_ptr lpoly;
  mpfr_chebpoly_ptr rpoly;
} mpfr_chebpoly_lrintop_struct;

typedef mpfr_chebpoly_lrintop_struct mpfr_chebpoly_lrintop_t[1];

typedef mpfr_chebpoly_lrintop_struct * mpfr_chebpoly_intop_lrptr;

typedef const mpfr_chebpoly_lrintop_struct * mpfr_chebpoly_lrintop_srcptr;




void mpfr_chebpoly_lrintop_init(mpfr_chebpoly_lrintop_t K);

void mpfr_chebpoly_lrintop_clear(mpfr_chebpoly_lrintop_t K);

void mpfr_chebpoly_lrintop_set(mpfr_chebpoly_lrintop_t L, const mpfr_chebpoly_lrintop_t K);

void mpfr_chebpoly_lrintop_set_double_chebpoly_lrintop(mpfr_chebpoly_lrintop_t L, const double_chebpoly_lrintop_t K);

void mpfr_chebpoly_lrintop_get_double_chebpoly_lrintop(double_chebpoly_lrintop_t L, const mpfr_chebpoly_lrintop_t K);

void mpfr_chebpoly_lrintop_swap(mpfr_chebpoly_lrintop_t L, mpfr_chebpoly_lrintop_t K);

void mpfr_chebpoly_lrintop_set_length(mpfr_chebpoly_lrintop_t K, long length);


// evaluate on a polynomial

void mpfr_chebpoly_lrintop_evaluate_d(mpfr_chebpoly_t P, const mpfr_chebpoly_lrintop_t K, const double_chebpoly_t Q);

void mpfr_chebpoly_lrintop_evaluate_fr(mpfr_chebpoly_t P, const mpfr_chebpoly_lrintop_t K, const mpfr_chebpoly_t Q);

void mpfr_chebpoly_lrintop_evaluate_fi(mpfi_chebpoly_t P, const mpfr_chebpoly_lrintop_t K, const mpfi_chebpoly_t Q);

void mpfr_chebpoly_lrintop_evaluate_cm(chebmodel_t P, const mpfr_chebpoly_lrintop_t K, const chebmodel_t Q);


// bounds

// crude bound on the sum
// A REVOIR : facteur 2 ?
void mpfr_chebpoly_lrintop_lr1norm(mpfr_t b, const mpfr_chebpoly_lrintop_t K);

// convert to 2-dimensional polynomial in Chebyshev basis and use 1 norm
void mpfr_chebpoly_lrintop_1norm(mpfr_t b, const mpfr_chebpoly_lrintop_t K);


// operations

void _mpfr_chebpoly_lrintop_neg(mpfr_chebpoly_ptr Llpoly, mpfr_chebpoly_ptr Lrpoly, mpfr_chebpoly_srcptr Klpoly, mpfr_chebpoly_srcptr Krpoly, long r);

// set L := -K
void mpfr_chebpoly_lrintop_neg(mpfr_chebpoly_lrintop_t L, const mpfr_chebpoly_lrintop_t K);

void _mpfr_chebpoly_lrintop_add(mpfr_chebpoly_ptr Mlpoly, mpfr_chebpoly_ptr Mrpoly, mpfr_chebpoly_srcptr Klpoly, mpfr_chebpoly_srcptr Krpoly, long r1, mpfr_chebpoly_srcptr Llpoly, mpfr_chebpoly_srcptr Lrpoly, long r2);

// set M := K + L
void mpfr_chebpoly_lrintop_add(mpfr_chebpoly_lrintop_t M, const mpfr_chebpoly_lrintop_t K, const mpfr_chebpoly_lrintop_t L);

void _mpfr_chebpoly_lrintop_sub(mpfr_chebpoly_ptr Mlpoly, mpfr_chebpoly_ptr Mrpoly, mpfr_chebpoly_srcptr Klpoly, mpfr_chebpoly_srcptr Krpoly, long r1, mpfr_chebpoly_srcptr Llpoly, mpfr_chebpoly_srcptr Lrpoly, long r2);

// set M := K - L
void mpfr_chebpoly_lrintop_sub(mpfr_chebpoly_lrintop_t M, const mpfr_chebpoly_lrintop_t K, const mpfr_chebpoly_lrintop_t L);

void _mpfr_chebpoly_lrintop_scalar_mul_si(mpfr_chebpoly_ptr Llpoly, mpfr_chebpoly_ptr Lrpoly, mpfr_chebpoly_srcptr Klpoly, mpfr_chebpoly_srcptr Krpoly, long r, long c);

// set L := c * K
void mpfr_chebpoly_lrintop_scalar_mul_si(mpfr_chebpoly_lrintop_t L, const mpfr_chebpoly_lrintop_t K, long c);

void _mpfr_chebpoly_lrintop_scalar_mul_z(mpfr_chebpoly_ptr Llpoly, mpfr_chebpoly_ptr Lrpoly, mpfr_chebpoly_srcptr Klpoly, mpfr_chebpoly_srcptr Krpoly, long r, const mpz_t c);

// set L := c * K
void mpfr_chebpoly_lrintop_scalar_mul_z(mpfr_chebpoly_lrintop_t L, const mpfr_chebpoly_lrintop_t K, const mpz_t c);

void _mpfr_chebpoly_lrintop_scalar_mul_q(mpfr_chebpoly_ptr Llpoly, mpfr_chebpoly_ptr Lrpoly, mpfr_chebpoly_srcptr Klpoly, mpfr_chebpoly_srcptr Krpoly, long r, const mpq_t c);

// set L := c * K
void mpfr_chebpoly_lrintop_scalar_mul_q(mpfr_chebpoly_lrintop_t L, const mpfr_chebpoly_lrintop_t K, const mpq_t c);

void _mpfr_chebpoly_lrintop_scalar_mul_d(mpfr_chebpoly_ptr Llpoly, mpfr_chebpoly_ptr Lrpoly, mpfr_chebpoly_srcptr Klpoly, mpfr_chebpoly_srcptr Krpoly, long r, const double_t c);

// set L := c * K
void mpfr_chebpoly_lrintop_scalar_mul_d(mpfr_chebpoly_lrintop_t L, const mpfr_chebpoly_lrintop_t K, const double_t c);

void _mpfr_chebpoly_lrintop_scalar_mul_fr(mpfr_chebpoly_ptr Llpoly, mpfr_chebpoly_ptr Lrpoly, mpfr_chebpoly_srcptr Klpoly, mpfr_chebpoly_srcptr Krpoly, long r, const mpfr_t c);

// set L := c * K
void mpfr_chebpoly_lrintop_scalar_mul_fr(mpfr_chebpoly_lrintop_t L, const mpfr_chebpoly_lrintop_t K, const mpfr_t c);

void _mpfr_chebpoly_lrintop_scalar_mul_2si(mpfr_chebpoly_ptr Llpoly, mpfr_chebpoly_ptr Lrpoly, mpfr_chebpoly_srcptr Klpoly, mpfr_chebpoly_srcptr Krpoly, long r, long k);

// set L := 2^k * K
void mpfr_chebpoly_lrintop_scalar_mul_2si(mpfr_chebpoly_lrintop_t L, const mpfr_chebpoly_lrintop_t K, long k);

void _mpfr_chebpoly_lrintop_scalar_div_si(mpfr_chebpoly_ptr Llpoly, mpfr_chebpoly_ptr Lrpoly, mpfr_chebpoly_srcptr Klpoly, mpfr_chebpoly_srcptr Krpoly, long r, long c);

// set L := K / c
void mpfr_chebpoly_lrintop_scalar_div_si(mpfr_chebpoly_lrintop_t L, const mpfr_chebpoly_lrintop_t K, long c);

void _mpfr_chebpoly_lrintop_scalar_div_z(mpfr_chebpoly_ptr Llpoly, mpfr_chebpoly_ptr Lrpoly, mpfr_chebpoly_srcptr Klpoly, mpfr_chebpoly_srcptr Krpoly, long r, const mpz_t c);

// set L := K / c
void mpfr_chebpoly_lrintop_scalar_div_z(mpfr_chebpoly_lrintop_t L, const mpfr_chebpoly_lrintop_t K, const mpz_t c);

void _mpfr_chebpoly_lrintop_scalar_div_q(mpfr_chebpoly_ptr Llpoly, mpfr_chebpoly_ptr Lrpoly, mpfr_chebpoly_srcptr Klpoly, mpfr_chebpoly_srcptr Krpoly, long r, const mpq_t c);

// set L := K / c
void mpfr_chebpoly_lrintop_scalar_div_q(mpfr_chebpoly_lrintop_t L, const mpfr_chebpoly_lrintop_t K, const mpq_t c);

void _mpfr_chebpoly_lrintop_scalar_div_d(mpfr_chebpoly_ptr Llpoly, mpfr_chebpoly_ptr Lrpoly, mpfr_chebpoly_srcptr Klpoly, mpfr_chebpoly_srcptr Krpoly, long r, const double_t c);

// set L := K / c
void mpfr_chebpoly_lrintop_scalar_div_d(mpfr_chebpoly_lrintop_t L, const mpfr_chebpoly_lrintop_t K, const double_t c);

void _mpfr_chebpoly_lrintop_scalar_div_fr(mpfr_chebpoly_ptr Llpoly, mpfr_chebpoly_ptr Lrpoly, mpfr_chebpoly_srcptr Klpoly, mpfr_chebpoly_srcptr Krpoly, long r, const mpfr_t c);

// set L := K / c
void mpfr_chebpoly_lrintop_scalar_div_fr(mpfr_chebpoly_lrintop_t L, const mpfr_chebpoly_lrintop_t K, const mpfr_t c);

void _mpfr_chebpoly_lrintop_scalar_div_2si(mpfr_chebpoly_ptr Llpoly, mpfr_chebpoly_ptr Lrpoly, mpfr_chebpoly_srcptr Klpoly, mpfr_chebpoly_srcptr Krpoly, long r, long k);

// set L := 2^-k * K
void mpfr_chebpoly_lrintop_scalar_div_2si(mpfr_chebpoly_lrintop_t L, const mpfr_chebpoly_lrintop_t K, long k);

void _mpfr_chebpoly_lrintop_comp(mpfr_chebpoly_ptr Mlpoly, mpfr_chebpoly_ptr Mrpoly, mpfr_chebpoly_srcptr Klpoly, mpfr_chebpoly_srcptr Krpoly, long r1, mpfr_chebpoly_srcptr Llpoly, mpfr_chebpoly_srcptr Lrpoly, long r2);

// set M := K ∘ L
void mpfr_chebpoly_lrintop_comp(mpfr_chebpoly_lrintop_t M, const mpfr_chebpoly_lrintop_t K, const mpfr_chebpoly_lrintop_t L);

/* convert from mpfr_chebpoly_intop_t */
void mpfr_chebpoly_lrintop_set_intop(mpfr_chebpoly_lrintop_t M, const mpfr_chebpoly_intop_t K);

/* convert to mpfr_chebpoly_intop_t */
void mpfr_chebpoly_lrintop_get_intop(mpfr_chebpoly_intop_t M, const mpfr_chebpoly_lrintop_t K);

/* print */

void mpfr_chebpoly_lrintop_print(const mpfr_chebpoly_lrintop_t K, const char * Cheb_var, size_t digits);



/* convert LODE to lrintop */

// to integral equation on last derivative
// from x-D form: y^(r-1) + a[r-1] * y^(r-1) + ... + a[1] * y' + a[0] * y = h
// with initial conditions y^(i)(-1) = v[i] (0 <= i < r)
// to: f + ∫_-1^t K(t,s) * f(s) ds = g

// compute polynomial kernel K(t,s)
void mpfr_chebpoly_lrintop_set_lode_xD(mpfr_chebpoly_lrintop_t K, const mpfr_chebpoly_srcptr a, long r);

// compute initial condition contribution to right hand side
void mpfr_chebpoly_lrintop_set_lode_xD_initvals(mpfr_chebpoly_t g, mpfr_chebpoly_srcptr a, long r, mpfr_srcptr v);

// compute right hand side g
void mpfr_chebpoly_lrintop_set_lode_xD_rhs(mpfr_chebpoly_t g, mpfr_chebpoly_srcptr a, long r, const mpfr_chebpoly_t h, mpfr_srcptr v);


// to integral equation on the same function
// from D-x form: (-D)^(r)y + (-D)^(r-1)(b[r-1] * y) + ... + (-D)(b[1] * y) + b[0] * y = h
// with initial conditions y^[i](-1) = w[i] (0 <= i < r)
// where y^[i] = (-D)^(i)y + (-D)^(i-1)(b[r-1] * y) + ... + b[r-i] * y
// to: y + ∫_-1^t K(t,s) * y(s) ds = g

// compute polynomial kernel K(t,s)
void mpfr_chebpoly_lrintop_set_lode_Dx(mpfr_chebpoly_lrintop_t K, const mpfr_chebpoly_srcptr b, long r);

// compute initial condition contribution to right hand side
void mpfr_chebpoly_lrintop_set_lode_Dx_initvals(mpfr_chebpoly_t g, long r, mpfr_srcptr w);

// compute right hand side g
void mpfr_chebpoly_lrintop_set_lode_Dx_rhs(mpfr_chebpoly_t g, long r, const mpfr_chebpoly_t h, mpfr_srcptr w);



/* numerical solution */

// get bandmatrix
void mpfr_chebpoly_lrintop_get_bandmatrix(mpfr_bandmatrix_t M, const mpfr_chebpoly_lrintop_t K, long N);

// solve F s.t. F + K(F) = G by truncating K at order N
void mpfr_chebpoly_lrintop_approxsolve(mpfr_chebpoly_t F, const mpfr_chebpoly_lrintop_t K, const mpfr_chebpoly_t G, long N);


#endif
