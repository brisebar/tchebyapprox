
/*
This file is part of the TchebyApprox library.

The type [mpfr_chebpoly_vec_t] implements a vector of
[mpfr_chebpoly_t] of length [dim]:
poly[0], ... , poly[dim-1]
*/


#ifndef MPFR_CHEBPOLY_VEC_H
#define MPFR_CHEBPOLY_VEC_H

#include <stdio.h>
#include <stdlib.h>

#include <gmp.h>
#include <mpfr.h>
#include "double_operations.h"

#include "mpfr_vec.h"
#include "mpfi_vec.h"

#include "mpfr_chebpoly.h"
#include "double_chebpoly_vec.h"


typedef struct mpfr_chebpoly_vec_struct {
  long dim;
  mpfr_chebpoly_ptr poly;
} mpfr_chebpoly_vec_struct;

typedef mpfr_chebpoly_vec_struct mpfr_chebpoly_vec_t[1];
typedef mpfr_chebpoly_vec_struct * mpfr_chebpoly_vec_ptr;
typedef const mpfr_chebpoly_vec_struct * mpfr_chebpoly_vec_srcptr;


/* memory management */

// init P with dim = 0
void mpfr_chebpoly_vec_init(mpfr_chebpoly_vec_t P);

// clear P
void mpfr_chebpoly_vec_clear(mpfr_chebpoly_vec_t P);

// set dim
void mpfr_chebpoly_vec_set_dim(mpfr_chebpoly_vec_t P, long n);


/* basic manipulations and assignments */

void mpfr_chebpoly_vec_set(mpfr_chebpoly_vec_t P, const mpfr_chebpoly_vec_t Q);

void mpfr_chebpoly_vec_set_double_chebpoly_vec(mpfr_chebpoly_vec_t P, const double_chebpoly_vec_t Q);

void mpfr_chebpoly_vec_get_double_chebpoly_vec(double_chebpoly_vec_t P, const mpfr_chebpoly_vec_t Q);

void mpfr_chebpoly_vec_swap(mpfr_chebpoly_vec_t P, mpfr_chebpoly_vec_t Q);

// max degree of the P[i]
long mpfr_chebpoly_vec_degree(const mpfr_chebpoly_vec_t P);

// set all P[i] to 0 (without changing dim)
void mpfr_chebpoly_vec_zero(mpfr_chebpoly_vec_t P);


/* componentwise evaluation */

void mpfr_chebpoly_vec_evaluate_d(mpfr_vec_t Y, const mpfr_chebpoly_vec_t P, const double_t x);

void mpfr_chebpoly_vec_evaluate_fr(mpfr_vec_t Y, const mpfr_chebpoly_vec_t P, const mpfr_t x);

void mpfr_chebpoly_vec_evaluate_fi(mpfi_vec_t Y, const mpfr_chebpoly_vec_t P, const mpfi_t x);


/* componentwise addition and subtraction */

void mpfr_chebpoly_vec_add(mpfr_chebpoly_vec_t P, const mpfr_chebpoly_vec_t Q, const mpfr_chebpoly_vec_t R);

void mpfr_chebpoly_vec_sub(mpfr_chebpoly_vec_t P, const mpfr_chebpoly_vec_t Q, const mpfr_chebpoly_vec_t R);

void mpfr_chebpoly_vec_neg(mpfr_chebpoly_vec_t P, const mpfr_chebpoly_vec_t Q);


/* componentwise scalar multiplication and division */

void mpfr_chebpoly_vec_scalar_mul_si(mpfr_chebpoly_vec_t P, const mpfr_chebpoly_vec_t Q, long c);

void mpfr_chebpoly_vec_scalar_mul_z(mpfr_chebpoly_vec_t P, const mpfr_chebpoly_vec_t Q, const mpz_t c);

void mpfr_chebpoly_vec_scalar_mul_q(mpfr_chebpoly_vec_t P, const mpfr_chebpoly_vec_t Q, const mpq_t c);

void mpfr_chebpoly_vec_scalar_mul_fr(mpfr_chebpoly_vec_t P, const mpfr_chebpoly_vec_t Q, const mpfr_t c);

void mpfr_chebpoly_vec_scalar_mul_2si(mpfr_chebpoly_vec_t P, const mpfr_chebpoly_vec_t Q, long k);


void mpfr_chebpoly_vec_scalar_div_si(mpfr_chebpoly_vec_t P, const mpfr_chebpoly_vec_t Q, long c);

void mpfr_chebpoly_vec_scalar_div_z(mpfr_chebpoly_vec_t P, const mpfr_chebpoly_vec_t Q, const mpz_t c);

void mpfr_chebpoly_vec_scalar_div_q(mpfr_chebpoly_vec_t P, const mpfr_chebpoly_vec_t Q, const mpq_t c);

void mpfr_chebpoly_vec_scalar_div_fr(mpfr_chebpoly_vec_t P, const mpfr_chebpoly_vec_t Q, const mpfr_t c);

void mpfr_chebpoly_vec_scalar_div_2si(mpfr_chebpoly_vec_t P, const mpfr_chebpoly_vec_t Q, long k);


/* componentwise multiplication */

void mpfr_chebpoly_vec_mul(mpfr_chebpoly_vec_t P, const mpfr_chebpoly_vec_t Q, const mpfr_chebpoly_vec_t R);


/* scalar product */

void mpfr_chebpoly_vec_scalar_prod(mpfr_chebpoly_t P, const mpfr_chebpoly_vec_t Q, const mpfr_chebpoly_vec_t R);


/* componentwise exponentiation */

void mpfr_chebpoly_vec_pow(mpfr_chebpoly_vec_t P, const mpfr_chebpoly_vec_t Q, unsigned long e);


/* componentwise derivative and antiderivative */

void mpfr_chebpoly_vec_derivative(mpfr_chebpoly_vec_t P, const mpfr_chebpoly_vec_t Q);

void mpfr_chebpoly_vec_antiderivative(mpfr_chebpoly_vec_t P, const mpfr_chebpoly_vec_t Q);

void mpfr_chebpoly_vec_antiderivative0(mpfr_chebpoly_vec_t P, const mpfr_chebpoly_vec_t Q);

void mpfr_chebpoly_vec_antiderivative_1(mpfr_chebpoly_vec_t P, const mpfr_chebpoly_vec_t Q);

void mpfr_chebpoly_vec_antiderivative1(mpfr_chebpoly_vec_t P, const mpfr_chebpoly_vec_t Q);





 
#endif
