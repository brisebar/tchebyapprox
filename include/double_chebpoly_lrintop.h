
/*
This file is part of the TchebyApprox library.

The type [double_chebpoly_lrintop_t] represents a (scalar)
linear Volterra integral operator of the second kind
with a bivariate polynomial kernel given as a low rank decomposition with [double_chebpoly_t], that is
K(t,s) = sum_{0 <= i < length} lpoly[t] * rpoly[s]
*/


#ifndef DOUBLE_CHEBPOLY_LRINTOP_H
#define DOUBLE_CHEBPOLY_LRINTOP_H


#include <stdio.h>
#include <stdlib.h>

#include <gmp.h>
#include <mpfr.h>
#include <mpfi.h>
#include <mpfi_io.h>

#include "double_operations.h"

#include "double_chebpoly.h"
#include "mpfr_chebpoly.h"
#include "mpfi_chebpoly.h"
#include "chebmodel.h"

#include "double_chebpoly_lode.h"
#include "double_chebpoly_intop.h"
#include "double_bandmatrix.h"


typedef struct {
  long length;
  double_chebpoly_ptr lpoly;
  double_chebpoly_ptr rpoly;
} double_chebpoly_lrintop_struct;

typedef double_chebpoly_lrintop_struct double_chebpoly_lrintop_t[1];

typedef double_chebpoly_lrintop_struct * double_chebpoly_intop_lrptr;

typedef const double_chebpoly_lrintop_struct * double_chebpoly_lrintop_srcptr;




void double_chebpoly_lrintop_init(double_chebpoly_lrintop_t K);

void double_chebpoly_lrintop_clear(double_chebpoly_lrintop_t K);

void double_chebpoly_lrintop_set(double_chebpoly_lrintop_t L, const double_chebpoly_lrintop_t K);

void double_chebpoly_lrintop_swap(double_chebpoly_lrintop_t L, double_chebpoly_lrintop_t K);

void double_chebpoly_lrintop_set_length(double_chebpoly_lrintop_t K, long length);


// evaluate on a polynomial

void double_chebpoly_lrintop_evaluate_d(double_chebpoly_t P, const double_chebpoly_lrintop_t K, const double_chebpoly_t Q);

void double_chebpoly_lrintop_evaluate_fr(mpfr_chebpoly_t P, const double_chebpoly_lrintop_t K, const mpfr_chebpoly_t Q);

void double_chebpoly_lrintop_evaluate_fi(mpfi_chebpoly_t P, const double_chebpoly_lrintop_t K, const mpfi_chebpoly_t Q);

void double_chebpoly_lrintop_evaluate_cm(chebmodel_t P, const double_chebpoly_lrintop_t K, const chebmodel_t Q);


// bounds

// crude bound on the sum
// A REVOIR : facteur 2 ?
void double_chebpoly_lrintop_lr1norm(double_t b, const double_chebpoly_lrintop_t K);

// convert to 2-dimensional polynomial in Chebyshev basis and use 1 norm
void double_chebpoly_lrintop_1norm(double_t b, const double_chebpoly_lrintop_t K);


// operations

void _double_chebpoly_lrintop_neg(double_chebpoly_ptr Llpoly, double_chebpoly_ptr Lrpoly, double_chebpoly_srcptr Klpoly, double_chebpoly_srcptr Krpoly, long r);

// set L := -K
void double_chebpoly_lrintop_neg(double_chebpoly_lrintop_t L, const double_chebpoly_lrintop_t K);

void _double_chebpoly_lrintop_add(double_chebpoly_ptr Mlpoly, double_chebpoly_ptr Mrpoly, double_chebpoly_srcptr Klpoly, double_chebpoly_srcptr Krpoly, long r1, double_chebpoly_srcptr Llpoly, double_chebpoly_srcptr Lrpoly, long r2);

// set M := K + L
void double_chebpoly_lrintop_add(double_chebpoly_lrintop_t M, const double_chebpoly_lrintop_t K, const double_chebpoly_lrintop_t L);

void _double_chebpoly_lrintop_sub(double_chebpoly_ptr Mlpoly, double_chebpoly_ptr Mrpoly, double_chebpoly_srcptr Klpoly, double_chebpoly_srcptr Krpoly, long r1, double_chebpoly_srcptr Llpoly, double_chebpoly_srcptr Lrpoly, long r2);

// set M := K - L
void double_chebpoly_lrintop_sub(double_chebpoly_lrintop_t M, const double_chebpoly_lrintop_t K, const double_chebpoly_lrintop_t L);

void _double_chebpoly_lrintop_scalar_mul_si(double_chebpoly_ptr Llpoly, double_chebpoly_ptr Lrpoly, double_chebpoly_srcptr Klpoly, double_chebpoly_srcptr Krpoly, long r, long c);

// set L := c * K
void double_chebpoly_lrintop_scalar_mul_si(double_chebpoly_lrintop_t L, const double_chebpoly_lrintop_t K, long c);

void _double_chebpoly_lrintop_scalar_mul_z(double_chebpoly_ptr Llpoly, double_chebpoly_ptr Lrpoly, double_chebpoly_srcptr Klpoly, double_chebpoly_srcptr Krpoly, long r, const mpz_t c);

// set L := c * K
void double_chebpoly_lrintop_scalar_mul_z(double_chebpoly_lrintop_t L, const double_chebpoly_lrintop_t K, const mpz_t c);

void _double_chebpoly_lrintop_scalar_mul_q(double_chebpoly_ptr Llpoly, double_chebpoly_ptr Lrpoly, double_chebpoly_srcptr Klpoly, double_chebpoly_srcptr Krpoly, long r, const mpq_t c);

// set L := c * K
void double_chebpoly_lrintop_scalar_mul_q(double_chebpoly_lrintop_t L, const double_chebpoly_lrintop_t K, const mpq_t c);

void _double_chebpoly_lrintop_scalar_mul_d(double_chebpoly_ptr Llpoly, double_chebpoly_ptr Lrpoly, double_chebpoly_srcptr Klpoly, double_chebpoly_srcptr Krpoly, long r, const double_t c);

// set L := c * K
void double_chebpoly_lrintop_scalar_mul_d(double_chebpoly_lrintop_t L, const double_chebpoly_lrintop_t K, const double_t c);

void _double_chebpoly_lrintop_scalar_mul_fr(double_chebpoly_ptr Llpoly, double_chebpoly_ptr Lrpoly, double_chebpoly_srcptr Klpoly, double_chebpoly_srcptr Krpoly, long r, const mpfr_t c);

// set L := c * K
void double_chebpoly_lrintop_scalar_mul_fr(double_chebpoly_lrintop_t L, const double_chebpoly_lrintop_t K, const mpfr_t c);

void _double_chebpoly_lrintop_scalar_mul_2si(double_chebpoly_ptr Llpoly, double_chebpoly_ptr Lrpoly, double_chebpoly_srcptr Klpoly, double_chebpoly_srcptr Krpoly, long r, long k);

// set L := 2^k * K
void double_chebpoly_lrintop_scalar_mul_2si(double_chebpoly_lrintop_t L, const double_chebpoly_lrintop_t K, long k);

void _double_chebpoly_lrintop_scalar_div_si(double_chebpoly_ptr Llpoly, double_chebpoly_ptr Lrpoly, double_chebpoly_srcptr Klpoly, double_chebpoly_srcptr Krpoly, long r, long c);

// set L := K / c
void double_chebpoly_lrintop_scalar_div_si(double_chebpoly_lrintop_t L, const double_chebpoly_lrintop_t K, long c);

void _double_chebpoly_lrintop_scalar_div_z(double_chebpoly_ptr Llpoly, double_chebpoly_ptr Lrpoly, double_chebpoly_srcptr Klpoly, double_chebpoly_srcptr Krpoly, long r, const mpz_t c);

// set L := K / c
void double_chebpoly_lrintop_scalar_div_z(double_chebpoly_lrintop_t L, const double_chebpoly_lrintop_t K, const mpz_t c);

void _double_chebpoly_lrintop_scalar_div_q(double_chebpoly_ptr Llpoly, double_chebpoly_ptr Lrpoly, double_chebpoly_srcptr Klpoly, double_chebpoly_srcptr Krpoly, long r, const mpq_t c);

// set L := K / c
void double_chebpoly_lrintop_scalar_div_q(double_chebpoly_lrintop_t L, const double_chebpoly_lrintop_t K, const mpq_t c);

void _double_chebpoly_lrintop_scalar_div_d(double_chebpoly_ptr Llpoly, double_chebpoly_ptr Lrpoly, double_chebpoly_srcptr Klpoly, double_chebpoly_srcptr Krpoly, long r, const double_t c);

// set L := K / c
void double_chebpoly_lrintop_scalar_div_d(double_chebpoly_lrintop_t L, const double_chebpoly_lrintop_t K, const double_t c);

void _double_chebpoly_lrintop_scalar_div_fr(double_chebpoly_ptr Llpoly, double_chebpoly_ptr Lrpoly, double_chebpoly_srcptr Klpoly, double_chebpoly_srcptr Krpoly, long r, const mpfr_t c);

// set L := K / c
void double_chebpoly_lrintop_scalar_div_fr(double_chebpoly_lrintop_t L, const double_chebpoly_lrintop_t K, const mpfr_t c);

void _double_chebpoly_lrintop_scalar_div_2si(double_chebpoly_ptr Llpoly, double_chebpoly_ptr Lrpoly, double_chebpoly_srcptr Klpoly, double_chebpoly_srcptr Krpoly, long r, long k);

// set L := 2^-k * K
void double_chebpoly_lrintop_scalar_div_2si(double_chebpoly_lrintop_t L, const double_chebpoly_lrintop_t K, long k);

void _double_chebpoly_lrintop_comp(double_chebpoly_ptr Mlpoly, double_chebpoly_ptr Mrpoly, double_chebpoly_srcptr Klpoly, double_chebpoly_srcptr Krpoly, long r1, double_chebpoly_srcptr Llpoly, double_chebpoly_srcptr Lrpoly, long r2);

// set M := K ∘ L
void double_chebpoly_lrintop_comp(double_chebpoly_lrintop_t M, const double_chebpoly_lrintop_t K, const double_chebpoly_lrintop_t L);


/* convert from double_chebpoly_intop_t */
void double_chebpoly_lrintop_set_intop(double_chebpoly_lrintop_t M, const double_chebpoly_intop_t K);

/* convert to double_chebpoly_intop_t */
void double_chebpoly_lrintop_get_intop(double_chebpoly_intop_t M, const double_chebpoly_lrintop_t K);


/* print */

void double_chebpoly_lrintop_print(const double_chebpoly_lrintop_t K, const char * Cheb_var);


/* convert LODE to lrintop */

// to integral equation on last derivative
// from x-D form: y^(r-1) + a[r-1] * y^(r-1) + ... + a[1] * y' + a[0] * y = h
// with initial conditions y^(i)(-1) = v[i] (0 <= i < r)
// to: f + ∫_-1^t K(t,s) * f(s) ds = g

// compute polynomial kernel K(t,s)
void double_chebpoly_lrintop_set_lode_xD(double_chebpoly_lrintop_t K, const double_chebpoly_srcptr a, long r);

// compute initial condition contribution to right hand side
void double_chebpoly_lrintop_set_lode_xD_initvals(double_chebpoly_t g, double_chebpoly_srcptr a, long r, double_srcptr v);

// compute right hand side g
void double_chebpoly_lrintop_set_lode_xD_rhs(double_chebpoly_t g, double_chebpoly_srcptr a, long r, const double_chebpoly_t h, double_srcptr v);


// to integral equation on the same function
// from D-x form: (-D)^(r)y + (-D)^(r-1)(b[r-1] * y) + ... + (-D)(b[1] * y) + b[0] * y = h
// with initial conditions y^[i](-1) = w[i] (0 <= i < r)
// where y^[i] = (-D)^(i)y + (-D)^(i-1)(b[r-1] * y) + ... + b[r-i] * y
// to: y + ∫_-1^t K(t,s) * y(s) ds = g

// compute polynomial kernel K(t,s)
void double_chebpoly_lrintop_set_lode_Dx(double_chebpoly_lrintop_t K, const double_chebpoly_srcptr b, long r);

// compute initial condition contribution to right hand side
void double_chebpoly_lrintop_set_lode_Dx_initvals(double_chebpoly_t g, long r, double_srcptr w);

// compute right hand side g
void double_chebpoly_lrintop_set_lode_Dx_rhs(double_chebpoly_t g, long r, const double_chebpoly_t h, double_srcptr w);


/* numerical solution */

// get bandmatrix
void double_chebpoly_lrintop_get_bandmatrix(double_bandmatrix_t M, const double_chebpoly_lrintop_t K, long N);

// solve F s.t. F + K(F) = G by truncating K at order N
void double_chebpoly_lrintop_approxsolve(double_chebpoly_t F, const double_chebpoly_lrintop_t K, const double_chebpoly_t G, long N);



#endif
