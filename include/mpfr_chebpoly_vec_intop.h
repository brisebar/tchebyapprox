
/*
This file is part of the TchebyApprox library.

The type [mpfr_chebpoly_intop_t] represents a vectorial 
linear Volterra integral operator of the second kind
of order [order] and dimension [dim]
derived from a [mpfr_chebpoly_vec_lode_t].
The new unknown of this intagral equation is the last derivative
of the unknown in the differential equation.

The vectorial operator is represented by blocks,
where each intop[i][j] (0 <= i,j <= [dim]-1)
is a [mpfr_chebpoly_intop_t].

Provided solving procedures for the integral equation
use Olver and Townsend's algorithm on almost-banded matrices.
*/


#ifndef MPFR_CHEBPOLY_VEC_INTOP_H
#define MPFR_CHEBPOLY_VEC_INTOP_H


#include <stdio.h>
#include <stdlib.h>

#include <gmp.h>
#include <mpfr.h>
#include <mpfi.h>
#include <mpfi_io.h>

#include "double_operations.h"

#include "mpfr_bandvec.h"
#include "mpfi_bandvec.h"
#include "mpfr_bandmatrix.h"
#include "double_chebpoly_vec.h"
#include "mpfr_chebpoly_vec.h"
#include "mpfi_chebpoly_vec.h"
#include "mpfr_chebpoly_vec_lode.h"
#include "mpfr_chebpoly_intop.h"

#include "double_chebpoly_vec_intop.h"

typedef struct {
  long dim;
  mpfr_chebpoly_intop_struct ** intop;
} mpfr_chebpoly_vec_intop_struct;

typedef mpfr_chebpoly_vec_intop_struct mpfr_chebpoly_vec_intop_t[1];

typedef mpfr_chebpoly_vec_intop_struct * mpfr_chebpoly_vec_intop_ptr;

typedef const mpfr_chebpoly_vec_intop_struct * mpfr_chebpoly_vec_intop_srcptr;




void mpfr_chebpoly_vec_intop_init(mpfr_chebpoly_vec_intop_t K);

void mpfr_chebpoly_vec_intop_clear(mpfr_chebpoly_vec_intop_t K);

void mpfr_chebpoly_vec_intop_set(mpfr_chebpoly_vec_intop_t K, const mpfr_chebpoly_vec_intop_t N);

//void mpfr_chebpoly_vec_intop_set_double_chebpoly_vec_intop(mpfr_chebpoly_vec_intop_t K, const double_chebpoly_vec_intop_t N);

//void mpfr_chebpoly_vec_intop_get_double_chebpoly_vec_intop(double_chebpoly_vec_intop_t K, const mpfr_chebpoly_vec_intop_t N);

void mpfr_chebpoly_vec_intop_swap(mpfr_chebpoly_vec_intop_t K, mpfr_chebpoly_vec_intop_t N);

void mpfr_chebpoly_vec_intop_set_dim(mpfr_chebpoly_vec_intop_t K, long dim);

long mpfr_chebpoly_vec_intop_Hwidth(const mpfr_chebpoly_vec_intop_t K);

long mpfr_chebpoly_vec_intop_Dwidth(const mpfr_chebpoly_vec_intop_t K);


void mpfr_chebpoly_vec_intop_set_lode(mpfr_chebpoly_vec_intop_t K, const mpfr_chebpoly_vec_lode_t L);

void mpfr_chebpoly_vec_intop_initvals(mpfr_chebpoly_vec_t G, const mpfr_chebpoly_vec_lode_t L, mpfr_ptr_ptr I);

void mpfr_chebpoly_vec_intop_rhs(mpfr_chebpoly_vec_t G, const mpfr_chebpoly_vec_lode_t L, const mpfr_chebpoly_vec_t g, mpfr_ptr_ptr I);

// evaluate on a polynomial ; use integration from -1

void mpfr_chebpoly_vec_intop_evaluate_fr(mpfr_chebpoly_vec_t P, const mpfr_chebpoly_vec_intop_t K, const mpfr_chebpoly_vec_t Q);

void mpfr_chebpoly_vec_intop_evaluate_fi(mpfi_chebpoly_vec_t P, const mpfr_chebpoly_vec_intop_t K, const mpfi_chebpoly_vec_t Q);

//void mpfr_chebpoly_intop_evaluate_Ti(mpfr_bandvec_t V, const mpfr_chebpoly_vec_intop_t K, long i);

//void mpfr_chebpoly_intop_evaluate_Ti_fi(mpfi_bandvec_t V, const mpfr_chebpoly_intop_t K, long i);


// get the bandmatrix_t associated to the N-th truncation of K

void mpfr_chebpoly_vec_intop_get_bandmatrix(mpfr_bandmatrix_t M, const mpfr_chebpoly_vec_intop_t K, long N);


/* approximate solutions */

void mpfr_chebpoly_vec_intop_approxsolve(mpfr_chebpoly_vec_t F, const mpfr_chebpoly_vec_intop_t K, const mpfr_chebpoly_vec_t G, long N);

void mpfr_chebpoly_vec_lode_intop_approxsolve(mpfr_chebpoly_vec_ptr Sols, const mpfr_chebpoly_vec_lode_t L, const mpfr_chebpoly_vec_t g, mpfr_ptr_ptr I, long N);


void mpfr_chebpoly_vec_intop_approxsolve_check(mpfr_ptr bounds, mpfr_chebpoly_vec_srcptr Sols, const mpfr_chebpoly_vec_lode_t L, const mpfr_chebpoly_vec_t g, mpfr_ptr_ptr I);

/* print */

//void mpfr_chebpoly_intop_print(const mpfr_chebpoly_intop_t K, const char * Cheb_var, size_t digits);


#endif
