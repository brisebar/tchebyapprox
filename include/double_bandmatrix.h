
/*
This file is part of the TchebyApprox library.

The type [double_bandmatrix_t] implements ([Hwidth],[Dwidth])
almost-banded square matrices of [double] of order [dim].
Non-zero coefficients are located:
  - in rows between indicies from 0 to Hwidth-1 (initial coefficients):
    H[i][0], ... , H[i][dim-1]  i = 0 ... Hwidth-1
  - at each row of index [i], between indicies from [max(i-Dwidth,0)] to [min(ind+Dwidth,dim-1)] (diagonal coefficients):
    D[i][0], ..., D[i][Dwidth], ... , D[i][2*Dwidth]   i = 0 ... dim-1
Normal form of the matrix: the entries in the initial band overlapped
by entries from the diagonal band are ignored (considered to be zero).
[double_bandmatrix_normalise]: convert to normal form
by adding the overlapped initial entries to the corresponding
diagonal entries and set them to zero.

The type [double_bandmatrix_QRdecomp_t] implements QR-decomposition
of an almost-banded matrix as used in Olver and Townsend's algorithm.
Implementation details are not necessary to use this decomposition
in linear algebra procedures provided by the library.
*/


#ifndef DOUBLE_BANDMATRIX_H
#define DOUBLE_BANDMATRIX_H


#include <stdio.h>
#include <stdlib.h>

#include <gmp.h>
#include <mpfr.h>
#include <mpfi.h>
#include <mpfi_io.h>

#include "double_operations.h"

#include "double_vec.h"
#include "mpfr_vec.h"
#include "mpfi_vec.h"
#include "double_bandvec.h"
#include "mpfr_bandvec.h"
#include "mpfi_bandvec.h"


typedef struct {
  long dim;
  long Hwidth;
  long Dwidth;
  double_ptr_ptr H;
  double_ptr_ptr D;
} double_bandmatrix_struct;

typedef double_bandmatrix_struct double_bandmatrix_t[1];

/* basic manipulations */

void double_bandmatrix_init(double_bandmatrix_t M);

void double_bandmatrix_clear(double_bandmatrix_t M);

void double_bandmatrix_set(double_bandmatrix_t M, const double_bandmatrix_t N);

void double_bandmatrix_swap(double_bandmatrix_t M, double_bandmatrix_t N);

void double_bandmatrix_set_params(double_bandmatrix_t M, long dim, long Hwidth, long Dwidth);

void double_bandmatrix_zero(double_bandmatrix_t M);

void double_bandmatrix_normalise(double_bandmatrix_t M);

void double_bandmatrix_get_column(double_bandvec_t V, const double_bandmatrix_t M, long i);

void double_bandmatrix_set_column(double_bandmatrix_t M, const double_bandvec_t V);


/* evaluation on vectors */

// on dense vectors

void _double_bandmatrix_evaluate_d(double_ptr V, const double_bandmatrix_t M, double_srcptr W);

void double_bandmatrix_evaluate_d(double_vec_t V, const double_bandmatrix_t M, const double_vec_t W);

void _double_bandmatrix_evaluate_fr(mpfr_ptr V, const double_bandmatrix_t M, mpfr_srcptr W);

void double_bandmatrix_evaluate_fr(mpfr_vec_t V, const double_bandmatrix_t M, const mpfr_vec_t W);

void _double_bandmatrix_evaluate_fi(mpfi_ptr V, const double_bandmatrix_t M, mpfi_srcptr W);

void double_bandmatrix_evaluate_fi(mpfi_vec_t V, const double_bandmatrix_t M, const mpfi_vec_t W);

// on sparse banded vectors

// all double_t must be distinct
void _double_bandmatrix_evaluate_band_d(double_ptr VH, double_ptr VD, long VHwidth, long VDwidth, const double_bandmatrix_t M, double_srcptr WH, double_srcptr WD, long WHwidth, long WDwidth, long ind);

void double_bandmatrix_evaluate_band_d(double_bandvec_t V, const double_bandmatrix_t M, const double_bandvec_t W);

// all mpfr_t must be distinct !
void _double_bandmatrix_evaluate_band_fr(mpfr_ptr VH, mpfr_ptr VD, long VHwidth, long VDwidth, const double_bandmatrix_t M, mpfr_srcptr WH, mpfr_srcptr WD, long WHwidth, long WDwidth, long ind);

void double_bandmatrix_evaluate_band_fr(mpfr_bandvec_t V, const double_bandmatrix_t M, const mpfr_bandvec_t W);

// all mpfi_t must be distinct !
void _double_bandmatrix_evaluate_band_fi(mpfi_ptr VH, mpfi_ptr VD, long VHwidth, long VDwidth, const double_bandmatrix_t M, mpfi_srcptr WH, mpfi_srcptr WD, long WHwidth, long WDwidth, long ind);

void double_bandmatrix_evaluate_band_fi(mpfi_bandvec_t V, const double_bandmatrix_t M, const mpfi_bandvec_t W);



/* norm 1 computations */


void double_bandmatrix_1norm(double_t norm, const double_bandmatrix_t M);

void double_bandmatrix_1norm_ubound(double_t norm, const double_bandmatrix_t M);

void double_bandmatrix_1norm_lbound(double_t norm, const double_bandmatrix_t M);

void double_bandmatrix_1norm_fi(mpfi_t norm, const double_bandmatrix_t M);



/* arithmetic operations */


void double_bandmatrix_add(double_bandmatrix_t M, const double_bandmatrix_t N, const double_bandmatrix_t P);

void double_bandmatrix_sub(double_bandmatrix_t M, const double_bandmatrix_t N, const double_bandmatrix_t P);

void double_bandmatrix_neg(double_bandmatrix_t M, const double_bandmatrix_t N);

void double_bandmatrix_mul(double_bandmatrix_t M, const double_bandmatrix_t N, const double_bandmatrix_t P);


/* split and marge matrices of matrices */


// merge n*n a-b matrices of same size N  into a 4*N a-b matrix
void double_bandmatrix_merge(double_bandmatrix_t A, double_bandmatrix_struct ** B, long n);

// split an n*N order a-b matrix into n*n a-b matrices
void double_bandmatrix_split(double_bandmatrix_struct ** A, const double_bandmatrix_t B, long n);







/* QR decomposition of a banded matrix to solve a linear system */


typedef struct {
  long dim;
  long Hwidth;
  long Dwidth;
  double_ptr_ptr H;
  double_ptr_ptr D;
  double_ptr_ptr B;
  long * XCH;
  double_ptr_ptr_ptr GR;
} double_bandmatrix_QRdecomp_struct;

typedef double_bandmatrix_QRdecomp_struct double_bandmatrix_QRdecomp_t[1];


/* basic manipulations */


void double_bandmatrix_QRdecomp_init(double_bandmatrix_QRdecomp_t M);

void double_bandmatrix_QRdecomp_clear(double_bandmatrix_QRdecomp_t M);

void double_bandmatrix_QRdecomp_set(double_bandmatrix_QRdecomp_t M, const double_bandmatrix_QRdecomp_t N);

void double_bandmatrix_QRdecomp_swap(double_bandmatrix_QRdecomp_t M, double_bandmatrix_QRdecomp_t N);

void double_bandmatrix_QRdecomp_set_params(double_bandmatrix_QRdecomp_t M, long dim, long Hwidth, long Dwidth);

void double_bandmatrix_QRdecomp_zero(double_bandmatrix_QRdecomp_t M);


/* get the QR decomposition of a banded matrix */

int double_bandmatrix_get_QRdecomp(double_bandmatrix_QRdecomp_t N, const double_bandmatrix_t M);


/* solve the linear system using the QR decomposition */

void _double_bandmatrix_QRdecomp_solve_d(double_ptr V, const double_bandmatrix_QRdecomp_t N, double_srcptr W);

void double_bandmatrix_QRdecomp_solve_d(double_vec_t C, const double_bandmatrix_QRdecomp_t N, const double_vec_t W);

void _double_bandmatrix_QRdecomp_solve_fr(mpfr_ptr V, const double_bandmatrix_QRdecomp_t N, mpfr_srcptr W);

void double_bandmatrix_QRdecomp_solve_fr(mpfr_vec_t V, const double_bandmatrix_QRdecomp_t N, const mpfr_vec_t W);

void _double_bandmatrix_QRdecomp_solve_fi(mpfi_ptr V, const double_bandmatrix_QRdecomp_t N, mpfi_srcptr W);

void double_bandmatrix_QRdecomp_solve_fi(mpfi_vec_t V, const double_bandmatrix_QRdecomp_t N, const mpfi_vec_t W);


/* norm 1 computations for the inverse */

// 1-norm of the inverse, in quadratic time

void double_bandmatrix_QRdecomp_inv1norm(double_t norm, const double_bandmatrix_QRdecomp_t N);

void double_bandmatrix_QRdecomp_inv1norm_ubound(double_t norm, const double_bandmatrix_QRdecomp_t N);

void double_bandmatrix_QRdecomp_inv1norm_lbound(double_t norm, const double_bandmatrix_QRdecomp_t N);

void double_bandmatrix_QRdecomp_inv1norm_fi(mpfi_t norm, const double_bandmatrix_QRdecomp_t N);


// approximate the inverse of a banded matrix (given under its QR decomposition) with a banded matrix
void double_bandmatrix_QRdecomp_approx_band_inverse(double_bandmatrix_t P, const double_bandmatrix_QRdecomp_t N, long Hwidth, long Dwidth);



#endif
