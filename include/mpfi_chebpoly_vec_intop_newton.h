
/*
This file is part of the TchebyApprox library.

Here are the routines to rigorously solve a vectorial integral equation
described by a [mpfi_chebpoly_vec_intop_t], using a Newton-like method.

This generalizes the scalar case described in [mpfi_chebpoly_intop_newton.h]
*/


#ifndef MPFI_CHEBPOLY_VEC_INTOP_NEWTON_H
#define MPFI_CHEBPOLY_VEC_INTOP_NEWTON_H

#include <stdio.h>
#include <stdlib.h>

#include <gmp.h>
#include <mpfr.h>
#include <mpfi.h>
#include <mpfi_io.h>

#include "double_operations.h"

#include "mpfi_chebpoly_vec.h"
#include "double_bandmatrix.h"
#include "mpfr_bandmatrix.h"
#include "mpfi_bandmatrix.h"
#include "mpfi_chebpoly_vec_intop.h"
#include "mpfi_chebpoly_intop_newton.h"

/* bounds for the Newton Operator */

// bound1: i from N-s+1 to N
//void mpfi_chebpoly_vec_intop_newton_bounds_bound1_d(double_t bound1, const mpfi_chebpoly_vec_intop_t K, mpfi_bandmatrix_struct ** M_K);
void mpfi_chebpoly_vec_intop_newton_bounds_bound1_fr(mpfr_ptr_ptr bound1, const mpfi_chebpoly_vec_intop_t K, mpfi_bandmatrix_struct ** M_K);
#define mpfi_chebpoly_vec_intop_newton_bounds_bound1 mpfi_chebpoly_intop_newton_bounds_bound1_fr

//#define mpfi_chebpoly_vec_intop_newton_bounds_bound1_aux1_d mpfi_chebpoly_intop_newton_bounds_bound1_d
#define mpfi_chebpoly_vec_intop_newton_bounds_bound1_aux1_fr mpfi_chebpoly_intop_newton_bounds_bound1_fr


// bound2: i from N+1 to N+s
//void mpfi_chebpoly_vec_intop_newton_bounds_bound2_d(double_ptr_ptr bound2, const mpfi_chebpoly_vec_intop_t K, mpfi_bandmatrix_struct ** M_K, double_bandmatrix_struct ** M_K_inv);
void mpfi_chebpoly_vec_intop_newton_bounds_bound2_fr(mpfr_ptr_ptr bound2, const mpfi_chebpoly_vec_intop_t K, mpfi_bandmatrix_struct ** M_K, mpfr_bandmatrix_struct ** M_K_inv);
#define mpfi_chebpoly_vec_intop_newton_bounds_bound2 mpfi_chebpoly_vec_intop_newton_bounds_bound2_fr

//void mpfi_chebpoly_vec_intop_newton_bounds_bound2_aux0_d(double_t bound2, const mpfi_chebpoly_intop_t K, const mpfi_bandmatrix_t M_K, const double_bandmatrix_t M_K_inv);
//#define mpfi_chebpoly_vec_intop_newton_bounds_bound2_aux1_d mpfi_chebpoly_intop_newton_bounds_bound2_d
void mpfi_chebpoly_vec_intop_newton_bounds_bound2_aux0_fr(mpfr_t bound2, const mpfi_chebpoly_intop_t K, const mpfi_bandmatrix_t M_K, const mpfr_bandmatrix_t M_K_inv);
#define mpfi_chebpoly_vec_intop_newton_bounds_bound2_aux1_fr mpfi_chebpoly_intop_newton_bounds_bound2_fr


// bound3 (diagonal coefficients) and bound4 (N_F^-1 on initial coefficient): i > N+s
//void mpfi_chebpoly_vec_intop_newton_bounds_bound34_d(double_ptr_ptr bound3, double_ptr_ptr bound4, const mpfi_chebpoly_vec_intop_t K, mpfi_bandmatrix_struct ** M_K, double_bandmatrix_struct ** M_K_inv);
void mpfi_chebpoly_vec_intop_newton_bounds_bound34_fr(mpfr_ptr_ptr bound3, mpfr_ptr_ptr bound4, const mpfi_chebpoly_vec_intop_t K, mpfi_bandmatrix_struct ** M_K, mpfr_bandmatrix_struct ** M_K_inv);
#define mpfi_chebpoly_vec_intop_newton_bounds_bound34 mpfi_chebpoly_vec_intop_newton_bounds_bound34_fr

//void mpfi_chebpoly_vec_intop_newton_bounds_bound4_aux0_d(double_t bound4, const mpfi_chebpoly_intop_t K, const mpfi_bandmatrix_t M_K, const double_bandmatrix_t M_K_inv);
//#define mpfi_chebpoly_vec_intop_newton_bounds_bound34_aux1_d mpfi_chebpoly_intop_newton_bounds_bound34_d
void mpfi_chebpoly_vec_intop_newton_bounds_bound4_aux0_fr(mpfr_t bound4, const mpfi_chebpoly_intop_t K, const mpfi_bandmatrix_t M_K, const mpfr_bandmatrix_t M_K_inv);
#define mpfi_chebpoly_vec_intop_newton_bounds_bound34_aux1_fr mpfi_chebpoly_intop_newton_bounds_bound34_fr


// bound5: numerical error = ||Id - M_K_inv*M_K||
//void mpfi_chebpoly_vec_intop_newton_bounds_bound5_d(double_ptr_ptr bound5, const mpfi_chebpoly_vec_intop_t K, mpfi_bandmatrix_struct ** M_K, double_bandmatrix_struct ** M_K_inv);
void mpfi_chebpoly_vec_intop_newton_bounds_bound5_fr(mpfr_ptr_ptr bound5, const mpfi_chebpoly_vec_intop_t K, mpfi_bandmatrix_struct ** M_K, mpfr_bandmatrix_struct ** M_K_inv);
#define mpfi_chebpoly_vec_intop_newton_bounds_bound5 mpfi_chebpoly_vec_intop_newton_bounds_bound5_fr


// compute bound_i for i = 1,...,5
// M_K[i][j] is the matrix associated to I-K[i][j] if i=j, -K[i][j] otherwise, whereas M_Kglob is the corresponding merged matrix,
//void mpfi_chebpoly_vec_intop_newton_bounds_d(double_ptr_ptr bound1, double_ptr_ptr bound2, double_ptr_ptr bound3, double_ptr_ptr bound4, double_ptr_ptr bound5, const mpfi_chebpoly_vec_intop_t K, mpfi_bandmatrix_struct ** M_K, double_bandmatrix_struct ** M_K_inv);
void mpfi_chebpoly_vec_intop_newton_bounds_fr(mpfr_ptr_ptr bound1, mpfr_ptr_ptr bound2, mpfr_ptr_ptr bound3, mpfr_ptr_ptr bound4, mpfr_ptr_ptr bound5, const mpfi_chebpoly_vec_intop_t K, mpfi_bandmatrix_struct ** M_K, const mpfi_bandmatrix_t M_Kglob, mpfr_bandmatrix_struct ** M_K_inv, const mpfr_bandmatrix_t M_K_inv_glob);
#define mpfi_chebpoly_vec_intop_newton_bounds mpfi_chebpoly_vec_intop_newton_bounds_fr

// merge all bound_i to get the final array of bounds
// 1 = N too small  2 = imprecise numerical inverse
//int mpfi_chebpoly_vec_intop_newton_bound_d(double_ptr_ptr bound, const mpfi_chebpoly_vec_intop_t K, mpfi_bandmatrix_struct ** M_K, double_bandmatrix_struct ** M_K_inv);
int mpfi_chebpoly_vec_intop_newton_bound_fr(mpfr_ptr_ptr bound, const mpfi_chebpoly_vec_intop_t K, mpfi_bandmatrix_struct ** M_K, const mpfi_bandmatrix_t M_Kglob, mpfr_bandmatrix_struct ** M_K_inv, const mpfr_bandmatrix_t M_K_inv_glob);
#define mpfi_chebpoly_vec_intop_newton_bound mpfi_chebpoly_vec_intop_newton_bound_fr


// producing a contracting Newton op
// start from N = init_N and double N the paramaters of the approx inverse until the Newton Operator is contractant

//void mpfi_chebpoly_vec_intop_newton_contract_d(double_ptr_ptr bound, double_bandmatrix_struct ** M_K_inv, const mpfi_chebpoly_vec_intop_t K, long init_N);
void mpfi_chebpoly_vec_intop_newton_contract_fr(mpfr_ptr_ptr bound, mpfr_bandmatrix_struct ** M_K_inv, const mpfi_chebpoly_vec_intop_t K, long init_N);
#define mpfi_chebpoly_vec_intop_newton_contract mpfi_chebpoly_vec_intop_newton_contract_fr

//void mpfi_chebpoly_vec_intop_newton_validate_sol_aux_d(mpfr_ptr bound, const mpfi_chebpoly_vec_intop_t K, double_ptr_ptr T_norm, double_bandmatrix_struct ** M_K_inv, const mpfi_chebpoly_vec_t G, const mpfr_chebpoly_vec_t P);
void mpfi_chebpoly_vec_intop_newton_validate_sol_aux_fr(mpfr_ptr bound, const mpfi_chebpoly_vec_intop_t K, mpfr_ptr_ptr T_norm, mpfr_bandmatrix_struct ** M_K_inv, const mpfi_chebpoly_vec_t G, const mpfr_chebpoly_vec_t P);
#define mpfi_chebpoly_vec_intop_newton_validate_sol_aux mpfi_chebpoly_vec_intop_newton_validate_sol_d

//void mpfi_chebpoly_vec_intop_newton_validate_sol_d(mpfr_ptr bound, const mpfi_chebpoly_vec_intop_t K, const mpfi_chebpoly_vec_t G, const mpfr_chebpoly_vec_t P, long init_N);
void mpfi_chebpoly_vec_intop_newton_validate_sol_fr(mpfr_ptr bound, const mpfi_chebpoly_vec_intop_t K, const mpfi_chebpoly_vec_t G, const mpfr_chebpoly_vec_t P, long init_N);
#define mpfi_chebpoly_vec_intop_newton_validate_sol mpfi_chebpoly_vec_intop_newton_validate_sol_fr


//void mpfi_chebpoly_vec_lode_intop_newton_validate_sol_d(mpfr_ptr bound, const mpfi_chebpoly_vec_lode_t L, const mpfi_chebpoly_vec_t g, mpfi_ptr_ptr I, const mpfr_chebpoly_vec_t P);
void mpfi_chebpoly_vec_lode_intop_newton_validate_sol_fr(mpfr_ptr bound, const mpfi_chebpoly_vec_lode_t L, const mpfi_chebpoly_vec_t g, mpfi_ptr_ptr I, const mpfr_chebpoly_vec_t P);
#define mpfi_chebpoly_vec_lode_intop_newton_validate_sol mpfi_chebpoly_vec_lode_intop_newton_validate_sol_fr


#endif
