
/*
This file is part of the TchebyApprox library.

Here are the routines to rigorously solve an integral equation
described by a [mpfi_chebpoly_intop_t], using a Newton-like method.

[mpfi_chebpoly_intop_newton_bound]: bound a Newton-like operator
[mpfi_chebpoly_intop_newton_bound]: create and bound a Newton-like operator
[mpfi_chebpoly_intop_newton_validate_sol_aux]: validate a candidate approximation, having already a contracting Newton-like operator
[mpfi_chebpoly_intop_newton_validate]: validate a candidate approximation by creating a contracting Newton-like operator
*/


#ifndef MPFI_CHEBPOLY_INTOP_NEWTON_H
#define MPFI_CHEBPOLY_INTOP_NEWTON_H

#include <stdio.h>
#include <stdlib.h>

#include <gmp.h>
#include <mpfr.h>
#include <mpfi.h>
#include <mpfi_io.h>

#include "double_operations.h"

#include "mpfi_chebpoly.h"
#include "double_bandmatrix.h"
#include "mpfr_bandmatrix.h"
#include "mpfi_bandmatrix.h"
#include "mpfi_chebpoly_intop.h"


/* bounds for the Newton Operator */

// bound1: i from N-s+1 to N
void mpfi_chebpoly_intop_newton_bounds_bound1_d(double_t bound1, const mpfi_chebpoly_intop_t K, const mpfi_bandmatrix_t M_K);
void mpfi_chebpoly_intop_newton_bounds_bound1_fr(mpfr_t bound1, const mpfi_chebpoly_intop_t K, const mpfi_bandmatrix_t M_K);
#define mpfi_chebpoly_intop_newton_bounds_bound1 mpfi_chebpoly_intop_newton_bounds_bound1_d

// bound2: i from N+1 to N+s
void mpfi_chebpoly_intop_newton_bounds_bound2_d(double_t bound2, const mpfi_chebpoly_intop_t K, const mpfi_bandmatrix_t M_K, const double_bandmatrix_t M_K_inv);
void mpfi_chebpoly_intop_newton_bounds_bound2_fr(mpfr_t bound2, const mpfi_chebpoly_intop_t K, const mpfi_bandmatrix_t M_K, const mpfr_bandmatrix_t M_K_inv);
#define mpfi_chebpoly_intop_newton_bounds_bound2 mpfi_chebpoly_intop_newton_bounds_bound2_d

// bound3 (diagonal coefficients) and bound4 (N_F^-1 on initial coefficient): i > N+s
void mpfi_chebpoly_intop_newton_bounds_bound34_d(double_t bound3, double_t bound4, const mpfi_chebpoly_intop_t K, const mpfi_bandmatrix_t M_K, const double_bandmatrix_t M_K_inv);
void mpfi_chebpoly_intop_newton_bounds_bound34_fr(mpfr_t bound3, mpfr_t bound4, const mpfi_chebpoly_intop_t K, const mpfi_bandmatrix_t M_K, const mpfr_bandmatrix_t M_K_inv);
#define mpfi_chebpoly_intop_newton_bounds_bound34 mpfi_chebpoly_intop_newton_bounds_bound34_d

// bound5: numerical error = ||Id - M_K_inv*M_K||
void mpfi_chebpoly_intop_newton_bounds_bound5_d(double_t bound5, const mpfi_bandmatrix_t M_K, const double_bandmatrix_t M_K_inv);
void mpfi_chebpoly_intop_newton_bounds_bound5_fr(mpfr_t bound5, const mpfi_bandmatrix_t M_K, const mpfr_bandmatrix_t M_K_inv);
#define mpfi_chebpoly_intop_newton_bounds_bound5 mpfi_chebpoly_intop_newton_bounds_bound5_d

// compute bound_i for i = 1,...,5
void mpfi_chebpoly_intop_newton_bounds_d(double_t bound1, double_t bound2, double_t bound3, double_t bound4, double_t bound5, const mpfi_chebpoly_intop_t K, const mpfi_bandmatrix_t M_K, const double_bandmatrix_t M_K_inv);
void mpfi_chebpoly_intop_newton_bounds_fr(mpfr_t bound1, mpfr_t bound2, mpfr_t bound3, mpfr_t bound4, mpfr_t bound5, const mpfi_chebpoly_intop_t K, const mpfi_bandmatrix_t M_K, const mpfr_bandmatrix_t M_K_inv);
#define mpfi_chebpoly_intop_newton_bounds mpfi_chebpoly_intop_newton_bounds_d

// merge all bound_i to get the final bound
// 1 = N too small  2 = imprecise numerical inverse
int mpfi_chebpoly_intop_newton_bound_d(double_t bound, const mpfi_chebpoly_intop_t K, const mpfi_bandmatrix_t M_K, const double_bandmatrix_t M_K_inv);
int mpfi_chebpoly_intop_newton_bound_fr(mpfr_t bound, const mpfi_chebpoly_intop_t K, const mpfi_bandmatrix_t M_K, const mpfr_bandmatrix_t M_K_inv);
#define mpfi_chebpoly_intop_newton_bound mpfi_chebpoly_intop_newton_bound_d

// producing a contracting Newton op
// start from N = init_N and double N the paramaters of the approx inverse until the Newton Operator is contractant

void mpfi_chebpoly_intop_newton_contract_d(double_t bound, double_bandmatrix_t M_K_inv, const mpfi_chebpoly_intop_t K, long init_N);
void mpfi_chebpoly_intop_newton_contract_fr(mpfr_t bound, mpfr_bandmatrix_t M_K_inv, const mpfi_chebpoly_intop_t K, long init_N);
#define mpfi_chebpoly_intop_newton_contract mpfi_chebpoly_intop_newton_contract_d

void mpfi_chebpoly_intop_newton_validate_sol_aux_d(mpfr_t bound, const mpfi_chebpoly_intop_t K, const double_t T_norm, const double_bandmatrix_t M_K_inv, const mpfi_chebpoly_t G, const mpfr_chebpoly_t P);
void mpfi_chebpoly_intop_newton_validate_sol_aux_fr(mpfr_t bound, const mpfi_chebpoly_intop_t K, const mpfr_t T_norm, const mpfr_bandmatrix_t M_K_inv, const mpfi_chebpoly_t G, const mpfr_chebpoly_t P);
#define mpfi_chebpoly_intop_newton_validate_sol_aux mpfi_chebpoly_intop_newton_validate_sol_d

void mpfi_chebpoly_intop_newton_validate_sol_d(mpfr_t bound, const mpfi_chebpoly_intop_t K, const mpfi_chebpoly_t G, const mpfr_chebpoly_t P, long init_N);
void mpfi_chebpoly_intop_newton_validate_sol_fr(mpfr_t bound, const mpfi_chebpoly_intop_t K, const mpfi_chebpoly_t G, const mpfr_chebpoly_t P, long init_N);
#define mpfi_chebpoly_intop_newton_validate_sol mpfi_chebpoly_intop_newton_validate_sol_d


void mpfi_chebpoly_lode_intop_newton_validate_sol_d(mpfr_t bound, const mpfi_chebpoly_lode_t L, const mpfi_chebpoly_t g, mpfi_srcptr I, const mpfr_chebpoly_t P);
void mpfi_chebpoly_lode_intop_newton_validate_sol_fr(mpfr_t bound, const mpfi_chebpoly_lode_t L, const mpfi_chebpoly_t g, mpfi_srcptr I, const mpfr_chebpoly_t P);
#define mpfi_chebpoly_lode_intop_newton_validate_sol mpfi_chebpoly_lode_intop_newton_validate_sol_d


#endif
