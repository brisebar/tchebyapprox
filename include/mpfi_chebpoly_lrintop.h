
/*
This file is part of the TchebyApprox library.

The type [mpfi_chebpoly_lrintop_t] represents a (scalar)
linear Volterra integral operator of the second kind
with a bivariate polynomial kernel given as a low rank decomposition with [mpfi_chebpoly_t], that is
K(t,s) = sum_{0 <= i < length} lpoly[t] * rpoly[s]
*/


#ifndef MPFI_CHEBPOLY_LRINTOP_H
#define MPFI_CHEBPOLY_LRINTOP_H


#include <stdio.h>
#include <stdlib.h>

#include <gmp.h>
#include <mpfr.h>
#include <mpfi.h>
#include <mpfi_io.h>

#include "double_operations.h"

#include "double_chebpoly.h"
#include "mpfr_chebpoly.h"
#include "mpfi_chebpoly.h"
#include "chebmodel.h"

#include "mpfi_chebpoly_lode.h"
#include "mpfi_chebpoly_intop.h"
#include "mpfi_bandmatrix.h"

#include "double_chebpoly_lrintop.h"
#include "mpfr_chebpoly_lrintop.h"


typedef struct {
  long length;
  mpfi_chebpoly_ptr lpoly;
  mpfi_chebpoly_ptr rpoly;
} mpfi_chebpoly_lrintop_struct;

typedef mpfi_chebpoly_lrintop_struct mpfi_chebpoly_lrintop_t[1];

typedef mpfi_chebpoly_lrintop_struct * mpfi_chebpoly_intop_lrptr;

typedef const mpfi_chebpoly_lrintop_struct * mpfi_chebpoly_lrintop_srcptr;




void mpfi_chebpoly_lrintop_init(mpfi_chebpoly_lrintop_t K);

void mpfi_chebpoly_lrintop_clear(mpfi_chebpoly_lrintop_t K);

void mpfi_chebpoly_lrintop_set(mpfi_chebpoly_lrintop_t L, const mpfi_chebpoly_lrintop_t K);

void mpfi_chebpoly_lrintop_set_double_chebpoly_lrintop(mpfi_chebpoly_lrintop_t L, const double_chebpoly_lrintop_t K);

void mpfi_chebpoly_lrintop_get_double_chebpoly_lrintop(double_chebpoly_lrintop_t L, const mpfi_chebpoly_lrintop_t K);

void mpfi_chebpoly_lrintop_set_mpfr_chebpoly_lrintop(mpfi_chebpoly_lrintop_t L, const mpfr_chebpoly_lrintop_t K);

void mpfi_chebpoly_lrintop_get_mpfr_chebpoly_lrintop(mpfr_chebpoly_lrintop_t L, const mpfi_chebpoly_lrintop_t K);

void mpfi_chebpoly_lrintop_swap(mpfi_chebpoly_lrintop_t L, mpfi_chebpoly_lrintop_t K);

void mpfi_chebpoly_lrintop_set_length(mpfi_chebpoly_lrintop_t K, long length);


// evaluate on a polynomial

void mpfi_chebpoly_lrintop_evaluate_d(mpfi_chebpoly_t P, const mpfi_chebpoly_lrintop_t K, const double_chebpoly_t Q);

void mpfi_chebpoly_lrintop_evaluate_fr(mpfi_chebpoly_t P, const mpfi_chebpoly_lrintop_t K, const mpfr_chebpoly_t Q);

void mpfi_chebpoly_lrintop_evaluate_fi(mpfi_chebpoly_t P, const mpfi_chebpoly_lrintop_t K, const mpfi_chebpoly_t Q);

void mpfi_chebpoly_lrintop_evaluate_cm(chebmodel_t P, const mpfi_chebpoly_lrintop_t K, const chebmodel_t Q);


// bounds
// algorithms multiply bound by 2 to get operator norm over [-1,1]

// crude bound on the sum
void mpfi_chebpoly_lrintop_lr1norm(mpfr_t b, const mpfi_chebpoly_lrintop_t K);

// convert to 2-dimensional polynomial in Chebyshev basis and use 1 norm
void mpfi_chebpoly_lrintop_1norm(mpfr_t b, const mpfi_chebpoly_lrintop_t K);


// operations

void _mpfi_chebpoly_lrintop_neg(mpfi_chebpoly_ptr Llpoly, mpfi_chebpoly_ptr Lrpoly, mpfi_chebpoly_srcptr Klpoly, mpfi_chebpoly_srcptr Krpoly, long r);

// set L := -K
void mpfi_chebpoly_lrintop_neg(mpfi_chebpoly_lrintop_t L, const mpfi_chebpoly_lrintop_t K);

void _mpfi_chebpoly_lrintop_add(mpfi_chebpoly_ptr Mlpoly, mpfi_chebpoly_ptr Mrpoly, mpfi_chebpoly_srcptr Klpoly, mpfi_chebpoly_srcptr Krpoly, long r1, mpfi_chebpoly_srcptr Llpoly, mpfi_chebpoly_srcptr Lrpoly, long r2);

// set M := K + L
void mpfi_chebpoly_lrintop_add(mpfi_chebpoly_lrintop_t M, const mpfi_chebpoly_lrintop_t K, const mpfi_chebpoly_lrintop_t L);

void _mpfi_chebpoly_lrintop_sub(mpfi_chebpoly_ptr Mlpoly, mpfi_chebpoly_ptr Mrpoly, mpfi_chebpoly_srcptr Klpoly, mpfi_chebpoly_srcptr Krpoly, long r1, mpfi_chebpoly_srcptr Llpoly, mpfi_chebpoly_srcptr Lrpoly, long r2);

// set M := K - L
void mpfi_chebpoly_lrintop_sub(mpfi_chebpoly_lrintop_t M, const mpfi_chebpoly_lrintop_t K, const mpfi_chebpoly_lrintop_t L);

void _mpfi_chebpoly_lrintop_scalar_mul_si(mpfi_chebpoly_ptr Llpoly, mpfi_chebpoly_ptr Lrpoly, mpfi_chebpoly_srcptr Klpoly, mpfi_chebpoly_srcptr Krpoly, long r, long c);

// set L := c * K
void mpfi_chebpoly_lrintop_scalar_mul_si(mpfi_chebpoly_lrintop_t L, const mpfi_chebpoly_lrintop_t K, long c);

void _mpfi_chebpoly_lrintop_scalar_mul_z(mpfi_chebpoly_ptr Llpoly, mpfi_chebpoly_ptr Lrpoly, mpfi_chebpoly_srcptr Klpoly, mpfi_chebpoly_srcptr Krpoly, long r, const mpz_t c);

// set L := c * K
void mpfi_chebpoly_lrintop_scalar_mul_z(mpfi_chebpoly_lrintop_t L, const mpfi_chebpoly_lrintop_t K, const mpz_t c);

void _mpfi_chebpoly_lrintop_scalar_mul_q(mpfi_chebpoly_ptr Llpoly, mpfi_chebpoly_ptr Lrpoly, mpfi_chebpoly_srcptr Klpoly, mpfi_chebpoly_srcptr Krpoly, long r, const mpq_t c);

// set L := c * K
void mpfi_chebpoly_lrintop_scalar_mul_q(mpfi_chebpoly_lrintop_t L, const mpfi_chebpoly_lrintop_t K, const mpq_t c);

void _mpfi_chebpoly_lrintop_scalar_mul_d(mpfi_chebpoly_ptr Llpoly, mpfi_chebpoly_ptr Lrpoly, mpfi_chebpoly_srcptr Klpoly, mpfi_chebpoly_srcptr Krpoly, long r, const double_t c);

// set L := c * K
void mpfi_chebpoly_lrintop_scalar_mul_d(mpfi_chebpoly_lrintop_t L, const mpfi_chebpoly_lrintop_t K, const double_t c);

void _mpfi_chebpoly_lrintop_scalar_mul_fr(mpfi_chebpoly_ptr Llpoly, mpfi_chebpoly_ptr Lrpoly, mpfi_chebpoly_srcptr Klpoly, mpfi_chebpoly_srcptr Krpoly, long r, const mpfr_t c);

// set L := c * K
void mpfi_chebpoly_lrintop_scalar_mul_fr(mpfi_chebpoly_lrintop_t L, const mpfi_chebpoly_lrintop_t K, const mpfr_t c);

void _mpfi_chebpoly_lrintop_scalar_mul_2si(mpfi_chebpoly_ptr Llpoly, mpfi_chebpoly_ptr Lrpoly, mpfi_chebpoly_srcptr Klpoly, mpfi_chebpoly_srcptr Krpoly, long r, long k);

// set L := 2^k * K
void mpfi_chebpoly_lrintop_scalar_mul_2si(mpfi_chebpoly_lrintop_t L, const mpfi_chebpoly_lrintop_t K, long k);

void _mpfi_chebpoly_lrintop_scalar_div_si(mpfi_chebpoly_ptr Llpoly, mpfi_chebpoly_ptr Lrpoly, mpfi_chebpoly_srcptr Klpoly, mpfi_chebpoly_srcptr Krpoly, long r, long c);

// set L := K / c
void mpfi_chebpoly_lrintop_scalar_div_si(mpfi_chebpoly_lrintop_t L, const mpfi_chebpoly_lrintop_t K, long c);

void _mpfi_chebpoly_lrintop_scalar_div_z(mpfi_chebpoly_ptr Llpoly, mpfi_chebpoly_ptr Lrpoly, mpfi_chebpoly_srcptr Klpoly, mpfi_chebpoly_srcptr Krpoly, long r, const mpz_t c);

// set L := K / c
void mpfi_chebpoly_lrintop_scalar_div_z(mpfi_chebpoly_lrintop_t L, const mpfi_chebpoly_lrintop_t K, const mpz_t c);

void _mpfi_chebpoly_lrintop_scalar_div_q(mpfi_chebpoly_ptr Llpoly, mpfi_chebpoly_ptr Lrpoly, mpfi_chebpoly_srcptr Klpoly, mpfi_chebpoly_srcptr Krpoly, long r, const mpq_t c);

// set L := K / c
void mpfi_chebpoly_lrintop_scalar_div_q(mpfi_chebpoly_lrintop_t L, const mpfi_chebpoly_lrintop_t K, const mpq_t c);

void _mpfi_chebpoly_lrintop_scalar_div_d(mpfi_chebpoly_ptr Llpoly, mpfi_chebpoly_ptr Lrpoly, mpfi_chebpoly_srcptr Klpoly, mpfi_chebpoly_srcptr Krpoly, long r, const double_t c);

// set L := K / c
void mpfi_chebpoly_lrintop_scalar_div_d(mpfi_chebpoly_lrintop_t L, const mpfi_chebpoly_lrintop_t K, const double_t c);

void _mpfi_chebpoly_lrintop_scalar_div_fr(mpfi_chebpoly_ptr Llpoly, mpfi_chebpoly_ptr Lrpoly, mpfi_chebpoly_srcptr Klpoly, mpfi_chebpoly_srcptr Krpoly, long r, const mpfr_t c);

// set L := K / c
void mpfi_chebpoly_lrintop_scalar_div_fr(mpfi_chebpoly_lrintop_t L, const mpfi_chebpoly_lrintop_t K, const mpfr_t c);

void _mpfi_chebpoly_lrintop_scalar_div_2si(mpfi_chebpoly_ptr Llpoly, mpfi_chebpoly_ptr Lrpoly, mpfi_chebpoly_srcptr Klpoly, mpfi_chebpoly_srcptr Krpoly, long r, long k);

// set L := 2^-k * K
void mpfi_chebpoly_lrintop_scalar_div_2si(mpfi_chebpoly_lrintop_t L, const mpfi_chebpoly_lrintop_t K, long k);

void _mpfi_chebpoly_lrintop_comp(mpfi_chebpoly_ptr Mlpoly, mpfi_chebpoly_ptr Mrpoly, mpfi_chebpoly_srcptr Klpoly, mpfi_chebpoly_srcptr Krpoly, long r1, mpfi_chebpoly_srcptr Llpoly, mpfi_chebpoly_srcptr Lrpoly, long r2);

// set M := K ∘ L
void mpfi_chebpoly_lrintop_comp(mpfi_chebpoly_lrintop_t M, const mpfi_chebpoly_lrintop_t K, const mpfi_chebpoly_lrintop_t L);

/* convert from mpfi_chebpoly_intop_t */
void mpfi_chebpoly_lrintop_set_intop(mpfi_chebpoly_lrintop_t M, const mpfi_chebpoly_intop_t K);

/* convert to mpfi_chebpoly_intop_t */
void mpfi_chebpoly_lrintop_get_intop(mpfi_chebpoly_intop_t M, const mpfi_chebpoly_lrintop_t K);


/* print */

void mpfi_chebpoly_lrintop_print(const mpfi_chebpoly_lrintop_t K, const char * Cheb_var, size_t digits);


/* convert LODE to lrintop */

// to integral equation on last derivative
// from x-D form: y^(r-1) + a[r-1] * y^(r-1) + ... + a[1] * y' + a[0] * y = h
// with initial conditions y^(i)(-1) = v[i] (0 <= i < r)
// to: f + ∫_-1^t K(t,s) * f(s) ds = g

// compute polynomial kernel K(t,s)
void mpfi_chebpoly_lrintop_set_lode_xD(mpfi_chebpoly_lrintop_t K, const mpfi_chebpoly_srcptr a, long r);

// compute initial condition contribution to right hand side
void mpfi_chebpoly_lrintop_set_lode_xD_initvals(mpfi_chebpoly_t g, mpfi_chebpoly_srcptr a, long r, mpfi_srcptr v);

// compute right hand side g
void mpfi_chebpoly_lrintop_set_lode_xD_rhs(mpfi_chebpoly_t g, mpfi_chebpoly_srcptr a, long r, const mpfi_chebpoly_t h, mpfi_srcptr v);


// to integral equation on the same function
// from D-x form: (-D)^(r)y + (-D)^(r-1)(b[r-1] * y) + ... + (-D)(b[1] * y) + b[0] * y = h
// with initial conditions y^[i](-1) = w[i] (0 <= i < r)
// where y^[i] = (-D)^(i)y + (-D)^(i-1)(b[r-1] * y) + ... + b[r-i] * y
// to: y + ∫_-1^t K(t,s) * y(s) ds = g

// compute polynomial kernel K(t,s)
void mpfi_chebpoly_lrintop_set_lode_Dx(mpfi_chebpoly_lrintop_t K, const mpfi_chebpoly_srcptr b, long r);

// compute initial condition contribution to right hand side
void mpfi_chebpoly_lrintop_set_lode_Dx_initvals(mpfi_chebpoly_t g, long r, mpfi_srcptr w);

// compute right hand side g
void mpfi_chebpoly_lrintop_set_lode_Dx_rhs(mpfi_chebpoly_t g, long r, const mpfi_chebpoly_t h, mpfi_srcptr w);


// get bandmatrix
void mpfi_chebpoly_lrintop_get_bandmatrix(mpfi_bandmatrix_t M, const mpfi_chebpoly_lrintop_t K, long N);


#endif
