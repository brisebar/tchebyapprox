
/*
This file is part of the TchebyApprox library.

The type [mpfr_chebpoly_intop_t] represents a (scalar)
linear Volterra integral operator of the second kind
of order [order] derived from a [mpfr_chebpoly_lode_t].
The new unknown of this intagral equation is the last derivative
of the unknown in the differential equation.
kernel k(t,s) = alpha0(t)*T0(s) + ... + alpha[order-1](t)*T[order-1](s)

Provided solving procedures for the integral equation
use Olver and Townsend's algorithm on almost-banded matrices.
*/


#ifndef MPFR_CHEBPOLY_INTOP_H
#define MPFR_CHEBPOLY_INTOP_H


#include <stdio.h>
#include <stdlib.h>

#include <gmp.h>
#include <mpfr.h>
#include <mpfi.h>
#include <mpfi_io.h>

#include "double_operations.h"

#include "mpfr_bandvec.h"
#include "mpfi_bandvec.h"
#include "mpfr_poly.h"
#include "mpfi_poly.h"
#include "double_chebpoly.h"
#include "mpfr_chebpoly.h"
#include "mpfi_chebpoly.h"
#include "mpfr_chebpoly_lode.h"
#include "mpfr_bandmatrix.h"

#include "double_chebpoly_intop.h"

typedef struct {
  long order;
  mpfr_chebpoly_ptr alpha;
} mpfr_chebpoly_intop_struct;

typedef mpfr_chebpoly_intop_struct mpfr_chebpoly_intop_t[1];

typedef mpfr_chebpoly_intop_struct * mpfr_chebpoly_intop_ptr;

typedef const mpfr_chebpoly_intop_struct * mpfr_chebpoly_intop_srcptr;




void mpfr_chebpoly_intop_init(mpfr_chebpoly_intop_t K);

void mpfr_chebpoly_intop_clear(mpfr_chebpoly_intop_t K);

void mpfr_chebpoly_intop_set(mpfr_chebpoly_intop_t K, const mpfr_chebpoly_intop_t N);

void mpfr_chebpoly_intop_set_double_chebpoly_intop(mpfr_chebpoly_intop_t K, const double_chebpoly_intop_t N);

void mpfr_chebpoly_intop_get_double_chebpoly_intop(double_chebpoly_intop_t K, const mpfr_chebpoly_intop_t N);

void mpfr_chebpoly_intop_swap(mpfr_chebpoly_intop_t K, mpfr_chebpoly_intop_t N);

void mpfr_chebpoly_intop_set_order(mpfr_chebpoly_intop_t K, long order);

long mpfr_chebpoly_intop_Hwidth(const mpfr_chebpoly_intop_t K);

long mpfr_chebpoly_intop_Dwidth(const mpfr_chebpoly_intop_t K);


void mpfr_chebpoly_intop_set_lode(mpfr_chebpoly_intop_t K, const mpfr_chebpoly_lode_t L);

void mpfr_chebpoly_intop_initvals(mpfr_chebpoly_t G, const mpfr_chebpoly_lode_t L, mpfr_srcptr I);

void mpfr_chebpoly_intop_rhs(mpfr_chebpoly_t G, const mpfr_chebpoly_lode_t L, const mpfr_chebpoly_t g, mpfr_srcptr I);

// evaluate on a polynomial ; use integration from -1

void mpfr_chebpoly_intop_evaluate_fr(mpfr_chebpoly_t P, const mpfr_chebpoly_intop_t K, const mpfr_chebpoly_t Q);

void mpfr_chebpoly_intop_evaluate_fi(mpfi_chebpoly_t P, const mpfr_chebpoly_intop_t K, const mpfi_chebpoly_t Q);

void mpfr_chebpoly_intop_evaluate_Ti(mpfr_bandvec_t V, const mpfr_chebpoly_intop_t K, long i);

void mpfr_chebpoly_intop_evaluate_Ti_fi(mpfi_bandvec_t V, const mpfr_chebpoly_intop_t K, long i);


// get the bandmatrix_t associated to the N-th truncation of K

void mpfr_chebpoly_intop_get_bandmatrix(mpfr_bandmatrix_t M, const mpfr_chebpoly_intop_t K, long N);


/* approximate solutions */

void mpfr_chebpoly_intop_approxsolve(mpfr_chebpoly_t F, const mpfr_chebpoly_intop_t K, const mpfr_chebpoly_t G, long N);

void mpfr_chebpoly_lode_intop_approxsolve(mpfr_chebpoly_ptr Sols, const mpfr_chebpoly_lode_t L, const mpfr_chebpoly_t g, mpfr_srcptr I, long N);


void mpfr_chebpoly_intop_approxsolve_check(mpfr_t bound, mpfr_chebpoly_srcptr Sols, const mpfr_chebpoly_lode_t L, const mpfr_chebpoly_t g, mpfr_srcptr I);

/* print */

void mpfr_chebpoly_intop_print(const mpfr_chebpoly_intop_t K, const char * Cheb_var, size_t digits);


#endif
