
/*
This file is part of the TchebyApprox library.

The type [mpfr_vec_t] implements usual vectors of [mpfr_t].
[length] = total length
coeffs[0], ... , coeffs[length-1] = vector entries
*/


#ifndef MPFR_VEC_H
#define MPFR_VEC_H

#include <stdio.h>
#include <stdlib.h>

#include <gmp.h>
#include <mpfr.h>
#include <mpfi.h>
#include <mpfi_io.h>


typedef struct mpfr_vec_struct {
  long length;
  mpfr_ptr coeffs;
} mpfr_vec_struct;

typedef mpfr_vec_struct mpfr_vec_t[1];
typedef mpfr_vec_struct * mpfr_vec_ptr;
typedef const mpfr_vec_struct * mpfr_vec_srcptr;
/* memory management */


// init with an empty vector 
void mpfr_vec_init(mpfr_vec_t V);

// clear V
void _mpfr_vec_clear(mpfr_ptr V, long n);

void mpfr_vec_clear(mpfr_vec_t V);

// set n as length for V
void mpfr_vec_set_length(mpfr_vec_t V, long n);


/* basic manipulations and assignments */

void _mpfr_vec_set(mpfr_ptr V, mpfr_srcptr W, long n);

// copy W into V
void mpfr_vec_set(mpfr_vec_t V, const mpfr_vec_t W);

// swap V and W efficiently
void mpfr_vec_swap(mpfr_vec_t P, mpfr_vec_t Q);

// set the n-th coefficient of V to c
void mpfr_vec_set_coeff_si(mpfr_vec_t V, long n, long c);

// set the n-th coefficient of V to c
void mpfr_vec_set_coeff_z(mpfr_vec_t V, long n, const mpz_t c);

// set the n-th coefficient of V to c
void mpfr_vec_set_coeff_q(mpfr_vec_t V, long n, const mpq_t c);

// set the n-th coefficient of V to c
void mpfr_vec_set_coeff_fr(mpfr_vec_t V, long n, const mpfr_t c);

// get the length of V
long mpfr_vec_length(const mpfr_vec_t V);

void _mpfr_vec_zero(mpfr_ptr V, long n);

// set all coefficients of V to 0
void mpfr_vec_zero(mpfr_vec_t V);


/* reverse */

void _mpfr_vec_shift_left(mpfr_ptr V, mpfr_srcptr W, long n, unsigned long k);

void mpfr_vec_shift_left(mpfr_vec_t V, const mpfr_vec_t W, unsigned long k);

void _mpfr_vec_shift_right(mpfr_ptr V, mpfr_srcptr W, long n, unsigned long k);

void mpfr_vec_shift_right(mpfr_vec_t V, const mpfr_vec_t W, unsigned long k);

void _mpfr_vec_reverse(mpfr_ptr V, mpfr_srcptr W, long n);

void mpfr_vec_reverse(mpfr_vec_t V, const mpfr_vec_t W);


/* norm 1 */

void _mpfr_vec_1norm_ubound(mpfr_t norm, mpfr_srcptr V, long n);

void _mpfr_vec_1norm_lbound(mpfr_t norm, mpfr_srcptr V, long n);

void mpfr_vec_1norm_ubound(mpfr_t norm, const mpfr_vec_t V);

void mpfr_vec_1norm_lbound(mpfr_t norm, const mpfr_vec_t V);



/* test functions */

int _mpfr_vec_is_zero(mpfr_srcptr V, long n);

int mpfr_vec_is_zero(const mpfr_vec_t V);

int _mpfr_vec_equal(mpfr_srcptr V, mpfr_srcptr W, long n);

int mpfr_vec_equal(const mpfr_vec_t V, const mpfr_vec_t W);


/* addition and substraction */

void _mpfr_vec_add(mpfr_ptr V, mpfr_srcptr W, mpfr_srcptr Z, long n); 

// set V := W + Z
void mpfr_vec_add(mpfr_vec_t V, const mpfr_vec_t W, const mpfr_vec_t Z);

void _mpfr_vec_sub(mpfr_ptr V, mpfr_srcptr W, mpfr_srcptr Z, long n);

// set V := W - Z
void mpfr_vec_sub(mpfr_vec_t V, const mpfr_vec_t W, const mpfr_vec_t Z);

void _mpfr_vec_neg(mpfr_ptr V, mpfr_srcptr W, long n);

// set V := -W
void mpfr_vec_neg(mpfr_vec_t V, const mpfr_vec_t W);


/* scalar multiplication and division */

void _mpfr_vec_scalar_mul_si(mpfr_ptr V, mpfr_srcptr W, long n, long c);

// set V := c*W
void mpfr_vec_scalar_mul_si(mpfr_vec_t V, const mpfr_vec_t W, long c);

void _mpfr_vec_scalar_mul_z(mpfr_ptr V, mpfr_srcptr W, long n, const mpz_t c);

// set V := c*W
void mpfr_vec_scalar_mul_z(mpfr_vec_t V, const mpfr_vec_t W, const mpz_t c);

void _mpfr_vec_scalar_mul_q(mpfr_ptr V, mpfr_srcptr W, long n, const mpq_t c);

// set V := c*W
void mpfr_vec_scalar_mul_q(mpfr_vec_t V, const mpfr_vec_t W, const mpq_t c);

void _mpfr_vec_scalar_mul_fr(mpfr_ptr V, mpfr_srcptr W, long n, const mpfr_t c);

// set V := c*W
void mpfr_vec_scalar_mul_fr(mpfr_vec_t V, const mpfr_vec_t W, const mpfr_t c);

void _mpfr_vec_scalar_mul_2si(mpfr_ptr V, mpfr_srcptr W, long n, long k);

// set V := 2^k*W
void mpfr_vec_scalar_mul_2si(mpfr_vec_t P, const mpfr_vec_t Q, long k);


void _mpfr_vec_scalar_div_si(mpfr_ptr V, mpfr_srcptr W, long n, long c);

// set V := (1/c)*W
void mpfr_vec_scalar_div_si(mpfr_vec_t V, const mpfr_vec_t W, long c);

void _mpfr_vec_scalar_div_z(mpfr_ptr V, mpfr_srcptr W, long n, const mpz_t c);

// set V := (1/c)*W
void mpfr_vec_scalar_div_z(mpfr_vec_t V, const mpfr_vec_t W, const mpz_t c);

void _mpfr_vec_scalar_div_q(mpfr_ptr V, mpfr_srcptr W, long n, const mpq_t c);

// set V := c*W
void mpfr_vec_scalar_div_q(mpfr_vec_t V, const mpfr_vec_t W, const mpq_t c);

void _mpfr_vec_scalar_div_fr(mpfr_ptr V, mpfr_srcptr W, long n, const mpfr_t c);

// set V := c*W
void mpfr_vec_scalar_div_fr(mpfr_vec_t V, const mpfr_vec_t W, const mpfr_t c);

void _mpfr_vec_scalar_div_2si(mpfr_ptr V, mpfr_srcptr W, long n, long k);

// set V := 2^-k*W
void mpfr_vec_scalar_div_2si(mpfr_vec_t V, const mpfr_vec_t W, long k);


/* scalar product */

void _mpfr_vec_scalar_prod_fr(mpfr_t prod, mpfr_srcptr V, mpfr_srcptr W, long n);

void mpfr_vec_scalar_prod_fr(mpfr_t prod, const mpfr_vec_t V, const mpfr_vec_t W);

void _mpfr_vec_scalar_prod_fi(mpfi_t prod, mpfr_srcptr V, mpfr_srcptr W, long n);

void mpfr_vec_scalar_prod_fi(mpfi_t prod, const mpfr_vec_t V, const mpfr_vec_t W);



/* display function */


void _mpfr_vec_print(mpfr_srcptr V, long n, size_t digits);

// display the vector V
void mpfr_vec_print(const mpfr_vec_t V, size_t digits);




#endif
