
/*
This file is part of the TchebyApprox library.

The type [mpfi_vec_t] implements usual vectors of [mpfi_t].
[length] = total length
coeffs[0], ... , coeffs[length-1] = vector entries
*/

#ifndef MPFI_VEC_H
#define MPFI_VEC_H

#include <stdio.h>
#include <stdlib.h>

#include <gmp.h>
#include <mpfr.h>
#include <mpfi.h>
#include <mpfi_io.h>

#include "mpfr_vec.h"

typedef struct mpfi_vec_struct {
  long length;
  mpfi_ptr coeffs;
} mpfi_vec_struct;

typedef mpfi_vec_struct mpfi_vec_t[1];
typedef mpfi_vec_struct * mpfi_vec_ptr;
typedef const mpfi_vec_struct * mpfi_vec_srcptr;
/* memory management */


// init with an empty vector
void mpfi_vec_init(mpfi_vec_t V);

// clear V
void _mpfi_vec_clear(mpfi_ptr V, long n);

void mpfi_vec_clear(mpfi_vec_t V);

// set n as length for V
void mpfi_vec_set_length(mpfi_vec_t V, long n);


/* basic manipulations and assignments */

void _mpfi_vec_set(mpfi_ptr V, mpfi_srcptr W, long n);

// copy W into V
void mpfi_vec_set(mpfi_vec_t V, const mpfi_vec_t W);

void _mpfi_vec_set_mpfr_vec(mpfi_ptr V, mpfr_srcptr W, long length);

// copy W (mpfr_vec_t) into V (mpfi_vec_t)
void mpfi_vec_set_mpfr_vec(mpfi_vec_t V, const mpfr_vec_t W);

void _mpfi_vec_get_mpfr_vec(mpfr_ptr V, mpfi_srcptr W, long length);

// get from W (mpfi_vec_t) the mpfr_vec_t V by taking the middle poitn of the coefficients of W
void mpfi_vec_get_mpfr_vec(mpfr_vec_t V, const mpfi_vec_t W);

// swap V and W efficiently
void mpfi_vec_swap(mpfi_vec_t P, mpfi_vec_t Q);

// set the n-th coefficient of V to c
void mpfi_vec_set_coeff_si(mpfi_vec_t V, long n, long c);

// set the n-th coefficient of V to c
void mpfi_vec_set_coeff_z(mpfi_vec_t V, long n, const mpz_t c);

// set the n-th coefficient of V to c
void mpfi_vec_set_coeff_q(mpfi_vec_t V, long n, const mpq_t c);

// set the n-th coefficient of V to c
void mpfi_vec_set_coeff_fr(mpfi_vec_t V, long n, const mpfr_t c);

// set the n-th coefficient of V to c
void mpfi_vec_set_coeff_fi(mpfi_vec_t V, long n, const mpfi_t c);

// get the length of V
long mpfi_vec_length(const mpfi_vec_t V);

void _mpfi_vec_zero(mpfi_ptr V, long n);

// set all coefficients of V to 0
void mpfi_vec_zero(mpfi_vec_t V);


/* shift and reverse */

void _mpfi_vec_shift_left(mpfi_ptr V, mpfi_srcptr W, long n, unsigned long k);

void mpfi_vec_shift_left(mpfi_vec_t V, const mpfi_vec_t W, unsigned long k);

void _mpfi_vec_shift_right(mpfi_ptr V, mpfi_srcptr W, long n, unsigned long k);

void mpfi_vec_shift_right(mpfi_vec_t V, const mpfi_vec_t W, unsigned long k);

void _mpfi_vec_reverse(mpfi_ptr V, mpfi_srcptr W, long n);

void mpfi_vec_reverse(mpfi_vec_t V, const mpfi_vec_t W);


/* norm 1 */

void _mpfi_vec_1norm_fi(mpfi_t norm, mpfi_srcptr V, long n);

void _mpfi_vec_1norm_ubound(mpfr_t norm, mpfi_srcptr V, long n);

void _mpfi_vec_1norm_lbound(mpfr_t norm, mpfi_srcptr V, long n);

void mpfi_vec_1norm_fi(mpfi_t norm, const mpfi_vec_t V);

void mpfi_vec_1norm_ubound(mpfr_t norm, const mpfi_vec_t V);

void mpfi_vec_1norm_lbound(mpfr_t norm, const mpfi_vec_t V);



/* test functions */

int _mpfi_vec_is_zero(mpfi_srcptr V, long n);

int mpfi_vec_is_zero(const mpfi_vec_t V);

int _mpfi_vec_has_zero(mpfi_srcptr V, long n);

int mpfi_vec_has_zero(const mpfi_vec_t V);


/* addition and substraction */

void _mpfi_vec_add(mpfi_ptr V, mpfi_srcptr W, mpfi_srcptr Z, long n); 

// set V := W + Z
void mpfi_vec_add(mpfi_vec_t V, const mpfi_vec_t W, const mpfi_vec_t Z);

void _mpfi_vec_sub(mpfi_ptr V, mpfi_srcptr W, mpfi_srcptr Z, long n);

// set V := W - Z
void mpfi_vec_sub(mpfi_vec_t V, const mpfi_vec_t W, const mpfi_vec_t Z);

void _mpfi_vec_neg(mpfi_ptr V, mpfi_srcptr W, long n);

// set V := -W
void mpfi_vec_neg(mpfi_vec_t V, const mpfi_vec_t W);


/* scalar multiplication and division */

void _mpfi_vec_scalar_mul_si(mpfi_ptr V, mpfi_srcptr W, long n, long c);

// set V := c*W
void mpfi_vec_scalar_mul_si(mpfi_vec_t V, const mpfi_vec_t W, long c);

void _mpfi_vec_scalar_mul_z(mpfi_ptr V, mpfi_srcptr W, long n, const mpz_t c);

// set V := c*W
void mpfi_vec_scalar_mul_z(mpfi_vec_t V, const mpfi_vec_t W, const mpz_t c);

void _mpfi_vec_scalar_mul_q(mpfi_ptr V, mpfi_srcptr W, long n, const mpq_t c);

// set V := c*W
void mpfi_vec_scalar_mul_q(mpfi_vec_t V, const mpfi_vec_t W, const mpq_t c);

void _mpfi_vec_scalar_mul_fr(mpfi_ptr V, mpfi_srcptr W, long n, const mpfr_t c);

// set V := c*W
void mpfi_vec_scalar_mul_fr(mpfi_vec_t V, const mpfi_vec_t W, const mpfr_t c);

void _mpfi_vec_scalar_mul_fi(mpfi_ptr V, mpfi_srcptr W, long n, const mpfi_t c);

// set V := c*W
void mpfi_vec_scalar_mul_fi(mpfi_vec_t V, const mpfi_vec_t W, const mpfi_t c);

void _mpfi_vec_scalar_mul_2si(mpfi_ptr V, mpfi_srcptr W, long n, long k);

// set V := 2^k*W
void mpfi_vec_scalar_mul_2si(mpfi_vec_t P, const mpfi_vec_t Q, long k);


void _mpfi_vec_scalar_div_si(mpfi_ptr V, mpfi_srcptr W, long n, long c);

// set V := (1/c)*W
void mpfi_vec_scalar_div_si(mpfi_vec_t V, const mpfi_vec_t W, long c);

void _mpfi_vec_scalar_div_z(mpfi_ptr V, mpfi_srcptr W, long n, const mpz_t c);

// set V := (1/c)*W
void mpfi_vec_scalar_div_z(mpfi_vec_t V, const mpfi_vec_t W, const mpz_t c);

void _mpfi_vec_scalar_div_q(mpfi_ptr V, mpfi_srcptr W, long n, const mpq_t c);

// set V := c*W
void mpfi_vec_scalar_div_q(mpfi_vec_t V, const mpfi_vec_t W, const mpq_t c);

void _mpfi_vec_scalar_div_fr(mpfi_ptr V, mpfi_srcptr W, long n, const mpfr_t c);

// set V := c*W
void mpfi_vec_scalar_div_fr(mpfi_vec_t V, const mpfi_vec_t W, const mpfr_t c);

void _mpfi_vec_scalar_div_fi(mpfi_ptr V, mpfi_srcptr W, long n, const mpfi_t c);

// set V := c*W
void mpfi_vec_scalar_div_fi(mpfi_vec_t V, const mpfi_vec_t W, const mpfi_t c);

void _mpfi_vec_scalar_div_2si(mpfi_ptr V, mpfi_srcptr W, long n, long k);

// set V := 2^-k*W
void mpfi_vec_scalar_div_2si(mpfi_vec_t V, const mpfi_vec_t W, long k);


/* scalar product */

void _mpfi_vec_scalar_prod_fi(mpfi_t prod, mpfi_srcptr V, mpfi_srcptr W, long n);

void mpfi_vec_scalar_prod_fi(mpfi_t prod, const mpfi_vec_t V, const mpfi_vec_t W);



/* display function */


void _mpfi_vec_print(mpfi_srcptr V, long n, size_t digits);

// display the vector V
void mpfi_vec_print(const mpfi_vec_t V, size_t digits);




#endif
