
/*
This file is part of the TchebyApprox library.

The type [chebmodel_lrintop_t] represents a (scalar)
linear Volterra integral operator of the second kind
with a bivariate polynomial kernel given as a low rank decomposition with [chebmodel_t], that is
K(t,s) = sum_{0 <= i < length} lpoly[t] * rpoly[s]
*/


#ifndef CHEBMODEL_LRINTOP_H
#define CHEBMODEL_LRINTOP_H


#include <stdio.h>
#include <stdlib.h>

#include <gmp.h>
#include <mpfr.h>
#include <mpfi.h>
#include <mpfi_io.h>

#include "double_operations.h"

#include "double_chebpoly.h"
#include "mpfr_chebpoly.h"
#include "mpfi_chebpoly.h"
#include "chebmodel.h"

#include "chebmodel_lode.h"
#include "chebmodel_intop.h"

#include "double_chebpoly_lrintop.h"
#include "mpfr_chebpoly_lrintop.h"
#include "mpfi_chebpoly_lrintop.h"


typedef struct {
  long length;
  chebmodel_ptr lpoly;
  chebmodel_ptr rpoly;
} chebmodel_lrintop_struct;

typedef chebmodel_lrintop_struct chebmodel_lrintop_t[1];

typedef chebmodel_lrintop_struct * chebmodel_intop_lrptr;

typedef const chebmodel_lrintop_struct * chebmodel_lrintop_srcptr;




void chebmodel_lrintop_init(chebmodel_lrintop_t K);

void chebmodel_lrintop_clear(chebmodel_lrintop_t K);

void chebmodel_lrintop_set(chebmodel_lrintop_t L, const chebmodel_lrintop_t K);

void chebmodel_lrintop_set_double_chebpoly_lrintop(chebmodel_lrintop_t L, const double_chebpoly_lrintop_t K);

void chebmodel_lrintop_get_double_chebpoly_lrintop(double_chebpoly_lrintop_t L, const chebmodel_lrintop_t K);

void chebmodel_lrintop_set_mpfr_chebpoly_lrintop(chebmodel_lrintop_t L, const mpfr_chebpoly_lrintop_t K);

void chebmodel_lrintop_get_mpfr_chebpoly_lrintop(mpfr_chebpoly_lrintop_t L, const chebmodel_lrintop_t K);

void chebmodel_lrintop_set_mpfi_chebpoly_lrintop(chebmodel_lrintop_t L, const mpfi_chebpoly_lrintop_t K);

void chebmodel_lrintop_get_mpfi_chebpoly_lrintop(mpfi_chebpoly_lrintop_t L, const chebmodel_lrintop_t K);

void chebmodel_lrintop_swap(chebmodel_lrintop_t L, chebmodel_lrintop_t K);

void chebmodel_lrintop_set_length(chebmodel_lrintop_t K, long length);


// evaluate on a polynomial

void chebmodel_lrintop_evaluate_d(chebmodel_t P, const chebmodel_lrintop_t K, const double_chebpoly_t Q);

void chebmodel_lrintop_evaluate_fr(chebmodel_t P, const chebmodel_lrintop_t K, const mpfr_chebpoly_t Q);

void chebmodel_lrintop_evaluate_fi(chebmodel_t P, const chebmodel_lrintop_t K, const mpfi_chebpoly_t Q);

void chebmodel_lrintop_evaluate_cm(chebmodel_t P, const chebmodel_lrintop_t K, const chebmodel_t Q);


// bounds

// crude bound on the sum
// A REVOIR : facteur 2 ?
void chebmodel_lrintop_lr1norm(mpfr_t b, const chebmodel_lrintop_t K);

// convert to 2-dimensional polynomial in Chebyshev basis and use 1 norm
void chebmodel_lrintop_1norm(mpfr_t b, const chebmodel_lrintop_t K);


// operations

void _chebmodel_lrintop_neg(chebmodel_ptr Llpoly, chebmodel_ptr Lrpoly, chebmodel_srcptr Klpoly, chebmodel_srcptr Krpoly, long r);

// set L := -K
void chebmodel_lrintop_neg(chebmodel_lrintop_t L, const chebmodel_lrintop_t K);

void _chebmodel_lrintop_add(chebmodel_ptr Mlpoly, chebmodel_ptr Mrpoly, chebmodel_srcptr Klpoly, chebmodel_srcptr Krpoly, long r1, chebmodel_srcptr Llpoly, chebmodel_srcptr Lrpoly, long r2);

// set M := K + L
void chebmodel_lrintop_add(chebmodel_lrintop_t M, const chebmodel_lrintop_t K, const chebmodel_lrintop_t L);

void _chebmodel_lrintop_sub(chebmodel_ptr Mlpoly, chebmodel_ptr Mrpoly, chebmodel_srcptr Klpoly, chebmodel_srcptr Krpoly, long r1, chebmodel_srcptr Llpoly, chebmodel_srcptr Lrpoly, long r2);

// set M := K - L
void chebmodel_lrintop_sub(chebmodel_lrintop_t M, const chebmodel_lrintop_t K, const chebmodel_lrintop_t L);

void _chebmodel_lrintop_scalar_mul_si(chebmodel_ptr Llpoly, chebmodel_ptr Lrpoly, chebmodel_srcptr Klpoly, chebmodel_srcptr Krpoly, long r, long c);

// set L := c * K
void chebmodel_lrintop_scalar_mul_si(chebmodel_lrintop_t L, const chebmodel_lrintop_t K, long c);

void _chebmodel_lrintop_scalar_mul_z(chebmodel_ptr Llpoly, chebmodel_ptr Lrpoly, chebmodel_srcptr Klpoly, chebmodel_srcptr Krpoly, long r, const mpz_t c);

// set L := c * K
void chebmodel_lrintop_scalar_mul_z(chebmodel_lrintop_t L, const chebmodel_lrintop_t K, const mpz_t c);

void _chebmodel_lrintop_scalar_mul_q(chebmodel_ptr Llpoly, chebmodel_ptr Lrpoly, chebmodel_srcptr Klpoly, chebmodel_srcptr Krpoly, long r, const mpq_t c);

// set L := c * K
void chebmodel_lrintop_scalar_mul_q(chebmodel_lrintop_t L, const chebmodel_lrintop_t K, const mpq_t c);

void _chebmodel_lrintop_scalar_mul_d(chebmodel_ptr Llpoly, chebmodel_ptr Lrpoly, chebmodel_srcptr Klpoly, chebmodel_srcptr Krpoly, long r, const double_t c);

// set L := c * K
void chebmodel_lrintop_scalar_mul_d(chebmodel_lrintop_t L, const chebmodel_lrintop_t K, const double_t c);

void _chebmodel_lrintop_scalar_mul_fr(chebmodel_ptr Llpoly, chebmodel_ptr Lrpoly, chebmodel_srcptr Klpoly, chebmodel_srcptr Krpoly, long r, const mpfr_t c);

// set L := c * K
void chebmodel_lrintop_scalar_mul_fr(chebmodel_lrintop_t L, const chebmodel_lrintop_t K, const mpfr_t c);

void _chebmodel_lrintop_scalar_mul_2si(chebmodel_ptr Llpoly, chebmodel_ptr Lrpoly, chebmodel_srcptr Klpoly, chebmodel_srcptr Krpoly, long r, long k);

// set L := 2^k * K
void chebmodel_lrintop_scalar_mul_2si(chebmodel_lrintop_t L, const chebmodel_lrintop_t K, long k);

void _chebmodel_lrintop_scalar_div_si(chebmodel_ptr Llpoly, chebmodel_ptr Lrpoly, chebmodel_srcptr Klpoly, chebmodel_srcptr Krpoly, long r, long c);

// set L := K / c
void chebmodel_lrintop_scalar_div_si(chebmodel_lrintop_t L, const chebmodel_lrintop_t K, long c);

void _chebmodel_lrintop_scalar_div_z(chebmodel_ptr Llpoly, chebmodel_ptr Lrpoly, chebmodel_srcptr Klpoly, chebmodel_srcptr Krpoly, long r, const mpz_t c);

// set L := K / c
void chebmodel_lrintop_scalar_div_z(chebmodel_lrintop_t L, const chebmodel_lrintop_t K, const mpz_t c);

void _chebmodel_lrintop_scalar_div_q(chebmodel_ptr Llpoly, chebmodel_ptr Lrpoly, chebmodel_srcptr Klpoly, chebmodel_srcptr Krpoly, long r, const mpq_t c);

// set L := K / c
void chebmodel_lrintop_scalar_div_q(chebmodel_lrintop_t L, const chebmodel_lrintop_t K, const mpq_t c);

void _chebmodel_lrintop_scalar_div_d(chebmodel_ptr Llpoly, chebmodel_ptr Lrpoly, chebmodel_srcptr Klpoly, chebmodel_srcptr Krpoly, long r, const double_t c);

// set L := K / c
void chebmodel_lrintop_scalar_div_d(chebmodel_lrintop_t L, const chebmodel_lrintop_t K, const double_t c);

void _chebmodel_lrintop_scalar_div_fr(chebmodel_ptr Llpoly, chebmodel_ptr Lrpoly, chebmodel_srcptr Klpoly, chebmodel_srcptr Krpoly, long r, const mpfr_t c);

// set L := K / c
void chebmodel_lrintop_scalar_div_fr(chebmodel_lrintop_t L, const chebmodel_lrintop_t K, const mpfr_t c);

void _chebmodel_lrintop_scalar_div_2si(chebmodel_ptr Llpoly, chebmodel_ptr Lrpoly, chebmodel_srcptr Klpoly, chebmodel_srcptr Krpoly, long r, long k);

// set L := 2^-k * K
void chebmodel_lrintop_scalar_div_2si(chebmodel_lrintop_t L, const chebmodel_lrintop_t K, long k);

void _chebmodel_lrintop_comp(chebmodel_ptr Mlpoly, chebmodel_ptr Mrpoly, chebmodel_srcptr Klpoly, chebmodel_srcptr Krpoly, long r1, chebmodel_srcptr Llpoly, chebmodel_srcptr Lrpoly, long r2);

// set M := K ∘ L
void chebmodel_lrintop_comp(chebmodel_lrintop_t M, const chebmodel_lrintop_t K, const chebmodel_lrintop_t L);

/* convert from chebmodel_intop_t */
void chebmodel_lrintop_set_intop(chebmodel_lrintop_t M, const chebmodel_intop_t K);


/* print */

void chebmodel_lrintop_print(const chebmodel_lrintop_t K, const char * Cheb_var, size_t digits);


/* convert LODE to lrintop */

// to integral equation on last derivative
// from x-D form: y^(r-1) + a[r-1] * y^(r-1) + ... + a[1] * y' + a[0] * y = h
// with initial conditions y^(i)(-1) = v[i] (0 <= i < r)
// to: f + ∫_-1^t K(t,s) * f(s) ds = g

// compute polynomial kernel K(t,s)
void chebmodel_lrintop_set_lode_xD(chebmodel_lrintop_t K, const chebmodel_srcptr a, long r);

// compute initial condition contribution to right hand side
void chebmodel_lrintop_set_lode_xD_initvals(chebmodel_t g, chebmodel_srcptr a, long r, mpfi_srcptr v);

// compute right hand side g
void chebmodel_lrintop_set_lode_xD_rhs(chebmodel_t g, chebmodel_srcptr a, long r, const chebmodel_t h, mpfi_srcptr v);


// to integral equation on the same function
// from D-x form: (-D)^(r)y + (-D)^(r-1)(b[r-1] * y) + ... + (-D)(b[1] * y) + b[0] * y = h
// with initial conditions y^[i](-1) = w[i] (0 <= i < r)
// where y^[i] = (-D)^(i)y + (-D)^(i-1)(b[r-1] * y) + ... + b[r-i] * y
// to: y + ∫_-1^t K(t,s) * y(s) ds = g

// compute polynomial kernel K(t,s)
void chebmodel_lrintop_set_lode_Dx(chebmodel_lrintop_t K, const chebmodel_srcptr b, long r);

// compute initial condition contribution to right hand side
#define chebmodel_lrintop_set_lode_Dx_initvals mpfi_chebpoly_lrintop_set_lode_Dx_initvals

// compute right hand side g
void chebmodel_lrintop_set_lode_Dx_rhs(chebmodel_t g, long r, const chebmodel_t h, mpfi_srcptr w);


#endif
