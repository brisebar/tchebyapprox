
#ifndef DOUBLE_OPERATIONS_H
#define DOUBLE_OPERATIONS_H

#include <stdlib.h>
#include <math.h>

typedef double double_tt[1];
typedef double * double_ptr;
typedef const double * double_srcptr;
typedef double_ptr * double_ptr_ptr;
typedef double_ptr_ptr * double_ptr_ptr_ptr;
#define double_t double_tt

#define double_init(x) {}	

#define double_clear(x)	{}

#define double_set(x, y, rnd)	*(x) = *(y)

void double_swap(double_t x, double_t y);

#define double_set_zero(x, rnd)	*(x) = 0

#define double_zero_p(x)	(*(x) == 0)

#define double_sgn(x)	((*(x) > 0) - (*(x) < 0))

#define double_cmp(x,y)	((*(x)-*(y) > 0) - (*(x)-*(y) < 0))

#define double_cmp_si(x,n)	((*(x)-n > 0) - (*(x)-n < 0))

#define double_cmpabs(x, y)	((fabs(*(x))-fabs(*(y)) > 0) - (fabs(*(x))-fabs(*(y)) < 0))

#define double_equal_p(x, y)	(*(x)==*(y))

#define double_set_si(x, n, rnd)	*(x) = n

#define double_set_z(x, z, rnd)	*(x) = mpz_get_d(z)

#define double_set_q(x, q, rnd)	*(x) = mpq_get_d(q)

#define double_set_fr(x, y, rnd) *(x) = mpfr_get_d(y, rnd)


#define double_add(x, y, z, rnd)	*(x) = *(y) + *(z)

#define double_add_si(x, y, z, rnd)	*(x) = *(y) + (z)

#define double_sub(x, y, z, rnd)	*(x) = *(y) - *(z)

#define double_sub_si(x, y, z, rnd)	*(x) = *(y) - (z)

#define double_si_sub(x, y, z, rnd)	*(x) = (y) - *(z)

#define double_neg(x, y, rnd)	*(x) = - *(y)

#define double_mul(x, y, z, rnd)	*(x) = *(y) * *(z)

#define double_mul_si(x, y, z, rnd)	*(x) = *(y) * (z)

#define double_mul_2si(x, y, n, rnd)	*(x) = *(y) * pow(2,n)

#define double_div(x, y, z, rnd)	*(x) = *(y) / *(z)

#define double_div_si(x, y, z, rnd)	*(x) = *(y) / (z)

#define double_si_div(x, y, z, rnd)	*(x) = (y) / *(z)

#define double_div_2si(x, y, n, rnd)	*(x) = *(y) / pow(2,n)

#define double_sqrt(x, y, rnd)	*(x) = sqrt(*(y))

#define double_abs(x, y, rnd)	*(x) = fabs(*(y))

#define double_const_pi(x, rnd)	*(x) = M_PI

#define double_cos(x, y, rnd)	*(x) = cos(*(y))

#define double_sin(x, y, rnd)	*(x) = sin(*(y))















#endif
