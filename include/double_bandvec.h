
/*
This file is part of the TchebyApprox library.

The type [double_bandvec_t] implements ([Hwidth],[Dwidth])
almost-banded vectors of [double] around index [ind].
The dimension (finite or infinite) of the underlying vector space
is not specified.
Non-zero coefficients are located:
  - between indicies from 0 to Hwidth-1 (initial coefficients):
    H[0], ... , H[Hwidth-1]
  - between indicies from max(ind-Dwidth,0) to ind+Dwidth (diagonal coefficients):
    D[0], ..., D[Dwidth], ... , D[2*Dwidth]
*/


#ifndef DOUBLE_BANDVEC_H
#define DOUBLE_BANDVEC_H

#include <stdio.h>
#include <stdlib.h>

#include <mpfr.h>
#include <mpfi.h>
#include <mpfi_io.h>

#include "double_operations.h"

#include "double_vec.h"

typedef struct double_bandvec_struct {
  long Hwidth;
  long Dwidth;
  long ind;
  double_ptr H;
  double_ptr D;
} double_bandvec_struct;

typedef double_bandvec_struct double_bandvec_t[1];
typedef double_bandvec_struct * double_bandvec_ptr;
typedef const double_bandvec_struct * double_bandvec_srcptr;


/* memory management */


// init with a zero vector (Hwidth = 0, Dwidth = 0, D[0] = 0)
void double_bandvec_init(double_bandvec_t V);

// clear V
void double_bandvec_clear(double_bandvec_t V);

// set parameters for V
void double_bandvec_set_params(double_bandvec_t V, long Hwidth, long Dwidth, long ind);


/* basic manipulations and assignments */

void _double_bandvec_set(double_ptr VH, double_ptr VD, long VHwidth, long VDwidth, double_srcptr WH, double_srcptr WD, long WHwidth, long WDwidth, long ind);

void double_bandvec_set(double_bandvec_t V, const double_bandvec_t W);

void double_bandvec_swap(double_bandvec_t V, double_bandvec_t W);

void _double_bandvec_zero(double_ptr VH, double_ptr VD, long Hwidth, long Dwidth);

void double_bandvec_zero(double_bandvec_t V);

// normalise the H and D components if they overlap

void _double_bandvec_normalise(double_ptr VH, double_ptr VD, long VHwidth, long VDwidth, long ind);

void double_bandvec_normalise(double_bandvec_t V);

// set_coeff: set the n-th coeff of V (or (VH,VD,Hwidth,Dwidth,ind)) to c

void _double_bandvec_set_coeff_si(double_ptr VH, double_ptr VD, long Hwidth, long Dwidth, long ind, long n, long c);

void double_bandvec_set_coeff_si(double_bandvec_t V, long n, long c);

void _double_bandvec_set_coeff_z(double_ptr VH, double_ptr VD, long Hwidth, long Dwidth, long ind, long n, const mpz_t c);

void double_bandvec_set_coeff_z(double_bandvec_t V, long n, const mpz_t c);

void _double_bandvec_set_coeff_q(double_ptr VH, double_ptr VD, long Hwidth, long Dwidth, long ind, long n, const mpq_t c);

void double_bandvec_set_coeff_q(double_bandvec_t V, long n, const mpq_t c);

void _double_bandvec_set_coeff_d(double_ptr VH, double_ptr VD, long Hwidth, long Dwidth, long ind, long n, const double_t c);

void double_bandvec_set_coeff_d(double_bandvec_t V, long n, const double_t c);

void _double_bandvec_set_coeff_fr(double_ptr VH, double_ptr VD, long Hwidth, long Dwidth, long ind, long n, const mpfr_t c);

void double_bandvec_set_coeff_fr(double_bandvec_t V, long n, const mpfr_t c);

void _double_bandvec_get_double_vec(double_ptr V, long length, double_srcptr WH, double_srcptr WD, long WHwidth, long WDwidth, long ind);

// convert a bandvec into a vec, by using the length of V
void double_bandvec_get_double_vec(double_vec_t V, const double_bandvec_t W);


/* norm 1 */

void _double_bandvec_1norm(double_t norm, double_srcptr VH, double_srcptr VD, long Hwidth, long Dwidth, long ind);

void double_bandvec_1norm(double_t norm, const double_bandvec_t V);

void _double_bandvec_1norm_ubound(double_t norm, double_srcptr VH, double_srcptr VD, long Hwidth, long Dwidth, long ind);

void double_bandvec_1norm_ubound(double_t norm, const double_bandvec_t V);

void _double_bandvec_1norm_lbound(double_t norm, double_srcptr VH, double_srcptr VD, long Hwidth, long Dwidth, long ind);

void double_bandvec_1norm_lbound(double_t norm, const double_bandvec_t V);

void _double_bandvec_1norm_fi(mpfi_t norm, double_srcptr VH, double_srcptr VD, long Hwidth, long Dwidth, long ind);

void double_bandvec_1norm_fi(mpfi_t norm, const double_bandvec_t V);


/* addition and substraction */
// W and Z must have the same ind, but not necessarily the same Hiwdht and Dwidth

// for add and sub, one must have VHwidth >= max(WHwidth,ZHwidth) and VDwidth >= max(WDwidth,ZDwidth)

void _double_bandvec_add(double_ptr VH, double_ptr VD, long VHwidth, long VDwidth, double_srcptr WH, double_srcptr WD, long WHwidth, long WDwidth, double_srcptr ZH, double_srcptr ZD, long ZHwidth, long ZDwidth, long ind);

void double_bandvec_add(double_bandvec_t V, const double_bandvec_t W, const double_bandvec_t Z);


void _double_bandvec_sub(double_ptr VH, double_ptr VD, long VHwidth, long VDwidth, double_srcptr WH, double_srcptr WD, long WHwidth, long WDwidth, double_srcptr ZH, double_srcptr ZD, long ZHwidth, long ZDwidth, long ind);

void double_bandvec_sub(double_bandvec_t V, const double_bandvec_t W, const double_bandvec_t Z);

// for neg, one must have VHwidth >= WHwidth and VDwidth >= WDwidth

void _double_bandvec_neg(double_ptr VH, double_ptr VD, long VHwidth, long VDwidth, double_srcptr WH, double_srcptr WD, long WHwidth, long WDwidth, long ind);

void double_bandvec_neg(double_bandvec_t V, const double_bandvec_t W);


/* scalar multiplication and division */

void _double_bandvec_scalar_mul_si(double_ptr VH, double_ptr VD, long VHwidth, long Vdwidth, double_srcptr WH, double_srcptr WD, long WHwidth, long WDwidth, long ind, long c);

void double_bandvec_scalar_mul_si(double_bandvec_t V, const double_bandvec_t W, long c); 

void _double_bandvec_scalar_mul_z(double_ptr VH, double_ptr VD, long VHwidth, long Vdwidth, double_srcptr WH, double_srcptr WD, long WHwidth, long WDwidth, long ind, const mpz_t c);

void double_bandvec_scalar_mul_z(double_bandvec_t V, const double_bandvec_t W, const mpz_t c);

void _double_bandvec_scalar_mul_q(double_ptr VH, double_ptr VD, long VHwidth, long Vdwidth, double_srcptr WH, double_srcptr WD, long WHwidth, long WDwidth, long ind, const mpq_t c);

void double_bandvec_scalar_mul_q(double_bandvec_t V, const double_bandvec_t W, const mpq_t c);

void _double_bandvec_scalar_mul_d(double_ptr VH, double_ptr VD, long VHwidth, long Vdwidth, double_srcptr WH, double_srcptr WD, long WHwidth, long WDwidth, long ind, const double_t c);

void double_bandvec_scalar_mul_d(double_bandvec_t V, const double_bandvec_t W, const double_t c);

void _double_bandvec_scalar_mul_fr(double_ptr VH, double_ptr VD, long VHwidth, long Vdwidth, double_srcptr WH, double_srcptr WD, long WHwidth, long WDwidth, long ind, const mpfr_t c);

void double_bandvec_scalar_mul_fr(double_bandvec_t V, const double_bandvec_t W, const mpfr_t c);

void _double_bandvec_scalar_mul_2si(double_ptr VH, double_ptr VD, long VHwidth, long VDwidth, double_srcptr WH, double_srcptr WD, long WHwidth, long WDwidth, long ind, long k);

void double_bandvec_scalar_mul_2si(double_bandvec_t V, const double_bandvec_t W, long k);


void _double_bandvec_scalar_div_si(double_ptr VH, double_ptr VD, long VHwidth, long Vdwidth, double_srcptr WH, double_srcptr WD, long WHwidth, long WDwidth, long ind, long c);

void double_bandvec_scalar_div_si(double_bandvec_t V, const double_bandvec_t W, long c); 

void _double_bandvec_scalar_div_z(double_ptr VH, double_ptr VD, long VHwidth, long Vdwidth, double_srcptr WH, double_srcptr WD, long WHwidth, long WDwidth, long ind, const mpz_t c);

void double_bandvec_scalar_div_z(double_bandvec_t V, const double_bandvec_t W, const mpz_t c);

void _double_bandvec_scalar_div_q(double_ptr VH, double_ptr VD, long VHwidth, long Vdwidth, double_srcptr WH, double_srcptr WD, long WHwidth, long WDwidth, long ind, const mpq_t c);

void double_bandvec_scalar_div_q(double_bandvec_t V, const double_bandvec_t W, const mpq_t c);

void _double_bandvec_scalar_div_d(double_ptr VH, double_ptr VD, long VHwidth, long Vdwidth, double_srcptr WH, double_srcptr WD, long WHwidth, long WDwidth, long ind, const double_t c);

void double_bandvec_scalar_div_d(double_bandvec_t V, const double_bandvec_t W, const double_t c);

void _double_bandvec_scalar_div_fr(double_ptr VH, double_ptr VD, long VHwidth, long Vdwidth, double_srcptr WH, double_srcptr WD, long WHwidth, long WDwidth, long ind, const mpfr_t c);

void double_bandvec_scalar_div_fr(double_bandvec_t V, const double_bandvec_t W, const mpfr_t c);

void _double_bandvec_scalar_div_2si(double_ptr VH, double_ptr VD, long VHwidth, long VDwidth, double_srcptr WH, double_srcptr WD, long WHwidth, long WDwidth, long ind, long k);

void double_bandvec_scalar_div_2si(double_bandvec_t V, const double_bandvec_t W, long k);


/* display function */


void _double_bandvec_print(double_srcptr VH, double_srcptr VD, long VHwidth, long VDwidth, long ind);

// display the vector V
void double_bandvec_print(const double_bandvec_t V);


#endif
