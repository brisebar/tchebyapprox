
/*
This file is part of the TchebyApprox library.

The type [mpfi_chebpoly_t] implements polynomials in Chebyshev basis
with coefficients of type [mpfi_t].
[degree] = degree of the polynomial (-1 = null polynomial).
coeffs[0], ... , coeffs[degree-1] = coefficients in Chebyshev basis.
*/


#ifndef MPFI_CHEBPOLY_H
#define MPFI_CHEBPOLY_H

#include <stdio.h>
#include <stdlib.h>

#include <gmp.h>
#include <mpfr.h>
#include <mpfi.h>
#include <mpfi_io.h>

#include "mpfi_poly.h"
#include "mpfr_chebpoly.h"
#include "mpfi_vec.h"


// deg is always the exact degree of the polynomial
// by convention, deg(0) = -1

typedef struct mpfi_chebpoly_struct {
  long degree;
  mpfi_ptr coeffs;
} mpfi_chebpoly_struct;

typedef mpfi_chebpoly_struct mpfi_chebpoly_t[1];
typedef mpfi_chebpoly_struct * mpfi_chebpoly_ptr;
typedef const mpfi_chebpoly_struct * mpfi_chebpoly_srcptr;
typedef mpfi_chebpoly_ptr * mpfi_chebpoly_ptr_ptr;
typedef mpfi_chebpoly_ptr_ptr * mpfi_chebpoly_ptr_ptr_ptr;


/* memory management */

// init with P = 0
void mpfi_chebpoly_init(mpfi_chebpoly_t P);

// clear P
void mpfi_chebpoly_clear(mpfi_chebpoly_t P);

// set n as degree for P
void mpfi_chebpoly_set_degree(mpfi_chebpoly_t P, long n);

// set P in canonical form (exact degree)
void mpfi_chebpoly_normalise(mpfi_chebpoly_t P);



/* basic manipulations and assignments */

#define _mpfi_chebpoly_set _mpfi_poly_set

// copy Q into P
void mpfi_chebpoly_set(mpfi_chebpoly_t P, const mpfi_chebpoly_t Q);

#define _mpfi_chebpoly_set_double_chebpoly _mpfi_poly_set_double_poly

// copy Q (with double coefficients) into P
void mpfi_chebpoly_set_double_chebpoly(mpfi_chebpoly_t P, const double_chebpoly_t Q);

#define _mpfi_chebpoly_set_mpfr_chebpoly _mpfi_poly_set_mpfr_poly

// copy Q (with mpfr coefficients) into P
void mpfi_chebpoly_set_mpfr_chebpoly(mpfi_chebpoly_t P, const mpfr_chebpoly_t Q);

#define _mpfi_chebpoly_get_double_chebpoly _mpfi_poly_get_double_poly

// obtain P (with double coefficients) from Q
void mpfi_chebpoly_get_double_chebpoly(double_chebpoly_t P, const mpfi_chebpoly_t Q);

#define _mpfi_chebpoly_get_mpfr_chebpoly _mpfi_poly_get_mpfr_poly

// obtain P (with mpfr coefficients) from Q
void mpfi_chebpoly_get_mpfr_chebpoly(mpfr_chebpoly_t P, const mpfi_chebpoly_t Q);

// swap P and Q efficiently
void mpfi_chebpoly_swap(mpfi_chebpoly_t P, mpfi_chebpoly_t Q);

// set the n-th coefficient of P to c
void mpfi_chebpoly_set_coeff_si(mpfi_chebpoly_t P, long n, long c);

// set the n-th coefficient of P to c
void mpfi_chebpoly_set_coeff_z(mpfi_chebpoly_t P, long n, const mpz_t c);

// set the n-th coefficient of P to c
void mpfi_chebpoly_set_coeff_q(mpfi_chebpoly_t P, long n, const mpq_t c);

// set the n-th coefficient of P to c
void mpfi_chebpoly_set_coeff_fr(mpfi_chebpoly_t P, long n, const mpfr_t c);

// set the n-th coefficient of P to c
void mpfi_chebpoly_set_coeff_fi(mpfi_chebpoly_t P, long n, const mpfi_t c);

// get the degree of P
long mpfi_chebpoly_degree(const mpfi_chebpoly_t P);

// get the n-th coefficient of P
void mpfi_chebpoly_get_coeff(mpfi_t c, const mpfi_chebpoly_t P, long n);

// get a pointer to the n-th coefficient of P
// return NULL if n < 0 or if n > degree(P)
#define mpfi_chebpoly_get_coeff_ptr(P, n) (((n) <= (P)->degree) && (n >= 0) ? (P)->coeffs + (n) : NULL)

// set P to 0
void mpfi_chebpoly_zero(mpfi_chebpoly_t P);

#define _mpfi_chebpoly_monic _mpfi_poly_monic

// set P to (1/c)*Q where c is the leading coefficient of P in the Chebyshev basis
void mpfi_chebpoly_monic(mpfi_chebpoly_t P, const mpfi_chebpoly_t Q);



/* shifting, reverse */

#define _mpfi_chebpoly_shift_left _mpfi_poly_shift_left

// set P to Q*X^k
void mpfi_chebpoly_shift_left(mpfi_chebpoly_t P, const mpfi_chebpoly_t Q, unsigned long k);

#define _mpfi_chebpoly_shift_right _mpfi_poly_shift_right

// set P to Q/X^k
void mpfi_chebpoly_shift_right(mpfi_chebpoly_t P, const mpfi_chebpoly_t Q, unsigned long k);

// reverse a list of mpfi of size (n+1)
#define _mpfi_chebpoly_reverse _mpfi_poly_reverse

// set P to Q(X^-1)*X^(degree(Q))
void mpfi_chebpoly_reverse(mpfi_chebpoly_t P, const mpfi_chebpoly_t Q);


/* norm 1 in the Chebyshev basis */

#define _mpfi_chebpoly_1norm _mpfi_poly_1norm

#define _mpfi_chebpoly_1norm_ubound _mpfi_poly_1norm_ubound

#define _mpfi_chebpoly_1norm_lbound _mpfi_poly_1norm_lbound

void mpfi_chebpoly_1norm(mpfi_t y, const mpfi_chebpoly_t P);

void mpfi_chebpoly_1norm_ubound(mpfr_t y, const mpfi_chebpoly_t P);

void mpfi_chebpoly_1norm_lbound(mpfr_t y, const mpfi_chebpoly_t P);


/* test functions */

#define mpfi_chebpoly_is_zero(P) ((P)->degree < 0)

#define mpfi_chebpoly_is_constant(P) ((P)->degree == 0)

#define mpfi_chebpoly_is_monic(P) (mpfr_cmp_si(&(((P)->coeffs + (P)->degree)->left), 1) == 0 && mpfr_cmp_si(&(((P)->coeffs + (P)->degree)->right), 1) == 0)

#define _mpfi_chebpoly_equal _mpfi_poly_equal

int mpfi_chebpoly_equal(const mpfi_chebpoly_t P, const mpfi_chebpoly_t Q);


/* evaluation */

void _mpfi_chebpoly_evaluate_si(mpfi_t y, mpfi_srcptr P, long n, long x);

// set y to P(x)
void mpfi_chebpoly_evaluate_si(mpfi_t y, const mpfi_chebpoly_t P, long x);

void _mpfi_chebpoly_evaluate_z(mpfi_t y, mpfi_srcptr P, long n, const mpz_t x);

// set y to P(x)
void mpfi_chebpoly_evaluate_z(mpfi_t y, const mpfi_chebpoly_t P, const mpz_t x);

void _mpfi_chebpoly_evaluate_q(mpfi_t y, mpfi_srcptr P, long n, const mpq_t x);

// set y to P(x)
void mpfi_chebpoly_evaluate_q(mpfi_t y, const mpfi_chebpoly_t P, const mpq_t x);

void _mpfi_chebpoly_evaluate_d(mpfi_t y, mpfi_srcptr P, long n, const double_t x);

// set y to P(x)
void mpfi_chebpoly_evaluate_d(mpfi_t y, const mpfi_chebpoly_t P, const double_t x);

void _mpfi_chebpoly_evaluate_fr(mpfi_t y, mpfi_srcptr P, long n, const mpfr_t x);

// set y to P(x)
void mpfi_chebpoly_evaluate_fr(mpfi_t y, const mpfi_chebpoly_t P, const mpfr_t x);

void _mpfi_chebpoly_evaluate_fi(mpfi_t y, mpfi_srcptr P, long n, const mpfi_t x);

// set y to P(x)
void mpfi_chebpoly_evaluate_fi(mpfi_t y, const mpfi_chebpoly_t P, const mpfi_t x);

// set y to P(-1)
void mpfi_chebpoly_evaluate_1(mpfi_t y, const mpfi_chebpoly_t P);

// set y to P(0)
void mpfi_chebpoly_evaluate0(mpfi_t y, const mpfi_chebpoly_t P);

// set y to P(1)
void mpfi_chebpoly_evaluate1(mpfi_t y, const mpfi_chebpoly_t P);


/* addition and substraction */

#define _mpfi_chebpoly_add _mpfi_poly_add

// set P := Q + R
void mpfi_chebpoly_add(mpfi_chebpoly_t P, const mpfi_chebpoly_t Q, const mpfi_chebpoly_t R);

#define _mpfi_chebpoly_sub _mpfi_poly_sub

// set P := Q - R
void mpfi_chebpoly_sub(mpfi_chebpoly_t P, const mpfi_chebpoly_t Q, const mpfi_chebpoly_t R);

#define _mpfi_chebpoly_neg _mpfi_poly_neg

// set P := -Q
void mpfi_chebpoly_neg(mpfi_chebpoly_t P, const mpfi_chebpoly_t Q);


/* scalar multiplication and division */

#define _mpfi_chebpoly_scalar_mul_si _mpfi_poly_scalar_mul_si

// set P := c*Q
void mpfi_chebpoly_scalar_mul_si(mpfi_chebpoly_t P, const mpfi_chebpoly_t Q, long c);

#define _mpfi_chebpoly_scalar_mul_z _mpfi_poly_scalar_mul_z

// set P := c*Q
void mpfi_chebpoly_scalar_mul_z(mpfi_chebpoly_t P, const mpfi_chebpoly_t Q, const mpz_t c);

#define _mpfi_chebpoly_scalar_mul_q _mpfi_poly_scalar_mul_q

// set P := c*Q
void mpfi_chebpoly_scalar_mul_q(mpfi_chebpoly_t P, const mpfi_chebpoly_t Q, const mpq_t c);

#define _mpfi_chebpoly_scalar_mul_d _mpfi_poly_scalar_mul_d

// set P := c*Q
void mpfi_chebpoly_scalar_mul_d(mpfi_chebpoly_t P, const mpfi_chebpoly_t Q, const double_t c);

#define _mpfi_chebpoly_scalar_mul_fr _mpfi_poly_scalar_mul_fr

// set P := c*Q
void mpfi_chebpoly_scalar_mul_fr(mpfi_chebpoly_t P, const mpfi_chebpoly_t Q, const mpfr_t c);

#define _mpfi_chebpoly_scalar_mul_fi _mpfi_poly_scalar_mul_fi

// set P := c*Q
void mpfi_chebpoly_scalar_mul_fi(mpfi_chebpoly_t P, const mpfi_chebpoly_t Q, const mpfi_t c);

#define _mpfi_chebpoly_scalar_mul_2si _mpfi_poly_scalar_mul_2si

// set P := 2^k*Q
void mpfi_chebpoly_scalar_mul_2si(mpfi_chebpoly_t P, const mpfi_chebpoly_t Q, long k);


#define _mpfi_chebpoly_scalar_div_si _mpfi_poly_scalar_div_si

// set P := (1/c)*Q
void mpfi_chebpoly_scalar_div_si(mpfi_chebpoly_t P, const mpfi_chebpoly_t Q, long c);

#define _mpfi_chebpoly_scalar_div_z _mpfi_poly_scalar_div_z

// set P := (1/c)*Q
void mpfi_chebpoly_scalar_div_z(mpfi_chebpoly_t P, const mpfi_chebpoly_t Q, const mpz_t c);

#define _mpfi_chebpoly_scalar_div_q _mpfi_poly_scalar_div_q

// set P := (1/c)*Q
void mpfi_chebpoly_scalar_div_q(mpfi_chebpoly_t P, const mpfi_chebpoly_t Q, const mpq_t c);

#define _mpfi_chebpoly_scalar_div_d _mpfi_poly_scalar_div_d

// set P := (1/c)*Q
void mpfi_chebpoly_scalar_div_d(mpfi_chebpoly_t P, const mpfi_chebpoly_t Q, const double_t c);

#define _mpfi_chebpoly_scalar_div_fr _mpfi_poly_scalar_div_fr

// set P := (1/c)*Q
void mpfi_chebpoly_scalar_div_fr(mpfi_chebpoly_t P, const mpfi_chebpoly_t Q, const mpfr_t c);

#define _mpfi_chebpoly_scalar_div_fi _mpfi_poly_scalar_div_fi

// set P := (1/c)*Q
void mpfi_chebpoly_scalar_div_fi(mpfi_chebpoly_t P, const mpfi_chebpoly_t Q, const mpfi_t c);

#define _mpfi_chebpoly_scalar_div_2si _mpfi_poly_scalar_div_2si

// set P := 2^-k*Q
void mpfi_chebpoly_scalar_div_2si(mpfi_chebpoly_t P, const mpfi_chebpoly_t Q, long k);




/* multiplication */

void _mpfi_chebpoly_mul(mpfi_ptr P, mpfi_srcptr Q, long n, mpfi_srcptr R, long m);

// set P := Q * R
void mpfi_chebpoly_mul(mpfi_chebpoly_t P, const mpfi_chebpoly_t Q, const mpfi_chebpoly_t R);


/* exponentiation */

// set P := Q^e
void mpfi_chebpoly_pow(mpfi_chebpoly_t P, const mpfi_chebpoly_t Q, unsigned long e);


/* composition */

// set P = Q(R)
void mpfi_chebpoly_comp(mpfi_chebpoly_t P, const mpfi_chebpoly_t Q, const mpfi_chebpoly_t R);


/* derivative and antiderivative */

void _mpfi_chebpoly_derivative(mpfi_ptr P, mpfi_srcptr Q, long n);

// set P := Q'
void mpfi_chebpoly_derivative(mpfi_chebpoly_t P, const mpfi_chebpoly_t Q);

void _mpfi_chebpoly_antiderivative(mpfi_ptr P, mpfi_srcptr Q, long n);

// set P st P' = Q and the 0-th coeff of P is 0
void mpfi_chebpoly_antiderivative(mpfi_chebpoly_t P, const mpfi_chebpoly_t Q);

// set P st P' = Q and P(0) = 0
void mpfi_chebpoly_antiderivative0(mpfi_chebpoly_t P, const mpfi_chebpoly_t Q);

// set P st P' = Q and P(-1) = 0
void mpfi_chebpoly_antiderivative_1(mpfi_chebpoly_t P, const mpfi_chebpoly_t Q);

// set P st P' = Q and P(1) = 0
void mpfi_chebpoly_antiderivative1(mpfi_chebpoly_t P, const mpfi_chebpoly_t Q);


/* validation functions for approximate inverse computations on mpfr_chebpoly */

// compute an upper bound of ||F-Q^-1||_1
// one must provide a witness G st ||1-GQ||_1 < 1
void mpfr_chebpoly_inverse_validate(mpfr_t bound, const mpfr_chebpoly_t F, const mpfr_chebpoly_t Q, const mpfr_chebpoly_t G);

// compute an upper bound of ||F-Q^-1||_1 provided that ||1-FQ||_1 < 1
void mpfr_chebpoly_inverse_validate2(mpfr_t bound, const mpfr_chebpoly_t F, const mpfr_chebpoly_t Q);

// compute an upper bound of ||F - P/Q||_1
// one must provide a witness G st ||1-GQ||_1 < 1
void mpfr_chebpoly_fract_validate(mpfr_t bound, const mpfr_chebpoly_t F, const mpfr_chebpoly_t P, const mpfr_chebpoly_t Q, const mpfr_chebpoly_t G);


/* validation functions for approximate square root computations on mpfr_chebpoly */

// compute an upper bound of ||F-sqrt(G)||_1
// one must provide a witness F0 st ||1-F0*F||_1 < 1
void mpfr_chebpoly_sqrt_validate(mpfr_t bound, const mpfr_chebpoly_t F, const mpfr_chebpoly_t F0, const mpfr_chebpoly_t G);

// compute an upper bound of ||F-1/sqrt(G)||_1
// one must provide a witness F0 st ||1-F0*G*F||_1 < 1
void mpfr_chebpoly_inv_sqrt_validate(mpfr_t bound, const mpfr_chebpoly_t F, const mpfr_chebpoly_t F0, const mpfr_chebpoly_t G);

// same as mpfr_chebpoly_inv_sqrt_validate but uses F0 = F
void mpfr_chebpoly_inv_sqrt_validate2(mpfr_t bound, const mpfr_chebpoly_t F, const mpfr_chebpoly_t G);


/* conversion to and from the monomial basis */

void _mpfi_chebpoly_get_mpfi_poly(mpfi_ptr P, mpfi_srcptr Q, long n);

// set in P the conversion of Q (in the Chebyshev basis) into the monomial basis
void mpfi_chebpoly_get_mpfi_poly(mpfi_poly_t P, const mpfi_chebpoly_t Q);

void _mpfi_chebpoly_set_mpfi_poly(mpfi_ptr P, mpfi_srcptr Q, long n);

// set in P the conversion of Q (in the monomial basis) into the Chebyshev basis
void mpfi_chebpoly_set_mpfi_poly(mpfi_chebpoly_t P, const mpfi_poly_t Q);




/* display function */


void _mpfi_chebpoly_print(mpfi_srcptr P, long n, const char * var, size_t digits);

// display the polynomial P with 'var' as indeterminate symbol
// 'var' is a 0-terminated string
void mpfi_chebpoly_print(const mpfi_chebpoly_t P, const char * var, size_t digits);




#endif
