
/*
This file is part of the TchebyApprox library.

The type [mpfi_bandvec_t] implements ([Hwidth],[Dwidth])
almost-banded vectors of [mpfi_t] around index [ind].
The dimension (finite or infinite) of the underlying vector space
is not specified.
Non-zero coefficients are located:
  - between indicies from 0 to Hwidth-1 (initial coefficients):
    H[0], ... , H[Hwidth-1]
  - between indicies from max(ind-Dwidth,0) to ind+Dwidth (diagonal coefficients):
    D[0], ..., D[Dwidth], ... , D[2*Dwidth]
*/


#ifndef MPFI_BANDVEC_H
#define MPFI_BANDVEC_H

#include <stdio.h>
#include <stdlib.h>

#include <mpfr.h>
#include <mpfi.h>
#include <mpfi_io.h>

#include "mpfi_vec.h"
#include "mpfr_bandvec.h"


typedef struct mpfi_bandvec_struct {
  long Hwidth;
  long Dwidth;
  long ind;
  mpfi_ptr H;
  mpfi_ptr D;
} mpfi_bandvec_struct;

typedef mpfi_bandvec_struct mpfi_bandvec_t[1];
typedef mpfi_bandvec_struct * mpfi_bandvec_ptr;
typedef const mpfi_bandvec_struct * mpfi_bandvec_srcptr;


/* memory management */


// init with a zero vector (Hwidth = 0, Dwidth = 0, D[0] = 0)
void mpfi_bandvec_init(mpfi_bandvec_t V);

// clear V
void mpfi_bandvec_clear(mpfi_bandvec_t V);

// set parameters for V
void mpfi_bandvec_set_params(mpfi_bandvec_t V, long Hwidth, long Dwidth, long ind);


/* basic manipulations and assignments */

void _mpfi_bandvec_set(mpfi_ptr VH, mpfi_ptr VD, long VHwidth, long VDwidth, mpfi_srcptr WH, mpfi_srcptr WD, long WHwidth, long WDwidth, long ind);

void mpfi_bandvec_set(mpfi_bandvec_t V, const mpfi_bandvec_t W);

void _mpfi_bandvec_set_mpfr_bandvec(mpfi_ptr VH, mpfi_ptr VD, long VHwidth, long VDwidth, mpfr_srcptr WH, mpfr_srcptr WD, long WHwidth, long WDwidth, long ind);

void mpfi_bandvec_set_mpfr_bandvec(mpfi_bandvec_t V, const mpfr_bandvec_t W);

void _mpfi_bandvec_get_mpfr_bandvec(mpfr_ptr VH, mpfr_ptr VD, long VHwidth, long VDwidth, mpfi_srcptr WH, mpfi_srcptr WD, long WHwidth, long WDwidth, long ind);

void mpfi_bandvec_get_mpfr_bandvec(mpfr_bandvec_t V, const mpfi_bandvec_t W);

void mpfi_bandvec_swap(mpfi_bandvec_t V, mpfi_bandvec_t W);

void _mpfi_bandvec_zero(mpfi_ptr VH, mpfi_ptr VD, long Hwidth, long Dwidth);

void mpfi_bandvec_zero(mpfi_bandvec_t V);

// normalise the H and D components if they overlap

void _mpfi_bandvec_normalise(mpfi_ptr VH, mpfi_ptr VD, long VHwidth, long VDwidth, long ind);

void mpfi_bandvec_normalise(mpfi_bandvec_t V);

// set_coeff: set the n-th coeff of V (or (VH,VD,Hwidth,Dwidth,ind)) to c

void _mpfi_bandvec_set_coeff_si(mpfi_ptr VH, mpfi_ptr VD, long Hwidth, long Dwidth, long ind, long n, long c);

void mpfi_bandvec_set_coeff_si(mpfi_bandvec_t V, long n, long c);

void _mpfi_bandvec_set_coeff_z(mpfi_ptr VH, mpfi_ptr VD, long Hwidth, long Dwidth, long ind, long n, const mpz_t c);

void mpfi_bandvec_set_coeff_z(mpfi_bandvec_t V, long n, const mpz_t c);

void _mpfi_bandvec_set_coeff_q(mpfi_ptr VH, mpfi_ptr VD, long Hwidth, long Dwidth, long ind, long n, const mpq_t c);

void mpfi_bandvec_set_coeff_q(mpfi_bandvec_t V, long n, const mpq_t c);

void _mpfi_bandvec_set_coeff_fr(mpfi_ptr VH, mpfi_ptr VD, long Hwidth, long Dwidth, long ind, long n, const mpfr_t c);

void mpfi_bandvec_set_coeff_fr(mpfi_bandvec_t V, long n, const mpfr_t c);

void _mpfi_bandvec_set_coeff_fi(mpfi_ptr VH, mpfi_ptr VD, long Hwidth, long Dwidth, long ind, long n, const mpfi_t c);

void mpfi_bandvec_set_coeff_fi(mpfi_bandvec_t V, long n, const mpfi_t c);

void _mpfi_bandvec_get_mpfi_vec(mpfi_ptr V, long length, mpfi_srcptr WH, mpfi_srcptr WD, long WHwidth, long WDwidth, long ind);

// convert a bandvec into a vec, by using the length of V
void mpfi_bandvec_get_mpfi_vec(mpfi_vec_t V, const mpfi_bandvec_t W);



/* norm 1 */

void _mpfi_bandvec_1norm_ubound(mpfr_t norm, mpfi_srcptr VH, mpfi_srcptr VD, long Hwidth, long Dwidth, long ind);

void mpfi_bandvec_1norm_ubound(mpfr_t norm, const mpfi_bandvec_t V);

void _mpfi_bandvec_1norm_lbound(mpfr_t norm, mpfi_srcptr VH, mpfi_srcptr VD, long Hwidth, long Dwidth, long ind);

void mpfi_bandvec_1norm_lbound(mpfr_t norm, const mpfi_bandvec_t V);

void _mpfi_bandvec_1norm_fi(mpfi_t norm, mpfi_srcptr VH, mpfi_srcptr VD, long Hwidth, long Dwidth, long ind);

void mpfi_bandvec_1norm_fi(mpfi_t norm, const mpfi_bandvec_t V);


/* addition and substraction */
// W and Z must have the same ind, but not necessarily the same Hiwdht and Dwidth

// for add and sub, one must have VHwidth >= max(WHwidth,ZHwidth) and VDwidth >= max(WDwidth,ZDwidth)

void _mpfi_bandvec_add(mpfi_ptr VH, mpfi_ptr VD, long VHwidth, long VDwidth, mpfi_srcptr WH, mpfi_srcptr WD, long WHwidth, long WDwidth, mpfi_srcptr ZH, mpfi_srcptr ZD, long ZHwidth, long ZDwidth, long ind);

void mpfi_bandvec_add(mpfi_bandvec_t V, const mpfi_bandvec_t W, const mpfi_bandvec_t Z);


void _mpfi_bandvec_sub(mpfi_ptr VH, mpfi_ptr VD, long VHwidth, long VDwidth, mpfi_srcptr WH, mpfi_srcptr WD, long WHwidth, long WDwidth, mpfi_srcptr ZH, mpfi_srcptr ZD, long ZHwidth, long ZDwidth, long ind);

void mpfi_bandvec_sub(mpfi_bandvec_t V, const mpfi_bandvec_t W, const mpfi_bandvec_t Z);

// for neg, one must have VHwidth >= WHwidth and VDwidth >= WDwidth

void _mpfi_bandvec_neg(mpfi_ptr VH, mpfi_ptr VD, long VHwidth, long VDwidth, mpfi_srcptr WH, mpfi_srcptr WD, long WHwidth, long WDwidth, long ind);

void mpfi_bandvec_neg(mpfi_bandvec_t V, const mpfi_bandvec_t W);


/* scalar multiplication and division */

void _mpfi_bandvec_scalar_mul_si(mpfi_ptr VH, mpfi_ptr VD, long VHwidth, long Vdwidth, mpfi_srcptr WH, mpfi_srcptr WD, long WHwidth, long WDwidth, long ind, long c);

void mpfi_bandvec_scalar_mul_si(mpfi_bandvec_t V, const mpfi_bandvec_t W, long c); 

void _mpfi_bandvec_scalar_mul_z(mpfi_ptr VH, mpfi_ptr VD, long VHwidth, long Vdwidth, mpfi_srcptr WH, mpfi_srcptr WD, long WHwidth, long WDwidth, long ind, const mpz_t c);

void mpfi_bandvec_scalar_mul_z(mpfi_bandvec_t V, const mpfi_bandvec_t W, const mpz_t c);

void _mpfi_bandvec_scalar_mul_q(mpfi_ptr VH, mpfi_ptr VD, long VHwidth, long Vdwidth, mpfi_srcptr WH, mpfi_srcptr WD, long WHwidth, long WDwidth, long ind, const mpq_t c);

void mpfi_bandvec_scalar_mul_q(mpfi_bandvec_t V, const mpfi_bandvec_t W, const mpq_t c);

void _mpfi_bandvec_scalar_mul_fr(mpfi_ptr VH, mpfi_ptr VD, long VHwidth, long Vdwidth, mpfi_srcptr WH, mpfi_srcptr WD, long WHwidth, long WDwidth, long ind, const mpfr_t c);

void mpfi_bandvec_scalar_mul_fr(mpfi_bandvec_t V, const mpfi_bandvec_t W, const mpfr_t c);

void _mpfi_bandvec_scalar_mul_fi(mpfi_ptr VH, mpfi_ptr VD, long VHwidth, long VDwidth, mpfi_srcptr WH, mpfi_srcptr WD, long WHwidth, long WDwidth, long ind, const mpfi_t c);

void mpfi_bandvec_scalar_mul_fi(mpfi_bandvec_t V, const mpfi_bandvec_t W, const mpfi_t c);

void _mpfi_bandvec_scalar_mul_2si(mpfi_ptr VH, mpfi_ptr VD, long VHwidth, long VDwidth, mpfi_srcptr WH, mpfi_srcptr WD, long WHwidth, long WDwidth, long ind, long k);

void mpfi_bandvec_scalar_mul_2si(mpfi_bandvec_t V, const mpfi_bandvec_t W, long k);


void _mpfi_bandvec_scalar_div_si(mpfi_ptr VH, mpfi_ptr VD, long VHwidth, long Vdwidth, mpfi_srcptr WH, mpfi_srcptr WD, long WHwidth, long WDwidth, long ind, long c);

void mpfi_bandvec_scalar_div_si(mpfi_bandvec_t V, const mpfi_bandvec_t W, long c); 

void _mpfi_bandvec_scalar_div_z(mpfi_ptr VH, mpfi_ptr VD, long VHwidth, long Vdwidth, mpfi_srcptr WH, mpfi_srcptr WD, long WHwidth, long WDwidth, long ind, const mpz_t c);

void mpfi_bandvec_scalar_div_z(mpfi_bandvec_t V, const mpfi_bandvec_t W, const mpz_t c);

void _mpfi_bandvec_scalar_div_q(mpfi_ptr VH, mpfi_ptr VD, long VHwidth, long Vdwidth, mpfi_srcptr WH, mpfi_srcptr WD, long WHwidth, long WDwidth, long ind, const mpq_t c);

void mpfi_bandvec_scalar_div_q(mpfi_bandvec_t V, const mpfi_bandvec_t W, const mpq_t c);

void _mpfi_bandvec_scalar_div_fr(mpfi_ptr VH, mpfi_ptr VD, long VHwidth, long Vdwidth, mpfi_srcptr WH, mpfi_srcptr WD, long WHwidth, long WDwidth, long ind, const mpfr_t c);

void mpfi_bandvec_scalar_div_fr(mpfi_bandvec_t V, const mpfi_bandvec_t W, const mpfr_t c);

void _mpfi_bandvec_scalar_div_fi(mpfi_ptr VH, mpfi_ptr VD, long VHwidth, long VDwidth, mpfi_srcptr WH, mpfi_srcptr WD, long WHwidth, long WDwidth, long ind, const mpfi_t c);

void mpfi_bandvec_scalar_div_fi(mpfi_bandvec_t V, const mpfi_bandvec_t W, const mpfi_t c);

void _mpfi_bandvec_scalar_div_2si(mpfi_ptr VH, mpfi_ptr VD, long VHwidth, long VDwidth, mpfi_srcptr WH, mpfi_srcptr WD, long WHwidth, long WDwidth, long ind, long k);

void mpfi_bandvec_scalar_div_2si(mpfi_bandvec_t V, const mpfi_bandvec_t W, long k);


/* display function */


void _mpfi_bandvec_print(mpfi_srcptr VH, mpfi_srcptr VD, long VHwidth, long VDwidth, long ind, size_t digits);

// display the vector V
void mpfi_bandvec_print(const mpfi_bandvec_t V, size_t digits);


#endif
