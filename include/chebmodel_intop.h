
/*
This file is part of the TchebyApprox library.

The type [chebmodel_chebpoly_intop_t] represents a (scalar)
linear Volterra integral operator of the second kind
of order [order] derived from a [chebmodel_chebpoly_lode_t].
The new unknown of this intagral equation is the last derivative
of the unknown in the differential equation.
kernel k(t,s) = alpha0(t)*T0(s) + ... + alpha[order-1](t)*T[order-1](s)
*/


#ifndef CHEBMODEL_INTOP_H
#define CHEBMODEL_INTOP_H


#include <stdio.h>
#include <stdlib.h>

#include <gmp.h>
#include <mpfr.h>
#include <mpfi.h>
#include <mpfi_io.h>

#include "double_operations.h"

#include "mpfi_bandvec.h"
#include "mpfi_poly.h"
#include "mpfi_chebpoly.h"
#include "chebmodel.h"

#include "chebmodel_lode.h"
#include "double_chebpoly_intop.h"
#include "mpfr_chebpoly_intop.h"
#include "mpfi_chebpoly_intop.h"

typedef struct {
  long order;
  chebmodel_ptr alpha;
} chebmodel_intop_struct;

typedef chebmodel_intop_struct chebmodel_intop_t[1];

typedef chebmodel_intop_struct * chebmodel_intop_ptr;

typedef const chebmodel_intop_struct * chebmodel_intop_srcptr;




void chebmodel_intop_init(chebmodel_intop_t K);

void chebmodel_intop_clear(chebmodel_intop_t K);

void chebmodel_intop_set(chebmodel_intop_t K, const chebmodel_intop_t N);

void chebmodel_intop_set_double_chebpoly_intop(chebmodel_intop_t K, const double_chebpoly_intop_t N);

void chebmodel_intop_set_mpfr_chebpoly_intop(chebmodel_intop_t K, const mpfr_chebpoly_intop_t N);

void chebmodel_intop_set_mpfi_chebpoly_intop(chebmodel_intop_t K, const mpfi_chebpoly_intop_t N);

void chebmodel_intop_get_double_chebpoly_intop(double_chebpoly_intop_t K, const chebmodel_intop_t N);

void chebmodel_intop_get_mpfr_chebpoly_intop(mpfr_chebpoly_intop_t K, const chebmodel_intop_t N);

void chebmodel_intop_get_mpfi_chebpoly_intop(mpfi_chebpoly_intop_t K, const chebmodel_intop_t N);

void chebmodel_intop_swap(chebmodel_intop_t K, chebmodel_intop_t N);

void chebmodel_intop_set_order(chebmodel_intop_t K, long order);

long chebmodel_intop_Hwidth(const chebmodel_intop_t K);

long chebmodel_intop_Dwidth(const chebmodel_intop_t K);

void chebmodel_intop_set_lode(chebmodel_intop_t K, const chebmodel_lode_t L);

void chebmodel_intop_initvals(chebmodel_t G, const chebmodel_lode_t L, mpfi_srcptr I);

void chebmodel_intop_rhs(chebmodel_t G, const chebmodel_lode_t L, const chebmodel_t g, mpfi_srcptr I);


// evaluate on a polynomial ; use integration from -1

void chebmodel_intop_evaluate_mpfr_chebpoly(chebmodel_t P, const chebmodel_intop_t K, const mpfr_chebpoly_t Q);

void chebmodel_intop_evaluate_mpfi_chebpoly(chebmodel_t P, const chebmodel_intop_t K, const mpfi_chebpoly_t Q);

void chebmodel_intop_evaluate_chebmodel(chebmodel_t P, const chebmodel_intop_t K, const chebmodel_t Q);


/* print */

void chebmodel_intop_print(const chebmodel_intop_t K, const char * Cheb_var, size_t digits);


#endif
