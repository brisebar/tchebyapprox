
#include "chebmodel_lode.h"


void chebmodel_lode_swap(chebmodel_lode_t L, chebmodel_lode_t M)
{
  long order_buf = L->order;
  chebmodel_ptr a_buf = L->a;

  L->order = M->order;
  L->a = M->a;

  M->order = order_buf;
  M->a = a_buf;
}
