
#include "chebmodel_lode.h"


void chebmodel_lode_clear(chebmodel_lode_t L)
{
  long r = L->order;
  long i;
  for (i = 0 ; i < r ; i++)
    chebmodel_clear(L->a + i);
  free(L->a);
}
