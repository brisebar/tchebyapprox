
#include "chebmodel_lode.h"


void chebmodel_lode_set_order(chebmodel_lode_t L, long order)
{
  long r = L->order;
  L->order = order;
  long i;

  if (order < r) {
    for (i = order ; i < r ; i++)
      chebmodel_clear(L->a + i);
    L->a = realloc(L->a, order * sizeof(chebmodel_t));
  }

  else if (order > r) {
    L->a = realloc(L->a, order * sizeof(chebmodel_t));
    for (i = r ; i < order ; i++)
      chebmodel_init(L->a + i);
  }

  else
    return;
}
