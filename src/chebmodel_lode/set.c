
#include "chebmodel_lode.h"


void chebmodel_lode_set(chebmodel_lode_t L, const chebmodel_lode_t M)
{
  long r = M->order;
  chebmodel_lode_set_order(L, r);

  long i;
  for (i = 0 ; i < r ; i++)
    chebmodel_set(L->a + i, M->a + i);
}


void chebmodel_lode_set_mpfr_chebpoly_lode(chebmodel_lode_t L, const mpfr_chebpoly_lode_t M)
{
  long r = M->order;
  chebmodel_lode_set_order(L, r);

  long i;
  for (i = 0 ; i < r ; i++)
    chebmodel_set_mpfr_chebpoly(L->a + i, M->a + i);
}


void chebmodel_lode_set_mpfi_chebpoly_lode(chebmodel_lode_t L, const mpfi_chebpoly_lode_t M)
{
  long r = M->order;
  chebmodel_lode_set_order(L, r);

  long i;
  for (i = 0 ; i < r ; i++)
    chebmodel_set_mpfi_chebpoly(L->a + i, M->a + i);
}


void chebmodel_lode_get_mpfr_chebpoly_lode(mpfr_chebpoly_lode_t L, const chebmodel_lode_t M)
{
  long r = M->order;
  mpfr_chebpoly_lode_set_order(L, r);

  long i;
  for (i = 0 ; i < r ; i++)
    mpfi_chebpoly_get_mpfr_chebpoly(L->a + i, M->a[i].poly);
}


void chebmodel_lode_get_mpfi_chebpoly_lode(mpfi_chebpoly_lode_t L, const chebmodel_lode_t M)
{
  long r = M->order;
  mpfi_chebpoly_lode_set_order(L, r);

  long i;
  for (i = 0 ; i < r ; i++)
    mpfi_chebpoly_set(L->a + i, M->a[i].poly);
}



