
#include "double_vec.h"


void _double_vec_sub(double_ptr V, double_srcptr W, double_srcptr Z, long n)
{
  long i;
  for (i = 0 ; i < n ; i++)
    double_sub(V + i, W + i, Z + i, MPFR_RNDN);
}


// set V := W + R
void double_vec_sub(double_vec_t V, const double_vec_t W, const double_vec_t Z)
{
  long n = W->length;
  if (n != Z->length) {
    fprintf(stderr, "double_vec_sub: error: incompatible lengths\n");
    return;
  }
  double_vec_set_length(V, n);
  _double_vec_sub(V->coeffs, W->coeffs, Z->coeffs, n);
}

