
#include "double_vec.h"


void _double_vec_neg(double_ptr V, double_srcptr W, long n)
{
  long i;
  for (i = 0 ; i < n ; i++)
    double_neg(V + i, W + i, MPFR_RNDN);
}


// set V := -W
void double_vec_neg(double_vec_t V, const double_vec_t W)
{
  double_vec_set_length(V, W->length);
  _double_vec_neg(V->coeffs, W->coeffs, W->length);
}
