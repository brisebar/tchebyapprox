
#include "double_vec.h"


// set n as length for V
void double_vec_set_length(double_vec_t V, long n)
{
  long m = V->length;
  long i;

  if (n < m) {
    for (i = n ; i < m ; i++)
      double_clear(V->coeffs + i);
    V->coeffs = realloc(V->coeffs, n * sizeof(double_t));
  }
  else if (n > m) {
    V->coeffs = realloc(V->coeffs, n * sizeof(double_t));
    for (i = m ; i < n ; i++) {
      double_init(V->coeffs + i);
      double_set_si(V->coeffs + i, 0, MPFR_RNDN);
    }
  }

  V->length = n;
}
