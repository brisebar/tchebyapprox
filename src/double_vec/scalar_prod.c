
#include "double_vec.h"


void _double_vec_scalar_prod(double_t prod, double_srcptr V, double_srcptr W, long n)
{
  double_set_zero(prod, 1);
  double_t temp;
  double_init(temp);
  long i;
  for (i = 0 ; i < n ; i++) {
    double_mul(temp, V + i, W + i, MPFR_RNDN);
    double_add(prod, prod, temp, MPFR_RNDN);
  }
  double_clear(temp);
}


void double_vec_scalar_prod(double_t prod, const double_vec_t V, const double_vec_t W)
{
  if (V->length != W->length)
    fprintf(stderr, "double_vec_scalar_prod_fr: error: incompatible lengths\n");
  else
    _double_vec_scalar_prod(prod, V->coeffs, W->coeffs, V->length);
}

void _double_vec_scalar_prod_fi(mpfi_t prod, double_srcptr V, double_srcptr W, long n)
{
  mpfi_set_si(prod, 0);
  mpfi_t temp;
  mpfi_init(temp);
  long i;
  for (i = 0 ; i < n ; i++) {
    mpfi_set_d(temp, V[i]);
    mpfi_mul_d(temp, temp, W[i]);
    mpfi_add(prod, prod, temp);
  }
  mpfi_clear(temp);
} 


void double_vec_scalar_prod_fi(mpfi_t prod, const double_vec_t V, const double_vec_t W)
{
  if (V->length != W->length)
    fprintf(stderr, "double_vec_scalar_prod_fi: error: incompatible lengths\n");
  else
    _double_vec_scalar_prod_fi(prod, V->coeffs, W->coeffs, V->length);
}
