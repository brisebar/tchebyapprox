
#include "double_vec.h"


void _double_vec_1norm(double_t y, double_srcptr V, long n)
{
  long i;
  double_set_si(y, 0, MPFR_RNDN);
  for (i = 0 ; i <= n ; i++)
    if (double_sgn(V + i) >= 0)
      double_add(y, y, V + i, MPFR_RNDN);
    else
      double_sub(y, y, V + i, MPFR_RNDN);
}


void double_vec_1norm(double_t y, const double_vec_t V)
{
  _double_vec_1norm(y, V->coeffs, V->length);
}


void _double_vec_1norm_ubound(double_t y, double_srcptr V, long n)
{
  mpfr_t norm;
  mpfr_init(norm);
  mpfr_set_si(norm, 0, MPFR_RNDU);
  long i;
  for (i = 0 ; i <= n ; i++)
    if (double_sgn(V + i) >= 0)
      mpfr_add_d(norm, norm, V[i], MPFR_RNDU);
    else
      mpfr_sub_d(norm, norm, V[i], MPFR_RNDU);

  double_set_fr(y, norm, MPFR_RNDU);
  mpfr_clear(norm);
}

void double_vec_1norm_ubound(double_t y, const double_vec_t V)
{
  _double_vec_1norm_ubound(y, V->coeffs, V->length);
}

void _double_vec_1norm_lbound(double_t y, double_srcptr V, long n)
{
  mpfr_t norm;
  mpfr_init(norm);
  mpfr_set_si(norm, 0, MPFR_RNDD);
  long i;
  for (i = 0 ; i <= n ; i++)
    if (double_sgn(V + i) >= 0)
      mpfr_add_d(norm, norm, V[i], MPFR_RNDD);
    else
      mpfr_sub_d(norm, norm, V[i], MPFR_RNDD);

  double_set_fr(y, norm, MPFR_RNDD);
  mpfr_clear(norm);
}

void double_vec_1norm_lbound(double_t y, const double_vec_t V)
{
  _double_vec_1norm_lbound(y, V->coeffs, V->length);
}


void _double_vec_1norm_fi(mpfi_t y, double_srcptr V, long n)
{
  mpfi_set_si(y, 0);
  long i;
  for (i = 0 ; i <= n ; i++)
    if (double_sgn(V + i) >= 0)
      mpfi_add_d(y, y, V[i]);
    else
      mpfi_sub_d(y, y, V[i]);
}

void double_vec_1norm_fi(mpfi_t y, const double_vec_t V)
{
  _double_vec_1norm_fi(y, V->coeffs, V->length);
}


