
#include "double_vec.h"


void _double_vec_shift_left(double_ptr V, double_srcptr W, long n, unsigned long k)
{
  long i;
  for (i = n-1 ; i >= 0 ; i--)
    double_set(V + i + k, W + i, MPFR_RNDN);
  for (i = 0 ; i < k ; i++)
    double_set_zero(V + i, 1);
}


void double_vec_shift_left(double_vec_t V, const double_vec_t W, unsigned long k)
{
  long i;
  long n = W->length;
  double_vec_set_length(V, n+k);
  _double_vec_shift_left(V->coeffs, W->coeffs, n, k);
}


void _double_vec_shift_right(double_ptr V, double_srcptr W, long n, unsigned long k)
{
  long i;
  for (i = 0 ; i+k < n ; i++)
    double_set(V + i, W + i + k, MPFR_RNDN);
}


void double_vec_shift_right(double_vec_t V, const double_vec_t W, unsigned long k)
{
  long i;
  long n = W->length;
  if (k >= n)
    double_vec_set_length(V, 0);
  else {
    double_ptr Vcoeffs = malloc((n-k) * sizeof(double_t));
    for (i = 0 ; i < n-k ; i++)
      double_init(Vcoeffs + i);
    _double_vec_shift_right(Vcoeffs, W->coeffs, n, k);
    double_vec_clear(V);
    V->length = n-k;
    V->coeffs = Vcoeffs;
  }
}
