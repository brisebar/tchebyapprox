
#include "double_vec.h"


int _double_vec_is_zero(double_srcptr V, long n)
{
  long i;
  for (i = 0 ; i < n ; i++)
    if (double_cmp_si(V + i, 0) != 0)
      break;

  return i == n;
}


int double_vec_is_zero(const double_vec_t V)
{
  return _double_vec_is_zero(V->coeffs, V->length);
}

