
#include "double_vec.h"


void _double_vec_scalar_div_si(double_ptr V, double_srcptr W, long n, long c)
{
  double d = c;
  _double_vec_scalar_div_d(V, W, n, &d);
}


void _double_vec_scalar_div_z(double_ptr V, double_srcptr W, long n, const mpz_t c)
{
  double d = mpz_get_d(c);
  _double_vec_scalar_div_d(V, W, n, &d);
}


void _double_vec_scalar_div_q(double_ptr V, double_srcptr W, long n, const mpq_t c)
{
  double d = mpq_get_d(c);
  _double_vec_scalar_div_d(V, W, n, &d);
}


void _double_vec_scalar_div_d(double_ptr V, double_srcptr W, long n, const double_t c)
{
  long i;
  for (i = 0 ; i < n ; i++)
    double_div(V + i, W + i, c, MPFR_RNDN);
}


void _double_vec_scalar_div_fr(double_ptr V, double_srcptr W, long n, const mpfr_t c)
{
  double d = mpfr_get_d(c, MPFR_RNDN);
  _double_vec_scalar_div_d(V, W, n, &d);

}


void _double_vec_scalar_div_2si(double_ptr V, double_srcptr W, long n, long k)
{
  long i;
  for (i = 0 ; i < n ; i++)
    double_mul_2si(V + i, W + i, -k, MPFR_RNDN);
}



// set V := (1/c)*W
void double_vec_scalar_div_si(double_vec_t V, const double_vec_t W, long c)
{
  long n = W->length;
  double_vec_set_length(V, n);
  _double_vec_scalar_div_si(V->coeffs, W->coeffs, n, c);
}


// set V := (1/c)*W
void double_vec_scalar_div_z(double_vec_t V, const double_vec_t W, const mpz_t c)
{
  long n = W->length;
  double_vec_set_length(V, n);
  _double_vec_scalar_div_z(V->coeffs, W->coeffs, n, c);
}


// set V := (1/c)*W
void double_vec_scalar_div_q(double_vec_t V, const double_vec_t W, const mpq_t c)
{
  long n = W->length;
  double_vec_set_length(V, n);
  _double_vec_scalar_div_q(V->coeffs, W->coeffs, n, c);
}


// set V := (1/c)*W
void double_vec_scalar_div_d(double_vec_t V, const double_vec_t W, const double_t c)
{
  long n = W->length;
  double_vec_set_length(V, n);
  _double_vec_scalar_div_d(V->coeffs, W->coeffs, n, c);
}


// set V := (1/c)*W
void double_vec_scalar_div_fr(double_vec_t V, const double_vec_t W, const mpfr_t c)
{
  long n = W->length;
  double_vec_set_length(V, n);
  _double_vec_scalar_div_fr(V->coeffs, W->coeffs, n, c);
}


// set V := (1/2^k)*W
void double_vec_scalar_div_2si(double_vec_t V, const double_vec_t W, long k)
{
  long n = W->length;
  double_vec_set_length(V, n);
  _double_vec_scalar_div_2si(V->coeffs, W->coeffs, n, k);
}
