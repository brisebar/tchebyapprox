
#include "double_vec.h"


// set the n-th coefficient of V to c
void double_vec_set_coeff_si(double_vec_t V, long n, long c)
{
  if (n < 0 || n >= V->length)
    return;

  else
    double_set_si(V->coeffs + n, c, MPFR_RNDN);
}


// set the n-th coefficient of V to c
void double_vec_set_coeff_z(double_vec_t V, long n, const mpz_t c)
{
  if (n < 0 || n >= V->length)
    return;

  else
    double_set_z(V->coeffs + n, c, MPFR_RNDN);
}


// set the n-th coefficient of V to c
void double_vec_set_coeff_q(double_vec_t V, long n, const mpq_t c)
{
  if (n < 0 || n >= V->length)
    return;

  else
    double_set_q(V->coeffs + n, c, MPFR_RNDN);
} 

// set the n-th coefficient of V to c
void double_vec_set_coeff_d(double_vec_t V, long n, const double_t c)
{
  if (n < 0 || n >= V->length)
    return;

  else
    double_set(V->coeffs + n, c, MPFR_RNDN);
}

// set the n-th coefficient of V to c
void double_vec_set_coeff_fr(double_vec_t V, long n, const mpfr_t c)
{
  if (n < 0 || n >= V->length)
    return;

  else
    double_set_fr(V->coeffs + n, c, MPFR_RNDN);
}

