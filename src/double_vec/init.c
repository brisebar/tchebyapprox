
#include "double_vec.h"


// init a vector of n double_t
void _double_vec_init(double_ptr V, long n)
{
  long i;
  for (i = 0 ; i < n ; i++)
    double_init(V + i);
}


// init with V = 0
void double_vec_init(double_vec_t V)
{
  V->length = 0;
  V->coeffs = malloc(0);
}


// init V as a zero-vector of size n
void double_vec_init_zero(double_vec_t V, long n)
{
  V->length = n;
  V->coeffs = malloc(n * sizeof(double_t));
  long i;
  for (i = 0 ; i < n ; i++) {
    double_init(V->coeffs + i);
    double_set_zero(V->coeffs + i, 1);
  }
}
