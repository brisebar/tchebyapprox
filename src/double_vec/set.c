
#include "double_vec.h"


void _double_vec_set(double_ptr V, double_srcptr W, long n)
{
  long i ;
  for (i = 0 ; i < n ; i++)
    double_set(V + i, W + i, MPFR_RNDN);
}


// copy W into V
void double_vec_set(double_vec_t V, const double_vec_t W)
{
  long n = W->length;
  long i;
  if (V != W) {
    double_vec_set_length(V, n);
    for (i = 0 ; i < n ; i++)
      double_set(V->coeffs + i, W->coeffs + i, MPFR_RNDN);
  }
}
