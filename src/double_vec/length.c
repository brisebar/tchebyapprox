
#include "double_vec.h"


// get the degree of V
long double_vec_degree(const double_vec_t V)
{
  return V->length;
}
