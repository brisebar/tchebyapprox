
#include "double_vec.h"


// reverse a list of double of size (n+1)
void _double_vec_reverse(double_ptr V, double_srcptr W, long n)
{
  long i;
  double_ptr R = malloc(n * sizeof(double_t));
  for (i = 0 ; i < n ; i++) {
    double_init(R + i);
    double_set(R + i, W + n-1-i, MPFR_RNDN);
  }
  for (i = 0 ; i < n ; i++) {
    double_swap(V + i, R + i);
    double_clear(R + i);
  }
}


void double_vec_reverse(double_vec_t V, const double_vec_t W)
{
  if (V != W)
    double_vec_set(V, W);
  _double_vec_reverse(V->coeffs, V->coeffs, V->length);
}
