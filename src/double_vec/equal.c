
#include "double_vec.h"


int _double_vec_equal(double_srcptr V, double_srcptr W, long n)
{
  long i;
  for (i = 0 ; i < n ; i++)
    if (!double_equal_p(V + i, W + i))
      break;
  return i == n;
}


int double_vec_equal(const double_vec_t V, const double_vec_t W)
{
  return (V->length == W->length && _double_vec_equal(V->coeffs, W->coeffs, V->length));
}
