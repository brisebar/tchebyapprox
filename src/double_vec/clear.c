#include "double_vec.h"


// clear a vector of n double_t
void _double_vec_clear(double_ptr V, long n)
{
  long i;
  for (i = 0 ; i < n ; i++)
    double_clear(V + i);
  free(V);
}


// clear V
void double_vec_clear(double_vec_t V)
{
  long n = V->length;
  long i;
  for (i = 0 ; i < n ; i++)
    double_clear(V->coeffs + i);
  free(V->coeffs);
}
