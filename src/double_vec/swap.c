
#include "double_vec.h"


// swap V and W efficiently
void double_vec_swap(double_vec_t V, double_vec_t W)
{
  long length_buf = V->length;
  double_ptr coeffs_buf = V->coeffs;
  V->length = W->length;
  V->coeffs = W->coeffs;
  W->length = length_buf;
  W->coeffs = coeffs_buf;
}
