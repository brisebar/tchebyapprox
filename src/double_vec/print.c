
#include "double_vec.h"


void _double_vec_print(double_srcptr V, long n)
{
  printf("[");
  long i;
  for (i = 0 ; i < n ; i++) {
    printf("%.10e", V[i]);
    if (i < n-1)
      printf(", ");
  }
  printf("]");
}


// display the vector V
void double_vec_print(const double_vec_t V)
{
  _double_vec_print(V->coeffs, V->length);
}

