
#include "double_vec.h"


void _double_vec_zero(double_ptr V, long n)
{
  long i;
  for (i = 0 ; i < n ; i++)
    double_set_zero(V + i, 1);
}


void double_vec_zero(double_vec_t V)
{
_double_vec_zero(V->coeffs, V->length);
}
