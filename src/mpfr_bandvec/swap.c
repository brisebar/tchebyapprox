
#include "mpfr_bandvec.h"


void mpfr_bandvec_swap(mpfr_bandvec_t V, mpfr_bandvec_t W)
{
  long long_buf = V->Hwidth;
  V->Hwidth = W->Hwidth;
  W->Hwidth = long_buf;

  long_buf = V->Dwidth;
  V->Dwidth = W->Dwidth;
  W->Dwidth = long_buf;

  long_buf = V->ind;
  V->ind = W->ind;
  W->ind = long_buf;

  mpfr_ptr mpfr_ptr_buf = V->H;
  V->H = W->H;
  W->H = mpfr_ptr_buf;

  mpfr_ptr_buf = V->D;
  V->D = W->D;
  W->D = mpfr_ptr_buf;
}

