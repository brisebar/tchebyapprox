
#include "mpfr_bandvec.h"


void _mpfr_bandvec_1norm_ubound(mpfr_t norm, mpfr_srcptr VH, mpfr_srcptr VD, long Hwidth, long Dwidth, long ind)
{
  mpfr_set_zero(norm, 1);
  long i, imin, imax;
  
  imax = ind-Dwidth < Hwidth ? ind-Dwidth : Hwidth;
  for (i = 0 ; i < imax ; i++)
    if (mpfr_sgn(VH + i) >= 0)
      mpfr_add(norm, norm, VH + i, MPFR_RNDU);
    else
      mpfr_sub(norm, norm, VH + i, MPFR_RNDU);

  imin = ind-Dwidth < 0 ? 0 : ind-Dwidth;
  for (i = imin ; i <= ind+Dwidth ; i++)
    if (mpfr_sgn(VD + Dwidth-ind+i) >= 0)
      mpfr_add(norm, norm, VD + Dwidth-ind+i, MPFR_RNDU);
    else
      mpfr_sub(norm, norm, VD + Dwidth-ind+i, MPFR_RNDU);

  for (i = ind+Dwidth+1 ; i < Hwidth ; i++)
    if (mpfr_sgn(VH + i) >= 0)
      mpfr_add(norm, norm, VH + i, MPFR_RNDU);
    else
      mpfr_sub(norm, norm, VH + i, MPFR_RNDU);
}


void mpfr_bandvec_1norm_ubound(mpfr_t norm, const mpfr_bandvec_t V)
{
  _mpfr_bandvec_1norm_ubound(norm, V->H, V->D, V->Hwidth, V->Dwidth, V->ind);
}


void _mpfr_bandvec_1norm_lbound(mpfr_t norm, mpfr_srcptr VH, mpfr_srcptr VD, long Hwidth, long Dwidth, long ind)
{
  mpfr_set_zero(norm, 1);
  long i, imin, imax;
  
  imax = ind-Dwidth < Hwidth ? ind-Dwidth : Hwidth;
  for (i = 0 ; i < imax ; i++)
    if (mpfr_sgn(VH + i) >= 0)
      mpfr_add(norm, norm, VH + i, MPFR_RNDD);
    else
      mpfr_sub(norm, norm, VH + i, MPFR_RNDD);

  imin = ind-Dwidth < 0 ? 0 : ind-Dwidth;
  for (i = imin ; i <= ind+Dwidth ; i++)
    if (mpfr_sgn(VD + Dwidth-ind+i) >= 0)
      mpfr_add(norm, norm, VD + Dwidth-ind+i, MPFR_RNDD);
    else
      mpfr_sub(norm, norm, VD + Dwidth-ind+i, MPFR_RNDD);

  for (i = ind+Dwidth+1 ; i < Hwidth ; i++)
    if (mpfr_sgn(VH + i) >= 0)
      mpfr_add(norm, norm, VH + i, MPFR_RNDD);
    else
      mpfr_sub(norm, norm, VH + i, MPFR_RNDD);
}


void mpfr_bandvec_1norm_lbound(mpfr_t norm, const mpfr_bandvec_t V)
{
  _mpfr_bandvec_1norm_lbound(norm, V->H, V->D, V->Hwidth, V->Dwidth, V->ind);
}


void _mpfr_bandvec_1norm_fi(mpfi_t norm, mpfr_srcptr VH, mpfr_srcptr VD, long Hwidth, long Dwidth, long ind)
{
  mpfi_set_si(norm, 0);
  long i, imin, imax;
  
  imax = ind-Dwidth < Hwidth ? ind-Dwidth : Hwidth;
  for (i = 0 ; i < imax ; i++)
    if (mpfr_sgn(VH + i) >= 0)
      mpfi_add_fr(norm, norm, VH + i);
    else
      mpfi_sub_fr(norm, norm, VH + i);

  imin = ind-Dwidth < 0 ? 0 : ind-Dwidth;
  for (i = imin ; i <= ind+Dwidth ; i++)
    if (mpfr_sgn(VD + Dwidth-ind+i) >= 0)
      mpfi_add_fr(norm, norm, VD + Dwidth-ind+i);
    else
      mpfi_sub_fr(norm, norm, VD + Dwidth-ind+i);

  for (i = ind+Dwidth+1 ; i < Hwidth ; i++)
    if (mpfr_sgn(VH + i) >= 0)
      mpfi_add_fr(norm, norm, VH + i);
    else
      mpfi_sub_fr(norm, norm, VH + i);
}


void mpfr_bandvec_1norm_fi(mpfi_t norm, const mpfr_bandvec_t V)
{
  _mpfr_bandvec_1norm_fi(norm, V->H, V->D, V->Hwidth, V->Dwidth, V->ind);
}

