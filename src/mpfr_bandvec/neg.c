
#include "mpfr_bandvec.h"



void _mpfr_bandvec_neg(mpfr_ptr VH, mpfr_ptr VD, long VHwidth, long VDwidth, mpfr_srcptr WH, mpfr_srcptr WD, long WHwidth, long WDwidth, long ind)
{
  _mpfr_bandvec_set(VH, VD, VHwidth, VDwidth, WH, WD, WHwidth, WDwidth, ind);
  long i;

  for (i = 0 ; i < VHwidth ; i++)
    mpfr_neg(VH + i, VH + i, MPFR_RNDN);

  for (i = 0 ; i <= 2*VDwidth ; i++)
    mpfr_neg(VD + i, VD + i, MPFR_RNDN);
}


void mpfr_bandvec_neg(mpfr_bandvec_t V, const mpfr_bandvec_t W)
{
  if (V != W)
    mpfr_bandvec_set_params(V, W->Hwidth, W->Dwidth, W->ind);

  _mpfr_bandvec_neg(V->H, V->D, V->Hwidth, V->Dwidth, W->H, W->D, W->Hwidth, W->Dwidth, V->ind);
}

