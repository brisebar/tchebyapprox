
#include "mpfr_bandvec.h"


void mpfr_bandvec_clear(mpfr_bandvec_t V)
{
  long i;
  for (i = 0 ; i < V->Hwidth ; i++)
    mpfr_clear(V->H + i);
  free(V->H);
  for (i = 0 ; i <= 2*V->Dwidth ; i++)
    mpfr_clear(V->D + i);
  free(V->D);
}
