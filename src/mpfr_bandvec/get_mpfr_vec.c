
#include "mpfr_bandvec.h"


void _mpfr_bandvec_get_mpfr_vec(mpfr_ptr V, long length, mpfr_srcptr WH, mpfr_srcptr WD, long WHwidth, long WDwidth, long ind)
{
  long j, jmin, jmax;

  _mpfr_vec_zero(V, length);

  jmax = WHwidth < ind - WDwidth ? WHwidth : ind - WDwidth;
  jmax = jmax < length ? jmax : length;
  for (j = 0 ; j < jmax ; j++)
    mpfr_set(V + j, WH + j, MPFR_RNDN);

  jmin = ind - WDwidth < 0 ? 0 : ind - WDwidth;
  jmax = ind + WDwidth < length ? ind + WDwidth : length - 1;
  for (j = jmin ; j <= jmax ; j++)
    mpfr_set(V + j, WD + WDwidth - ind + j, MPFR_RNDN);

  jmax = WHwidth < length ? WHwidth : length;
  for (j = ind + WDwidth + 1 ; j < jmax ; j++)
    mpfr_set(V + j, WH + j, MPFR_RNDN);
}


void mpfr_bandvec_get_mpfr_vec(mpfr_vec_t V, const mpfr_bandvec_t W)
{
  long n = W->Hwidth < W->ind + W->Dwidth + 1 ? W->ind + W->Dwidth + 1 : W->Hwidth;
  mpfr_vec_set_length(V, n);
  _mpfr_bandvec_get_mpfr_vec(V->coeffs, V->length, W->H, W->D, W->Hwidth, W->Dwidth, W->ind);
}
