
#include "mpfr_bandvec.h"


void _mpfr_bandvec_zero(mpfr_ptr VH, mpfr_ptr VD, long Hwidth, long Dwidth)
{
  long i;
  for (i = 0 ; i < Hwidth ; i++)
    mpfr_set_zero(VH + i, 1);
  for (i = 0 ; i <= 2*Dwidth ; i++)
    mpfr_set_zero(VD + i, 1);
}


void mpfr_bandvec_zero(mpfr_bandvec_t V)
{
  _mpfr_bandvec_zero(V->H, V->D, V->Hwidth, V->Dwidth);
}



