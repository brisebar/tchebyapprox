
#include "mpfr_bandvec.h"


void _mpfr_bandvec_set_coeff_si(mpfr_ptr VH, mpfr_ptr VD, long Hwidth, long Dwidth, long ind, long n, long c)
{
  if (n >= 0 && n >= ind-Dwidth && n <= ind+Dwidth)
    mpfr_set_si(VD + Dwidth-ind+n, c, MPFR_RNDN);

  else if (n >= 0 && n < Hwidth)
    mpfr_set_si(VH + n, c, MPFR_RNDN);

  else
    fprintf(stderr, "_mpfr_bandvec_set_coeff_si: error: index out of range\n");
}


void mpfr_bandvec_set_coeff_si(mpfr_bandvec_t V, long n, long c)
{
  _mpfr_bandvec_set_coeff_si(V->H, V->D, V->Hwidth, V->Dwidth, V->ind, n, c);
}


void _mpfr_bandvec_set_coeff_z(mpfr_ptr VH, mpfr_ptr VD, long Hwidth, long Dwidth, long ind, long n, const mpz_t c)
{
  if (n >= 0 && n >= ind-Dwidth && n <= ind+Dwidth)
    mpfr_set_z(VD + Dwidth-ind+n, c, MPFR_RNDN);

  else if (n >= 0 && n < Hwidth)
    mpfr_set_z(VH + n, c, MPFR_RNDN);

  else
    fprintf(stderr, "_mpfr_bandvec_set_coeff_z: error: index out of range\n");
}


void mpfr_bandvec_set_coeff_z(mpfr_bandvec_t V, long n, const mpz_t c)
{
  _mpfr_bandvec_set_coeff_z(V->H, V->D, V->Hwidth, V->Dwidth, V->ind, n, c);
}


void _mpfr_bandvec_set_coeff_q(mpfr_ptr VH, mpfr_ptr VD, long Hwidth, long Dwidth, long ind, long n, const mpq_t c)
{
  if (n >= 0 && n >= ind-Dwidth && n <= ind+Dwidth)
    mpfr_set_q(VD + Dwidth-ind+n, c, MPFR_RNDN);

  else if (n >= 0 && n < Hwidth)
    mpfr_set_q(VH + n, c, MPFR_RNDN);

  else
    fprintf(stderr, "_mpfr_bandvec_set_coeff_q: error: index out of range\n");
}


void mpfr_bandvec_set_coeff_q(mpfr_bandvec_t V, long n, const mpq_t c)
{
  _mpfr_bandvec_set_coeff_q(V->H, V->D, V->Hwidth, V->Dwidth, V->ind, n, c);
}


void _mpfr_bandvec_set_coeff_fr(mpfr_ptr VH, mpfr_ptr VD, long Hwidth, long Dwidth, long ind, long n, const mpfr_t c)
{
  if (n >= 0 && n >= ind-Dwidth && n <= ind+Dwidth)
    mpfr_set(VD + Dwidth-ind+n, c, MPFR_RNDN);

  else if (n >= 0 && n < Hwidth)
    mpfr_set(VH + n, c, MPFR_RNDN);

  else
    fprintf(stderr, "_mpfr_bandvec_set_coeff_fr: error: index out of range\n");
}


void mpfr_bandvec_set_coeff_fr(mpfr_bandvec_t V, long n, const mpfr_t c)
{
  _mpfr_bandvec_set_coeff_fr(V->H, V->D, V->Hwidth, V->Dwidth, V->ind, n, c);
}


