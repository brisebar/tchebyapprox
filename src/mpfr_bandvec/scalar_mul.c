
#include "mpfr_bandvec.h"


void _mpfr_bandvec_scalar_mul_si(mpfr_ptr VH, mpfr_ptr VD, long VHwidth, long VDwidth, mpfr_srcptr WH, mpfr_srcptr WD, long WHwidth, long WDwidth, long ind, long c)
{
  long i;
  _mpfr_bandvec_set(VH, VD, VHwidth, VDwidth, WH, WD, WHwidth, WDwidth, ind);

  for (i = 0 ; i < VHwidth ; i++)
    mpfr_mul_si(VH + i, VH + i, c, MPFR_RNDN);

  for (i = 0 ; i <= 2*VDwidth ; i++)
    mpfr_mul_si(VD + i, VD + i, c, MPFR_RNDN);
}


void mpfr_bandvec_scalar_mul_si(mpfr_bandvec_t V, const mpfr_bandvec_t W, long c)
{
  if (V != W)
    mpfr_bandvec_set_params(V, W->Hwidth, W->Dwidth, W->ind);

  _mpfr_bandvec_scalar_mul_si(V->H, V->D, V->Hwidth, V->Dwidth, W->H, W->D, W->Hwidth, W->Dwidth, V->ind, c);
}


void _mpfr_bandvec_scalar_mul_z(mpfr_ptr VH, mpfr_ptr VD, long VHwidth, long VDwidth, mpfr_srcptr WH, mpfr_srcptr WD, long WHwidth, long WDwidth, long ind, const mpz_t c)
{
  long i;
  _mpfr_bandvec_set(VH, VD, VHwidth, VDwidth, WH, WD, WHwidth, WDwidth, ind);

  for (i = 0 ; i < VHwidth ; i++)
    mpfr_mul_z(VH + i, VH + i, c, MPFR_RNDN);

  for (i = 0 ; i <= 2*VDwidth ; i++)
    mpfr_mul_z(VD + i, VD + i, c, MPFR_RNDN);
}


void mpfr_bandvec_scalar_mul_z(mpfr_bandvec_t V, const mpfr_bandvec_t W, const mpz_t c)
{
  if (V != W)
    mpfr_bandvec_set_params(V, W->Hwidth, W->Dwidth, W->ind);

  _mpfr_bandvec_scalar_mul_z(V->H, V->D, V->Hwidth, V->Dwidth, W->H, W->D, W->Hwidth, W->Dwidth, V->ind, c);
}


void _mpfr_bandvec_scalar_mul_q(mpfr_ptr VH, mpfr_ptr VD, long VHwidth, long VDwidth, mpfr_srcptr WH, mpfr_srcptr WD, long WHwidth, long WDwidth, long ind, const mpq_t c)
{
  long i;
  _mpfr_bandvec_set(VH, VD, VHwidth, VDwidth, WH, WD, WHwidth, WDwidth, ind);

  for (i = 0 ; i < VHwidth ; i++)
    mpfr_mul_q(VH + i, VH + i, c, MPFR_RNDN);

  for (i = 0 ; i <= 2*VDwidth ; i++)
    mpfr_mul_q(VD + i, VD + i, c, MPFR_RNDN);
}


void mpfr_bandvec_scalar_mul_q(mpfr_bandvec_t V, const mpfr_bandvec_t W, const mpq_t c)
{
  if (V != W)
    mpfr_bandvec_set_params(V, W->Hwidth, W->Dwidth, W->ind);

  _mpfr_bandvec_scalar_mul_q(V->H, V->D, V->Hwidth, V->Dwidth, W->H, W->D, W->Hwidth, W->Dwidth, V->ind, c);
}


void _mpfr_bandvec_scalar_mul_fr(mpfr_ptr VH, mpfr_ptr VD, long VHwidth, long VDwidth, mpfr_srcptr WH, mpfr_srcptr WD, long WHwidth, long WDwidth, long ind, const mpfr_t c)
{
  long i;
  _mpfr_bandvec_set(VH, VD, VHwidth, VDwidth, WH, WD, WHwidth, WDwidth, ind);

  for (i = 0 ; i < VHwidth ; i++)
    mpfr_mul(VH + i, VH + i, c, MPFR_RNDN);

  for (i = 0 ; i <= 2*VDwidth ; i++)
    mpfr_mul(VD + i, VD + i, c, MPFR_RNDN);
}


void mpfr_bandvec_scalar_mul_fr(mpfr_bandvec_t V, const mpfr_bandvec_t W, const mpfr_t c)
{
  if (V != W)
    mpfr_bandvec_set_params(V, W->Hwidth, W->Dwidth, W->ind);

  _mpfr_bandvec_scalar_mul_fr(V->H, V->D, V->Hwidth, V->Dwidth, W->H, W->D, W->Hwidth, W->Dwidth, V->ind, c);
}


void _mpfr_bandvec_scalar_mul_2si(mpfr_ptr VH, mpfr_ptr VD, long VHwidth, long VDwidth, mpfr_srcptr WH, mpfr_srcptr WD, long WHwidth, long WDwidth, long ind, long k)
{
   _mpfr_bandvec_set(VH, VD, VHwidth, VDwidth, WH, WD, WHwidth, WDwidth, ind);
long i;

for (i = 0 ; i < VHwidth ; i++)
  mpfr_mul_2si(VH + i, VH + i, k, MPFR_RNDN);

for (i = 0 ; i <= 2*VDwidth ; i++)
  mpfr_mul_2si(VD + i, VD + i, k, MPFR_RNDN);
}


void mpfr_bandvec_scalar_mul_2si(mpfr_bandvec_t V, const mpfr_bandvec_t W, long k)
{
if (V != W)
    mpfr_bandvec_set_params(V, W->Hwidth, W->Dwidth, W->ind);

  _mpfr_bandvec_scalar_mul_2si(V->H, V->D, V->Hwidth, V->Dwidth, W->H, W->D, W->Hwidth, W->Dwidth, V->ind, k);
}




