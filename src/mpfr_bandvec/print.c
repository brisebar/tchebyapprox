
#include "mpfr_bandvec.h"


void _mpfr_bandvec_print(mpfr_srcptr VH, mpfr_srcptr VD, long VHwidth, long VDwidth, long ind, size_t digits)
{
  long i, imin, imax;
  printf("[");

  imax = ind-VDwidth < VHwidth ? ind-VDwidth : VHwidth;
  for (i = 0 ; i < imax ; i++) {
    mpfr_out_str(stdout, 10, digits, VH + i, MPFR_RNDN);
    if (i < imax-1)
      printf(", ");
    else
      printf(" || ");
  }

  imin = ind-VDwidth < 0 ? 0 : ind-VDwidth;
  for (i = imin ; i <= ind+VDwidth ; i++) {
    mpfr_out_str(stdout, 10, digits, VD + VDwidth-ind+i, MPFR_RNDN);
    if (i < ind+VDwidth)
      printf(", ");
  }

  for (i = ind+VDwidth+1 ; i < VHwidth ; i++) {
    if (i == ind+VDwidth+1)
      printf(" || ");
    mpfr_out_str(stdout, 10, digits, VH + i, MPFR_RNDN);
    if (i < VHwidth-1)
      printf(", ");
  }

  printf("]");
}


void mpfr_bandvec_print(const mpfr_bandvec_t V, size_t digits)
{
  _mpfr_bandvec_print(V->H, V->D, V->Hwidth, V->Dwidth, V->ind, digits);
}


