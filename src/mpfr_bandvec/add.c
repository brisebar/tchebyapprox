
#include "mpfr_bandvec.h"


void _mpfr_bandvec_add(mpfr_ptr VH, mpfr_ptr VD, long VHwidth, long VDwidth, mpfr_srcptr WH, mpfr_srcptr WD, long WHwidth, long WDwidth, mpfr_srcptr ZH, mpfr_srcptr ZD, long ZHwidth, long ZDwidth, long ind)
{
  if (VHwidth < WHwidth || VHwidth < ZHwidth)
    fprintf(stderr, "_mpfr_bandvec_add: error: Hwidth of result too small\n");

  else if (VDwidth < WDwidth || VDwidth < ZDwidth)
    fprintf(stderr, "_mpfr_bandvec_add: error: Dwidth of result too small\n");

  else {

    long i;

    for (i = 0 ; i < VHwidth ; i++) {
      if (i < WHwidth && i < ZHwidth)
	mpfr_add(VH + i, WH + i, ZH + i, MPFR_RNDN);
      else if (i < WHwidth)
	mpfr_set(VH + i, WH + i, MPFR_RNDN);
      else if (i < ZHwidth)
	mpfr_set(VH + i, ZH + i, MPFR_RNDN);
      else
	mpfr_set_zero(VH + i, 1);
    }

    for (i = -VDwidth ; i <= VDwidth ; i++) {
      long abs_i = abs(i);
      if (abs_i <= WDwidth && abs_i <= ZDwidth)
	mpfr_add(VD + VDwidth + i, WD + WDwidth + i, ZD + ZDwidth + i, MPFR_RNDN);
      else if (abs_i <= WDwidth)
	mpfr_set(VD + VDwidth + i, WD + WDwidth + i, MPFR_RNDN);
      else if (abs_i <= ZDwidth)
	mpfr_set(VD + VDwidth + i, ZD + ZDwidth + i, MPFR_RNDN);
      else
	mpfr_set_zero(VD + VDwidth + i, 1);
    }

    _mpfr_bandvec_normalise(VH, VD, VHwidth, VDwidth, ind);

  }
}


void mpfr_bandvec_add(mpfr_bandvec_t V, const mpfr_bandvec_t W, const mpfr_bandvec_t Z)
{
  if (W->ind != Z->ind)
    fprintf(stderr, "mpfr_bandvec_add: error: incompatible ind\n");

  else {

    long ind = W->ind;
    long VHwidth = W->Hwidth < Z->Hwidth ? Z->Hwidth : W->Hwidth;
    long VDwidth = W->Dwidth < Z->Dwidth ? Z->Dwidth : W->Dwidth;
    
    mpfr_bandvec_set_params(V, VHwidth, VDwidth, ind);

    _mpfr_bandvec_add(V->H, V->D, VHwidth, VDwidth, W->H, W->D, W->Hwidth, W->Dwidth, Z->H, Z->D, Z->Hwidth, Z->Dwidth, ind);

  }
}

