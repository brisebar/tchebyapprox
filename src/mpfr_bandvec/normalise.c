
#include "mpfr_bandvec.h"


void _mpfr_bandvec_normalise(mpfr_ptr VH, mpfr_ptr VD, long VHwidth, long VDwidth, long ind)
{
  long i;

  // handle negative index for VD
  for (i = ind - VDwidth ; i < 0 ; i++) {
    mpfr_add(VD + VDwidth - ind - i, VD + VDwidth - ind - i, VD + VDwidth - ind + i, MPFR_RNDN);
    mpfr_set_zero(VD + VDwidth - ind + i, 1);
  }

  // add to VD the part of VH that overlaps
  if (ind - VDwidth < VHwidth) {

    long imin = ind - VDwidth < 0 ? 0 : ind - VDwidth;
    long imax = ind + VDwidth < VHwidth ? ind + VDwidth : VHwidth - 1;

    for (i = imin ; i <= imax ; i++) {
      mpfr_add(VD + VDwidth - ind + i, VD + VDwidth - ind + i, VH + i, MPFR_RNDN);
      mpfr_set_zero(VH + i, 1);
    }

  }
}


void mpfr_bandvec_normalise(mpfr_bandvec_t V)
{
  _mpfr_bandvec_normalise(V->H, V->D, V->Hwidth, V->Dwidth, V->ind);
}
