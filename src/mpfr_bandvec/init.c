
#include "mpfr_bandvec.h"


void mpfr_bandvec_init(mpfr_bandvec_t V)
{
  V->Hwidth = 0;
  V->Dwidth = 0;
  V->ind = 0;
  V->H = malloc(0);
  V->D = malloc(sizeof(mpfr_t));
  mpfr_init(V->D + 0);
  mpfr_set_zero(V->D + 0, 1);
}
