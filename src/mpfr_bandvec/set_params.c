
#include "mpfr_bandvec.h"


void mpfr_bandvec_set_params(mpfr_bandvec_t V, long Hwidth, long Dwidth, long ind)
{
  V->ind = ind;

  long i;

  if (Hwidth < V->Hwidth) {
    for (i = Hwidth ; i < V->Hwidth ; i++)
      mpfr_clear(V->H + i);
    V->H = realloc(V->H, Hwidth * sizeof(mpfr_t));
  }

  else if (V->Hwidth < Hwidth) {
    V->H = realloc(V->H, Hwidth * sizeof(mpfr_t));
    for (i = V->Hwidth ; i < Hwidth ; i++) {
      mpfr_init(V->H + i);
      mpfr_set_zero(V->H + i, 1);
    }
  }

  V->Hwidth = Hwidth;

  if (Dwidth < V->Dwidth) {
    long delta = V->Dwidth - Dwidth;
    for (i = 0 ; i <= 2*Dwidth ; i++)
      mpfr_swap(V->D + i, V->D + delta + i);
    for (i = 2*Dwidth+1 ; i <= 2*V->Dwidth ; i++)
      mpfr_clear(V->D + i);
    V->D = realloc(V->D, (2*Dwidth + 1) * sizeof(mpfr_t));
  }

  else if (V->Dwidth < Dwidth) {
    long delta = Dwidth - V->Dwidth;
    V->D = realloc(V->D, (2*Dwidth+1) * sizeof(mpfr_t));
    for (i = 2*V->Dwidth+1 ; i <= 2*Dwidth ; i++) {
      mpfr_init(V->D + i);
      mpfr_set_zero(V->D + i, 1);
    }
    for (i = 2*V->Dwidth ; i >= 0 ; i--)
      mpfr_swap(V->D + i, V->D + delta + i);
  }

  V->Dwidth = Dwidth;

  mpfr_bandvec_normalise(V);
}

