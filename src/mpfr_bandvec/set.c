
#include "mpfr_bandvec.h"


void _mpfr_bandvec_set(mpfr_ptr VH, mpfr_ptr VD, long VHwidth, long VDwidth, mpfr_srcptr WH, mpfr_srcptr WD, long WHwidth, long WDwidth, long ind)
{
  if (VHwidth < VDwidth) {
    fprintf(stderr, "_mpfr_bandvec_set: error: Hwidth of result too small\n");
    return;
  }

  else if (VDwidth < WDwidth) {
    fprintf(stderr, "_mpfr_bandvec_set: error: Dwidth of result too small\n");
    return;
  }

  else {
    long i;
    for (i = 0 ; i < WHwidth ; i++)
      mpfr_set(VH + i, WH + i, MPFR_RNDN);
    for (i = WHwidth ; i < VHwidth ; i++)
      mpfr_set_zero(VH + i, 1);
    for (i = -WDwidth ; i <= WDwidth ; i++)
      mpfr_set(VD + VDwidth+i, WD + WDwidth+i, MPFR_RNDN);
    for (i = WDwidth+1 ; i <= VDwidth ; i++) {
      mpfr_set_zero(VD + VDwidth+i, 1);
      mpfr_set_zero(VD + VDwidth-i, 1);
    }
    _mpfr_bandvec_normalise(VH, VD, VHwidth, VDwidth, ind);
  }
}


void mpfr_bandvec_set(mpfr_bandvec_t V, const mpfr_bandvec_t W)
{
  if (V != W) {
    mpfr_bandvec_set_params(V, W->Hwidth, W->Dwidth, W->ind);
    long i;
    for (i = 0 ; i < W->Hwidth ; i++)
      mpfr_set(V->H + i, W->H + i, MPFR_RNDN);
    for (i = 0 ; i <= 2*W->Dwidth ; i++)
      mpfr_set(V->D + i, W->D + i, MPFR_RNDN);
  }
}
