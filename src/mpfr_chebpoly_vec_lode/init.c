
#include "mpfr_chebpoly_vec_lode.h"


void mpfr_chebpoly_vec_lode_init(mpfr_chebpoly_vec_lode_t L)
{
  L->order = 0;
  L->dim = 0;
  L->A = malloc(0);
}
