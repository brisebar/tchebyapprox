
#include "mpfr_chebpoly_vec_lode.h"


void mpfr_chebpoly_vec_lode_evaluate_d(mpfr_chebpoly_vec_t P, const mpfr_chebpoly_vec_lode_t L, const double_chebpoly_vec_t Q)
{

  if (L->dim != Q->dim) {
    fprintf(stderr, "mpfr_chebpoly_vec_lode_evaluate_d: error: incompatible dimensions\n");
    return;
  }

  mpfr_chebpoly_vec_t Qfr;
  mpfr_chebpoly_vec_init(Qfr);
  mpfr_chebpoly_vec_set_double_chebpoly_vec(Qfr, Q);
  mpfr_chebpoly_vec_lode_evaluate_fr(P, L, Qfr);
  mpfr_chebpoly_vec_clear(Qfr);
}


void mpfr_chebpoly_vec_lode_evaluate_fr(mpfr_chebpoly_vec_t P, const mpfr_chebpoly_vec_lode_t L, const mpfr_chebpoly_vec_t Q)
{

  if (L->dim != Q->dim) {
    fprintf(stderr, "mpfr_chebpoly_vec_lode_evaluate_fr: error: incompatible dimensions\n");
    return;
  }

  mpfr_chebpoly_vec_t L_Q, d_Q; 
  mpfr_chebpoly_t F;
  mpfr_chebpoly_vec_init(L_Q);
  mpfr_chebpoly_vec_init(d_Q);
  mpfr_chebpoly_init(F);
  mpfr_chebpoly_vec_set(d_Q, Q);

  long r = L->order;
  long n = L->dim;
  mpfr_chebpoly_vec_set_dim(L_Q, n);

  long i, j, k;
  for (i = 0 ; i < r ; i++) {
    
    for (j = 0 ; j < n ; j++)
      for (k = 0 ; k < n ; k++) {  
        mpfr_chebpoly_mul(F, L->A[i][j] + k, d_Q->poly + k);
	mpfr_chebpoly_add(L_Q->poly + j, L_Q->poly + j, F);
      }

    mpfr_chebpoly_vec_derivative(d_Q, d_Q);

  }

  mpfr_chebpoly_vec_add(L_Q, L_Q, d_Q);

  mpfr_chebpoly_vec_swap(P, L_Q);
  mpfr_chebpoly_vec_clear(L_Q);
  mpfr_chebpoly_vec_clear(d_Q);
  mpfr_chebpoly_clear(F);
}


void mpfr_chebpoly_vec_lode_evaluate_fi(mpfi_chebpoly_vec_t P, const mpfr_chebpoly_vec_lode_t L, const mpfi_chebpoly_vec_t Q)
{

  if (L->dim != Q->dim) {
    fprintf(stderr, "mpfr_chebpoly_vec_lode_evaluate_fi: error: incompatible dimensions\n");
    return;
  }

  mpfi_chebpoly_vec_t L_Q, d_Q; 
  mpfi_chebpoly_t F;
  mpfi_chebpoly_vec_init(L_Q);
  mpfi_chebpoly_vec_init(d_Q);
  mpfi_chebpoly_init(F);
  mpfi_chebpoly_vec_set(d_Q, Q);

  long r = L->order;
  long n = L->dim;
  mpfi_chebpoly_vec_set_dim(L_Q, n);

  long i, j, k;
  for (i = 0 ; i < r ; i++) {
    
    for (j = 0 ; j < n ; j++)
      for (k = 0 ; k < n ; k++) {
	mpfi_chebpoly_set_mpfr_chebpoly(F, L->A[i][j] + k);
        mpfi_chebpoly_mul(F, F, d_Q->poly + k);
	mpfi_chebpoly_add(L_Q->poly + j, L_Q->poly + j, F);
      }

    mpfi_chebpoly_vec_derivative(d_Q, d_Q);

  }

  mpfi_chebpoly_vec_add(L_Q, L_Q, d_Q);

  mpfi_chebpoly_vec_swap(P, L_Q);
  mpfi_chebpoly_vec_clear(L_Q);
  mpfi_chebpoly_vec_clear(d_Q);
  mpfi_chebpoly_clear(F);
}


