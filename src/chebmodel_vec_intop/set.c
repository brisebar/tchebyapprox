
#include "chebmodel_vec_intop.h"


void chebmodel_vec_intop_set(chebmodel_vec_intop_t K, const chebmodel_vec_intop_t N)
{
  if (K != N) {
    long n = N->dim;
    chebmodel_vec_intop_set_dim(K, n);
    long i, j;
    for (i = 0 ; i < n ; i++)
      for (j = 0 ; j < n ; j++)
	chebmodel_intop_set(K->intop[i] + j, N->intop[i] + j);
  }
}


void chebmodel_vec_intop_set_double_chebpoly_vec_intop(chebmodel_vec_intop_t K, const double_chebpoly_vec_intop_t N)
{
  long n = N->dim;
  chebmodel_vec_intop_set_dim(K, n);
  long i, j;
  for (i = 0 ; i < n ; i++)
    for (j = 0 ; j < n ; j++)
      chebmodel_intop_set_double_chebpoly_intop(K->intop[i] + j, N->intop[i] + j);
}


void chebmodel_vec_intop_set_mpfr_chebpoly_vec_intop(chebmodel_vec_intop_t K, const mpfr_chebpoly_vec_intop_t N)
{
  long n = N->dim;
  chebmodel_vec_intop_set_dim(K, n);
  long i, j;
  for (i = 0 ; i < n ; i++)
    for (j = 0 ; j < n ; j++)
      chebmodel_intop_set_mpfr_chebpoly_intop(K->intop[i] + j, N->intop[i] + j);
}


void chebmodel_vec_intop_set_mpfi_chebpoly_vec_intop(chebmodel_vec_intop_t K, const mpfi_chebpoly_vec_intop_t N)
{
  long n = N->dim;
  chebmodel_vec_intop_set_dim(K, n);
  long i, j;
  for (i = 0 ; i < n ; i++)
    for (j = 0 ; j < n ; j++)
      chebmodel_intop_set_mpfi_chebpoly_intop(K->intop[i] + j, N->intop[i] + j);
}


void chebmodel_vec_intop_get_double_chebpoly_vec_intop(double_chebpoly_vec_intop_t K, const chebmodel_vec_intop_t N)
{
  long n = N->dim;
  double_chebpoly_vec_intop_set_dim(K, n);
  long i, j;
  for (i = 0 ; i < n ; i++)
    for (j = 0 ; j < n ; j++)
      chebmodel_intop_get_double_chebpoly_intop(K->intop[i] + j, N->intop[i] + j);
}


void chebmodel_vec_intop_get_mpfr_chebpoly_vec_intop(mpfr_chebpoly_vec_intop_t K, const chebmodel_vec_intop_t N)
{
  long n = N->dim;
  mpfr_chebpoly_vec_intop_set_dim(K, n);
  long i, j;
  for (i = 0 ; i < n ; i++)
    for (j = 0 ; j < n ; j++)
      chebmodel_intop_get_mpfr_chebpoly_intop(K->intop[i] + j, N->intop[i] + j);
}


void chebmodel_vec_intop_get_mpfi_chebpoly_vec_intop(mpfi_chebpoly_vec_intop_t K, const chebmodel_vec_intop_t N)
{
  long n = N->dim;
  mpfi_chebpoly_vec_intop_set_dim(K, n);
  long i, j;
  for (i = 0 ; i < n ; i++)
    for (j = 0 ; j < n ; j++)
      chebmodel_intop_get_mpfi_chebpoly_intop(K->intop[i] + j, N->intop[i] + j);
}
