
#include "chebmodel_vec_intop.h"


void chebmodel_vec_intop_rhs(chebmodel_vec_t G, const chebmodel_vec_lode_t L, const chebmodel_vec_t g, mpfi_ptr_ptr I)
{
  chebmodel_vec_intop_initvals(G, L, I);
  chebmodel_vec_add(G, G, g);
}

