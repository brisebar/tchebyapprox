
#include "chebmodel_vec_intop.h"


void chebmodel_vec_intop_swap(chebmodel_vec_intop_t K, chebmodel_vec_intop_t N)
{
  long dim_buf = K->dim;
  chebmodel_intop_struct ** intop_buf = K->intop;

  K->dim = N->dim;
  K->intop = N->intop;

  N->dim = dim_buf;
  N->intop = intop_buf;
}
