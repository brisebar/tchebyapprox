
#include "chebmodel_vec_intop.h"


void chebmodel_vec_intop_initvals(chebmodel_vec_t G, const chebmodel_vec_lode_t L, mpfi_ptr_ptr I)
{
  long n = L->dim;
  chebmodel_vec_set_dim(G, n);
  chebmodel_vec_zero(G);
  long i, j;
  chebmodel_lode_t Lij;
  chebmodel_lode_init(Lij);
  chebmodel_t F;
  chebmodel_init(F);

  for (i = 0 ; i < n ; i++)
    for (j = 0 ; j < n ; j++) {
      chebmodel_vec_lode_get_chebmodel_lode(Lij, L, i, j);
      chebmodel_intop_initvals(F, Lij, I[j]);
      chebmodel_add(G->poly + i, G->poly + i, F);
    }
  chebmodel_lode_clear(Lij);
  chebmodel_clear(F);
}


