
#include "chebmodel_vec_intop.h"


void chebmodel_vec_intop_set_lode(chebmodel_vec_intop_t K, const chebmodel_vec_lode_t L)
{
  long n = L->dim;
  chebmodel_vec_intop_set_dim(K, n);
  long i, j;
  chebmodel_lode_t Lij;
  chebmodel_lode_init(Lij);
  
  for (i = 0 ; i < n ; i++)
    for (j = 0 ; j < n ; j++) {
      chebmodel_vec_lode_get_chebmodel_lode(Lij, L, i, j);
      chebmodel_intop_set_lode(K->intop[i] + j, Lij);
    }

  chebmodel_lode_clear(Lij);
}


