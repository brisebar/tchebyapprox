
#include "chebmodel_vec_intop.h"


void chebmodel_vec_intop_evaluate_mpfr_chebpoly(chebmodel_vec_t P, const chebmodel_vec_intop_t K, const mpfr_chebpoly_vec_t Q)
{
  if (K->dim != Q->dim) {
    fprintf(stderr, "chebmodel_vec_intop_evaluate_mpfr_chebpoly: error: incompatible dimensions\n");
    return;
  }

  long n = K->dim;
  long i, j;
  chebmodel_vec_t K_Q;
  chebmodel_vec_init(K_Q);
  chebmodel_vec_set_dim(K_Q, n);
  chebmodel_t F;
  chebmodel_init(F);

  for (i = 0 ; i < n ; i++)
    for (j = 0 ; j < n ; j++) {
      chebmodel_intop_evaluate_mpfr_chebpoly(F, K->intop[i] + j, Q->poly + j);
      chebmodel_add(K_Q->poly + i, K_Q->poly + i, F);
    }

  chebmodel_vec_swap(P, K_Q);
  chebmodel_vec_clear(K_Q);
  chebmodel_clear(F);
}


void chebmodel_vec_intop_evaluate_mpfi_chebpoly(chebmodel_vec_t P, const chebmodel_vec_intop_t K, const mpfi_chebpoly_vec_t Q)
{
  if (K->dim != Q->dim) {
    fprintf(stderr, "chebmodel_vec_intop_evaluate_mpfi_chebpoly: error: incompatible dimensions\n");
    return;
  }

  long n = K->dim;
  long i, j;
  chebmodel_vec_t K_Q;
  chebmodel_vec_init(K_Q);
  chebmodel_vec_set_dim(K_Q, n);
  chebmodel_t F;
  chebmodel_init(F);

  for (i = 0 ; i < n ; i++)
    for (j = 0 ; j < n ; j++) {
      chebmodel_intop_evaluate_mpfi_chebpoly(F, K->intop[i] + j, Q->poly + j);
      chebmodel_add(K_Q->poly + i, K_Q->poly + i, F);
    }

  chebmodel_vec_swap(P, K_Q);
  chebmodel_vec_clear(K_Q);
  chebmodel_clear(F);
}


void chebmodel_vec_intop_evaluate_chebmodel(chebmodel_vec_t P, const chebmodel_vec_intop_t K, const chebmodel_vec_t Q)
{
  if (K->dim != Q->dim) {
    fprintf(stderr, "chebmodel_vec_intop_evaluate_chebmodel: error: incompatible dimensions\n");
    return;
  }

  long n = K->dim;
  long i, j;
  chebmodel_vec_t K_Q;
  chebmodel_vec_init(K_Q);
  chebmodel_vec_set_dim(K_Q, n);
  chebmodel_t F;
  chebmodel_init(F);

  for (i = 0 ; i < n ; i++)
    for (j = 0 ; j < n ; j++) {
      chebmodel_intop_evaluate_chebmodel(F, K->intop[i] + j, Q->poly + j);
      chebmodel_add(K_Q->poly + i, K_Q->poly + i, F);
    }

  chebmodel_vec_swap(P, K_Q);
  chebmodel_vec_clear(K_Q);
  chebmodel_clear(F);
}


