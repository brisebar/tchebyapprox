
#include "chebmodel_vec_intop.h"


void chebmodel_vec_intop_set_dim(chebmodel_vec_intop_t K, long dim)
{
  chebmodel_vec_intop_clear(K);
  K->dim = dim;
  long i, j;

  K->intop = malloc(dim * sizeof(chebmodel_intop_ptr));
  for (i = 0 ; i < dim ; i++) {
    K->intop[i] = malloc(dim * sizeof(chebmodel_intop_t));
    for (j = 0 ; j < dim ; j++)
      chebmodel_intop_init(K->intop[i] + j);
  }
}

