
#include "mpfr_bandmatrix.h"



void mpfr_bandmatrix_clear(mpfr_bandmatrix_t M)
{
  long n = M->dim;
  long r = M->Hwidth;
  long s = M->Dwidth;
  long i, j;

  for (i = 0 ; i < n ; i++) {
    for (j = 0 ; j <= 2*s ; j++)
      mpfr_clear(M->D[i] + j);
    free(M->D[i]);
  }
  free(M->D);

  for (j = 0 ; j < r ; j++) {
    for (i = 0 ; i < n ; i++)
      mpfr_clear(M->H[j] + i);
    free(M->H[j]);
  }
  free(M->H);
}
