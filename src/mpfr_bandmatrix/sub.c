
#include "mpfr_bandmatrix.h"


void mpfr_bandmatrix_sub(mpfr_bandmatrix_t M, const mpfr_bandmatrix_t N, const mpfr_bandmatrix_t P)
{
  long n = N->dim;
 
  if (n != P->dim) 
    fprintf(stderr, "mpfr_bandmatrix_sub: error: operands 1 and 2 have different dimensions.\n");

  else {

    long r1 = N->Hwidth;
    long s1 = N->Dwidth;
    long r2 = P->Hwidth;
    long s2 = P->Dwidth;
    long i, j;

    long R = r1 < r2 ? r2 : r1;
    long S = s1 < s2 ? s2 : s1;

    mpfr_bandmatrix_t Q;
    mpfr_bandmatrix_init(Q);
    mpfr_bandmatrix_set_params(Q, n, R, S);
    mpfr_bandmatrix_zero(Q);

    // horizontal band
    for (i = 0 ; i < r1 ; i++)
      for (j = 0 ; j < n ; j++)
	mpfr_set(Q->H[i] + j, N->H[i] + j, MPFR_RNDN);
    for (i = 0 ; i < r2 ; i++)
      for (j = 0 ; j < n ; j++)
	mpfr_sub(Q->H[i] + j, Q->H[i] + j, P->H[i] + j, MPFR_RNDN);

    // diagonal band
    for (i = 0 ; i < n ; i++) {
      for (j = -s1 ; j <= s1 ; j++)
	mpfr_set(Q->D[i] + S - j, N->D[i] + s1 - j, MPFR_RNDN);
      for (j = -s2 ; j <= s2 ; j++)
	mpfr_sub(Q->D[i] + S - j, Q->D[i] + S - j, P->D[i] + s2 - j, MPFR_RNDN);
    }

    mpfr_bandmatrix_normalise(Q);

    mpfr_bandmatrix_swap(M, Q);
    mpfr_bandmatrix_clear(Q);
  }
}
