
#include "mpfr_bandmatrix.h"


void mpfr_bandmatrix_swap(mpfr_bandmatrix_t M, mpfr_bandmatrix_t N)
{
  long long_buf;
  mpfr_ptr_ptr mpfr_ptr_ptr_buf;

  long_buf = M->dim;
  M->dim = N->dim;
  N->dim = long_buf;

  long_buf = M->Hwidth;
  M->Hwidth = N->Hwidth;
  N->Hwidth = long_buf;

  long_buf = M->Dwidth;
  M->Dwidth = N->Dwidth;
  N->Dwidth = long_buf;

  mpfr_ptr_ptr_buf = M->H;
  M->H = N->H;
  N->H = mpfr_ptr_ptr_buf;

  mpfr_ptr_ptr_buf = M->D;
  M->D = N->D;
  N->D = mpfr_ptr_ptr_buf;
}
