
#include "mpfr_bandmatrix.h"


void mpfr_bandmatrix_QRdecomp_set_params(mpfr_bandmatrix_QRdecomp_t M, long dim, long Hwidth, long Dwidth)
{
  mpfr_bandmatrix_QRdecomp_clear(M);
  M->dim = dim;
  M->Hwidth = Hwidth;
  M->Dwidth = Dwidth;
  long i, j;

  M->H = malloc(Hwidth * sizeof(mpfr_ptr));
  for (j = 0 ; j < Hwidth ; j++) {
    M->H[j] = malloc(dim * sizeof(mpfr_t));
    for (i = 0 ; i < dim ; i++)
      mpfr_init(M->H[j] + i);
  }

  M->D = malloc(dim * sizeof(mpfr_ptr));
  M->B = malloc(dim * sizeof(mpfr_ptr));
  M->XCH = malloc(dim * sizeof(long));
  M->GR = malloc(dim * sizeof(mpfr_ptr_ptr));

  for (i = 0 ; i < dim ; i++) {

    M->D[i] = malloc((2*Dwidth+1) * sizeof(mpfr_t));
    for (j = 0 ; j <= 2*Dwidth ; j++)
      mpfr_init(M->D[i] + j);

    M->B[i] = malloc(Hwidth * sizeof(mpfr_t));
    for (j = 0 ; j < Hwidth ; j++)
      mpfr_init(M->B[i] + j);

    M->XCH[i] = 0;

    M->GR[i] = malloc(Dwidth * sizeof(mpfr_ptr));
    for (j = 0 ; j < Dwidth ; j++) {
      M->GR[i][j] = malloc(2 * sizeof(mpfr_t));
      mpfr_init(M->GR[i][j]);
      mpfr_init(M->GR[i][j] + 1);
    }

  }

}
