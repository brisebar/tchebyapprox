
#include "mpfr_bandmatrix.h"


void _mpfr_bandmatrix_get_QRdecomp_GR(mpfr_bandmatrix_QRdecomp_t N, long i, long j)
{
  long n = N->dim;
  long r = N->Hwidth;
  long s = N->Dwidth;
  long k, l, max_index;
  mpfr_t temp, temp2;
  mpfr_init(temp);
  mpfr_init(temp2);

  // compute the coefficients of the pseudo-Givens rotation
  mpfr_mul(temp, N->D[i] + 0, N->D[i] + 0, MPFR_RNDN);
  mpfr_mul(temp2, N->D[i+j] + 0, N->D[i+j] + 0, MPFR_RNDN);
  mpfr_add(temp, temp, temp2, MPFR_RNDN);
  mpfr_sqrt(temp, temp, MPFR_RNDN);
  mpfr_div(N->GR[i][j-1] + 0, N->D[i] + 0, temp, MPFR_RNDN);
  mpfr_div(N->GR[i][j-1] + 1, N->D[i+j] + 0, temp, MPFR_RNDN);
  mpfr_neg(N->GR[i][j-1] + 1, N->GR[i][j-1] + 1, MPFR_RNDN);

  // apply the rotation on the i-th column
  mpfr_mul(N->D[i] + 0, N->D[i] + 0, N->D[i] + 0, MPFR_RNDN);
  mpfr_mul(N->D[i+j] + 0, N->D[i+j] + 0, N->D[i+j] + 0, MPFR_RNDN);
  mpfr_add(N->D[i] + 0, N->D[i] + 0, N->D[i+j] + 0, MPFR_RNDN);
  mpfr_div(N->D[i] + 0, N->D[i] + 0, temp, MPFR_RNDN);
  mpfr_set_si(N->D[i+j] + 0, 0, MPFR_RNDN);

  // apply the Givens rotation on the diagonal coefficients in rows i and i+j
  max_index = i+2*s >= n ? n-1-i : 2*s; // = min(2*s,n-1-i)
  for (k = 1 ; k <= max_index ; k++) {
    mpfr_swap(temp, N->D[i+j] + k);
    mpfr_mul(N->D[i+j] + k, N->GR[i][j-1] + 1, N->D[i] + k, MPFR_RNDN);
    mpfr_mul(N->D[i] + k, N->GR[i][j-1] + 0, N->D[i] + k, MPFR_RNDN);
    mpfr_mul(temp2, N->GR[i][j-1] + 1, temp, MPFR_RNDN);
    mpfr_sub(N->D[i] + k, N->D[i] + k, temp2, MPFR_RNDN);
    mpfr_mul(temp2, N->GR[i][j-1] + 0, temp, MPFR_RNDN);
    mpfr_add(N->D[i+j] + k, N->D[i+j] + k, temp2, MPFR_RNDN);
  }

  // new values for the vectors B[i] and B[i+j] after the Givens rotation
  for (l = 0 ; l < r ; l++) {
    mpfr_swap(temp, N->B[i+j] + l);
    mpfr_mul(N->B[i+j] + l, N->GR[i][j-1] + 1, N->B[i] + l, MPFR_RNDN);
    mpfr_mul(N->B[i] + l, N->GR[i][j-1] + 0, N->B[i] + l, MPFR_RNDN);
    mpfr_mul(temp2, N->GR[i][j-1] + 1, temp, MPFR_RNDN);
    mpfr_sub(N->B[i] + l, N->B[i] + l, temp2, MPFR_RNDN);
    mpfr_mul(temp2, N->GR[i][j-1] + 0, temp, MPFR_RNDN);
    mpfr_add(N->B[i+j] + l, N->B[i+j] + l, temp2, MPFR_RNDN);
  }

  mpfr_clear(temp);
  mpfr_clear(temp2);
}



int mpfr_bandmatrix_get_QRdecomp(mpfr_bandmatrix_QRdecomp_t N, const mpfr_bandmatrix_t M)
{
  long n = M->dim;
  long r = M->Hwidth;
  long s = M->Dwidth;

  mpfr_bandmatrix_QRdecomp_set_params(N, n, r, s);
  mpfr_bandmatrix_QRdecomp_zero(N);

  long i, j, k, l, max_index1, max_index2;
  mpfr_t temp;
  mpfr_init(temp);
  mpfr_ptr row_buf;

  // copy M into N
  for (i = 0 ; i < n ; i++) {
    for (j = 0 ; j < r ; j++)
      mpfr_set(N->H[j] + i, M->H[j] + i, MPFR_RNDN);
    for (j = 0 ; j <= 2*s ; j++)
      mpfr_set(N->D[i] + j, M->D[i] + j, MPFR_RNDN);
    if (i < r)
      mpfr_set_si(N->B[i] + i, 1, MPFR_RNDN);
  }
  
  // shift the m first lines on the left to align them
  max_index1 = s-1 >= n ? n-1 : s-1; // = min(s-1,n-1)
  for (j = 0 ; j <= max_index1 ; j++) {
    for (k = 0 ; k <= s+j ; k++) {
      mpfr_set_si(N->D[j] + k, 0, MPFR_RNDN);
      mpfr_swap(N->D[j] + k, N->D[j] + k+s-j);
    }
    max_index2 = 2*s >= n ? n-1 : 2*s; // = min(2*s,n-1)
    for (k = s+j+1 ; k <= max_index2 ; k++)
      for (l = 0 ; l < r ; l++) {
	mpfr_mul(temp, N->B[j] + l, N->H[l] + k, MPFR_RNDN);
	mpfr_add(N->D[j] + k, N->D[j] + k, temp, MPFR_RNDN);
      }
  }
  
  // main loop : cancel the i-th coefficient of the m rows under the i-th row 
  for (i = 0 ; i < n ; i++) {

    max_index1 = i+s >= n ? n-1-i : s; // = min(s,n-1-i)
    max_index2 = i+2*s >= n ? n-1-i : 2*s; // min(2*s,n-1-i)

    // find the maximal pivot
    mpfr_abs(temp, N->D[i] + 0, MPFR_RNDN);
    for (j = 1 ; j <= max_index1 ; j++)
      if (mpfr_cmpabs(N->D[i+j] + 0, temp) > 0) {
	mpfr_abs(temp, N->D[i+j] + 0, MPFR_RNDN);
	N->XCH[i] = j;
      }
    if (mpfr_zero_p(temp)) {
      fprintf(stderr, "mpfr_bandmatrix_get_QRdecomp: error: no non-zero pivot at step %ld\n", i);
      mpfr_clear(temp);
      return 1;
    }
    else if (N->XCH[i] != 0) {
      j = N->XCH[i];
      row_buf = N->D[i];
      N->D[i] = N->D[i+j];
      N->D[i+j] = row_buf;
      row_buf = N->B[i];
      N->B[i] = N->B[i+j];
      N->B[i+j] = row_buf;
    }

    // use Givens rotations to annihilate M[i+j][0] for j in [1,min(m,N-i)]
    for (j = 1 ; j <= max_index1 ; j++)
      _mpfr_bandmatrix_get_QRdecomp_GR(N, i, j);

    // shift the next m rows by 1 on the left (erase the first coeff, equal to 0)
    max_index1 = i+s >= n ? n-1-i : s; // = min(s,n-1-i)
    for (j = 1 ; j <= max_index1 ; j++) {
      for (k = 0 ; k < 2*s ; k++)
	mpfr_swap(N->D[i+j] + k, N->D[i+j] + k+1);
      if (i+1+2*s < n)
	for (l = 0 ; l < r ; l++) {
	  mpfr_mul(temp, N->B[i+j] + l, N->H[l] + i+1+2*s, MPFR_RNDN);
	  mpfr_add(N->D[i+j] + 2*s, N->D[i+j] + 2*s, temp, MPFR_RNDN);
	}
    }

  }

  mpfr_clear(temp);
  return 0;
}
