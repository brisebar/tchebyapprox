
#include "mpfr_bandmatrix.h"


void mpfr_bandmatrix_QRdecomp_clear(mpfr_bandmatrix_QRdecomp_t M)
{
  long n = M->dim;
  long r = M->Hwidth;
  long s = M->Dwidth;
  long i, j;

  for (i = 0 ; i < n ; i++) {

    for (j = 0 ; j <= 2*s ; j++)
      mpfr_clear(M->D[i] + j);
    free(M->D[i]);

    for (j = 0 ; j < r ; j++)
      mpfr_clear(M->B[i] + j);
    free(M->B[i]);

    for (j = 0 ; j < s ; j++) {
      mpfr_clear(M->GR[i][j]);
      mpfr_clear(M->GR[i][j] + 1);
      free(M->GR[i][j]);
    }
    free(M->GR[i]);
  
  }
  
  free(M->D);
  free(M->B);
  free(M->XCH);
  free(M->GR);

  for (j = 0 ; j < r ; j++) {
    for (i = 0 ; i < n ; i++)
      mpfr_clear(M->H[j] + i);
    free(M->H[j]);
  }
  free(M->H);
}
