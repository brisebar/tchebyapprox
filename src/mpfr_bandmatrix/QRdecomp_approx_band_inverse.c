
#include "mpfr_bandmatrix.h"
#include "mpfr_chebpoly.h"


// approximate the inverse of a banded matrix (given under its QR decomposition) with a banded matrix
void mpfr_bandmatrix_QRdecomp_approx_band_inverse(mpfr_bandmatrix_t P, const mpfr_bandmatrix_QRdecomp_t N, long Hwidth, long Dwidth)
{
  long n = N->dim;
  long r1 = N->Hwidth;
  long s1 = N->Dwidth;

  long r2 = Hwidth;
  long s2 = Dwidth;
  long S = s1+s2;
  mpfr_bandmatrix_set_params(P, n, r2, s2);
  mpfr_bandmatrix_zero(P);

  long i, j, k, l;
  mpfr_t temp1, temp2;
  mpfr_inits(temp1, temp2, NULL);

  mpfr_ptr V = malloc((2*S+1) * sizeof(mpfr_t));
  for (i = 0 ; i <= 2*S ; i++)
    mpfr_init(V + i);

  mpfr_ptr Z = malloc(r1 * sizeof(mpfr_t));
  for (i = 0 ; i < r1 ; i++)
    mpfr_init(Z + i);

  
  for (i = 0 ; i < n ; i++) {
    
    // V = (0,...,0,1,0,...,0) (standard i-th vector)
    mpfr_set_si(V + S, 1, MPFR_RNDN);
    for (j = 1 ; j <= S ; j++) {
      mpfr_set_zero(V + S-j, 1);
      mpfr_set_zero(V + S+j, 1);
    }

    // Z = (0,...,0)
    for (l = 0 ; l < r1 ; l++)
      mpfr_set_zero(Z + l, 1);

    // apply the Givens rotations on V
    long jmin = i-S < 0 ? 0 : i-S;
    long jmax = i+s2 < n ? i+s2 : n-1;
    for (j = jmin ; j <= jmax ; j++) {

      if (N->XCH[j] != 0)
	mpfr_swap(V + S-i+j, V + S-i+j+N->XCH[j]);

      long kmax = j+s1 < n ? s1 : n-1-j;
      for (k = 1 ; k <= kmax ; k++) {
	mpfr_swap(temp1, V + S-i+j+k);
	mpfr_mul(V + S-i+j+k, N->GR[j][k-1] + 1, V + S-i+j, MPFR_RNDN);
	mpfr_mul(V + S-i+j, N->GR[j][k-1] + 0, V + S-i+j, MPFR_RNDN);
	mpfr_mul(temp2, N->GR[j][k-1] + 1, temp1, MPFR_RNDN);
	mpfr_sub(V + S-i+j, V + S-i+j, temp2, MPFR_RNDN);
	mpfr_mul(temp2, N->GR[j][k-1] + 0, temp1, MPFR_RNDN);
	mpfr_add(V + S-i+j+k, V + S-i+j+k, temp2, MPFR_RNDN);
      }

    }
    
    // solve for the diagonal unknowns (between i-s2 and i+s2)
    jmin = i-s2 < 0 ? 0 : i-s2;
    jmax = i+s2 < n ? i+s2 : n-1;
    for (j = jmax ; j >= jmin ; j--) {
      mpfr_ptr value_j = P->D[j] + s2+i-j;
      // read right hand side
      mpfr_set(value_j, V + S-i+j, MPFR_RNDN);
      // diagonal coefficients
      long kmax = j+2*s1 < jmax ? j+2*s1 : jmax;
      for (k = j+1 ; k <= kmax ; k++) {
	mpfr_mul(temp1, N->D[j] + k-j, P->D[k] + s2+i-k, MPFR_RNDN);
	mpfr_sub(value_j, value_j, temp1, MPFR_RNDN);
      }
      // remaining coefficients
      for (l = 0 ; l < r1 ; l++) {
	mpfr_mul(temp1, N->B[j] + l, Z + l, MPFR_RNDN);
	mpfr_sub(value_j, value_j, temp1, MPFR_RNDN);
      }
      // divide by pivot
      mpfr_div(value_j, value_j, N->D[j] + 0, MPFR_RNDN);
      // update Z
      k = j+2*s1;
      if (k <= jmax)
	for (l = 0 ; l < r1 ; l++) {
	  mpfr_mul(temp1, N->H[l] + k, P->D[k] + s2+i-k, MPFR_RNDN);
	  mpfr_add(Z + l, Z + l, temp1, MPFR_RNDN);
	}
    }
    
    // solve for the initial unknowns
    jmax = r2 < i-s2 ? r2 : i-s2; // !!! strict bound
    for (j = jmax-1 ; j >= 0 ; j--) {
      mpfr_ptr value_j = P->H[j] + i;
      mpfr_ptr value_k;
      // read right hand side
      if (j >= i-S)
	mpfr_set(value_j, V + S-i+j, MPFR_RNDN);
      // diagonal coefficients
      long kmax = j+2*s1 < n ? j+2*s1 : n-1;
      for (k = j+1 ; k <= kmax ; k++) {
	if (k < r2 && k < i-s2)
	  value_k = P->H[k] + i;
	else if (k >= i-s2 && k <= i+s2)
	  value_k = P->D[k] + s2+i-k;
	else
	  value_k = NULL;
	if (value_k != NULL) {
	  mpfr_mul(temp1, N->D[j] + k-j, value_k, MPFR_RNDN);
	  mpfr_sub(value_j, value_j, temp1, MPFR_RNDN);
	}
      }
      // remaining coefficients
      for (l = 0 ; l < r1 ; l++) {
	mpfr_mul(temp1, N->B[j] + l, Z + l, MPFR_RNDN);
	mpfr_sub(value_j, value_j, temp1, MPFR_RNDN);
      }
      // divide by pivot
      mpfr_div(value_j, value_j, N->D[j] + 0, MPFR_RNDN);
      // update Z
      k = j+2*s1;
      if (k < r2 && k < i-s2)
	value_k = P->H[k] + i;
      else if (k >= i-s2 && k <= i+s2 && k < n)
	value_k = P->D[k] + s2+i-k;
      else
	value_k = NULL;
      if (value_k != NULL)
	for (l = 0 ; l < r1 ; l++) {
	  mpfr_mul(temp1, N->H[l] + k, value_k, MPFR_RNDN);
	  mpfr_add(Z + l, Z + l, temp1, MPFR_RNDN);
	}
    }

  }
  
  // clear variables
  for (j = 0 ; j <= 2*S ; j++)
    mpfr_clear(V + j);
  free(V);
  for (l = 0 ; l < r1 ; l++)
    mpfr_clear(Z + l);
  free(Z);
  mpfr_clears(temp1, temp2, NULL);

}
