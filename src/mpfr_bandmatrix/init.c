
#include "mpfr_bandmatrix.h"


void mpfr_bandmatrix_init(mpfr_bandmatrix_t M)
{
  M->dim = 0;
  M->Hwidth = 0;
  M->Dwidth = 0;
  M->H = malloc(0);
  M->D = malloc(0);
}
