
#include "mpfr_bandmatrix.h"


// approximate the inverse of a banded matrix (given under its QR decomposition) with a banded matrix
void mpfr_bandmatrix_QRdecomp_approx_band_inverse_old(mpfr_bandmatrix_t P, const mpfr_bandmatrix_QRdecomp_t N, long Hwidth, long Dwidth)
{
  long n = N->dim;
  long r1 = N->Hwidth;
  long s1 = N->Dwidth;

  long r2 = Hwidth;
  long s2 = Dwidth;
  long S = s1+s2;
  mpfr_bandmatrix_set_params(P, n, r2, s2);
  mpfr_bandmatrix_zero(P);

  long i, j, k, l;
  mpfr_t temp1, temp2;
  mpfr_init(temp1);
  mpfr_init(temp2);

  mpfr_ptr V = malloc((2*S+1) * sizeof(mpfr_t));
  for (i = 0 ; i <= 2*S ; i++)
    mpfr_init(V + i);


  for (i = 0 ; i < n ; i++) {

    // V = (0,...,0,1,0,...,0) (standard i-th vector)
    mpfr_set_si(V + S, 1, MPFR_RNDN);
    for (j = 1 ; j <= S ; j++) {
      mpfr_set_si(V + S-j, 0, MPFR_RNDN);
      mpfr_set_si(V + S+j, 0, MPFR_RNDN);
    }

    // apply the Givens rotations on V
    long jmin = i-S < 0 ? 0 : i-S;
    long jmax = i+s2 < n ? i+s2 : n-1;
    for (j = jmin ; j <= jmax ; j++) {

      if (N->XCH[j] != 0)
	mpfr_swap(V + S-i+j, V + S-i+j+N->XCH[j]);

      long kmax = j+s1 < n ? s1 : n-1-j;
      for (k = 1 ; k <= kmax ; k++) {
	mpfr_swap(temp1, V + S-i+j+k);
	mpfr_mul(V + S-i+j+k, N->GR[j][k-1] + 1, V + S-i+j, MPFR_RNDN);
	mpfr_mul(V + S-i+j, N->GR[j][k-1] + 0, V + S-i+j, MPFR_RNDN);
	mpfr_mul(temp2, N->GR[j][k-1] + 1, temp1, MPFR_RNDN);
	mpfr_sub(V + S-i+j, V + S-i+j, temp2, MPFR_RNDN);
	mpfr_mul(temp2, N->GR[j][k-1] + 0, temp1, MPFR_RNDN);
	mpfr_add(V + S-i+j+k, V + S-i+j+k, temp2, MPFR_RNDN);
      }

    }
    
    // set U of equations between i-s2 and i+s2 (upper triangular square matrix, other mpfr_t coefficients NOT intialized)
    jmin = i-s2 < 0 ? 0 : i-s2;
    jmax = i+s2 < n ? i+s2 : n-1;
    long Ulen = jmax-jmin+1;
    mpfr_ptr_ptr U = malloc(Ulen * sizeof(mpfr_ptr));
    for (j = jmin ; j <= jmax ; j++) {
      U[j-jmin] = malloc(Ulen * sizeof(mpfr_t));
      for (k = j ; k <= jmax ; k++) {
	mpfr_init(U[j-jmin] + k-jmin);
	if (k-j <= 2*s1)
	  mpfr_set(U[j-jmin] + k-jmin, N->D[j] + k-j, MPFR_RNDN);
	else {
	  mpfr_set_si(U[j-jmin] + k-jmin, 0, MPFR_RNDN);
	  for (l = 0 ; l < r1 ; l++) {
	    mpfr_mul(temp1, N->B[j] + l, N->H[l] + k, MPFR_RNDN);
	    mpfr_add(U[j-jmin] + k-jmin, U[j-jmin] + k-jmin, temp1, MPFR_RNDN);
	  }
	}
      }
    }
    
    // set (L1,L2) of equations between 0 and r2-1 (L1 for coefficients of initial unknowns, and L2 for those of diagonal ones)
    jmax = r2 < i-s2 ? r2 : i-s2; // !!! strict bound
    mpfr_ptr_ptr L1 = malloc(jmax * sizeof(mpfr_ptr));
    mpfr_ptr_ptr L2 = malloc(jmax * sizeof(mpfr_ptr));
    for (j = 0 ; j < jmax ; j++) {
      L1[j] = malloc(jmax * sizeof(mpfr_t));
      for (k = j ; k < jmax ; k++) {
	mpfr_init(L1[j] + k);
	if (k-j <= 2*s1)
	  mpfr_set(L1[j] + k, N->D[j] + k-j, MPFR_RNDN);
	else {
	  mpfr_set_si(L1[j] + k, 0, MPFR_RNDN);
	  for (l = 0 ; l < r1 ; l++) {
	    mpfr_mul(temp1, N->B[j] + l, N->H[l] + k, MPFR_RNDN);
	    mpfr_add(L1[j] + k, L1[j] + k, temp1, MPFR_RNDN);
	  }
	}
      }
      L2[j] = malloc(Ulen * sizeof(mpfr_t));
      long kmin = i-s2 < 0 ? 0 : i-s2;
      long kmax = i+s2 < n ? i+s2 : n-1;
      for (k = kmin ; k <= kmax ; k++) {
	mpfr_init(L2[j] + k-kmin);
	if (k-j <= 2*s1)
	  mpfr_set(L2[j] + k-kmin, N->D[j] + k-j, MPFR_RNDN);
	else {
	  mpfr_set_si(L2[j] + k-kmin, 0, MPFR_RNDN);
	  for (l = 0 ; l < r1 ; l++) {
	    mpfr_mul(temp1, N->B[j] + l, N->H[l] + k, MPFR_RNDN);
	    mpfr_add(L2[j] + k-kmin, L2[j] + k-kmin, temp1, MPFR_RNDN);
	  }
	}
      }
    }

    // solve the system to the restricted set of coordinates
    
    // solve for the diagonal unknowns
    jmin = i-s2 < 0 ? 0 : i-s2;
    jmax = i+s2 < n ? i+s2 : n-1;
    for (j = jmax ; j >= jmin ; j--) {
      mpfr_set(P->D[j] + s2+i-j, V + S-i+j, MPFR_RNDN);
      for (k = j+1 ; k <= jmax ; k++) {
	mpfr_mul(temp1, U[j-jmin] + k-jmin, P->D[k] + s2+i-k, MPFR_RNDN);
	mpfr_sub(P->D[j] + s2+i-j, P->D[j] + s2+i-j, temp1, MPFR_RNDN);
      }
      mpfr_div(P->D[j] + s2+i-j, P->D[j] + s2+i-j, U[j-jmin] + j-jmin, MPFR_RNDN);
    }
    
    // solve for the initial unknowns
    jmax = r2 < i-s2 ? r2 : i-s2; // !!! strict bound
    for (j = jmax-1 ; j >= 0 ; j--) {
      if (j >= i-S)
	mpfr_set(P->H[j] + i, V + S-i+j, MPFR_RNDN);
      for (k = j+1 ; k < jmax ; k++) {
	mpfr_mul(temp1, L1[j] + k, P->H[k] + i, MPFR_RNDN);
	mpfr_sub(P->H[j] + i, P->H[j] + i, temp1, MPFR_RNDN);
      }
      long kmin = i-s2 < 0 ? 0 : i-s2;
      long kmax = i+s2 < n ? i+s2 : n-1;
      for (k = kmin ; k <= kmax ; k++) {
	mpfr_mul(temp1, L2[j] + k-kmin, P->D[k] + s2+i-k, MPFR_RNDN);
	mpfr_sub(P->H[j] + i, P->H[j] + i, temp1, MPFR_RNDN);
      }
      mpfr_div(P->H[j] + i, P->H[j] + i, L1[j] + j, MPFR_RNDN);
    }
    
    // clear variables intialized in the loop
    for (j = 0 ; j < Ulen ; j++) {
      for (k = j ; k < Ulen ; k++)
	mpfr_clear(U[j] + k);
      free(U[j]);
    }
    free(U);
    jmax = r2 < i-s2 ? r2 : i-s2;
    for (j = 0 ; j < jmax ; j++) {
      for (k = j ; k < jmax ; k++)
	mpfr_clear(L1[j] + k);
      free(L1[j]);
      for (k = 0 ; k < Ulen ; k++)
	mpfr_clear(L2[j] + k);
      free(L2[j]);
    }
    free(L1);
    free(L2);
    
  }
  
  for (j = 0 ; j <= 2*S ; j++)
    mpfr_clear(V + j);
  free(V);
  mpfr_clear(temp1);
  mpfr_clear(temp2);

}
