
#include "mpfr_bandmatrix.h"


void mpfr_bandmatrix_get_column(mpfr_bandvec_t V, const mpfr_bandmatrix_t M, long i)
{
  long n = M->dim;
  long r = M->Hwidth;
  long s = M->Dwidth;
  long j, jmin, jmax;

  mpfr_bandvec_set_params(V, r, s, i);

  jmax = r < i-s ? r : i-s;
  jmax = jmax < n ? jmax : n;
  for (j = 0 ; j < jmax ; j++)
    mpfr_set(V->H + j, M->H[j] + i, MPFR_RNDN);

  jmin = i-s < 0 ? 0 : i-s;
  jmax = i+s < n ? i+s : n-1;
  for (j = jmin ; j <= jmax ; j++)
    mpfr_set(V->D + s - i + j, M->D[j] + s + i - j, MPFR_RNDN);

  jmax = r < n ? r : n;
  for (j = i+s+1 ; j < jmax ; j++)
    mpfr_set(V->H + j, M->H[j] + i, MPFR_RNDN);
}
