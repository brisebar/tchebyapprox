
#include "mpfr_bandmatrix.h"


void mpfr_bandmatrix_mul(mpfr_bandmatrix_t M, const mpfr_bandmatrix_t N, const mpfr_bandmatrix_t P)
{
  long n = N->dim;

  if (n != P->dim)
    fprintf(stderr, "mpfr_bandmatrix_mul: error: operands 1 and 2 have different dimensions (%ld != %ld)\n", n, P->dim);

  else {

    long r1 = N->Hwidth;
    long s1 = N->Dwidth;
    long r2 = P->Hwidth;
    long s2 = P->Dwidth;
    long i, j;

    long R = r1 < s1 + r2 ? s1 + r2 : r1;
    long S = s1 + s2;

    mpfr_bandmatrix_t Q;
    mpfr_bandmatrix_init(Q);
    mpfr_bandmatrix_set_params(Q, n, R, S);
    mpfr_bandmatrix_zero(Q);

    mpfr_bandvec_t V;
    mpfr_bandvec_init(V);

    for (i = 0 ; i < n ; i++) {

      mpfr_bandmatrix_get_column(V, P, i);

      mpfr_bandmatrix_evaluate_band_fr(V, N, V);

      mpfr_bandmatrix_set_column(Q, V);

    }

    mpfr_bandmatrix_swap(Q, M);
    mpfr_bandmatrix_clear(Q);

  }
}


