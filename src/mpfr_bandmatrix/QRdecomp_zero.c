
#include "mpfr_bandmatrix.h"


void mpfr_bandmatrix_QRdecomp_zero(mpfr_bandmatrix_QRdecomp_t M)
{
  long n = M->dim;
  long r = M->Hwidth;
  long s = M->Dwidth;
  long i, j;

  for (i = 0 ; i < n ; i++) {

    for (j = 0 ; j < r ; j++) {
      mpfr_set_si(M->H[j] + i, 0, MPFR_RNDN);
      mpfr_set_si(M->B[i] + j, 0, MPFR_RNDN);
    }

    for (j = 0 ; j <= 2*s ; j++)
      mpfr_set_si(M->D[i] + j, 0, MPFR_RNDN);

    for (j = 0 ; j < s ; j++) {
      mpfr_set_si(M->GR[i][j], 0, MPFR_RNDN);
      mpfr_set_si(M->GR[i][j] + 1, 0, MPFR_RNDN);
    }

    M->XCH[i] = 0;
  
  }
}


