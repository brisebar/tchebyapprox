
#include "mpfr_bandmatrix.h"


void mpfr_bandmatrix_set(mpfr_bandmatrix_t M, const mpfr_bandmatrix_t N)
{
  long n = N->dim;
  long r = N->Hwidth;
  long s = N->Dwidth;
  long i, j;

  mpfr_bandmatrix_set_params(M, n, r, s);

  for (i = 0 ; i < n ; i++) {

    for (j = 0 ; j < r ; j++)
      mpfr_set(M->H[j] + i, N->H[j] + i, MPFR_RNDN);

    for (j = 0 ; j <= 2*s ; j++)
      mpfr_set(M->D[i] + j, N->D[i] + j, MPFR_RNDN);
  
  }
}


void mpfr_bandmatrix_set_double_bandmatrix(mpfr_bandmatrix_t M, const double_bandmatrix_t N)
{
  long n = N->dim;
  long r = N->Hwidth;
  long s = N->Dwidth;
  long i, j;

  mpfr_bandmatrix_set_params(M, n, r, s);

  for (i = 0 ; i < n ; i++) {

    for (j = 0 ; j < r ; j++)
      mpfr_set_d(M->H[j] + i, N->H[j][i], MPFR_RNDN);

    for (j = 0 ; j <= 2*s ; j++)
      mpfr_set_d(M->D[i] + j, N->D[i][j], MPFR_RNDN);
  
  }
}


void mpfr_bandmatrix_get_double_bandmatrix(double_bandmatrix_t M, const mpfr_bandmatrix_t N)
{
  long n = N->dim;
  long r = N->Hwidth;
  long s = N->Dwidth;
  long i, j;

  double_bandmatrix_set_params(M, n, r, s);

  for (i = 0 ; i < n ; i++) {

    for (j = 0 ; j < r ; j++)
      double_set_fr(M->H[j] + i, N->H[j] + i, MPFR_RNDN);

    for (j = 0 ; j <= 2*s ; j++)
      double_set_fr(M->D[i] + j, N->D[i] + j, MPFR_RNDN);
  
  }
}
