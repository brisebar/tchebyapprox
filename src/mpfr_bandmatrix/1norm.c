
#include "mpfr_bandmatrix.h"


void mpfr_bandmatrix_1norm_ubound(mpfr_t norm, const mpfr_bandmatrix_t M)
{
  long n = M->dim;
  long r = M->Hwidth;
  long s = M->Dwidth;
  long i, j, jmin, jmax;

  mpfr_t temp;
  mpfr_init(temp);

  mpfr_set_zero(norm, 1);

  for (i = 0 ; i < n ; i++) {
  
    mpfr_set_zero(temp, 1);

    jmax = r < i-s ? r : i-s;
    jmax = jmax < n ? jmax : n;
    for (j = 0 ; j < jmax ; j++)
      if (mpfr_sgn(M->H[j] + i) >= 0)
	mpfr_add(temp, temp, M->H[j] + i, MPFR_RNDU);
      else
	mpfr_sub(temp, temp, M->H[j] + i, MPFR_RNDU);
      
    jmin = i-s < 0 ? 0 : i-s;
    jmax = i+s < n ? i+s : n-1;
    for (j = jmin ; j <= jmax ; j++)
      if (mpfr_sgn(M->D[j] + s - j + i) >= 0)
	mpfr_add(temp, temp, M->D[j] + s - j + i, MPFR_RNDU);
      else
	mpfr_sub(temp, temp, M->D[j] + s - j + i, MPFR_RNDU);

    jmax = r < n ? r : n;
    for (j = i+s+1 ; j < jmax ; j++)
      if (mpfr_sgn(M->H[j] + i) >= 0)
	mpfr_add(temp, temp, M->H[j] + i, MPFR_RNDU);
      else
	mpfr_sub(temp, temp, M->H[j] + i, MPFR_RNDU);
    
    if (mpfr_cmp(temp, norm) > 0)
      mpfr_set(norm, temp, MPFR_RNDU);
  
  }

  mpfr_clear(temp);
}


void mpfr_bandmatrix_1norm_lbound(mpfr_t norm, const mpfr_bandmatrix_t M)
{
  long n = M->dim;
  long r = M->Hwidth;
  long s = M->Dwidth;
  long i, j, jmin, jmax;

  mpfr_t temp;
  mpfr_init(temp);

  mpfr_set_zero(norm, 1);

  for (i = 0 ; i < n ; i++) {
  
    mpfr_set_zero(temp, 1);

    jmax = r < i-s ? r : i-s;
    jmax = jmax < n ? jmax : n;
    for (j = 0 ; j < jmax ; j++)
      if (mpfr_sgn(M->H[j] + i) >= 0)
	mpfr_add(temp, temp, M->H[j] + i, MPFR_RNDD);
      else
	mpfr_sub(temp, temp, M->H[j] + i, MPFR_RNDD);
      
    jmin = i-s < 0 ? 0 : i-s;
    jmax = i+s < n ? i+s : n-1;
    for (j = jmin ; j <= jmax ; j++)
      if (mpfr_sgn(M->D[j] + s - j + i) >= 0)
	mpfr_add(temp, temp, M->D[j] + s - j + i, MPFR_RNDD);
      else
	mpfr_sub(temp, temp, M->D[j] + s - j + i, MPFR_RNDD);

    jmax = r < n ? r : n;
    for (j = i+s+1 ; j < jmax ; j++)
      if (mpfr_sgn(M->H[j] + i) >= 0)
	mpfr_add(temp, temp, M->H[j] + i, MPFR_RNDD);
      else
	mpfr_sub(temp, temp, M->H[j] + i, MPFR_RNDD);
    
    if (mpfr_cmp(temp, norm) > 0)
      mpfr_set(norm, temp, MPFR_RNDD);
  
  }

  mpfr_clear(temp);
}


void mpfr_bandmatrix_1norm_fi(mpfi_t norm, const mpfr_bandmatrix_t M)
{
  long n = M->dim;
  long r = M->Hwidth;
  long s = M->Dwidth;
  long i, j, jmin, jmax;

  mpfi_t temp;
  mpfi_init(temp);

  mpfi_set_si(norm, 0);

  for (i = 0 ; i < n ; i++) {
  
    mpfi_set_si(temp, 0);

    jmax = r < i-s ? r : i-s;
    jmax = jmax < n ? jmax : n;
    for (j = 0 ; j < jmax ; j++)
      if (mpfr_sgn(M->H[j] + i) >= 0)
	mpfi_add_fr(temp, temp, M->H[j] + i);
      else
	mpfi_sub_fr(temp, temp, M->H[j] + i);
      
    jmin = i-s < 0 ? 0 : i-s;
    jmax = i+s < n ? i+s : n-1;
    for (j = jmin ; j <= jmax ; j++)
      if (mpfr_sgn(M->D[j] + s - j + i) >= 0)
	mpfi_add_fr(temp, temp, M->D[j] + s - j + i);
      else
	mpfi_sub_fr(temp, temp, M->D[j] + s - j + i);

    jmax = r < n ? r : n;
    for (j = i+s+1 ; j < jmax ; j++)
      if (mpfr_sgn(M->H[j] + i) >= 0)
	mpfi_add_fr(temp, temp, M->H[j] + i);
      else
	mpfi_sub_fr(temp, temp, M->H[j] + i);

    mpfr_max(&(norm->left), &(norm->left), &(temp->left), MPFR_RNDD);
    mpfr_max(&(norm->right), &(norm->right), &(temp->right), MPFR_RNDU);
  
  }

  mpfi_clear(temp);
}

