
#include "mpfr_bandmatrix.h"


void mpfr_bandmatrix_set_params(mpfr_bandmatrix_t M, long dim, long Hwidth, long Dwidth)
{
  mpfr_bandmatrix_clear(M);

  M->dim = dim;
  M->Hwidth = Hwidth;
  M->Dwidth = Dwidth;

  long i, j;

  M->H = malloc(Hwidth * sizeof(mpfr_ptr));
  for (j = 0 ; j < Hwidth ; j++) {
    M->H[j] = malloc(dim * sizeof(mpfr_t));
    for (i = 0 ; i < dim ; i++) {
      mpfr_init(M->H[j] + i);
      mpfr_set_zero(M->H[j] + i, 1);
    }
  }

  M->D = malloc(dim * sizeof(mpfr_ptr));
  for (i = 0 ; i < dim ; i++) {
    M->D[i] = malloc((2*Dwidth+1) * sizeof(mpfr_t));
    for (j = 0 ; j <= 2*Dwidth ; j++) {
      mpfr_init(M->D[i] + j);
      mpfr_set_zero(M->D[i] + j, 1);
    }
  }

}
