
#include "double_bandmatrix.h"


void double_bandmatrix_1norm(double_t norm, const double_bandmatrix_t M)
{
  long n = M->dim;
  long r = M->Hwidth;
  long s = M->Dwidth;
  long i, j, jmin, jmax;

  double_t temp;
  double_init(temp);

  double_set_zero(norm, 1);

  for (i = 0 ; i < n ; i++) {
  
    double_set_zero(temp, 1);

    jmax = r < i-s ? r : i-s;
    jmax = jmax < n ? jmax : n;
    for (j = 0 ; j < jmax ; j++)
      if (double_sgn(M->H[j] + i) >= 0)
	double_add(temp, temp, M->H[j] + i, MPFR_RNDU);
      else
	double_sub(temp, temp, M->H[j] + i, MPFR_RNDU);
      
    jmin = i-s < 0 ? 0 : i-s;
    jmax = i+s < n ? i+s : n-1;
    for (j = jmin ; j <= jmax ; j++)
      if (double_sgn(M->D[j] + s - j + i) >= 0)
	double_add(temp, temp, M->D[j] + s - j + i, MPFR_RNDU);
      else
	double_sub(temp, temp, M->D[j] + s - j + i, MPFR_RNDU);

    jmax = r < n ? r : n;
    for (j = i+s+1 ; j < jmax ; j++)
      if (double_sgn(M->H[j] + i) >= 0)
	double_add(temp, temp, M->H[j] + i, MPFR_RNDU);
      else
	double_sub(temp, temp, M->H[j] + i, MPFR_RNDU);
    
    if (double_cmp(temp, norm) > 0)
      double_set(norm, temp, MPFR_RNDU);
  
  }

  double_clear(temp);
}




void double_bandmatrix_1norm_ubound(double_t norm, const double_bandmatrix_t M)
{
  long n = M->dim;
  long r = M->Hwidth;
  long s = M->Dwidth;
  long i, j, jmin, jmax;

  mpfr_t temp, norm_fr;
  mpfr_init(temp);
  mpfr_init(norm_fr);

  mpfr_set_zero(norm_fr, 1);

  for (i = 0 ; i < n ; i++) {
  
    mpfr_set_zero(temp, 1);

    jmax = r < i-s ? r : i-s;
    jmax = jmax < n ? jmax : n;
    for (j = 0 ; j < jmax ; j++)
      if (double_sgn(M->H[j] + i) >= 0)
	mpfr_add_d(temp, temp, M->H[j][i], MPFR_RNDU);
      else
	mpfr_sub_d(temp, temp, M->H[j][i], MPFR_RNDU);
      
    jmin = i-s < 0 ? 0 : i-s;
    jmax = i+s < n ? i+s : n-1;
    for (j = jmin ; j <= jmax ; j++)
      if (double_sgn(M->D[j] + s - j + i) >= 0)
	mpfr_add_d(temp, temp, M->D[j][s - j + i], MPFR_RNDU);
      else
	mpfr_sub_d(temp, temp, M->D[j][s - j + i], MPFR_RNDU);

    jmax = r < n ? r : n;
    for (j = i+s+1 ; j < jmax ; j++)
      if (double_sgn(M->H[j] + i) >= 0)
	mpfr_add_d(temp, temp, M->H[j][i], MPFR_RNDU);
      else
	mpfr_sub_d(temp, temp, M->H[j][i], MPFR_RNDU);
    
    if (mpfr_cmp(temp, norm_fr) > 0)
      mpfr_set(norm_fr, temp, MPFR_RNDU);
  
  }

  *norm = mpfr_get_d(norm_fr, MPFR_RNDU);

  mpfr_clear(norm_fr);
  mpfr_clear(temp);
}


void double_bandmatrix_1norm_lbound(double_t norm, const double_bandmatrix_t M)
{
  long n = M->dim;
  long r = M->Hwidth;
  long s = M->Dwidth;
  long i, j, jmin, jmax;

  mpfr_t temp, norm_fr;
  mpfr_init(temp);
  mpfr_init(norm_fr);

  mpfr_set_zero(norm_fr, 1);

  for (i = 0 ; i < n ; i++) {
  
    mpfr_set_zero(temp, 1);

    jmax = r < i-s ? r : i-s;
    jmax = jmax < n ? jmax : n;
    for (j = 0 ; j < jmax ; j++)
      if (double_sgn(M->H[j] + i) >= 0)
	mpfr_add_d(temp, temp, M->H[j][i], MPFR_RNDD);
      else
	mpfr_sub_d(temp, temp, M->H[j][i], MPFR_RNDD);
      
    jmin = i-s < 0 ? 0 : i-s;
    jmax = i+s < n ? i+s : n-1;
    for (j = jmin ; j <= jmax ; j++)
      if (double_sgn(M->D[j] + s - j + i) >= 0)
	mpfr_add_d(temp, temp, M->D[j][s - j + i], MPFR_RNDD);
      else
	mpfr_sub_d(temp, temp, M->D[j][s - j + i], MPFR_RNDD);

    jmax = r < n ? r : n;
    for (j = i+s+1 ; j < jmax ; j++)
      if (double_sgn(M->H[j] + i) >= 0)
	mpfr_add_d(temp, temp, M->H[j][i], MPFR_RNDD);
      else
	mpfr_sub_d(temp, temp, M->H[j][i], MPFR_RNDD);
    
    if (mpfr_cmp(temp, norm_fr) > 0)
      mpfr_set(norm_fr, temp, MPFR_RNDD);
  
  }

  *norm = mpfr_get_d(norm_fr, MPFR_RNDD);
  
  mpfr_clear(norm_fr);
  mpfr_clear(temp);
}


void double_bandmatrix_1norm_fi(mpfi_t norm, const double_bandmatrix_t M)
{
  long n = M->dim;
  long r = M->Hwidth;
  long s = M->Dwidth;
  long i, j, jmin, jmax;

  mpfi_t temp;
  mpfi_init(temp);

  mpfi_set_si(norm, 0);

  for (i = 0 ; i < n ; i++) {
  
    mpfi_set_si(temp, 0);

    jmax = r < i-s ? r : i-s;
    jmax = jmax < n ? jmax : n;
    for (j = 0 ; j < jmax ; j++)
      if (double_sgn(M->H[j] + i) >= 0)
	mpfi_add_d(temp, temp, M->H[j][i]);
      else
	mpfi_sub_d(temp, temp, M->H[j][i]);
      
    jmin = i-s < 0 ? 0 : i-s;
    jmax = i+s < n ? i+s : n-1;
    for (j = jmin ; j <= jmax ; j++)
      if (double_sgn(M->D[j] + s - j + i) >= 0)
	mpfi_add_d(temp, temp, M->D[j][s - j + i]);
      else
	mpfi_sub_d(temp, temp, M->D[j][s - j + i]);

    jmax = r < n ? r : n;
    for (j = i+s+1 ; j < jmax ; j++)
      if (double_sgn(M->H[j] + i) >= 0)
	mpfi_add_d(temp, temp, M->H[j][i]);
      else
	mpfi_sub_d(temp, temp, M->H[j][i]);

    mpfr_max(&(norm->left), &(norm->left), &(temp->left), MPFR_RNDD);
    mpfr_max(&(norm->right), &(norm->right), &(temp->right), MPFR_RNDU);
  
  }

  mpfi_clear(temp);
}

