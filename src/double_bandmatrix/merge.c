
#include "double_bandmatrix.h"


void double_bandmatrix_merge(double_bandmatrix_t A, double_bandmatrix_struct ** B, long n)
{
  long N = B[0][0].dim;
  long h = 0;
  long d = 0;
  long i, j, k, l, hij, dij, lmin, lmax;

  for (i = 0 ; i < n ; i++)
    for (j = 0 ; j < n ; j++) {
      if (B[i][j].dim != N) {
	fprintf(stderr, "double_bandmatrix_merge: error: incompatible dimensions\n");
	return;
      }
      if (B[i][j].Hwidth > h)
	h = B[i][j].Hwidth;
      if (B[i][j].Dwidth > d)
	d = B[i][j].Dwidth;
    }

  h = n*h;
  d = n*d+n-1;
  double_bandmatrix_set_params(A, n * N, h, d);

  for (i = 0 ; i < n ; i++)
    for (j = 0 ; j < n ; j++) {
      hij = B[i][j].Hwidth;
      dij = B[i][j].Dwidth;
      // initial coefficients
      for (k = 0 ; k < N ; k++) {
        lmin = 0;
	lmax = hij < N ? hij-1 : N-1;
	for (l = lmin ; l <= lmax ; l++)
	  double_set(A->H[n*l+i] + n*k+j, B[i][j].H[l] + k, MPFR_RNDN);
      }
      // diagonal coefficients
      for (k = 0 ; k < N ; k++) {
	lmin = k-dij < 0 ? -k : -dij;
	lmax = k+dij < N ? dij : N-1-k;
	for (l = lmin ; l <= lmax ; l++)
	  double_set(A->D[n*k+i] + d+n*l+j-i, B[i][j].D[k] + dij+l, MPFR_RNDN);
      }
    }

  double_bandmatrix_normalise(A);
}
