
#include "mpfr_bandmatrix.h"


void mpfr_bandmatrix_QRdecomp_inv1norm_overestimate(mpfr_t bound, const mpfr_bandmatrix_QRdecomp_t N)
{
  long n = N->dim;
  long r = N->Hwidth;
  long s = N->Dwidth;
  long i, j, k, l, max_index;
  mpfr_t temp, temp2;
  mpfr_init(temp);
  mpfr_init(temp2);

  // Nabs is a copy of N taken in absolute value
  mpfr_bandmatrix_QRdecomp_t Nabs;
  mpfr_bandmatrix_QRdecomp_init(Nabs);
  mpfr_bandmatrix_QRdecomp_set_params(Nabs, n, r, s);
  for (i = 0 ; i < n ; i++) {
    for (j = 0 ; j < r ; j++) {
      mpfr_abs(Nabs->H[j] + i, N->H[j] + i, MPFR_RNDU);
      mpfr_abs(Nabs->B[i] + j, N->B[i] + j, MPFR_RNDU);
    }
    for (j = 0 ; j <= 2*s ; j++)
      mpfr_abs(Nabs->D[i] + j, N->D[i] + j, MPFR_RNDU);
    for (j = 0 ; j < s ; j++) {
      mpfr_abs(Nabs->GR[i][j] + 0, N->GR[i][j] + 0, MPFR_RNDU);
      mpfr_abs(Nabs->GR[i][j] + 1, N->GR[i][j] + 1, MPFR_RNDU);
    }
    Nabs->XCH[i] = N->XCH[i];
  }

  mpfr_ptr Z = malloc(n * sizeof(mpfr_t));
  for (i = 0 ; i < n ; i++) {
    mpfr_init(Z + i);
    mpfr_set_zero(Z + i, 1);
  }
  
  // C[l] contains H[l][n-1]*X[n-1] + ... + K[l][n-k]*X[n-k] for a certain k
  mpfr_ptr C = malloc(r * sizeof(mpfr_t));
  for (l = 0 ; l < r ; l++) {
    mpfr_init(C + l);
    mpfr_set_zero(C + l, 1);
  }

  // main loop : overestimate X[n-1], ..., X[0]
  for (i = n-1 ; i >= 0 ; i--) {

    // contribution of the superdiagonal coefficients
    max_index = i+2*s >= n ? n-1-i : 2*s; // = min(2*s, n-1-i)
    for (k = 1 ; k <= max_index ; k++) {
      mpfr_mul(temp, Nabs->D[i] + k, Z + i+k, MPFR_RNDU);
      mpfr_add(Z + i, Z + i, temp, MPFR_RNDU);
    }

    // contribution of the rest of the i-th row
    if (i+2*s < n-1)
      for (l = 0 ; l < r ; l++) {
	mpfr_mul(temp, Nabs->H[l] + i+2*s+1, Z + i+2*s+1, MPFR_RNDU);
	mpfr_add(C + l, C + l, temp, MPFR_RNDU);
	mpfr_mul(temp, Nabs->B[i] + l, C + l, MPFR_RNDU);
	mpfr_add(Z + i, Z + i, temp, MPFR_RNDU);
      }

    // if less than 1, the i-th standard vector gives a bigger result
    if (mpfr_cmp_si(Z + i, 1) < 0)
      mpfr_set_si(Z + i, 1, MPFR_RNDU);

    mpfr_div(Z + i, Z + i, Nabs->D[i] + 0, MPFR_RNDU);

  }

  // compute the final bound
  mpfr_set_zero(bound, 1);
  for (i = 0 ; i < n ; i++)
    mpfr_add(bound, bound, Z + i, MPFR_RNDU);

  // clear variables
  for (i = 0 ; i < n ; i++)
    mpfr_clear(Z + i);
  free(Z);
  for (l = 0 ; l < r ; l++)
    mpfr_clear(C + l);
  free(C);
  mpfr_clear(temp);
  mpfr_clear(temp2);
  mpfr_bandmatrix_QRdecomp_clear(Nabs);
}


