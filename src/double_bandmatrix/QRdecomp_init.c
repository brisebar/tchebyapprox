
#include "double_bandmatrix.h"


void double_bandmatrix_QRdecomp_init(double_bandmatrix_QRdecomp_t M)
{
  M->dim = -1;
  M->Hwidth = -1;
  M->Dwidth = -1;
  M->H = malloc(0);
  M->D = malloc(0);
  M->B = malloc(0);
  M->XCH = malloc(0);
  M->GR = malloc(0);
}
