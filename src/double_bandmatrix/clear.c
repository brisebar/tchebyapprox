
#include "double_bandmatrix.h"



void double_bandmatrix_clear(double_bandmatrix_t M)
{
  long n = M->dim;
  long r = M->Hwidth;
  long s = M->Dwidth;
  long i, j;

  for (i = 0 ; i < n ; i++) {
    for (j = 0 ; j <= 2*s ; j++)
      double_clear(M->D[i] + j);
    free(M->D[i]);
  }
  free(M->D);

  for (j = 0 ; j < r ; j++) {
    for (i = 0 ; i < n ; i++)
      double_clear(M->H[j] + i);
    free(M->H[j]);
  }
  free(M->H);
}
