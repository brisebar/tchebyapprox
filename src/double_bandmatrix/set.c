
#include "double_bandmatrix.h"


void double_bandmatrix_set(double_bandmatrix_t M, const double_bandmatrix_t N)
{
  long n = N->dim;
  long r = N->Hwidth;
  long s = N->Dwidth;
  long i, j;

  double_bandmatrix_set_params(M, n, r, s);

  for (i = 0 ; i < n ; i++) {

    for (j = 0 ; j < r ; j++)
      double_set(M->H[j] + i, N->H[j] + i, MPFR_RNDN);

    for (j = 0 ; j <= 2*s ; j++)
      double_set(M->D[i] + j, N->D[i] + j, MPFR_RNDN);
  
  }
}
