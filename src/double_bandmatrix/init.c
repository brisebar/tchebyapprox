
#include "double_bandmatrix.h"


void double_bandmatrix_init(double_bandmatrix_t M)
{
  M->dim = 0;
  M->Hwidth = 0;
  M->Dwidth = 0;
  M->H = malloc(0);
  M->D = malloc(0);
}
