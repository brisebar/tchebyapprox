
#include "double_bandmatrix.h"


void double_bandmatrix_QRdecomp_set_params(double_bandmatrix_QRdecomp_t M, long dim, long Hwidth, long Dwidth)
{
  double_bandmatrix_QRdecomp_clear(M);
  M->dim = dim;
  M->Hwidth = Hwidth;
  M->Dwidth = Dwidth;
  long i, j;

  M->H = malloc(Hwidth * sizeof(double_ptr));
  for (j = 0 ; j < Hwidth ; j++) {
    M->H[j] = malloc(dim * sizeof(double_t));
    for (i = 0 ; i < dim ; i++)
      double_init(M->H[j] + i);
  }

  M->D = malloc(dim * sizeof(double_ptr));
  M->B = malloc(dim * sizeof(double_ptr));
  M->XCH = malloc(dim * sizeof(long));
  M->GR = malloc(dim * sizeof(double_ptr_ptr));

  for (i = 0 ; i < dim ; i++) {

    M->D[i] = malloc((2*Dwidth+1) * sizeof(double_t));
    for (j = 0 ; j <= 2*Dwidth ; j++)
      double_init(M->D[i] + j);

    M->B[i] = malloc(Hwidth * sizeof(double_t));
    for (j = 0 ; j < Hwidth ; j++)
      double_init(M->B[i] + j);

    M->XCH[i] = 0;

    M->GR[i] = malloc(Dwidth * sizeof(double_ptr));
    for (j = 0 ; j < Dwidth ; j++) {
      M->GR[i][j] = malloc(2 * sizeof(double_t));
      double_init(M->GR[i][j]);
      double_init(M->GR[i][j] + 1);
    }

  }

}
