
#include "double_bandmatrix.h"


void double_bandmatrix_QRdecomp_inv1norm(double_t y, const double_bandmatrix_QRdecomp_t N)
{
  double_set_si(y, 0, MPFR_RNDN);
  double_t temp;
  double_init(temp);

  long n = N->dim;
  long i, j;
  double_ptr V = malloc(n * sizeof(double_t));
  for (i = 0 ; i < n ; i++)
    double_init(V + i);

  for (j = 0 ; j < n ; j++) {

    for (i = 0 ; i < n ; i++)
      double_set_si(V + i, i==j, MPFR_RNDN);
    
    _double_bandmatrix_QRdecomp_solve_d(V, N, V);
    
    double_set_si(temp, 0, MPFR_RNDN);
    
    for (i = 0 ; i < n ; i++) {
      double_abs(V + i, V + i, MPFR_RNDN);
      double_add(temp, temp, V + i, MPFR_RNDN);
    }

    if (double_cmp(temp, y) > 0)
      double_set(y, temp, MPFR_RNDN);

  }

  for (i = 0 ; i < n ; i++)
    double_clear(V + i);
  free(V);
  double_clear(temp);
}


void double_bandmatrix_QRdecomp_inv1norm_fi(mpfi_t y, const double_bandmatrix_QRdecomp_t N)
{
  mpfi_set_si(y, 0);
  mpfi_t temp;
  mpfi_init(temp);

  long n = N->dim;
  long i, j;
  mpfi_ptr V = malloc(n * sizeof(mpfi_t));
  for (i = 0 ; i < n ; i++)
    mpfi_init(V + i);

  for (j = 0 ; j < n ; j++) {

    for (i = 0 ; i < n ; i++)
      mpfi_set_si(V + i, i==j);
    
    _double_bandmatrix_QRdecomp_solve_fi(V, N, V);
    
    mpfi_set_si(temp, 0);
    
    for (i = 0 ; i < n ; i++) {
      mpfi_abs(V + i, V + i);
      mpfi_add(temp, temp, V + i);
    }

    mpfr_max(&(y->left), &(y->left), &(temp->left), MPFR_RNDD);
    mpfr_max(&(y->right), &(y->right), &(temp->right), MPFR_RNDU);
  
  }

  for (i = 0 ; i < n ; i++)
    mpfi_clear(V + i);
  free(V);
  mpfi_clear(temp);
}


void double_bandmatrix_QRdecomp_inv1norm_ubound(double_t norm, const double_bandmatrix_QRdecomp_t N)
{
  mpfr_t y;
  mpfr_init(y);
  mpfr_set_si(y, 0, MPFR_RNDU);
  mpfr_t temp;
  mpfr_init(temp);

  long n = N->dim;
  long i, j;
  mpfi_ptr V = malloc(n * sizeof(mpfi_t));
  for (i = 0 ; i < n ; i++)
    mpfi_init(V + i);

  for (j = 0 ; j < n ; j++) {
    
    for (i = 0 ; i < n ; i++)
      mpfi_set_si(V + i, i==j);
    
    _double_bandmatrix_QRdecomp_solve_fi(V, N, V);
    
    mpfr_set_si(temp, 0, MPFR_RNDU);
    
    for (i = 0 ; i < n ; i++) {
      mpfi_abs(V + i, V + i);
      mpfr_add(temp, temp, &(V[i].right), MPFR_RNDU);
    }
    
    mpfr_max(y, y, temp, MPFR_RNDU);
  
  }
  
  *norm = mpfr_get_d(y, MPFR_RNDU);

  for (i = 0 ; i < n ; i++)
    mpfi_clear(V + i);
  free(V);
  mpfr_clear(temp);
  mpfr_clear(y);
}


void double_bandmatrix_QRdecomp_inv1norm_lbound(double_t norm, const double_bandmatrix_QRdecomp_t N)
{
  mpfr_t y;
  mpfr_init(y);
  mpfr_set_si(y, 0, MPFR_RNDD);
  mpfr_t temp;
  mpfr_init(temp);

  long n = N->dim;
  long i, j;
  mpfi_ptr V = malloc(n * sizeof(mpfi_t));
  for (i = 0 ; i < n ; i++)
    mpfi_init(V + i);

  for (j = 0 ; j < n ; j++) {
    
    for (i = 0 ; i < n ; i++)
      mpfi_set_si(V + i, i==j);
    
    _double_bandmatrix_QRdecomp_solve_fi(V, N, V);
    
    mpfr_set_si(temp, 0, MPFR_RNDD);
    
    for (i = 0 ; i < n ; i++) {
      mpfi_abs(V + i, V + i);
      mpfr_add(temp, temp, &(V[i].right), MPFR_RNDD);
    }
    
    mpfr_max(y, y, temp, MPFR_RNDD);
  
  }

  *norm = mpfr_get_d(y, MPFR_RNDD);

  for (i = 0 ; i < n ; i++)
    mpfi_clear(V + i);
  free(V);
  mpfr_clear(temp);
  mpfr_clear(y);
}
