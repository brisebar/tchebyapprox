
#include "double_bandmatrix.h"


void _double_bandmatrix_evaluate_d(double_ptr V, const double_bandmatrix_t M, double_srcptr W)
{
  long n = M->dim;
  long r = M->Hwidth;
  long s = M->Dwidth;
  long i, j, jmin, jmax;
 
  double_t temp;
  double_init(temp);
 
  double_ptr Z = malloc(n * sizeof(double_t));
  for (i = 0 ; i < n ; i++) {
    double_init(Z + i);
    double_set_zero(Z + i, 1);
  }

  for (i = 0 ; i < n ; i++) {

    jmax = i-s < r ? i-s : r;
    jmax = jmax < n ? jmax : n;
    for (j = 0 ; j < jmax ; j++) {
      double_mul(temp, W + i, M->H[j] + i, MPFR_RNDN);
      double_add(Z + j, Z + j, temp, MPFR_RNDN);
    }

    jmin = i-s < 0 ? 0 : i-s;
    jmax = i+s < n ? i+s : n-1;
    for (j = jmin ; j <= jmax ; j++) {
      double_mul(temp, W + i, M->D[j] + s + i - j, MPFR_RNDN);
      double_add(Z + j, Z + j, temp, MPFR_RNDN);
    }

    jmax = r < n ? r : n;
    for (j = i+s+1 ; j < jmax ; j++) {
      double_mul(temp, W + i, M->H[j] + i, MPFR_RNDN);
      double_add(Z + j, Z + j, temp, MPFR_RNDN);
    }

  }

  for (i = 0 ; i < n ; i++) {
    double_swap(V + i, Z + i);
    double_clear(Z + i);
  }
  free(Z);
  double_clear(temp);
}


void double_bandmatrix_evaluate_d(double_vec_t V, const double_bandmatrix_t M, const double_vec_t W)
{
  long n = M->dim;

  if (n != W->length)
    fprintf(stderr, "double_bandmatrix_evaluate_double: error: matrix and vector have uncompatible dimensions\n");

  else {
    double_vec_set_length(V, n);
    _double_bandmatrix_evaluate_d(V->coeffs, M, W->coeffs);
  }
}

void _double_bandmatrix_evaluate_fr(mpfr_ptr V, const double_bandmatrix_t M, mpfr_srcptr W)
{
  long n = M->dim;
  long r = M->Hwidth;
  long s = M->Dwidth;
  long i, j, jmin, jmax;
 
  mpfr_t temp;
  mpfr_init(temp);
 
  mpfr_ptr Z = malloc(n * sizeof(mpfr_t));
  for (i = 0 ; i < n ; i++) {
    mpfr_init(Z + i);
    mpfr_set_zero(Z + i, 1);
  }

  for (i = 0 ; i < n ; i++) {

    jmax = i-s < r ? i-s : r;
    jmax = jmax < n ? jmax : n;
    for (j = 0 ; j < jmax ; j++) {
      mpfr_mul_d(temp, W + i, M->H[j][i], MPFR_RNDN);
      mpfr_add(Z + j, Z + j, temp, MPFR_RNDN);
    }

    jmin = i-s < 0 ? 0 : i-s;
    jmax = i+s < n ? i+s : n-1;
    for (j = jmin ; j <= jmax ; j++) {
      mpfr_mul_d(temp, W + i, M->D[j][s + i - j], MPFR_RNDN);
      mpfr_add(Z + j, Z + j, temp, MPFR_RNDN);
    }

    jmax = r < n ? r : n;
    for (j = i+s+1 ; j < jmax ; j++) {
      mpfr_mul_d(temp, W + i, M->H[j][i], MPFR_RNDN);
      mpfr_add(Z + j, Z + j, temp, MPFR_RNDN);
    }

  }

  for (i = 0 ; i < n ; i++) {
    mpfr_swap(V + i, Z + i);
    mpfr_clear(Z + i);
  }
  free(Z);
  mpfr_clear(temp);
}


void double_bandmatrix_evaluate_fr(mpfr_vec_t V, const double_bandmatrix_t M, const mpfr_vec_t W)
{
  long n = M->dim;

  if (n != W->length)
    fprintf(stderr, "double_bandmatrix_evaluate_fr: error: matrix and vector have uncompatible dimensions\n");

  else {
    mpfr_vec_set_length(V, n);
    _double_bandmatrix_evaluate_fr(V->coeffs, M, W->coeffs);
  }
}


void _double_bandmatrix_evaluate_fi(mpfi_ptr V, const double_bandmatrix_t M, mpfi_srcptr W)
{
  long n = M->dim;
  long r = M->Hwidth;
  long s = M->Dwidth;
  long i, j, jmin, jmax;
 
  mpfi_t temp;
  mpfi_init(temp);
 
  mpfi_ptr Z = malloc(n * sizeof(mpfi_t));
  for (i = 0 ; i < n ; i++) {
    mpfi_init(Z + i);
    mpfi_set_si(Z + i, 0);
  }

  for (i = 0 ; i < n ; i++) {

    jmax = i-s < r ? i-s : r;
    jmax = jmax < n ? jmax : n;
    for (j = 0 ; j < jmax ; j++) {
      mpfi_mul_d(temp, W + i, M->H[j][i]);
      mpfi_add(Z + j, Z + j, temp);
    }

    jmin = i-s < 0 ? 0 : i-s;
    jmax = i+s < n ? i+s : n-1;
    for (j = jmin ; j <= jmax ; j++) {
      mpfi_mul_d(temp, W + i, M->D[j][s + i - j]);
      mpfi_add(Z + j, Z + j, temp);
    }

    jmax = r < n ? r : n;
    for (j = i+s+1 ; j < jmax ; j++) {
      mpfi_mul_d(temp, W + i, M->H[j][i]);
      mpfi_add(Z + j, Z + j, temp);
    }

  }

  for (i = 0 ; i < n ; i++) {
    mpfi_swap(V + i, Z + i);
    mpfi_clear(Z + i);
  }
  free(Z);
  mpfi_clear(temp);
}


void double_bandmatrix_evaluate_fi(mpfi_vec_t V, const double_bandmatrix_t M, const mpfi_vec_t W)
{
  long n = M->dim;

  if (n != W->length)
    fprintf(stderr, "double_bandmatrix_evaluate_fi: error: matrix and vector have uncompatible dimensions\n");

  else {
    mpfi_vec_set_length(V, n);
    _double_bandmatrix_evaluate_fi(V->coeffs, M, W->coeffs);
  }
}



