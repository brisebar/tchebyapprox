
#include "double_bandmatrix.h"


void double_bandmatrix_set_params(double_bandmatrix_t M, long dim, long Hwidth, long Dwidth)
{
  double_bandmatrix_clear(M);

  M->dim = dim;
  M->Hwidth = Hwidth;
  M->Dwidth = Dwidth;

  long i, j;

  M->H = malloc(Hwidth * sizeof(double_ptr));
  for (j = 0 ; j < Hwidth ; j++) {
    M->H[j] = malloc(dim * sizeof(double_t));
    for (i = 0 ; i < dim ; i++) {
      double_init(M->H[j] + i);
      double_set_zero(M->H[j] + i, 1);
    }
  }

  M->D = malloc(dim * sizeof(double_ptr));
  for (i = 0 ; i < dim ; i++) {
    M->D[i] = malloc((2*Dwidth+1) * sizeof(double_t));
    for (j = 0 ; j <= 2*Dwidth ; j++) {
      double_init(M->D[i] + j);
      double_set_zero(M->D[i] + j, 1);
    }
  }

}
