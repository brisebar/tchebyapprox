
#include "double_bandmatrix.h"


void double_bandmatrix_QRdecomp_swap(double_bandmatrix_QRdecomp_t M, double_bandmatrix_QRdecomp_t N)
{
  long dim_buf = M->dim;
  long Hwidth_buf = M->Hwidth;
  long Dwidth_buf = M->Dwidth;
  double_ptr_ptr H_buf = M->H;
  double_ptr_ptr D_buf = M->D;
  double_ptr_ptr B_buf = M->B;
  long * XCH_buf = M->XCH;
  double_ptr_ptr_ptr GR_buf = M->GR;

  M->dim = N->dim;
  M->Hwidth = N->Hwidth;
  M->Dwidth = N->Dwidth;
  M->H = N->H;
  M->D = N->D;
  M->B = N->B;
  M->XCH = N->XCH;
  M->GR = N->GR;

  N->dim = dim_buf;
  N->Hwidth = Hwidth_buf;
  N->Dwidth = Dwidth_buf;
  N->H = H_buf;
  N->D = D_buf;
  N->B = B_buf;
  N->XCH = XCH_buf;
  N->GR = GR_buf;
}
