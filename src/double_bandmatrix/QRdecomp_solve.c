
#include "double_bandmatrix.h"


void _double_bandmatrix_QRdecomp_solve_d(double_ptr X, const double_bandmatrix_QRdecomp_t N, double_srcptr Y)
{
  long n = N->dim;
  long r = N->Hwidth;
  long s = N->Dwidth;
  long i, j, k, l, max_index;

  double_t temp, temp2;
  double_init(temp);
  double_init(temp2);

  // apply to Y the row-exchanges (denoted by XCH) and the Givens rotations (denoted by GR), to obtain Z
  double_ptr Z = malloc(n * sizeof(double_t));
  for (i = 0 ; i < n ; i++) {
    double_init(Z + i);
    double_set(Z + i, Y + i, MPFR_RNDN);
  }
  
  for (i = 0 ; i < n ; i++) {

    if (N->XCH[i])
      double_swap(Z + i, Z + i+(N->XCH[i]));

    max_index = i+s >= n ? n-1-i : s;
    for (j = 1 ; j <= max_index ; j++) {
      double_swap(temp, Z + i+j);
      double_mul(Z+ i+j, N->GR[i][j-1] + 1, Z + i, MPFR_RNDN);
      double_mul(Z + i, N->GR[i][j-1] + 0, Z + i, MPFR_RNDN);
      double_mul(temp2, N->GR[i][j-1] + 1, temp, MPFR_RNDN);
      double_sub(Z + i, Z + i, temp2, MPFR_RNDN);
      double_mul(temp2, N->GR[i][j-1] + 0, temp, MPFR_RNDN);
      double_add(Z + i+j, Z + i+j, temp2, MPFR_RNDN);
    }

  }

  // C[l] contains H[l][n-1]*X[n-1] + ... + K[l][n-k]*X[n-k] for a certain k
  double_ptr C = malloc(r * sizeof(double_t));
  for (l = 0 ; l < r ; l++) {
    double_init(C + l);
    double_set_zero(C + l, 1);
  }

  // main loop : find X[n-1], ..., X[0]
  for (i = n-1 ; i >= 0 ; i--) {

    double_set(X + i, Z + i, MPFR_RNDN);

    // contribution of the superdiagonal coefficients
    max_index = i+2*s >= n ? n-1-i : 2*s; // = min(2*s, n-1-i)
    for (k = 1 ; k <= max_index ; k++) {
      double_mul(temp, N->D[i] + k, X + i+k, MPFR_RNDN);
      double_sub(X + i, X + i, temp, MPFR_RNDN);
    }

    // contribution of the rest of the i-th row
    if (i+2*s < n-1)
      for (l = 0 ; l < r ; l++) {
	double_mul(temp, N->H[l] + i+2*s+1, X + i+2*s+1, MPFR_RNDN);
	double_add(C + l, C + l, temp, MPFR_RNDN);
	double_mul(temp, N->B[i] + l, C + l, MPFR_RNDN);
	double_sub(X + i, X + i, temp, MPFR_RNDN);
      }

    double_div(X + i, X + i, N->D[i] + 0, MPFR_RNDN);

  }

  // clear variables
  for (i = 0 ; i < n ; i++)
    double_clear(Z + i);
  free(Z);
  for (l = 0 ; l < r ; l++)
    double_clear(C + l);
  free(C);
  double_clear(temp);
  double_clear(temp2);
}

void double_bandmatrix_QRdecomp_solve_d(double_vec_t V, const double_bandmatrix_QRdecomp_t N, const double_vec_t W)
{
  long n = N->dim;

  if (n != W->length)
    fprintf(stderr, "double_bandmatrix_QRdecomp_solve_d: error: matrix and vector have incompatible dimensions\n");

  else {
    double_vec_set_length(V, n);
    _double_bandmatrix_QRdecomp_solve_d(V->coeffs, N, W->coeffs);
  }
}




void _double_bandmatrix_QRdecomp_solve_fr(mpfr_ptr X, const double_bandmatrix_QRdecomp_t N, mpfr_srcptr Y)
{
  long n = N->dim;
  long r = N->Hwidth;
  long s = N->Dwidth;
  long i, j, k, l, max_index;

  mpfr_t temp, temp2;
  mpfr_inits(temp, temp2, NULL);

  // apply to Y the row-exchanges (denoted by XCH) and the Givens rotations (denoted by GR), to obtain Z
  mpfr_ptr Z = malloc(n * sizeof(mpfr_t));
  for (i = 0 ; i < n ; i++) {
    mpfr_init(Z + i);
    mpfr_set(Z + i, Y + i, MPFR_RNDN);
  }
  
  for (i = 0 ; i < n ; i++) {

    if (N->XCH[i])
      mpfr_swap(Z + i, Z + i+(N->XCH[i]));

    max_index = i+s >= n ? n-1-i : s;
    for (j = 1 ; j <= max_index ; j++) {
      mpfr_swap(temp, Z + i+j);
      mpfr_mul_d(Z+ i+j, Z + i, N->GR[i][j-1][1], MPFR_RNDN);
      mpfr_mul_d(Z + i, Z + i, N->GR[i][j-1][0], MPFR_RNDN);
      mpfr_mul_d(temp2, temp, N->GR[i][j-1][1], MPFR_RNDN);
      mpfr_sub(Z + i, Z + i, temp2, MPFR_RNDN);
      mpfr_mul_d(temp2, temp, N->GR[i][j-1][0], MPFR_RNDN);
      mpfr_add(Z + i+j, Z + i+j, temp2, MPFR_RNDN);
    }

  }

  // C[l] contains H[l][n-1]*X[n-1] + ... + K[l][n-k]*X[n-k] for a certain k
  mpfr_ptr C = malloc(r * sizeof(mpfr_t));
  for (l = 0 ; l < r ; l++) {
    mpfr_init(C + l);
    mpfr_set_zero(C + l, 1);
  }

  // main loop : find X[n-1], ..., X[0]
  for (i = n-1 ; i >= 0 ; i--) {

    mpfr_set(X + i, Z + i, MPFR_RNDN);

    // contribution of the superdiagonal coefficients
    max_index = i+2*s >= n ? n-1-i : 2*s; // = min(2*s, n-1-i)
    for (k = 1 ; k <= max_index ; k++) {
      mpfr_mul_d(temp, X + i+k, N->D[i][k], MPFR_RNDN);
      mpfr_sub(X + i, X + i, temp, MPFR_RNDN);
    }

    // contribution of the rest of the i-th row
    if (i+2*s < n-1)
      for (l = 0 ; l < r ; l++) {
	mpfr_mul_d(temp, X + i+2*s+1, N->H[l][i+2*s+1], MPFR_RNDN);
	mpfr_add(C + l, C + l, temp, MPFR_RNDN);
	mpfr_mul_d(temp, C + l, N->B[i][l], MPFR_RNDN);
	mpfr_sub(X + i, X + i, temp, MPFR_RNDN);
      }

    mpfr_div_d(X + i, X + i, N->D[i][0], MPFR_RNDN);

  }

  // clear variables
  for (i = 0 ; i < n ; i++)
    mpfr_clear(Z + i);
  free(Z);
  for (l = 0 ; l < r ; l++)
    mpfr_clear(C + l);
  free(C);
  mpfr_clears(temp, temp2, NULL);
}

void double_bandmatrix_QRdecomp_solve_fr(mpfr_vec_t V, const double_bandmatrix_QRdecomp_t N, const mpfr_vec_t W)
{
  long n = N->dim;

  if (n != W->length)
    fprintf(stderr, "double_bandmatrix_QRdecomp_solve_fr: error: matrix and vector have incompatible dimensions\n");

  else {
    mpfr_vec_set_length(V, n);
    _double_bandmatrix_QRdecomp_solve_fr(V->coeffs, N, W->coeffs);
  }
}


void _double_bandmatrix_QRdecomp_solve_fi(mpfi_ptr X, const double_bandmatrix_QRdecomp_t N, mpfi_srcptr Y)
{
  long n = N->dim;
  long r = N->Hwidth;
  long s = N->Dwidth;
  long i, j, k, l, max_index;

  mpfi_t temp, temp2;
  mpfi_init(temp);
  mpfi_init(temp2);

  // apply to Y the row-exchanges (denoted by XCH) and the Givens rotations (denoted by GR), to obtain Z
  mpfi_ptr Z = malloc(n * sizeof(mpfi_t));
  for (i = 0 ; i < n ; i++) {
    mpfi_init(Z + i);
    mpfi_set(Z + i, Y + i);
  }
  
  for (i = 0 ; i < n ; i++) {

    if (N->XCH[i])
      mpfi_swap(Z + i, Z + i+(N->XCH[i]));
    
    max_index = i+s >= n ? n-1-i : s; // = min(s,n-1-i)
    for (j = 1 ; j <= max_index ; j++) {
      mpfi_swap(temp, Z + i+j);
      mpfi_mul_d(Z+ i+j, Z + i, N->GR[i][j-1][1]);
      mpfi_mul_d(Z + i, Z + i, N->GR[i][j-1][0]);
      mpfi_mul_d(temp2, temp, N->GR[i][j-1][1]);
      mpfi_sub(Z + i, Z + i, temp2);
      mpfi_mul_d(temp2, temp, N->GR[i][j-1][0]);
      mpfi_add(Z + i+j, Z + i+j, temp2);
    }
  
  }
  
  // C[l] contains H[l][n-1]*X[n-1] + ... + K[l][n-k]*X[n-k] for a certain k
  mpfi_ptr C = malloc(r * sizeof(mpfi_t));
  for (l = 0 ; l < r ; l++) {
    mpfi_init(C + l);
    mpfi_set_si(C + l, 0);
  }

  // main loop : find X[n-1], ..., X[0]
  for (i = n-1 ; i >= 0 ; i--) {

    mpfi_set(X + i, Z + i);

    // contribution of the superdiagonal coefficients
    max_index = i+2*s >= n ? n-1-i : 2*s; // = min(2*s, n-1-i)
    for (k = 1 ; k <= max_index ; k++) {
      mpfi_mul_d(temp, X + i+k, N->D[i][k]);
      mpfi_sub(X + i, X + i, temp);
    }

    // contribution of the rest of the i-th row
    if (i+2*s < n-1)
      for (l = 0 ; l < r ; l++) {
	mpfi_mul_d(temp, X + i+2*s+1, N->H[l][i+2*s+1]);
	mpfi_add(C + l, C + l, temp);
	mpfi_mul_d(temp, C + l, N->B[i][l]);
	mpfi_sub(X + i, X + i, temp);
      }

    mpfi_div_d(X + i, X + i, N->D[i][0]);

  }

  // clear variables
  for (i = 0 ; i < n ; i++)
    mpfi_clear(Z + i);
  free(Z);
  for (l = 0 ; l < r ; l++)
    mpfi_clear(C + l);
  free(C);
  mpfi_clear(temp);
  mpfi_clear(temp2);
}



void double_bandmatrix_QRdecomp_solve_fi(mpfi_vec_t V, const double_bandmatrix_QRdecomp_t N, const mpfi_vec_t W)
{
  long n = N->dim;

  if (n != W->length)
    fprintf(stderr, "double_bandmatrix_QRdecomp_solve_fi: error: matrix and vector have incompatible dimensions\n");

  else {
    mpfi_vec_set_length(V, n);
    _double_bandmatrix_QRdecomp_solve_fi(V->coeffs, N, W->coeffs);
  }
}


