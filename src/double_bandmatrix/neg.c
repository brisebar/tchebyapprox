
#include "double_bandmatrix.h"


void double_bandmatrix_neg(double_bandmatrix_t M, const double_bandmatrix_t N)
{
  long n = N->dim;
  long r = N->Hwidth;
  long s = N->Dwidth;
  long i, j;

  if (M != N)
    double_bandmatrix_set_params(M, n, r, s);

  for (i = 0 ; i < r ; i++)
    for (j = 0 ; j < n ; j++)
      double_neg(M->H[i] + j, N->H[i] + j, MPFR_RNDN);

  for (i = 0 ; i < n ; i++)
    for (j = 0 ; j <= 2*s ; j++)
      double_neg(M->D[i] + j, N->D[i] + j, MPFR_RNDN);
}
