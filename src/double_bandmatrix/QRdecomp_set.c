
#include "double_bandmatrix.h"


void double_bandmatrix_QRdecomp_set(double_bandmatrix_QRdecomp_t M, const double_bandmatrix_QRdecomp_t N)
{
  long n = N->dim;
  long r = N->Hwidth;
  long s = N->Dwidth;
  long i, j;

  double_bandmatrix_QRdecomp_set_params(M, n, r, s);

  for (i = 0 ; i < n ; i++) {

    for (j = 0 ; j < r ; j++) {
      double_set(M->H[j] + i, N->H[j] + i, MPFR_RNDN);
      double_set(M->B[i] + j, N->B[i] + j, MPFR_RNDN);
    }

    for (j = 0 ; j <= 2*s ; j++)
      double_set(M->D[i] + j, N->D[i] + j, MPFR_RNDN);

    for (j = 0 ; j < s ; j++) {
      double_set(M->GR[i][j], N->GR[i][j], MPFR_RNDN);
      double_set(M->GR[i][j] + 1, N->GR[i][j] + 1, MPFR_RNDN);
    }

    M->XCH[i] = N->XCH[i];
  
  }
}
