
#include "double_bandmatrix.h"


void double_bandmatrix_swap(double_bandmatrix_t M, double_bandmatrix_t N)
{
  long long_buf;
  double_ptr_ptr double_ptr_ptr_buf;

  long_buf = M->dim;
  M->dim = N->dim;
  N->dim = long_buf;

  long_buf = M->Hwidth;
  M->Hwidth = N->Hwidth;
  N->Hwidth = long_buf;

  long_buf = M->Dwidth;
  M->Dwidth = N->Dwidth;
  N->Dwidth = long_buf;

  double_ptr_ptr_buf = M->H;
  M->H = N->H;
  N->H = double_ptr_ptr_buf;

  double_ptr_ptr_buf = M->D;
  M->D = N->D;
  N->D = double_ptr_ptr_buf;
}
