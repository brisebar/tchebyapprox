
#include "double_bandmatrix.h"


void double_bandmatrix_mul(double_bandmatrix_t M, const double_bandmatrix_t N, const double_bandmatrix_t P)
{
  long n = N->dim;

  if (n != P->dim)
    fprintf(stderr, "double_bandmatrix_mul: error: operands 1 and 2 have different dimensions (%ld != %ld)\n", n, P->dim);

  else {

    long r1 = N->Hwidth;
    long s1 = N->Dwidth;
    long r2 = P->Hwidth;
    long s2 = P->Dwidth;
    long i, j;

    long R = r1 < s1 + r2 ? s1 + r2 : r1;
    long S = s1 + s2;

    double_bandmatrix_t Q;
    double_bandmatrix_init(Q);
    double_bandmatrix_set_params(Q, n, R, S);
    double_bandmatrix_zero(Q);

    double_bandvec_t V;
    double_bandvec_init(V);

    for (i = 0 ; i < n ; i++) {

      double_bandmatrix_get_column(V, P, i);

      double_bandmatrix_evaluate_band_d(V, N, V);

      double_bandmatrix_set_column(Q, V);

    }

    double_bandmatrix_swap(Q, M);
    double_bandmatrix_clear(Q);

  }
}


