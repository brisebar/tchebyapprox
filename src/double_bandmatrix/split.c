
#include "double_bandmatrix.h"


void double_bandmatrix_split(double_bandmatrix_struct ** A, const double_bandmatrix_t B, long n)
{
  long N = B->dim;
  if (N%n != 0) {
    fprintf(stderr, "double_bandmatrix_split: error: cannot split the matrix, total size is not a multiple of dimension\n");
    return;
  }

  long h = B->Hwidth;
  long d = B->Dwidth;
  long Nrs = N / n;
  long hrs = h % n ? h/n + 1 : h/n;
  long drs = d%n ? d/n + 2 : d/n + 1; 
  long i, j, jmin, jmax, k, l, r, s;
  // i = current line ; j = current column / position in line i in B
  // (r,s) defining the submatrix in A
  // (k,l) position in that submatrix

  for (r = 0 ; r < n ; r++)
    for (s = 0 ; s < n ; s++)
      double_bandmatrix_set_params(A[r] + s, Nrs, hrs, drs);

  // diagonal coefficients
  for (i = 0 ; i < N ; i++) {
    r = i % n;
    k = i / n;
    jmin = i-d < 0 ? -i : -d;
    jmax = i+d < N ? d : N-1-i;
    for (j = jmin ; j <= jmax ; j++) {
      s = (i+j) % n;
      l = (j+i%n)%n < 0 ? (j+i%n)/n - 1 : (j+i%n)/n;
      double_set(A[r][s].D[k] + drs+l, B->D[i] + d+j, MPFR_RNDN);
    }
  }

  // initial coefficients
  for (i = 0 ; i < h ; i++) {
    r = i % n;
    k = i / n;
    for (j = 0 ; j < N ; j++) {
      s = j % n;
      l = j / n;
      double_set(A[r][s].H[k] + l, B->H[i] + j, MPFR_RNDN);
    }
  }

  for (r = 0 ; r < n ; r++)
    for (s = 0 ; s < n ; s++)
      double_bandmatrix_normalise(A[r] + s);


}



