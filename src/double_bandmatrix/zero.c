
#include "double_bandmatrix.h"


void double_bandmatrix_zero(double_bandmatrix_t M)
{
  long n = M->dim;
  long r = M->Hwidth;
  long s = M->Dwidth;
  long i, j;

  for (i = 0 ; i < n ; i++) {

    for (j = 0 ; j < r ; j++)
      double_set_zero(M->H[j] + i, 1);

    for (j = 0 ; j <= 2*s ; j++)
      double_set_zero(M->D[i] + j, 1);
  
  }
}
