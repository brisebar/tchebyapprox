
#include "double_poly.h"


void _double_poly_shift_left(double_ptr P, double_srcptr Q, long n, unsigned long k)
{
  long i;
  for (i = n ; i >= 0 ; i--)
    double_set(P + i + k, Q + i, MPFR_RNDN);
  for (i = 0 ; i < k ; i++)
    double_set_si(P + i, 0, MPFR_RNDN);
}


// set P to Q*X^k
void double_poly_shift_left(double_poly_t P, const double_poly_t Q, unsigned long k)
{
  long i;
  long n = Q->degree;
  double_ptr Pcoeffs = malloc((n+k+1) * sizeof(double_t));
  for (i = 0 ; i <= n+k ; i++)
    double_init(Pcoeffs + i);
  _double_poly_shift_left(Pcoeffs, Q->coeffs, n, k);
  double_poly_clear(P);
  P->degree = n+k;
  P->coeffs = Pcoeffs;
}


void _double_poly_shift_right(double_ptr P, double_srcptr Q, long n, unsigned long k)
{
  long i;
  for (i = 0 ; i+k <= n ; i++)
    double_set(P + i, Q + i + k, MPFR_RNDN);
}


// set P to Q/X^k
void double_poly_shift_right(double_poly_t P, const double_poly_t Q, unsigned long k)
{
  long i;
  long n = Q->degree;
  if (k > n)
    double_poly_zero(P);
  else {
    double_ptr Pcoeffs = malloc((n-k+1) * sizeof(double_t));
    for (i = 0 ; i <= n-k ; i++)
      double_init(Pcoeffs + i);
    _double_poly_shift_right(Pcoeffs, Q->coeffs, n, k);
    double_poly_clear(P);
    P->degree = n-k;
    P->coeffs = Pcoeffs;
  }
}
