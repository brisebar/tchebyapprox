
#include "double_poly.h"


// set P in canonical form (exact degree)
void double_poly_normalise(double_poly_t P)
{
  long n, i; 
  for (n = P->degree ; n >= 0 ; n--) {
    if (!double_zero_p(P->coeffs + n))
      break;
  }
  if (n < P->degree) {
    for (i = n+1 ; i <= P->degree ; i++)
      double_clear(P->coeffs + i);
    P->degree = n;
    P->coeffs = realloc(P->coeffs, (n+1) * sizeof(double_t));
  }
}
