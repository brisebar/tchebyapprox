
#include "double_poly.h"


void _double_poly_set(double_ptr P, double_srcptr Q, long n)
{
  long i ;
  for (i = 0 ; i <= n ; i++)
    double_set(P + i, Q + i, MPFR_RNDN);
}


// copy Q into P
void double_poly_set(double_poly_t P, const double_poly_t Q)
{
  long n = Q->degree;
  long i;
  if (P != Q) {
    double_poly_set_degree(P, n);
    for (i = 0 ; i <= n ; i++)
      double_set(P->coeffs + i, Q->coeffs + i, MPFR_RNDN);
  }
}
