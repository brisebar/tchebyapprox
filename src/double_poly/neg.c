
#include "double_poly.h"


void _double_poly_neg(double_ptr P, double_srcptr Q, long n)
{
  long i;
  for (i = 0 ; i <= n ; i++)
    double_neg(P + i, Q + i, MPFR_RNDN);
}


// set P := -Q
void double_poly_neg(double_poly_t P, const double_poly_t Q)
{
  double_poly_set_degree(P, Q->degree);
  _double_poly_neg(P->coeffs, Q->coeffs, Q->degree);
}
