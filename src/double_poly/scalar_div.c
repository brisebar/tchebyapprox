
#include "double_poly.h"


void _double_poly_scalar_div_si(double_ptr P, double_srcptr Q, long n, long c)
{
  double d = c;
  _double_poly_scalar_div_d(P, Q, n, &d);
}


void _double_poly_scalar_div_z(double_ptr P, double_srcptr Q, long n, const mpz_t c)
{
  double d = mpz_get_d(c);
  _double_poly_scalar_div_d(P, Q, n, &d);
}


void _double_poly_scalar_div_q(double_ptr P, double_srcptr Q, long n, const mpq_t c)
{
  double d = mpq_get_d(c);
  _double_poly_scalar_div_d(P, Q, n, &d);
}


void _double_poly_scalar_div_d(double_ptr P, double_srcptr Q, long n, const double_t c)
{
  long i;
  for (i = 0 ; i <= n ; i++)
    double_div(P + i, Q + i, c, MPFR_RNDN);
}


void _double_poly_scalar_div_fr(double_ptr P, double_srcptr Q, long n, const mpfr_t c)
{
  double d = mpfr_get_d(c, MPFR_RNDN);
  _double_poly_scalar_div_d(P, Q, n, &d);
}


void _double_poly_scalar_div_2si(double_ptr P, double_srcptr Q, long n, long k)
{
  long i;
  for (i = 0 ; i <= n ; i++)
    double_mul_2si(P + i, Q + i, -k, MPFR_RNDN);
}



// set P := (1/c)*Q
void double_poly_scalar_div_si(double_poly_t P, const double_poly_t Q, long c)
{
  long n = Q->degree;
  double_poly_set_degree(P, n);
  _double_poly_scalar_div_si(P->coeffs, Q->coeffs, n, c);
}


// set P := (1/c)*Q
void double_poly_scalar_div_z(double_poly_t P, const double_poly_t Q, const mpz_t c)
{
  long n = Q->degree;
  double_poly_set_degree(P, n);
  _double_poly_scalar_div_z(P->coeffs, Q->coeffs, n, c);
}


// set P := (1/c)*Q
void double_poly_scalar_div_q(double_poly_t P, const double_poly_t Q, const mpq_t c)
{
  long n = Q->degree;
  double_poly_set_degree(P, n);
  _double_poly_scalar_div_q(P->coeffs, Q->coeffs, n, c);
}

// set P := (1/c)*Q
void double_poly_scalar_div_d(double_poly_t P, const double_poly_t Q, const double_t c)
{
  long n = Q->degree;
  double_poly_set_degree(P, n);
  _double_poly_scalar_div_d(P->coeffs, Q->coeffs, n, c);
}


// set P := (1/c)*Q
void double_poly_scalar_div_fr(double_poly_t P, const double_poly_t Q, const mpfr_t c)
{
  long n = Q->degree;
  double_poly_set_degree(P, n);
  _double_poly_scalar_div_fr(P->coeffs, Q->coeffs, n, c);
}


// set P := 2^-k*Q
void double_poly_scalar_div_2si(double_poly_t P, const double_poly_t Q, long k)
{
  long n = Q->degree;
  double_poly_set_degree(P, n);
  _double_poly_scalar_div_2si(P->coeffs, Q->coeffs, n, k);
}
