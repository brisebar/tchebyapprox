
#include "double_poly.h"


int _double_poly_equal(double_srcptr P, double_srcptr Q, long n)
{
  long i;
  for (i = 0 ; i <= n ; i++)
    if (!double_equal_p(P + i, Q + i))
      break;
  return (i == n+1);
}


int double_poly_equal(const double_poly_t P, const double_poly_t Q)
{
  return (P->degree == Q->degree && _double_poly_equal(P->coeffs, Q->coeffs, P->degree));
}
