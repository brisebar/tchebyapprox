
#include "double_poly.h"


// reverse a list of double of size (n+1)
void _double_poly_reverse(double_ptr P, double_srcptr Q, long n)
{
  long i;
  double_ptr R = malloc((n+1) * sizeof(double_t));
  for (i = 0 ; i <= n ; i++) {
    double_init(R + i);
    double_set(R + i, Q + n - i, MPFR_RNDN);
  }
  for (i = 0 ; i <= n ; i++) {
    double_swap(P + i, R + i);
    double_clear(R + i);
  }
}


// set P to Q(X^-1)*X^(degree(Q))
void double_poly_reverse(double_poly_t P, const double_poly_t Q)
{
  if (P != Q)
    double_poly_set(P, Q);
  _double_poly_reverse(P->coeffs, P->coeffs, P->degree);
  double_poly_normalise(P);
}
