
#include "double_poly.h"


// set P := Q^e
void double_poly_pow(double_poly_t P, const double_poly_t Q, unsigned long e)
{
  double_poly_t Qe;
  double_poly_init(Qe);
  double_poly_set_coeff_si(Qe, 0, 1);
  double_poly_t Qi;
  double_poly_init(Qi);
  double_poly_set_coeff_si(Qi, 0, 1);

  while (e != 0) {
    double_poly_mul(Qi, Qi, Q);
    if (e%2 == 1)
      double_poly_mul(Qe, Qe, Qi);
    e /= 2;
  }

  double_poly_clear(Qi);
  double_poly_clear(P);
  P->degree = Qe->degree;
  P->coeffs = Qe->coeffs;
}
