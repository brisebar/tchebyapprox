
#include "double_poly.h"


void _double_poly_derivative(double_ptr P, double_srcptr Q, long n)
{
  long i;
  for (i = 0 ; i < n ; i++)
    double_mul_si(P + i, Q + i + 1, i+1, MPFR_RNDN);
}


// set P := Q'
void double_poly_derivative(double_poly_t P, const double_poly_t Q)
{
  long n = Q->degree;

  if (n <= 0)
    double_poly_zero(P);

  else {
    double_poly_set_degree(P, n);
    _double_poly_derivative(P->coeffs, Q->coeffs, n);
    double_clear(P->coeffs + n);
    P->degree --;
  }
}
