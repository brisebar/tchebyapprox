#include "double_poly.h"


// clear P
void double_poly_clear(double_poly_t P)
{
  long n = P->degree;
  long i;
  for (i = 0 ; i <= n ; i++)
    double_clear(P->coeffs + i);
  free(P->coeffs);
}
