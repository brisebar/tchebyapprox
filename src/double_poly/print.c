
#include "double_poly.h"


void _double_poly_print(double_srcptr P, long n, const char * var)
{
  long i;
  int flag = 0;

  for (i = n ; i >=0 ; i--)
    if (!double_zero_p(P + i)) {
      if (flag)
	printf("+");
      else
	flag = 1;
      printf("%f", P[i]);
      if (i > 1)
	printf("*%s^%ld", var, i);
      else if (i == 1)
	printf("*%s", var);
    }

}


// display the polynomial P with 'var' as indeterminate symbol
// 'var' is a 0-terminated string
void double_poly_print(const double_poly_t P, const char * var)
{
  long n = P->degree;

  if (n < 0)
    printf("0");

  else
    _double_poly_print(P->coeffs, n, var);
}
