
#include "double_poly.h"


// init with P = 0
void double_poly_init(double_poly_t P)
{
  P->degree = -1;
  P->coeffs = malloc(0);
}
