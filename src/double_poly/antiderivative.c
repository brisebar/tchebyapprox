
#include "double_poly.h"


void _double_poly_antiderivative(double_ptr P, double_srcptr Q, long n)
{
  long i;
  for (i = n ; i >= 0 ; i--)
    double_div_si(P + i + 1, Q + i, i+1, MPFR_RNDU);
  double_set_si(P, 0, MPFR_RNDN);
}


// set P st P' = Q and P(0) = 0
void double_poly_antiderivative(double_poly_t P, const double_poly_t Q)
{
  long n = Q->degree;

  if (n < 0)
    double_poly_zero(P);

  else {
    double_poly_set_degree(P, n+1);
    _double_poly_antiderivative(P->coeffs, Q->coeffs, n);
  }
}
