
#include "double_poly.h"


// swap P and Q efficiently
void double_poly_swap(double_poly_t P, double_poly_t Q)
{
  long degree_buf = P->degree;
  double_ptr coeffs_buf = P->coeffs;
  P->degree = Q->degree;
  P->coeffs = Q->coeffs;
  Q->degree = degree_buf;
  Q->coeffs = coeffs_buf;
}
