
#include "double_poly.h"


void _double_poly_evaluate_si(double_t y, double_srcptr P, long n, long x)
{
  double_t t;
  double_init(t);
  double_set_si(t, x, MPFR_RNDN);
  _double_poly_evaluate_d(y, P, n, t);
  double_clear(t);
}


void _double_poly_evaluate_z(double_t y, double_srcptr P, long n, const mpz_t x)
{
  double_t t;
  double_init(t);
  double_set_z(t, x, MPFR_RNDN);
  _double_poly_evaluate_d(y, P, n, t);
  double_clear(t);
}


void _double_poly_evaluate_q(double_t y, double_srcptr P, long n, const mpq_t x)
{
  double_t t;
  double_init(t);
  double_set_q(t, x, MPFR_RNDN);
  _double_poly_evaluate_d(y, P, n, t);
  double_clear(t);
}

void _double_poly_evaluate_d(double_t y, double_srcptr P, long n, const double_t x)
{
  if (n < 0)
    double_set_si(y, 0, MPFR_RNDN);

  else {
    long i;
    double_set(y, P + n, MPFR_RNDN);
    for (i = n-1 ; i >= 0 ; i--) {
      double_mul(y, x, y, MPFR_RNDN);
      double_add(y, y, P + i, MPFR_RNDN);
    }
  }
}

void _double_poly_evaluate_fr(mpfr_t y, double_srcptr P, long n, const mpfr_t x)
{
  if (n < 0)
    mpfr_set_si(y, 0, MPFR_RNDN);

  else {
    long i;
    mpfr_set_d(y, P[n], MPFR_RNDN);
    for (i = n-1 ; i >= 0 ; i--) {
      mpfr_mul(y, x, y, MPFR_RNDN);
      mpfr_add_d(y, y, P[i], MPFR_RNDN);
    }
  }
}



// set y to P(x)
void double_poly_evaluate_si(double_t y, const double_poly_t P, long x)
{
  _double_poly_evaluate_si(y, P->coeffs, P->degree, x);
}


// set y to P(x)
void double_poly_evaluate_z(double_t y, const double_poly_t P, const mpz_t x)
{
  _double_poly_evaluate_z(y, P->coeffs, P->degree, x);
}


// set y to P(x)
void double_poly_evaluate_q(double_t y, const double_poly_t P, const mpq_t x)
{
  _double_poly_evaluate_q(y, P->coeffs, P->degree, x);
}

// set y to P(x)
void double_poly_evaluate_d(double_t y, const double_poly_t P, const double_t x)
{
  _double_poly_evaluate_d(y, P->coeffs, P->degree, x);
}

// set y to P(x)
void double_poly_evaluate_fr(mpfr_t y, const double_poly_t P, const mpfr_t x)
{
  _double_poly_evaluate_fr(y, P->coeffs, P->degree, x);
}

