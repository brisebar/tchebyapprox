
#include "double_poly.h"


void _double_poly_1norm(double_t y, double_srcptr P, long n)
{
  long i;
  double_set_si(y, 0, MPFR_RNDN);
  for (i = 0 ; i <= n ; i++)
    if (double_sgn(P + i) >= 0)
      double_add(y, y, P + i, MPFR_RNDN);
    else
      double_sub(y, y, P + i, MPFR_RNDN);
}


void double_poly_1norm(double_t y, const double_poly_t P)
{
  _double_poly_1norm(y, P->coeffs, P->degree);
}


void _double_poly_1norm_ubound(double_t y, double_srcptr P, long n)
{
  mpfr_t norm;
  mpfr_init(norm);
  mpfr_set_si(norm, 0, MPFR_RNDU);
  long i;
  for (i = 0 ; i <= n ; i++)
    if (double_sgn(P + i) >= 0)
      mpfr_add_d(norm, norm, P[i], MPFR_RNDU);
    else
      mpfr_sub_d(norm, norm, P[i], MPFR_RNDU);

  double_set_fr(y, norm, MPFR_RNDU);
  mpfr_clear(norm);
}

void double_poly_1norm_ubound(double_t y, const double_poly_t P)
{
  _double_poly_1norm_ubound(y, P->coeffs, P->degree);
}

void _double_poly_1norm_lbound(double_t y, double_srcptr P, long n)
{
  mpfr_t norm;
  mpfr_init(norm);
  mpfr_set_si(norm, 0, MPFR_RNDD);
  long i;
  for (i = 0 ; i <= n ; i++)
    if (double_sgn(P + i) >= 0)
      mpfr_add_d(norm, norm, P[i], MPFR_RNDD);
    else
      mpfr_sub_d(norm, norm, P[i], MPFR_RNDD);

  double_set_fr(y, norm, MPFR_RNDD);
  mpfr_clear(norm);
}

void double_poly_1norm_lbound(double_t y, const double_poly_t P)
{
  _double_poly_1norm_lbound(y, P->coeffs, P->degree);
}


void _double_poly_1norm_fi(mpfi_t y, double_srcptr P, long n)
{
  mpfi_set_si(y, 0);
  long i;
  for (i = 0 ; i <= n ; i++)
    if (double_sgn(P + i) >= 0)
      mpfi_add_d(y, y, P[i]);
    else
      mpfi_sub_d(y, y, P[i]);
}

void double_poly_1norm_fi(mpfi_t y, const double_poly_t P)
{
  _double_poly_1norm_fi(y, P->coeffs, P->degree);
}


