
#include "double_poly.h"


void _double_poly_scalar_mul_si(double_ptr P, double_srcptr Q, long n, long c)
{
  double d = c;
  _double_poly_scalar_mul_d(P, Q, n, &d);
}


void _double_poly_scalar_mul_z(double_ptr P, double_srcptr Q, long n, const mpz_t c)
{
  double d = mpz_get_d(c);
  _double_poly_scalar_mul_d(P, Q, n, &d);
}


void _double_poly_scalar_mul_q(double_ptr P, double_srcptr Q, long n, const mpq_t c)
{
  double d = mpq_get_d(c);
  _double_poly_scalar_mul_d(P, Q, n, &d);
}


void _double_poly_scalar_mul_d(double_ptr P, double_srcptr Q, long n, const double_t c)
{
  long i;
  for (i = 0 ; i <= n ; i++)
    double_mul(P + i, Q + i, c, MPFR_RNDN);
}


void _double_poly_scalar_mul_fr(double_ptr P, double_srcptr Q, long n, const mpfr_t c)
{
  double d = mpfr_get_d(c, MPFR_RNDN);
  _double_poly_scalar_mul_d(P, Q, n, &d);
}


void _double_poly_scalar_mul_2si(double_ptr P, double_srcptr Q, long n, long k)
{
  long i;
  for (i = 0 ; i <= n ; i++)
    double_mul_2si(P + i, Q + i, k, MPFR_RNDN);
}



// set P := c*Q
void double_poly_scalar_mul_si(double_poly_t P, const double_poly_t Q, long c)
{
  if (c == 0)
    double_poly_zero(P);
  else {
    long n = Q->degree;
    double_poly_set_degree(P, n);
    _double_poly_scalar_mul_si(P->coeffs, Q->coeffs, n, c);
  }
}


// set P := c*Q
void double_poly_scalar_mul_z(double_poly_t P, const double_poly_t Q, const mpz_t c)
{
  if (!mpz_sgn(c))
    double_poly_zero(P);
  else {
    long n = Q->degree;
    double_poly_set_degree(P, n);
    _double_poly_scalar_mul_z(P->coeffs, Q->coeffs, n, c);
  }
}


// set P := c*Q
void double_poly_scalar_mul_q(double_poly_t P, const double_poly_t Q, const mpq_t c)
{
  if (!mpq_sgn(c))
    double_poly_zero(P);
  else {
    long n = Q->degree;
    double_poly_set_degree(P, n);
    _double_poly_scalar_mul_q(P->coeffs, Q->coeffs, n, c);
  }
}

// set P := c*Q
void double_poly_scalar_mul_d(double_poly_t P, const double_poly_t Q, const double_t c)
{
  if (double_zero_p(c))
    double_poly_zero(P);
  else {
    long n = Q->degree;
    double_poly_set_degree(P, n);
    _double_poly_scalar_mul_d(P->coeffs, Q->coeffs, n, c);
  }
}

// set P := c*Q
void double_poly_scalar_mul_fr(double_poly_t P, const double_poly_t Q, const mpfr_t c)
{
  if (mpfr_zero_p(c))
    double_poly_zero(P);
  else {
    long n = Q->degree;
    double_poly_set_degree(P, n);
    _double_poly_scalar_mul_fr(P->coeffs, Q->coeffs, n, c);
  }
}


// set P := 2^k*Q
void double_poly_scalar_mul_2si(double_poly_t P, const double_poly_t Q, long k)
{
  long n = Q->degree;

  if (n < 0)
    double_poly_zero(P);

  else {
    double_poly_set_degree(P, n);
    _double_poly_scalar_mul_2si(P->coeffs, Q->coeffs, n, k);
  }
}
