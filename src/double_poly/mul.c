
#include "double_poly.h"


void _double_poly_mul(double_ptr P, double_srcptr Q, long n, double_srcptr R, long m)
{
  long i, j;
  double_ptr S = malloc((n+m+1) * sizeof(double_t));
  double_t temp;
  double_init(temp);

  for (i = 0 ; i <= n+m ; i++) {
    double_init(S + i);
    double_set_si(S + i, 0, MPFR_RNDN);
  }

  for (i = 0 ; i <= n ; i++)
    for (j = 0 ; j <= m ; j++) {
      double_mul(temp, Q + i, R + j, MPFR_RNDN);
      double_add(S + i + j, S + i + j, temp, MPFR_RNDN);
    }

  for (i = 0 ; i <= n+m ; i++) {
    double_swap(P + i, S + i);
    double_clear(S + i);
  }

  double_clear(temp);
  free(S);
}


// set P := Q * R
void double_poly_mul(double_poly_t P, const double_poly_t Q, const double_poly_t R)
{
  long n = Q->degree;
  long m = R->degree;

  if (n < 0 || m < 0)
    double_poly_zero(P);

  else {
    double_poly_set_degree(P, n+m);
    _double_poly_mul(P->coeffs, Q->coeffs, n, R->coeffs, m);
  }
}
