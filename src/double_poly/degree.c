
#include "double_poly.h"


// get the degree of P
long double_poly_degree(const double_poly_t P)
{
  return P->degree;
}
