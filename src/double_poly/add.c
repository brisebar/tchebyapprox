
#include "double_poly.h"


void _double_poly_add(double_ptr P, double_srcptr Q, long n, double_srcptr R, long m)
{
  long min_n_m = n < m ? n : m;
  long i;

  for (i = 0 ; i <= min_n_m ; i++)
    double_add(P + i, Q + i, R + i, MPFR_RNDN);

  if (P != Q)
    for (i = min_n_m+1 ; i <= n ; i++)
      double_set(P + i, Q + i, MPFR_RNDN);

  if (P != R)
    for (i = min_n_m+1 ; i <= m ; i++)
      double_set(P + i, R + i, MPFR_RNDN);
}


// set P := Q + R
void double_poly_add(double_poly_t P, const double_poly_t Q, const double_poly_t R)
{
  long n = Q->degree;
  long m = R->degree;
  long max_n_m = n < m ? m : n;

  double_poly_set_degree(P, max_n_m);
  _double_poly_add(P->coeffs, Q->coeffs, n, R->coeffs, m);
  double_poly_normalise(P);
}
