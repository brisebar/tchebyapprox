
#include "double_poly.h"


void _double_poly_monic(double_ptr P, double_srcptr Q, long n)
{
  long i;
  for (i = 0 ; i < n ; i++)
    double_div(P + i, Q + i, Q + n, MPFR_RNDN);
  double_set_si(P + i, 1, MPFR_RNDN);
}


// set P to (1/c)*Q where c is the leading coefficient of P
void double_poly_monic(double_poly_t P, const double_poly_t Q)
{
  long n = Q->degree;

  if (n < 0)
    double_poly_zero(P);

  else {
    double_poly_set_degree(P, n);
    _double_poly_monic(P->coeffs, Q->coeffs, n);
  }
}
