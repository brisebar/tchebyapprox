
#include "mpfr_chebpoly_lrintop.h"

void _mpfr_chebpoly_lrintop_scalar_mul_si(mpfr_chebpoly_ptr Llpoly, mpfr_chebpoly_ptr Lrpoly, mpfr_chebpoly_srcptr Klpoly, mpfr_chebpoly_srcptr Krpoly, long r, long c)
{
  long i;
  for (i = 0 ; i < r ; i++) {
    mpfr_chebpoly_scalar_mul_si(Llpoly + i, Klpoly + i, c);
    mpfr_chebpoly_set(Lrpoly + i, Krpoly + i);
  }
}


// set L := c * K
void mpfr_chebpoly_lrintop_scalar_mul_si(mpfr_chebpoly_lrintop_t L, const mpfr_chebpoly_lrintop_t K, long c)
{
  mpfr_chebpoly_lrintop_set_length(L, K->length);
  _mpfr_chebpoly_lrintop_scalar_mul_si(L->lpoly, L->rpoly, K->lpoly, K->rpoly, K->length, c);
}


void _mpfr_chebpoly_lrintop_scalar_mul_z(mpfr_chebpoly_ptr Llpoly, mpfr_chebpoly_ptr Lrpoly, mpfr_chebpoly_srcptr Klpoly, mpfr_chebpoly_srcptr Krpoly, long r, const mpz_t c)
{
  long i;
  for (i = 0 ; i < r ; i++) {
    mpfr_chebpoly_scalar_mul_z(Llpoly + i, Klpoly + i, c);
    mpfr_chebpoly_set(Lrpoly + i, Krpoly + i);
  }
}


// set L := c * K
void mpfr_chebpoly_lrintop_scalar_mul_z(mpfr_chebpoly_lrintop_t L, const mpfr_chebpoly_lrintop_t K, const mpz_t c)
{
  mpfr_chebpoly_lrintop_set_length(L, K->length);
  _mpfr_chebpoly_lrintop_scalar_mul_z(L->lpoly, L->rpoly, K->lpoly, K->rpoly, K->length, c);
}


void _mpfr_chebpoly_lrintop_scalar_mul_q(mpfr_chebpoly_ptr Llpoly, mpfr_chebpoly_ptr Lrpoly, mpfr_chebpoly_srcptr Klpoly, mpfr_chebpoly_srcptr Krpoly, long r, const mpq_t c)
{
  long i;
  for (i = 0 ; i < r ; i++) {
    mpfr_chebpoly_scalar_mul_q(Llpoly + i, Klpoly + i, c);
    mpfr_chebpoly_set(Lrpoly + i, Krpoly + i);
  }
}


// set L := c * K
void mpfr_chebpoly_lrintop_scalar_mul_q(mpfr_chebpoly_lrintop_t L, const mpfr_chebpoly_lrintop_t K, const mpq_t c)
{
  mpfr_chebpoly_lrintop_set_length(L, K->length);
  _mpfr_chebpoly_lrintop_scalar_mul_q(L->lpoly, L->rpoly, K->lpoly, K->rpoly, K->length, c);
}


void _mpfr_chebpoly_lrintop_scalar_mul_d(mpfr_chebpoly_ptr Llpoly, mpfr_chebpoly_ptr Lrpoly, mpfr_chebpoly_srcptr Klpoly, mpfr_chebpoly_srcptr Krpoly, long r, const double_t c)
{
  long i;
  for (i = 0 ; i < r ; i++) {
    mpfr_chebpoly_scalar_mul_d(Llpoly + i, Klpoly + i, c);
    mpfr_chebpoly_set(Lrpoly + i, Krpoly + i);
  }
}


// set L := c * K
void mpfr_chebpoly_lrintop_scalar_mul_d(mpfr_chebpoly_lrintop_t L, const mpfr_chebpoly_lrintop_t K, const double_t c)
{
  mpfr_chebpoly_lrintop_set_length(L, K->length);
  _mpfr_chebpoly_lrintop_scalar_mul_d(L->lpoly, L->rpoly, K->lpoly, K->rpoly, K->length, c);
}


void _mpfr_chebpoly_lrintop_scalar_mul_fr(mpfr_chebpoly_ptr Llpoly, mpfr_chebpoly_ptr Lrpoly, mpfr_chebpoly_srcptr Klpoly, mpfr_chebpoly_srcptr Krpoly, long r, const mpfr_t c)
{
  long i;
  for (i = 0 ; i < r ; i++) {
    mpfr_chebpoly_scalar_mul_fr(Llpoly + i, Klpoly + i, c);
    mpfr_chebpoly_set(Lrpoly + i, Krpoly + i);
  }
}


// set L := c * K
void mpfr_chebpoly_lrintop_scalar_mul_fr(mpfr_chebpoly_lrintop_t L, const mpfr_chebpoly_lrintop_t K, const mpfr_t c)
{
  mpfr_chebpoly_lrintop_set_length(L, K->length);
  _mpfr_chebpoly_lrintop_scalar_mul_fr(L->lpoly, L->rpoly, K->lpoly, K->rpoly, K->length, c);
}


void _mpfr_chebpoly_lrintop_scalar_mul_2si(mpfr_chebpoly_ptr Llpoly, mpfr_chebpoly_ptr Lrpoly, mpfr_chebpoly_srcptr Klpoly, mpfr_chebpoly_srcptr Krpoly, long r, long k)
{
  long i;
  for (i = 0 ; i < r ; i++) {
    mpfr_chebpoly_scalar_mul_2si(Llpoly + i, Klpoly + i, k);
    mpfr_chebpoly_set(Lrpoly + i, Krpoly + i);
  }
}


// set L := 2^k * K
void mpfr_chebpoly_lrintop_scalar_mul_2si(mpfr_chebpoly_lrintop_t L, const mpfr_chebpoly_lrintop_t K, long k)
{
  mpfr_chebpoly_lrintop_set_length(L, K->length);
  _mpfr_chebpoly_lrintop_scalar_mul_2si(L->lpoly, L->rpoly, K->lpoly, K->rpoly, K->length, k);
}
