
#include "mpfr_chebpoly_lrintop.h"


// get bandmatrix
void mpfr_chebpoly_lrintop_get_bandmatrix(mpfr_bandmatrix_t M, const mpfr_chebpoly_lrintop_t K, long N)
{
  mpfr_chebpoly_intop_t KK;
  mpfr_chebpoly_intop_init(KK);
  mpfr_chebpoly_lrintop_get_intop(KK, K);
  mpfr_chebpoly_intop_get_bandmatrix(M, KK, N);
  mpfr_chebpoly_intop_clear(KK);
}
