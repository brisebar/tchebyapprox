
#include "mpfr_chebpoly_lrintop.h"

void _mpfr_chebpoly_lrintop_scalar_div_si(mpfr_chebpoly_ptr Llpoly, mpfr_chebpoly_ptr Lrpoly, mpfr_chebpoly_srcptr Klpoly, mpfr_chebpoly_srcptr Krpoly, long r, long c)
{
  long i;
  for (i = 0 ; i < r ; i++) {
    mpfr_chebpoly_scalar_div_si(Llpoly + i, Klpoly + i, c);
    mpfr_chebpoly_set(Lrpoly + i, Krpoly + i);
  }
}


// set L := K / c
void mpfr_chebpoly_lrintop_scalar_div_si(mpfr_chebpoly_lrintop_t L, const mpfr_chebpoly_lrintop_t K, long c)
{
  mpfr_chebpoly_lrintop_set_length(L, K->length);
  _mpfr_chebpoly_lrintop_scalar_div_si(L->lpoly, L->rpoly, K->lpoly, K->rpoly, K->length, c);
}


void _mpfr_chebpoly_lrintop_scalar_div_z(mpfr_chebpoly_ptr Llpoly, mpfr_chebpoly_ptr Lrpoly, mpfr_chebpoly_srcptr Klpoly, mpfr_chebpoly_srcptr Krpoly, long r, const mpz_t c)
{
  long i;
  for (i = 0 ; i < r ; i++) {
    mpfr_chebpoly_scalar_div_z(Llpoly + i, Klpoly + i, c);
    mpfr_chebpoly_set(Lrpoly + i, Krpoly + i);
  }
}


// set L := K / c
void mpfr_chebpoly_lrintop_scalar_div_z(mpfr_chebpoly_lrintop_t L, const mpfr_chebpoly_lrintop_t K, const mpz_t c)
{
  mpfr_chebpoly_lrintop_set_length(L, K->length);
  _mpfr_chebpoly_lrintop_scalar_div_z(L->lpoly, L->rpoly, K->lpoly, K->rpoly, K->length, c);
}


void _mpfr_chebpoly_lrintop_scalar_div_q(mpfr_chebpoly_ptr Llpoly, mpfr_chebpoly_ptr Lrpoly, mpfr_chebpoly_srcptr Klpoly, mpfr_chebpoly_srcptr Krpoly, long r, const mpq_t c)
{
  long i;
  for (i = 0 ; i < r ; i++) {
    mpfr_chebpoly_scalar_div_q(Llpoly + i, Klpoly + i, c);
    mpfr_chebpoly_set(Lrpoly + i, Krpoly + i);
  }
}


// set L := K / c
void mpfr_chebpoly_lrintop_scalar_div_q(mpfr_chebpoly_lrintop_t L, const mpfr_chebpoly_lrintop_t K, const mpq_t c)
{
  mpfr_chebpoly_lrintop_set_length(L, K->length);
  _mpfr_chebpoly_lrintop_scalar_div_q(L->lpoly, L->rpoly, K->lpoly, K->rpoly, K->length, c);
}


void _mpfr_chebpoly_lrintop_scalar_div_d(mpfr_chebpoly_ptr Llpoly, mpfr_chebpoly_ptr Lrpoly, mpfr_chebpoly_srcptr Klpoly, mpfr_chebpoly_srcptr Krpoly, long r, const double_t c)
{
  long i;
  for (i = 0 ; i < r ; i++) {
    mpfr_chebpoly_scalar_div_d(Llpoly + i, Klpoly + i, c);
    mpfr_chebpoly_set(Lrpoly + i, Krpoly + i);
  }
}


// set L := K / c
void mpfr_chebpoly_lrintop_scalar_div_d(mpfr_chebpoly_lrintop_t L, const mpfr_chebpoly_lrintop_t K, const double_t c)
{
  mpfr_chebpoly_lrintop_set_length(L, K->length);
  _mpfr_chebpoly_lrintop_scalar_div_d(L->lpoly, L->rpoly, K->lpoly, K->rpoly, K->length, c);
}


void _mpfr_chebpoly_lrintop_scalar_div_fr(mpfr_chebpoly_ptr Llpoly, mpfr_chebpoly_ptr Lrpoly, mpfr_chebpoly_srcptr Klpoly, mpfr_chebpoly_srcptr Krpoly, long r, const mpfr_t c)
{
  long i;
  for (i = 0 ; i < r ; i++) {
    mpfr_chebpoly_scalar_div_fr(Llpoly + i, Klpoly + i, c);
    mpfr_chebpoly_set(Lrpoly + i, Krpoly + i);
  }
}


// set L := K / c
void mpfr_chebpoly_lrintop_scalar_div_fr(mpfr_chebpoly_lrintop_t L, const mpfr_chebpoly_lrintop_t K, const mpfr_t c)
{
  mpfr_chebpoly_lrintop_set_length(L, K->length);
  _mpfr_chebpoly_lrintop_scalar_div_fr(L->lpoly, L->rpoly, K->lpoly, K->rpoly, K->length, c);
}


void _mpfr_chebpoly_lrintop_scalar_div_2si(mpfr_chebpoly_ptr Llpoly, mpfr_chebpoly_ptr Lrpoly, mpfr_chebpoly_srcptr Klpoly, mpfr_chebpoly_srcptr Krpoly, long r, long k)
{
  long i;
  for (i = 0 ; i < r ; i++) {
    mpfr_chebpoly_scalar_div_2si(Llpoly + i, Klpoly + i, k);
    mpfr_chebpoly_set(Lrpoly + i, Krpoly + i);
  }
}


// set L := 2^-k * K
void mpfr_chebpoly_lrintop_scalar_div_2si(mpfr_chebpoly_lrintop_t L, const mpfr_chebpoly_lrintop_t K, long k)
{
  mpfr_chebpoly_lrintop_set_length(L, K->length);
  _mpfr_chebpoly_lrintop_scalar_div_2si(L->lpoly, L->rpoly, K->lpoly, K->rpoly, K->length, k);
}
