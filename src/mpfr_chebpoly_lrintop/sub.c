
#include "mpfr_chebpoly_lrintop.h"


// Mlpoly, Mrpoly must be freshed allocated arrays of mpfr_chebpoly of size r1+r2
void _mpfr_chebpoly_lrintop_sub(mpfr_chebpoly_ptr Mlpoly, mpfr_chebpoly_ptr Mrpoly, mpfr_chebpoly_srcptr Klpoly, mpfr_chebpoly_srcptr Krpoly, long r1, mpfr_chebpoly_srcptr Llpoly, mpfr_chebpoly_srcptr Lrpoly, long r2)
{
  long i;

  for (i = 0 ; i < r1 ; i++) {
    mpfr_chebpoly_set(Mlpoly + i, Klpoly + i);
    mpfr_chebpoly_set(Mrpoly + i, Krpoly + i);
  }

  for (i = 0 ; i < r2 ; i++) {
    mpfr_chebpoly_neg(Mlpoly + r1 + i, Llpoly + i);
    mpfr_chebpoly_set(Mrpoly + r1 + i, Lrpoly + i);
  }

}


void mpfr_chebpoly_lrintop_sub(mpfr_chebpoly_lrintop_t M, const mpfr_chebpoly_lrintop_t K, const mpfr_chebpoly_lrintop_t L)
{
  mpfr_chebpoly_lrintop_t K_L;
  mpfr_chebpoly_lrintop_init(K_L);

  mpfr_chebpoly_lrintop_set_length(K_L, K->length + L->length);
  _mpfr_chebpoly_lrintop_sub(K_L->lpoly, K_L->rpoly, K->lpoly, K->rpoly, K->length, L->lpoly, L->rpoly, L->length);

  mpfr_chebpoly_lrintop_swap(M, K_L);
  mpfr_chebpoly_lrintop_clear(K_L);
}
