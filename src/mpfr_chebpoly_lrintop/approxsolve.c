
#include "mpfr_chebpoly_lrintop.h"


// solve F s.t. F + K(F) = G by truncating K at order N
void mpfr_chebpoly_lrintop_approxsolve(mpfr_chebpoly_t F, const mpfr_chebpoly_lrintop_t K, const mpfr_chebpoly_t G, long N)
{
  // convert K to mpfr_chebpoly_intop_t KK
  mpfr_chebpoly_intop_t KK;
  mpfr_chebpoly_intop_init(KK);
  mpfr_chebpoly_lrintop_get_intop(KK, K);

  // negate due to convention used by mpfr_chebpoly_intop_t
  long i;
  for (i = 0 ; i < KK->order ; i++)
    mpfr_chebpoly_neg(KK->alpha + i, KK->alpha + i);

  // solve system
  mpfr_chebpoly_intop_approxsolve(F, KK, G, N);

  mpfr_chebpoly_intop_clear(KK);
}
