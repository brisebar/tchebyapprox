
#include "mpfr_chebpoly_lrintop.h"


void mpfr_chebpoly_lrintop_set(mpfr_chebpoly_lrintop_t L, const mpfr_chebpoly_lrintop_t K)
{
  long r = K->length;
  mpfr_chebpoly_lrintop_set_length(L, r);

  long i;
  for (i = 0 ; i < r ; i++) {
    mpfr_chebpoly_set(L->lpoly + i, K->lpoly + i);
    mpfr_chebpoly_set(L->rpoly + i, K->rpoly + i);
  }
}


void mpfr_chebpoly_lrintop_set_double_chebpoly_lrintop(mpfr_chebpoly_lrintop_t L, const double_chebpoly_lrintop_t K)
{
  long r = K->length;
  mpfr_chebpoly_lrintop_set_length(L, r);

  long i;
  for (i = 0 ; i < r ; i++) {
    mpfr_chebpoly_set_double_chebpoly(L->lpoly + i, K->lpoly + i);
    mpfr_chebpoly_set_double_chebpoly(L->rpoly + i, K->rpoly + i);
  }
}


void mpfr_chebpoly_lrintop_get_double_chebpoly_lrintop(double_chebpoly_lrintop_t L, const mpfr_chebpoly_lrintop_t K)
{
  long r = K->length;
  double_chebpoly_lrintop_set_length(L, r);

  long i;
  for (i = 0 ; i < r ; i++) {
    mpfr_chebpoly_get_double_chebpoly(L->lpoly + i, K->lpoly + i);
    mpfr_chebpoly_get_double_chebpoly(L->rpoly + i, K->rpoly + i);
  }
}
