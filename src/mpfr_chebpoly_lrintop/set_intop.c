
#include "mpfr_chebpoly_lrintop.h"


/* convert from mpfr_chebpoly_intop_t */
void mpfr_chebpoly_lrintop_set_intop(mpfr_chebpoly_lrintop_t M, const mpfr_chebpoly_intop_t K)
{
  long r = K->order;
  mpfr_chebpoly_lrintop_set_length(M, r);
  long i;
  for (i = 0 ; i < r ; i++) {
    mpfr_chebpoly_set(M->lpoly + i, K->alpha + i);
    mpfr_chebpoly_zero(M->rpoly + i);
    mpfr_chebpoly_set_coeff_si(M->rpoly + i, i, 1);
  }
}


/* convert to mpfr_chebpoly_intop_t */
void mpfr_chebpoly_lrintop_get_intop(mpfr_chebpoly_intop_t M, const mpfr_chebpoly_lrintop_t K)
{
  long r = K->length;
  long i, j;

  // order of M = max degree rpoly in s + 1
  long s = 0;
  for (i = 0 ; i < r ; i++)
    s = s < K->rpoly[i].degree ? K->rpoly[i].degree : s;
  s++;
  mpfr_chebpoly_intop_set_order(M, s);
  for (i = 0 ; i < s ; i++)
    mpfr_chebpoly_zero(M->alpha + i);

  mpfr_chebpoly_t Temp;
  mpfr_chebpoly_init(Temp);
  for (i = 0 ; i < r ; i++)
    for (j = 0 ; j <= K->rpoly[i].degree ; j++) {
      mpfr_chebpoly_scalar_mul_fr(Temp, K->lpoly + i, K->rpoly[i].coeffs + j);
      mpfr_chebpoly_add(M->alpha + j, M->alpha + j, Temp);
    }

  mpfr_chebpoly_clear(Temp);
}
