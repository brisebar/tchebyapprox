
#include "mpfr_chebpoly_lrintop.h"


void mpfr_chebpoly_lrintop_clear(mpfr_chebpoly_lrintop_t K)
{
  long r = K->length;
  long i;
  for (i = 0 ; i < r ; i++) {
    mpfr_chebpoly_clear(K->lpoly + i);
    mpfr_chebpoly_clear(K->rpoly + i);
  }
  free(K->lpoly);
  free(K->rpoly);
}
