
#include "mpfr_chebpoly_lrintop.h"

typedef mpfi_ptr * mpfi_ptr_ptr;


// crude bound on the sum
void mpfr_chebpoly_lrintop_lr1norm(mpfr_t b, const mpfr_chebpoly_lrintop_t K)
{
  mpfr_t lbi, rbi;
  mpfr_init(lbi);
  mpfr_init(rbi);
  mpfr_set_si(b, 0, MPFR_RNDN);
  long i;

  for (i = 0 ; i < K->length ; i++) {
    mpfr_chebpoly_1norm(lbi, K->lpoly + i);
    mpfr_chebpoly_1norm(rbi, K->rpoly + i);
    mpfr_mul(lbi, lbi, rbi, MPFR_RNDU);
    mpfr_add(b, b, lbi, MPFR_RNDU);
  }
  mpfr_mul_2si(b, b, 1, MPFR_RNDU);

  mpfr_clear(lbi);
  mpfr_clear(rbi);
}


// convert to 2-dimensional polynomial in Chebyshev basis and use 1 norm
void mpfr_chebpoly_lrintop_1norm(mpfr_t b, const mpfr_chebpoly_lrintop_t K)
{
  long r = K->length;
  long i, j, k, nk, mk;
  mpfi_t lc, rc;
  mpfi_init(lc);
  mpfi_init(rc);
  mpfr_set_si(b, 0, MPFR_RNDU);

  // n = maximum degree in t and m = maximum degree in s
  long n = -1;
  long m = -1;
  for (k = 0 ; k < r ; k++) {
    nk = mpfr_chebpoly_degree(K->lpoly + k);
    mk = mpfr_chebpoly_degree(K->rpoly + k);
    if (nk > n)
      n = nk;
    if (mk > m)
      m = mk;
  }

  // initialize matrix A of dim (n+1)×(m+1)
  mpfi_ptr_ptr A = malloc((n+1) * sizeof(mpfi_ptr));
  for (i = 0 ; i <= n ; i++) {
    A[i] = malloc((m+1) * sizeof(mpfi_t));
    for (j = 0 ; j <= m ; j++) {
      mpfi_init(A[i] + j);
      mpfi_set_si(A[i] + j, 0);
    }
  }

  // fill A with coefficients of the bivariate kernel
  for (k = 0 ; k < r ; k++) {
    nk = mpfr_chebpoly_degree(K->lpoly + k);
    mk = mpfr_chebpoly_degree(K->rpoly + k);
    for (i = 0 ; i <= nk ; i++) {
      mpfi_set_fr(lc, K->lpoly[k].coeffs + i);
      for (j = 0 ; j <= mk ; j++) {
        mpfi_set_fr(rc, K->rpoly[k].coeffs + j);
        mpfi_mul(rc, lc, rc);
        mpfi_add(A[i] + j, A[i] + j, rc);
      }
    }
  }

  // compute 1 norm and delete matrix A
  for (i = 0 ; i <= n ; i++) {
    for (j = 0 ; j <= m ; j++) {
      mpfi_abs(A[i] + j, A[i] + j);
      mpfr_add(b, b, &A[i][j].right, MPFR_RNDU);
      mpfi_clear(A[i] + j);
    }
    free(A[i]);
  }
  mpfr_mul_2si(b, b, 1, MPFR_RNDU);

  free(A);
  mpfi_clear(lc);
  mpfi_clear(rc);
}
