
#include "mpfr_chebpoly_lrintop.h"


// Mlpoly, Mrpoly must be freshed allocated arrays of mpfr_chebpoly of size r1+r2
void _mpfr_chebpoly_lrintop_comp(mpfr_chebpoly_ptr Mlpoly, mpfr_chebpoly_ptr Mrpoly, mpfr_chebpoly_srcptr Klpoly, mpfr_chebpoly_srcptr Krpoly, long r1, mpfr_chebpoly_srcptr Llpoly, mpfr_chebpoly_srcptr Lrpoly, long r2)
{
  mpfr_chebpoly_t P, Q;
  mpfr_chebpoly_init(P);
  mpfr_chebpoly_init(Q);
  long i, j;

  // initialization
  for (i = 0 ; i < r1 ; i++) {
    mpfr_chebpoly_set(Mlpoly + i, Klpoly + i);
    mpfr_chebpoly_zero(Mrpoly + i);
  }

  for (j = 0 ; j < r2 ; j++) {
    mpfr_chebpoly_zero(Mlpoly + r1 + j);
    mpfr_chebpoly_set(Mrpoly + r1 + j, Lrpoly + j);
  }

  // composition loop
  for (i = 0 ; i < r1 ; i++)
    for (j = 0 ; j < r2 ; j++) {

      // primitive of middle product
      mpfr_chebpoly_mul(P, Krpoly + i, Llpoly + j);
      mpfr_chebpoly_antiderivative(P, P);

      // distribute to the left
      mpfr_chebpoly_mul(Q, Klpoly + i, P);
      mpfr_chebpoly_add(Mlpoly + r1 + j, Mlpoly + r1 + j, Q);

      // distribute to the right
      mpfr_chebpoly_mul(Q, P, Lrpoly + j);
      mpfr_chebpoly_sub(Mrpoly + i, Mrpoly + i, Q);

    }

  mpfr_chebpoly_clear(P);
  mpfr_chebpoly_clear(Q);

}


// set M := K ∘ L
void mpfr_chebpoly_lrintop_comp(mpfr_chebpoly_lrintop_t M, const mpfr_chebpoly_lrintop_t K, const mpfr_chebpoly_lrintop_t L)
{
  mpfr_chebpoly_lrintop_t K_L;
  mpfr_chebpoly_lrintop_init(K_L);

  mpfr_chebpoly_lrintop_set_length(K_L, K->length + L->length);
  _mpfr_chebpoly_lrintop_comp(K_L->lpoly, K_L->rpoly, K->lpoly, K->rpoly, K->length, L->lpoly, L->rpoly, L->length);

  mpfr_chebpoly_lrintop_swap(M, K_L);
  mpfr_chebpoly_lrintop_clear(K_L);
}
