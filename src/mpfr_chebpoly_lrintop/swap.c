
#include "mpfr_chebpoly_lrintop.h"


void mpfr_chebpoly_lrintop_swap(mpfr_chebpoly_lrintop_t K, mpfr_chebpoly_lrintop_t L)
{
  long length_buf = K->length;
  mpfr_chebpoly_ptr lpoly_buf = K->lpoly;
  mpfr_chebpoly_ptr rpoly_buf = K->rpoly;

  K->length = L->length;
  K->lpoly = L->lpoly;
  K->rpoly = L->rpoly;

  L->length = length_buf;
  L->lpoly = lpoly_buf;
  L->rpoly = rpoly_buf;
}
