
#include "mpfr_chebpoly_lrintop.h"


void _mpfr_chebpoly_lrintop_neg(mpfr_chebpoly_ptr Llpoly, mpfr_chebpoly_ptr Lrpoly, mpfr_chebpoly_srcptr Klpoly, mpfr_chebpoly_srcptr Krpoly, long r)
{
  long i;
  for (i = 0 ; i < r ; i++) {
    mpfr_chebpoly_neg(Llpoly + i, Klpoly + i);
    mpfr_chebpoly_set(Lrpoly + i, Krpoly + i);
  }
}

void mpfr_chebpoly_lrintop_neg(mpfr_chebpoly_lrintop_t L, const mpfr_chebpoly_lrintop_t K)
{
  mpfr_chebpoly_lrintop_set_length(L, K->length);
  _mpfr_chebpoly_lrintop_neg(L->lpoly, L->rpoly, K->lpoly, K->rpoly, K->length);
}
