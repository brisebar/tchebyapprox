
#include "mpfr_chebpoly_lrintop.h"


void mpfr_chebpoly_lrintop_init(mpfr_chebpoly_lrintop_t K)
{
  K->length = 0;
  K->lpoly = malloc(0);
  K->rpoly = malloc(0);
}
