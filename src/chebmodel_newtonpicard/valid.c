
#include "chebmodel_newtonpicard.h"

/* validating a candidate solution */
// x-D and D-x forms

// a = LODE coefficients
// r = LODE order
// K = associated integral operator
// R = resolvent kernel approximation
// Tbound = rigorous bound for Newton-Picard operator computed by
//       chebmodel_newtonpicard_xD_Tbound
// g = right hand side
// f = approximate solution to be validated
// bound = rigorously computed error bound

void chebmodel_newtonpicard_valid_aux(mpfr_t bound, const chebmodel_lrintop_t K, const mpfr_t Tbound, const chebmodel_lrintop_t R, const chebmodel_t g, const mpfr_chebpoly_t f)
{
  chebmodel_t delta, temp;
  chebmodel_init(delta);
  chebmodel_init(temp);
  mpfr_t d;
  mpfr_init(d);

  // compute delta := f-T(f) and defect d := ||delta||
  chebmodel_lrintop_evaluate_fr(temp, K, f);
  chebmodel_set_mpfr_chebpoly(delta, f);
  chebmodel_add(delta, delta, temp);
  chebmodel_sub(delta, delta, g); // delta = f + K(f) - g
  chebmodel_lrintop_evaluate_cm(temp, R, delta);
  chebmodel_add(delta, delta, temp); // delta = (I+R)(f+K(f)-g)
  chebmodel_1norm_ubound(d, delta); // d = ||delta||

  // final bound d/(1-Tbound)
  mpfr_si_sub(bound, 1, Tbound, MPFR_RNDD);
  mpfr_div(bound, d, bound, MPFR_RNDU);

  // clear variables
  chebmodel_clear(delta);
  chebmodel_clear(temp);
  mpfr_clear(d);
}


// a = LODE coefficients
// r = LODE order
// K = associated integral operator
// g = right hand side
// f = approximate solution to be validated

void chebmodel_newtonpicard_xD_valid(mpfr_t bound, chebmodel_srcptr a, long r, const chebmodel_t g, const mpfr_chebpoly_t f)
{
  // integral operators
  chebmodel_lrintop_t K, R;
  chebmodel_lrintop_init(K);
  chebmodel_lrintop_init(R);
  chebmodel_lrintop_set_lode_xD(K, a, r);

  // contracting operator
  mpfr_t Tbound;
  mpfr_init(Tbound);
  chebmodel_newtonpicard_xD_Tbound(Tbound, R, a, r);

  // compute bound
  chebmodel_newtonpicard_valid_aux(bound, K, Tbound, R, g, f);

  // clear variables
  mpfr_clear(Tbound);
  chebmodel_lrintop_clear(K);
  chebmodel_lrintop_clear(R);
}


void chebmodel_newtonpicard_Dx_valid(mpfr_t bound, chebmodel_srcptr a, long r, const chebmodel_t g, const mpfr_chebpoly_t f)
{
  // integral operators
  chebmodel_lrintop_t K, R;
  chebmodel_lrintop_init(K);
  chebmodel_lrintop_init(R);
  chebmodel_lrintop_set_lode_Dx(K, a, r);

  // contracting operator
  mpfr_t Tbound;
  mpfr_init(Tbound);
  chebmodel_newtonpicard_Dx_Tbound(Tbound, R, a, r);

  // compute bound
  chebmodel_newtonpicard_valid_aux(bound, K, Tbound, R, g, f);

  // clear variables
  mpfr_clear(Tbound);
  chebmodel_lrintop_clear(K);
  chebmodel_lrintop_clear(R);
}
