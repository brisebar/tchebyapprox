
#include "chebmodel_newtonpicard.h"

/* compute approximation and validate it */

// a = LODE coefficients
// r = LODE order
// h = rhs
// v = initial conditions
// N = approximation degree
// return :
// f = approximate solution
// bound = rigorously computed error bound

// compute polynomial approx f and error bound for y^(r) with y solution of
// y^(r) + a_{r-1} y^(r-1) + ... + a_0 y = h
// y^(i)(-1) = v[i]  0 <= i < r

void chebmodel_newtonpicard_xD_approxvalid(chebmodel_t f, chebmodel_srcptr a, long r, const chebmodel_t h, mpfi_srcptr v, long N)
{
  // integral operator
  chebmodel_lrintop_t K, R;
  chebmodel_lrintop_init(K);
  chebmodel_lrintop_init(R);
  chebmodel_lrintop_set_lode_xD(K, a, r);

  // right hand side
  chebmodel_t g;
  chebmodel_init(g);
  chebmodel_lrintop_set_lode_xD_rhs(g, a, r, h, v);

  // approximate solution
  mpfr_chebpoly_t ff;
  mpfr_chebpoly_init(ff);
  mpfr_chebpoly_lrintop_t(Kfr);
  mpfr_chebpoly_lrintop_init(Kfr);
  chebmodel_lrintop_get_mpfr_chebpoly_lrintop(Kfr, K);
  mpfr_chebpoly_t gfr;
  mpfr_chebpoly_init(gfr);
  chebmodel_get_mpfr_chebpoly(gfr, g);
  mpfr_chebpoly_lrintop_approxsolve(ff, Kfr, gfr, N);

  // contracting operator
  mpfr_t Tbound;
  mpfr_init(Tbound);
  chebmodel_newtonpicard_xD_Tbound(Tbound, R, a, r);

  // compute bound
  chebmodel_set_mpfr_chebpoly(f, ff);
  chebmodel_newtonpicard_valid_aux(f->rem, K, Tbound, R, g, ff);

  // clear variables
  mpfr_chebpoly_clear(ff);
  chebmodel_lrintop_clear(K);
  chebmodel_lrintop_clear(R);
  mpfr_chebpoly_lrintop_clear(Kfr);
  chebmodel_clear(g);
  mpfr_chebpoly_clear(gfr);
  mpfr_clear(Tbound);
}

// compute polynomial approx f and error bound for y solution of
// (-D)^(r)y + (-D)^(r-1)(a_{r-1} y) + ... + a_0 y = h
// y^[i](-1) = w[i]  0 <= i < r
// where y^[0] = y and y^[i+1] = a_{r-1-i} y - Dy^[i]

void chebmodel_newtonpicard_Dx_approxvalid(chebmodel_t f, chebmodel_srcptr a, long r, const chebmodel_t h, mpfi_srcptr w, long N)
{
  // integral operator
  chebmodel_lrintop_t K, R;
  chebmodel_lrintop_init(K);
  chebmodel_lrintop_init(R);
  chebmodel_lrintop_set_lode_Dx(K, a, r);

  // right hand side
  chebmodel_t g;
  chebmodel_init(g);
  chebmodel_lrintop_set_lode_Dx_rhs(g, r, h, w);

  // approximate solution
  mpfr_chebpoly_t ff;
  mpfr_chebpoly_init(ff);
  mpfr_chebpoly_lrintop_t(Kfr);
  mpfr_chebpoly_lrintop_init(Kfr);
  chebmodel_lrintop_get_mpfr_chebpoly_lrintop(Kfr, K);
  mpfr_chebpoly_t gfr;
  mpfr_chebpoly_init(gfr);
  chebmodel_get_mpfr_chebpoly(gfr, g);
  mpfr_chebpoly_lrintop_approxsolve(ff, Kfr, gfr, N);

  // contracting operator
  mpfr_t Tbound;
  mpfr_init(Tbound);
  chebmodel_newtonpicard_Dx_Tbound(Tbound, R, a, r);

  // compute bound
  chebmodel_set_mpfr_chebpoly(f, ff);
  chebmodel_newtonpicard_valid_aux(f->rem, K, Tbound, R, g, ff);

  // clear variables
  mpfr_chebpoly_clear(ff);
  chebmodel_lrintop_clear(K);
  chebmodel_lrintop_clear(R);
  mpfr_chebpoly_lrintop_clear(Kfr);
  chebmodel_clear(g);
  mpfr_chebpoly_clear(gfr);
  mpfr_clear(Tbound);
}
