
#include "mpfi_chebpoly_vec_intop.h"


void mpfi_chebpoly_vec_intop_initvals(mpfi_chebpoly_vec_t G, const mpfi_chebpoly_vec_lode_t L, mpfi_ptr_ptr I)
{
  long n = L->dim;
  mpfi_chebpoly_vec_set_dim(G, n);
  mpfi_chebpoly_vec_zero(G);
  long i, j;
  mpfi_chebpoly_lode_t Lij;
  mpfi_chebpoly_lode_init(Lij);
  mpfi_chebpoly_t F;
  mpfi_chebpoly_init(F);

  for (i = 0 ; i < n ; i++)
    for (j = 0 ; j < n ; j++) {
      mpfi_chebpoly_vec_lode_get_mpfi_chebpoly_lode(Lij, L, i, j);
      mpfi_chebpoly_intop_initvals(F, Lij, I[j]);
      mpfi_chebpoly_add(G->poly + i, G->poly + i, F);
    }
  mpfi_chebpoly_lode_clear(Lij);
  mpfi_chebpoly_clear(F);
}


