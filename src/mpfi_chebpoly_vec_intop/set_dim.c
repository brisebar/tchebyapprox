
#include "mpfi_chebpoly_vec_intop.h"


void mpfi_chebpoly_vec_intop_set_dim(mpfi_chebpoly_vec_intop_t K, long dim)
{
  mpfi_chebpoly_vec_intop_clear(K);
  K->dim = dim;
  long i, j;

  K->intop = malloc(dim * sizeof(mpfi_chebpoly_intop_ptr));
  for (i = 0 ; i < dim ; i++) {
    K->intop[i] = malloc(dim * sizeof(mpfi_chebpoly_intop_t));
    for (j = 0 ; j < dim ; j++)
      mpfi_chebpoly_intop_init(K->intop[i] + j);
  }
}

