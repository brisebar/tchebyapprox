
#include "mpfi_chebpoly_vec_intop.h"


void mpfi_chebpoly_vec_intop_clear(mpfi_chebpoly_vec_intop_t K)
{
  long n = K->dim;
  long i, j;
  for (i = 0 ; i < n ; i++) {
    for (j = 0 ; j < n ; j++)
      mpfi_chebpoly_intop_clear(K->intop[i] + j);
    free(K->intop[i]);
  }
  free(K->intop);
}
