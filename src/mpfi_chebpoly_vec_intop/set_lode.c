
#include "mpfi_chebpoly_vec_intop.h"


void mpfi_chebpoly_vec_intop_set_lode(mpfi_chebpoly_vec_intop_t K, const mpfi_chebpoly_vec_lode_t L)
{
  long n = L->dim;
  mpfi_chebpoly_vec_intop_set_dim(K, n);
  long i, j;
  mpfi_chebpoly_lode_t Lij;
  mpfi_chebpoly_lode_init(Lij);
  
  for (i = 0 ; i < n ; i++)
    for (j = 0 ; j < n ; j++) {
      mpfi_chebpoly_vec_lode_get_mpfi_chebpoly_lode(Lij, L, i, j);
      mpfi_chebpoly_intop_set_lode(K->intop[i] + j, Lij);
    }

  mpfi_chebpoly_lode_clear(Lij);
}


