
#include "mpfi_chebpoly_vec_intop.h"


void mpfi_chebpoly_vec_intop_swap(mpfi_chebpoly_vec_intop_t K, mpfi_chebpoly_vec_intop_t N)
{
  long dim_buf = K->dim;
  mpfi_chebpoly_intop_struct ** intop_buf = K->intop;

  K->dim = N->dim;
  K->intop = N->intop;

  N->dim = dim_buf;
  N->intop = intop_buf;
}
