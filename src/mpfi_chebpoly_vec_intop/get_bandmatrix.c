
#include "mpfi_chebpoly_vec_intop.h"


void mpfi_chebpoly_vec_intop_get_bandmatrix(mpfi_bandmatrix_t M, const mpfi_chebpoly_vec_intop_t K, long N)
{
  long n = K->dim;
  long h = mpfi_chebpoly_vec_intop_Hwidth(K);
  long d = mpfi_chebpoly_vec_intop_Dwidth(K);
  long i, j, k, l, hij, dij, lmin, lmax;

  mpfi_bandmatrix_set_params(M, n * (N + 1), h, d);
  mpfi_bandmatrix_zero(M);
  mpfi_bandmatrix_t Mij;
  mpfi_bandmatrix_init(Mij);

  for (i = 0 ; i < n ; i++)
    for (j = 0 ; j < n ; j++) {
      mpfi_chebpoly_intop_get_bandmatrix(Mij, K->intop[i] + j, N);
      hij = Mij->Hwidth;
      dij = Mij->Dwidth;
      // initial coefficients
      for (k = 0 ; k <= N ; k++) {
	lmin = 0;
	lmax = hij <= N ? hij-1 : N;
	for (l = lmin ; l <= lmax ; l++)
	  mpfi_set(M->H[n*l+i] + n*k+j, Mij->H[l] + k);
      }
      // diagonal coefficients
      for (k = 0 ; k <= N ; k++) {
        lmin = k-dij < 0 ? -k : -dij;
	lmax = k+dij > N ? N-k : dij;
	for (l = lmin ; l <= lmax ; l++)
	  mpfi_set(M->D[n*k+i] + d+n*l+j-i, Mij->D[k] + dij+l);
      }
    }
  
  mpfi_bandmatrix_normalise(M); 

  mpfi_bandmatrix_clear(Mij);
}


