
#include "mpfi_chebpoly_vec_intop.h"


void mpfi_chebpoly_vec_intop_rhs(mpfi_chebpoly_vec_t G, const mpfi_chebpoly_vec_lode_t L, const mpfi_chebpoly_vec_t g, mpfi_ptr_ptr I)
{
  mpfi_chebpoly_vec_intop_initvals(G, L, I);
  mpfi_chebpoly_vec_add(G, G, g);
}

