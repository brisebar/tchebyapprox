
#include "chebmodel_vec_intop_newton.h"


void chebmodel_vec_intop_newton_solve_fr(chebmodel_vec_t P, const chebmodel_vec_intop_t K, const chebmodel_vec_t G, long N)
{
  long n = K->dim;
  long i;

  mpfr_chebpoly_vec_intop_t Kfr;
  mpfr_chebpoly_vec_intop_init(Kfr);
  chebmodel_vec_intop_get_mpfr_chebpoly_vec_intop(Kfr, K);
  long d = mpfr_chebpoly_vec_intop_Dwidth(Kfr) / n;

  mpfr_chebpoly_vec_t Gfr;
  mpfr_chebpoly_vec_init(Gfr);
  chebmodel_vec_get_mpfr_chebpoly_vec(Gfr, G);

  // approximate solution
  mpfr_chebpoly_vec_t Sol;
  mpfr_chebpoly_vec_init(Sol);
  mpfr_chebpoly_vec_intop_approxsolve(Sol, Kfr, Gfr, N);

  // validating the solution to get a chebmodel
  mpfr_ptr bound = malloc(n * sizeof(mpfr_t));
  for (i = 0 ; i < n ; i++)
    mpfr_init(bound + i);
  long init_N = N < 2 * d ? 2 * d : N;
  // long init_N = 2*d;
  chebmodel_vec_intop_newton_validate_sol_fr(bound, K, G, Sol, init_N);
  chebmodel_vec_set_mpfr_chebpoly_vec(P, Sol);
  for (i = 0 ; i < n ; i++)
    mpfr_set(P->poly[i].rem, bound + i, MPFR_RNDU);

  // clear variables
  mpfr_chebpoly_vec_intop_clear(Kfr);
  mpfr_chebpoly_vec_clear(Gfr);
  mpfr_chebpoly_vec_clear(Sol);
  for (i = 0 ; i < n ; i++)
    mpfr_clear(bound + i);
  free(bound);

}




void chebmodel_vec_lode_intop_newton_solve_fr(chebmodel_vec_ptr Sols, const chebmodel_vec_lode_t L, const chebmodel_vec_t g, mpfi_ptr_ptr I, long N)
{
  long n = L->dim;
  long r = L->order;
  long i, j;

  // creating integral operator intop
  chebmodel_vec_intop_t K;
  chebmodel_vec_intop_init(K);
  chebmodel_vec_intop_set_lode(K, L);

  // constructing rhs
  chebmodel_vec_t G;
  chebmodel_vec_init(G);
  chebmodel_vec_intop_rhs(G, L, g, I);

  // solving for the r-th derivative using intop
  chebmodel_vec_intop_newton_solve_fr(Sols + r, K, G, N);

  // computing the other derivatives
  for (j = 0 ; j < n ; j++) {
    chebmodel_zero(G->poly + j);
    mpfi_chebpoly_set_degree(G->poly[j].poly, 0);
  }
  for (i = r - 1 ; i >= 0 ; i--) {
    chebmodel_vec_antiderivative_1(Sols + i, Sols + i + 1);
    for (j = 0 ; j < n ; j++)
      mpfi_set(G->poly[j].poly->coeffs + 0, I[j] + i);
    chebmodel_vec_add(Sols + i, Sols + i, G);
  }

  // clear variables
  chebmodel_vec_intop_clear(K);
  chebmodel_vec_clear(G);
}

