
#include "chebmodel_vec_intop_newton.h"


void chebmodel_vec_intop_newton_validate_sol_aux_fr(mpfr_ptr bound, const chebmodel_vec_intop_t K, mpfr_ptr_ptr T_norm, mpfr_bandmatrix_struct ** M_K_inv, const chebmodel_vec_t G, const mpfr_chebpoly_vec_t P)
{
  long n = K->dim;
  long i, j, k, l;
  long N = M_K_inv[0][0].dim - 1;

  // norms of M_K_inv[i][j]
  mpfr_ptr_ptr M_K_inv_norm = malloc(n * sizeof(mpfr_ptr));
  for (i = 0 ; i < n ; i++) {
    M_K_inv_norm[i] = malloc(n * sizeof(mpfr_t));
    for (j = 0 ; j < n ; j++) {
      mpfr_init(M_K_inv_norm[i] + j);
      mpfr_bandmatrix_1norm_ubound(M_K_inv_norm[i] + j, M_K_inv[i] + j);
    }
  }

  // compute P-T(P)
  chebmodel_t Q;
  chebmodel_init(Q);
  chebmodel_vec_t P_fi, K_P, T_P;
  chebmodel_vec_init(P_fi); chebmodel_vec_init(K_P); chebmodel_vec_init(T_P);
  chebmodel_vec_set_mpfr_chebpoly_vec(P_fi, P);
  chebmodel_vec_intop_evaluate_chebmodel(K_P, K, P_fi);
  chebmodel_vec_add(K_P, K_P, G);
  chebmodel_vec_sub(K_P, P_fi, K_P);
  chebmodel_vec_set_dim(T_P, n);
  for (i = 0 ; i < n ; i++) {
    for (j = 0 ; j < n ; j++) {
      chebmodel_set(Q, K_P->poly + j);
      if (Q->poly->degree < N)
	mpfi_chebpoly_set_degree(Q->poly, N);
      _mpfr_bandmatrix_evaluate_fi(Q->poly->coeffs, M_K_inv[i] + j, Q->poly->coeffs);
      mpfr_mul(Q->rem, Q->rem, M_K_inv_norm[i] + j, MPFR_RNDU);
      chebmodel_add(T_P->poly + i, T_P->poly + i, Q);
    }
    chebmodel_1norm_ubound(bound + i, T_P->poly + i);
  }

  // deduce upper bound by system solving
  mpfi_ptr_ptr Mat = malloc(n * sizeof(mpfi_ptr));
  mpfi_ptr Y = malloc(n * sizeof(mpfi_t));
  // construct matrix Id-T_norm
  for (i = 0 ; i < n ; i++) {
    Mat[i] = malloc(n * sizeof(mpfi_t));
    for (j = 0 ; j < n ; j++) {
      mpfi_init(Mat[i] + j);
      mpfi_set_fr(Mat[i] + j, T_norm[i] + j);
      if (i == j)
	mpfi_si_sub(Mat[i] + j, 1, Mat[i] + j);
      else
	mpfi_neg(Mat[i] + j, Mat[i] + j);
    }
    mpfi_init(Y + i);
    mpfi_set_fr(Y + i, bound + i);
  }
  mpfr_t mig, mig_temp;
  mpfr_inits(mig, mig_temp, NULL);
  mpfi_t lambda, temp_fi;
  mpfi_init(lambda); mpfi_init(temp_fi);
  // upper triangular form (Gauss pivot)
  for (i = 0 ; i < n ; i++) {
    mpfi_mig(mig, Mat[i] + i);
    j = i;
    for (k = i+1 ; k < n ; k++) {
      mpfi_mig(mig_temp, Mat[k] + i);
      if (mpfr_cmp(mig_temp, mig) > 0) {
	mpfr_set(mig, mig_temp, MPFR_RNDU);
	j = k;
      }
    }
    if (j > i) {
      for (k = i ; k < n ; k++)
	mpfi_swap(Mat[i] + k, Mat[j] + k);
      mpfi_swap(Y + i, Y + j);
    }
    for (j = i+1 ; j < n ; j++) {
      mpfi_div(lambda, Mat[j] + i, Mat[i] + i);
      mpfi_set_si(Mat[j] + i, 0);
      for (k = i+1 ; k < n ; k++) {
	mpfi_mul(temp_fi, lambda, Mat[i] + k);
	mpfi_sub(Mat[j] + k, Mat[j] + k, temp_fi);
      }
      mpfi_mul(temp_fi, lambda, Y + i);
      mpfi_sub(Y + j, Y + j, temp_fi);
    }
  }
  // back substitution
  for (i = n-1 ; i >= 0 ; i--) {
    for (j = i+1 ; j < n ; j++) {
      mpfi_mul(temp_fi, Mat[i] + j, Y + j);
      mpfi_sub(Y + i, Y + i, temp_fi);
    }
    mpfi_div(Y + i, Y + i, Mat[i] + i);
    mpfi_mag(bound + i, Y + i);
  }


  // clear variables
  for (i = 0 ; i < n ; i++) {
    for (j = 0 ; j < n ; j++) {
      mpfi_clear(Mat[i] + j);
      mpfr_clear(M_K_inv_norm[i] + j);
    }
    free(Mat[i]); free(M_K_inv_norm[i]);
    mpfi_clear(Y + i);
  }
  free(Mat); free(Y); free(M_K_inv_norm);
  chebmodel_clear(Q);
  chebmodel_vec_clear(P_fi);
  chebmodel_vec_clear(K_P);
  chebmodel_vec_clear(T_P);
  mpfr_clears(mig, mig_temp, NULL);
  mpfi_clear(lambda); mpfi_clear(temp_fi);
}


