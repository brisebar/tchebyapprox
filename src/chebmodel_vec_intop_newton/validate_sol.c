
#include "chebmodel_vec_intop_newton.h"



void chebmodel_vec_intop_newton_validate_sol_fr(mpfr_ptr bound, const chebmodel_vec_intop_t K, const chebmodel_vec_t G, const mpfr_chebpoly_vec_t P, long init_N)
{

  long n = K->dim;
  long i, j, k, l;

  mpfr_ptr_ptr T_norm = malloc(n * sizeof(mpfr_ptr));
  for (i = 0 ; i < n ; i++) {
    T_norm[i] = malloc(n * sizeof(mpfr_t));
    for (j = 0 ; j < n ; j++)
      mpfr_init(T_norm[i] + j);
  }
  
  mpfr_bandmatrix_struct ** M_K_inv = malloc(n * sizeof(mpfr_bandmatrix_struct*));
  for (i = 0 ; i < n ; i++) {
    M_K_inv[i] = malloc(n * sizeof(mpfr_bandmatrix_t));
    for (j = 0 ; j < n ; j++)
      mpfr_bandmatrix_init(M_K_inv[i] + j);
  }

  // get a contracting operator
  chebmodel_vec_intop_newton_contract_fr(T_norm, M_K_inv, K, init_N);

  // obtaining upper bounds
  chebmodel_vec_intop_newton_validate_sol_aux_fr(bound, K, T_norm, M_K_inv, G, P);
  
  // clear variables
  for (i = 0 ; i < n ; i++) {
    for (j = 0 ; j < n ; j++) {
      mpfr_clear(T_norm[i] + j);
      mpfr_bandmatrix_clear(M_K_inv[i] + j);
    }
    free(T_norm[i]); free(M_K_inv[i]);
  }
  free(T_norm); free(M_K_inv);
}



void chebmodel_vec_lode_intop_newton_validate_sol_fr(mpfr_ptr bound, const chebmodel_vec_lode_t L, const chebmodel_vec_t g, mpfi_ptr_ptr I, const mpfr_chebpoly_vec_t P)
{
  chebmodel_vec_intop_t K;
  chebmodel_vec_intop_init(K);
  chebmodel_vec_intop_set_lode(K, L);

  chebmodel_vec_t G;
  chebmodel_vec_init(G);
  chebmodel_vec_intop_rhs(G, L, g, I);

  long init_N = 2 * chebmodel_vec_intop_Dwidth(K);
  chebmodel_vec_intop_newton_validate_sol_fr(bound, K, G, P, init_N);

  chebmodel_vec_intop_clear(K);
  chebmodel_vec_clear(G);
}

