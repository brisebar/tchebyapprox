
#include "chebmodel_vec_intop_newton.h"



/* bound for the Newton Operator */
// 1 = N too small  2 = imprecise numerical inverse  3 = the cumulated error of the coefficients of K is too big


int chebmodel_vec_intop_newton_bound_fr(mpfr_ptr_ptr bound, const chebmodel_vec_intop_t K, const mpfi_chebpoly_vec_intop_t Ktrunc, mpfi_bandmatrix_struct ** M_Ktrunc, const mpfi_bandmatrix_t M_Ktrunc_glob, mpfr_bandmatrix_struct ** M_Ktrunc_inv, const mpfr_bandmatrix_t M_Ktrunc_inv_glob)
{
  long n = K->dim;
  int return_flag = 0;
  long i, j, k;

  mpfr_t temp;
  mpfr_init(temp);

  // compute ||A||*||K-Ktrunc|| in bound6
  mpfr_ptr_ptr bound6 = malloc(n * sizeof(mpfr_ptr));
  mpfr_ptr_ptr Anorm = malloc(n * sizeof(mpfr_ptr));
  mpfr_ptr_ptr Krem = malloc(n * sizeof(mpfr_ptr));
  for (i = 0 ; i < n ; i++) {
    bound6[i] = malloc(n * sizeof(mpfr_t));
    Anorm[i] = malloc(n * sizeof(mpfr_t));
    Krem[i] = malloc(n * sizeof(mpfr_t));
    for (j = 0 ; j < n ; j++) {
      mpfr_inits(bound6[i] + j, Anorm[i] + j, Krem[i] + j, NULL);
      mpfr_set_zero(bound6[i] + j, 1);
      mpfr_bandmatrix_1norm_ubound(Anorm[i] + j, M_Ktrunc_inv[i] + j);
      mpfr_set_zero(Krem[i] + j, 1);
      for (k = 0 ; k < K->intop[i][j].order ; k++) 
        mpfr_add(Krem[i] + j, Krem[i] + j, K->intop[i][j].alpha[k].rem, MPFR_RNDU);
      mpfr_mul_2si(Krem[i] + j, Krem[i] + j, 1, MPFR_RNDU);
    }
  }
  for (i = 0 ; i < n ; i++)
    for (j = 0 ; j < n ; j++) {
      for (k = 0 ; k < n ; k++) {
        mpfr_mul(temp, Anorm[i] + k, Krem[k] + j, MPFR_RNDU);
	mpfr_add(bound6[i] + j, bound6[i] + j, temp, MPFR_RNDU);
      }
      if (mpfr_cmp_d(bound6[i] + j, 0.2/n) > 0)
	return_flag = 3;
    }

  if (return_flag != 3) {
       fprintf(stderr, "[[%ld -- %ld]]\n", M_Ktrunc[0][0].dim, M_Ktrunc_glob->dim); 
    return_flag = mpfi_chebpoly_vec_intop_newton_bound_fr(bound, Ktrunc, M_Ktrunc, M_Ktrunc_glob, M_Ktrunc_inv, M_Ktrunc_inv_glob);

    for (i = 0 ; i < n ; i++)
      for (j = 0 ; j < n ; j++)
	mpfr_add(bound[i] + j, bound[i] + j, bound6[i] + j, MPFR_RNDU);

  }

  // clear variables
  mpfr_clear(temp);
  for (i = 0 ; i < n ; i++) {
    for (j = 0 ; j < n ; j++)
      mpfr_clears(bound6[i] + j, Anorm[i] + j, Krem[i] + j, NULL);
    free(bound6[i]); free(Anorm[i]); free(Krem[i]);
  }
  free(bound6); free(Anorm); free(Krem);


  return return_flag;
}



