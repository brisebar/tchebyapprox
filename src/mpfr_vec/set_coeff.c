
#include "mpfr_vec.h"




// set the n-th coefficient of V to c
void mpfr_vec_set_coeff_si(mpfr_vec_t V, long n, long c)
{
  if (n < 0 || n >= V->length)
    return;

  else
    mpfr_set_si(V->coeffs + n, c, MPFR_RNDN);
}


// set the n-th coefficient of V to c
void mpfr_vec_set_coeff_z(mpfr_vec_t V, long n, const mpz_t c)
{
  if (n < 0 || n >= V->length)
    return;

  else
    mpfr_set_z(V->coeffs + n, c, MPFR_RNDN);
}


// set the n-th coefficient of V to c
void mpfr_vec_set_coeff_q(mpfr_vec_t V, long n, const mpq_t c)
{
  if (n < 0 || n >= V->length)
    return;

  else
    mpfr_set_q(V->coeffs + n, c, MPFR_RNDN);
} 


// set the n-th coefficient of V to c
void mpfr_vec_set_coeff_fr(mpfr_vec_t V, long n, const mpfr_t c)
{
  if (n < 0 || n >= V->length)
    return;

  else
    mpfr_set(V->coeffs + n, c, MPFR_RNDN);
}

