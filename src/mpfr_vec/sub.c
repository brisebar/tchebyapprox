
#include "mpfr_vec.h"


void _mpfr_vec_sub(mpfr_ptr V, mpfr_srcptr W, mpfr_srcptr Z, long n)
{
  long i;
  for (i = 0 ; i < n ; i++)
    mpfr_sub(V + i, W + i, Z + i, MPFR_RNDN);
}


// set V := W + R
void mpfr_vec_sub(mpfr_vec_t V, const mpfr_vec_t W, const mpfr_vec_t Z)
{
  long n = W->length;
  if (n != Z->length) {
    fprintf(stderr, "mpfr_vec_sub: error: incompatible lengths\n");
    return;
  }
  mpfr_vec_set_length(V, n);
  _mpfr_vec_sub(V->coeffs, W->coeffs, Z->coeffs, n);
}

