
#include "mpfr_vec.h"


void _mpfr_vec_zero(mpfr_ptr V, long n)
{
  long i;
  for (i = 0 ; i < n ; i++)
    mpfr_set_zero(V + i, 1);
}


void mpfr_vec_zero(mpfr_vec_t V)
{
_mpfr_vec_zero(V->coeffs, V->length);
}
