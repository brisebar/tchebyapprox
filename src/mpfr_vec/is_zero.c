
#include "mpfr_vec.h"


int _mpfr_vec_is_zero(mpfr_srcptr V, long n)
{
  long i;
  for (i = 0 ; i < n ; i++)
    if (mpfr_cmp_si(V + i, 0) != 0)
      break;

  return i == n;
}


int mpfr_vec_is_zero(const mpfr_vec_t V)
{
  return _mpfr_vec_is_zero(V->coeffs, V->length);
}

