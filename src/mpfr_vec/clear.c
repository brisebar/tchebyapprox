#include "mpfr_vec.h"


// clear a vector of n mpfr_t
void _mpfr_vec_clear(mpfr_ptr V, long n)
{
  long i;
  for (i = 0 ; i < n ; i++)
    mpfr_clear(V + i);
  free(V);
}


// clear V
void mpfr_vec_clear(mpfr_vec_t V)
{
  long n = V->length;
  long i;
  for (i = 0 ; i < n ; i++)
    mpfr_clear(V->coeffs + i);
  free(V->coeffs);
}
