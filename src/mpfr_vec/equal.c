
#include "mpfr_vec.h"


int _mpfr_vec_equal(mpfr_srcptr V, mpfr_srcptr W, long n)
{
  long i;
  for (i = 0 ; i < n ; i++)
    if (!mpfr_equal_p(V + i, W + i))
      break;
  return i == n;
}


int mpfr_vec_equal(const mpfr_vec_t V, const mpfr_vec_t W)
{
  return (V->length == W->length && _mpfr_vec_equal(V->coeffs, W->coeffs, V->length));
}
