
#include "mpfr_vec.h"


void _mpfr_vec_set(mpfr_ptr V, mpfr_srcptr W, long n)
{
  long i ;
  for (i = 0 ; i < n ; i++)
    mpfr_set(V + i, W + i, MPFR_RNDN);
}


// copy W into V
void mpfr_vec_set(mpfr_vec_t V, const mpfr_vec_t W)
{
  long n = W->length;
  long i;
  if (V != W) {
    mpfr_vec_set_length(V, n);
    for (i = 0 ; i < n ; i++)
      mpfr_set(V->coeffs + i, W->coeffs + i, MPFR_RNDN);
  }
}
