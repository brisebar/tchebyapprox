
#include "mpfr_vec.h"


void _mpfr_vec_scalar_prod_fr(mpfr_t prod, mpfr_srcptr V, mpfr_srcptr W, long n)
{
  mpfr_set_zero(prod, 1);
  mpfr_t temp;
  mpfr_init(temp);
  long i;
  for (i = 0 ; i < n ; i++) {
    mpfr_mul(temp, V + i, W + i, MPFR_RNDN);
    mpfr_add(prod, prod, temp, MPFR_RNDN);
  }
  mpfr_clear(temp);
}


void mpfr_vec_scalar_prod_fr(mpfr_t prod, const mpfr_vec_t V, const mpfr_vec_t W)
{
  if (V->length != W->length)
    fprintf(stderr, "mpfr_vec_scalar_prod_fr: error: incompatible lengths\n");
  else
    _mpfr_vec_scalar_prod_fr(prod, V->coeffs, W->coeffs, V->length);
}

void _mpfr_vec_scalar_prod_fi(mpfi_t prod, mpfr_srcptr V, mpfr_srcptr W, long n)
{
  mpfi_set_si(prod, 0);
  mpfi_t temp;
  mpfi_init(temp);
  long i;
  for (i = 0 ; i < n ; i++) {
    mpfi_set_fr(temp, V + i);
    mpfi_mul_fr(temp, temp, W + i);
    mpfi_add(prod, prod, temp);
  }
  mpfi_clear(temp);
} 


void mpfr_vec_scalar_prod_fi(mpfi_t prod, const mpfr_vec_t V, const mpfr_vec_t W)
{
  if (V->length != W->length)
    fprintf(stderr, "mpfr_vec_scalar_prod_fi: error: incompatible lengths\n");
  else
    _mpfr_vec_scalar_prod_fi(prod, V->coeffs, W->coeffs, V->length);
}
