
#include "mpfr_vec.h"


void _mpfr_vec_scalar_div_si(mpfr_ptr V, mpfr_srcptr W, long n, long c)
{
  long i;
  for (i = 0 ; i < n ; i++)
    mpfr_div_si(V + i, W + i, c, MPFR_RNDN);
}


void _mpfr_vec_scalar_div_z(mpfr_ptr V, mpfr_srcptr W, long n, const mpz_t c)
{
  long i;
  for (i = 0 ; i < n ; i++)
    mpfr_div_z(V + i, W + i, c, MPFR_RNDN);
}


void _mpfr_vec_scalar_div_q(mpfr_ptr V, mpfr_srcptr W, long n, const mpq_t c)
{
  long i;
  for (i = 0 ; i < n ; i++)
    mpfr_div_q(V + i, W + i, c, MPFR_RNDN);
}


void _mpfr_vec_scalar_div_fr(mpfr_ptr V, mpfr_srcptr W, long n, const mpfr_t c)
{
  long i;
  for (i = 0 ; i < n ; i++)
    mpfr_div(V + i, W + i, c, MPFR_RNDN);
}


void _mpfr_vec_scalar_div_2si(mpfr_ptr V, mpfr_srcptr W, long n, long k)
{
  long i;
  for (i = 0 ; i < n ; i++)
    mpfr_mul_2si(V + i, W + i, -k, MPFR_RNDN);
}



// set V := c*W
void mpfr_vec_scalar_div_si(mpfr_vec_t V, const mpfr_vec_t W, long c)
{
  long n = W->length;
  mpfr_vec_set_length(V, n);
  _mpfr_vec_scalar_div_si(V->coeffs, W->coeffs, n, c);
}


// set V := c*W
void mpfr_vec_scalar_div_z(mpfr_vec_t V, const mpfr_vec_t W, const mpz_t c)
{
  long n = W->length;
  mpfr_vec_set_length(V, n);
  _mpfr_vec_scalar_div_z(V->coeffs, W->coeffs, n, c);
}


// set V := c*W
void mpfr_vec_scalar_div_q(mpfr_vec_t V, const mpfr_vec_t W, const mpq_t c)
{
  long n = W->length;
  mpfr_vec_set_length(V, n);
  _mpfr_vec_scalar_div_q(V->coeffs, W->coeffs, n, c);
}


// set V := c*W
void mpfr_vec_scalar_div_fr(mpfr_vec_t V, const mpfr_vec_t W, const mpfr_t c)
{
  long n = W->length;
  mpfr_vec_set_length(V, n);
  _mpfr_vec_scalar_div_fr(V->coeffs, W->coeffs, n, c);
}


// set V := 2^k*W
void mpfr_vec_scalar_div_2si(mpfr_vec_t V, const mpfr_vec_t W, long k)
{
  long n = W->length;
  mpfr_vec_set_length(V, n);
  _mpfr_vec_scalar_div_2si(V->coeffs, W->coeffs, n, k);
}
