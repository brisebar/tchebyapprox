
#include "mpfr_vec.h"


void _mpfr_vec_print(mpfr_srcptr V, long n, size_t digits)
{
  printf("[");
  long i;
  for (i = 0 ; i < n ; i++) {
    mpfr_out_str(stdout, 10, digits, V + i, MPFR_RNDN);
    if (i < n-1)
      printf(", ");
  }
  printf("]");
}


// display the vector V
void mpfr_vec_print(const mpfr_vec_t V, size_t digits)
{
  _mpfr_vec_print(V->coeffs, V->length, digits);
}

