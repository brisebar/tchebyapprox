
#include "mpfr_vec.h"


void _mpfr_vec_neg(mpfr_ptr V, mpfr_srcptr W, long n)
{
  long i;
  for (i = 0 ; i < n ; i++)
    mpfr_neg(V + i, W + i, MPFR_RNDN);
}


// set V := -W
void mpfr_vec_neg(mpfr_vec_t V, const mpfr_vec_t W)
{
  mpfr_vec_set_length(V, W->length);
  _mpfr_vec_neg(V->coeffs, W->coeffs, W->length);
}
