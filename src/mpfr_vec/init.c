
#include "mpfr_vec.h"


// init a vector of n mpfr_t
void _mpfr_vec_init(mpfr_ptr V, long n)
{
  long i;
  for (i = 0 ; i < n ; i++)
    mpfr_init(V + i);
}


// init with V = 0
void mpfr_vec_init(mpfr_vec_t V)
{
  V->length = 0;
  V->coeffs = malloc(0);
}


// init V as a zero-vector of size n
void mpfr_vec_init_zero(mpfr_vec_t V, long n)
{
  V->length = n;
  V->coeffs = malloc(n * sizeof(mpfr_t));
  long i;
  for (i = 0 ; i < n ; i++) {
    mpfr_init(V->coeffs + i);
    mpfr_set_zero(V->coeffs + i, 1);
  }
}
