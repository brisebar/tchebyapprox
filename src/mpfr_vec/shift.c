
#include "mpfr_vec.h"


void _mpfr_vec_shift_left(mpfr_ptr V, mpfr_srcptr W, long n, unsigned long k)
{
  long i;
  for (i = n-1 ; i >= 0 ; i--)
    mpfr_set(V + i + k, W + i, MPFR_RNDN);
  for (i = 0 ; i < k ; i++)
    mpfr_set_zero(V + i, 1);
}


void mpfr_vec_shift_left(mpfr_vec_t V, const mpfr_vec_t W, unsigned long k)
{
  long i;
  long n = W->length;
  mpfr_vec_set_length(V, n+k);
  _mpfr_vec_shift_left(V->coeffs, W->coeffs, n, k);
}


void _mpfr_vec_shift_right(mpfr_ptr V, mpfr_srcptr W, long n, unsigned long k)
{
  long i;
  for (i = 0 ; i+k < n ; i++)
    mpfr_set(V + i, W + i + k, MPFR_RNDN);
}


void mpfr_vec_shift_right(mpfr_vec_t V, const mpfr_vec_t W, unsigned long k)
{
  long i;
  long n = W->length;
  if (k >= n)
    mpfr_vec_set_length(V, 0);
  else {
    mpfr_ptr Vcoeffs = malloc((n-k) * sizeof(mpfr_t));
    for (i = 0 ; i < n-k ; i++)
      mpfr_init(Vcoeffs + i);
    _mpfr_vec_shift_right(Vcoeffs, W->coeffs, n, k);
    mpfr_vec_clear(V);
    V->length = n-k;
    V->coeffs = Vcoeffs;
  }
}
