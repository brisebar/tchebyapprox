
#include "mpfr_vec.h"


// reverse a list of mpfr of size (n+1)
void _mpfr_vec_reverse(mpfr_ptr V, mpfr_srcptr W, long n)
{
  long i;
  mpfr_ptr R = malloc(n * sizeof(mpfr_t));
  for (i = 0 ; i < n ; i++) {
    mpfr_init(R + i);
    mpfr_set(R + i, W + n-1-i, MPFR_RNDN);
  }
  for (i = 0 ; i < n ; i++) {
    mpfr_swap(V + i, R + i);
    mpfr_clear(R + i);
  }
}


void mpfr_vec_reverse(mpfr_vec_t V, const mpfr_vec_t W)
{
  if (V != W)
    mpfr_vec_set(V, W);
  _mpfr_vec_reverse(V->coeffs, V->coeffs, V->length);
}
