
#include "mpfr_vec.h"


void _mpfr_vec_1norm_ubound(mpfr_t norm, mpfr_srcptr V, long n)
{
  long i;
  mpfr_set_zero(norm, 1);
  for (i = 0 ; i < n ; i++)
    if (mpfr_sgn(V + i) >= 0)
      mpfr_add(norm, norm, V + i, MPFR_RNDU);
    else
      mpfr_sub(norm, norm, V + i, MPFR_RNDU);
}


void mpfr_vec_1norm_ubound(mpfr_t norm, const mpfr_vec_t V)
{
  _mpfr_vec_1norm_ubound(norm, V->coeffs, V->length);
}


void _mpfr_vec_1norm_lbound(mpfr_t norm, mpfr_srcptr V, long n)
{
  long i;
  mpfr_set_zero(norm, 1);
  for (i = 0 ; i < n ; i++)
    if (mpfr_sgn(V + i) >= 0)
      mpfr_add(norm, norm, V + i, MPFR_RNDD);
    else
      mpfr_sub(norm, norm, V + i, MPFR_RNDD);
}


void mpfr_vec_1norm_lbound(mpfr_t norm, const mpfr_vec_t V)
{
  _mpfr_vec_1norm_lbound(norm, V->coeffs, V->length);
}


void _mpfr_vec_1norm_fi(mpfi_t norm, mpfr_srcptr V, long n)
{
  long i;
  mpfi_set_si(norm, 0);
  for (i = 0 ; i < n ; i++)
    if (mpfr_sgn(V + i) >= 0)
      mpfi_add_fr(norm, norm, V + i);
    else
      mpfi_sub_fr(norm, norm, V + i);
}


void mpfr_vec_1norm_fi(mpfi_t norm, const mpfr_vec_t V)
{
  _mpfr_vec_1norm_fi(norm, V->coeffs, V->length);
}
