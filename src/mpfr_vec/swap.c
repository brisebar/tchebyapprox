
#include "mpfr_vec.h"


// swap V and W efficiently
void mpfr_vec_swap(mpfr_vec_t V, mpfr_vec_t W)
{
  long length_buf = V->length;
  mpfr_ptr coeffs_buf = V->coeffs;
  V->length = W->length;
  V->coeffs = W->coeffs;
  W->length = length_buf;
  W->coeffs = coeffs_buf;
}
