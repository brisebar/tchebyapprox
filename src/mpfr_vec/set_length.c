
#include "mpfr_vec.h"


// set n as length for V
void mpfr_vec_set_length(mpfr_vec_t V, long n)
{
  long m = V->length;
  long i;

  if (n < m) {
    for (i = n ; i < m ; i++)
      mpfr_clear(V->coeffs + i);
    V->coeffs = realloc(V->coeffs, n * sizeof(mpfr_t));
  }
  else if (n > m) {
    V->coeffs = realloc(V->coeffs, n * sizeof(mpfr_t));
    for (i = m ; i < n ; i++) {
      mpfr_init(V->coeffs + i);
      mpfr_set_si(V->coeffs + i, 0, MPFR_RNDN);
    }
  }

  V->length = n;
}
