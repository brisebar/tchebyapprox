
#include "mpfr_vec.h"


// get the degree of V
long mpfr_vec_degree(const mpfr_vec_t V)
{
  return V->length;
}
