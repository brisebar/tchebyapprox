
#include "mpfi_chebpoly_lode.h"


void mpfi_chebpoly_lode_clear(mpfi_chebpoly_lode_t L)
{
  long r = L->order;
  long i;
  for (i = 0 ; i < r ; i++)
    mpfi_chebpoly_clear(L->a + i);
  free(L->a);
}
