
#include "mpfi_chebpoly_lode.h"


/* transpose from x-D form to D-x form and conversely */
// x-D form : a_{r-1} * D^(r-1)y + ... + a_1 * Dy + a_0 * y
// D-x form : (-D)^(r-1)(b_{r-1} * y) + ... + (-D)(b_1 * y) + b_0 * y
// note: involutive function, can be used in both directions
void mpfi_chebpoly_lode_transpose(mpfi_chebpoly_ptr b, mpfi_chebpoly_srcptr a, long r)
{
  long i, j;
  mpfi_chebpoly_t d_bj;
  mpfi_chebpoly_init(d_bj);

  // copy (and alternate signs) from a to b
  for (i = 0 ; i < r ; i++)
    if ((r - i) % 2)
      mpfi_chebpoly_neg(b + i, a + i);
    else
      mpfi_chebpoly_set(b + i, a + i);

  // transpose
  for (i = r-2 ; i >= 0 ; i--)
    for (j = i+1 ; j < r ; j++) {
      mpfi_chebpoly_derivative(d_bj, b + j);
      mpfi_chebpoly_add(b + j-1, b + j-1, d_bj);
    }

  // clear variables
  mpfi_chebpoly_clear(d_bj);
}



/* transpose initial conditions */
// x-D initial conditions: y^(i) = v[i]
// D-x initial conditions: y^[i] = w[i]
// where y^[0] = y and y^[i+1] = -Dy^[i] + a_{r-1-i} y

// v -> w
// take as input the coefficients b[i] of the D-x form differential operator
void mpfi_chebpoly_lode_transpose_init_xDtoDx(mpfi_ptr w, mpfi_srcptr v, mpfi_chebpoly_srcptr b, long r)
{
  long i, j;

  // germ of Y at -1
  mpfi_chebpoly_t Y, cst;
  mpfi_chebpoly_init(Y);
  mpfi_chebpoly_init(cst);
  mpfi_chebpoly_set_degree(cst, 0);
  for (i = r-1 ; i >= 0 ; i--) {
    mpfi_chebpoly_antiderivative_1(Y, Y);
    mpfi_chebpoly_set_coeff_fi(cst, 0, v + i);
    mpfi_chebpoly_add(Y, Y, cst);
  }

  // Yi = Y^[i]
  mpfi_chebpoly_t Yi, Temp;
  mpfi_chebpoly_init(Yi);
  mpfi_chebpoly_init(Temp);
  mpfi_chebpoly_set(Yi, Y);
  for (i = 0 ; i < r ; i++) {
    mpfi_chebpoly_evaluate_1(w + i, Yi);
    mpfi_chebpoly_mul(Temp, b + r-1-i, Y);
    mpfi_chebpoly_derivative(Yi, Yi);
    mpfi_chebpoly_sub(Yi, Temp, Yi);
  }

  // clear variables
  mpfi_chebpoly_clear(Y);
  mpfi_chebpoly_clear(cst);
  mpfi_chebpoly_clear(Yi);
  mpfi_chebpoly_clear(Temp);
}


// v -> w
// take as input the coefficients b[i] of the D-x form differential operator
void mpfi_chebpoly_lode_transpose_init_DxtoxD(mpfi_ptr v, mpfi_srcptr w, mpfi_chebpoly_srcptr b, long r)
{
  long i, j;
  mpfi_chebpoly_t Y, Yj, Temp, cst;
  mpfi_chebpoly_init(Y);
  mpfi_chebpoly_init(Yj);
  mpfi_chebpoly_init(Temp);
  mpfi_chebpoly_init(cst);
  mpfi_chebpoly_set_degree(cst, 0);

  // at step i, compute the Yj := y^[j] for j=i..0
  // and set Y=Yj at j=0 ==> degree i germ of Y at -1
  for (i = 0 ; i < r ; i++) {
    // set Yj = y^[i](-1)
    mpfi_chebpoly_set_degree(Yj, 0);
    mpfi_chebpoly_set_coeff_fi(Yj, 0, w + i);
    // compute Yj = y^[j]
    for (j = i-1 ; j >= 0 ; j--) {
      mpfi_chebpoly_mul(Temp, b + r-1-j, Y);
      mpfi_chebpoly_sub(Temp, Temp, Yj);
      mpfi_chebpoly_antiderivative_1(Temp, Temp);
      mpfi_chebpoly_set_coeff_fi(cst, 0, w + j);
      mpfi_chebpoly_add(Yj, cst, Temp);
    }
    // assign Yj to Y
    mpfi_chebpoly_set(Y, Yj);
  }

  // extract initial conditions
  for (i = 0 ; i < r ; i++) {
    mpfi_chebpoly_evaluate_1(v + i, Y);
    mpfi_chebpoly_derivative(Y, Y);
  }

  // clear variables
  mpfi_chebpoly_clear(Y);
  mpfi_chebpoly_clear(Yj);
  mpfi_chebpoly_clear(Temp);
  mpfi_chebpoly_clear(cst);
}
