
#include "mpfi_chebpoly_lode.h"


void mpfi_chebpoly_lode_swap(mpfi_chebpoly_lode_t L, mpfi_chebpoly_lode_t M)
{
  long order_buf = L->order;
  mpfi_chebpoly_ptr a_buf = L->a;

  L->order = M->order;
  L->a = M->a;

  M->order = order_buf;
  M->a = a_buf;
}
