
#include "mpfi_chebpoly_lode.h"


void mpfi_chebpoly_lode_print(const mpfi_chebpoly_lode_t L, const char * var, const char * Cheb_var, size_t digits)
{
  long r = L->order;
  long i;

  if (r > 2)
    printf("d^%ld/d%s", r, var);
  else if (r == 1)
    printf("d/d%s", var);
  else
    printf("1");

  for (i = r-1 ; i >= 2 ; i--) {
    printf(" + ");
    mpfi_chebpoly_print(L->a + i, Cheb_var, digits);
    printf(".d^%ld/d%s", i, var);
  }

  if (r > 1) {
    printf(" + ");
    mpfi_chebpoly_print(L->a + 1, Cheb_var, digits);
    printf(".d/d%s", var);
  }

  if (r > 0) {
    printf(" + ");
    mpfi_chebpoly_print(L->a + 0, Cheb_var, digits);
    printf(".1");
  }
}
