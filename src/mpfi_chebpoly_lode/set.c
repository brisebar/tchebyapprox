
#include "mpfi_chebpoly_lode.h"


void mpfi_chebpoly_lode_set(mpfi_chebpoly_lode_t L, const mpfi_chebpoly_lode_t M)
{
  long r = M->order;
  mpfi_chebpoly_lode_set_order(L, r);

  long i;
  for (i = 0 ; i < r ; i++)
    mpfi_chebpoly_set(L->a + i, M->a + i);
}

