
#include "mpfi_chebpoly_lode.h"


void mpfi_chebpoly_lode_set_mpfr_chebpoly_lode(mpfi_chebpoly_lode_t L, const mpfr_chebpoly_lode_t M)
{
  long i;
  long r = M->order;
  mpfi_chebpoly_lode_set_order(L, r);

  for (i = 0 ; i < r ; i++)
    mpfi_chebpoly_set_mpfr_chebpoly(L->a + i, M->a + i);
}
