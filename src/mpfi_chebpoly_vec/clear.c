
#include "mpfi_chebpoly_vec.h"


void mpfi_chebpoly_vec_clear(mpfi_chebpoly_vec_t P)
{
  long i;
  long n = P->dim;
  for (i = 0 ; i < n ; i++)
    mpfi_chebpoly_clear(P->poly + i);
  free(P->poly);
}
