
#include "mpfi_chebpoly_vec.h"


void mpfi_chebpoly_vec_pow(mpfi_chebpoly_vec_t P, const mpfi_chebpoly_vec_t Q, unsigned long e)
{
  long n = Q->dim;
  long i;

  mpfi_chebpoly_vec_set_dim(P, n);
  for (i = 0 ; i < n ; i++)
    mpfi_chebpoly_pow(P->poly + i, Q->poly + i, e);

}

