
#include "mpfi_chebpoly_vec.h"


void mpfi_chebpoly_vec_swap(mpfi_chebpoly_vec_t P, mpfi_chebpoly_vec_t Q)
{
  long dim_buf = P->dim;
  mpfi_chebpoly_ptr poly_buf = P->poly;
  P->dim = Q->dim;
  P->poly = Q->poly;
  Q->dim = dim_buf;
  Q->poly = poly_buf;
}
