
#include "mpfi_chebpoly_vec.h"


void mpfi_chebpoly_vec_mul(mpfi_chebpoly_vec_t P, const mpfi_chebpoly_vec_t Q, const mpfi_chebpoly_vec_t R)
{
  long n = Q->dim;
  long i;

  if (n != R->dim) {
    fprintf(stderr, "mpfi_chebpoly_vec_mul: error: incompatible dimensions\n");
    return;
  }

  mpfi_chebpoly_vec_set_dim(P, n);
  for (i = 0 ; i < n ; i++)
    mpfi_chebpoly_mul(P->poly + i, Q->poly + i, R->poly + i);

}

