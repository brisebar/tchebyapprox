
#include "mpfi_chebpoly_vec.h"


void mpfi_chebpoly_vec_scalar_div_si(mpfi_chebpoly_vec_t P, const mpfi_chebpoly_vec_t Q, long c)
{
  long n = Q->dim;
  long i;
  mpfi_chebpoly_vec_set_dim(P, n);
  for (i = 0 ; i < n ; i++)
    mpfi_chebpoly_scalar_div_si(P->poly + i, Q->poly + i, c);
}


void mpfi_chebpoly_vec_scalar_div_z(mpfi_chebpoly_vec_t P, const mpfi_chebpoly_vec_t Q, const mpz_t c)
{
  long n = Q->dim;
  long i;
  mpfi_chebpoly_vec_set_dim(P, n);
  for (i = 0 ; i < n ; i++)
    mpfi_chebpoly_scalar_div_z(P->poly + i, Q->poly + i, c);
}


void mpfi_chebpoly_vec_scalar_div_q(mpfi_chebpoly_vec_t P, const mpfi_chebpoly_vec_t Q, const mpq_t c)
{
  long n = Q->dim;
  long i;
  mpfi_chebpoly_vec_set_dim(P, n);
  for (i = 0 ; i < n ; i++)
    mpfi_chebpoly_scalar_div_q(P->poly + i, Q->poly + i, c);
}


void mpfi_chebpoly_vec_scalar_div_fr(mpfi_chebpoly_vec_t P, const mpfi_chebpoly_vec_t Q, const mpfr_t c)
{
  long n = Q->dim;
  long i;
  mpfi_chebpoly_vec_set_dim(P, n);
  for (i = 0 ; i < n ; i++)
    mpfi_chebpoly_scalar_div_fr(P->poly + i, Q->poly + i, c);
}


void mpfi_chebpoly_vec_scalar_div_fi(mpfi_chebpoly_vec_t P, const mpfi_chebpoly_vec_t Q, const mpfi_t c)
{
  long n = Q->dim;
  long i;
  mpfi_chebpoly_vec_set_dim(P, n);
  for (i = 0 ; i < n ; i++)
    mpfi_chebpoly_scalar_div_fi(P->poly + i, Q->poly + i, c);
}


void mpfi_chebpoly_vec_scalar_div_2si(mpfi_chebpoly_vec_t P, const mpfi_chebpoly_vec_t Q, long k)
{
  long n = Q->dim;
  long i;
  mpfi_chebpoly_vec_set_dim(P, n);
  for (i = 0 ; i < n ; i++)
    mpfi_chebpoly_scalar_div_2si(P->poly + i, Q->poly + i, k);
}



