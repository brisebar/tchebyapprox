
#include "mpfi_chebpoly_vec.h"


void mpfi_chebpoly_vec_scalar_prod(mpfi_chebpoly_t P, const mpfi_chebpoly_vec_t Q, const mpfi_chebpoly_vec_t R)
{
  long n = Q->dim;
  long i;

  if (n != R->dim) {
    fprintf(stderr, "mpfi_chebpoly_vec_scalar_prod: error: incompatible dimensions\n");
    return;
  }

  mpfi_chebpoly_zero(P);
  mpfi_chebpoly_t Temp;
  mpfi_chebpoly_init(Temp);
  for (i = 0 ; i < n ; i++) {
    mpfi_chebpoly_mul(Temp, Q->poly + i, R->poly + i);
    mpfi_chebpoly_add(P, P, Temp);
  }

  mpfi_chebpoly_clear(Temp);
}

