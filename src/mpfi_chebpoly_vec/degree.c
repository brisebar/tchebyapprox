
#include "mpfi_chebpoly_vec.h"


long mpfi_chebpoly_vec_degree(const mpfi_chebpoly_vec_t P)
{
  long deg = -1;
  long n = P->dim;
  long i;
  for (i = 0 ; i < n ; i++)
    if (P->poly[i].degree > deg)
      deg = P->poly[i].degree;
  return deg;
}
