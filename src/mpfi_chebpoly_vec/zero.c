
#include "mpfi_chebpoly_vec.h"


void mpfi_chebpoly_vec_zero(mpfi_chebpoly_vec_t P)
{
  long n = P->dim;
  long i;
  for (i = 0 ; i < n ; i++)
    mpfi_chebpoly_zero(P->poly + i);
}
