
#include "mpfi_chebpoly_vec.h"


void mpfi_chebpoly_vec_evaluate_d(mpfi_vec_t Y, const mpfi_chebpoly_vec_t P, const double_t x)
{
  long n = P->dim;
  long i;
  mpfi_vec_set_length(Y, n);
  for (i = 0 ; i < n ; i++)
    mpfi_chebpoly_evaluate_d(Y->coeffs + i, P->poly + i, x);
}


void mpfi_chebpoly_vec_evaluate_fr(mpfi_vec_t Y, const mpfi_chebpoly_vec_t P, const mpfr_t x)
{
  long n = P->dim;
  long i;
  mpfi_vec_set_length(Y, n);
  for (i = 0 ; i < n ; i++)
    mpfi_chebpoly_evaluate_fr(Y->coeffs + i, P->poly + i, x);
}


void mpfi_chebpoly_vec_evaluate_fi(mpfi_vec_t Y, const mpfi_chebpoly_vec_t P, const mpfi_t x)
{
  long n = P->dim;
  long i;
  mpfi_vec_set_length(Y, n);
  for (i = 0 ; i < n ; i++)
    mpfi_chebpoly_evaluate_fi(Y->coeffs + i, P->poly + i, x);
}
