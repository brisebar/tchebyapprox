
#include "mpfi_chebpoly_vec.h"


void mpfi_chebpoly_vec_set_dim(mpfi_chebpoly_vec_t P, long n)
{
  long m = P->dim;
  long i;

  if (n < m) {
    for (i = n ; i < m ; i++)
      mpfi_chebpoly_clear(P->poly + i);
    P->poly = realloc(P->poly, n * sizeof(mpfi_chebpoly_t));
  }
  else if (n > m) {
    P->poly = realloc(P->poly, n * sizeof(mpfi_chebpoly_t));
    for (i = m ; i < n ; i++)
      mpfi_chebpoly_init(P->poly + i);
  }

  P->dim = n;
}
