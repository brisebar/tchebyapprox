
#include "mpfi_chebpoly_vec.h"


void mpfi_chebpoly_vec_antiderivative(mpfi_chebpoly_vec_t P, const mpfi_chebpoly_vec_t Q)
{
  long n = Q->dim;
  long i;
  mpfi_chebpoly_vec_set_dim(P, n);
  for (i = 0 ; i < n ; i++)
    mpfi_chebpoly_antiderivative(P->poly + i, Q->poly + i);
}


void mpfi_chebpoly_vec_antiderivative0(mpfi_chebpoly_vec_t P, const mpfi_chebpoly_vec_t Q)
{
  long n = Q->dim;
  long i;
  mpfi_chebpoly_vec_set_dim(P, n);
  for (i = 0 ; i < n ; i++)
    mpfi_chebpoly_antiderivative0(P->poly + i, Q->poly + i);
}


void mpfi_chebpoly_vec_antiderivative_1(mpfi_chebpoly_vec_t P, const mpfi_chebpoly_vec_t Q)
{
  long n = Q->dim;
  long i;
  mpfi_chebpoly_vec_set_dim(P, n);
  for (i = 0 ; i < n ; i++)
    mpfi_chebpoly_antiderivative_1(P->poly + i, Q->poly + i);
}


void mpfi_chebpoly_vec_antiderivative1(mpfi_chebpoly_vec_t P, const mpfi_chebpoly_vec_t Q)
{
  long n = Q->dim;
  long i;
  mpfi_chebpoly_vec_set_dim(P, n);
  for (i = 0 ; i < n ; i++)
    mpfi_chebpoly_antiderivative1(P->poly + i, Q->poly + i);
}


