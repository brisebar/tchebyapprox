
#include "double_chebpoly_lode.h"


void double_chebpoly_lode_print(const double_chebpoly_lode_t L, const char * var, const char * Cheb_var)
{
  long r = L->order;
  long i;

  if (r > 2)
    printf("d^%ld/d%s", r, var);
  else if (r == 1)
    printf("d/d%s", var);
  else
    printf("1");

  for (i = r-1 ; i >= 2 ; i--) {
    printf(" + ");
    double_chebpoly_print(L->a + i, Cheb_var);
    printf(".d^%ld/d%s", i, var);
  }

  if (r > 1) {
    printf(" + ");
    double_chebpoly_print(L->a + 1, Cheb_var);
    printf(".d/d%s", var);
  }

  if (r > 0) {
    printf(" + ");
    double_chebpoly_print(L->a + 0, Cheb_var);
    printf(".1");
  }
}
