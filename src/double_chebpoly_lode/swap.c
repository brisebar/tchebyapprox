
#include "double_chebpoly_lode.h"


void double_chebpoly_lode_swap(double_chebpoly_lode_t L, double_chebpoly_lode_t M)
{
  long order_buf = L->order;
  double_chebpoly_ptr a_buf = L->a;

  L->order = M->order;
  L->a = M->a;

  M->order = order_buf;
  M->a = a_buf;
}
