
#include "double_chebpoly_lode.h"


void double_chebpoly_lode_clear(double_chebpoly_lode_t L)
{
  long r = L->order;
  long i;
  for (i = 0 ; i < r ; i++)
    double_chebpoly_clear(L->a + i);
  free(L->a);
}
