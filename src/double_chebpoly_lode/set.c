
#include "double_chebpoly_lode.h"


void double_chebpoly_lode_set(double_chebpoly_lode_t L, const double_chebpoly_lode_t M)
{
  long r = M->order;
  double_chebpoly_lode_set_order(L, r);

  long i;
  for (i = 0 ; i < r ; i++)
    double_chebpoly_set(L->a + i, M->a + i);
}

