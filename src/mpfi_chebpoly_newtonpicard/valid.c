
#include "mpfi_chebpoly_newtonpicard.h"

/* validating a candidate solution */
// x-D and D-x forms

// a = LODE coefficients
// r = LODE order
// K = associated integral operator
// R = resolvent kernel approximation
// Tbound = rigorous bound for Newton-Picard operator computed by
//       mpfi_chebpoly_newtonpicard_xD_Tbound
// g = right hand side
// f = approximate solution to be validated
// bound = rigorously computed error bound

void mpfi_chebpoly_newtonpicard_valid_aux(mpfr_t bound, const mpfi_chebpoly_lrintop_t K, const mpfr_t Tbound, const mpfi_chebpoly_lrintop_t R, const mpfi_chebpoly_t g, const mpfr_chebpoly_t f)
{
  mpfi_chebpoly_t delta, temp;
  mpfi_chebpoly_init(delta);
  mpfi_chebpoly_init(temp);
  mpfr_t d;
  mpfr_init(d);

  // compute delta := f-T(f) and defect d := ||delta||
  mpfi_chebpoly_lrintop_evaluate_fr(temp, K, f);
  mpfi_chebpoly_set_mpfr_chebpoly(delta, f);
  mpfi_chebpoly_add(delta, delta, temp);
  mpfi_chebpoly_sub(delta, delta, g); // delta = f + K(f) - g
  mpfi_chebpoly_lrintop_evaluate_fi(temp, R, delta);
  mpfi_chebpoly_add(delta, delta, temp); // delta = (I+R)(f+K(f)-g)
  mpfi_chebpoly_1norm_ubound(d, delta); // d = ||delta||

  // final bound d/(1-Tbound)
  mpfr_si_sub(bound, 1, Tbound, MPFR_RNDD);
  mpfr_div(bound, d, bound, MPFR_RNDU);

  // clear variables
  mpfi_chebpoly_clear(delta);
  mpfi_chebpoly_clear(temp);
  mpfr_clear(d);
}


// a = LODE coefficients
// r = LODE order
// K = associated integral operator
// g = right hand side
// f = approximate solution to be validated

void mpfi_chebpoly_newtonpicard_xD_valid(mpfr_t bound, mpfi_chebpoly_srcptr a, long r, const mpfi_chebpoly_t g, const mpfr_chebpoly_t f)
{
  // integral operators
  mpfi_chebpoly_lrintop_t K, R;
  mpfi_chebpoly_lrintop_init(K);
  mpfi_chebpoly_lrintop_init(R);
  mpfi_chebpoly_lrintop_set_lode_xD(K, a, r);

  // contracting operator
  mpfr_t Tbound;
  mpfr_init(Tbound);
  mpfi_chebpoly_newtonpicard_xD_Tbound(Tbound, R, a, r);

  // compute bound
  mpfi_chebpoly_newtonpicard_valid_aux(bound, K, Tbound, R, g, f);

  // clear variables
  mpfr_clear(Tbound);
  mpfi_chebpoly_lrintop_clear(K);
  mpfi_chebpoly_lrintop_clear(R);
}


void mpfi_chebpoly_newtonpicard_Dx_valid(mpfr_t bound, mpfi_chebpoly_srcptr a, long r, const mpfi_chebpoly_t g, const mpfr_chebpoly_t f)
{
  // integral operators
  mpfi_chebpoly_lrintop_t K, R;
  mpfi_chebpoly_lrintop_init(K);
  mpfi_chebpoly_lrintop_init(R);
  mpfi_chebpoly_lrintop_set_lode_Dx(K, a, r);

  // contracting operator
  mpfr_t Tbound;
  mpfr_init(Tbound);
  mpfi_chebpoly_newtonpicard_Dx_Tbound(Tbound, R, a, r);

  // compute bound
  mpfi_chebpoly_newtonpicard_valid_aux(bound, K, Tbound, R, g, f);

  // clear variables
  mpfr_clear(Tbound);
  mpfi_chebpoly_lrintop_clear(K);
  mpfi_chebpoly_lrintop_clear(R);
}
