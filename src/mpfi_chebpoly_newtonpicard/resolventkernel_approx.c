
#include "mpfi_chebpoly_newtonpicard.h"

/* resolvent kernel approximation */
// r = LODE order
// a = LODE coefficients:
// L = D^r + a_{r-1} D^{r-1} + ... + a_0
// L* = (-D)^r + (-D)^{r-1} a_{r-1} + ... + a_0
// returns the phi's and psi's
// phi[i] = phi^(r) with L(phi) = 0 and phi^(j)(-1) = delta_ij
// psi[i] = psi with L(psi) = 0 and psi^[j](-1) = delta_ij
void mpfr_chebpoly_resolventkernel(mpfr_chebpoly_ptr phi, mpfr_chebpoly_ptr psi, mpfr_chebpoly_srcptr a, long r, long N)
{
    long i, j;

  // construct integral operators K and KK=K*
  mpfr_chebpoly_lrintop_t K, KK;
  mpfr_chebpoly_lrintop_init(K);
  mpfr_chebpoly_lrintop_init(KK);
  mpfr_chebpoly_lrintop_set_lode_xD(K, a, r);
  mpfr_chebpoly_lrintop_set_length(KK, r);
  for (i = 0 ; i < r ; i++) {
    mpfr_chebpoly_neg(KK->lpoly + i, K->rpoly + i);
    mpfr_chebpoly_set(KK->rpoly + i, K->lpoly + i);
  }

  // construct almost-banded matrices and QR factorizations
  mpfr_bandmatrix_t M, MM;
  mpfr_bandmatrix_init(M);
  mpfr_bandmatrix_init(MM);
  mpfr_bandmatrix_QRdecomp_t Q, QQ;
  mpfr_bandmatrix_QRdecomp_init(Q);
  mpfr_bandmatrix_QRdecomp_init(QQ);
  mpfr_chebpoly_lrintop_get_bandmatrix(M, K, N);
  mpfr_chebpoly_lrintop_get_bandmatrix(MM, KK, N);
  for (i = 0 ; i <= N ; i++) {
    mpfr_add_si(M->D[i] + M->Dwidth, M->D[i] + M->Dwidth, 1, MPFR_RNDN);
    mpfr_add_si(MM->D[i] + MM->Dwidth, MM->D[i] + MM->Dwidth, 1, MPFR_RNDN);
  }
  mpfr_bandmatrix_get_QRdecomp(Q, M);
  mpfr_bandmatrix_get_QRdecomp(QQ, MM);

  // right hand sides
  mpfr_chebpoly_t g, gg, X;
  mpfr_chebpoly_init(g);
  mpfr_chebpoly_init(gg);
  mpfr_chebpoly_init(X);
  mpfr_chebpoly_set_degree(X, 1);
  mpfr_chebpoly_set_coeff_si(X, 1, 1);
  mpfr_chebpoly_set_coeff_si(X, 0, 1); // X = x+1
  mpfr_chebpoly_ptr gj = malloc(r * sizeof(mpfr_chebpoly_t));
  for (j = 0 ; j < r ; j++)
    mpfr_chebpoly_init(gj + j);

  // compute the phi_i^(r)
  for (i = 0 ; i < r ; i++) {
    // rhs
    for (j = 0 ; j < i ; j++) {
      mpfr_chebpoly_mul(gj + j, gj + j, X);
      mpfr_chebpoly_scalar_div_si(gj + j, gj + j, i-j);
    }
    mpfr_chebpoly_neg(gj + i, a + i);
    mpfr_chebpoly_zero(g);
    for (j = 0 ; j <= i ; j++)
      mpfr_chebpoly_add(g, g, gj + j);
    // solve
    mpfr_chebpoly_set(phi + i, g);
    mpfr_chebpoly_set_degree(phi + i, N);
    _mpfr_bandmatrix_QRdecomp_solve_fr(phi[i].coeffs, Q, phi[i].coeffs);
  }

  // compute the psi_i
  mpfr_chebpoly_neg(X, X); // X = -(x+1)
  mpfr_chebpoly_set_degree(gg, 0);
  mpfr_chebpoly_set_coeff_si(gg, 0, 1);
  for (i = 0 ; i < r ; i++) {
    // solve
    mpfr_chebpoly_set(psi + i, gg);
    mpfr_chebpoly_set_degree(psi + i, N);
    _mpfr_bandmatrix_QRdecomp_solve_fr(psi[i].coeffs, QQ, psi[i].coeffs);
    // update rhs
    mpfr_chebpoly_mul(gg, gg, X);
    mpfr_chebpoly_scalar_div_si(gg, gg, i+1);
  }

  // clear variables
  for (i = 0 ; i < r ; i++)
    mpfr_chebpoly_clear(gj + i);
  free(gj);
  mpfr_chebpoly_lrintop_clear(K);
  mpfr_chebpoly_lrintop_clear(KK);
  mpfr_chebpoly_clear(g);
  mpfr_chebpoly_clear(gg);
  mpfr_chebpoly_clear(X);
  mpfr_bandmatrix_clear(M);
  mpfr_bandmatrix_clear(MM);
  mpfr_bandmatrix_QRdecomp_clear(Q);
  mpfr_bandmatrix_QRdecomp_clear(QQ);
}
