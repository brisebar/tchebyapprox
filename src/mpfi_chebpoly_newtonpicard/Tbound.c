
#include "mpfi_chebpoly_newtonpicard.h"

/* computing and bounding Newton-Picard fixed-point operator */
// for x-D form and D-x forms
// a = coeffs
// r = LODE order
// return R and bound for ||I-(I+R)(I+K)||

void mpfi_chebpoly_newtonpicard_xD_Tbound(mpfr_t Tbound, mpfi_chebpoly_lrintop_t R, mpfi_chebpoly_srcptr a, long r)
{
  long i;

  long s = 0; // maximum degree
  long si;
  for (i = 0 ; i < r ; i++) {
    si = mpfi_chebpoly_degree(a + i);
    s = si > s ? si : s;
  }

  long N = r + s;

  // integral operators
  mpfi_chebpoly_lrintop_t K, E;
  mpfi_chebpoly_lrintop_init(K);
  mpfi_chebpoly_lrintop_init(E);
  mpfi_chebpoly_lrintop_set_lode_xD(K, a, r);
  mpfi_chebpoly_lrintop_set_length(R, r);

  // afr, phi, psi
  mpfr_chebpoly_ptr afr = malloc(r * sizeof(mpfr_chebpoly_t));
  mpfr_chebpoly_ptr phi = malloc(r * sizeof(mpfr_chebpoly_t));
  mpfr_chebpoly_ptr psi = malloc(r * sizeof(mpfr_chebpoly_t));
  for (i = 0 ; i < r ; i++) {
    mpfr_chebpoly_init(afr + i);
    mpfi_chebpoly_get_mpfr_chebpoly(afr + i, a + i);
    mpfr_chebpoly_init(phi + i);
    mpfr_chebpoly_init(psi + i);
  }

  // compute and bound Newton-Picard operator T, until contraction ratio < 0.1
  mpfr_set_si(Tbound, 2, MPFR_RNDN);
  while (mpfr_cmp_d(Tbound, 0.1) >= 0) {
    N *= 2;
     fprintf(stderr, "(Newton-Picard) try with N = %ld\n", N);
    // build resolvent kernel R
    mpfr_chebpoly_resolventkernel(phi, psi, afr, r, N);
    for (i = 0 ; i < r ; i++) {
      mpfi_chebpoly_set_mpfr_chebpoly(R->lpoly + i, phi + i);
      mpfi_chebpoly_set_mpfr_chebpoly(R->rpoly + i, psi + r-1-i);
    }
    // bound linear part of T
    mpfi_chebpoly_lrintop_comp(E, R, K);
    mpfi_chebpoly_lrintop_add(E, E, R);
    mpfi_chebpoly_lrintop_add(E, E, K);
    mpfi_chebpoly_lrintop_1norm(Tbound, E);
     fprintf(stderr, "(Newton-Picard) Tbound = %f\n", mpfr_get_d(Tbound, MPFR_RNDU));
  }

  // clear variables
  mpfi_chebpoly_lrintop_clear(K);
  mpfi_chebpoly_lrintop_clear(E);
  for (i = 0 ; i < r ; i++) {
    mpfr_chebpoly_clear(afr + i);
    mpfr_chebpoly_clear(phi + i);
    mpfr_chebpoly_clear(psi + i);
  }
  free(afr);
  free(phi);
  free(psi);
}


void mpfi_chebpoly_newtonpicard_Dx_Tbound(mpfr_t Tbound, mpfi_chebpoly_lrintop_t R, mpfi_chebpoly_srcptr a, long r)
{
  long i;

  long s = 0; // maximum degree
  long si;
  for (i = 0 ; i < r ; i++) {
    si = mpfi_chebpoly_degree(a + i);
    s = si > s ? si : s;
  }

  long N = r + s;

  // integral operators
  mpfi_chebpoly_lrintop_t K, E;
  mpfi_chebpoly_lrintop_init(K);
  mpfi_chebpoly_lrintop_init(E);
  mpfi_chebpoly_lrintop_set_lode_Dx(K, a, r);
  mpfi_chebpoly_lrintop_set_length(R, r);

  // afr, phi, psi
  mpfr_chebpoly_ptr afr = malloc(r * sizeof(mpfr_chebpoly_t));
  mpfr_chebpoly_ptr phi = malloc(r * sizeof(mpfr_chebpoly_t));
  mpfr_chebpoly_ptr psi = malloc(r * sizeof(mpfr_chebpoly_t));
  for (i = 0 ; i < r ; i++) {
    mpfr_chebpoly_init(afr + i);
    mpfi_chebpoly_get_mpfr_chebpoly(afr + i, a + i);
    mpfr_chebpoly_init(phi + i);
    mpfr_chebpoly_init(psi + i);
  }

  // compute and bound Newton-Picard operator T, until contraction ratio < 0.1
  mpfr_set_si(Tbound, 2, MPFR_RNDN);
  while (mpfr_cmp_d(Tbound, 0.1) >= 0) {
    N *= 2;
    fprintf(stderr, "(Newton-Picard) try with N = %ld\n", N);
    // build resolvent kernel R
    mpfr_chebpoly_resolventkernel(phi, psi, afr, r, N);
    for (i = 0 ; i < r ; i++) {
      mpfi_chebpoly_set_mpfr_chebpoly(R->lpoly + i, psi + i);
      mpfi_chebpoly_neg(R->lpoly + i, R->lpoly + i);
      mpfi_chebpoly_set_mpfr_chebpoly(R->rpoly + i, phi + r-1-i);
    }
    // bound linear part of T
    mpfi_chebpoly_lrintop_comp(E, R, K);
    mpfi_chebpoly_lrintop_add(E, E, R);
    mpfi_chebpoly_lrintop_add(E, E, K);
    mpfi_chebpoly_lrintop_1norm(Tbound, E);
    fprintf(stderr, "(Newton-Picard) Tbound = %f\n", mpfr_get_d(Tbound, MPFR_RNDU));
  }

  // clear variables
  mpfi_chebpoly_lrintop_clear(K);
  mpfi_chebpoly_lrintop_clear(E);
  for (i = 0 ; i < r ; i++) {
    mpfr_chebpoly_clear(afr + i);
    mpfr_chebpoly_clear(phi + i);
    mpfr_chebpoly_clear(psi + i);
  }
  free(afr);
  free(phi);
  free(psi);
}
