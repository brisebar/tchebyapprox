
#include "mpfi_chebpoly_newtonpicard.h"

/* compute approximation and validate it */

// a = LODE coefficients
// r = LODE order
// h = rhs
// v = initial conditions
// N = approximation degree
// return :
// f = approximate solution
// bound = rigorously computed error bound

// compute polynomial approx f and error bound for y^(r) with y solution of
// y^(r) + a_{r-1} y^(r-1) + ... + a_0 y = h
// y^(i)(-1) = v[i]  0 <= i < r

void mpfi_chebpoly_newtonpicard_xD_approxvalid(mpfr_chebpoly_t f, mpfr_t bound, mpfi_chebpoly_srcptr a, long r, const mpfi_chebpoly_t h, mpfi_srcptr v, long N)
{
  // integral operator
  mpfi_chebpoly_lrintop_t K, R;
  mpfi_chebpoly_lrintop_init(K);
  mpfi_chebpoly_lrintop_init(R);
  mpfi_chebpoly_lrintop_set_lode_xD(K, a, r);

  // right hand side
  mpfi_chebpoly_t g;
  mpfi_chebpoly_init(g);
  mpfi_chebpoly_lrintop_set_lode_xD_rhs(g, a, r, h, v);

  // approximate solution
  mpfr_chebpoly_lrintop_t(Kfr);
  mpfr_chebpoly_lrintop_init(Kfr);
  mpfi_chebpoly_lrintop_get_mpfr_chebpoly_lrintop(Kfr, K);
  mpfr_chebpoly_t gfr;
  mpfr_chebpoly_init(gfr);
  mpfi_chebpoly_get_mpfr_chebpoly(gfr, g);
  mpfr_chebpoly_lrintop_approxsolve(f, Kfr, gfr, N);

  // contracting operator
  mpfr_t Tbound;
  mpfr_init(Tbound);
  mpfi_chebpoly_newtonpicard_xD_Tbound(Tbound, R, a, r);

  // compute bound
  mpfi_chebpoly_newtonpicard_valid_aux(bound, K, Tbound, R, g, f);

  // clear variables
  mpfi_chebpoly_lrintop_clear(K);
  mpfi_chebpoly_lrintop_clear(R);
  mpfr_chebpoly_lrintop_clear(Kfr);
  mpfi_chebpoly_clear(g);
  mpfr_chebpoly_clear(gfr);
  mpfr_clear(Tbound);
}

// compute polynomial approx f and error bound for y solution of
// (-D)^(r)y + (-D)^(r-1)(a_{r-1} y) + ... + a_0 y = h
// y^[i](-1) = w[i]  0 <= i < r
// where y^[0] = y and y^[i+1] = a_{r-1-i} y - Dy^[i]

void mpfi_chebpoly_newtonpicard_Dx_approxvalid(mpfr_chebpoly_t f, mpfr_t bound, mpfi_chebpoly_srcptr a, long r, const mpfi_chebpoly_t h, mpfi_srcptr w, long N)
{
  // integral operator
  mpfi_chebpoly_lrintop_t K, R;
  mpfi_chebpoly_lrintop_init(K);
  mpfi_chebpoly_lrintop_init(R);
  mpfi_chebpoly_lrintop_set_lode_Dx(K, a, r);

  // right hand side
  mpfi_chebpoly_t g;
  mpfi_chebpoly_init(g);
  mpfi_chebpoly_lrintop_set_lode_Dx_rhs(g, r, h, w);

  // approximate solution
  mpfr_chebpoly_lrintop_t(Kfr);
  mpfr_chebpoly_lrintop_init(Kfr);
  mpfi_chebpoly_lrintop_get_mpfr_chebpoly_lrintop(Kfr, K);
  mpfr_chebpoly_t gfr;
  mpfr_chebpoly_init(gfr);
  mpfi_chebpoly_get_mpfr_chebpoly(gfr, g);
  mpfr_chebpoly_lrintop_approxsolve(f, Kfr, gfr, N);

  // contracting operator
  mpfr_t Tbound;
  mpfr_init(Tbound);
  mpfi_chebpoly_newtonpicard_Dx_Tbound(Tbound, R, a, r);

  // compute bound
  mpfi_chebpoly_newtonpicard_valid_aux(bound, K, Tbound, R, g, f);

  // clear variables
  mpfi_chebpoly_lrintop_clear(K);
  mpfi_chebpoly_lrintop_clear(R);
  mpfr_chebpoly_lrintop_clear(Kfr);
  mpfi_chebpoly_clear(g);
  mpfr_chebpoly_clear(gfr);
  mpfr_clear(Tbound);
}
