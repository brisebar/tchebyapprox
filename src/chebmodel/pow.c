
#include "chebmodel.h"


// set P := Q^e
void chebmodel_pow(chebmodel_t P, const chebmodel_t Q, unsigned long e)
{
  chebmodel_t Qe;
  chebmodel_init(Qe);
  mpfi_chebpoly_set_coeff_si(Qe->poly, 0, 1);
  chebmodel_t Qi;
  chebmodel_init(Qi);
  chebmodel_set(Qi, Q);

  while (e != 0) {
    if (e%2 == 1)
      chebmodel_mul(Qe, Qe, Qi);
    chebmodel_mul(Qi, Qi, Qi);
    e /= 2;
  }

  chebmodel_clear(Qi);
  chebmodel_swap(P, Qe);
  chebmodel_clear(Qe);
}
