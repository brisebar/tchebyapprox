
#include "chebmodel.h"


void chebmodel_sub(chebmodel_t P, const chebmodel_t Q, const chebmodel_t R)
{
  mpfi_chebpoly_sub(P->poly, Q->poly, R->poly);
  mpfr_add(P->rem, Q->rem, R->rem, MPFR_RNDU);
}
