
#include "chebmodel.h"


void chebmodel_add(chebmodel_t P, const chebmodel_t Q, const chebmodel_t R)
{
  mpfi_chebpoly_add(P->poly, Q->poly, R->poly);
  mpfr_add(P->rem, Q->rem, R->rem, MPFR_RNDU);
}
