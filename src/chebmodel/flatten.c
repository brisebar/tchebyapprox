
#include "chebmodel.h"


void chebmodel_flatten(chebmodel_t P)
{
  mpfr_t mid, rad;
  mpfr_inits(mid, rad, NULL);
  long i;
  long n = P->poly->degree;

  for (i = 0 ; i <= n ; i++) {
    mpfi_mid(mid, P->poly->coeffs + i);
    mpfi_diam_abs(rad, P->poly->coeffs + i);
    mpfr_mul_2si(rad, rad, -1, MPFR_RNDU);
    mpfi_set_fr(P->poly->coeffs + i, mid);
    mpfr_add(P->rem, P->rem, rad, MPFR_RNDU);
  }
  mpfr_clears(mid, rad, NULL);
}
