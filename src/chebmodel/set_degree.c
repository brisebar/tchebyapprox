
#include "chebmodel.h"


void chebmodel_set_degree(chebmodel_t P, long n)
{
  mpfi_chebpoly_set_degree(P->poly, n);
}
