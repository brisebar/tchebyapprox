
#include "chebmodel.h"


void chebmodel_antiderivative(chebmodel_t P, const chebmodel_t Q)
{
  mpfi_chebpoly_antiderivative(P->poly, Q->poly);
  mpfr_set(P->rem, Q->rem, MPFR_RNDU);
}


void chebmodel_antiderivative0(chebmodel_t P, const chebmodel_t Q)
{
  mpfi_chebpoly_antiderivative0(P->poly, Q->poly);
  mpfr_set(P->rem, Q->rem, MPFR_RNDU);
}


void chebmodel_antiderivative_1(chebmodel_t P, const chebmodel_t Q)
{
  mpfi_chebpoly_antiderivative_1(P->poly, Q->poly);
  mpfr_mul_2si(P->rem, Q->rem, 1, MPFR_RNDU);
}


void chebmodel_antiderivative1(chebmodel_t P, const chebmodel_t Q)
{
  mpfi_chebpoly_antiderivative1(P->poly, Q->poly);
  mpfr_mul_2si(P->rem, Q->rem, 1, MPFR_RNDU);
}
