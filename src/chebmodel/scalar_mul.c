
#include "chebmodel.h"


void chebmodel_scalar_mul_si(chebmodel_t P, const chebmodel_t Q, long c)
{
  mpfi_chebpoly_scalar_mul_si(P->poly, Q->poly, c);

  long d = c < 0 ? -c : c;
  mpfr_mul_si(P->rem, Q->rem, d, MPFR_RNDU);
}


void chebmodel_scalar_mul_z(chebmodel_t P, const chebmodel_t Q, const mpz_t c)
{
  mpfi_chebpoly_scalar_mul_z(P->poly, Q->poly, c);

  mpz_t d;
  mpz_init(d);
  if (mpz_sgn(c) < 0)
    mpz_neg(d, c);
  else
    mpz_set(d, c);
  mpfr_mul_z(P->rem, Q->rem, d, MPFR_RNDU);

  mpz_clear(d);
}


void chebmodel_scalar_mul_q(chebmodel_t P, const chebmodel_t Q, const mpq_t c)
{
  mpfi_chebpoly_scalar_mul_q(P->poly, Q->poly, c);

  mpq_t d;
  mpq_init(d);
  if (mpq_sgn(c) < 0)
    mpq_neg(d, c);
  else
    mpq_set(d, c);
  mpfr_mul_q(P->rem, Q->rem, d, MPFR_RNDU);

  mpq_clear(d);
}


void chebmodel_scalar_mul_d(chebmodel_t P, const chebmodel_t Q, const double_t c)
{
  mpfi_chebpoly_scalar_mul_d(P->poly, Q->poly, c);

  mpfr_t d;
  mpfr_init(d);
  if (*c < 0)
    mpfr_mul_d(P->rem, Q->rem, - *c, MPFR_RNDU);
  else
    mpfr_mul_d(P->rem, Q->rem, *c, MPFR_RNDU);
}


void chebmodel_scalar_mul_fr(chebmodel_t P, const chebmodel_t Q, const mpfr_t c)
{
  mpfi_chebpoly_scalar_mul_fr(P->poly, Q->poly, c);

  mpfr_t d;
  mpfr_init(d);
  if (mpfr_sgn(c) < 0)
    mpfr_neg(d, c, MPFR_RNDU);
  else
    mpfr_set(d, c, MPFR_RNDU);
  mpfr_mul(P->rem, Q->rem, d, MPFR_RNDU);

  mpfr_clear(d);
}


void chebmodel_scalar_mul_fi(chebmodel_t P, const chebmodel_t Q, const mpfi_t c)
{
  mpfi_chebpoly_scalar_mul_fi(P->poly, Q->poly, c);

  mpfr_t d;
  mpfr_init(d);
  mpfi_mag(d, c);
  mpfr_mul(P->rem, Q->rem, d, MPFR_RNDU);

  mpfr_clear(d);
}


void chebmodel_scalar_mul_2si(chebmodel_t P, const chebmodel_t Q, long k)
{
  mpfi_chebpoly_scalar_mul_2si(P->poly, Q->poly, k);
  mpfr_mul_2si(P->rem, Q->rem, k, MPFR_RNDU);
}
