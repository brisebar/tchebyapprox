
#include "chebmodel.h"


// N = degree of polynomial approximation
void chebmodel_inverse(chebmodel_t P, const chebmodel_t Q, long N)
{
  chebmodel_t Qflat;
  chebmodel_init(Qflat);
  chebmodel_set(Qflat, Q);
  chebmodel_flatten(Qflat);

  mpfr_chebpoly_t Qfr;
  mpfr_chebpoly_init(Qfr);
  mpfi_chebpoly_get_mpfr_chebpoly(Qfr, Qflat->poly);

  mpfr_t rem;
  mpfr_init(rem);
  // approx inverse of degree N
  mpfr_chebpoly_t F;
  mpfr_chebpoly_init(F);
  mpfr_chebpoly_inverse_approx(F, Qfr, N);
  
  // validating the bound ||F-1/Qfr||
  mpfr_chebpoly_inverse_validate2(rem, F, Qfr);

  // bound ||1/Qfr-1/Qflat||
  mpfr_t Qfrinv_norm, bound;
  mpfr_inits(Qfrinv_norm, bound, NULL);
  // Qfrinv_norm
  mpfr_chebpoly_1norm(Qfrinv_norm, F);
  mpfr_add(Qfrinv_norm, Qfrinv_norm, rem, MPFR_RNDU);
  // check < 1
  mpfr_mul(bound, Qflat->rem, Qfrinv_norm, MPFR_RNDU);
  if (mpfr_cmp_si(bound, 1) >= 0)
    fprintf(stderr, "chebmodel_inverse: error: approx inverse not precise enough\n");
  mpfr_si_sub(bound, 1, bound, MPFR_RNDD);
  mpfr_si_div(bound, 1, bound, MPFR_RNDU);
  mpfr_sub_si(bound, bound, 1, MPFR_RNDU);
  mpfr_mul(bound, bound, Qfrinv_norm, MPFR_RNDU);

  // final bound and assignments
  mpfi_chebpoly_set_mpfr_chebpoly(P->poly, F);
  mpfr_add(P->rem, rem, bound, MPFR_RNDU);

  // clear variables
  chebmodel_clear(Qflat);
  mpfr_chebpoly_clear(Qfr);
  mpfr_chebpoly_clear(F);
  mpfr_clears(Qfrinv_norm, bound, rem, NULL);
}
