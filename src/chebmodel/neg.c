
#include "chebmodel.h"


void chebmodel_neg(chebmodel_t P, const chebmodel_t Q)
{
  mpfi_chebpoly_neg(P->poly, Q->poly);
  mpfr_set(P->rem, Q->rem, MPFR_RNDU);
}
