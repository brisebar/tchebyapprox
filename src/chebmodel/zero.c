
#include "chebmodel.h"


void chebmodel_zero(chebmodel_t P)
{
  mpfi_chebpoly_zero(P->poly);
  mpfr_set_zero(P->rem, 1);
}
