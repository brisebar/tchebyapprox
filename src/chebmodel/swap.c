
#include "chebmodel.h"


void chebmodel_swap(chebmodel_t P, chebmodel_t Q)
{
  mpfi_chebpoly_swap(P->poly, Q->poly);
  mpfr_swap(P->rem, Q->rem);
}
