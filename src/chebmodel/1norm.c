
#include "chebmodel.h"


void chebmodel_1norm_ubound(mpfr_t norm, const chebmodel_t P)
{
  mpfi_chebpoly_1norm_ubound(norm, P->poly);
  mpfr_add(norm, norm, P->rem, MPFR_RNDU);
}


void chebmodel_1norm_lbound(mpfr_t norm, const chebmodel_t P)
{
  mpfi_t t;
  mpfi_init(t);

  mpfi_chebpoly_1norm(t, P->poly);
  mpfr_set(norm, &(t->left), MPFR_RNDD);
 
  if (mpfr_cmp(norm, P->rem) > 0)
    mpfr_sub(norm, norm, P->rem, MPFR_RNDD);
  else
    mpfr_set_zero(norm, 1);

  mpfi_clear(t);
}


void chebmodel_1norm(mpfi_t norm, const chebmodel_t P)
{
  chebmodel_1norm_ubound(&(norm->right), P);
  chebmodel_1norm_lbound(&(norm->left), P);
}
