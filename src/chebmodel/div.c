
#include "chebmodel.h"


// N = degree of polynomial approximation
void chebmodel_div(chebmodel_t R, const chebmodel_t P, const chebmodel_t Q, long N)
{
  chebmodel_t Res;
  chebmodel_init(Res);
  chebmodel_inverse(Res, Q, N);
  chebmodel_mul(Res, P, Res);
  chebmodel_truncate(Res, N);

  chebmodel_swap(R, Res);
  chebmodel_clear(Res);
}  


