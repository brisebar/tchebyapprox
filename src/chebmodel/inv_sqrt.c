
#include "chebmodel.h"


// N = degree of polynomial approximation
void chebmodel_inv_sqrt(chebmodel_t P, const chebmodel_t Q, long N)
{
  chebmodel_t Qflat;
  chebmodel_init(Qflat);
  chebmodel_set(Qflat, Q);
  chebmodel_flatten(Qflat);

  mpfr_chebpoly_t Qfr;
  mpfr_chebpoly_init(Qfr);
  mpfi_chebpoly_get_mpfr_chebpoly(Qfr, Qflat->poly);

  mpfr_t rem;
  mpfr_inits(rem, NULL);

  // approx square root and inverse square root of degree N
  chebmodel_t F, G;
  chebmodel_init(F);
  chebmodel_init(G);
  mpfr_chebpoly_t Ffr, Gfr;
  mpfr_chebpoly_init(Ffr);
  mpfr_chebpoly_init(Gfr);
  mpfr_chebpoly_inv_sqrt_approx(Ffr, Qfr, N);
  chebmodel_set_mpfr_chebpoly(F, Ffr);
  mpfr_chebpoly_inverse_approx(Gfr, Qfr, N);
  chebmodel_set_mpfr_chebpoly(G, Gfr);

  // validating the bound ||F-sqrt(Qfr)||, ||G-1/sqrt(Qfr)|| and ||H-1/Qfr||
  mpfr_chebpoly_inv_sqrt_validate2(F->rem, Ffr, Qfr);
  mpfr_set(rem, F->rem, MPFR_RNDU);
  mpfr_chebpoly_inverse_validate2(G->rem, Gfr, Qfr);

  // bound ||sqrt(Qfr)-sqrt(Qflat)||
  mpfr_t temp1, temp2;
  mpfr_inits(temp1, temp2, NULL);
  chebmodel_mul(F, F, G); // F = 1/Qfr^3/2 +- qqch
  mpfr_chebpoly_1norm(temp1, Gfr);
  mpfr_mul(temp1, temp1, Qflat->rem, MPFR_RNDU);
  if (mpfr_cmp_si(temp1, 1) >= 0)
    fprintf(stderr, "chebmodel_inv_sqrt: error: approx inverse square root not precise enough\n");
  mpfr_si_sub(temp1, 1, temp1, MPFR_RNDD);
  mpfr_sqrt(temp1, temp1, MPFR_RNDD);
  mpfr_set(temp2, temp1, MPFR_RNDD);
  mpfr_add_si(temp1, temp1, 1, MPFR_RNDD);
  mpfr_div(temp1, Qflat->rem, temp1, MPFR_RNDU);
  mpfr_si_div(temp2, 1, temp2, MPFR_RNDU);
  mpfr_mul(temp1, temp1, temp2, MPFR_RNDU);
  mpfr_mul(temp1, temp1, F->rem, MPFR_RNDU);

  // final bound and assignments
  mpfi_chebpoly_set_mpfr_chebpoly(P->poly, Ffr);
  mpfr_add(P->rem, rem, temp1, MPFR_RNDU);

  // clear variables
  chebmodel_clear(Qflat);
  mpfr_chebpoly_clear(Qfr);
  mpfr_chebpoly_clear(Ffr);
  mpfr_chebpoly_clear(Gfr);
  chebmodel_clear(F);
  chebmodel_clear(G);
  mpfr_clears(rem, temp1, temp2, NULL);
}


