
#include "chebmodel.h"


void chebmodel_evaluate_si(mpfi_t y, const chebmodel_t P, long x)
{
  mpfi_chebpoly_evaluate_si(y, P->poly, x);
  mpfr_add(&(y->right), &(y->right), P->rem, MPFR_RNDU);
  mpfr_sub(&(y->left), &(y->left), P->rem, MPFR_RNDD);
}


void chebmodel_evaluate_z(mpfi_t y, const chebmodel_t P, const mpz_t x)
{
  mpfi_chebpoly_evaluate_z(y, P->poly, x);
  mpfr_add(&(y->right), &(y->right), P->rem, MPFR_RNDU);
  mpfr_sub(&(y->left), &(y->left), P->rem, MPFR_RNDD);
}


void chebmodel_evaluate_q(mpfi_t y, const chebmodel_t P, const mpq_t x)
{
  mpfi_chebpoly_evaluate_q(y, P->poly, x);
  mpfr_add(&(y->right), &(y->right), P->rem, MPFR_RNDU);
  mpfr_sub(&(y->left), &(y->left), P->rem, MPFR_RNDD);
}


void chebmodel_evaluate_d(mpfi_t y, const chebmodel_t P, const double_t x)
{
  mpfi_chebpoly_evaluate_d(y, P->poly, x);
  mpfr_add(&(y->right), &(y->right), P->rem, MPFR_RNDU);
  mpfr_sub(&(y->left), &(y->left), P->rem, MPFR_RNDD);
}


void chebmodel_evaluate_fr(mpfi_t y, const chebmodel_t P, const mpfr_t x)
{
  mpfi_chebpoly_evaluate_fr(y, P->poly, x);
  mpfr_add(&(y->right), &(y->right), P->rem, MPFR_RNDU);
  mpfr_sub(&(y->left), &(y->left), P->rem, MPFR_RNDD);
}


void chebmodel_evaluate_fi(mpfi_t y, const chebmodel_t P, const mpfi_t x)
{
  mpfi_chebpoly_evaluate_fi(y, P->poly, x);
  mpfr_add(&(y->right), &(y->right), P->rem, MPFR_RNDU);
  mpfr_sub(&(y->left), &(y->left), P->rem, MPFR_RNDD);
}


void chebmodel_evaluate_1(mpfi_t y, const chebmodel_t P)
{
  mpfi_chebpoly_evaluate_1(y, P->poly);
  mpfr_add(&(y->right), &(y->right), P->rem, MPFR_RNDU);
  mpfr_sub(&(y->left), &(y->left), P->rem, MPFR_RNDD);
}


void chebmodel_evaluate0(mpfi_t y, const chebmodel_t P)
{
  mpfi_chebpoly_evaluate0(y, P->poly);
  mpfr_add(&(y->right), &(y->right), P->rem, MPFR_RNDU);
  mpfr_sub(&(y->left), &(y->left), P->rem, MPFR_RNDD);
}


void chebmodel_evaluate1(mpfi_t y, const chebmodel_t P)
{
  mpfi_chebpoly_evaluate1(y, P->poly);
  mpfr_add(&(y->right), &(y->right), P->rem, MPFR_RNDU);
  mpfr_sub(&(y->left), &(y->left), P->rem, MPFR_RNDD);
}
