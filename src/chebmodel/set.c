
#include "chebmodel.h"


void chebmodel_set(chebmodel_t P, const chebmodel_t Q)
{
  mpfi_chebpoly_set(P->poly, Q->poly);
  mpfr_set(P->rem, Q->rem, MPFR_RNDU);
}


void chebmodel_set_double_chebpoly(chebmodel_t P, const double_chebpoly_t Q)
{
  mpfi_chebpoly_set_double_chebpoly(P->poly, Q);
  mpfr_set_zero(P->rem, 1);
}


void chebmodel_set_mpfr_chebpoly(chebmodel_t P, const mpfr_chebpoly_t Q)
{
  mpfi_chebpoly_set_mpfr_chebpoly(P->poly, Q);
  mpfr_set_zero(P->rem, 1);
}


void chebmodel_set_mpfi_chebpoly(chebmodel_t P, const mpfi_chebpoly_t Q)
{
  mpfi_chebpoly_set(P->poly, Q);
  mpfr_set_zero(P->rem, 1);
}


void chebmodel_get_double_chebpoly(double_chebpoly_t P, const chebmodel_t Q)
{
  mpfi_chebpoly_get_double_chebpoly(P, Q->poly);
}


void chebmodel_get_mpfr_chebpoly(mpfr_chebpoly_t P, const chebmodel_t Q)
{
  mpfi_chebpoly_get_mpfr_chebpoly(P, Q->poly);
}


void chebmodel_get_mpfi_chebpoly(mpfi_chebpoly_t P, const chebmodel_t Q)
{
  mpfi_chebpoly_set(P, Q->poly);
}
