
#include "chebmodel.h"


void chebmodel_get_coeff(mpfi_t c, const chebmodel_t P, long n)
{
  mpfi_chebpoly_get_coeff(c, P->poly, n);
  mpfr_add(&(c->right), &(c->right), P->rem, MPFR_RNDU);
  mpfr_sub(&(c->left), &(c->left), P->rem, MPFR_RNDD);
}
