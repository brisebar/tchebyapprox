
#include "chebmodel.h"


// N = degree of polynomial approximation
void chebmodel_sqrt(chebmodel_t P, const chebmodel_t Q, long N)
{
  chebmodel_t Qflat;
  chebmodel_init(Qflat);
  chebmodel_set(Qflat, Q);
  chebmodel_flatten(Qflat);

  mpfr_chebpoly_t Qfr;
  mpfr_chebpoly_init(Qfr);
  mpfi_chebpoly_get_mpfr_chebpoly(Qfr, Qflat->poly);

  mpfr_t remF, remG, remH;
  mpfr_inits(remF, remG, remH, NULL);

  // approx square root and inverse square root of degree N
  mpfr_chebpoly_t F, G, H;
  mpfr_chebpoly_init(F);
  mpfr_chebpoly_init(G);
  mpfr_chebpoly_init(H);
  mpfr_chebpoly_sqrt_inv_sqrt_approx(F, G, Qfr, N);
  mpfr_chebpoly_inverse_approx(H, Qfr, N);

  // validating the bound ||F-sqrt(Qfr)||, ||G-1/sqrt(Qfr)|| and ||H-1/Qfr||
  mpfr_chebpoly_sqrt_validate(remF, F, G, Qfr);
  mpfr_chebpoly_inv_sqrt_validate2(remG, G, Qfr);
  mpfr_chebpoly_inverse_validate2(remH, H, Qfr);

  // bound ||sqrt(Qfr)-sqrt(Qflat)||
  mpfr_t temp1, temp2;
  mpfr_inits(temp1, temp2, NULL);
  mpfr_chebpoly_1norm(temp1, G);
  mpfr_add(temp1, temp1, remG, MPFR_RNDU);
  mpfr_chebpoly_1norm(temp2, H);
  mpfr_add(temp2, temp2, remH, MPFR_RNDU);
  mpfr_mul(temp2, temp2, Qflat->rem, MPFR_RNDU);
  if (mpfr_cmp_si(temp2, 1) >= 0)
    fprintf(stderr, "chebmodel_sqrt: error: approx square root not precise enough\n");
  mpfr_si_sub(temp2, 1, temp2, MPFR_RNDD);
  mpfr_sqrt(temp2, temp2, MPFR_RNDD);
  mpfr_add_si(temp2, temp2, 1, MPFR_RNDD);
  mpfr_div(temp2, Qflat->rem, temp2, MPFR_RNDU);
  mpfr_mul(temp1, temp1, temp2, MPFR_RNDU);

  // final bound and assignments
  mpfi_chebpoly_set_mpfr_chebpoly(P->poly, F);
  mpfr_add(P->rem, remF, temp1, MPFR_RNDU);

  // clear variables
  chebmodel_clear(Qflat);
  mpfr_chebpoly_clear(Qfr);
  mpfr_chebpoly_clear(F);
  mpfr_chebpoly_clear(G);
  mpfr_chebpoly_clear(H);
  mpfr_clears(temp1, temp2, remF, remG, remH, NULL);
}


