
#include "chebmodel.h"


void chebmodel_init(chebmodel_t P)
{
  mpfi_chebpoly_init(P->poly);
  mpfr_init(P->rem);
  mpfr_set_zero(P->rem, 1);
}
