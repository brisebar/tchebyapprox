
#include "chebmodel.h"


long chebmodel_degree(const chebmodel_t P)
{
  return mpfi_chebpoly_degree(P->poly);
}
