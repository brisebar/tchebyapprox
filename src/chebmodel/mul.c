
#include "chebmodel.h"


void chebmodel_mul(chebmodel_t P, const chebmodel_t Q, const chebmodel_t R)
{
  chebmodel_t QR;
  chebmodel_init(QR);

  mpfi_chebpoly_mul(QR->poly, Q->poly, R->poly);

  mpfr_t t;
  mpfr_init(t);

  mpfi_chebpoly_1norm_ubound(t, Q->poly);
  mpfr_mul(QR->rem, t, R->rem, MPFR_RNDU);

  mpfi_chebpoly_1norm_ubound(t, R->poly);
  mpfr_mul(t, t, Q->rem, MPFR_RNDU);
  mpfr_add(QR->rem, QR->rem, t, MPFR_RNDU);

  mpfr_mul(t, Q->rem, R->rem, MPFR_RNDU);
  mpfr_add(QR->rem, QR->rem, t, MPFR_RNDU);

  // final assignment and clears variables
  chebmodel_swap(P, QR);
  chebmodel_clear(QR);
  mpfr_clear(t);

}
