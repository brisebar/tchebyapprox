
#include "chebmodel.h"


void chebmodel_truncate(chebmodel_t P, long n)
{
  long i;
  long N = P->poly->degree;

  mpfr_t temp;
  mpfr_init(temp);

  if (n < -1)
    n = -1;

  if (n < N)
    for (i = n+1 ; i <= N ; i++) {
      mpfi_mag(temp, P->poly->coeffs + i);
      mpfr_add(P->rem, P->rem, temp, MPFR_RNDU);
    }

  mpfi_chebpoly_set_degree(P->poly, n);
}
