
#include "chebmodel.h"


void chebmodel_print(const chebmodel_t P, const char * var, size_t digits)
{
  mpfi_chebpoly_print(P->poly, var, digits);
  printf(" +- ");
  mpfr_out_str(stdout, 10, 10, P->rem, MPFR_RNDU);
}
