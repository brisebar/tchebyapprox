
#include "chebmodel.h"


void chebmodel_clear(chebmodel_t P)
{
  mpfi_chebpoly_clear(P->poly);
  mpfr_clear(P->rem);
}
