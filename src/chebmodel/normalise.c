
#include "chebmodel.h"


void chebmodel_normalise(chebmodel_t P)
{
  mpfi_chebpoly_normalise(P->poly);
}
