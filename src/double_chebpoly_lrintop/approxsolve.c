
#include "double_chebpoly_lrintop.h"


// solve F s.t. F + K(F) = G by truncating K at order N
void double_chebpoly_lrintop_approxsolve(double_chebpoly_t F, const double_chebpoly_lrintop_t K, const double_chebpoly_t G, long N)
{
  // convert K to double_chebpoly_intop_t KK
  double_chebpoly_intop_t KK;
  double_chebpoly_intop_init(KK);
  double_chebpoly_lrintop_get_intop(KK, K);

  // negate due to convention used by double_chebpoly_intop_t
  long i;
  for (i = 0 ; i < KK->order ; i++)
    double_chebpoly_neg(KK->alpha + i, KK->alpha + i);

  // solve system
  double_chebpoly_intop_approxsolve(F, KK, G, N);

  double_chebpoly_intop_clear(KK);
}
