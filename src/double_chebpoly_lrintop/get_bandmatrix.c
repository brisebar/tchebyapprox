
#include "double_chebpoly_lrintop.h"


// get bandmatrix
void double_chebpoly_lrintop_get_bandmatrix(double_bandmatrix_t M, const double_chebpoly_lrintop_t K, long N)
{
  double_chebpoly_intop_t KK;
  double_chebpoly_intop_init(KK);
  double_chebpoly_lrintop_get_intop(KK, K);
  double_chebpoly_intop_get_bandmatrix(M, KK, N);
  double_chebpoly_intop_clear(KK);
}
