
#include "double_chebpoly_lrintop.h"


// Mlpoly, Mrpoly must be freshed allocated arrays of double_chebpoly of size r1+r2
void _double_chebpoly_lrintop_add(double_chebpoly_ptr Mlpoly, double_chebpoly_ptr Mrpoly, double_chebpoly_srcptr Klpoly, double_chebpoly_srcptr Krpoly, long r1, double_chebpoly_srcptr Llpoly, double_chebpoly_srcptr Lrpoly, long r2)
{
  long i;

  for (i = 0 ; i < r1 ; i++) {
    double_chebpoly_set(Mlpoly + i, Klpoly + i);
    double_chebpoly_set(Mrpoly + i, Krpoly + i);
  }

  for (i = 0 ; i < r2 ; i++) {
    double_chebpoly_set(Mlpoly + r1 + i, Llpoly + i);
    double_chebpoly_set(Mrpoly + r1 + i, Lrpoly + i);
  }

}


void double_chebpoly_lrintop_add(double_chebpoly_lrintop_t M, const double_chebpoly_lrintop_t K, const double_chebpoly_lrintop_t L)
{
  double_chebpoly_lrintop_t K_L;
  double_chebpoly_lrintop_init(K_L);

  double_chebpoly_lrintop_set_length(K_L, K->length + L->length);
  _double_chebpoly_lrintop_add(K_L->lpoly, K_L->rpoly, K->lpoly, K->rpoly, K->length, L->lpoly, L->rpoly, L->length);

  double_chebpoly_lrintop_swap(M, K_L);
  double_chebpoly_lrintop_clear(K_L);
}
