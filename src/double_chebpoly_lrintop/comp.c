
#include "double_chebpoly_lrintop.h"


// Mlpoly, Mrpoly must be freshed allocated arrays of double_chebpoly of size r1+r2
void _double_chebpoly_lrintop_comp(double_chebpoly_ptr Mlpoly, double_chebpoly_ptr Mrpoly, double_chebpoly_srcptr Klpoly, double_chebpoly_srcptr Krpoly, long r1, double_chebpoly_srcptr Llpoly, double_chebpoly_srcptr Lrpoly, long r2)
{
  double_chebpoly_t P, Q;
  double_chebpoly_init(P);
  double_chebpoly_init(Q);
  long i, j;

  // initialization
  for (i = 0 ; i < r1 ; i++) {
    double_chebpoly_set(Mlpoly + i, Klpoly + i);
    double_chebpoly_zero(Mrpoly + i);
  }

  for (j = 0 ; j < r2 ; j++) {
    double_chebpoly_zero(Mlpoly + r1 + j);
    double_chebpoly_set(Mrpoly + r1 + j, Lrpoly + j);
  }

  // composition loop
  for (i = 0 ; i < r1 ; i++)
    for (j = 0 ; j < r2 ; j++) {

      // primitive of middle product
      double_chebpoly_mul(P, Krpoly + i, Llpoly + j);
      double_chebpoly_antiderivative(P, P);

      // distribute to the left
      double_chebpoly_mul(Q, Klpoly + i, P);
      double_chebpoly_add(Mlpoly + r1 + j, Mlpoly + r1 + j, Q);

      // distribute to the right
      double_chebpoly_mul(Q, P, Lrpoly + j);
      double_chebpoly_sub(Mrpoly + i, Mrpoly + i, Q);

    }

  double_chebpoly_clear(P);
  double_chebpoly_clear(Q);

}


// set M := K ∘ L
void double_chebpoly_lrintop_comp(double_chebpoly_lrintop_t M, const double_chebpoly_lrintop_t K, const double_chebpoly_lrintop_t L)
{
  double_chebpoly_lrintop_t K_L;
  double_chebpoly_lrintop_init(K_L);

  double_chebpoly_lrintop_set_length(K_L, K->length + L->length);
  _double_chebpoly_lrintop_comp(K_L->lpoly, K_L->rpoly, K->lpoly, K->rpoly, K->length, L->lpoly, L->rpoly, L->length);

  double_chebpoly_lrintop_swap(M, K_L);
  double_chebpoly_lrintop_clear(K_L);
}
