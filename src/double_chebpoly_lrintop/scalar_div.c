
#include "double_chebpoly_lrintop.h"

void _double_chebpoly_lrintop_scalar_div_si(double_chebpoly_ptr Llpoly, double_chebpoly_ptr Lrpoly, double_chebpoly_srcptr Klpoly, double_chebpoly_srcptr Krpoly, long r, long c)
{
  long i;
  for (i = 0 ; i < r ; i++) {
    double_chebpoly_scalar_div_si(Llpoly + i, Klpoly + i, c);
    double_chebpoly_set(Lrpoly + i, Krpoly + i);
  }
}


// set L := K / c
void double_chebpoly_lrintop_scalar_div_si(double_chebpoly_lrintop_t L, const double_chebpoly_lrintop_t K, long c)
{
  double_chebpoly_lrintop_set_length(L, K->length);
  _double_chebpoly_lrintop_scalar_div_si(L->lpoly, L->rpoly, K->lpoly, K->rpoly, K->length, c);
}


void _double_chebpoly_lrintop_scalar_div_z(double_chebpoly_ptr Llpoly, double_chebpoly_ptr Lrpoly, double_chebpoly_srcptr Klpoly, double_chebpoly_srcptr Krpoly, long r, const mpz_t c)
{
  long i;
  for (i = 0 ; i < r ; i++) {
    double_chebpoly_scalar_div_z(Llpoly + i, Klpoly + i, c);
    double_chebpoly_set(Lrpoly + i, Krpoly + i);
  }
}


// set L := K / c
void double_chebpoly_lrintop_scalar_div_z(double_chebpoly_lrintop_t L, const double_chebpoly_lrintop_t K, const mpz_t c)
{
  double_chebpoly_lrintop_set_length(L, K->length);
  _double_chebpoly_lrintop_scalar_div_z(L->lpoly, L->rpoly, K->lpoly, K->rpoly, K->length, c);
}


void _double_chebpoly_lrintop_scalar_div_q(double_chebpoly_ptr Llpoly, double_chebpoly_ptr Lrpoly, double_chebpoly_srcptr Klpoly, double_chebpoly_srcptr Krpoly, long r, const mpq_t c)
{
  long i;
  for (i = 0 ; i < r ; i++) {
    double_chebpoly_scalar_div_q(Llpoly + i, Klpoly + i, c);
    double_chebpoly_set(Lrpoly + i, Krpoly + i);
  }
}


// set L := K / c
void double_chebpoly_lrintop_scalar_div_q(double_chebpoly_lrintop_t L, const double_chebpoly_lrintop_t K, const mpq_t c)
{
  double_chebpoly_lrintop_set_length(L, K->length);
  _double_chebpoly_lrintop_scalar_div_q(L->lpoly, L->rpoly, K->lpoly, K->rpoly, K->length, c);
}


void _double_chebpoly_lrintop_scalar_div_d(double_chebpoly_ptr Llpoly, double_chebpoly_ptr Lrpoly, double_chebpoly_srcptr Klpoly, double_chebpoly_srcptr Krpoly, long r, const double_t c)
{
  long i;
  for (i = 0 ; i < r ; i++) {
    double_chebpoly_scalar_div_d(Llpoly + i, Klpoly + i, c);
    double_chebpoly_set(Lrpoly + i, Krpoly + i);
  }
}


// set L := K / c
void double_chebpoly_lrintop_scalar_div_d(double_chebpoly_lrintop_t L, const double_chebpoly_lrintop_t K, const double_t c)
{
  double_chebpoly_lrintop_set_length(L, K->length);
  _double_chebpoly_lrintop_scalar_div_d(L->lpoly, L->rpoly, K->lpoly, K->rpoly, K->length, c);
}


void _double_chebpoly_lrintop_scalar_div_fr(double_chebpoly_ptr Llpoly, double_chebpoly_ptr Lrpoly, double_chebpoly_srcptr Klpoly, double_chebpoly_srcptr Krpoly, long r, const mpfr_t c)
{
  long i;
  for (i = 0 ; i < r ; i++) {
    double_chebpoly_scalar_div_fr(Llpoly + i, Klpoly + i, c);
    double_chebpoly_set(Lrpoly + i, Krpoly + i);
  }
}


// set L := K / c
void double_chebpoly_lrintop_scalar_div_fr(double_chebpoly_lrintop_t L, const double_chebpoly_lrintop_t K, const mpfr_t c)
{
  double_chebpoly_lrintop_set_length(L, K->length);
  _double_chebpoly_lrintop_scalar_div_fr(L->lpoly, L->rpoly, K->lpoly, K->rpoly, K->length, c);
}


void _double_chebpoly_lrintop_scalar_div_2si(double_chebpoly_ptr Llpoly, double_chebpoly_ptr Lrpoly, double_chebpoly_srcptr Klpoly, double_chebpoly_srcptr Krpoly, long r, long k)
{
  long i;
  for (i = 0 ; i < r ; i++) {
    double_chebpoly_scalar_div_2si(Llpoly + i, Klpoly + i, k);
    double_chebpoly_set(Lrpoly + i, Krpoly + i);
  }
}


// set L := 2^-k * K
void double_chebpoly_lrintop_scalar_div_2si(double_chebpoly_lrintop_t L, const double_chebpoly_lrintop_t K, long k)
{
  double_chebpoly_lrintop_set_length(L, K->length);
  _double_chebpoly_lrintop_scalar_div_2si(L->lpoly, L->rpoly, K->lpoly, K->rpoly, K->length, k);
}
