
#include "double_chebpoly_lrintop.h"


/* convert from double_chebpoly_intop_t */
void double_chebpoly_lrintop_set_intop(double_chebpoly_lrintop_t M, const double_chebpoly_intop_t K)
{
  long r = K->order;
  double_chebpoly_lrintop_set_length(M, r);
  long i;
  for (i = 0 ; i < r ; i++) {
    double_chebpoly_set(M->lpoly + i, K->alpha + i);
    double_chebpoly_zero(M->rpoly + i);
    double_chebpoly_set_coeff_si(M->rpoly + i, i, 1);
  }
}


/* convert to double_chebpoly_intop_t */
void double_chebpoly_lrintop_get_intop(double_chebpoly_intop_t M, const double_chebpoly_lrintop_t K)
{
  long r = K->length;
  long i, j;

  // order of M = max degree rpoly in s + 1
  long s = 0;
  for (i = 0 ; i < r ; i++)
    s = s < K->rpoly[i].degree ? K->rpoly[i].degree : s;
  s++;
  double_chebpoly_intop_set_order(M, s);
  for (i = 0 ; i < s ; i++)
    double_chebpoly_zero(M->alpha + i);

  double_chebpoly_t Temp;
  double_chebpoly_init(Temp);
  for (i = 0 ; i < r ; i++)
    for (j = 0 ; j <= K->rpoly[i].degree ; j++) {
      double_chebpoly_scalar_mul_d(Temp, K->lpoly + i, K->rpoly[i].coeffs + j);
      double_chebpoly_add(M->alpha + j, M->alpha + j, Temp);
    }

  double_chebpoly_clear(Temp);
}
