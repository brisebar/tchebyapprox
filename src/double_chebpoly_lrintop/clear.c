
#include "double_chebpoly_lrintop.h"


void double_chebpoly_lrintop_clear(double_chebpoly_lrintop_t K)
{
  long r = K->length;
  long i;
  for (i = 0 ; i < r ; i++) {
    double_chebpoly_clear(K->lpoly + i);
    double_chebpoly_clear(K->rpoly + i);
  }
  free(K->lpoly);
  free(K->rpoly);
}
