
#include "double_chebpoly_lrintop.h"


void double_chebpoly_lrintop_print(const double_chebpoly_lrintop_t K, const char * Cheb_var)
{
  long r = K->length;
  long i;

  for (i = 0 ; i < r ; i++) {
    printf("lpoly[%ld] = ", i);
    double_chebpoly_print(K->lpoly + i, Cheb_var);
    printf("\nrpoly[%ld] = ", i);
    double_chebpoly_print(K->rpoly + i, Cheb_var);
    printf("\n");
  }
}
