
#include "double_chebpoly_lrintop.h"


void double_chebpoly_lrintop_evaluate_d(double_chebpoly_t P, const double_chebpoly_lrintop_t K, const double_chebpoly_t Q)
{
  long r = K->length;
  long i;

  double_chebpoly_t K_Q, K_Q_i;
  double_chebpoly_init(K_Q);
  double_chebpoly_init(K_Q_i);

  for (i = 0 ; i < r ; i++) {
    double_chebpoly_mul(K_Q_i, K->rpoly + i, Q);
    double_chebpoly_antiderivative_1(K_Q_i, K_Q_i);
    double_chebpoly_mul(K_Q_i, K->lpoly + i, K_Q_i);
    double_chebpoly_add(K_Q, K_Q, K_Q_i);
  }

  // clear variables
  double_chebpoly_clear(K_Q_i);
  double_chebpoly_swap(P, K_Q);
  double_chebpoly_clear(K_Q);
}


void double_chebpoly_lrintop_evaluate_fr(mpfr_chebpoly_t P, const double_chebpoly_lrintop_t K, const mpfr_chebpoly_t Q)
{
  long r = K->length;
  long i;

  mpfr_chebpoly_t K_Q, K_Q_i, Temp;
  mpfr_chebpoly_init(K_Q);
  mpfr_chebpoly_init(K_Q_i);
  mpfr_chebpoly_init(Temp);

  for (i = 0 ; i < r ; i++) {
    mpfr_chebpoly_set_double_chebpoly(Temp, K->rpoly + i);
    mpfr_chebpoly_mul(K_Q_i, Temp, Q);
    mpfr_chebpoly_antiderivative_1(K_Q_i, K_Q_i);
    mpfr_chebpoly_set_double_chebpoly(Temp, K->lpoly + i);
    mpfr_chebpoly_mul(K_Q_i, Temp, K_Q_i);
    mpfr_chebpoly_add(K_Q, K_Q, K_Q_i);
  }

  // clear variables
  mpfr_chebpoly_clear(Temp);
  mpfr_chebpoly_clear(K_Q_i);
  mpfr_chebpoly_swap(P, K_Q);
  mpfr_chebpoly_clear(K_Q);
}


void double_chebpoly_lrintop_evaluate_fi(mpfi_chebpoly_t P, const double_chebpoly_lrintop_t K, const mpfi_chebpoly_t Q)
{
  long r = K->length;
  long i;

  mpfi_chebpoly_t K_Q, K_Q_i, Temp;
  mpfi_chebpoly_init(K_Q);
  mpfi_chebpoly_init(K_Q_i);
  mpfi_chebpoly_init(Temp);

  for (i = 0 ; i < r ; i++) {
    mpfi_chebpoly_set_double_chebpoly(Temp, K->rpoly + i);
    mpfi_chebpoly_mul(K_Q_i, Temp, Q);
    mpfi_chebpoly_antiderivative_1(K_Q_i, K_Q_i);
    mpfi_chebpoly_set_double_chebpoly(Temp, K->lpoly + i);
    mpfi_chebpoly_mul(K_Q_i, Temp, K_Q_i);
    mpfi_chebpoly_add(K_Q, K_Q, K_Q_i);
  }

  // clear variables
  mpfi_chebpoly_clear(Temp);
  mpfi_chebpoly_clear(K_Q_i);
  mpfi_chebpoly_swap(P, K_Q);
  mpfi_chebpoly_clear(K_Q);
}


void double_chebpoly_lrintop_evaluate_cm(chebmodel_t P, const double_chebpoly_lrintop_t K, const chebmodel_t Q)
{
  long r = K->length;
  long i;

  chebmodel_t K_Q, K_Q_i, Temp;
  chebmodel_init(K_Q);
  chebmodel_init(K_Q_i);
  chebmodel_init(Temp);

  for (i = 0 ; i < r ; i++) {
    chebmodel_set_double_chebpoly(Temp, K->rpoly + i);
    chebmodel_mul(K_Q_i, Temp, Q);
    chebmodel_antiderivative_1(K_Q_i, K_Q_i);
    chebmodel_set_double_chebpoly(Temp, K->lpoly + i);
    chebmodel_mul(K_Q_i, Temp, K_Q_i);
    chebmodel_add(K_Q, K_Q, K_Q_i);
  }

  // clear variables
  chebmodel_clear(Temp);
  chebmodel_clear(K_Q_i);
  chebmodel_swap(P, K_Q);
  chebmodel_clear(K_Q);
}
