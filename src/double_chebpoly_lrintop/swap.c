
#include "double_chebpoly_lrintop.h"


void double_chebpoly_lrintop_swap(double_chebpoly_lrintop_t K, double_chebpoly_lrintop_t L)
{
  long length_buf = K->length;
  double_chebpoly_ptr lpoly_buf = K->lpoly;
  double_chebpoly_ptr rpoly_buf = K->rpoly;

  K->length = L->length;
  K->lpoly = L->lpoly;
  K->rpoly = L->rpoly;

  L->length = length_buf;
  L->lpoly = lpoly_buf;
  L->rpoly = rpoly_buf;
}
