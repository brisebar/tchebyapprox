
#include "double_chebpoly_lrintop.h"


void _double_chebpoly_lrintop_neg(double_chebpoly_ptr Llpoly, double_chebpoly_ptr Lrpoly, double_chebpoly_srcptr Klpoly, double_chebpoly_srcptr Krpoly, long r)
{
  long i;
  for (i = 0 ; i < r ; i++) {
    double_chebpoly_neg(Llpoly + i, Klpoly + i);
    double_chebpoly_set(Lrpoly + i, Krpoly + i);
  }
}

void double_chebpoly_lrintop_neg(double_chebpoly_lrintop_t L, const double_chebpoly_lrintop_t K)
{
  double_chebpoly_lrintop_set_length(L, K->length);
  _double_chebpoly_lrintop_neg(L->lpoly, L->rpoly, K->lpoly, K->rpoly, K->length);
}
