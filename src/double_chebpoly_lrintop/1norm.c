
#include "double_chebpoly_lrintop.h"

typedef mpfi_ptr * mpfi_ptr_ptr;


// crude bound on the sum
void double_chebpoly_lrintop_lr1norm(double_t b, const double_chebpoly_lrintop_t K)
{
  mpfi_t bound, lbi, rbi;
  mpfi_init(bound);
  mpfi_init(lbi);
  mpfi_init(rbi);
  mpfi_set_si(bound, 0);
  long i;

  for (i = 0 ; i < K->length ; i++) {
    double_chebpoly_1norm_fi(lbi, K->lpoly + i);
    double_chebpoly_1norm_fi(rbi, K->rpoly + i);
    mpfi_mul(lbi, lbi, rbi);
    mpfi_add(bound, bound, lbi);
  }
  mpfi_mul_2si(bound, bound, 1);

  double_set_fr(b, &(bound->right), MPFR_RNDU);
  mpfi_clear(bound);
  mpfi_clear(lbi);
  mpfi_clear(rbi);
}


// convert to 2-dimensional polynomial in Chebyshev basis and use 1 norm
void double_chebpoly_lrintop_1norm(double_t b, const double_chebpoly_lrintop_t K)
{
  long r = K->length;
  long i, j, k, nk, mk;
  mpfi_t lc, rc, bound;
  mpfi_init(lc);
  mpfi_init(rc);
  mpfi_init(bound);
  mpfi_set_si(bound, 0);

  // n = maximum degree in t and m = maximum degree in s
  long n = -1;
  long m = -1;
  for (k = 0 ; k < r ; k++) {
    nk = double_chebpoly_degree(K->lpoly + k);
    mk = double_chebpoly_degree(K->rpoly + k);
    if (nk > n)
      n = nk;
    if (mk > m)
      m = mk;
  }

  // initialize matrix A of dim (n+1)×(m+1)
  mpfi_ptr_ptr A = malloc((n+1) * sizeof(mpfi_ptr));
  for (i = 0 ; i <= n ; i++) {
    A[i] = malloc((m+1) * sizeof(mpfi_t));
    for (j = 0 ; j <= m ; j++) {
      mpfi_init(A[i] + j);
      mpfi_set_si(A[i] + j, 0);
    }
  }

  // fill A with coefficients of the bivariate kernel
  for (k = 0 ; k < r ; k++) {
    nk = double_chebpoly_degree(K->lpoly + k);
    mk = double_chebpoly_degree(K->rpoly + k);
    for (i = 0 ; i <= nk ; i++) {
      mpfi_set_d(lc, K->lpoly[k].coeffs[i]);
      for (j = 0 ; j <= mk ; j++) {
        mpfi_set_d(rc, K->rpoly[k].coeffs[j]);
        mpfi_mul(rc, lc, rc);
        mpfi_add(A[i] + j, A[i] + j, rc);
      }
    }
  }

  // compute 1 norm and delete matrix A
  for (i = 0 ; i <= n ; i++) {
    for (j = 0 ; j <= m ; j++) {
      mpfi_abs(A[i] + j, A[i] + j);
      mpfi_add(bound, bound, A[i] + j);
      mpfi_clear(A[i] + j);
    }
    free(A[i]);
  }
  mpfi_mul_2si(bound, bound, 1);

  free(A);
  double_set_fr(b, &(bound->right), MPFR_RNDU);
  mpfi_clear(bound);
  mpfi_clear(lc);
  mpfi_clear(rc);
}
