
#include "double_chebpoly_lrintop.h"


void double_chebpoly_lrintop_set(double_chebpoly_lrintop_t L, const double_chebpoly_lrintop_t K)
{
  long r = K->length;
  double_chebpoly_lrintop_set_length(L, r);

  long i;
  for (i = 0 ; i < r ; i++) {
    double_chebpoly_set(L->lpoly + i, K->lpoly + i);
    double_chebpoly_set(L->rpoly + i, K->rpoly + i);
  }
}
