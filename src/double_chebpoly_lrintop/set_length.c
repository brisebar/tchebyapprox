
#include "double_chebpoly_lrintop.h"


void double_chebpoly_lrintop_set_length(double_chebpoly_lrintop_t K, long length)
{
  long r = K->length;
  K->length = length;
  long i;

  if (length < r) {
    for (i = length ; i < r ; i++) {
      double_chebpoly_clear(K->lpoly + i);
      double_chebpoly_clear(K->rpoly + i);
    }
    K->lpoly = realloc(K->lpoly, length * sizeof(double_chebpoly_t));
    K->rpoly = realloc(K->rpoly, length * sizeof(double_chebpoly_t));
  }

  else if (length > r) {
    K->lpoly = realloc(K->lpoly, length * sizeof(double_chebpoly_t));
    K->rpoly = realloc(K->rpoly, length * sizeof(double_chebpoly_t));
    for (i = r ; i < length ; i++) {
      double_chebpoly_init(K->lpoly + i);
      double_chebpoly_init(K->rpoly + i);
    }
  }

}
