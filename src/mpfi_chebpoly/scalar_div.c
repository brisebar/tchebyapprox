
#include "mpfi_chebpoly.h"


// set P := (1/c)*Q
void mpfi_chebpoly_scalar_div_si(mpfi_chebpoly_t P, const mpfi_chebpoly_t Q, long c)
{
  long n = Q->degree;
  mpfi_chebpoly_set_degree(P, n);
  _mpfi_chebpoly_scalar_div_si(P->coeffs, Q->coeffs, n, c);
}


// set P := (1/c)*Q
void mpfi_chebpoly_scalar_div_z(mpfi_chebpoly_t P, const mpfi_chebpoly_t Q, const mpz_t c)
{
  long n = Q->degree;
  mpfi_chebpoly_set_degree(P, n);
  _mpfi_chebpoly_scalar_div_z(P->coeffs, Q->coeffs, n, c);
}


// set P := (1/c)*Q
void mpfi_chebpoly_scalar_div_q(mpfi_chebpoly_t P, const mpfi_chebpoly_t Q, const mpq_t c)
{
  long n = Q->degree;
  mpfi_chebpoly_set_degree(P, n);
  _mpfi_chebpoly_scalar_div_q(P->coeffs, Q->coeffs, n, c);
}


// set P := (1/c)*Q
void mpfi_chebpoly_scalar_div_d(mpfi_chebpoly_t P, const mpfi_chebpoly_t Q, const double_t c)
{
  long n = Q->degree;
  mpfi_chebpoly_set_degree(P, n);
  _mpfi_chebpoly_scalar_div_d(P->coeffs, Q->coeffs, n, c);
}


// set P := (1/c)*Q
void mpfi_chebpoly_scalar_div_fr(mpfi_chebpoly_t P, const mpfi_chebpoly_t Q, const mpfr_t c)
{
  long n = Q->degree;
  mpfi_chebpoly_set_degree(P, n);
  _mpfi_chebpoly_scalar_div_fr(P->coeffs, Q->coeffs, n, c);
}


// set P := (1/c)*Q
void mpfi_chebpoly_scalar_div_fi(mpfi_chebpoly_t P, const mpfi_chebpoly_t Q, const mpfi_t c)
{
  long n = Q->degree;
  mpfi_chebpoly_set_degree(P, n);
  _mpfi_chebpoly_scalar_div_fi(P->coeffs, Q->coeffs, n, c);
}


// set P := 2^-k*Q
void mpfi_chebpoly_scalar_div_2si(mpfi_chebpoly_t P, const mpfi_chebpoly_t Q, long k)
{
  long n = Q->degree;
  mpfi_chebpoly_set_degree(P, n);
  _mpfi_chebpoly_scalar_div_2si(P->coeffs, Q->coeffs, n, k);
}
