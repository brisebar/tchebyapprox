
#include "mpfi_chebpoly.h"


// set P in canonical form (exact degree)
void mpfi_chebpoly_normalise(mpfi_chebpoly_t P)
{
  long n, i; 
  for (n = P->degree ; n >= 0 ; n--) {
    if (!mpfi_is_zero(P->coeffs + n))
      break;
  }
  if (n < P->degree) {
    for (i = n+1 ; i <= P->degree ; i++)
      mpfi_clear(P->coeffs + i);
    P->degree = n;
    P->coeffs = realloc(P->coeffs, (n+1) * sizeof(mpfi_t));
  }
}
