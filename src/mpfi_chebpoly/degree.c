
#include "mpfi_chebpoly.h"


// get the degree of P
long mpfi_chebpoly_degree(const mpfi_chebpoly_t P)
{
  return P->degree;
}
