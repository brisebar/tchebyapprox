
#include "mpfi_chebpoly.h"


// set n as degree for P
void mpfi_chebpoly_set_degree(mpfi_chebpoly_t P, long n)
{
  if (n < -1)
    n = -1;
  long m = P->degree;
  long i;

  if (n < m) {
    for (i = n+1 ; i <= m ; i++)
      mpfi_clear(P->coeffs + i);
    P->coeffs = realloc(P->coeffs, (n+1) * sizeof(mpfi_t));
  }
  else if (n > m) {
    P->coeffs = realloc(P->coeffs, (n+1) * sizeof(mpfi_t));
    for (i = m+1 ; i <= n ; i++) {
      mpfi_init(P->coeffs + i);
      mpfi_set_si(P->coeffs + i, 0);
    }
  }

  P->degree = n;
}
