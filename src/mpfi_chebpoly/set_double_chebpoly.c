
#include "mpfi_chebpoly.h"


// copy Q into P
void mpfi_chebpoly_set_double_chebpoly(mpfi_chebpoly_t P, const double_chebpoly_t Q)
{
  mpfi_chebpoly_set_degree(P, Q->degree);
  _mpfi_chebpoly_set_double_chebpoly(P->coeffs, Q->coeffs, Q->degree);
}
