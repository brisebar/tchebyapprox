
#include "mpfi_chebpoly.h"


// copy Q into P
void mpfi_chebpoly_set(mpfi_chebpoly_t P, const mpfi_chebpoly_t Q)
{
  long n = Q->degree;
  long i;
  if (P != Q) {
    mpfi_chebpoly_set_degree(P, n);
    for (i = 0 ; i <= n ; i++)
      mpfi_set(P->coeffs + i, Q->coeffs + i);
  }
}
