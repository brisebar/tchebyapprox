
#include "mpfi_chebpoly.h"


// set P to (1/c)*Q where c is the leading coefficient of P
void mpfi_chebpoly_monic(mpfi_chebpoly_t P, const mpfi_chebpoly_t Q)
{
  long n = Q->degree;

  if (n < 0)
    mpfi_chebpoly_zero(P);

  else {
    mpfi_chebpoly_set_degree(P, n);
    _mpfi_chebpoly_monic(P->coeffs, Q->coeffs, n);
  }
}
