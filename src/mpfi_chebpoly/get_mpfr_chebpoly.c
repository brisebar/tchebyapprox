
#include "mpfi_chebpoly.h"


// coefficients of P are set to the middle of those of Q
void mpfi_chebpoly_get_mpfr_chebpoly(mpfr_chebpoly_t P, const mpfi_chebpoly_t Q)
{
  long n = Q->degree;

  if (n < 0)
    mpfr_chebpoly_zero(P);

  else {
    mpfr_chebpoly_set_degree(P, n);
    _mpfi_chebpoly_get_mpfr_chebpoly(P->coeffs, Q->coeffs, n);
  }
}
