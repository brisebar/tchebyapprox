
#include "mpfi_chebpoly.h"


// swap P and Q efficiently
void mpfi_chebpoly_swap(mpfi_chebpoly_t P, mpfi_chebpoly_t Q)
{
  long degree_buf = P->degree;
  mpfi_ptr coeffs_buf = P->coeffs;
  P->degree = Q->degree;
  P->coeffs = Q->coeffs;
  Q->degree = degree_buf;
  Q->coeffs = coeffs_buf;
}
