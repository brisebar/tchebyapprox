
#include "mpfi_chebpoly.h"


int mpfi_chebpoly_equal(const mpfi_chebpoly_t P, const mpfi_chebpoly_t Q)
{
  return (P->degree == Q->degree && _mpfi_chebpoly_equal(P->coeffs, Q->coeffs, P->degree));
}
