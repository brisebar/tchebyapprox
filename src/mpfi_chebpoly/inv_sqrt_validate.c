
#include "mpfi_chebpoly.h"


// compute an upper bound of ||F-1/sqrt(G)||_1
// one must provide a witness F0 st ||1-F0*G*F||_1 < 1
void mpfr_chebpoly_inv_sqrt_validate(mpfr_t bound, const mpfr_chebpoly_t F, const mpfr_chebpoly_t F0, const mpfr_chebpoly_t G)
{
  mpfi_t F0Gnorm, F0quality, TF_Fnorm, delta, r_min, Tnorm, temp;
  mpfi_init(F0Gnorm);
  mpfi_init(F0quality);
  mpfi_init(TF_Fnorm);
  mpfi_init(delta);
  mpfi_init(r_min);
  mpfi_init(Tnorm);
  mpfi_init(temp);
  mpfi_chebpoly_t Ffi, F0fi, Gfi, Temp;
  mpfi_chebpoly_init(Ffi);
  mpfi_chebpoly_set_mpfr_chebpoly(Ffi, F);
  mpfi_chebpoly_init(F0fi);
  mpfi_chebpoly_set_mpfr_chebpoly(F0fi, F0);
  mpfi_chebpoly_init(Gfi);
  mpfi_chebpoly_set_mpfr_chebpoly(Gfi, G);
  mpfi_chebpoly_init(Temp);

  // check that ||1-F0*G*F||_1 < 1
  mpfi_chebpoly_mul(Temp, F0fi, Gfi);
  mpfi_chebpoly_mul(Temp, Temp, Ffi);
  mpfi_sub_si(Temp->coeffs + 0, Temp->coeffs + 0, 1);
  mpfi_chebpoly_1norm(F0quality, Temp);
  if (mpfr_cmp_si(&(F0quality->right), 1) >= 0)
    fprintf(stderr, "mpfr_chebpoly_inv_sqrt_validate: error: ||1-F0*G*F||_1 >= 1\n");

  // check that the discriminant is non negative
  else {

    // compute ||F0*G||_1 and ||TF-F||_1 = ||F0/2*(G*F^2-1)||_1
    mpfi_chebpoly_mul(Temp, F0fi, Gfi);
    mpfi_chebpoly_1norm(F0Gnorm, Temp);
    mpfi_chebpoly_mul(Temp, Temp, Ffi);
    mpfi_chebpoly_mul(Temp, Temp, Ffi);
    mpfi_chebpoly_sub(Temp, Temp, F0fi);
    mpfi_chebpoly_1norm(TF_Fnorm, Temp);
    mpfi_mul_2si(TF_Fnorm, TF_Fnorm, -1);

    // compute the discriminant delta
    mpfi_si_sub(delta, 1, F0quality);
    mpfi_sqr(delta, delta);
    mpfi_mul(temp, F0Gnorm, TF_Fnorm);
    mpfi_mul_2si(temp, temp, 2);
    mpfi_sub(delta, delta, temp);

    // check that delta is non negative
    if (mpfr_cmp_si(&(delta->left), 0) < 0)
      fprintf(stderr, "mpfr_chebpoly_inv_sqrt_validate: error: discriminant is negative\n");

    else {

      // compute r_min
      mpfi_sqrt(r_min, delta);
      mpfi_add(r_min, r_min, F0quality);
      mpfi_si_sub(r_min, 1, r_min);
      mpfi_div(r_min, r_min, F0Gnorm);
      mpfi_mul_2si(r_min, r_min, -1);

      // compute Tnorm
      mpfi_mul(temp, F0Gnorm, r_min);
      mpfi_add(Tnorm, F0quality, temp);

      // check that Tnorm < 1 (should be always OK at this point)
      if (mpfr_cmp_si(&(Tnorm->right), 1) >= 0)
	fprintf(stderr, "mpfr_chebpoly_inv_sqrt_validate: error: Tnorm > 1\n");

      // compute the desired bound
      else {
	mpfi_si_sub(temp, 1, Tnorm);
	mpfi_div(temp, TF_Fnorm, temp);
	mpfr_set(bound, &(temp->right), MPFR_RNDU);
      }

    }

  }

  // clear variables
  mpfi_clear(F0Gnorm);
  mpfi_clear(F0quality);
  mpfi_clear(TF_Fnorm);
  mpfi_clear(delta);
  mpfi_clear(r_min);
  mpfi_clear(Tnorm);
  mpfi_clear(temp);
  mpfi_chebpoly_clear(Ffi);
  mpfi_chebpoly_clear(F0fi);
  mpfi_chebpoly_clear(Gfi);
  mpfi_chebpoly_clear(Temp);

}


// same as mpfr_chebpoly_inv_sqrt_validate but uses F0 = F
void mpfr_chebpoly_inv_sqrt_validate2(mpfr_t bound, const mpfr_chebpoly_t F, const mpfr_chebpoly_t G)
{
  mpfr_chebpoly_inv_sqrt_validate(bound, F, F, G);
}


