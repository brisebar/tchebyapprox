
#include "mpfi_chebpoly.h"


// set P to 0
void mpfi_chebpoly_zero(mpfi_chebpoly_t P)
{
  long n = P->degree;
  long i ;
  for (i = 0 ; i <= n ; i++)
    mpfi_clear(P->coeffs + i);
  P->coeffs = realloc(P->coeffs, 0);
  P->degree = -1;
}
