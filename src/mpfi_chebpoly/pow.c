
#include "mpfi_chebpoly.h"


// set P := Q^e
void mpfi_chebpoly_pow(mpfi_chebpoly_t P, const mpfi_chebpoly_t Q, unsigned long e)
{
  mpfi_chebpoly_t Qe;
  mpfi_chebpoly_init(Qe);
  mpfi_chebpoly_set_coeff_si(Qe, 0, 1);
  mpfi_chebpoly_t Qi;
  mpfi_chebpoly_init(Qi);
  mpfi_chebpoly_set(Qi, Q);

  while (e != 0) {
    if (e%2 == 1)
      mpfi_chebpoly_mul(Qe, Qe, Qi);
    mpfi_chebpoly_mul(Qi, Qi, Qi);
    e /= 2;
  }

  mpfi_chebpoly_clear(Qi);
  mpfi_chebpoly_clear(P);
  P->degree = Qe->degree;
  P->coeffs = Qe->coeffs;
}
