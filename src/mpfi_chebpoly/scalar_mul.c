
#include "mpfi_chebpoly.h"


// set P := c*Q
void mpfi_chebpoly_scalar_mul_si(mpfi_chebpoly_t P, const mpfi_chebpoly_t Q, long c)
{
  if (c == 0)
    mpfi_chebpoly_zero(P);
  else {
    long n = Q->degree;
    mpfi_chebpoly_set_degree(P, n);
    _mpfi_chebpoly_scalar_mul_si(P->coeffs, Q->coeffs, n, c);
  }
}


// set P := c*Q
void mpfi_chebpoly_scalar_mul_z(mpfi_chebpoly_t P, const mpfi_chebpoly_t Q, const mpz_t c)
{
  if (!mpz_sgn(c))
    mpfi_chebpoly_zero(P);
  else {
    long n = Q->degree;
    mpfi_chebpoly_set_degree(P, n);
    _mpfi_chebpoly_scalar_mul_z(P->coeffs, Q->coeffs, n, c);
  }
}


// set P := c*Q
void mpfi_chebpoly_scalar_mul_q(mpfi_chebpoly_t P, const mpfi_chebpoly_t Q, const mpq_t c)
{
  if (!mpq_sgn(c))
    mpfi_chebpoly_zero(P);
  else {
    long n = Q->degree;
    mpfi_chebpoly_set_degree(P, n);
    _mpfi_chebpoly_scalar_mul_q(P->coeffs, Q->coeffs, n, c);
  }
}


// set P := c*Q
void mpfi_chebpoly_scalar_mul_d(mpfi_chebpoly_t P, const mpfi_chebpoly_t Q, const double_t c)
{
  if (*c == 0)
    mpfi_chebpoly_zero(P);
  else {
    long n = Q->degree;
    mpfi_chebpoly_set_degree(P, n);
    _mpfi_chebpoly_scalar_mul_d(P->coeffs, Q->coeffs, n, c);
  }
}


// set P := c*Q
void mpfi_chebpoly_scalar_mul_fr(mpfi_chebpoly_t P, const mpfi_chebpoly_t Q, const mpfr_t c)
{
  if (mpfr_zero_p(c))
    mpfi_chebpoly_zero(P);
  else {
    long n = Q->degree;
    mpfi_chebpoly_set_degree(P, n);
    _mpfi_chebpoly_scalar_mul_fr(P->coeffs, Q->coeffs, n, c);
  }
}


// set P := c*Q
void mpfi_chebpoly_scalar_mul_fi(mpfi_chebpoly_t P, const mpfi_chebpoly_t Q, const mpfi_t c)
{
  if (mpfi_is_zero(c))
    mpfi_chebpoly_zero(P);
  else {
    long n = Q->degree;
    mpfi_chebpoly_set_degree(P, n);
    _mpfi_chebpoly_scalar_mul_fi(P->coeffs, Q->coeffs, n, c);
  }
}


// set P := 2^k*Q
void mpfi_chebpoly_scalar_mul_2si(mpfi_chebpoly_t P, const mpfi_chebpoly_t Q, long k)
{
  long n = Q->degree;

  if (n < 0)
    mpfi_chebpoly_zero(P);

  else {
    mpfi_chebpoly_set_degree(P, n);
    _mpfi_chebpoly_scalar_mul_2si(P->coeffs, Q->coeffs, n, k);
  }
}
