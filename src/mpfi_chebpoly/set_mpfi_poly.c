
#include "mpfi_chebpoly.h"


void _mpfi_chebpoly_set_mpfi_poly(mpfi_ptr P, mpfi_srcptr Q, long n)
{
  long i, j;
  mpfi_t prev, temp;
  mpfi_init(prev);
  mpfi_init(temp);

  mpfi_ptr R = malloc((n+1) * sizeof(mpfi_t));
  for (i = 0 ; i <= n ; i++) {
    mpfi_init(R + i);
    mpfi_set_si(R + i, 0);
  }
  
  mpfi_set(R, Q + n); // R = Q_n

  for (i = 1 ; i <= n ; i++) {

    // multiplication by X
    for (j = 1 ; j < i ; j++)
      mpfi_mul_2si(R + j, R + j, -1);
    mpfi_set(prev, R);
    mpfi_set_si(R, 0);
    for (j = 1 ; j < i ; j++) {
      mpfi_add(R + j - 1, R + j - 1, R + j);
      mpfi_set(temp, R + j);
      mpfi_set(R + j, prev);
      mpfi_set(prev, temp);
    }
    mpfi_set(R + i, prev);

    // add Q[n-i]
    mpfi_add(R, R, Q + n - i);

  }

  for (i = 0 ; i <= n ; i++) {
    mpfi_swap(P + i, R + i);
    mpfi_clear(R + i);
  }
  free(R);
  mpfi_clear(prev);
  mpfi_clear(temp);
}


// set in P the conversion of Q (in the monomial basis) into the Chebyshev basis
void mpfi_chebpoly_set_mpfi_poly(mpfi_chebpoly_t P, const mpfi_poly_t Q)
{
  long n = Q->degree;

  if (n < 0)
    mpfi_chebpoly_zero(P);

  else {
    mpfi_chebpoly_set_degree(P, n);
    _mpfi_chebpoly_set_mpfi_poly(P->coeffs, Q->coeffs, n);
  }
}
