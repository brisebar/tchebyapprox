
#include "mpfi_chebpoly.h"


void _mpfi_chebpoly_derivative(mpfi_ptr P, mpfi_srcptr Q, long n)
{
  long i;

  mpfi_ptr R = malloc(n * sizeof(mpfi_t));
  for (i = 0 ; i < n ; i++) {
    mpfi_init(R + i);
    mpfi_set_si(R + i, 0);
  }
  
  for (i = n-1 ; i >= 0 ; i--) {
    mpfi_mul_si(R + i, Q + i + 1, 2*(i+1));
    if (i < n-2)
      mpfi_add(R + i, R + i, R + i + 2);
  }
  
  mpfi_mul_2si(R, R, -1);

  for (i = 0 ; i < n ; i++) {
    mpfi_swap(P + i, R + i);
    mpfi_clear(R + i);
  }
  
  free(R);
}


// set P := Q'
void mpfi_chebpoly_derivative(mpfi_chebpoly_t P, const mpfi_chebpoly_t Q)
{
  long n = Q->degree;

  if (n <= 0)
    mpfi_chebpoly_zero(P);

  else {
    mpfi_chebpoly_set_degree(P, n);
    _mpfi_chebpoly_derivative(P->coeffs, Q->coeffs, n);
    mpfi_clear(P->coeffs + n);
    P->degree --;
  }
}
