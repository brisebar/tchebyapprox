
#include "mpfi_chebpoly.h"


// compute an upper bound of ||F - P/Q||_1
// one must provide a witness G st ||1-GQ||_1 < 1
void mpfr_chebpoly_fract_validate(mpfr_t bound, const mpfr_chebpoly_t F, const mpfr_chebpoly_t P, const mpfr_chebpoly_t Q, const mpfr_chebpoly_t G)
{
  mpfi_chebpoly_t C1, C2, C3, C4;
  mpfi_chebpoly_init(C1);
  mpfi_chebpoly_set_mpfr_chebpoly(C1, F); // C1 = F
  mpfi_chebpoly_init(C2);
  mpfi_chebpoly_set_mpfr_chebpoly(C2, Q); // C2 = Q
  mpfi_chebpoly_init(C3);
  mpfi_chebpoly_set_mpfr_chebpoly(C3, G); // C3 = G
  mpfi_chebpoly_init(C4);
  mpfi_chebpoly_set_mpfr_chebpoly(C4, P); // C4 = P

  mpfi_chebpoly_mul(C1, C1, C2);
  mpfi_chebpoly_sub(C1, C4, C1);
  mpfi_chebpoly_mul(C1, C1, C3);
  mpfi_chebpoly_1norm_ubound(bound, C1);

  mpfr_t norm;
  mpfr_init(norm);
  mpfi_chebpoly_mul(C1, C2, C3);
  if (C1->degree < 0)
    fprintf(stderr, "mpfr_chebpoly_fract_validate: error: ||1-GQ||_1 is not less than 1\n");
  else {
    mpfi_sub_si(C1->coeffs + 0, C1->coeffs + 0, 1);
    mpfi_chebpoly_1norm_ubound(norm, C1);
    mpfr_si_sub(norm, 1, norm, MPFR_RNDD);
    // final bound
    mpfr_div(bound, bound, norm, MPFR_RNDU);
  }

  // clear variables
  mpfi_chebpoly_clear(C1);
  mpfi_chebpoly_clear(C2);
  mpfi_chebpoly_clear(C3);
  mpfi_chebpoly_clear(C4);
  mpfr_clear(norm);

}

