
#include "mpfi_chebpoly.h"


// init with P = 0
void mpfi_chebpoly_init(mpfi_chebpoly_t P)
{
  P->degree = -1;
  P->coeffs = malloc(0);
}
