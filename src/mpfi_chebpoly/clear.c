#include "mpfi_chebpoly.h"


// clear P
void mpfi_chebpoly_clear(mpfi_chebpoly_t P)
{
  long n = P->degree;
  long i;
  for (i = 0 ; i <= n ; i++)
    mpfi_clear(P->coeffs + i);
  free(P->coeffs);
}
