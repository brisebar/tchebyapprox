
#include "mpfi_chebpoly.h"


void _mpfi_chebpoly_evaluate_si(mpfi_t y, mpfi_srcptr P, long n, long x)
{
  mpfi_t t;
  mpfi_init(t);
  mpfi_set_si(t, x);
  _mpfi_chebpoly_evaluate_fi(y, P, n, t);
  mpfi_clear(t);
}


void _mpfi_chebpoly_evaluate_z(mpfi_t y, mpfi_srcptr P, long n, const mpz_t x)
{
  mpfi_t t;
  mpfi_init(t);
  mpfi_set_z(t, x);
  _mpfi_chebpoly_evaluate_fi(y, P, n, t);
  mpfi_clear(t);
}


void _mpfi_chebpoly_evaluate_q(mpfi_t y, mpfi_srcptr P, long n, const mpq_t x)
{
  mpfi_t t;
  mpfi_init(t);
  mpfi_set_q(t, x);
  _mpfi_chebpoly_evaluate_fi(y, P, n, t);
  mpfi_clear(t);
}


void _mpfi_chebpoly_evaluate_d(mpfi_t y, mpfi_srcptr P, long n, const double_t x)
{
  mpfi_t t;
  mpfi_init(t);
  mpfi_set_d(t, *x);
  _mpfi_chebpoly_evaluate_fi(y, P, n, t);
  mpfi_clear(t);
}


void _mpfi_chebpoly_evaluate_fr(mpfi_t y, mpfi_srcptr P, long n, const mpfr_t x)
{
  mpfi_t t;
  mpfi_init(t);
  mpfi_set_fr(t, x);
  _mpfi_chebpoly_evaluate_fi(y, P, n, t);
  mpfi_clear(t);
}


void _mpfi_chebpoly_evaluate_fi(mpfi_t y, mpfi_srcptr P, long n, const mpfi_t x)
{
  if (n < 0)
    mpfi_set_si(y, 0);

  else if (n == 0)
    mpfi_set(y, P);

  else {

    mpfi_t c1, c2, temp;
    mpfi_init(temp);
    mpfi_init(c1);
    mpfi_set(c1, P + n - 1);
    mpfi_init(c2);
    mpfi_set(c2, P + n);
    long i;

    for (i = n-2 ; i >= 0 ; i--) {
      // (c1,c2) -> (P[i]-c2, c1+2*x*c2)
      // new value of c1
      mpfi_swap(temp, c1);
      mpfi_sub(c1, P + i, c2);
      // new value of c2
      mpfi_mul(c2, c2, x);
      mpfi_mul_2si(c2, c2, 1);
      mpfi_add(c2, c2, temp);
    }

    // y = P(x) = c1+x*c2
    mpfi_mul(y, x, c2);
    mpfi_add(y, y, c1);
    // clear variables
    mpfi_clear(temp);
    mpfi_clear(c1);
    mpfi_clear(c2);
  }

}



// set y to P(x)
void mpfi_chebpoly_evaluate_si(mpfi_t y, const mpfi_chebpoly_t P, long x)
{
  _mpfi_chebpoly_evaluate_si(y, P->coeffs, P->degree, x);
}


// set y to P(x)
void mpfi_chebpoly_evaluate_z(mpfi_t y, const mpfi_chebpoly_t P, const mpz_t x)
{
  _mpfi_chebpoly_evaluate_z(y, P->coeffs, P->degree, x);
}


// set y to P(x)
void mpfi_chebpoly_evaluate_q(mpfi_t y, const mpfi_chebpoly_t P, const mpq_t x)
{
  _mpfi_chebpoly_evaluate_q(y, P->coeffs, P->degree, x);
}


// set y to P(x)
void mpfi_chebpoly_evaluate_d(mpfi_t y, const mpfi_chebpoly_t P, const double_t x)
{
  _mpfi_chebpoly_evaluate_d(y, P->coeffs, P->degree, x);
}


// set y to P(x)
void mpfi_chebpoly_evaluate_fr(mpfi_t y, const mpfi_chebpoly_t P, const mpfr_t x)
{
  _mpfi_chebpoly_evaluate_fr(y, P->coeffs, P->degree, x);
}


// set y to P(x)
void mpfi_chebpoly_evaluate_fi(mpfi_t y, const mpfi_chebpoly_t P, const mpfi_t x)
{
  _mpfi_chebpoly_evaluate_fi(y, P->coeffs, P->degree, x);
}


// set y to P(-1)
void mpfi_chebpoly_evaluate_1(mpfi_t y, const mpfi_chebpoly_t P)
{
  long n = P->degree;
  long i;
  mpfi_set_si(y, 0);
  for (i = 0 ; i <= n ; i++)
    if (i % 2)
      mpfi_sub(y, y, P->coeffs + i);
    else
      mpfi_add(y, y, P->coeffs + i);
}

// set y to P(0)
void mpfi_chebpoly_evaluate0(mpfi_t y, const mpfi_chebpoly_t P)
{
  long n = P->degree;
  long i;
  mpfi_set_si(y, 0);
  for (i = 0 ; i <= n ; i+=2)
    if (i % 4)
      mpfi_sub(y, y, P->coeffs + i);
    else
      mpfi_add(y, y, P->coeffs + i);
}

// set y to P(1)
void mpfi_chebpoly_evaluate1(mpfi_t y, const mpfi_chebpoly_t P)
{
  long n = P->degree;
  long i;
  mpfi_set_si(y, 0);
  for (i = 0 ; i <= n ; i++)
    mpfi_add(y, y, P->coeffs + i);
}
