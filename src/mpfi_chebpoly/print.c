
#include "mpfi_chebpoly.h"


void _mpfi_chebpoly_print(mpfi_srcptr P, long n, const char * var, size_t digits)
{
  long i;
  int flag = 0;

  for (i = n ; i >=0 ; i--)
    if (!mpfi_is_zero(P + i)) {
      if (flag)
	  printf("+");
      else
	flag = 1;
      mpfi_out_str(stdout, 10, digits, P + i);
      printf("*%s_%ld", var, i);
    }

}


// display the polynomial P with 'var' as indeterminate symbol
// 'var' is a 0-terminated string
void mpfi_chebpoly_print(const mpfi_chebpoly_t P, const char * var, size_t digits)
{
  long n = P->degree;

  if (n < 0)
    printf("0");

  else
    _mpfi_chebpoly_print(P->coeffs, n, var, digits);

}
