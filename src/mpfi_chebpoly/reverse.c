
#include "mpfi_chebpoly.h"


// set P to Q(X^-1)*X^(degree(Q))
void mpfi_chebpoly_reverse(mpfi_chebpoly_t P, const mpfi_chebpoly_t Q)
{
  if (P != Q)
    mpfi_chebpoly_set(P, Q);
  _mpfi_chebpoly_reverse(P->coeffs, P->coeffs, P->degree);
  mpfi_chebpoly_normalise(P);
}
