
#include "mpfi_chebpoly.h"


void _mpfi_chebpoly_get_mpfi_poly(mpfi_ptr P, mpfi_srcptr Q, long n)
{
  mpfi_set(P, Q); // P = Q_0

  long i;
  mpfi_ptr Ti = malloc((n+2) * sizeof(mpfi_t));
  mpfi_ptr Tip1 = malloc((n+2) * sizeof(mpfi_t));
  mpfi_ptr Temp = malloc((n+2) * sizeof(mpfi_t));
  for (i = 0 ; i <= n+1 ; i++) {
    mpfi_init(Ti + i);
    mpfi_set_si(Ti + i, 0);
    mpfi_init(Tip1 + i);
    mpfi_set_si(Tip1 + i, 0);
    mpfi_init(Temp + i);
    mpfi_set_si(Temp + i, 0);
  }
  mpfi_set_si(Ti, 1); // T0 = 1
  mpfi_set_si(Tip1 + 1, 1); // T1 = X
  
  for (i = 0 ; i < n ; i++) {

    // P += Q_{i+1} * T_{i+1}
    _mpfi_poly_scalar_mul_fi(Temp, Tip1, i+1, Q + i + 1);
    mpfi_set_si(P + i + 1, 0);
    _mpfi_poly_add(P, P, i+1, Temp, i+1);
      
    // (Ti, Tip1) -> (Tip1, 2*X*Tip1-Ti)
    _mpfi_poly_scalar_mul_2si(Temp, Tip1, i+1, 1);
    _mpfi_poly_shift_left(Temp, Temp, i+1, 1);
    _mpfi_poly_sub(Temp, Temp, i+2, Ti, i);
    _mpfi_poly_set(Ti, Tip1, i+1);
    _mpfi_poly_set(Tip1, Temp, i+2);

  }

  // clear variables
  for (i = 0 ; i <= n+1 ; i++) {
    mpfi_clear(Ti + i);
    mpfi_clear(Tip1 + i);
    mpfi_clear(Temp + i);
  }
}


// set in P the conversion of Q (in the Chebyshev basis) into the monomial basis
void mpfi_chebpoly_get_mpfi_poly(mpfi_poly_t P, const mpfi_chebpoly_t Q)
{
  long n = Q->degree;

  if (n < 0)
    mpfi_poly_zero(P);

  else {
    mpfi_poly_set_degree(P, n);
    _mpfi_chebpoly_get_mpfi_poly(P->coeffs, Q->coeffs, n);
  }
}
