
#include "mpfi_chebpoly.h"


void mpfi_chebpoly_comp(mpfi_chebpoly_t P, const mpfi_chebpoly_t Q, const mpfi_chebpoly_t R)
{
  long n = Q->degree;

  if (n <= 0)
    mpfi_chebpoly_set(P, Q);

  else {
    
    mpfi_chebpoly_t A, B, Temp;
    mpfi_chebpoly_init(A);
    mpfi_chebpoly_init(B);
    mpfi_chebpoly_init(Temp);
    mpfi_chebpoly_set_degree(A, 0);
    mpfi_set(A->coeffs + 0, Q->coeffs + n - 1);
    mpfi_chebpoly_set_degree(B, 0);
    mpfi_set(B->coeffs + 0, Q->coeffs + n);
    long i;
    
    for (i = n-2 ; i >= 0 ; i--) {
      // (A,B) -> (P[i]-B, A+2*R*B)
      // new value for A
      mpfi_chebpoly_swap(Temp, A);
      if (B->degree < 0) {
	mpfi_chebpoly_set_degree(A, 0);
	mpfi_set(A->coeffs + 0, Q->coeffs + i);
      }
      else {
	mpfi_chebpoly_neg(A, B);
	mpfi_add(A->coeffs + 0, A->coeffs + 0, Q->coeffs + i);
      }
      // new value for B
      mpfi_chebpoly_mul(B, B, R);
      mpfi_chebpoly_scalar_mul_2si(B, B, 1);
      mpfi_chebpoly_add(B, B, Temp);
    }
    
    // P = A+R*B
    mpfi_chebpoly_mul(P, R, B);
    mpfi_chebpoly_add(P, P, A);
    
    // clear variables
    mpfi_chebpoly_clear(Temp);
    mpfi_chebpoly_clear(A);
    mpfi_chebpoly_clear(B);
  } 

}


