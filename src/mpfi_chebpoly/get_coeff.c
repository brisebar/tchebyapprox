
#include "mpfi_chebpoly.h"


// get the n-th coefficient of P
void mpfi_chebpoly_get_coeff(mpfi_t c, const mpfi_chebpoly_t P, long n)
{
  if (n < 0 || n > P->degree)
    mpfi_set_si(c, 0);
  else
    mpfi_set(c, P->coeffs + n);
}
