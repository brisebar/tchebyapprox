
#include "mpfi_chebpoly.h"


void mpfi_chebpoly_1norm(mpfi_t y, const mpfi_chebpoly_t P)
{
  _mpfi_chebpoly_1norm(y, P->coeffs, P->degree);
}


void mpfi_chebpoly_1norm_ubound(mpfr_t y, const mpfi_chebpoly_t P)
{
  _mpfi_chebpoly_1norm_ubound(y, P->coeffs, P->degree);
}


void mpfi_chebpoly_1norm_lbound(mpfr_t y, const mpfi_chebpoly_t P)
{
  _mpfi_chebpoly_1norm_lbound(y, P->coeffs, P->degree);
}
