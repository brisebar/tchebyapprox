
#include "mpfi_chebpoly.h"


// set P to Q*X^k
void mpfi_chebpoly_shift_left(mpfi_chebpoly_t P, const mpfi_chebpoly_t Q, unsigned long k)
{
  long i;
  long n = Q->degree;
  mpfi_ptr Pcoeffs = malloc((n+k+1) * sizeof(mpfi_t));
  for (i = 0 ; i <= n+k ; i++)
    mpfi_init(Pcoeffs + i);
  _mpfi_chebpoly_shift_left(Pcoeffs, Q->coeffs, n, k);
  mpfi_chebpoly_clear(P);
  P->degree = n+k;
  P->coeffs = Pcoeffs;
}


// set P to Q/X^k
void mpfi_chebpoly_shift_right(mpfi_chebpoly_t P, const mpfi_chebpoly_t Q, unsigned long k)
{
  long i;
  long n = Q->degree;
  if (k > n)
    mpfi_chebpoly_zero(P);
  else {
    mpfi_ptr Pcoeffs = malloc((n-k+1) * sizeof(mpfi_t));
    for (i = 0 ; i <= n-k ; i++)
      mpfi_init(Pcoeffs + i);
    _mpfi_chebpoly_shift_right(Pcoeffs, Q->coeffs, n, k);
    mpfi_chebpoly_clear(P);
    P->degree = n-k;
    P->coeffs = Pcoeffs;
  }
}
