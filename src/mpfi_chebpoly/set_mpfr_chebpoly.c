
#include "mpfi_chebpoly.h"


// copy Q into P
void mpfi_chebpoly_set_mpfr_chebpoly(mpfi_chebpoly_t P, const mpfr_chebpoly_t Q)
{
  mpfi_chebpoly_set_degree(P, Q->degree);
  _mpfi_chebpoly_set_mpfr_chebpoly(P->coeffs, Q->coeffs, Q->degree);
}
