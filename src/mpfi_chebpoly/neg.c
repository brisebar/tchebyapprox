
#include "mpfi_chebpoly.h"


// set P := -Q
void mpfi_chebpoly_neg(mpfi_chebpoly_t P, const mpfi_chebpoly_t Q)
{
  mpfi_chebpoly_set_degree(P, Q->degree);
  _mpfi_chebpoly_neg(P->coeffs, Q->coeffs, Q->degree);
}
