
#include "mpfi_chebpoly.h"


// set P := Q + R
void mpfi_chebpoly_add(mpfi_chebpoly_t P, const mpfi_chebpoly_t Q, const mpfi_chebpoly_t R)
{
  long n = Q->degree;
  long m = R->degree;
  long max_n_m = n < m ? m : n;

  mpfi_chebpoly_set_degree(P, max_n_m);
  _mpfi_chebpoly_add(P->coeffs, Q->coeffs, n, R->coeffs, m);
  mpfi_chebpoly_normalise(P);
}
