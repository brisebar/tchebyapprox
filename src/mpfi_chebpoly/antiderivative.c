
#include "mpfi_chebpoly.h"


void _mpfi_chebpoly_antiderivative(mpfi_ptr P, mpfi_srcptr Q, long n)
{
  long i;
  
  mpfi_ptr R = malloc((n+2) * sizeof(mpfi_t));
  for (i = 0 ; i <= n+1 ; i++) {
    mpfi_init(R + i);
    mpfi_set_si(R + i, 0);
  }

  // compute R[1]
  mpfi_mul_2si(R + 1, Q, 1);
  if (n >= 2)
    mpfi_sub(R + 1, R + 1, Q + 2);
  mpfi_mul_2si(R + 1, R + 1, -1);

  // compute R[i] for i in [2,n-1]
  for (i = 2 ; i < n ; i++) {
    mpfi_sub(R + i, Q + i - 1, Q + i + 1);
    mpfi_div_si(R + i, R + i, 2*i);
  }

  // compute R[n] (if n >= 2)
  if (n >= 2)
    mpfi_div_si(R + n, Q + n - 1, 2*n);

  // compute R[n+1] (if n >= 1)
  if (n >= 1)
    mpfi_div_si(R + n + 1, Q + n, 2*(n+1));

  for (i = 0 ; i <= n+1 ; i++) {
    mpfi_swap(P + i, R + i);
    mpfi_clear(R + i);
  }
  free(R);
}


// set P st P' = Q and 0-th coeff of P = 0 
void mpfi_chebpoly_antiderivative(mpfi_chebpoly_t P, const mpfi_chebpoly_t Q)
{
  long n = Q->degree;

  if (n < 0)
    mpfi_chebpoly_zero(P);

  else {
    mpfi_chebpoly_set_degree(P, n+1);
    _mpfi_chebpoly_antiderivative(P->coeffs, Q->coeffs, n);
  }
}


// set P st P' = Q and P(0) = 0
void mpfi_chebpoly_antiderivative0(mpfi_chebpoly_t P, const mpfi_chebpoly_t Q)
{
  mpfi_chebpoly_antiderivative(P, Q);
  long n = P->degree;
  long i;
  for (i = 1 ; 2*i <= n ; i++)
    if (i%2)
      mpfi_add(P->coeffs + 0, P->coeffs + 0, P->coeffs + 2*i);
    else
      mpfi_sub(P->coeffs + 0, P->coeffs + 0, P->coeffs + 2*i);
}


// set P st P' = Q and P(-1) = 0
void mpfi_chebpoly_antiderivative_1(mpfi_chebpoly_t P, const mpfi_chebpoly_t Q)
{
  mpfi_chebpoly_antiderivative(P, Q);
  long n = P->degree;
  long i;
  for (i = 1 ; i <= n ; i++)
    if (i%2)
      mpfi_add(P->coeffs + 0, P->coeffs + 0, P->coeffs + i);
    else
      mpfi_sub(P->coeffs + 0, P->coeffs + 0, P->coeffs + i);
}


// set P st P' = Q and P(1) = 0
void mpfi_chebpoly_antiderivative1(mpfi_chebpoly_t P, const mpfi_chebpoly_t Q)
{
  mpfi_chebpoly_antiderivative(P, Q);
  long n = P->degree;
  long i;
  for (i = 1 ; i <= n ; i++)
    mpfi_sub(P->coeffs + 0, P->coeffs + 0 , P->coeffs + i);
}
