
#include "double_chebpoly_vec.h"


void double_chebpoly_vec_pow(double_chebpoly_vec_t P, const double_chebpoly_vec_t Q, unsigned long e)
{
  long n = Q->dim;
  long i;

  double_chebpoly_vec_set_dim(P, n);
  for (i = 0 ; i < n ; i++)
    double_chebpoly_pow(P->poly + i, Q->poly + i, e);

}

