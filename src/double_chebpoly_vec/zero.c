
#include "double_chebpoly_vec.h"


void double_chebpoly_vec_zero(double_chebpoly_vec_t P)
{
  long n = P->dim;
  long i;
  for (i = 0 ; i < n ; i++)
    double_chebpoly_zero(P->poly + i);
}
