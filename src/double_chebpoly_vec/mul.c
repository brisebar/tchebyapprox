
#include "double_chebpoly_vec.h"


void double_chebpoly_vec_mul(double_chebpoly_vec_t P, const double_chebpoly_vec_t Q, const double_chebpoly_vec_t R)
{
  long n = Q->dim;
  long i;

  if (n != R->dim) {
    fprintf(stderr, "double_chebpoly_vec_mul: error: incompatible dimensions\n");
    return;
  }

  double_chebpoly_vec_set_dim(P, n);
  for (i = 0 ; i < n ; i++)
    double_chebpoly_mul(P->poly + i, Q->poly + i, R->poly + i);

}

