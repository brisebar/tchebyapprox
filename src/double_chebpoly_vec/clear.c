
#include "double_chebpoly_vec.h"


void double_chebpoly_vec_clear(double_chebpoly_vec_t P)
{
  long i;
  long n = P->dim;
  for (i = 0 ; i < n ; i++)
    double_chebpoly_clear(P->poly + i);
  free(P->poly);
}
