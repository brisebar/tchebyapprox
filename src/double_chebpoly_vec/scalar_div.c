
#include "double_chebpoly_vec.h"


void double_chebpoly_vec_scalar_div_si(double_chebpoly_vec_t P, const double_chebpoly_vec_t Q, long c)
{
  long n = Q->dim;
  long i;
  double_chebpoly_vec_set_dim(P, n);
  for (i = 0 ; i < n ; i++)
    double_chebpoly_scalar_div_si(P->poly + i, Q->poly + i, c);
}


void double_chebpoly_vec_scalar_div_z(double_chebpoly_vec_t P, const double_chebpoly_vec_t Q, const mpz_t c)
{
  long n = Q->dim;
  long i;
  double_chebpoly_vec_set_dim(P, n);
  for (i = 0 ; i < n ; i++)
    double_chebpoly_scalar_div_z(P->poly + i, Q->poly + i, c);
}


void double_chebpoly_vec_scalar_div_q(double_chebpoly_vec_t P, const double_chebpoly_vec_t Q, const mpq_t c)
{
  long n = Q->dim;
  long i;
  double_chebpoly_vec_set_dim(P, n);
  for (i = 0 ; i < n ; i++)
    double_chebpoly_scalar_div_q(P->poly + i, Q->poly + i, c);
}


void double_chebpoly_vec_scalar_div_d(double_chebpoly_vec_t P, const double_chebpoly_vec_t Q, const double_t c)
{
  long n = Q->dim;
  long i;
  double_chebpoly_vec_set_dim(P, n);
  for (i = 0 ; i < n ; i++)
    double_chebpoly_scalar_div_d(P->poly + i, Q->poly + i, c);
}


void double_chebpoly_vec_scalar_div_fr(double_chebpoly_vec_t P, const double_chebpoly_vec_t Q, const mpfr_t c)
{
  long n = Q->dim;
  long i;
  double_chebpoly_vec_set_dim(P, n);
  for (i = 0 ; i < n ; i++)
    double_chebpoly_scalar_div_fr(P->poly + i, Q->poly + i, c);
}


void double_chebpoly_vec_scalar_div_2si(double_chebpoly_vec_t P, const double_chebpoly_vec_t Q, long k)
{
  long n = Q->dim;
  long i;
  double_chebpoly_vec_set_dim(P, n);
  for (i = 0 ; i < n ; i++)
    double_chebpoly_scalar_div_2si(P->poly + i, Q->poly + i, k);
}



