
#include "double_chebpoly_vec.h"


void double_chebpoly_vec_scalar_prod(double_chebpoly_t P, const double_chebpoly_vec_t Q, const double_chebpoly_vec_t R)
{
  long n = Q->dim;
  long i;

  if (n != R->dim) {
    fprintf(stderr, "double_chebpoly_vec_scalar_prod: error: incompatible dimensions\n");
    return;
  }

  double_chebpoly_zero(P);
  double_chebpoly_t Temp;
  double_chebpoly_init(Temp);
  for (i = 0 ; i < n ; i++) {
    double_chebpoly_mul(Temp, Q->poly + i, R->poly + i);
    double_chebpoly_add(P, P, Temp);
  }

  double_chebpoly_clear(Temp);
}

