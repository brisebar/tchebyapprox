
#include "double_chebpoly_vec.h"


void double_chebpoly_vec_swap(double_chebpoly_vec_t P, double_chebpoly_vec_t Q)
{
  long dim_buf = P->dim;
  double_chebpoly_ptr poly_buf = P->poly;
  P->dim = Q->dim;
  P->poly = Q->poly;
  Q->dim = dim_buf;
  Q->poly = poly_buf;
}
