
#include "double_chebpoly_intop.h"


void double_chebpoly_intop_initvals(double_chebpoly_t G, const double_chebpoly_lode_t L, double_srcptr I)
{
  long r = L->order;
  long i;

  double_chebpoly_t P, Q;
  double_chebpoly_init(P);
  double_chebpoly_init(Q);

  double_chebpoly_zero(G);

  for (i = r - 1 ; i >= 0 ; i--) {
    
    // update P
    double_chebpoly_antiderivative_1(P, P);
    double_chebpoly_set_degree(Q, 0);
    double_chebpoly_set_coeff_d(Q, 0, I + i);
    double_chebpoly_add(P, P, Q);

    // add L->a + i * P to G
    double_chebpoly_mul(Q, L->a + i, P);
    double_chebpoly_sub(G, G, Q);

  }

  // clear variables
  double_chebpoly_clear(P);
  double_chebpoly_clear(Q);
}

