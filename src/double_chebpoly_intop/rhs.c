
#include "double_chebpoly_intop.h"


void double_chebpoly_intop_rhs(double_chebpoly_t G, const double_chebpoly_lode_t L, const double_chebpoly_t g, double_srcptr I)
{
  double_chebpoly_intop_initvals(G, L, I);
  double_chebpoly_add(G, G, g);
}

