
#include "double_chebpoly_intop.h"


void double_chebpoly_intop_approxsolve(double_chebpoly_t F, const double_chebpoly_intop_t K, const double_chebpoly_t G, long N)
{
  long i;

  // matrix associated to K
  double_bandmatrix_t M;
  double_bandmatrix_init(M);
  double_chebpoly_intop_get_bandmatrix(M, K, N);
  double_bandmatrix_neg(M, M);
  for (i = 0 ; i <= N ; i++)
    double_add_si(M->D[i] + M->Dwidth, M->D[i] + M->Dwidth, 1, MPFR_RNDN);

  // QR factorization
  double_bandmatrix_QRdecomp_t MM;
  double_bandmatrix_QRdecomp_init(MM);
  double_bandmatrix_get_QRdecomp(MM, M);

  // linear system solving
  double_chebpoly_t Sol;
  double_chebpoly_init(Sol);
  double_chebpoly_set(Sol, G);
  if (Sol->degree < N)
    double_chebpoly_set_degree(Sol, N);
  _double_bandmatrix_QRdecomp_solve_d(Sol->coeffs, MM, Sol->coeffs);
  double_chebpoly_normalise(Sol);

  // clear variables
  double_chebpoly_swap(F, Sol);
  double_chebpoly_clear(Sol);
  double_bandmatrix_clear(M);
  double_bandmatrix_QRdecomp_clear(MM);
}


void double_chebpoly_lode_intop_approxsolve(double_chebpoly_ptr Sols, const double_chebpoly_lode_t L, const double_chebpoly_t g, double_srcptr I, long N)
{
  long r = L->order;
  long i;

  // creating integral operator intop
  double_chebpoly_intop_t K;
  double_chebpoly_intop_init(K);
  double_chebpoly_intop_set_lode(K, L);

  // constructing rhs
  double_chebpoly_t G;
  double_chebpoly_init(G);
  double_chebpoly_intop_rhs(G, L, g, I);

  // solving for the r-th derivative using intop
  double_chebpoly_intop_approxsolve(Sols + r, K, G, N);

  // computing the other derivatives
  double_chebpoly_set_degree(G, 0);
  for (i = r - 1 ; i >= 0 ; i--) {
    double_chebpoly_antiderivative_1(Sols + i, Sols + i + 1);
    double_chebpoly_set_coeff_d(G, 0, I + i);
    double_chebpoly_add(Sols + i, Sols + i, G);
  }

  // clear variables
  double_chebpoly_intop_clear(K);
  double_chebpoly_clear(G);
}


 
