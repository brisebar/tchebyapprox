
#include "double_chebpoly_intop.h"


void double_chebpoly_intop_set_order(double_chebpoly_intop_t K, long order)
{
  long r = K->order;
  K->order = order;
  long i;

  if (order < r) {
    for (i = order ; i < r ; i++)
      double_chebpoly_clear(K->alpha + i);
    K->alpha = realloc(K->alpha, order * sizeof(double_chebpoly_t));
  }

  else if (order > r) {
    K->alpha = realloc(K->alpha, order * sizeof(double_chebpoly_t));
    for (i = r ; i < order ; i++)
      double_chebpoly_init(K->alpha + i);
  }

  else
    return;
}
