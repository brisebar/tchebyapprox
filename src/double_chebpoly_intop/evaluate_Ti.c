
#include "double_chebpoly_intop.h"


void double_chebpoly_intop_evaluate_Ti(double_bandvec_t V, const double_chebpoly_intop_t K, long i)
{
  long r = K->order;
  long h = double_chebpoly_intop_Hwidth(K);
  long s = double_chebpoly_intop_Dwidth(K);
  long j, k;

  double_bandvec_set_params(V, h, s, i);
  double_bandvec_zero(V);

  double_t temp, int_const;
  double_init(temp);
  double_init(int_const);


  for (j = 0 ; j < r ; j++) {

    double_set_zero(int_const, 1);

    // T_{i-j-1}
    if (i - j - 1) {

      for (k = 0 ; k <= K->alpha[j].degree ; k++) {
	double_div_si(temp, K->alpha[j].coeffs + k, i - j - 1, MPFR_RNDN);
	double_sub(V->D + s - j - 1 - k, V->D + s - j - 1 - k, temp, MPFR_RNDN);
	double_sub(V->D + s - j - 1 + k, V->D + s - j - 1 + k, temp, MPFR_RNDN);
      }

      if ((i + j) % 2)
	double_set_si(temp, 1, MPFR_RNDN);
      else
	double_set_si(temp, -1, MPFR_RNDN);
      double_div_si(temp, temp, i - j - 1, MPFR_RNDN);
      double_add(int_const, int_const, temp, MPFR_RNDN);

    }

    // T_{i-j+1}
    if (i - j + 1) {

      for (k = 0 ; k <= K->alpha[j].degree ; k++) {
	double_div_si(temp, K->alpha[j].coeffs + k, i - j + 1, MPFR_RNDN);
	double_add(V->D + s - j + 1 - k, V->D + s - j + 1 - k, temp, MPFR_RNDN);
	double_add(V->D + s - j + 1 + k, V->D + s - j + 1 + k, temp, MPFR_RNDN);
      }

      if ((i + j) % 2)
	double_set_si(temp, -1, MPFR_RNDN);
      else
	double_set_si(temp, 1, MPFR_RNDN);
      double_div_si(temp, temp, i - j + 1, MPFR_RNDN);
      double_add(int_const, int_const, temp, MPFR_RNDN);

    }

    // T_{i+j-1}
    if (i + j - 1) {

      for (k = 0 ; k <= K->alpha[j].degree ; k++) {
	double_div_si(temp, K->alpha[j].coeffs + k, i + j - 1, MPFR_RNDN);
	double_sub(V->D + s + j - 1 - k, V->D + s + j - 1 - k, temp, MPFR_RNDN);
	double_sub(V->D + s + j - 1 + k, V->D + s + j - 1 + k, temp, MPFR_RNDN);
      }

      if ((i + j) % 2)
	double_set_si(temp, 1, MPFR_RNDN);
      else
	double_set_si(temp, -1, MPFR_RNDN);
      double_div_si(temp, temp, i + j - 1, MPFR_RNDN);
      double_add(int_const, int_const, temp, MPFR_RNDN);

    }

    // T_{i+j+1}
    if (i + j + 1) {

      for (k = 0 ; k <= K->alpha[j].degree ; k++) {
	double_div_si(temp, K->alpha[j].coeffs + k, i + j + 1, MPFR_RNDN);
	double_add(V->D + s + j + 1 - k, V->D + s + j + 1 - k, temp, MPFR_RNDN);
	double_add(V->D + s + j + 1 + k, V->D + s + j + 1 + k, temp, MPFR_RNDN);
      }

      if ((i + j) % 2)
	double_set_si(temp, -1, MPFR_RNDN);
      else
	double_set_si(temp, 1, MPFR_RNDN);
      double_div_si(temp, temp, i + j + 1, MPFR_RNDN);
      double_add(int_const, int_const, temp, MPFR_RNDN);

    }

    // contribution of the integration constant
    for (k = 0 ; k <= K->alpha[j].degree ; k++) {
      double_mul(temp, int_const, K->alpha[j].coeffs + k, MPFR_RNDN);
      double_add(V->H + k, V->H + k, temp, MPFR_RNDN);
    }

  }

  // normalisation
  for (j = 0 ; j < h ; j++)
    double_mul_2si(V->H + j, V->H + j, -2, MPFR_RNDN);
  
  for (j = 0 ; j <= 2 * s ; j++)
    double_mul_2si(V->D + j, V->D + j, -3, MPFR_RNDN);

  double_bandvec_normalise(V);

  // clear variables
  double_clear(temp);
  double_clear(int_const);
}


void double_chebpoly_intop_evaluate_Ti_fi(mpfi_bandvec_t V, const double_chebpoly_intop_t K, long i)
{
  long r = K->order;
  long h = double_chebpoly_intop_Hwidth(K);
  long s = double_chebpoly_intop_Dwidth(K);
  long j, k;

  mpfi_bandvec_set_params(V, h, s, i);
  mpfi_bandvec_zero(V);

  mpfi_t temp, int_const;
  mpfi_init(temp);
  mpfi_init(int_const);


  for (j = 0 ; j < r ; j++) {

    mpfi_set_si(int_const, 0);

    // T_{i-j-1}
    if (i - j - 1) {

      for (k = 0 ; k <= K->alpha[j].degree ; k++) {
	mpfi_set_d(temp, K->alpha[j].coeffs[k]);
	mpfi_div_si(temp, temp, i - j - 1);
	mpfi_sub(V->D + s - j - 1 - k, V->D + s - j - 1 - k, temp);
	mpfi_sub(V->D + s - j - 1 + k, V->D + s - j - 1 + k, temp);
      }

      if ((i + j) % 2)
	mpfi_set_si(temp, 1);
      else
	mpfi_set_si(temp, -1);
      mpfi_div_si(temp, temp, i - j - 1);
      mpfi_add(int_const, int_const, temp);

    }

    // T_{i-j+1}
    if (i - j + 1) {

      for (k = 0 ; k <= K->alpha[j].degree ; k++) {
	mpfi_set_d(temp, K->alpha[j].coeffs[k]);
	mpfi_div_si(temp, temp, i - j + 1);
	mpfi_add(V->D + s - j + 1 - k, V->D + s - j + 1 - k, temp);
	mpfi_add(V->D + s - j + 1 + k, V->D + s - j + 1 + k, temp);
      }

      if ((i + j) % 2)
	mpfi_set_si(temp, -1);
      else
	mpfi_set_si(temp, 1);
      mpfi_div_si(temp, temp, i - j + 1);
      mpfi_add(int_const, int_const, temp);

    }

    // T_{i+j-1}
    if (i + j - 1) {

      for (k = 0 ; k <= K->alpha[j].degree ; k++) {
	mpfi_set_d(temp, K->alpha[j].coeffs[k]);
	mpfi_div_si(temp, temp, i + j - 1);
	mpfi_sub(V->D + s + j - 1 - k, V->D + s + j - 1 - k, temp);
	mpfi_sub(V->D + s + j - 1 + k, V->D + s + j - 1 + k, temp);
      }

      if ((i + j) % 2)
	mpfi_set_si(temp, 1);
      else
	mpfi_set_si(temp, -1);
      mpfi_div_si(temp, temp, i + j - 1);
      mpfi_add(int_const, int_const, temp);

    }

    // T_{i+j+1}
    if (i + j + 1) {

      for (k = 0 ; k <= K->alpha[j].degree ; k++) {
	mpfi_set_d(temp, K->alpha[j].coeffs[k]);
	mpfi_div_si(temp, temp, i + j + 1);
	mpfi_add(V->D + s + j + 1 - k, V->D + s + j + 1 - k, temp);
	mpfi_add(V->D + s + j + 1 + k, V->D + s + j + 1 + k, temp);
      }

      if ((i + j) % 2)
	mpfi_set_si(temp, -1);
      else
	mpfi_set_si(temp, 1);
      mpfi_div_si(temp, temp, i + j + 1);
      mpfi_add(int_const, int_const, temp);

    }

    // contribution of the integration constant
    for (k = 0 ; k <= K->alpha[j].degree ; k++) {
      mpfi_mul_d(temp, int_const, K->alpha[j].coeffs[k]);
      mpfi_add(V->H + k, V->H + k, temp);
    }

  }

  // normalisation
  for (j = 0 ; j < h ; j++)
    mpfi_mul_2si(V->H + j, V->H + j, -2);
  
  for (j = 0 ; j <= 2 * s ; j++)
    mpfi_mul_2si(V->D + j, V->D + j, -3);

  mpfi_bandvec_normalise(V);

  // clear variables
  mpfi_clear(temp);
  mpfi_clear(int_const);
}

