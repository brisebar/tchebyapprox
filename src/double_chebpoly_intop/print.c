
#include "double_chebpoly_intop.h"


void double_chebpoly_intop_print(const double_chebpoly_intop_t K, const char * Cheb_var)
{
  long r = K->order;
  long i;

  for (i = 0 ; i < r ; i++) {
    printf("alpha[%ld] = ", i);
    double_chebpoly_print(K->alpha + i, Cheb_var);
    printf("\n");
  }
}
