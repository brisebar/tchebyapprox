
#include "double_chebpoly_intop.h"



void double_chebpoly_intop_approxsolve_check(double_t bound, double_chebpoly_srcptr Sols, const double_chebpoly_lode_t L, const double_chebpoly_t g, double_srcptr I)
{
  long r = L->order; 
  long i;

  // error in the differential equation
  double_chebpoly_t P, Temp;
  double_chebpoly_init(P);
  double_chebpoly_init(Temp);
 
  double_chebpoly_set(P, Sols + r);

  for (i = 0 ; i < r ; i++) {
    double_chebpoly_mul(Temp, L->a + i, Sols + i);
    double_chebpoly_add(P, P, Temp);
  }

  double_chebpoly_sub(P, P, g);

  double_chebpoly_1norm(bound, P);

  // error in the initial conditions
  double_t t;
  double_init(t);

  for (i = 0 ; i < r ; i++) {
    double_chebpoly_evaluate_si(t, Sols + i, -1);
    double_sub(t, t, I + i, MPFR_RNDN);
    double_abs(t, t, MPFR_RNDU);
    double_add(bound, bound, t, MPFR_RNDU);
  }

  double_clear(t);
  double_chebpoly_clear(P);
  double_chebpoly_clear(Temp);
}

