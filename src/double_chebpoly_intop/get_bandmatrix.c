
#include "double_chebpoly_intop.h"


void double_chebpoly_intop_get_bandmatrix(double_bandmatrix_t M, const double_chebpoly_intop_t K, long N)
{
  long h = double_chebpoly_intop_Hwidth(K);
  long s = double_chebpoly_intop_Dwidth(K);
  long i;

  double_bandmatrix_set_params(M, N + 1, h, s);
  double_bandmatrix_zero(M);

  double_bandvec_t V;
  double_bandvec_init(V);

  for (i = 0 ; i <= N ; i++) {
    double_chebpoly_intop_evaluate_Ti(V, K, i);
    double_bandmatrix_set_column(M, V);
  }

  double_bandvec_clear(V);
}



