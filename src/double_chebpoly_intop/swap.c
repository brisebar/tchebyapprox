
#include "double_chebpoly_intop.h"


void double_chebpoly_intop_swap(double_chebpoly_intop_t K, double_chebpoly_intop_t N)
{
  long order_buf = K->order;
  double_chebpoly_ptr alpha_buf = K->alpha;

  K->order = N->order;
  K->alpha = N->alpha;

  N->order = order_buf;
  N->alpha = alpha_buf;
}
