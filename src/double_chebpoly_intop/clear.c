
#include "double_chebpoly_intop.h"


void double_chebpoly_intop_clear(double_chebpoly_intop_t K)
{
  long r = K->order;
  long i;
  for (i = 0 ; i < r ; i++)
    double_chebpoly_clear(K->alpha + i);
  free(K->alpha);
}
