
#include "double_chebpoly_intop.h"


void double_chebpoly_intop_set(double_chebpoly_intop_t K, const double_chebpoly_intop_t N)
{
  long r = N->order;
  double_chebpoly_intop_set_order(K, r);

  long i;
  for (i = 0 ; i < r ; i++)
    double_chebpoly_set(K->alpha + i, N->alpha + i);
}
