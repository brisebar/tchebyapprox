
#include "mpfi_chebpoly_intop.h"


void mpfi_chebpoly_intop_rhs(mpfi_chebpoly_t G, const mpfi_chebpoly_lode_t L, const mpfi_chebpoly_t g, mpfi_srcptr I)
{
  mpfi_chebpoly_intop_initvals(G, L, I);
  mpfi_chebpoly_add(G, G, g);
}

