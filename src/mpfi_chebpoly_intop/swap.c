
#include "mpfi_chebpoly_intop.h"


void mpfi_chebpoly_intop_swap(mpfi_chebpoly_intop_t K, mpfi_chebpoly_intop_t N)
{
  long order_buf = K->order;
  mpfi_chebpoly_ptr alpha_buf = K->alpha;

  K->order = N->order;
  K->alpha = N->alpha;

  N->order = order_buf;
  N->alpha = alpha_buf;
}
