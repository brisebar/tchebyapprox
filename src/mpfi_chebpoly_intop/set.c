
#include "mpfi_chebpoly_intop.h"


void mpfi_chebpoly_intop_set(mpfi_chebpoly_intop_t K, const mpfi_chebpoly_intop_t N)
{
  long r = N->order;
  mpfi_chebpoly_intop_set_order(K, r);

  long i;
  for (i = 0 ; i < r ; i++)
    mpfi_chebpoly_set(K->alpha + i, N->alpha + i);
}


void mpfi_chebpoly_intop_set_double_chebpoly_intop(mpfi_chebpoly_intop_t K, const double_chebpoly_intop_t L)
{
  long r = L->order;
  long i;

  mpfi_chebpoly_intop_set_order(K, r);

  for (i = 0 ; i < r ; i++)
    mpfi_chebpoly_set_double_chebpoly(K->alpha + i, L->alpha + i);
}



void mpfi_chebpoly_intop_set_mpfr_chebpoly_intop(mpfi_chebpoly_intop_t K, const mpfr_chebpoly_intop_t L)
{
  long r = L->order;
  long i;

  mpfi_chebpoly_intop_set_order(K, r);

  for (i = 0 ; i < r ; i++)
    mpfi_chebpoly_set_mpfr_chebpoly(K->alpha + i, L->alpha + i);
}


void mpfi_chebpoly_intop_get_double_chebpoly_intop(double_chebpoly_intop_t K, const mpfi_chebpoly_intop_t L)
{
  long r = L->order;
  long i;

  double_chebpoly_intop_set_order(K, r);

  for (i = 0 ; i < r ; i++)
    mpfi_chebpoly_get_double_chebpoly(K->alpha + i, L->alpha + i);
}


void mpfi_chebpoly_intop_get_mpfr_chebpoly_intop(mpfr_chebpoly_intop_t K, const mpfi_chebpoly_intop_t L)
{
  long r = L->order;
  long i;

  mpfr_chebpoly_intop_set_order(K, r);

  for (i = 0 ; i < r ; i++)
    mpfi_chebpoly_get_mpfr_chebpoly(K->alpha + i, L->alpha + i);
}


