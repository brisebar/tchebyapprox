
#include "mpfi_chebpoly_intop.h"


void mpfi_chebpoly_intop_initvals(mpfi_chebpoly_t G, const mpfi_chebpoly_lode_t L, mpfi_srcptr I)
{
  long r = L->order;
  long i;

  mpfi_chebpoly_t P, Q;
  mpfi_chebpoly_init(P);
  mpfi_chebpoly_init(Q);

  mpfi_chebpoly_zero(G);

  for (i = r - 1 ; i >= 0 ; i--) {
    
    // update P
    mpfi_chebpoly_antiderivative_1(P, P);
    mpfi_chebpoly_set_degree(Q, 0);
    mpfi_chebpoly_set_coeff_fi(Q, 0, I + i);
    mpfi_chebpoly_add(P, P, Q);

    // add L->a + i * P to G
    mpfi_chebpoly_mul(Q, L->a + i, P);
    mpfi_chebpoly_sub(G, G, Q);

  }

  // clear variables
  mpfi_chebpoly_clear(P);
  mpfi_chebpoly_clear(Q);
}

