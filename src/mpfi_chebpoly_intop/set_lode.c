
#include "mpfi_chebpoly_intop.h"

void mpfi_chebpoly_intop_set_lode(mpfi_chebpoly_intop_t K, const mpfi_chebpoly_lode_t L)
{
	long r = L->order;
	long i, j, k;
	mpfi_chebpoly_t Temp, gamma;
	mpfi_chebpoly_init(Temp);
	mpfi_chebpoly_init(gamma);

	mpfi_chebpoly_intop_set_order(K, r);
	for (i = 0 ; i < r ; i++)
		mpfi_chebpoly_zero(K->alpha + i);

	// compute alpha's by subtracting (t-s)^i / i! * a[r-1-i](t)

	// (t-s)^i / i! = Sum_{j=0}^i beta[j](t) * T_j(s)  for i=0..r-1
	mpfi_chebpoly_ptr beta = malloc(r * sizeof(mpfi_chebpoly_t));
	for (j = 0 ; j < r ; j++)
		mpfi_chebpoly_init(beta + j);

	// start with i=0
	if (r > 0) {
		mpfi_chebpoly_set_coeff_si(beta + 0, 0, 1);
		mpfi_chebpoly_sub(K->alpha + 0, K->alpha + 0, L->a + r-1);
	}

	for (i = 1 ; i < r ; i++) {

		// update beta[j] s.t. (t-s)^{i-1} / (i-1)! => (t-s)^i / i!
		// by integrating w.r.t t from s to t
		mpfi_chebpoly_zero(gamma);
		for (j = 0 ; j < i ; j++) {
			// primitive along t
			mpfi_chebpoly_antiderivative(beta + j, beta + j);
			// store component at t=s to be subtracted
			mpfi_chebpoly_zero(Temp);
			mpfi_chebpoly_set_coeff_si(Temp, j, 1);
			mpfi_chebpoly_mul(Temp, beta + j, Temp);
			mpfi_chebpoly_add(gamma, gamma, Temp);
		}

		// subtract gamma by distributing over the beta[j]
		// assignes b[i] for the first time
		mpfi_chebpoly_set_degree(Temp, 0);
		for (j = 0 ; j <= i ; j++) {
			mpfi_chebpoly_get_coeff(Temp->coeffs + 0, gamma, j);
			mpfi_chebpoly_sub(beta + j, beta + j, Temp);
		}

		// multiply by a[r-1-i] and add to alpha's
		for (j = 0 ; j <= i ; j++) {
			mpfi_chebpoly_mul(Temp, beta + j, L->a + r-1-i);
			mpfi_chebpoly_sub(K->alpha + j, K->alpha + j, Temp);
		}

	}

	// clear variables
	for (i = 0 ; i < r ; i++)
		mpfi_chebpoly_clear(beta + i);
	free(beta);
	mpfi_chebpoly_clear(Temp);
	mpfi_chebpoly_clear(gamma);
}
