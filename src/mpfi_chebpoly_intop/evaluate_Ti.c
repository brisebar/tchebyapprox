
#include "mpfi_chebpoly_intop.h"


void mpfi_chebpoly_intop_evaluate_Ti(mpfi_bandvec_t V, const mpfi_chebpoly_intop_t K, long i)
{
  long r = K->order;
  long h = mpfi_chebpoly_intop_Hwidth(K);
  long s = mpfi_chebpoly_intop_Dwidth(K);
  long j, k;

  mpfi_bandvec_set_params(V, h, s, i);
  mpfi_bandvec_zero(V);

  mpfi_t temp, int_const;
  mpfi_init(temp);
  mpfi_init(int_const);


  for (j = 0 ; j < r ; j++) {

    mpfi_set_si(int_const, 0);

    // T_{i-j-1}
    if (i - j - 1) {

      for (k = 0 ; k <= K->alpha[j].degree ; k++) {
	mpfi_div_si(temp, K->alpha[j].coeffs + k, i - j - 1);
	mpfi_sub(V->D + s - j - 1 - k, V->D + s - j - 1 - k, temp);
	mpfi_sub(V->D + s - j - 1 + k, V->D + s - j - 1 + k, temp);
      }

      if ((i + j) % 2)
	mpfi_set_si(temp, 1);
      else
	mpfi_set_si(temp, -1);
      mpfi_div_si(temp, temp, i - j - 1);
      mpfi_add(int_const, int_const, temp);

    }

    // T_{i-j+1}
    if (i - j + 1) {

      for (k = 0 ; k <= K->alpha[j].degree ; k++) {
	mpfi_div_si(temp, K->alpha[j].coeffs + k, i - j + 1);
	mpfi_add(V->D + s - j + 1 - k, V->D + s - j + 1 - k, temp);
	mpfi_add(V->D + s - j + 1 + k, V->D + s - j + 1 + k, temp);
      }

      if ((i + j) % 2)
	mpfi_set_si(temp, -1);
      else
	mpfi_set_si(temp, 1);
      mpfi_div_si(temp, temp, i - j + 1);
      mpfi_add(int_const, int_const, temp);

    }

    // T_{i+j-1}
    if (i + j - 1) {

      for (k = 0 ; k <= K->alpha[j].degree ; k++) {
	mpfi_div_si(temp, K->alpha[j].coeffs + k, i + j - 1);
	mpfi_sub(V->D + s + j - 1 - k, V->D + s + j - 1 - k, temp);
	mpfi_sub(V->D + s + j - 1 + k, V->D + s + j - 1 + k, temp);
      }

      if ((i + j) % 2)
	mpfi_set_si(temp, 1);
      else
	mpfi_set_si(temp, -1);
      mpfi_div_si(temp, temp, i + j - 1);
      mpfi_add(int_const, int_const, temp);

    }

    // T_{i+j+1}
    if (i + j + 1) {

      for (k = 0 ; k <= K->alpha[j].degree ; k++) {
	mpfi_div_si(temp, K->alpha[j].coeffs + k, i + j + 1);
	mpfi_add(V->D + s + j + 1 - k, V->D + s + j + 1 - k, temp);
	mpfi_add(V->D + s + j + 1 + k, V->D + s + j + 1 + k, temp);
      }

      if ((i + j) % 2)
	mpfi_set_si(temp, -1);
      else
	mpfi_set_si(temp, 1);
      mpfi_div_si(temp, temp, i + j + 1);
      mpfi_add(int_const, int_const, temp);

    }

    // contribution of the integration constant
    for (k = 0 ; k <= K->alpha[j].degree ; k++) {
      mpfi_mul(temp, int_const, K->alpha[j].coeffs + k);
      mpfi_add(V->H + k, V->H + k, temp);
    }

  }

  // normalisation
  for (j = 0 ; j < h ; j++)
    mpfi_mul_2si(V->H + j, V->H + j, -2);
  
  for (j = 0 ; j <= 2 * s ; j++)
    mpfi_mul_2si(V->D + j, V->D + j, -3);

  mpfi_bandvec_normalise(V);

  // clear variables
  mpfi_clear(temp);
  mpfi_clear(int_const);
}

