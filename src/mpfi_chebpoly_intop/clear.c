
#include "mpfi_chebpoly_intop.h"


void mpfi_chebpoly_intop_clear(mpfi_chebpoly_intop_t K)
{
  long r = K->order;
  long i;
  for (i = 0 ; i < r ; i++)
    mpfi_chebpoly_clear(K->alpha + i);
  free(K->alpha);
}
