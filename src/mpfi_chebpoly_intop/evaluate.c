
#include "mpfi_chebpoly_intop.h"


void mpfi_chebpoly_intop_evaluate_fr(mpfi_chebpoly_t P, const mpfi_chebpoly_intop_t K, const mpfr_chebpoly_t Q)
{
  long r = K->order;
  long i;

  mpfi_chebpoly_t K_Q, K_Q_i, Q_fi;
  mpfi_chebpoly_init(K_Q);
  mpfi_chebpoly_init(K_Q_i);
  mpfi_chebpoly_init(Q_fi);
  mpfi_chebpoly_set_mpfr_chebpoly(Q_fi, Q);

  for (i = 0 ; i < r ; i++) {
    
    mpfi_chebpoly_zero(K_Q_i);
    mpfi_chebpoly_set_coeff_si(K_Q_i, i, 1);
    mpfi_chebpoly_mul(K_Q_i, K_Q_i, Q_fi);

    mpfi_chebpoly_antiderivative_1(K_Q_i, K_Q_i);

    mpfi_chebpoly_mul(K_Q_i, K->alpha + i, K_Q_i);

    mpfi_chebpoly_add(K_Q, K_Q, K_Q_i);

  }

  // clear variables
  mpfi_chebpoly_clear(Q_fi);
  mpfi_chebpoly_clear(K_Q_i);
  mpfi_chebpoly_swap(P, K_Q);
  mpfi_chebpoly_clear(K_Q);
}


void mpfi_chebpoly_intop_evaluate_fi(mpfi_chebpoly_t P, const mpfi_chebpoly_intop_t K, const mpfi_chebpoly_t Q)
{
  long r = K->order;
  long i;

  mpfi_chebpoly_t K_Q, K_Q_i;
  mpfi_chebpoly_init(K_Q);
  mpfi_chebpoly_init(K_Q_i);

  for (i = 0 ; i < r ; i++) {
    
    mpfi_chebpoly_zero(K_Q_i);
    mpfi_chebpoly_set_coeff_si(K_Q_i, i, 1);
    mpfi_chebpoly_mul(K_Q_i, K_Q_i, Q);

    mpfi_chebpoly_antiderivative_1(K_Q_i, K_Q_i);

    mpfi_chebpoly_mul(K_Q_i, K->alpha + i, K_Q_i);

    mpfi_chebpoly_add(K_Q, K_Q, K_Q_i);

  }

  // clear variables
  mpfi_chebpoly_clear(K_Q_i);
  mpfi_chebpoly_swap(P, K_Q);
  mpfi_chebpoly_clear(K_Q);
}

