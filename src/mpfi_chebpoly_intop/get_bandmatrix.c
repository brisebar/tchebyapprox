
#include "mpfi_chebpoly_intop.h"


void mpfi_chebpoly_intop_get_bandmatrix(mpfi_bandmatrix_t M, const mpfi_chebpoly_intop_t K, long N)
{
  long h = mpfi_chebpoly_intop_Hwidth(K);
  long s = mpfi_chebpoly_intop_Dwidth(K);
  long i;

  mpfi_bandmatrix_set_params(M, N + 1, h, s);
  mpfi_bandmatrix_zero(M);

  mpfi_bandvec_t V;
  mpfi_bandvec_init(V);

  for (i = 0 ; i <= N ; i++) {
    mpfi_chebpoly_intop_evaluate_Ti(V, K, i);
    mpfi_bandmatrix_set_column(M, V);
  }

  mpfi_bandvec_clear(V);
}



