
#include "double_operations.h"


void double_swap(double_t x, double_t y)
{
  double buf = *x;
  *x = *y;
  *y = buf;
  return;
}


