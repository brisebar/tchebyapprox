
#include "mpfr_chebpoly_lode.h"


void mpfr_chebpoly_lode_swap(mpfr_chebpoly_lode_t L, mpfr_chebpoly_lode_t M)
{
  long order_buf = L->order;
  mpfr_chebpoly_ptr a_buf = L->a;

  L->order = M->order;
  L->a = M->a;

  M->order = order_buf;
  M->a = a_buf;
}
