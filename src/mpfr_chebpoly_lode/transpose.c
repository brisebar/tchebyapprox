
#include "mpfr_chebpoly_lode.h"


/* transpose from x-D form to D-x form and conversely */
// x-D form : a_{r-1} * D^(r-1)y + ... + a_1 * Dy + a_0 * y
// D-x form : (-D)^(r-1)(b_{r-1} * y) + ... + (-D)(b_1 * y) + b_0 * y
// note: involutive function, can be used in both directions
void mpfr_chebpoly_lode_transpose(mpfr_chebpoly_ptr b, mpfr_chebpoly_srcptr a, long r)
{
  long i, j;
  mpfr_chebpoly_t d_bj;
  mpfr_chebpoly_init(d_bj);

  // copy (and alternate signs) from a to b
  for (i = 0 ; i < r ; i++)
    if ((r - i) % 2)
      mpfr_chebpoly_neg(b + i, a + i);
    else
      mpfr_chebpoly_set(b + i, a + i);

  // transpose
  for (i = r-2 ; i >= 0 ; i--)
    for (j = i+1 ; j < r ; j++) {
      mpfr_chebpoly_derivative(d_bj, b + j);
      mpfr_chebpoly_add(b + j-1, b + j-1, d_bj);
    }

  // clear variables
  mpfr_chebpoly_clear(d_bj);
}



/* transpose initial conditions */
// x-D initial conditions: y^(i) = v[i]
// D-x initial conditions: y^[i] = w[i]
// where y^[0] = y and y^[i+1] = -Dy^[i] + a_{r-1-i} y

// v -> w
// take as input the coefficients b[i] of the D-x form differential operator
void mpfr_chebpoly_lode_transpose_init_xDtoDx(mpfr_ptr w, mpfr_srcptr v, mpfr_chebpoly_srcptr b, long r)
{
  long i, j;

  // germ of Y at -1
  mpfr_chebpoly_t Y, cst;
  mpfr_chebpoly_init(Y);
  mpfr_chebpoly_init(cst);
  mpfr_chebpoly_set_degree(cst, 0);
  for (i = r-1 ; i >= 0 ; i--) {
    mpfr_chebpoly_antiderivative_1(Y, Y);
    mpfr_chebpoly_set_coeff_fr(cst, 0, v + i);
    mpfr_chebpoly_add(Y, Y, cst);
  }

  // Yi = Y^[i]
  mpfr_chebpoly_t Yi, Temp;
  mpfr_chebpoly_init(Yi);
  mpfr_chebpoly_init(Temp);
  mpfr_chebpoly_set(Yi, Y);
  for (i = 0 ; i < r ; i++) {
    mpfr_chebpoly_evaluate_1(w + i, Yi);
    mpfr_chebpoly_mul(Temp, b + r-1-i, Y);
    mpfr_chebpoly_derivative(Yi, Yi);
    mpfr_chebpoly_sub(Yi, Temp, Yi);
  }

  // clear variables
  mpfr_chebpoly_clear(Y);
  mpfr_chebpoly_clear(cst);
  mpfr_chebpoly_clear(Yi);
  mpfr_chebpoly_clear(Temp);
}


// v -> w
// take as input the coefficients b[i] of the D-x form differential operator
void mpfr_chebpoly_lode_transpose_init_DxtoxD(mpfr_ptr v, mpfr_srcptr w, mpfr_chebpoly_srcptr b, long r)
{
  long i, j;
  mpfr_chebpoly_t Y, Yj, Temp, cst;
  mpfr_chebpoly_init(Y);
  mpfr_chebpoly_init(Yj);
  mpfr_chebpoly_init(Temp);
  mpfr_chebpoly_init(cst);
  mpfr_chebpoly_set_degree(cst, 0);

  // at step i, compute the Yj := y^[j] for j=i..0
  // and set Y=Yj at j=0 ==> degree i germ of Y at -1
  for (i = 0 ; i < r ; i++) {
    // set Yj = y^[i](-1)
    mpfr_chebpoly_set_degree(Yj, 0);
    mpfr_chebpoly_set_coeff_fr(Yj, 0, w + i);
    // compute Yj = y^[j]
    for (j = i-1 ; j >= 0 ; j--) {
      mpfr_chebpoly_mul(Temp, b + r-1-j, Y);
      mpfr_chebpoly_sub(Temp, Temp, Yj);
      mpfr_chebpoly_antiderivative_1(Temp, Temp);
      mpfr_chebpoly_set_coeff_fr(cst, 0, w + j);
      mpfr_chebpoly_add(Yj, cst, Temp);
    }
    // assign Yj to Y
    mpfr_chebpoly_set(Y, Yj);
  }

  // extract initial conditions
  for (i = 0 ; i < r ; i++) {
    mpfr_chebpoly_evaluate_1(v + i, Y);
    mpfr_chebpoly_derivative(Y, Y);
  }

  // clear variables
  mpfr_chebpoly_clear(Y);
  mpfr_chebpoly_clear(Yj);
  mpfr_chebpoly_clear(Temp);
  mpfr_chebpoly_clear(cst);
}
