
#include "mpfr_chebpoly_lode.h"


void mpfr_chebpoly_lode_set_order(mpfr_chebpoly_lode_t L, long order)
{
  long r = L->order;
  L->order = order;
  long i;

  if (order < r) {
    for (i = order ; i < r ; i++)
      mpfr_chebpoly_clear(L->a + i);
    L->a = realloc(L->a, order * sizeof(mpfr_chebpoly_t));
  }

  else if (order > r) {
    L->a = realloc(L->a, order * sizeof(mpfr_chebpoly_t));
    for (i = r ; i < order ; i++)
      mpfr_chebpoly_init(L->a + i);
  }

  else
    return;
}
