
#include "mpfr_chebpoly_lode.h"


void mpfr_chebpoly_lode_set(mpfr_chebpoly_lode_t L, const mpfr_chebpoly_lode_t M)
{
  long r = M->order;
  mpfr_chebpoly_lode_set_order(L, r);

  long i;
  for (i = 0 ; i < r ; i++)
    mpfr_chebpoly_set(L->a + i, M->a + i);
}

