
#include "mpfr_chebpoly_lode.h"


void mpfr_chebpoly_lode_evaluate_fr(mpfr_chebpoly_t P, const mpfr_chebpoly_lode_t L, const mpfr_chebpoly_t Q)
{
  mpfr_chebpoly_t L_Q, d_Q, Temp;
  mpfr_chebpoly_init(L_Q);
  mpfr_chebpoly_init(d_Q);
  mpfr_chebpoly_init(Temp);
  mpfr_chebpoly_set(d_Q, Q);

  long r = L->order;
  long i;

  for (i = 0 ; i < r ; i++) {
    
    mpfr_chebpoly_mul(Temp, L->a + i, d_Q);
    mpfr_chebpoly_add(L_Q, L_Q, Temp);

    mpfr_chebpoly_derivative(d_Q, d_Q);

  }

  mpfr_chebpoly_add(L_Q, L_Q, d_Q);

  mpfr_chebpoly_swap(P, L_Q);
  mpfr_chebpoly_clear(L_Q);
  mpfr_chebpoly_clear(d_Q);
  mpfr_chebpoly_clear(Temp);
}


void mpfr_chebpoly_lode_evaluate_fi(mpfi_chebpoly_t P, const mpfr_chebpoly_lode_t L, const mpfi_chebpoly_t Q)
{
  mpfi_chebpoly_t L_Q, d_Q, Temp;
  mpfi_chebpoly_init(L_Q);
  mpfi_chebpoly_init(d_Q);
  mpfi_chebpoly_init(Temp);
  mpfi_chebpoly_set(d_Q, Q);

  long r = L->order;
  long i;

  for (i = 0 ; i < r ; i++) {
    
    mpfi_chebpoly_set_mpfr_chebpoly(Temp, L->a + i);
    mpfi_chebpoly_mul(Temp, Temp, d_Q);
    mpfi_chebpoly_add(L_Q, L_Q, Temp);

    mpfi_chebpoly_derivative(d_Q, d_Q);

  }

  mpfi_chebpoly_add(L_Q, L_Q, d_Q);

  mpfi_chebpoly_swap(P, L_Q);
  mpfi_chebpoly_clear(L_Q);
  mpfi_chebpoly_clear(d_Q);
  mpfi_chebpoly_clear(Temp);
}


