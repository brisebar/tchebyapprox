
#include "mpfr_chebpoly_lode.h"


void mpfr_chebpoly_lode_clear(mpfr_chebpoly_lode_t L)
{
  long r = L->order;
  long i;
  for (i = 0 ; i < r ; i++)
    mpfr_chebpoly_clear(L->a + i);
  free(L->a);
}
