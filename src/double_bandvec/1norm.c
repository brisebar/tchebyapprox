
#include "double_bandvec.h"


void _double_bandvec_1norm(double_t norm, double_srcptr VH, double_srcptr VD, long Hwidth, long Dwidth, long ind)
{
  double_set_zero(norm, 1);
  long i, imin, imax;
  
  imax = ind-Dwidth < Hwidth ? ind-Dwidth : Hwidth;
  for (i = 0 ; i < imax ; i++)
    if (double_sgn(VH + i) >= 0)
      double_add(norm, norm, VH + i, MPFR_RNDN);
    else
      double_sub(norm, norm, VH + i, MPFR_RNDN);

  imin = ind-Dwidth < 0 ? 0 : ind-Dwidth;
  for (i = imin ; i <= ind+Dwidth ; i++)
    if (double_sgn(VD + Dwidth-ind+i) >= 0)
      double_add(norm, norm, VD + Dwidth-ind+i, MPFR_RNDN);
    else
      double_sub(norm, norm, VD + Dwidth-ind+i, MPFR_RNDN);

  for (i = ind+Dwidth+1 ; i < Hwidth ; i++)
    if (double_sgn(VH + i) >= 0)
      double_add(norm, norm, VH + i, MPFR_RNDN);
    else
      double_sub(norm, norm, VH + i, MPFR_RNDN);
}


void double_bandvec_1norm(double_t norm, const double_bandvec_t V)
{
  _double_bandvec_1norm(norm, V->H, V->D, V->Hwidth, V->Dwidth, V->ind);
}



void _double_bandvec_1norm_ubound(double_t norm, double_srcptr VH, double_srcptr VD, long Hwidth, long Dwidth, long ind)
{
  mpfr_t norm_fr;
  mpfr_init(norm_fr);
  mpfr_set_zero(norm_fr, 1);
  long i, imin, imax;
  
  imax = ind-Dwidth < Hwidth ? ind-Dwidth : Hwidth;
  for (i = 0 ; i < imax ; i++)
    if (double_sgn(VH + i) >= 0)
      mpfr_add_d(norm_fr, norm_fr, VH[i], MPFR_RNDU);
    else
      mpfr_sub_d(norm_fr, norm_fr, VH[i], MPFR_RNDU);

  imin = ind-Dwidth < 0 ? 0 : ind-Dwidth;
  for (i = imin ; i <= ind+Dwidth ; i++)
    if (double_sgn(VD + Dwidth-ind+i) >= 0)
      mpfr_add_d(norm_fr, norm_fr, VD[Dwidth-ind+i], MPFR_RNDU);
    else
      mpfr_sub_d(norm_fr, norm_fr, VD[Dwidth-ind+i], MPFR_RNDU);

  for (i = ind+Dwidth+1 ; i < Hwidth ; i++)
    if (double_sgn(VH + i) >= 0)
      mpfr_add_d(norm_fr, norm_fr, VH[i], MPFR_RNDU);
    else
      mpfr_sub_d(norm_fr, norm_fr, VH[i], MPFR_RNDU);

  double_set_fr(norm, norm_fr, MPFR_RNDU);
  mpfr_clear(norm_fr); 
  
}


void double_bandvec_1norm_ubound(double_t norm, const double_bandvec_t V)
{
  _double_bandvec_1norm_ubound(norm, V->H, V->D, V->Hwidth, V->Dwidth, V->ind);
}


void _double_bandvec_1norm_lbound(double_t norm, double_srcptr VH, double_srcptr VD, long Hwidth, long Dwidth, long ind)
{
  mpfr_t norm_fr;
  mpfr_init(norm_fr);
  mpfr_set_zero(norm_fr, 1);
  long i, imin, imax;
  
  imax = ind-Dwidth < Hwidth ? ind-Dwidth : Hwidth;
  for (i = 0 ; i < imax ; i++)
    if (double_sgn(VH + i) >= 0)
      mpfr_add_d(norm_fr, norm_fr, VH[i], MPFR_RNDD);
    else
      mpfr_sub_d(norm_fr, norm_fr, VH[i], MPFR_RNDD);

  imin = ind-Dwidth < 0 ? 0 : ind-Dwidth;
  for (i = imin ; i <= ind+Dwidth ; i++)
    if (double_sgn(VD + Dwidth-ind+i) >= 0)
      mpfr_add_d(norm_fr, norm_fr, VD[Dwidth-ind+i], MPFR_RNDD);
    else
      mpfr_sub_d(norm_fr, norm_fr, VD[Dwidth-ind+i], MPFR_RNDD);

  for (i = ind+Dwidth+1 ; i < Hwidth ; i++)
    if (double_sgn(VH + i) >= 0)
      mpfr_add_d(norm_fr, norm_fr, VH[i], MPFR_RNDD);
    else
      mpfr_sub_d(norm_fr, norm_fr, VH[i], MPFR_RNDD);

  double_set_fr(norm, norm_fr, MPFR_RNDD);
  mpfr_clear(norm_fr);

}


void double_bandvec_1norm_lbound(double_t norm, const double_bandvec_t V)
{
  _double_bandvec_1norm_lbound(norm, V->H, V->D, V->Hwidth, V->Dwidth, V->ind);
}


void _double_bandvec_1norm_fi(mpfi_t norm, double_srcptr VH, double_srcptr VD, long Hwidth, long Dwidth, long ind)
{
  mpfi_set_si(norm, 0);
  long i, imin, imax;
  
  imax = ind-Dwidth < Hwidth ? ind-Dwidth : Hwidth;
  for (i = 0 ; i < imax ; i++)
    if (double_sgn(VH + i) >= 0)
      mpfi_add_d(norm, norm, VH[i]);
    else
      mpfi_sub_d(norm, norm, VH[i]);

  imin = ind-Dwidth < 0 ? 0 : ind-Dwidth;
  for (i = imin ; i <= ind+Dwidth ; i++)
    if (double_sgn(VD + Dwidth-ind+i) >= 0)
      mpfi_add_d(norm, norm, VD[Dwidth-ind+i]);
    else
      mpfi_sub_d(norm, norm, VD[Dwidth-ind+i]);

  for (i = ind+Dwidth+1 ; i < Hwidth ; i++)
    if (double_sgn(VH + i) >= 0)
      mpfi_add_d(norm, norm, VH[i]);
    else
      mpfi_sub_d(norm, norm, VH[i]);
}


void double_bandvec_1norm_fi(mpfi_t norm, const double_bandvec_t V)
{
  _double_bandvec_1norm_fi(norm, V->H, V->D, V->Hwidth, V->Dwidth, V->ind);
}

