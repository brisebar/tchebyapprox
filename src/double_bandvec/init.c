
#include "double_bandvec.h"


void double_bandvec_init(double_bandvec_t V)
{
  V->Hwidth = 0;
  V->Dwidth = 0;
  V->ind = 0;
  V->H = malloc(0);
  V->D = malloc(sizeof(double_t));
  double_init(V->D + 0);
  double_set_zero(V->D + 0, 1);
}
