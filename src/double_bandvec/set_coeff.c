
#include "double_bandvec.h"


void _double_bandvec_set_coeff_si(double_ptr VH, double_ptr VD, long Hwidth, long Dwidth, long ind, long n, long c)
{
  if (n >= 0 && n >= ind-Dwidth && n <= ind+Dwidth)
    double_set_si(VD + Dwidth-ind+n, c, MPFR_RNDN);

  else if (n >= 0 && n < Hwidth)
    double_set_si(VH + n, c, MPFR_RNDN);

  else
    fprintf(stderr, "_double_bandvec_set_coeff_si: error: index out of range\n");
}


void double_bandvec_set_coeff_si(double_bandvec_t V, long n, long c)
{
  _double_bandvec_set_coeff_si(V->H, V->D, V->Hwidth, V->Dwidth, V->ind, n, c);
}


void _double_bandvec_set_coeff_z(double_ptr VH, double_ptr VD, long Hwidth, long Dwidth, long ind, long n, const mpz_t c)
{
  if (n >= 0 && n >= ind-Dwidth && n <= ind+Dwidth)
    double_set_z(VD + Dwidth-ind+n, c, MPFR_RNDN);

  else if (n >= 0 && n < Hwidth)
    double_set_z(VH + n, c, MPFR_RNDN);

  else
    fprintf(stderr, "_double_bandvec_set_coeff_z: error: index out of range\n");
}


void double_bandvec_set_coeff_z(double_bandvec_t V, long n, const mpz_t c)
{
  _double_bandvec_set_coeff_z(V->H, V->D, V->Hwidth, V->Dwidth, V->ind, n, c);
}


void _double_bandvec_set_coeff_q(double_ptr VH, double_ptr VD, long Hwidth, long Dwidth, long ind, long n, const mpq_t c)
{
  if (n >= 0 && n >= ind-Dwidth && n <= ind+Dwidth)
    double_set_q(VD + Dwidth-ind+n, c, MPFR_RNDN);

  else if (n >= 0 && n < Hwidth)
    double_set_q(VH + n, c, MPFR_RNDN);

  else
    fprintf(stderr, "_double_bandvec_set_coeff_q: error: index out of range\n");
}


void double_bandvec_set_coeff_q(double_bandvec_t V, long n, const mpq_t c)
{
  _double_bandvec_set_coeff_q(V->H, V->D, V->Hwidth, V->Dwidth, V->ind, n, c);
}

void _double_bandvec_set_coeff_d(double_ptr VH, double_ptr VD, long Hwidth, long Dwidth, long ind, long n, const double_t c)
{
  if (n >= 0 && n >= ind-Dwidth && n <= ind+Dwidth)
    double_set(VD + Dwidth-ind+n, c, MPFR_RNDN);

  else if (n >= 0 && n < Hwidth)
    double_set(VH + n, c, MPFR_RNDN);

  else
    fprintf(stderr, "_double_bandvec_set_coeff_d: error: index out of range\n");
}


void double_bandvec_set_coeff_d(double_bandvec_t V, long n, const double_t c)
{
  _double_bandvec_set_coeff_d(V->H, V->D, V->Hwidth, V->Dwidth, V->ind, n, c);
}



void _double_bandvec_set_coeff_fr(double_ptr VH, double_ptr VD, long Hwidth, long Dwidth, long ind, long n, const mpfr_t c)
{
  if (n >= 0 && n >= ind-Dwidth && n <= ind+Dwidth)
    double_set_fr(VD + Dwidth-ind+n, c, MPFR_RNDN);

  else if (n >= 0 && n < Hwidth)
    double_set_fr(VH + n, c, MPFR_RNDN);

  else
    fprintf(stderr, "_double_bandvec_set_coeff_fr: error: index out of range\n");
}


void double_bandvec_set_coeff_fr(double_bandvec_t V, long n, const mpfr_t c)
{
  _double_bandvec_set_coeff_fr(V->H, V->D, V->Hwidth, V->Dwidth, V->ind, n, c);
}


