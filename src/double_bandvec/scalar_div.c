
#include "double_bandvec.h"


void _double_bandvec_scalar_div_si(double_ptr VH, double_ptr VD, long VHwidth, long VDwidth, double_srcptr WH, double_srcptr WD, long WHwidth, long WDwidth, long ind, long c)
{
  double d = c;
  _double_bandvec_scalar_div_d(VH, VD, VHwidth, VDwidth, WH, WD, WHwidth, WDwidth, ind, &d);

}


void double_bandvec_scalar_div_si(double_bandvec_t V, const double_bandvec_t W, long c)
{
  if (V != W)
    double_bandvec_set_params(V, W->Hwidth, W->Dwidth, W->ind);

  _double_bandvec_scalar_div_si(V->H, V->D, V->Hwidth, V->Dwidth, W->H, W->D, W->Hwidth, W->Dwidth, V->ind, c);
}


void _double_bandvec_scalar_div_z(double_ptr VH, double_ptr VD, long VHwidth, long VDwidth, double_srcptr WH, double_srcptr WD, long WHwidth, long WDwidth, long ind, const mpz_t c)
{
  double d = mpz_get_d(c);
  _double_bandvec_scalar_div_d(VH, VD, VHwidth, VDwidth, WH, WD, WHwidth, WDwidth, ind, &d);

}


void double_bandvec_scalar_div_z(double_bandvec_t V, const double_bandvec_t W, const mpz_t c)
{
  if (V != W)
    double_bandvec_set_params(V, W->Hwidth, W->Dwidth, W->ind);

  _double_bandvec_scalar_div_z(V->H, V->D, V->Hwidth, V->Dwidth, W->H, W->D, W->Hwidth, W->Dwidth, V->ind, c);
}


void _double_bandvec_scalar_div_q(double_ptr VH, double_ptr VD, long VHwidth, long VDwidth, double_srcptr WH, double_srcptr WD, long WHwidth, long WDwidth, long ind, const mpq_t c)
{
  double d = mpq_get_d(c);
  _double_bandvec_scalar_div_d(VH, VD, VHwidth, VDwidth, WH, WD, WHwidth, WDwidth, ind, &d);

}


void double_bandvec_scalar_div_q(double_bandvec_t V, const double_bandvec_t W, const mpq_t c)
{
  if (V != W)
    double_bandvec_set_params(V, W->Hwidth, W->Dwidth, W->ind);

  _double_bandvec_scalar_div_q(V->H, V->D, V->Hwidth, V->Dwidth, W->H, W->D, W->Hwidth, W->Dwidth, V->ind, c);
}

void _double_bandvec_scalar_div_d(double_ptr VH, double_ptr VD, long VHwidth, long VDwidth, double_srcptr WH, double_srcptr WD, long WHwidth, long WDwidth, long ind, const double_t c)
{
  long i;
  _double_bandvec_set(VH, VD, VHwidth, VDwidth, WH, WD, WHwidth, WDwidth, ind);

  for (i = 0 ; i < VHwidth ; i++)
    double_div(VH + i, VH + i, c, MPFR_RNDN);

  for (i = 0 ; i <= 2*VDwidth ; i++)
    double_div(VD + i, VD + i, c, MPFR_RNDN);
}


void double_bandvec_scalar_div_d(double_bandvec_t V, const double_bandvec_t W, const double_t c)
{
  if (V != W)
    double_bandvec_set_params(V, W->Hwidth, W->Dwidth, W->ind);

  _double_bandvec_scalar_div_d(V->H, V->D, V->Hwidth, V->Dwidth, W->H, W->D, W->Hwidth, W->Dwidth, V->ind, c);
}


void _double_bandvec_scalar_div_fr(double_ptr VH, double_ptr VD, long VHwidth, long VDwidth, double_srcptr WH, double_srcptr WD, long WHwidth, long WDwidth, long ind, const mpfr_t c)
{
  double d = mpfr_get_d(c, MPFR_RNDN);
  _double_bandvec_scalar_div_d(VH, VD, VHwidth, VDwidth, WH, WD, WHwidth, WDwidth, ind, &d);
}


void double_bandvec_scalar_div_fr(double_bandvec_t V, const double_bandvec_t W, const mpfr_t c)
{
  if (V != W)
    double_bandvec_set_params(V, W->Hwidth, W->Dwidth, W->ind);

  _double_bandvec_scalar_div_fr(V->H, V->D, V->Hwidth, V->Dwidth, W->H, W->D, W->Hwidth, W->Dwidth, V->ind, c);
}


void _double_bandvec_scalar_div_2si(double_ptr VH, double_ptr VD, long VHwidth, long VDwidth, double_srcptr WH, double_srcptr WD, long WHwidth, long WDwidth, long ind, long k)
{
   _double_bandvec_set(VH, VD, VHwidth, VDwidth, WH, WD, WHwidth, WDwidth, ind);
long i;

for (i = 0 ; i < VHwidth ; i++)
  double_mul_2si(VH + i, VH + i, -k, MPFR_RNDN);

for (i = 0 ; i <= 2*VDwidth ; i++)
  double_mul_2si(VD + i, VD + i, -k, MPFR_RNDN);
}


void double_bandvec_scalar_div_2si(double_bandvec_t V, const double_bandvec_t W, long k)
{
if (V != W)
    double_bandvec_set_params(V, W->Hwidth, W->Dwidth, W->ind);

  _double_bandvec_scalar_div_2si(V->H, V->D, V->Hwidth, V->Dwidth, W->H, W->D, W->Hwidth, W->Dwidth, V->ind, k);
}




