
#include "double_bandvec.h"


void _double_bandvec_set(double_ptr VH, double_ptr VD, long VHwidth, long VDwidth, double_srcptr WH, double_srcptr WD, long WHwidth, long WDwidth, long ind)
{
  if (VHwidth < VDwidth) {
    fprintf(stderr, "_double_bandvec_set: error: Hwidth of result too small\n");
    return;
  }

  else if (VDwidth < WDwidth) {
    fprintf(stderr, "_double_bandvec_set: error: Dwidth of result too small\n");
    return;
  }

  else {
    long i;
    for (i = 0 ; i < WHwidth ; i++)
      double_set(VH + i, WH + i, MPFR_RNDN);
    for (i = WHwidth ; i < VHwidth ; i++)
      double_set_zero(VH + i, 1);
    for (i = -WDwidth ; i <= WDwidth ; i++)
      double_set(VD + VDwidth+i, WD + WDwidth+i, MPFR_RNDN);
    for (i = WDwidth+1 ; i <= VDwidth ; i++) {
      double_set_zero(VD + VDwidth+i, 1);
      double_set_zero(VD + VDwidth-i, 1);
    }
    _double_bandvec_normalise(VH, VD, VHwidth, VDwidth, ind);
  }
}


void double_bandvec_set(double_bandvec_t V, const double_bandvec_t W)
{
  if (V != W) {
    double_bandvec_set_params(V, W->Hwidth, W->Dwidth, W->ind);
    long i;
    for (i = 0 ; i < W->Hwidth ; i++)
      double_set(V->H + i, W->H + i, MPFR_RNDN);
    for (i = 0 ; i <= 2*W->Dwidth ; i++)
      double_set(V->D + i, W->D + i, MPFR_RNDN);
  }
}
