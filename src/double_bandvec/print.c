
#include "double_bandvec.h"


void _double_bandvec_print(double_srcptr VH, double_srcptr VD, long VHwidth, long VDwidth, long ind)
{
  long i, imin, imax;
  printf("[");

  imax = ind-VDwidth < VHwidth ? ind-VDwidth : VHwidth;
  for (i = 0 ; i < imax ; i++) {
    printf("%.10e", VH[i]);
    if (i < imax-1)
      printf(", ");
    else
      printf(" || ");
  }

  imin = ind-VDwidth < 0 ? 0 : ind-VDwidth;
  for (i = imin ; i <= ind+VDwidth ; i++) {
    printf("%.10e", VD[VDwidth-ind+i]);
    if (i < ind+VDwidth)
      printf(", ");
  }

  for (i = ind+VDwidth+1 ; i < VHwidth ; i++) {
    if (i == ind+VDwidth+1)
      printf(" || ");
    printf("%.10e", VH[i]);
    if (i < VHwidth-1)
      printf(", ");
  }

  printf("]");
}


void double_bandvec_print(const double_bandvec_t V)
{
  _double_bandvec_print(V->H, V->D, V->Hwidth, V->Dwidth, V->ind);
}


