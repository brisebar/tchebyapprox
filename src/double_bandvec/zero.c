
#include "double_bandvec.h"


void _double_bandvec_zero(double_ptr VH, double_ptr VD, long Hwidth, long Dwidth)
{
  long i;
  for (i = 0 ; i < Hwidth ; i++)
    double_set_zero(VH + i, 1);
  for (i = 0 ; i <= 2*Dwidth ; i++)
    double_set_zero(VD + i, 1);
}


void double_bandvec_zero(double_bandvec_t V)
{
  _double_bandvec_zero(V->H, V->D, V->Hwidth, V->Dwidth);
}



