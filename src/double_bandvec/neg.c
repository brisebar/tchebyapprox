
#include "double_bandvec.h"



void _double_bandvec_neg(double_ptr VH, double_ptr VD, long VHwidth, long VDwidth, double_srcptr WH, double_srcptr WD, long WHwidth, long WDwidth, long ind)
{
  _double_bandvec_set(VH, VD, VHwidth, VDwidth, WH, WD, WHwidth, WDwidth, ind);
  long i;

  for (i = 0 ; i < VHwidth ; i++)
    double_neg(VH + i, VH + i, MPFR_RNDN);

  for (i = 0 ; i <= 2*VDwidth ; i++)
    double_neg(VD + i, VD + i, MPFR_RNDN);
}


void double_bandvec_neg(double_bandvec_t V, const double_bandvec_t W)
{
  if (V != W)
    double_bandvec_set_params(V, W->Hwidth, W->Dwidth, W->ind);

  _double_bandvec_neg(V->H, V->D, V->Hwidth, V->Dwidth, W->H, W->D, W->Hwidth, W->Dwidth, V->ind);
}

