
#include "double_bandvec.h"


void double_bandvec_clear(double_bandvec_t V)
{
  long i;
  for (i = 0 ; i < V->Hwidth ; i++)
    double_clear(V->H + i);
  free(V->H);
  for (i = 0 ; i <= 2*V->Dwidth ; i++)
    double_clear(V->D + i);
  free(V->D);
}
