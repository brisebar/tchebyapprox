
#include "double_bandvec.h"


void double_bandvec_swap(double_bandvec_t V, double_bandvec_t W)
{
  long long_buf = V->Hwidth;
  V->Hwidth = W->Hwidth;
  W->Hwidth = long_buf;

  long_buf = V->Dwidth;
  V->Dwidth = W->Dwidth;
  W->Dwidth = long_buf;

  long_buf = V->ind;
  V->ind = W->ind;
  W->ind = long_buf;

  double_ptr double_ptr_buf = V->H;
  V->H = W->H;
  W->H = double_ptr_buf;

  double_ptr_buf = V->D;
  V->D = W->D;
  W->D = double_ptr_buf;
}

