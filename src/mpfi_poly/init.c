
#include "mpfi_poly.h"


// init with P = 0
void mpfi_poly_init(mpfi_poly_t P)
{
  P->degree = -1;
  P->coeffs = malloc(0);
}
