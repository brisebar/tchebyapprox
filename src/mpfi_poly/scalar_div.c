
#include "mpfi_poly.h"


void _mpfi_poly_scalar_div_si(mpfi_ptr P, mpfi_srcptr Q, long n, long c)
{
  long i;
  for (i = 0 ; i <= n ; i++)
    mpfi_div_si(P + i, Q + i, c);
}


void _mpfi_poly_scalar_div_z(mpfi_ptr P, mpfi_srcptr Q, long n, const mpz_t c)
{
  long i;
  for (i = 0 ; i <= n ; i++)
    mpfi_div_z(P + i, Q + i, c);
}


void _mpfi_poly_scalar_div_q(mpfi_ptr P, mpfi_srcptr Q, long n, const mpq_t c)
{
  long i;
  for (i = 0 ; i <= n ; i++)
    mpfi_div_q(P + i, Q + i, c);
}


void _mpfi_poly_scalar_div_d(mpfi_ptr P, mpfi_srcptr Q, long n, const double_t c)
{
  long i;
  for (i = 0 ; i <= n ; i++)
    mpfi_div_d(P + i, Q + i, *c);
}


void _mpfi_poly_scalar_div_fr(mpfi_ptr P, mpfi_srcptr Q, long n, const mpfr_t c)
{
  long i;
  for (i = 0 ; i <= n ; i++)
    mpfi_div_fr(P + i, Q + i, c);
}


void _mpfi_poly_scalar_div_fi(mpfi_ptr P, mpfi_srcptr Q, long n, const mpfi_t c)
{
  long i;
  for (i = 0 ; i <= n ; i++)
    mpfi_div(P + i, Q + i, c);
}


void _mpfi_poly_scalar_div_2si(mpfi_ptr P, mpfi_srcptr Q, long n, long k)
{
  long i;
  for (i = 0 ; i <= n ; i++)
    mpfi_mul_2si(P + i, Q + i, -k);
}



// set P := (1/c)*Q
void mpfi_poly_scalar_div_si(mpfi_poly_t P, const mpfi_poly_t Q, long c)
{
  long n = Q->degree;
  mpfi_poly_set_degree(P, n);
  _mpfi_poly_scalar_div_si(P->coeffs, Q->coeffs, n, c);
}


// set P := (1/c)*Q
void mpfi_poly_scalar_div_z(mpfi_poly_t P, const mpfi_poly_t Q, const mpz_t c)
{
  long n = Q->degree;
  mpfi_poly_set_degree(P, n);
  _mpfi_poly_scalar_div_z(P->coeffs, Q->coeffs, n, c);
}


// set P := (1/c)*Q
void mpfi_poly_scalar_div_q(mpfi_poly_t P, const mpfi_poly_t Q, const mpq_t c)
{
  long n = Q->degree;
  mpfi_poly_set_degree(P, n);
  _mpfi_poly_scalar_div_q(P->coeffs, Q->coeffs, n, c);
}


// set P := (1/c)*Q
void mpfi_poly_scalar_div_d(mpfi_poly_t P, const mpfi_poly_t Q, const double_t c)
{
  long n = Q->degree;
  mpfi_poly_set_degree(P, n);
  _mpfi_poly_scalar_div_d(P->coeffs, Q->coeffs, n, c);
}


// set P := (1/c)*Q
void mpfi_poly_scalar_div_fr(mpfi_poly_t P, const mpfi_poly_t Q, const mpfr_t c)
{
  long n = Q->degree;
  mpfi_poly_set_degree(P, n);
  _mpfi_poly_scalar_div_fr(P->coeffs, Q->coeffs, n, c);
}


// set P := (1/c)*Q
void mpfi_poly_scalar_div_fi(mpfi_poly_t P, const mpfi_poly_t Q, const mpfi_t c)
{
  long n = Q->degree;
  mpfi_poly_set_degree(P, n);
  _mpfi_poly_scalar_div_fi(P->coeffs, Q->coeffs, n, c);
}


// set P := 2^-k*Q
void mpfi_poly_scalar_div_2si(mpfi_poly_t P, const mpfi_poly_t Q, long k)
{
  long n = Q->degree;
  mpfi_poly_set_degree(P, n);
  _mpfi_poly_scalar_div_2si(P->coeffs, Q->coeffs, n, k);
}
