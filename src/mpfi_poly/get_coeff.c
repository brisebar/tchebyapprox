
#include "mpfi_poly.h"


// get the n-th coefficient of P
void mpfi_poly_get_coeff(mpfi_t c, const mpfi_poly_t P, long n)
{
  if (n < 0 || n > P->degree)
    mpfi_set_si(c, 0);
  else
    mpfi_set(c, P->coeffs + n);
}
