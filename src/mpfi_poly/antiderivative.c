
#include "mpfi_poly.h"


void _mpfi_poly_antiderivative(mpfi_ptr P, mpfi_srcptr Q, long n)
{
  long i;
  for (i = n ; i >= 0 ; i--)
    mpfi_div_si(P + i + 1, Q + i, i+1);
  mpfi_set_si(P, 0);
}


// set P st P' = Q and P(0) = 0
void mpfi_poly_antiderivative(mpfi_poly_t P, const mpfi_poly_t Q)
{
  long n = Q->degree;

  if (n < 0)
    mpfi_poly_zero(P);

  else {
    mpfi_poly_set_degree(P, n+1);
    _mpfi_poly_antiderivative(P->coeffs, Q->coeffs, n);
  }
}
