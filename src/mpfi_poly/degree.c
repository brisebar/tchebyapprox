
#include "mpfi_poly.h"


// get the degree of P
long mpfi_poly_degree(const mpfi_poly_t P)
{
  return P->degree;
}
