
#include "mpfi_poly.h"


void _mpfi_poly_monic(mpfi_ptr P, mpfi_srcptr Q, long n)
{
  long i;
  for (i = 0 ; i < n ; i++)
    mpfi_div(P + i, Q + i, Q + n);
  mpfi_set_si(P + i, 1);
}


// set P to (1/c)*Q where c is the leading coefficient of P
void mpfi_poly_monic(mpfi_poly_t P, const mpfi_poly_t Q)
{
  long n = Q->degree;

  if (n < 0)
    mpfi_poly_zero(P);

  else {
    mpfi_poly_set_degree(P, n);
    _mpfi_poly_monic(P->coeffs, Q->coeffs, n);
  }
}
