
#include "mpfi_poly.h"


// reverse a list of mpfi of size (n+1)
void _mpfi_poly_reverse(mpfi_ptr P, mpfi_srcptr Q, long n)
{
  long i;
  mpfi_ptr R = malloc((n+1) * sizeof(mpfi_t));
  for (i = 0 ; i <= n ; i++) {
    mpfi_init(R + i);
    mpfi_set(R + i, Q + n - i);
  }
  for (i = 0 ; i <= n ; i++) {
    mpfi_swap(P + i, R + i);
    mpfi_clear(R + i);
  }
}


// set P to Q(X^-1)*X^(degree(Q))
void mpfi_poly_reverse(mpfi_poly_t P, const mpfi_poly_t Q)
{
  if (P != Q)
    mpfi_poly_set(P, Q);
  _mpfi_poly_reverse(P->coeffs, P->coeffs, P->degree);
  mpfi_poly_normalise(P);
}
