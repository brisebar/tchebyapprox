
#include "mpfi_poly.h"


// set P := Q^e
void mpfi_poly_pow(mpfi_poly_t P, const mpfi_poly_t Q, unsigned long e)
{
  mpfi_poly_t Qe;
  mpfi_poly_init(Qe);
  mpfi_poly_set_coeff_si(Qe, 0, 1);
  mpfi_poly_t Qi;
  mpfi_poly_init(Qi);
  mpfi_poly_set_coeff_si(Qi, 0, 1);

  while (e != 0) {
    mpfi_poly_mul(Qi, Qi, Q);
    if (e%2 == 1)
      mpfi_poly_mul(Qe, Qe, Qi);
    e /= 2;
  }

  mpfi_poly_clear(Qi);
  mpfi_poly_clear(P);
  P->degree = Qe->degree;
  P->coeffs = Qe->coeffs;
}
