
#include "mpfi_poly.h"


void _mpfi_poly_mul(mpfi_ptr P, mpfi_srcptr Q, long n, mpfi_srcptr R, long m)
{
  long i, j;
  mpfi_ptr S = malloc((n+m+1) * sizeof(mpfi_t));
  mpfi_t temp;
  mpfi_init(temp);

  for (i = 0 ; i <= n+m ; i++) {
    mpfi_init(S + i);
    mpfi_set_si(S + i, 0);
  }

  for (i = 0 ; i <= n ; i++)
    for (j = 0 ; j <= m ; j++) {
      mpfi_mul(temp, Q + i, R + j);
      mpfi_add(S + i + j, S + i + j, temp);
    }

  for (i = 0 ; i <= n+m ; i++) {
    mpfi_swap(P + i, S + i);
    mpfi_clear(S + i);
  }

  mpfi_clear(temp);
  free(S);
}


// set P := Q * R
void mpfi_poly_mul(mpfi_poly_t P, const mpfi_poly_t Q, const mpfi_poly_t R)
{
  long n = Q->degree;
  long m = R->degree;

  if (n < 0 || m < 0)
    mpfi_poly_zero(P);

  else {
    mpfi_poly_set_degree(P, n+m);
    _mpfi_poly_mul(P->coeffs, Q->coeffs, n, R->coeffs, m);
  }
}
