
#include "mpfi_poly.h"


void _mpfi_poly_scalar_mul_si(mpfi_ptr P, mpfi_srcptr Q, long n, long c)
{
  long i;
  for (i = 0 ; i <= n ; i++)
    mpfi_mul_si(P + i, Q + i, c);
}


void _mpfi_poly_scalar_mul_z(mpfi_ptr P, mpfi_srcptr Q, long n, const mpz_t c)
{
  long i;
  for (i = 0 ; i <= n ; i++)
    mpfi_mul_z(P + i, Q + i, c);
}


void _mpfi_poly_scalar_mul_q(mpfi_ptr P, mpfi_srcptr Q, long n, const mpq_t c)
{
  long i;
  for (i = 0 ; i <= n ; i++)
    mpfi_mul_q(P + i, Q + i, c);
}


void _mpfi_poly_scalar_mul_d(mpfi_ptr P, mpfi_srcptr Q, long n, const double_t c)
{
  long i;
  for (i = 0 ; i <= n ; i++)
    mpfi_mul_d(P + i, Q + i, *c);
}


void _mpfi_poly_scalar_mul_fr(mpfi_ptr P, mpfi_srcptr Q, long n, const mpfr_t c)
{
  long i;
  for (i = 0 ; i <= n ; i++)
    mpfi_mul_fr(P + i, Q + i, c);
}


void _mpfi_poly_scalar_mul_fi(mpfi_ptr P, mpfi_srcptr Q, long n, const mpfi_t c)
{
  long i;
  for (i = 0 ; i <= n ; i++)
    mpfi_mul(P + i, Q + i, c);
}


void _mpfi_poly_scalar_mul_2si(mpfi_ptr P, mpfi_srcptr Q, long n, long k)
{
  long i;
  for (i = 0 ; i <= n ; i++)
    mpfi_mul_2si(P + i, Q + i, k);
}



// set P := c*Q
void mpfi_poly_scalar_mul_si(mpfi_poly_t P, const mpfi_poly_t Q, long c)
{
  if (c == 0)
    mpfi_poly_zero(P);
  else {
    long n = Q->degree;
    mpfi_poly_set_degree(P, n);
    _mpfi_poly_scalar_mul_si(P->coeffs, Q->coeffs, n, c);
  }
}


// set P := c*Q
void mpfi_poly_scalar_mul_z(mpfi_poly_t P, const mpfi_poly_t Q, const mpz_t c)
{
  if (!mpz_sgn(c))
    mpfi_poly_zero(P);
  else {
    long n = Q->degree;
    mpfi_poly_set_degree(P, n);
    _mpfi_poly_scalar_mul_z(P->coeffs, Q->coeffs, n, c);
  }
}


// set P := c*Q
void mpfi_poly_scalar_mul_q(mpfi_poly_t P, const mpfi_poly_t Q, const mpq_t c)
{
  if (!mpq_sgn(c))
    mpfi_poly_zero(P);
  else {
    long n = Q->degree;
    mpfi_poly_set_degree(P, n);
    _mpfi_poly_scalar_mul_q(P->coeffs, Q->coeffs, n, c);
  }
}


// set P := c*Q
void mpfi_poly_scalar_mul_d(mpfi_poly_t P, const mpfi_poly_t Q, const double_t c)
{
  if (*c == 0)
    mpfi_poly_zero(P);
  else {
    long n = Q->degree;
    mpfi_poly_set_degree(P, n);
    _mpfi_poly_scalar_mul_d(P->coeffs, Q->coeffs, n, c);
  }
}


// set P := c*Q
void mpfi_poly_scalar_mul_fr(mpfi_poly_t P, const mpfi_poly_t Q, const mpfr_t c)
{
  if (mpfr_zero_p(c))
    mpfi_poly_zero(P);
  else {
    long n = Q->degree;
    mpfi_poly_set_degree(P, n);
    _mpfi_poly_scalar_mul_fr(P->coeffs, Q->coeffs, n, c);
  }
}


// set P := c*Q
void mpfi_poly_scalar_mul_fi(mpfi_poly_t P, const mpfi_poly_t Q, const mpfi_t c)
{
  if (mpfi_is_zero(c))
    mpfi_poly_zero(P);
  else {
    long n = Q->degree;
    mpfi_poly_set_degree(P, n);
    _mpfi_poly_scalar_mul_fi(P->coeffs, Q->coeffs, n, c);
  }
}


// set P := 2^k*Q
void mpfi_poly_scalar_mul_2si(mpfi_poly_t P, const mpfi_poly_t Q, long k)
{
  long n = Q->degree;

  if (n < 0)
    mpfi_poly_zero(P);

  else {
    mpfi_poly_set_degree(P, n);
    _mpfi_poly_scalar_mul_2si(P->coeffs, Q->coeffs, n, k);
  }
}
