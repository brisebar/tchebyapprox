
#include "mpfi_poly.h"




// set the n-th coefficient of P to c
void mpfi_poly_set_coeff_si(mpfi_poly_t P, long n, long c)
{
  if (n < 0)
    return;

  else if (n <= P->degree) {
    mpfi_set_si(P->coeffs + n, c);
    if (n == P->degree && c == 0)
      mpfi_poly_normalise(P);
  }

  else if (c != 0) {
    P->coeffs = realloc(P->coeffs, (n+1) * sizeof(mpfi_t));
    long i;
    for (i = P->degree + 1 ; i <= n ; i++) {
      mpfi_init(P->coeffs + i);
      mpfi_set_si(P->coeffs + i, 0);
    }
    mpfi_set_si(P->coeffs + n, c);
    P->degree = n;
  }

  else
    return;
}


// set the n-th coefficient of P to c
void mpfi_poly_set_coeff_z(mpfi_poly_t P, long n, const mpz_t c)
{
  if (n < 0)
    return;

  else if (n <= P->degree) {
    mpfi_set_z(P->coeffs + n, c);
    if (n == P->degree && !mpz_sgn(c))
      mpfi_poly_normalise(P);
  }

  else if (mpz_sgn(c)) {
    P->coeffs = realloc(P->coeffs, (n+1) * sizeof(mpfi_t));
    long i;
    for (i = P->degree + 1 ; i <= n ; i++) {
      mpfi_init(P->coeffs + i);
      mpfi_set_si(P->coeffs + i, 0);
    }
    mpfi_set_z(P->coeffs + n, c);
    P->degree = n;
  }

  else
    return;
}


// set the n-th coefficient of P to c
void mpfi_poly_set_coeff_q(mpfi_poly_t P, long n, const mpq_t c)
{
  if (n < 0)
    return;

  else if (n <= P->degree) {
    mpfi_set_q(P->coeffs + n, c);
    if (n == P->degree && !mpq_sgn(c))
      mpfi_poly_normalise(P);
  }

  else if (mpq_sgn(c)) {
    P->coeffs = realloc(P->coeffs, (n+1) * sizeof(mpfi_t));
    long i;
    for (i = P->degree + 1 ; i <= n ; i++) {
      mpfi_init(P->coeffs + i);
      mpfi_set_si(P->coeffs + i, 0);
    }
    mpfi_set_q(P->coeffs + n, c);
    P->degree = n;
  }

  else
    return;
}


// set the n-th coefficient of P to c
void mpfi_poly_set_coeff_fr(mpfi_poly_t P, long n, const mpfr_t c)
{
  if (n < 0)
    return;

  else if (n <= P->degree) {
    mpfi_set_fr(P->coeffs + n, c);
    if (n == P->degree && mpfr_zero_p(c))
      mpfi_poly_normalise(P);
  }

  else if (!mpfr_zero_p(c)) {
    P->coeffs = realloc(P->coeffs, (n+1) * sizeof(mpfi_t));
    long i;
    for (i = P->degree + 1 ; i <= n ; i++) {
      mpfi_init(P->coeffs + i);
      mpfi_set_si(P->coeffs + i, 0);
    }
    mpfi_set_fr(P->coeffs + n, c);
    P->degree = n;
  }

  else
    return;
}


// set the n-th coefficient of P to c
void mpfi_poly_set_coeff_fi(mpfi_poly_t P, long n, const mpfi_t c)
{
  if (n < 0)
    return;

  else if (n <= P->degree) {
    mpfi_set(P->coeffs + n, c);
    if (n == P->degree && mpfi_is_zero(c))
      mpfi_poly_normalise(P);
  }

  else if (!mpfi_is_zero(c)) {
    P->coeffs = realloc(P->coeffs, (n+1) * sizeof(mpfi_t));
    long i;
    for (i = P->degree + 1 ; i <= n ; i++) {
      mpfi_init(P->coeffs + i);
      mpfi_set_si(P->coeffs + i, 0);
    }
    mpfi_set(P->coeffs + n, c);
    P->degree = n;
  }

  else
    return;
}


