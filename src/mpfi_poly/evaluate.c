
#include "mpfi_poly.h"


void _mpfi_poly_evaluate_si(mpfi_t y, mpfi_srcptr P, long n, long x)
{
  mpfi_t t;
  mpfi_init(t);
  mpfi_set_si(t, x);
  _mpfi_poly_evaluate_fi(y, P, n, t);
  mpfi_clear(t);
}


void _mpfi_poly_evaluate_z(mpfi_t y, mpfi_srcptr P, long n, const mpz_t x)
{
  mpfi_t t;
  mpfi_init(t);
  mpfi_set_z(t, x);
  _mpfi_poly_evaluate_fi(y, P, n, t);
  mpfi_clear(t);
}


void _mpfi_poly_evaluate_q(mpfi_t y, mpfi_srcptr P, long n, const mpq_t x)
{
  mpfi_t t;
  mpfi_init(t);
  mpfi_set_q(t, x);
  _mpfi_poly_evaluate_fi(y, P, n, t);
  mpfi_clear(t);
}


void _mpfi_poly_evaluate_fr(mpfi_t y, mpfi_srcptr P, long n, const mpfr_t x)
{
  mpfi_t t;
  mpfi_init(t);
  mpfi_set_fr(t, x);
  _mpfi_poly_evaluate_fi(y, P, n, t);
  mpfi_clear(t);
}


void _mpfi_poly_evaluate_fi(mpfi_t y, mpfi_srcptr P, long n, const mpfi_t x)
{
  if (n < 0)
    mpfi_set_si(y, 0);

  else {
    long i;
    mpfi_set(y, P + n);
    for (i = n-1 ; i >= 0 ; i--) {
      mpfi_mul(y, x, y);
      mpfi_add(y, y, P + i);
    }
  }
}



// set y to P(x)
void mpfi_poly_evaluate_si(mpfi_t y, const mpfi_poly_t P, long x)
{
  _mpfi_poly_evaluate_si(y, P->coeffs, P->degree, x);
}


// set y to P(x)
void mpfi_poly_evaluate_z(mpfi_t y, const mpfi_poly_t P, const mpz_t x)
{
  _mpfi_poly_evaluate_z(y, P->coeffs, P->degree, x);
}


// set y to P(x)
void mpfi_poly_evaluate_q(mpfi_t y, const mpfi_poly_t P, const mpq_t x)
{
  _mpfi_poly_evaluate_q(y, P->coeffs, P->degree, x);
}


// set y to P(x)
void mpfi_poly_evaluate_fr(mpfi_t y, const mpfi_poly_t P, const mpfr_t x)
{
  _mpfi_poly_evaluate_fr(y, P->coeffs, P->degree, x);
}


// set y to P(x)
void mpfi_poly_evaluate_fi(mpfi_t y, const mpfi_poly_t P, const mpfi_t x)
{
  _mpfi_poly_evaluate_fi(y, P->coeffs, P->degree, x);
}
