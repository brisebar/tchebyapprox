
#include "mpfi_poly.h"


int _mpfi_poly_equal(mpfi_srcptr P, mpfi_srcptr Q, long n)
{
  long i;
  for (i = 0 ; i <= n ; i++)
    if (!mpfr_equal_p(&((P + i)->left), &((Q + i)->left)) || !mpfr_equal_p(&((P + i)->right), &((Q + i)->right)))
      break;
  return (i == n+1);
}


int mpfi_poly_equal(const mpfi_poly_t P, const mpfi_poly_t Q)
{
  return (P->degree == Q->degree && _mpfi_poly_equal(P->coeffs, Q->coeffs, P->degree));
}
