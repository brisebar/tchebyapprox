
#include "mpfi_poly.h"


void _mpfi_poly_get_mpfr_poly(mpfr_ptr P, mpfi_srcptr Q, long n)
{
  long i;
  for (i = 0 ; i <= n ; i++)
    mpfi_mid(P + i, Q + i);
}

// coefficients of P are set to the middle of those of Q
void mpfi_poly_get_mpfr_poly(mpfr_poly_t P, const mpfi_poly_t Q)
{
  long n = Q->degree;

  if (n < 0)
    mpfr_poly_zero(P);

  else {
    mpfr_poly_set_degree(P, n);
    _mpfi_poly_get_mpfr_poly(P->coeffs, Q->coeffs, n);
  }
}
