
#include "mpfi_poly.h"


void _mpfi_poly_get_double_poly(double_ptr P, mpfi_srcptr Q, long n)
{
  mpfr_t temp;
  mpfr_init(temp);
  long i;
  for (i = 0 ; i <= n ; i++) {
    mpfi_mid(temp, Q + i);
    double_set_fr(P + i, temp, MPFR_RNDN);
  }
  mpfr_clear(temp);
}

// coefficients of P are set to the middle of those of Q
void mpfi_poly_get_double_poly(double_poly_t P, const mpfi_poly_t Q)
{
  long n = Q->degree;

  if (n < 0)
    double_poly_zero(P);

  else {
    double_poly_set_degree(P, n);
    _mpfi_poly_get_double_poly(P->coeffs, Q->coeffs, n);
  }
}
