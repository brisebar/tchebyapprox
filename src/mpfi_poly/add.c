
#include "mpfi_poly.h"


void _mpfi_poly_add(mpfi_ptr P, mpfi_srcptr Q, long n, mpfi_srcptr R, long m)
{
  long min_n_m = n < m ? n : m;
  long i;

  for (i = 0 ; i <= min_n_m ; i++)
    mpfi_add(P + i, Q + i, R + i);

  if (P != Q)
    for (i = min_n_m+1 ; i <= n ; i++)
      mpfi_set(P + i, Q + i);

  if (P != R)
    for (i = min_n_m+1 ; i <= m ; i++)
      mpfi_set(P + i, R + i);
}


// set P := Q + R
void mpfi_poly_add(mpfi_poly_t P, const mpfi_poly_t Q, const mpfi_poly_t R)
{
  long n = Q->degree;
  long m = R->degree;
  long max_n_m = n < m ? m : n;

  mpfi_poly_set_degree(P, max_n_m);
  _mpfi_poly_add(P->coeffs, Q->coeffs, n, R->coeffs, m);
  mpfi_poly_normalise(P);
}
