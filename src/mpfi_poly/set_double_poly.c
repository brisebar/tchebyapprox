
#include "mpfi_poly.h"


void _mpfi_poly_set_double_poly(mpfi_ptr P, double_srcptr Q, long n)
{
  long i ;
  for (i = 0 ; i <= n ; i++)
    mpfi_set_d(P + i, Q[i]);
}


// copy Q into P
void mpfi_poly_set_double_poly(mpfi_poly_t P, const double_poly_t Q)
{
  mpfi_poly_set_degree(P, Q->degree);
  _mpfi_poly_set_double_poly(P->coeffs, Q->coeffs, Q->degree);
}
