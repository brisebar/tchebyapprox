
#include "mpfi_poly.h"


void _mpfi_poly_shift_left(mpfi_ptr P, mpfi_srcptr Q, long n, unsigned long k)
{
  long i;
  for (i = n ; i >= 0 ; i--)
    mpfi_set(P + i + k, Q + i);
  for (i = 0 ; i < k ; i++)
    mpfi_set_si(P + i, 0);
}


// set P to Q*X^k
void mpfi_poly_shift_left(mpfi_poly_t P, const mpfi_poly_t Q, unsigned long k)
{
  long i;
  long n = Q->degree;
  mpfi_ptr Pcoeffs = malloc((n+k+1) * sizeof(mpfi_t));
  for (i = 0 ; i <= n+k ; i++)
    mpfi_init(Pcoeffs + i);
  _mpfi_poly_shift_left(Pcoeffs, Q->coeffs, n, k);
  mpfi_poly_clear(P);
  P->degree = n+k;
  P->coeffs = Pcoeffs;
}


void _mpfi_poly_shift_right(mpfi_ptr P, mpfi_srcptr Q, long n, unsigned long k)
{
  long i;
  for (i = 0 ; i+k <= n ; i++)
    mpfi_set(P + i, Q + i + k);
}


// set P to Q/X^k
void mpfi_poly_shift_right(mpfi_poly_t P, const mpfi_poly_t Q, unsigned long k)
{
  long i;
  long n = Q->degree;
  if (k > n)
    mpfi_poly_zero(P);
  else {
    mpfi_ptr Pcoeffs = malloc((n-k+1) * sizeof(mpfi_t));
    for (i = 0 ; i <= n-k ; i++)
      mpfi_init(Pcoeffs + i);
    _mpfi_poly_shift_right(Pcoeffs, Q->coeffs, n, k);
    mpfi_poly_clear(P);
    P->degree = n-k;
    P->coeffs = Pcoeffs;
  }
}
