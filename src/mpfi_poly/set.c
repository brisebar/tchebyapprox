
#include "mpfi_poly.h"


void _mpfi_poly_set(mpfi_ptr P, mpfi_srcptr Q, long n)
{
  long i ;
  for (i = 0 ; i <= n ; i++)
    mpfi_set(P + i, Q + i);
}


// copy Q into P
void mpfi_poly_set(mpfi_poly_t P, const mpfi_poly_t Q)
{
  long n = Q->degree;
  long i;
  if (P != Q) {
    mpfi_poly_set_degree(P, n);
    for (i = 0 ; i <= n ; i++)
      mpfi_set(P->coeffs + i, Q->coeffs + i);
  }
}
