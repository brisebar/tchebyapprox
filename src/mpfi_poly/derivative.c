
#include "mpfi_poly.h"


void _mpfi_poly_derivative(mpfi_ptr P, mpfi_srcptr Q, long n)
{
  long i;
  for (i = 0 ; i < n ; i++)
    mpfi_mul_si(P + i, Q + i + 1, i+1);
}


// set P := Q'
void mpfi_poly_derivative(mpfi_poly_t P, const mpfi_poly_t Q)
{
  long n = Q->degree;

  if (n <= 0)
    mpfi_poly_zero(P);

  else {
    mpfi_poly_set_degree(P, n);
    _mpfi_poly_derivative(P->coeffs, Q->coeffs, n);
    mpfi_clear(P->coeffs + n);
    P->degree --;
  }
}
