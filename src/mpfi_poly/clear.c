#include "mpfi_poly.h"


// clear P
void mpfi_poly_clear(mpfi_poly_t P)
{
  long n = P->degree;
  long i;
  for (i = 0 ; i <= n ; i++)
    mpfi_clear(P->coeffs + i);
  free(P->coeffs);
}
