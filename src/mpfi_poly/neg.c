
#include "mpfi_poly.h"


void _mpfi_poly_neg(mpfi_ptr P, mpfi_srcptr Q, long n)
{
  long i;
  for (i = 0 ; i <= n ; i++)
    mpfi_neg(P + i, Q + i);
}


// set P := -Q
void mpfi_poly_neg(mpfi_poly_t P, const mpfi_poly_t Q)
{
  mpfi_poly_set_degree(P, Q->degree);
  _mpfi_poly_neg(P->coeffs, Q->coeffs, Q->degree);
}
