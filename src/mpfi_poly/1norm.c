
#include "mpfi_poly.h"


void _mpfi_poly_1norm(mpfi_t y, mpfi_srcptr P, long n)
{
  mpfi_t temp;
  mpfi_init(temp);
  long i;
  mpfi_set_si(y, 0);
  for (i = 0 ; i <= n ; i++) {
    mpfi_abs(temp, P + i);
    mpfi_add(y, y, temp);
  }
  mpfi_clear(temp);
}


void _mpfi_poly_1norm_ubound(mpfr_t y, mpfi_srcptr P, long n)
{
  mpfi_t temp;
  mpfi_init(temp);
  long i;
  mpfr_set_si(y, 0, MPFR_RNDN);
  for (i = 0 ; i <= n ; i++) {
    mpfi_abs(temp, P + i);
    mpfr_add(y, y, &(temp->right), MPFR_RNDU);
  }
  mpfi_clear(temp);
}


void _mpfi_poly_1norm_lbound(mpfr_t y, mpfi_srcptr P, long n)
{
  mpfi_t temp;
  mpfi_init(temp);
  long i;
  mpfr_set_si(y, 0, MPFR_RNDN);
  for (i = 0 ; i <= n ; i++) {
    mpfi_abs(temp, P + i);
    mpfr_add(y, y, &(temp->left), MPFR_RNDD);
  }
  mpfi_clear(temp);
}


void mpfi_poly_1norm(mpfi_t y, const mpfi_poly_t P)
{
  _mpfi_poly_1norm(y, P->coeffs, P->degree);
}


void mpfi_poly_1norm_ubound(mpfr_t y, const mpfi_poly_t P)
{
  _mpfi_poly_1norm_ubound(y, P->coeffs, P->degree);
}


void mpfi_poly_1norm_lbound(mpfr_t y, const mpfi_poly_t P)
{
  _mpfi_poly_1norm_lbound(y, P->coeffs, P->degree);
}
