
#include "mpfr_chebpoly_vec.h"


void mpfr_chebpoly_vec_swap(mpfr_chebpoly_vec_t P, mpfr_chebpoly_vec_t Q)
{
  long dim_buf = P->dim;
  mpfr_chebpoly_ptr poly_buf = P->poly;
  P->dim = Q->dim;
  P->poly = Q->poly;
  Q->dim = dim_buf;
  Q->poly = poly_buf;
}
