
#include "mpfr_chebpoly_vec.h"


long mpfr_chebpoly_vec_degree(const mpfr_chebpoly_vec_t P)
{
  long deg = -1;
  long n = P->dim;
  long i;
  for (i = 0 ; i < n ; i++)
    if (P->poly[i].degree > deg)
      deg = P->poly[i].degree;
  return deg;
}
