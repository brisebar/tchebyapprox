
#include "mpfr_chebpoly_vec.h"


void mpfr_chebpoly_vec_scalar_prod(mpfr_chebpoly_t P, const mpfr_chebpoly_vec_t Q, const mpfr_chebpoly_vec_t R)
{
  long n = Q->dim;
  long i;

  if (n != R->dim) {
    fprintf(stderr, "mpfr_chebpoly_vec_scalar_prod: error: incompatible dimensions\n");
    return;
  }

  mpfr_chebpoly_zero(P);
  mpfr_chebpoly_t Temp;
  mpfr_chebpoly_init(Temp);
  for (i = 0 ; i < n ; i++) {
    mpfr_chebpoly_mul(Temp, Q->poly + i, R->poly + i);
    mpfr_chebpoly_add(P, P, Temp);
  }

  mpfr_chebpoly_clear(Temp);
}

