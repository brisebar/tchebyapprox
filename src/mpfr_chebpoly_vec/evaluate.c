
#include "mpfr_chebpoly_vec.h"


void mpfr_chebpoly_vec_evaluate_d(mpfr_vec_t Y, const mpfr_chebpoly_vec_t P, const double_t x)
{
  long n = P->dim;
  long i;
  mpfr_vec_set_length(Y, n);
  for (i = 0 ; i < n ; i++)
    mpfr_chebpoly_evaluate_d(Y->coeffs + i, P->poly + i, x);
}


void mpfr_chebpoly_vec_evaluate_fr(mpfr_vec_t Y, const mpfr_chebpoly_vec_t P, const mpfr_t x)
{
  long n = P->dim;
  long i;
  mpfr_vec_set_length(Y, n);
  for (i = 0 ; i < n ; i++)
    mpfr_chebpoly_evaluate_fr(Y->coeffs + i, P->poly + i, x);
}


void mpfr_chebpoly_vec_evaluate_fi(mpfi_vec_t Y, const mpfr_chebpoly_vec_t P, const mpfi_t x)
{
  long n = P->dim;
  long i;
  mpfi_vec_set_length(Y, n);
  for (i = 0 ; i < n ; i++)
    mpfr_chebpoly_evaluate_fi(Y->coeffs + i, P->poly + i, x);
}
