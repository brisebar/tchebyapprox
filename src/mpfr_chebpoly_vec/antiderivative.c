
#include "mpfr_chebpoly_vec.h"


void mpfr_chebpoly_vec_antiderivative(mpfr_chebpoly_vec_t P, const mpfr_chebpoly_vec_t Q)
{
  long n = Q->dim;
  long i;
  mpfr_chebpoly_vec_set_dim(P, n);
  for (i = 0 ; i < n ; i++)
    mpfr_chebpoly_antiderivative(P->poly + i, Q->poly + i);
}


void mpfr_chebpoly_vec_antiderivative0(mpfr_chebpoly_vec_t P, const mpfr_chebpoly_vec_t Q)
{
  long n = Q->dim;
  long i;
  mpfr_chebpoly_vec_set_dim(P, n);
  for (i = 0 ; i < n ; i++)
    mpfr_chebpoly_antiderivative0(P->poly + i, Q->poly + i);
}


void mpfr_chebpoly_vec_antiderivative_1(mpfr_chebpoly_vec_t P, const mpfr_chebpoly_vec_t Q)
{
  long n = Q->dim;
  long i;
  mpfr_chebpoly_vec_set_dim(P, n);
  for (i = 0 ; i < n ; i++)
    mpfr_chebpoly_antiderivative_1(P->poly + i, Q->poly + i);
}


void mpfr_chebpoly_vec_antiderivative1(mpfr_chebpoly_vec_t P, const mpfr_chebpoly_vec_t Q)
{
  long n = Q->dim;
  long i;
  mpfr_chebpoly_vec_set_dim(P, n);
  for (i = 0 ; i < n ; i++)
    mpfr_chebpoly_antiderivative1(P->poly + i, Q->poly + i);
}


