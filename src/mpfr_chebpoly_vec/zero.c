
#include "mpfr_chebpoly_vec.h"


void mpfr_chebpoly_vec_zero(mpfr_chebpoly_vec_t P)
{
  long n = P->dim;
  long i;
  for (i = 0 ; i < n ; i++)
    mpfr_chebpoly_zero(P->poly + i);
}
