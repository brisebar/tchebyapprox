
#include "mpfr_chebpoly_vec.h"


void mpfr_chebpoly_vec_neg(mpfr_chebpoly_vec_t P, const mpfr_chebpoly_vec_t Q)
{
  long n = Q->dim;
  long i;
  mpfr_chebpoly_vec_set_dim(P, n);
  for (i = 0 ; i < n ; i++)
    mpfr_chebpoly_neg(P->poly + i, Q->poly + i);
}

