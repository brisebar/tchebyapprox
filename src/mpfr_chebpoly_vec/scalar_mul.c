
#include "mpfr_chebpoly_vec.h"


void mpfr_chebpoly_vec_scalar_mul_si(mpfr_chebpoly_vec_t P, const mpfr_chebpoly_vec_t Q, long c)
{
  long n = Q->dim;
  long i;
  mpfr_chebpoly_vec_set_dim(P, n);
  for (i = 0 ; i < n ; i++)
    mpfr_chebpoly_scalar_mul_si(P->poly + i, Q->poly + i, c);
}


void mpfr_chebpoly_vec_scalar_mul_z(mpfr_chebpoly_vec_t P, const mpfr_chebpoly_vec_t Q, const mpz_t c)
{
  long n = Q->dim;
  long i;
  mpfr_chebpoly_vec_set_dim(P, n);
  for (i = 0 ; i < n ; i++)
    mpfr_chebpoly_scalar_mul_z(P->poly + i, Q->poly + i, c);
}


void mpfr_chebpoly_vec_scalar_mul_q(mpfr_chebpoly_vec_t P, const mpfr_chebpoly_vec_t Q, const mpq_t c)
{
  long n = Q->dim;
  long i;
  mpfr_chebpoly_vec_set_dim(P, n);
  for (i = 0 ; i < n ; i++)
    mpfr_chebpoly_scalar_mul_q(P->poly + i, Q->poly + i, c);
}


void mpfr_chebpoly_vec_scalar_mul_fr(mpfr_chebpoly_vec_t P, const mpfr_chebpoly_vec_t Q, const mpfr_t c)
{
  long n = Q->dim;
  long i;
  mpfr_chebpoly_vec_set_dim(P, n);
  for (i = 0 ; i < n ; i++)
    mpfr_chebpoly_scalar_mul_fr(P->poly + i, Q->poly + i, c);
}


void mpfr_chebpoly_vec_scalar_mul_2si(mpfr_chebpoly_vec_t P, const mpfr_chebpoly_vec_t Q, long k)
{
  long n = Q->dim;
  long i;
  mpfr_chebpoly_vec_set_dim(P, n);
  for (i = 0 ; i < n ; i++)
    mpfr_chebpoly_scalar_mul_2si(P->poly + i, Q->poly + i, k);
}



