
#include "mpfr_chebpoly_vec.h"


void mpfr_chebpoly_vec_clear(mpfr_chebpoly_vec_t P)
{
  long i;
  long n = P->dim;
  for (i = 0 ; i < n ; i++)
    mpfr_chebpoly_clear(P->poly + i);
  free(P->poly);
}
