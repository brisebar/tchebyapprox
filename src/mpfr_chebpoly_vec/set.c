
#include "mpfr_chebpoly_vec.h"


void mpfr_chebpoly_vec_set(mpfr_chebpoly_vec_t P, const mpfr_chebpoly_vec_t Q)
{
  long n = Q->dim;
  long i;
  if (P != Q) {
    mpfr_chebpoly_vec_set_dim(P, n);
    for (i = 0 ; i < n ; i++)
      mpfr_chebpoly_set(P->poly + i, Q->poly + i);
  }
}


void mpfr_chebpoly_vec_set_double_chebpoly_vec(mpfr_chebpoly_vec_t P, const double_chebpoly_vec_t Q)
{
  long n = Q->dim;
  long i;
  mpfr_chebpoly_vec_set_dim(P, n);
  for (i = 0 ; i < n ; i++)
    mpfr_chebpoly_set_double_chebpoly(P->poly + i, Q->poly + i);
}


void mpfr_chebpoly_vec_get_double_chebpoly_vec(double_chebpoly_vec_t P, const mpfr_chebpoly_vec_t Q)
{
  long n = Q->dim;
  long i;
  double_chebpoly_vec_set_dim(P, n);
  for (i = 0 ; i < n ; i++)
    mpfr_chebpoly_get_double_chebpoly(P->poly + i, Q->poly + i);
}


