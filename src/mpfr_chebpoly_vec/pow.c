
#include "mpfr_chebpoly_vec.h"


void mpfr_chebpoly_vec_pow(mpfr_chebpoly_vec_t P, const mpfr_chebpoly_vec_t Q, unsigned long e)
{
  long n = Q->dim;
  long i;

  mpfr_chebpoly_vec_set_dim(P, n);
  for (i = 0 ; i < n ; i++)
    mpfr_chebpoly_pow(P->poly + i, Q->poly + i, e);

}

