
#include "mpfr_chebpoly_vec.h"


void mpfr_chebpoly_vec_add(mpfr_chebpoly_vec_t P, const mpfr_chebpoly_vec_t Q, const mpfr_chebpoly_vec_t R)
{
  long n = Q->dim;
  long i;

  if (n != R->dim) {
    fprintf(stderr, "mpfr_chebpoly_vec_add: error: incompatible dimensions\n");
    return;
  }

  mpfr_chebpoly_vec_set_dim(P, n);
  for (i = 0 ; i < n ; i++)
    mpfr_chebpoly_add(P->poly + i, Q->poly + i, R->poly + i);

}

