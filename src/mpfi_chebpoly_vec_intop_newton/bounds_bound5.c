
#include "mpfi_chebpoly_vec_intop_newton.h"


void mpfi_chebpoly_vec_intop_newton_bounds_bound5_fr(mpfr_ptr_ptr bound5, const mpfi_chebpoly_vec_intop_t K, mpfi_bandmatrix_struct ** M_K, mpfr_bandmatrix_struct ** M_K_inv)
{
  long n = K->dim;
  long i, j, k;
  long N = M_K[0][0].dim - 1;
  mpfi_bandmatrix_t A, B;
  mpfi_bandmatrix_init(A);
  mpfi_bandmatrix_init(B);
  mpfi_bandmatrix_set_params(A, N+1, 0, 0);
  mpfi_bandmatrix_set_params(B, N+1, 0, 0);

  for (i = 0 ; i < n ; i++)
    for (j = 0 ; j < n ; j++) {
      mpfi_bandmatrix_zero(A);
      for (k = 0 ; k < n ; k++) {//fprintf(stderr, "<<1>>\n");
	mpfi_bandmatrix_set_mpfr_bandmatrix(B, M_K_inv[i] + k);//fprintf(stderr, "<<2: %ld - %ld - %ld - %ld - %ld - %ld>>\n", B->dim, B->Hwidth, B->Dwidth, M_K[k][j].dim, M_K[k][j].Hwidth, M_K[k][j].Dwidth);
	mpfi_bandmatrix_mul(B, B, M_K[k] + j);//fprintf(stderr, "<<3: %ld - %ld - %ld - %ld - %ld - %ld>>\n", A->dim, A->Hwidth, A->Dwidth, B->dim, B->Hwidth, B->Dwidth);
	mpfi_bandmatrix_add(A, A, B);//fprintf(stderr, "<<4>>\n");
      }
      if (i == j)
	for (k = 0 ; k <= N ; k++) {//fprintf(stderr, "<<7>>\n");
	  mpfi_sub_si(A->D[k] + A->Dwidth, A->D[k] + A->Dwidth, 1);//fprintf(stderr, "<<7!>>\n");
        }
      mpfi_bandmatrix_1norm_ubound(bound5[i] + j, A);
    }

  mpfi_bandmatrix_clear(A);
  mpfi_bandmatrix_clear(B);

}
