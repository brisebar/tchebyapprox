
#include "mpfi_chebpoly_vec_intop_newton.h"

// compute bound_i for i = 1,...,5


void mpfi_chebpoly_vec_intop_newton_bounds_fr(mpfr_ptr_ptr bound1, mpfr_ptr_ptr bound2, mpfr_ptr_ptr bound3, mpfr_ptr_ptr bound4, mpfr_ptr_ptr bound5, const mpfi_chebpoly_vec_intop_t K, mpfi_bandmatrix_struct ** M_K, const mpfi_bandmatrix_t M_Kglob, mpfr_bandmatrix_struct ** M_K_inv, const mpfr_bandmatrix_t M_K_inv_glob)
{ 
  mpfi_chebpoly_vec_intop_newton_bounds_bound1_fr(bound1, K, M_K);
  mpfi_chebpoly_vec_intop_newton_bounds_bound2_fr(bound2, K, M_K, M_K_inv);
  mpfi_chebpoly_vec_intop_newton_bounds_bound34_fr(bound3, bound4, K, M_K, M_K_inv);
  mpfi_chebpoly_vec_intop_newton_bounds_bound5_fr(bound5, K, M_K, M_K_inv);
}
  
