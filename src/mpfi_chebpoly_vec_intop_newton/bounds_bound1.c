
#include "mpfi_chebpoly_vec_intop_newton.h"


void mpfi_chebpoly_vec_intop_newton_bounds_bound1_fr(mpfr_ptr_ptr bound1, const mpfi_chebpoly_vec_intop_t K, mpfi_bandmatrix_struct ** M_K)
{
  long n = K->dim;
  long i, j;
  for (i = 0 ; i < n ; i++)
    for (j = 0 ; j < n ; j++)
      mpfi_chebpoly_vec_intop_newton_bounds_bound1_aux1_fr(bound1[i] + j, K->intop[i] + j, M_K[i] + j);
}
