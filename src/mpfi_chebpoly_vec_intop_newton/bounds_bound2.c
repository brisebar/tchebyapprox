
#include "mpfi_chebpoly_vec_intop_newton.h"

// bound2: i from N+1 to N+s


void mpfi_chebpoly_vec_intop_newton_bounds_bound2_aux0_fr(mpfr_t bound2, const mpfi_chebpoly_intop_t K, const mpfi_bandmatrix_t M_K, const mpfr_bandmatrix_t M_K_inv)
{
  long N = M_K->dim - 1;
  long s = M_K->Dwidth;
  long i;

  mpfi_bandvec_t V;
  mpfi_bandvec_init(V);
  mpfr_t temp;
  mpfr_init(temp);

  mpfr_set_zero(bound2, 1);

  for (i = N + 1 ; i <= N + s ; i++) {
   
    mpfi_chebpoly_intop_evaluate_Ti(V, K, i);
    _mpfi_vec_zero(V->D + s + N + 1 - i, s + i - N);
  
    mpfr_bandmatrix_evaluate_band_fi(V, M_K_inv, V);
    mpfi_bandvec_1norm_ubound(temp, V);

    if (mpfr_cmp(temp, bound2) > 0)
      mpfr_set(bound2, temp, MPFR_RNDU);
  
  }

  mpfi_bandvec_clear(V);
  mpfr_clear(temp);
}


void mpfi_chebpoly_vec_intop_newton_bounds_bound2_fr(mpfr_ptr_ptr bound2, const mpfi_chebpoly_vec_intop_t K, mpfi_bandmatrix_struct ** M_K, mpfr_bandmatrix_struct ** M_K_inv)
{
  long n = K->dim;
  long i, j, k;
  mpfr_t temp;
  mpfr_init(temp);

  for (i = 0 ; i < n ; i++)
    for (j = 0 ; j < n ; j++) {
      mpfr_set_zero(bound2[i] + j, 1);
      for (k = 0 ; k < n ; k++) {
	if (k == i)
	  mpfi_chebpoly_vec_intop_newton_bounds_bound2_aux1_fr(temp, K->intop[k] + j, M_K[k] + j, M_K_inv[i] + k);
	else
	  mpfi_chebpoly_vec_intop_newton_bounds_bound2_aux0_fr(temp, K->intop[k] + j, M_K[k] + j, M_K_inv[i] + k);
	mpfr_add(bound2[i] + j, bound2[i] + j, temp, MPFR_RNDU);
      }
    }

  mpfr_clear(temp);
}





