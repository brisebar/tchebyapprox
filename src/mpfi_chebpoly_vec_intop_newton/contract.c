
#include "mpfi_chebpoly_vec_intop_newton.h"


void mpfi_chebpoly_vec_intop_newton_contract_fr(mpfr_ptr_ptr bound, mpfr_bandmatrix_struct ** M_K_inv, const mpfi_chebpoly_vec_intop_t K, long init_N)
{
  //long r = K->order;
  long n = K->dim;
  long h = mpfi_chebpoly_vec_intop_Hwidth(K);
  long d = mpfi_chebpoly_vec_intop_Dwidth(K);
  long N = init_N > 2*d ? init_N : 2*d;
  long i, j;

  mpfi_bandmatrix_t M_K;
  mpfi_bandmatrix_init(M_K);
  mpfi_bandmatrix_struct ** M_Ks = malloc(n * sizeof(mpfi_bandmatrix_struct*));
  for (i = 0 ; i < n ; i++) {
    M_Ks[i] = malloc(n * sizeof(mpfi_bandmatrix_t));
    for (j = 0 ; j < n ; j++)
      mpfi_bandmatrix_init(M_Ks[i] + j);
  }
  mpfr_bandmatrix_t M_K_fr;
  mpfr_bandmatrix_init(M_K_fr);
  mpfr_bandmatrix_struct ** M_Ks_fr = malloc(n * sizeof(mpfr_bandmatrix_struct*));
  for (i = 0 ; i < n ; i++) {
    M_Ks_fr[i] = malloc(n * sizeof(mpfr_bandmatrix_t));
    for (j = 0 ; j < n ; j++)
      mpfr_bandmatrix_init(M_Ks_fr[i] + j);
  }
  mpfr_bandmatrix_QRdecomp_t N_K;
  mpfr_bandmatrix_QRdecomp_init(N_K);
  mpfr_bandmatrix_t M_K_inv_glob;
  mpfr_bandmatrix_init(M_K_inv_glob);

  int flag = -1;

  long h2 = h+h/2;
  long d2 = d+d/2;


  // validation loop

  printf("Certifying bound...\n");

  while (flag) {

    printf("Try with N = %ld, h' = %ld, d' = %ld...\n", N, h2, d2);

    // matrix representations of K
    for (i = 0 ; i < n ; i++)
      for (j = 0 ; j < n ; j++) {
	mpfi_chebpoly_intop_get_bandmatrix(M_Ks[i] + j, K->intop[i] + j, N);
	mpfi_bandmatrix_normalise(M_Ks[i] + j);
      }
    mpfi_bandmatrix_merge(M_K, M_Ks, n);
    
    // matrix representation of Id-K
    for (i = 0 ; i < n ; i++) {
      for (j = 0 ; j <= N ; j++)
	mpfi_sub_si(M_Ks[i][i].D[j] + M_Ks[i][i].Dwidth, M_Ks[i][i].D[j] + M_Ks[i][i].Dwidth, 1);
      for (j = 0 ; j < n ; j++)
	mpfi_bandmatrix_neg(M_Ks[i] + j, M_Ks[i] + j);
    }
    for (i = 0 ; i < M_K->dim ; i++)
      mpfi_sub_si(M_K->D[i] + M_K->Dwidth, M_K->D[i] + M_K->Dwidth, 1);
    mpfi_bandmatrix_neg(M_K, M_K);

    // mpfr matrices
    mpfi_bandmatrix_get_mpfr_bandmatrix(M_K_fr, M_K);
    for (i = 0 ; i < n ; i++)
      for (j = 0 ; j < n ; j++)
	mpfi_bandmatrix_get_mpfr_bandmatrix(M_Ks_fr[i] + j, M_Ks[i] + j);

    // upper triangular decomposition
    mpfr_bandmatrix_get_QRdecomp(N_K, M_K_fr);

    // approx inverse
    mpfr_bandmatrix_QRdecomp_approx_band_inverse(M_K_inv_glob, N_K, h2, d2);

    // small inverse matrices
    mpfr_bandmatrix_split(M_K_inv, M_K_inv_glob, n);

    // compute the bounds of the operator
    flag = mpfi_chebpoly_vec_intop_newton_bound_fr(bound, K, M_Ks, M_K, M_K_inv, M_K_inv_glob);

    printf("bound = \n");
    for (i = 0 ; i < n ; i++) {
      _mpfr_vec_print(bound[i], n, 5);
      printf("\n");
    }
    printf("[flag=%d]\n", flag);
    fflush(stdout);
    
    if (flag == 1)
      N *= 2;
    else if (flag == 2) {
      h2 *= 2; if (h2 > M_K->dim) h2 = M_K->dim;
      d2 *= 2; if (d2 >= M_K->dim) d2 = M_K->dim-1;
    }
  
  }


  // clear variables
  mpfi_bandmatrix_clear(M_K);
  mpfr_bandmatrix_clear(M_K_fr);
  mpfr_bandmatrix_QRdecomp_clear(N_K);
  mpfr_bandmatrix_clear(M_K_inv_glob);
  for (i = 0 ; i < n ; i++) {
    for (j = 0 ; j < n ; j++) {
      mpfi_bandmatrix_clear(M_Ks[i] + j);
      mpfr_bandmatrix_clear(M_Ks_fr[i] + j);
    }
    free(M_Ks[i]); free(M_Ks_fr[i]);
  }

}


