
#include "mpfi_chebpoly_vec_intop_newton.h"


// bound3 (diagonal coefficients) and bound4 (N_F^-1 on initial coefficient): i > N+s

void mpfi_chebpoly_vec_intop_newton_bounds_bound4_aux0_fr(mpfr_t bound4, const mpfi_chebpoly_intop_t K, const mpfi_bandmatrix_t M_K, const mpfr_bandmatrix_t M_K_inv)
{
  long N = M_K->dim - 1;
  long r = K->order;
  long h = M_K->Hwidth;
  long s = M_K->Dwidth;
  long j, k, l;

  mpfi_t t, u;
  mpfi_init(t);
  mpfi_init(u);

  // interval where the index i is living = [N+s+1, +inf[
  mpfi_t i;
  mpfi_init(i);
  mpfr_set_si(&(i->left), N + s + 1, MPFR_RNDN);
  mpfr_set_inf(&(i->right), 1);

  // first order difference method to compute bound4
  mpfr_t bound4_alt, bound4_alt_diff, bound4_alt_diff_j, temp_fr;
  mpfr_inits(bound4_alt, bound4_alt_diff, bound4_alt_diff_j, temp_fr, NULL);

  mpfr_set_zero(bound4_alt_diff, 1);

  mpfi_bandvec_t V, W;
  mpfi_bandvec_init(V);
  mpfi_bandvec_init(W);
  mpfi_bandvec_set_params(V, h, s, N + s + 1);
  mpfi_bandvec_set_params(W, h, s, N + s + 1);

  for (j = 0 ; j < r ; j++) {

    // int_-1^x T_j*T_i = 1/4*( T_{i+j+1}/(i+j+1) - 
    // T_{i+j-1}/(i+j-1) + T_{i-j+1}/(i-j+1) - T_{i-j-1}/(i-j-1) )
    // + (-1)^{i+j}/4 * (-2/((i+j)^2-1) - 2/((i-j)^2-1))

    long d = K->alpha[j].degree;

    // add integration constant (t) * alpha_j into V->H
    // t = 1/2 * (1/((i+j)^2-1) + 1/((i-j)^2-1))
    mpfi_add_si(u, i, j);
    mpfi_sqr(u, u);
    mpfi_sub_si(u, u, 1);
    mpfi_si_div(u, 1, u);
    mpfi_sub_si(t, i, j);
    mpfi_sqr(t, t);
    mpfi_sub_si(t, t, 1);
    mpfi_si_div(t, 1, t);
    mpfi_add(t, u, t);
    mpfi_mul_2si(t, t, -1);

    for (k = 0 ; k <= d ; k++) {
      mpfi_mul(u, K->alpha[j].coeffs + k, t);
      mpfi_add(V->H + k, V->H + k, u);
    }
    
    // focus on bound4_alt_diff_j
    // first part
    k = j + 1 < 0 ? -j - 1 : j + 1;
    mpfi_si_div(t, k, i);
    mpfi_sqr(t, t);
    mpfi_si_sub(t, 1, t);
    mpfi_si_div(t, 1, t);
    mpfi_sub(t, t, t);
    mpfi_mag(bound4_alt_diff_j, t);
    mpfr_mul_si(bound4_alt_diff_j, bound4_alt_diff_j, k, MPFR_RNDU);
    // second part
    k = j - 1 < 0 ? -j + 1 : j - 1;
    mpfi_si_div(t, k, i);
    mpfi_sqr(t, t);
    mpfi_si_sub(t, 1, t);
    mpfi_si_div(t, 1, t);
    mpfi_sub(t, t, t);
    mpfi_mag(temp_fr, t);
    mpfr_mul_si(temp_fr, temp_fr, k, MPFR_RNDU);
    // together
    mpfr_add(bound4_alt_diff_j, bound4_alt_diff_j, temp_fr, MPFR_RNDU);
    mpfr_mul(temp_fr, &(i->left), &(i->left), MPFR_RNDD);
    mpfr_div(bound4_alt_diff_j, bound4_alt_diff_j, temp_fr, MPFR_RNDU);
    mpfr_mul_2si(bound4_alt_diff_j, bound4_alt_diff_j, -1, MPFR_RNDU);
    // norm ||M_K_inv * alpha_j||
    mpfi_bandvec_zero(W);
    for (k = 0 ; k <= d ; k++)
      mpfi_set(W->H + k, K->alpha[j].coeffs + k);
    mpfr_bandmatrix_evaluate_band_fi(W, M_K_inv, W);
    _mpfi_vec_1norm_ubound(temp_fr, W->H, h);
    // correct value of bound4_alt_diff_j
    mpfr_mul(bound4_alt_diff_j, bound4_alt_diff_j, temp_fr, MPFR_RNDU);

    // update bound4_alt_diff
    mpfr_add(bound4_alt_diff, bound4_alt_diff, bound4_alt_diff_j, MPFR_RNDU);

  }

  mpfi_chebpoly_intop_evaluate_Ti(W, K, N + s + 1);

  // compute bound4
  mpfr_bandmatrix_evaluate_band_fi(V, M_K_inv, V);
  _mpfi_vec_1norm_ubound(bound4, V->H, h);

  // compute bound4_alt
  mpfr_bandmatrix_evaluate_band_fi(W, M_K_inv, W);
  _mpfi_vec_1norm_ubound(bound4_alt, W->H, h);
  mpfr_add(bound4_alt, bound4_alt, bound4_alt_diff, MPFR_RNDU);
  if (mpfr_cmp(bound4_alt, bound4) < 0) {
    mpfr_set(bound4, bound4_alt, MPFR_RNDU);
  }

  // clear variables
  mpfi_clear(t);
  mpfi_clear(u);
  mpfi_clear(i);
  mpfi_bandvec_clear(V);
  mpfi_bandvec_clear(W);
  mpfr_clears(bound4_alt, bound4_alt_diff, bound4_alt_diff_j, temp_fr, NULL);
}


void mpfi_chebpoly_vec_intop_newton_bounds_bound34_fr(mpfr_ptr_ptr bound3, mpfr_ptr_ptr bound4, const mpfi_chebpoly_vec_intop_t K, mpfi_bandmatrix_struct ** M_K, mpfr_bandmatrix_struct ** M_K_inv)
{
  long n = K->dim;
  long i, j, k;
  mpfr_t temp3, temp4;
  mpfr_inits(temp3, temp4, NULL);

  for (i = 0 ; i < n ; i++)
    for (j = 0 ; j < n ; j++) {
      mpfr_set_zero(bound3[i] + j, 1);
      mpfr_set_zero(bound4[i] + j, 1);
      for (k = 0 ; k < n ; k++)
	if (k == i) {
	  mpfi_chebpoly_vec_intop_newton_bounds_bound34_aux1_fr(temp3, temp4, K->intop[k] + j, M_K[k] + j, M_K_inv[i] + k);
	  mpfr_add(bound3[i] + j, bound3[i] + j, temp3, MPFR_RNDU);
	  mpfr_add(bound4[i] + j, bound4[i] + j, temp4, MPFR_RNDU);
	}
	else {
	  mpfi_chebpoly_vec_intop_newton_bounds_bound4_aux0_fr(temp4, K->intop[k] + j, M_K[k] + j, M_K_inv[i] + k);
	mpfr_add(bound4[i] + j, bound4[i] + j, temp4, MPFR_RNDU);
	}
     
    }

  mpfr_clears(temp3, temp4, NULL);
}





