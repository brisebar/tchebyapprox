
#include "mpfi_chebpoly_vec_intop_newton.h"

// merge all bound_i to get the final array of bounds


int mpfi_chebpoly_vec_intop_newton_bound_fr(mpfr_ptr_ptr bound, const mpfi_chebpoly_vec_intop_t K, mpfi_bandmatrix_struct ** M_K, const mpfi_bandmatrix_t M_Kglob, mpfr_bandmatrix_struct ** M_K_inv, const mpfr_bandmatrix_t M_K_inv_glob)
{
  long n = K->dim;
  long i, j, k;
  int return_flag = 0;

  mpfr_ptr_ptr bound1 = malloc(n * sizeof(mpfr_ptr));
  mpfr_ptr_ptr bound2 = malloc(n * sizeof(mpfr_ptr));
  mpfr_ptr_ptr bound3 = malloc(n * sizeof(mpfr_ptr));
  mpfr_ptr_ptr bound4 = malloc(n * sizeof(mpfr_ptr));
  mpfr_ptr_ptr bound5 = malloc(n * sizeof(mpfr_ptr));
  for (i = 0 ; i < n ; i++) {
    bound1[i] = malloc(n * sizeof(mpfr_t));
    bound2[i] = malloc(n * sizeof(mpfr_t));
    bound3[i] = malloc(n * sizeof(mpfr_t));
    bound4[i] = malloc(n * sizeof(mpfr_t));
    bound5[i] = malloc(n * sizeof(mpfr_t));
    for (j = 0 ; j < n ; j++)
      mpfr_inits(bound1[i] + j, bound2[i] + j, bound3[i] + j, bound4[i] + j, bound5[i] + j, NULL);
  }

  // first just compute bounds
  mpfi_chebpoly_vec_intop_newton_bounds_bound1_fr(bound1, K, M_K);
  mpfi_chebpoly_vec_intop_newton_bounds_bound2_fr(bound2, K, M_K, M_K_inv);
  mpfi_chebpoly_vec_intop_newton_bounds_bound34_fr(bound3, bound4, K, M_K, M_K_inv);
  for (i = 0 ; i < n ; i++)
    for (j = 0 ; j < n ; j++) {
      mpfr_add(bound[i] + j, bound3[i] + j, bound4[i] + j, MPFR_RNDU);
      if (mpfr_cmp(bound1[i] + j, bound[i] + j) > 0)
        mpfr_set(bound[i] + j, bound1[i] + j, MPFR_RNDU);
      if (mpfr_cmp(bound2[i] + j, bound[i] + j) > 0)
        mpfr_set(bound[i] + j, bound2[i] + j, MPFR_RNDU);
      if (mpfr_cmp_d(bound[i] + j, 0.2/n) > 0)
	return_flag = 1;
    }

  if (return_flag == 0) {
    // bound5: approx error of the inverse
    mpfi_chebpoly_vec_intop_newton_bounds_bound5_fr(bound5, K, M_K, M_K_inv);
    for (i = 0 ; i < n ; i++)
      for (j = 0 ; j < n ; j++) {
        mpfr_add(bound[i] + j, bound[i] + j, bound5[i] + j, MPFR_RNDU);
	if (mpfr_cmp_d(bound5[i] + j, 0.1/n) > 0)
	  return_flag = 2;
      }

  }

  for (i = 0 ; i < n ; i++) {
    for (j = 0 ; j < n ; j++)
      mpfr_clears(bound1[i] + j, bound2[i] + j, bound3[i] + j, bound4[i] + j, bound5[i] + j, NULL);
    free(bound1[i]); free(bound2[i]); free(bound3[i]); free(bound4[i]); free(bound5[i]);
  }
  free(bound1); free(bound2); free(bound3); free(bound4); free(bound5);

  return return_flag;
}

