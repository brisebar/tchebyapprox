
#include "chebmodel_intop_newton.h"



/* bound for the Newton Operator */
// 1 = N too small  2 = imprecise numerical inverse  3 = the cumulated error of the coefficients of K is too big

int chebmodel_intop_newton_bound_d(double_t bound, const chebmodel_intop_t K, const mpfi_chebpoly_intop_t Ktrunc, const mpfi_bandmatrix_t M_Ktrunc, const double_bandmatrix_t M_Ktrunc_inv)
{
  int return_flag = 0;
  long i;
  long r = K->order;

  mpfr_t temp;;
  mpfr_init(temp);
  double_t (bound_temp);
  double_init(bound_temp);

  // error due to the rem of the chebmodels coefficients of K
  double_bandmatrix_1norm_ubound(bound, M_Ktrunc_inv);
  mpfr_set_zero(temp, 1);
  for (i = 0 ; i < r ; i++)
    mpfr_add(temp, temp, K->alpha[i].rem, MPFR_RNDU);
  mpfr_mul_2si(temp, temp, 1, MPFR_RNDU);
  mpfr_mul_d(temp, temp, *bound, MPFR_RNDU);
  double_set_fr(bound, temp, MPFR_RNDU);
  printf("[bound6=%f]\n", *bound);

  if (*bound > 0.2)
    return_flag = 3;

  else {
    return_flag = mpfi_chebpoly_intop_newton_bound_d(bound_temp, Ktrunc, M_Ktrunc, M_Ktrunc_inv);
    mpfr_set_d(temp, *bound_temp, MPFR_RNDU);
    mpfr_add_d(temp, temp, *bound, MPFR_RNDU);
    double_set_fr(bound, temp, MPFR_RNDU);
  }

  // clear variables
  double_clear(bound_temp);
  mpfr_clear(temp);

  return return_flag;
}



int chebmodel_intop_newton_bound_fr(mpfr_t bound, const chebmodel_intop_t K, const mpfi_chebpoly_intop_t Ktrunc, const mpfi_bandmatrix_t M_Ktrunc, const mpfr_bandmatrix_t M_Ktrunc_inv)
{
  int return_flag = 0;
  long i;
  long r = K->order;

  mpfr_t bound_temp;
  mpfr_init(bound_temp);

  // error due to the rem of the chebmodels coefficients of K
  mpfr_bandmatrix_1norm_ubound(bound, M_Ktrunc_inv);
  mpfr_set_zero(bound_temp, 1);
  for (i = 0 ; i < r ; i++)
    mpfr_add(bound_temp, bound_temp, K->alpha[i].rem, MPFR_RNDU);
  mpfr_mul_2si(bound_temp, bound_temp, 1, MPFR_RNDU);
  mpfr_mul(bound, bound, bound_temp, MPFR_RNDU);

  if (mpfr_cmp_d(bound, 0.2) > 0)
    return_flag = 3;

  else {
    return_flag = mpfi_chebpoly_intop_newton_bound_fr(bound_temp, Ktrunc, M_Ktrunc, M_Ktrunc_inv);
    mpfr_add(bound, bound, bound_temp, MPFR_RNDU);
  }

  // clear variables
  mpfr_clear(bound_temp);

  return return_flag;
}


