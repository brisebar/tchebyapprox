
#include "chebmodel_intop_newton.h"


void chebmodel_intop_newton_contract_d(double_t bound, double_bandmatrix_t M_K_inv, const chebmodel_intop_t K, long init_N)
{
  mpfr_t bound_fr;
  mpfr_init(bound_fr);

  long r = K->order;
  long h = chebmodel_intop_Hwidth(K);
  long s = chebmodel_intop_Dwidth(K);
  long N = init_N > 2*s ? init_N : 2*s;
  long i, j;

  double_chebpoly_intop_t Kdb;
  double_chebpoly_intop_init(Kdb);
  chebmodel_intop_get_double_chebpoly_intop(Kdb, K);
  mpfi_chebpoly_intop_t K_fi;
  mpfi_chebpoly_intop_init(K_fi);
  chebmodel_intop_get_mpfi_chebpoly_intop(K_fi, K);
  mpfi_bandmatrix_t M_K_fi;
  mpfi_bandmatrix_init(M_K_fi);
  double_bandmatrix_t M_K;
  double_bandmatrix_init(M_K);
  double_bandmatrix_QRdecomp_t N_K;
  double_bandmatrix_QRdecomp_init(N_K);
  
  mpfi_chebpoly_t P;
  mpfi_chebpoly_init(P);
  double_chebpoly_t Q;
  double_chebpoly_init(Q);
  double_bandmatrix_t B;
  double_bandmatrix_init(B);

  int flag = -1;

  // first loop to efficiently estimate a good N
  printf("Estimating an appropriate N...\n");

  while (flag) {

    printf("Try with N = %ld...   ", N);

    // M_K
    double_chebpoly_intop_get_bandmatrix(M_K, Kdb, N);
    for (i = 0 ; i <= N ; i++)
      double_sub_si(M_K->D[i] + s, M_K->D[i] + s, 1, MPFR_RNDN);
    double_bandmatrix_neg(M_K, M_K);

    // N_K
    double_bandmatrix_get_QRdecomp(N_K, M_K);

    // get an estimate of the norm of the Newton operator using the N+1-th column
    i = N + 1;
    double_chebpoly_zero(Q);
    double_chebpoly_set_coeff_si(Q, i, 1);
    double_chebpoly_intop_evaluate_d(Q, Kdb, Q);
    if (Q->degree < N)
      double_chebpoly_set_degree(Q, N);
    _double_bandmatrix_QRdecomp_solve_d(Q->coeffs, N_K, Q->coeffs);
    double_chebpoly_1norm(bound, Q);

    printf("bound = %f\n", *bound);

    // if bound is too big (> 0.4), double N
    flag = (*bound > 0.4);
    if (flag)
      N *= 2;

  }

  printf("\n");

  // increase a bit N
  N += N / 2; // N := 1.5*N
  double_chebpoly_intop_get_bandmatrix(M_K, Kdb, N);
  for (i = 0 ; i <= N ; i++)
    double_sub_si(M_K->D[i] + s, M_K->D[i] + s, 1, MPFR_RNDN);
  double_bandmatrix_neg(M_K, M_K);
  double_bandmatrix_get_QRdecomp(N_K, M_K);
  
  flag = -1;

  // second loop to determine good h2 and s2 for the approx inverse
  printf("Estimating appropriate h' and s'...\n");

  long h2 = h;
  long s2 = s;

  while (flag) {
  
    printf("Try with h' = %ld and s' = %ld...   ", h2, s2);

    // approx inverse
    double_bandmatrix_QRdecomp_approx_band_inverse(M_K_inv, N_K, h2, s2);

    // compute ||Id - M_K_inv * M_K||
    double_bandmatrix_mul(B, M_K_inv, M_K);
    for (i = 0 ; i <= N ; i++)
      double_sub_si(B->D[i] + B->Dwidth, B->D[i] + B->Dwidth, 1, MPFR_RNDN);
    double_bandmatrix_1norm_ubound(bound, B);

    printf("bound = %f\n", *bound);


    // if bound is too big (> 0.1), increase h2 and s2
    flag = (*bound > 0.1);
    if (flag) {
      h2 *= 2; if (h2 > M_K->dim) h2 = M_K->dim;
      s2 *= 2; if (s2 >= M_K->dim) s2 = M_K->dim-1;
    }

  }

  printf("\n");

  flag = -1;

  // now we can execute the real validation loop

  printf("Certifying bound...\n");

  while (flag) {

    printf("Try with N = %ld, h' = %ld, s' = %ld...\n", N, h2, s2);

    // matrix representation of K
    mpfi_chebpoly_intop_get_bandmatrix(M_K_fi, K_fi, N);

    // matrix representation of Id-K
    mpfi_bandmatrix_neg(M_K_fi, M_K_fi);
    for (i = 0 ; i <= N ; i++)
      mpfi_add_si(M_K_fi->D[i] + s, M_K_fi->D[i] + s, 1);

    mpfi_bandmatrix_get_double_bandmatrix(M_K, M_K_fi);

    // upper triangular decomposition
    double_bandmatrix_get_QRdecomp(N_K, M_K);

    // approx inverse
    double_bandmatrix_QRdecomp_approx_band_inverse(M_K_inv, N_K, h2, s2);

    // compute the bounds of the operator
    flag = chebmodel_intop_newton_bound_d(bound, K, K_fi, M_K_fi, M_K_inv);

    printf("bound = %f", *bound);
    printf("[flag=%d]\n", flag);
    fflush(stdout);
    
    if (flag == 1)
      N *= 2;
    else if (flag == 2) {
      h2 *= 2; if (h2 > M_K->dim) h2 = M_K->dim;
      s2 *= 2; if (s2 >= M_K->dim) s2 = M_K->dim-1;
    }
    else if (flag == 3) {
      fprintf(stderr, "chebmodel_intop_newton_contract: warning: the coefficients of K probably have too big remainders\n");
      /*N *= 2;
      h2 *= 2; if (h2 > 2*M_K->dim) h2 = 2*M_K->dim;
      s2 *= 2; if (s2 >= 2*M_K->dim) s2 = 2*M_K->dim-1;*/
    }
  
  }

  mpfi_chebpoly_intop_clear(K_fi);
  double_chebpoly_intop_clear(Kdb);
  mpfi_bandmatrix_clear(M_K_fi);
  double_bandmatrix_clear(M_K);
  double_bandmatrix_QRdecomp_clear(N_K);
  mpfi_chebpoly_clear(P);
  double_chebpoly_clear(Q);
  double_bandmatrix_clear(B);

}




void chebmodel_intop_newton_contract_fr(mpfr_t bound, mpfr_bandmatrix_t M_K_inv, const chebmodel_intop_t K, long init_N)
{
  long r = K->order;

  mpfi_chebpoly_intop_t Ktrunc;
  mpfi_chebpoly_intop_init(Ktrunc);
  chebmodel_intop_get_mpfi_chebpoly_intop(Ktrunc, K);

  long h = mpfi_chebpoly_intop_Hwidth(Ktrunc);
  long s = mpfi_chebpoly_intop_Dwidth(Ktrunc);
  long N = init_N > 2*s ? init_N : 2*s;
  long i, j;

  mpfi_bandmatrix_t M_K_fi;
  mpfi_bandmatrix_init(M_K_fi);
  mpfr_bandmatrix_t M_K;
  mpfr_bandmatrix_init(M_K);
  mpfr_bandmatrix_QRdecomp_t N_K;
  mpfr_bandmatrix_QRdecomp_init(N_K);
  mpfi_bandmatrix_t A;
  mpfi_bandmatrix_init(A);

  mpfi_chebpoly_t P;
  mpfi_chebpoly_init(P);

  int flag = -1;

  // first loop to efficiently estimate a good N
  printf("Estimating an appropriate N...\n");

  while (flag) {

    printf("Try with N = %ld...   ", N);

    // M_K_fi
    mpfi_chebpoly_intop_get_bandmatrix(M_K_fi, Ktrunc, N);
    for (i = 0 ; i <= N ; i++)
      mpfi_sub_si(M_K_fi->D[i] + s, M_K_fi->D[i] + s, 1);
    mpfi_bandmatrix_neg(M_K_fi, M_K_fi);

    // M_K
    mpfi_bandmatrix_get_mpfr_bandmatrix(M_K, M_K_fi);

    // N_K
    mpfr_bandmatrix_get_QRdecomp(N_K, M_K);

    // get an estimate of the norm of the Newton operator using the N+1-th column
    i = N + 1;
    mpfi_chebpoly_zero(P);
    mpfi_chebpoly_set_coeff_si(P, i, 1);
    mpfi_chebpoly_intop_evaluate_fi(P, Ktrunc, P);
    if (P->degree < N)
      mpfi_chebpoly_set_degree(P, N);
    _mpfr_bandmatrix_QRdecomp_solve_fi(P->coeffs, N_K, P->coeffs);
    mpfi_chebpoly_1norm_ubound(bound, P);

    printf("bound = ");
    mpfr_out_str(stdout, 10, 10, bound, MPFR_RNDU);
    printf("\n");

    // if bound is too big (> 0.4), double N
    flag = (mpfr_cmp_d(bound, 0.4) > 0);
    if (flag)
      N *= 2;

  }

  printf("\n");

  // increase a bit N
  N += N / 2; // N := 1.5*N
  mpfi_chebpoly_intop_get_bandmatrix(M_K_fi, Ktrunc, N);
  for (i = 0 ; i <= N ; i++)
    mpfi_sub_si(M_K_fi->D[i] + s, M_K_fi->D[i] + s, 1);
  mpfi_bandmatrix_neg(M_K_fi, M_K_fi);
  mpfi_bandmatrix_get_mpfr_bandmatrix(M_K, M_K_fi);
  mpfr_bandmatrix_get_QRdecomp(N_K, M_K);
  
  flag = -1;

  // second loop to determine good h2 and s2 for the approx inverse
  printf("Estimating appropriate h' and s'...\n");

  long h2 = h;
  long s2 = s;

  while (flag) {
  
    printf("Try with h' = %ld and s' = %ld...   ", h2, s2);

    // approx inverse
    mpfr_bandmatrix_QRdecomp_approx_band_inverse(M_K_inv, N_K, h2, s2);

    // compute ||Id - M_K_inv * M_K||
    mpfi_bandmatrix_set_mpfr_bandmatrix(A, M_K_inv);
    mpfi_bandmatrix_mul(A, A, M_K_fi);
    for (i = 0 ; i <= N ; i++)
      mpfi_sub_si(A->D[i] + A->Dwidth, A->D[i] + A->Dwidth, 1);
    mpfi_bandmatrix_1norm_ubound(bound, A);

    printf("bound = ");
    mpfr_out_str(stdout, 10, 10, bound, MPFR_RNDU);
    printf("\n");


    // if bound is too big (> 0.1), increase h2 and s2
    flag = (mpfr_cmp_d(bound, 0.1) > 0);
    if (flag) {
      h2 += h; if (h2 > M_K->dim) h2 = M_K->dim;
      s2 += s; if (s2 >= M_K->dim) s2 = M_K->dim-1;
    }

  }

  printf("\n");

  flag = -1;

  // now we can execute the real validation loop

  printf("Certifying bound...\n");

  while (flag) {

    printf("Try with N = %ld, h' = %ld, s' = %ld...\n", N, h2, s2);

    // matrix representation of K
    mpfi_chebpoly_intop_get_bandmatrix(M_K_fi, Ktrunc, N);

    // matrix representation of Id-K
    mpfi_bandmatrix_neg(M_K_fi, M_K_fi);
    for (i = 0 ; i <= N ; i++)
      mpfi_add_si(M_K_fi->D[i] + s, M_K_fi->D[i] + s, 1);

    mpfi_bandmatrix_get_mpfr_bandmatrix(M_K, M_K_fi);

    // upper triangular decomposition
    mpfr_bandmatrix_get_QRdecomp(N_K, M_K);

    // approx inverse
    mpfr_bandmatrix_QRdecomp_approx_band_inverse(M_K_inv, N_K, h2, s2);

    // compute the bounds of the operator
    flag = chebmodel_intop_newton_bound_fr(bound, K, Ktrunc, M_K_fi, M_K_inv);

    printf("bound = ");
    mpfr_out_str(stdout, 10, 10, bound, MPFR_RNDU);
    printf("[flag=%d]\n", flag);
    fflush(stdout);
    
    if (flag == 1)
      N *= 2;
    else if (flag == 2) {
      h2 *= 2; if (h2 > M_K->dim) h2 = M_K->dim;
      s2 *= 2; if (s2 >= M_K->dim) s2 = M_K->dim-1;
    }
    else if (flag == 3) {
      printf("the coefficients of K have probably too big remainders\n");
      /*N *= 2;
      h2 *= 2;
      s2 += s;*/
    }
  
  }

  mpfi_chebpoly_intop_clear(Ktrunc);
  mpfi_bandmatrix_clear(M_K_fi);
  mpfr_bandmatrix_clear(M_K);
  mpfr_bandmatrix_QRdecomp_clear(N_K);
  mpfi_bandmatrix_clear(A);
  mpfi_chebpoly_clear(P);
}

