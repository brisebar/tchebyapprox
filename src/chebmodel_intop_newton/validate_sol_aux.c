
#include "chebmodel_intop_newton.h"

void chebmodel_intop_newton_validate_sol_aux_d(mpfr_t bound, const chebmodel_intop_t K, const double_t T_norm, const double_bandmatrix_t M_K_inv, const chebmodel_t G, const mpfr_chebpoly_t P)
{
  mpfr_t temp;
  mpfr_init(temp);
  long N = M_K_inv->dim - 1;

  // norm of M_K_inv
  double_t M_K_inv_norm;
  double_init(M_K_inv_norm);
  double_bandmatrix_1norm_ubound(M_K_inv_norm, M_K_inv);
  if (*M_K_inv_norm < 1)
    double_set_si(M_K_inv_norm, 1, MPFR_RNDU);

  // compute P-T(P)
  chebmodel_t Pbis;
  chebmodel_init(Pbis);
  chebmodel_set_mpfr_chebpoly(Pbis, P);

  chebmodel_t T_P;
  chebmodel_init(T_P);
  chebmodel_intop_evaluate_chebmodel(T_P, K, Pbis);
  chebmodel_add(T_P, T_P, G);
  chebmodel_sub(T_P, Pbis, T_P);
  if (T_P->poly->degree < N)
    mpfi_chebpoly_set_degree(T_P->poly, N);
  _double_bandmatrix_evaluate_fi(T_P->poly->coeffs, M_K_inv, T_P->poly->coeffs);
  mpfr_mul_d(T_P->rem, T_P->rem, *M_K_inv_norm, MPFR_RNDU);

  // validated bound
  chebmodel_1norm_ubound(bound, T_P);
  mpfr_set_d(temp, *T_norm, MPFR_RNDU);
  mpfr_si_sub(temp, 1, temp, MPFR_RNDD);
  mpfr_div(bound, bound, temp, MPFR_RNDU);

  // clear variables
  double_clear(M_K_inv_norm);
  chebmodel_clear(T_P);
  chebmodel_clear(Pbis);
  mpfr_clear(temp);
}


void chebmodel_intop_newton_validate_sol_aux_fr(mpfr_t bound, const chebmodel_intop_t K, const mpfr_t T_norm, const mpfr_bandmatrix_t M_K_inv, const chebmodel_t G, const mpfr_chebpoly_t P)
{
  mpfr_t temp;
  mpfr_init(temp);
  long N = M_K_inv->dim - 1;

  // norm of M_K_inv
  mpfr_t M_K_inv_norm;
  mpfr_init(M_K_inv_norm);
  mpfr_bandmatrix_1norm_ubound(M_K_inv_norm, M_K_inv);
  if (mpfr_cmp_si(M_K_inv_norm, 1) < 0)
    mpfr_set_si(M_K_inv_norm, 1, MPFR_RNDU);

  // compute P-T(P)
  chebmodel_t Pbis;
  chebmodel_init(Pbis);
  chebmodel_set_mpfr_chebpoly(Pbis, P);

  chebmodel_t T_P;
  chebmodel_init(T_P);
  chebmodel_intop_evaluate_chebmodel(T_P, K, Pbis);
  chebmodel_add(T_P, T_P, G);
  chebmodel_sub(T_P, Pbis, T_P);
  if (T_P->poly->degree < N)
    mpfi_chebpoly_set_degree(T_P->poly, N);
  _mpfr_bandmatrix_evaluate_fi(T_P->poly->coeffs, M_K_inv, T_P->poly->coeffs);
  mpfr_mul(T_P->rem, T_P->rem, M_K_inv_norm, MPFR_RNDU);

  // validated bound
  chebmodel_1norm_ubound(bound, T_P);
  mpfr_si_sub(temp, 1, T_norm, MPFR_RNDD);
  mpfr_div(bound, bound, temp, MPFR_RNDU);

  // clear variables
  mpfr_clear(M_K_inv_norm);
  chebmodel_clear(T_P);
  chebmodel_clear(Pbis);
  mpfr_clear(temp);
}



