
#include "chebmodel_intop_newton.h"


void chebmodel_intop_newton_validate_sol_d(mpfr_t bound, const chebmodel_intop_t K, const chebmodel_t G, const mpfr_chebpoly_t P, long init_N)
{
  double_t T_norm;
  double_init(T_norm);
  double_bandmatrix_t M_K_inv;
  double_bandmatrix_init(M_K_inv);

  // get a contracting operator
  chebmodel_intop_newton_contract_d(T_norm, M_K_inv, K, init_N);

  // obtain upper bound
  chebmodel_intop_newton_validate_sol_aux_d(bound, K, T_norm, M_K_inv, G, P);

  // clear variables
  double_clear(T_norm);
  double_bandmatrix_clear(M_K_inv);
}


void chebmodel_intop_newton_validate_sol_fr(mpfr_t bound, const chebmodel_intop_t K, const chebmodel_t G, const mpfr_chebpoly_t P, long init_N)
{
  mpfr_t T_norm;
  mpfr_init(T_norm);
  mpfr_bandmatrix_t M_K_inv;
  mpfr_bandmatrix_init(M_K_inv);

  // get a contracting operator
  chebmodel_intop_newton_contract_fr(T_norm, M_K_inv, K, init_N);

  // obtain upper bound
  chebmodel_intop_newton_validate_sol_aux_fr(bound, K, T_norm, M_K_inv, G, P);

  // clear variables
  mpfr_clear(T_norm);
  mpfr_bandmatrix_clear(M_K_inv);
}


void chebmodel_lode_intop_newton_validate_sol_d(mpfr_t bound, const chebmodel_lode_t L, const chebmodel_t g, mpfi_srcptr I, const mpfr_chebpoly_t P)
{
  chebmodel_intop_t K;
  chebmodel_intop_init(K);
  chebmodel_intop_set_lode(K, L);

  mpfi_chebpoly_intop_t Ktrunc;
  mpfi_chebpoly_intop_init(Ktrunc);
  chebmodel_intop_get_mpfi_chebpoly_intop(Ktrunc, K);

  chebmodel_t G;
  chebmodel_init(G);
  chebmodel_intop_rhs(G, L, g, I);

  long init_N = 2 * mpfi_chebpoly_intop_Dwidth(Ktrunc);

  chebmodel_intop_newton_validate_sol_d(bound, K, G, P, init_N);

  mpfi_chebpoly_intop_clear(Ktrunc);
}


void chebmodel_lode_intop_newton_validate_sol_fr(mpfr_t bound, const chebmodel_lode_t L, const chebmodel_t g, mpfi_srcptr I, const mpfr_chebpoly_t P)
{
  chebmodel_intop_t K;
  chebmodel_intop_init(K);
  chebmodel_intop_set_lode(K, L);

  chebmodel_t G;
  chebmodel_init(G);
  chebmodel_intop_rhs(G, L, g, I);

  long init_N = 2 * chebmodel_intop_Dwidth(K);

  chebmodel_intop_newton_validate_sol_fr(bound, K, G, P, init_N);

  chebmodel_intop_clear(K);
  chebmodel_clear(G);
}
