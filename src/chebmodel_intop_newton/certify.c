
#include "chebmodel_intop_newton.h"


void chebmodel_intop_newton_certify_d(double_t bound, double_bandmatrix_t M_K_inv, const chebmodel_intop_t K, long N, long h2, long d2)
{
  long i;
  long h = chebmodel_intop_Hwidth(K);
  long d = chebmodel_intop_Dwidth(K);

  mpfi_chebpoly_intop_t K_fi;
  mpfi_chebpoly_intop_init(K_fi);
  mpfi_bandmatrix_t M_K_fi;
  mpfi_bandmatrix_init(M_K_fi);
  double_bandmatrix_t M_K_d;
  double_bandmatrix_init(M_K_d);
  double_bandmatrix_QRdecomp_t N_K_d;
  double_bandmatrix_QRdecomp_init(N_K_d);

  // matrix representation of I-K
  chebmodel_intop_get_mpfi_chebpoly_intop(K_fi, K);
  mpfi_chebpoly_intop_get_bandmatrix(M_K_fi, K_fi, N);
  for (i = 0 ; i <= N ; i++)
    mpfi_sub_si(M_K_fi->D[i] + d, M_K_fi->D[i] + d, 1);
  mpfi_bandmatrix_neg(M_K_fi, M_K_fi);
  mpfi_bandmatrix_get_double_bandmatrix(M_K_d, M_K_fi);

  // approx inverse
  double_bandmatrix_get_QRdecomp(N_K_d, M_K_d);
  double_bandmatrix_QRdecomp_approx_band_inverse(M_K_inv, N_K_d, h2, d2);

  // compute the bounds of the operator
  int flag = chebmodel_intop_newton_bound_d(bound, K, K_fi, M_K_fi, M_K_inv);

  printf("bound = %f\n[flag=%d]\n", *bound, flag);

  //clear variables
  mpfi_chebpoly_intop_clear(K_fi);
  mpfi_bandmatrix_clear(M_K_fi);
  double_bandmatrix_clear(M_K_d);
  double_bandmatrix_QRdecomp_clear(N_K_d);

}


void chebmodel_intop_newton_certify_fr(mpfr_t bound, mpfr_bandmatrix_t M_K_inv, const chebmodel_intop_t K, long N, long h2, long d2)
{
  long i;
  long h = chebmodel_intop_Hwidth(K);
  long d = chebmodel_intop_Dwidth(K);

  mpfi_chebpoly_intop_t K_fi;
  mpfi_chebpoly_intop_init(K_fi);
  mpfi_bandmatrix_t M_K_fi;
  mpfi_bandmatrix_init(M_K_fi);
  mpfr_bandmatrix_t M_K_fr;
  mpfr_bandmatrix_init(M_K_fr);
  mpfr_bandmatrix_QRdecomp_t N_K_fr;
  mpfr_bandmatrix_QRdecomp_init(N_K_fr);

  // matrix representation of I-K
  chebmodel_intop_get_mpfi_chebpoly_intop(K_fi, K);
  mpfi_chebpoly_intop_get_bandmatrix(M_K_fi, K_fi, N);
  for (i = 0 ; i <= N ; i++)
    mpfi_sub_si(M_K_fi->D[i] + d, M_K_fi->D[i] + d, 1);
  mpfi_bandmatrix_neg(M_K_fi, M_K_fi);
  mpfi_bandmatrix_get_mpfr_bandmatrix(M_K_fr, M_K_fi);

  // approx inverse
  mpfr_bandmatrix_get_QRdecomp(N_K_fr, M_K_fr);
  mpfr_bandmatrix_QRdecomp_approx_band_inverse(M_K_inv, N_K_fr, h2, d2);

  // compute the bounds of the operator
  int flag = chebmodel_intop_newton_bound_fr(bound, K, K_fi, M_K_fi, M_K_inv);

  printf("bound = %f\n[flag=%d]\n", mpfr_get_d(bound, MPFR_RNDN), flag);

  //clear variables
  mpfi_chebpoly_intop_clear(K_fi);
  mpfi_bandmatrix_clear(M_K_fi);
  mpfr_bandmatrix_clear(M_K_fr);
  mpfr_bandmatrix_QRdecomp_clear(N_K_fr);

}







