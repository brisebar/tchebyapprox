
#include "chebmodel_intop_newton.h"


void chebmodel_intop_newton_solve_d(chebmodel_t P, const chebmodel_intop_t K, const chebmodel_t G, long N)
{
  long r = K->order;
  long i;

  mpfr_chebpoly_intop_t Kfr;
  mpfr_chebpoly_intop_init(Kfr);
  chebmodel_intop_get_mpfr_chebpoly_intop(Kfr, K);
  long s = mpfr_chebpoly_intop_Dwidth(Kfr);

  mpfr_chebpoly_t Gfr;
  mpfr_chebpoly_init(Gfr);
  chebmodel_get_mpfr_chebpoly(Gfr, G);

  // approximate solution
  mpfr_chebpoly_t Sol;
  mpfr_chebpoly_init(Sol);
  mpfr_chebpoly_intop_approxsolve(Sol, Kfr, Gfr, N);

  // validating the solution to get a chebmodel
  mpfr_t bound;
  mpfr_init(bound);
  long init_N = N < 2 * s ? 2 * s : N;
  chebmodel_intop_newton_validate_sol_d(bound, K, G, Sol, init_N);
  chebmodel_set_mpfr_chebpoly(P, Sol);
  mpfr_set(P->rem, bound, MPFR_RNDU);

  // clear variables
  mpfr_chebpoly_intop_clear(Kfr);
  mpfr_chebpoly_clear(Gfr);
  mpfr_chebpoly_clear(Sol);
  mpfr_clear(bound);
}


void chebmodel_intop_newton_solve_fr(chebmodel_t P, const chebmodel_intop_t K, const chebmodel_t G, long N)
{
  long r = K->order;
  long i;

  mpfr_chebpoly_intop_t Kfr;
  mpfr_chebpoly_intop_init(Kfr);
  chebmodel_intop_get_mpfr_chebpoly_intop(Kfr, K);
  long s = mpfr_chebpoly_intop_Dwidth(Kfr);

  mpfr_chebpoly_t Gfr;
  mpfr_chebpoly_init(Gfr);
  chebmodel_get_mpfr_chebpoly(Gfr, G);

  // approximate solution
  mpfr_chebpoly_t Sol;
  mpfr_chebpoly_init(Sol);
  mpfr_chebpoly_intop_approxsolve(Sol, Kfr, Gfr, N);

  // validating the solution to get a chebmodel
  mpfr_t bound;
  mpfr_init(bound);
  long init_N = N < 2 * s ? 2 * s : N;
  chebmodel_intop_newton_validate_sol_fr(bound, K, G, Sol, init_N);
  chebmodel_set_mpfr_chebpoly(P, Sol);
  mpfr_set(P->rem, bound, MPFR_RNDU);

  // clear variables
  mpfr_chebpoly_intop_clear(Kfr);
  mpfr_chebpoly_clear(Gfr);
  mpfr_chebpoly_clear(Sol);
  mpfr_clear(bound);
}


void chebmodel_lode_intop_newton_solve_d(chebmodel_ptr Sols, const chebmodel_lode_t L, const chebmodel_t g, mpfi_srcptr I, long N)
{
  long r = L->order;
  long i;

  // creating integral operator intop
  chebmodel_intop_t K;
  chebmodel_intop_init(K);
  chebmodel_intop_set_lode(K, L);

  // constructing rhs
  chebmodel_t G;
  chebmodel_init(G);
  chebmodel_intop_rhs(G, L, g, I);

  // solving for the r-th derivative using intop
  chebmodel_intop_newton_solve_d(Sols + r, K, G, N);

  // computing the other derivatives
  chebmodel_zero(G);
  mpfi_chebpoly_set_degree(G->poly, 0);
  for (i = r - 1 ; i >= 0 ; i--) {
    chebmodel_antiderivative_1(Sols + i, Sols + i + 1);
    mpfi_chebpoly_set_coeff_fi(G->poly, 0, I + i);
    chebmodel_add(Sols + i, Sols + i, G);
  }

  // clear variables
  chebmodel_intop_clear(K);
  chebmodel_clear(G);
}


void chebmodel_lode_intop_newton_solve_fr(chebmodel_ptr Sols, const chebmodel_lode_t L, const chebmodel_t g, mpfi_srcptr I, long N)
{
  long r = L->order;
  long i;

  // creating integral operator intop
  chebmodel_intop_t K;
  chebmodel_intop_init(K);
  chebmodel_intop_set_lode(K, L);

  // constructing rhs
  chebmodel_t G;
  chebmodel_init(G);
  chebmodel_intop_rhs(G, L, g, I);

  // solving for the r-th derivative using intop
  chebmodel_intop_newton_solve_fr(Sols + r, K, G, N);

  // computing the other derivatives
  chebmodel_zero(G);
  mpfi_chebpoly_set_degree(G->poly, 0);
  for (i = r - 1 ; i >= 0 ; i--) {
    chebmodel_antiderivative_1(Sols + i, Sols + i + 1);
    mpfi_chebpoly_set_coeff_fi(G->poly, 0, I + i);
    chebmodel_add(Sols + i, Sols + i, G);
  }

  // clear variables
  chebmodel_intop_clear(K);
  chebmodel_clear(G);
}

