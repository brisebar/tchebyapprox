
#include "chebmodel_vec.h"


void chebmodel_vec_antiderivative(chebmodel_vec_t P, const chebmodel_vec_t Q)
{
  long n = Q->dim;
  long i;
  chebmodel_vec_set_dim(P, n);
  for (i = 0 ; i < n ; i++)
    chebmodel_antiderivative(P->poly + i, Q->poly + i);
}


void chebmodel_vec_antiderivative0(chebmodel_vec_t P, const chebmodel_vec_t Q)
{
  long n = Q->dim;
  long i;
  chebmodel_vec_set_dim(P, n);
  for (i = 0 ; i < n ; i++)
    chebmodel_antiderivative0(P->poly + i, Q->poly + i);
}


void chebmodel_vec_antiderivative_1(chebmodel_vec_t P, const chebmodel_vec_t Q)
{
  long n = Q->dim;
  long i;
  chebmodel_vec_set_dim(P, n);
  for (i = 0 ; i < n ; i++)
    chebmodel_antiderivative_1(P->poly + i, Q->poly + i);
}


void chebmodel_vec_antiderivative1(chebmodel_vec_t P, const chebmodel_vec_t Q)
{
  long n = Q->dim;
  long i;
  chebmodel_vec_set_dim(P, n);
  for (i = 0 ; i < n ; i++)
    chebmodel_antiderivative1(P->poly + i, Q->poly + i);
}


