
#include "chebmodel_vec.h"


void chebmodel_vec_set_dim(chebmodel_vec_t P, long n)
{
  long m = P->dim;
  long i;

  if (n < m) {
    for (i = n ; i < m ; i++)
      chebmodel_clear(P->poly + i);
    P->poly = realloc(P->poly, n * sizeof(chebmodel_t));
  }
  else if (n > m) {
    P->poly = realloc(P->poly, n * sizeof(chebmodel_t));
    for (i = m ; i < n ; i++)
      chebmodel_init(P->poly + i);
  }

  P->dim = n;
}
