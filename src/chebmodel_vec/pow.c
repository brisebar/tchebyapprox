
#include "chebmodel_vec.h"


void chebmodel_vec_pow(chebmodel_vec_t P, const chebmodel_vec_t Q, unsigned long e)
{
  long n = Q->dim;
  long i;

  chebmodel_vec_set_dim(P, n);
  for (i = 0 ; i < n ; i++)
    chebmodel_pow(P->poly + i, Q->poly + i, e);

}

