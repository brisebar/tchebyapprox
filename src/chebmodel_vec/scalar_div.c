
#include "chebmodel_vec.h"


void chebmodel_vec_scalar_div_si(chebmodel_vec_t P, const chebmodel_vec_t Q, long c)
{
  long n = Q->dim;
  long i;
  chebmodel_vec_set_dim(P, n);
  for (i = 0 ; i < n ; i++)
    chebmodel_scalar_div_si(P->poly + i, Q->poly + i, c);
}


void chebmodel_vec_scalar_div_z(chebmodel_vec_t P, const chebmodel_vec_t Q, const mpz_t c)
{
  long n = Q->dim;
  long i;
  chebmodel_vec_set_dim(P, n);
  for (i = 0 ; i < n ; i++)
    chebmodel_scalar_div_z(P->poly + i, Q->poly + i, c);
}


void chebmodel_vec_scalar_div_q(chebmodel_vec_t P, const chebmodel_vec_t Q, const mpq_t c)
{
  long n = Q->dim;
  long i;
  chebmodel_vec_set_dim(P, n);
  for (i = 0 ; i < n ; i++)
    chebmodel_scalar_div_q(P->poly + i, Q->poly + i, c);
}


void chebmodel_vec_scalar_div_fr(chebmodel_vec_t P, const chebmodel_vec_t Q, const mpfr_t c)
{
  long n = Q->dim;
  long i;
  chebmodel_vec_set_dim(P, n);
  for (i = 0 ; i < n ; i++)
    chebmodel_scalar_div_fr(P->poly + i, Q->poly + i, c);
}


void chebmodel_vec_scalar_div_fi(chebmodel_vec_t P, const chebmodel_vec_t Q, const mpfi_t c)
{
  long n = Q->dim;
  long i;
  chebmodel_vec_set_dim(P, n);
  for (i = 0 ; i < n ; i++)
    chebmodel_scalar_div_fi(P->poly + i, Q->poly + i, c);
}


void chebmodel_vec_scalar_div_2si(chebmodel_vec_t P, const chebmodel_vec_t Q, long k)
{
  long n = Q->dim;
  long i;
  chebmodel_vec_set_dim(P, n);
  for (i = 0 ; i < n ; i++)
    chebmodel_scalar_div_2si(P->poly + i, Q->poly + i, k);
}



