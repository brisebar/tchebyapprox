
#include "chebmodel_vec.h"


void chebmodel_vec_zero(chebmodel_vec_t P)
{
  long n = P->dim;
  long i;
  for (i = 0 ; i < n ; i++)
    chebmodel_zero(P->poly + i);
}
