
#include "chebmodel_vec.h"


void chebmodel_vec_set(chebmodel_vec_t P, const chebmodel_vec_t Q)
{
  long n = Q->dim;
  long i;
  if (P != Q) {
    chebmodel_vec_set_dim(P, n);
    for (i = 0 ; i < n ; i++)
      chebmodel_set(P->poly + i, Q->poly + i);
  }
}


void chebmodel_vec_set_double_chebpoly_vec(chebmodel_vec_t P, const double_chebpoly_vec_t Q)
{
  long n = Q->dim;
  long i;
  chebmodel_vec_set_dim(P, n);
  for (i = 0 ; i < n ; i++)
    chebmodel_set_double_chebpoly(P->poly + i, Q->poly + i);
}


void chebmodel_vec_set_mpfr_chebpoly_vec(chebmodel_vec_t P, const mpfr_chebpoly_vec_t Q)
{
  long n = Q->dim;
  long i;
  chebmodel_vec_set_dim(P, n);
  for (i = 0 ; i < n ; i++)
    chebmodel_set_mpfr_chebpoly(P->poly + i, Q->poly + i);
}


void chebmodel_vec_set_mpfi_chebpoly_vec(chebmodel_vec_t P, const mpfi_chebpoly_vec_t Q)
{
  long n = Q->dim;
  long i;
  chebmodel_vec_set_dim(P, n);
  for (i = 0 ; i < n ; i++)
    chebmodel_set_mpfi_chebpoly(P->poly + i, Q->poly + i);
}


void chebmodel_vec_get_double_chebpoly_vec(double_chebpoly_vec_t P, const chebmodel_vec_t Q)
{
  long n = Q->dim;
  long i;
  double_chebpoly_vec_set_dim(P, n);
  for (i = 0 ; i < n ; i++)
    chebmodel_get_double_chebpoly(P->poly + i, Q->poly + i);
}


void chebmodel_vec_get_mpfr_chebpoly_vec(mpfr_chebpoly_vec_t P, const chebmodel_vec_t Q)
{
  long n = Q->dim;
  long i;
  mpfr_chebpoly_vec_set_dim(P, n);
  for (i = 0 ; i < n ; i++)
    chebmodel_get_mpfr_chebpoly(P->poly + i, Q->poly + i);
}


void chebmodel_vec_get_mpfi_chebpoly_vec(mpfi_chebpoly_vec_t P, const chebmodel_vec_t Q)
{
  long n = Q->dim;
  long i;
  mpfi_chebpoly_vec_set_dim(P, n);
  for (i = 0 ; i < n ; i++)
    chebmodel_get_mpfi_chebpoly(P->poly + i, Q->poly + i);
}
