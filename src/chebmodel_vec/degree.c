
#include "chebmodel_vec.h"


long chebmodel_vec_degree(const chebmodel_vec_t P)
{
  long deg = -1;
  long n = P->dim;
  long i;
  for (i = 0 ; i < n ; i++)
    if (P->poly[i].poly->degree > deg)
      deg = P->poly[i].poly->degree;
  return deg;
}
