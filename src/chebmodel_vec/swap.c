
#include "chebmodel_vec.h"


void chebmodel_vec_swap(chebmodel_vec_t P, chebmodel_vec_t Q)
{
  long dim_buf = P->dim;
  chebmodel_ptr poly_buf = P->poly;
  P->dim = Q->dim;
  P->poly = Q->poly;
  Q->dim = dim_buf;
  Q->poly = poly_buf;
}
