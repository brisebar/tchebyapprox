
#include "chebmodel_vec.h"


void chebmodel_vec_add(chebmodel_vec_t P, const chebmodel_vec_t Q, const chebmodel_vec_t R)
{
  long n = Q->dim;
  long i;

  if (n != R->dim) {
    fprintf(stderr, "chebmodel_vec_add: error: incompatible dimensions\n");
    return;
  }

  chebmodel_vec_set_dim(P, n);
  for (i = 0 ; i < n ; i++)
    chebmodel_add(P->poly + i, Q->poly + i, R->poly + i);

}

