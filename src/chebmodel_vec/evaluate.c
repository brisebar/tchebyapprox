
#include "chebmodel_vec.h"


void chebmodel_vec_evaluate_d(mpfi_vec_t Y, const chebmodel_vec_t P, const double_t x)
{
  long n = P->dim;
  long i;
  mpfi_vec_set_length(Y, n);
  for (i = 0 ; i < n ; i++)
    chebmodel_evaluate_d(Y->coeffs + i, P->poly + i, x);
}


void chebmodel_vec_evaluate_fr(mpfi_vec_t Y, const chebmodel_vec_t P, const mpfr_t x)
{
  long n = P->dim;
  long i;
  mpfi_vec_set_length(Y, n);
  for (i = 0 ; i < n ; i++)
    chebmodel_evaluate_fr(Y->coeffs + i, P->poly + i, x);
}


void chebmodel_vec_evaluate_fi(mpfi_vec_t Y, const chebmodel_vec_t P, const mpfi_t x)
{
  long n = P->dim;
  long i;
  mpfi_vec_set_length(Y, n);
  for (i = 0 ; i < n ; i++)
    chebmodel_evaluate_fi(Y->coeffs + i, P->poly + i, x);
}
