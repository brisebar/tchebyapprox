
#include "chebmodel_vec.h"


void chebmodel_vec_scalar_prod(chebmodel_t P, const chebmodel_vec_t Q, const chebmodel_vec_t R)
{
  long n = Q->dim;
  long i;

  if (n != R->dim) {
    fprintf(stderr, "chebmodel_vec_scalar_prod: error: incompatible dimensions\n");
    return;
  }

  chebmodel_zero(P);
  chebmodel_t Temp;
  chebmodel_init(Temp);
  for (i = 0 ; i < n ; i++) {
    chebmodel_mul(Temp, Q->poly + i, R->poly + i);
    chebmodel_add(P, P, Temp);
  }

  chebmodel_clear(Temp);
}

