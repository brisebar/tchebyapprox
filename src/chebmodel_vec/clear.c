
#include "chebmodel_vec.h"


void chebmodel_vec_clear(chebmodel_vec_t P)
{
  long i;
  long n = P->dim;
  for (i = 0 ; i < n ; i++)
    chebmodel_clear(P->poly + i);
  free(P->poly);
}
