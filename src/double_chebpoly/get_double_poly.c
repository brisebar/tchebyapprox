
#include "double_chebpoly.h"


void _double_chebpoly_get_double_poly(double_ptr P, double_srcptr Q, long n)
{
  double_set(P + 0, Q + 0, MPFR_RNDN); // P = Q_0

  long i;
  double_ptr Ti = malloc((n+2) * sizeof(double_t));
  double_ptr Tip1 = malloc((n+2) * sizeof(double_t));
  double_ptr Temp = malloc((n+2) * sizeof(double_t));
  for (i = 0 ; i <= n+1 ; i++) {
    double_init(Ti + i);
    double_set_si(Ti + i, 0, MPFR_RNDN);
    double_init(Tip1 + i);
    double_set_si(Tip1 + i, 0, MPFR_RNDN);
    double_init(Temp + i);
    double_set_si(Temp + i, 0, MPFR_RNDN);
  }
  double_set_si(Ti, 1, MPFR_RNDN); // T0 = 1
  double_set_si(Tip1 + 1, 1, MPFR_RNDN); // T1 = X
  
  for (i = 0 ; i < n ; i++) {

    // P += Q_{i+1} * T_{i+1}
    _double_poly_scalar_mul_d(Temp, Tip1, i+1, Q + i + 1);
    double_set_si(P + i + 1, 0, MPFR_RNDN);
    _double_poly_add(P, P, i+1, Temp, i+1);
      
    // (Ti, Tip1) -> (Tip1, 2*X*Tip1-Ti)
    _double_poly_scalar_mul_2si(Temp, Tip1, i+1, 1);
    _double_poly_shift_left(Temp, Temp, i+1, 1);
    _double_poly_sub(Temp, Temp, i+2, Ti, i);
    _double_poly_set(Ti, Tip1, i+1);
    _double_poly_set(Tip1, Temp, i+2);

  }

  // clear variables
  for (i = 0 ; i <= n+1 ; i++) {
    double_clear(Ti + i);
    double_clear(Tip1 + i);
    double_clear(Temp + i);
  }
}


// set in P the conversion of Q (in the Chebyshev basis) into the monomial basis
void double_chebpoly_get_double_poly(double_poly_t P, const double_chebpoly_t Q)
{
  long n = Q->degree;

  if (n < 0)
    double_poly_zero(P);

  else {
    double_poly_set_degree(P, n);
    _double_chebpoly_get_double_poly(P->coeffs, Q->coeffs, n);
  }
}
