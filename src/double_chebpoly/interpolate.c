
#include "double_chebpoly.h"


void _double_chebpoly_interpolate(double_ptr P, double_srcptr F, long n)
{
  long i, j, k;

  // X[j] = cos(pi*j/(2n+2)) for 0 <= j < 4n+4 
  double * X = malloc((4*n+4) * sizeof(double));
  for (j = 0 ; j < 4*n+4 ; j++)
    X[j] = cos(M_PI * j / (2*n+2));

  // FF is a copy of F
  double * FF = malloc((n+1) * sizeof(double));
  for (k = 0 ; k <= n ; k++)
    FF[k] = F[k];

  // P[i] = (2* if i>0) 1/(n+1) sum_{0 <= k <= n} F[k] X[(2k+1)i mod (4n+4)]
  for (i = 0 ; i <= n ; i++) {
    P[i] = 0;
    for (k = 0 ; k <= n ; k++)
      P[i] += FF[k] * X[((2*k+1)*i) % (4*n+4)]; 
    P[i] /= (n+1);
    if (i > 0)
      P[i] *= 2;    
  }

  free(X);
  free(FF);
}


void double_chebpoly_interpolate(double_chebpoly_t P, const double_vec_t F)
{
  long n = F->length;
  
  if (n == 0)
    double_chebpoly_zero(P);

  else {
    double_chebpoly_set_degree(P, n-1);
    _double_chebpoly_interpolate(P->coeffs, F->coeffs, n-1);
  } 
}
