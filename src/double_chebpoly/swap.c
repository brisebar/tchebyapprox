
#include "double_chebpoly.h"


// swap P and Q efficiently
void double_chebpoly_swap(double_chebpoly_t P, double_chebpoly_t Q)
{
  long degree_buf = P->degree;
  double_ptr coeffs_buf = P->coeffs;
  P->degree = Q->degree;
  P->coeffs = Q->coeffs;
  Q->degree = degree_buf;
  Q->coeffs = coeffs_buf;
}
