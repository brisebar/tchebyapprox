
#include "double_chebpoly.h"


// set P to (1/c)*Q where c is the leading coefficient of P
void double_chebpoly_monic(double_chebpoly_t P, const double_chebpoly_t Q)
{
  long n = Q->degree;

  if (n < 0)
    double_chebpoly_zero(P);

  else {
    double_chebpoly_set_degree(P, n);
    _double_chebpoly_monic(P->coeffs, Q->coeffs, n);
  }
}
