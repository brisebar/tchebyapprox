
#include "double_chebpoly.h"


// set P to 0
void double_chebpoly_zero(double_chebpoly_t P)
{
  long n = P->degree;
  long i ;
  for (i = 0 ; i <= n ; i++)
    double_clear(P->coeffs + i);
  P->coeffs = realloc(P->coeffs, 0);
  P->degree = -1;
}
