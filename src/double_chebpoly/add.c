
#include "double_chebpoly.h"


// set P := Q + R
void double_chebpoly_add(double_chebpoly_t P, const double_chebpoly_t Q, const double_chebpoly_t R)
{
  long n = Q->degree;
  long m = R->degree;
  long max_n_m = n < m ? m : n;

  double_chebpoly_set_degree(P, max_n_m);
  _double_chebpoly_add(P->coeffs, Q->coeffs, n, R->coeffs, m);
  double_chebpoly_normalise(P);
}
