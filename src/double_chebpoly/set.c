
#include "double_chebpoly.h"


// copy Q into P
void double_chebpoly_set(double_chebpoly_t P, const double_chebpoly_t Q)
{
  long n = Q->degree;
  long i;
  if (P != Q) {
    double_chebpoly_set_degree(P, n);
    for (i = 0 ; i <= n ; i++)
      double_set(P->coeffs + i, Q->coeffs + i, MPFR_RNDN);
  }
}
