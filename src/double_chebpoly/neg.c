
#include "double_chebpoly.h"


// set P := -Q
void double_chebpoly_neg(double_chebpoly_t P, const double_chebpoly_t Q)
{
  double_chebpoly_set_degree(P, Q->degree);
  _double_chebpoly_neg(P->coeffs, Q->coeffs, Q->degree);
}
