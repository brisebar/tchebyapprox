
#include "double_chebpoly.h"


void double_chebpoly_1norm(double_t y, const double_chebpoly_t P)
{
  _double_chebpoly_1norm(y, P->coeffs, P->degree);
}

void double_chebpoly_1norm_ubound(double_t y, const double_chebpoly_t P)
{
  _double_chebpoly_1norm_ubound(y, P->coeffs, P->degree);
}

void double_chebpoly_1norm_lbound(double_t y, const double_chebpoly_t P)
{
  _double_chebpoly_1norm_lbound(y, P->coeffs, P->degree);
}

void double_chebpoly_1norm_fi(mpfi_t y, const double_chebpoly_t P)
{
  _double_chebpoly_1norm_fi(y, P->coeffs, P->degree);
}

