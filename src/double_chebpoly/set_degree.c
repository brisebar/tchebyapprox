
#include "double_chebpoly.h"


// set n as degree for P
void double_chebpoly_set_degree(double_chebpoly_t P, long n)
{
  if (n < -1)
    n = -1;
  long m = P->degree;
  long i;

  if (n < m) {
    for (i = n+1 ; i <= m ; i++)
      double_clear(P->coeffs + i);
    P->coeffs = realloc(P->coeffs, (n+1) * sizeof(double_t));
  }
  else if (n > m) {
    P->coeffs = realloc(P->coeffs, (n+1) * sizeof(double_t));
    for (i = m+1 ; i <= n ; i++) {
      double_init(P->coeffs + i);
      double_set_si(P->coeffs + i, 0, MPFR_RNDN);
    }
  }

  P->degree = n;
}
