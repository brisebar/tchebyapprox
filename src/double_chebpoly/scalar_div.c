
#include "double_chebpoly.h"


// set P := (1/c)*Q
void double_chebpoly_scalar_div_si(double_chebpoly_t P, const double_chebpoly_t Q, long c)
{
  long n = Q->degree;
  double_chebpoly_set_degree(P, n);
  _double_chebpoly_scalar_div_si(P->coeffs, Q->coeffs, n, c);
}


// set P := (1/c)*Q
void double_chebpoly_scalar_div_z(double_chebpoly_t P, const double_chebpoly_t Q, const mpz_t c)
{
  long n = Q->degree;
  double_chebpoly_set_degree(P, n);
  _double_chebpoly_scalar_div_z(P->coeffs, Q->coeffs, n, c);
}


// set P := (1/c)*Q
void double_chebpoly_scalar_div_q(double_chebpoly_t P, const double_chebpoly_t Q, const mpq_t c)
{
  long n = Q->degree;
  double_chebpoly_set_degree(P, n);
  _double_chebpoly_scalar_div_q(P->coeffs, Q->coeffs, n, c);
}


// set P := (1/c)*Q
void double_chebpoly_scalar_div_d(double_chebpoly_t P, const double_chebpoly_t Q, const double_t c)
{
  long n = Q->degree;
  double_chebpoly_set_degree(P, n);
  _double_chebpoly_scalar_div_d(P->coeffs, Q->coeffs, n, c);
}

// set P := (1/c)*Q
void double_chebpoly_scalar_div_fr(double_chebpoly_t P, const double_chebpoly_t Q, const mpfr_t c)
{
  long n = Q->degree;
  double_chebpoly_set_degree(P, n);
  _double_chebpoly_scalar_div_fr(P->coeffs, Q->coeffs, n, c);
}

// set P := 2^-k*Q
void double_chebpoly_scalar_div_2si(double_chebpoly_t P, const double_chebpoly_t Q, long k)
{
  long n = Q->degree;
  double_chebpoly_set_degree(P, n);
  _double_chebpoly_scalar_div_2si(P->coeffs, Q->coeffs, n, k);
}
