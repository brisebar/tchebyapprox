
#include "double_chebpoly.h"




// set the n-th coefficient of P to c
void double_chebpoly_set_coeff_si(double_chebpoly_t P, long n, long c)
{
  if (n < 0)
    return;

  else if (n <= P->degree) {
    double_set_si(P->coeffs + n, c, MPFR_RNDN);
    if (n == P->degree && c == 0)
      double_chebpoly_normalise(P);
  }

  else if (c != 0) {
    P->coeffs = realloc(P->coeffs, (n+1) * sizeof(double_t));
    long i;
    for (i = P->degree + 1 ; i <= n ; i++) {
      double_init(P->coeffs + i);
      double_set_si(P->coeffs + i, 0, MPFR_RNDN);
    }
    double_set_si(P->coeffs + n, c, MPFR_RNDN);
    P->degree = n;
  }

  else
    return;
}


// set the n-th coefficient of P to c
void double_chebpoly_set_coeff_z(double_chebpoly_t P, long n, const mpz_t c)
{
  if (n < 0)
    return;

  else if (n <= P->degree) {
    double_set_z(P->coeffs + n, c, MPFR_RNDN);
    if (n == P->degree && !mpz_sgn(c))
      double_chebpoly_normalise(P);
  }

  else if (mpz_sgn(c)) {
    P->coeffs = realloc(P->coeffs, (n+1) * sizeof(double_t));
    long i;
    for (i = P->degree + 1 ; i <= n ; i++) {
      double_init(P->coeffs + i);
      double_set_si(P->coeffs + i, 0, MPFR_RNDN);
    }
    double_set_z(P->coeffs + n, c, MPFR_RNDN);
    P->degree = n;
  }

  else
    return;
}


// set the n-th coefficient of P to c
void double_chebpoly_set_coeff_q(double_chebpoly_t P, long n, const mpq_t c)
{
  if (n < 0)
    return;

  else if (n <= P->degree) {
    double_set_q(P->coeffs + n, c, MPFR_RNDN);
    if (n == P->degree && !mpq_sgn(c))
      double_chebpoly_normalise(P);
  }

  else if (mpq_sgn(c)) {
    P->coeffs = realloc(P->coeffs, (n+1) * sizeof(double_t));
    long i;
    for (i = P->degree + 1 ; i <= n ; i++) {
      double_init(P->coeffs + i);
      double_set_si(P->coeffs + i, 0, MPFR_RNDN);
    }
    double_set_q(P->coeffs + n, c, MPFR_RNDN);
    P->degree = n;
  }

  else
    return;
}

// set the n-th coefficient of P to c
void double_chebpoly_set_coeff_d(double_chebpoly_t P, long n, const double_t c)
{
  if (n < 0)
    return;

  else if (n <= P->degree) {
    double_set(P->coeffs + n, c, MPFR_RNDN);
    if (n == P->degree && double_zero_p(c))
      double_chebpoly_normalise(P);
  }

  else if (!double_zero_p(c)) {
    P->coeffs = realloc(P->coeffs, (n+1) * sizeof(double_t));
    long i;
    for (i = P->degree + 1 ; i <= n ; i++) {
      double_init(P->coeffs + i);
      double_set_si(P->coeffs + i, 0, MPFR_RNDN);
    }
    double_set(P->coeffs + n, c, MPFR_RNDN);
    P->degree = n;
  }

  else
    return;
}



// set the n-th coefficient of P to c
void double_chebpoly_set_coeff_fr(double_chebpoly_t P, long n, const mpfr_t c)
{
  if (n < 0)
    return;

  else if (n <= P->degree) {
    double_set_fr(P->coeffs + n, c, MPFR_RNDN);
    if (n == P->degree && mpfr_zero_p(c))
      double_chebpoly_normalise(P);
  }

  else if (!mpfr_zero_p(c)) {
    P->coeffs = realloc(P->coeffs, (n+1) * sizeof(double_t));
    long i;
    for (i = P->degree + 1 ; i <= n ; i++) {
      double_init(P->coeffs + i);
      double_set_si(P->coeffs + i, 0, MPFR_RNDN);
    }
    double_set_fr(P->coeffs + n, c, MPFR_RNDN);
    P->degree = n;
  }

  else
    return;
}



