
#include "double_chebpoly.h"


// get the n-th coefficient of P
void double_chebpoly_get_coeff(double_t c, const double_chebpoly_t P, long n)
{
  if (n < 0 || n > P->degree)
    double_set_si(c, 0, MPFR_RNDN);
  else
    double_set(c, P->coeffs + n, MPFR_RNDN);
}
