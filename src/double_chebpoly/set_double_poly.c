
#include "double_chebpoly.h"


void _double_chebpoly_set_double_poly(double_ptr P, double_srcptr Q, long n)
{
  long i, j;
  double_t prev, temp;
  double_init(prev);
  double_init(temp);

  double_ptr R = malloc((n+1) * sizeof(double_t));
  for (i = 0 ; i <= n ; i++) {
    double_init(R + i);
    double_set_si(R + i, 0, MPFR_RNDN);
  }
  
  double_set(R + 0, Q + n, MPFR_RNDN); // R = Q_n

  for (i = 1 ; i <= n ; i++) {

    // multiplication by X
    for (j = 1 ; j < i ; j++)
      double_mul_2si(R + j, R + j, -1, MPFR_RNDN);
    double_set(prev, R + 0, MPFR_RNDN);
    double_set_si(R + 0, 0, MPFR_RNDN);
    for (j = 1 ; j < i ; j++) {
      double_add(R + j - 1, R + j - 1, R + j, MPFR_RNDN);
      double_set(temp, R + j, MPFR_RNDN);
      double_set(R + j, prev, MPFR_RNDN);
      double_set(prev, temp, MPFR_RNDN);
    }
    double_set(R + i, prev, MPFR_RNDN);

    // add Q[n-i]
    double_add(R, R, Q + n - i, MPFR_RNDN);

  }

  for (i = 0 ; i <= n ; i++) {
    double_swap(P + i, R + i);
    double_clear(R + i);
  }
  free(R);
  double_clear(prev);
  double_clear(temp);
}


// set in P the conversion of Q (in the monomial basis) into the Chebyshev basis
void double_chebpoly_set_double_poly(double_chebpoly_t P, const double_poly_t Q)
{
  long n = Q->degree;

  if (n < 0)
    double_chebpoly_zero(P);

  else {
    double_chebpoly_set_degree(P, n);
    _double_chebpoly_set_double_poly(P->coeffs, Q->coeffs, n);
  }
}
