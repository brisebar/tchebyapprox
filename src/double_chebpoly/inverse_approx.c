
#include "double_chebpoly.h"


// computes F of degree N st F ~ 1/Q
void double_chebpoly_inverse_approx(double_chebpoly_t F, const double_chebpoly_t Q, long N)
{
  long m = Q->degree;
  long i, j, k;
  double_t temp;
  double_init(temp);

  // compute the values cos(i*pi/(2*N+2)) for 0 <= i < 4*N+4
  double_ptr X = malloc((4*N+4) * sizeof(double_t));
  for (i = 0 ; i < 4*N+4 ; i++) {
    double_init(X + i);
    double_const_pi(temp, MPFR_RNDN);
    double_mul_si(temp, temp, i, MPFR_RNDN);
    double_div_si(temp, temp, 2*N+2, MPFR_RNDN);
    double_cos(X + i, temp, MPFR_RNDN);
  }

  // compute the coefficients of F
  double_t Q_X_k;
  double_init(Q_X_k);
  double_ptr Fcoeffs = malloc((N+1) * sizeof(double_t));
  for (i = 0 ; i <= N ; i++) {
    double_init(Fcoeffs + i);
    double_set_si(Fcoeffs + i, 0, MPFR_RNDN);
  }

  for (k = 0 ; k <= N ; k++) {

    // compute in Q_X_k the value of Q(X + (2k+1)*pi/(2*N+2))
    double_set_si(Q_X_k, 0, MPFR_RNDN);
    for (j = 0 ; j <= m && j <= N ; j++) {
      double_mul(temp, Q->coeffs + j, X + (j * (2*k+1)) % (4*N+4), MPFR_RNDN);
      double_add(Q_X_k, Q_X_k, temp, MPFR_RNDN);
    }

    // use these values to compute the coefficients of F
    for (i = 0 ; i <= N ; i++) {
      double_div(temp, X + (i * (2*k+1)) % (4*N+4), Q_X_k, MPFR_RNDN);
      double_add(Fcoeffs + i, Fcoeffs + i, temp, MPFR_RNDN);
    }

  }

  for (i = 0 ; i <= N ; i++) {
    double_div_si(Fcoeffs + i, Fcoeffs + i, N+1, MPFR_RNDN);
    if (i > 0)
      double_mul_2si(Fcoeffs + i, Fcoeffs + i, 1, MPFR_RNDN);
  }

  // store them into F
  double_chebpoly_clear(F);
  F->degree = N;
  F->coeffs = Fcoeffs;
  double_chebpoly_normalise(F);

  // clear variables
  double_clear(temp);
  double_clear(Q_X_k);
  for (i = 0 ; i < 4*N+4 ; i++)
    double_clear(X + i);
  free(X);

}



