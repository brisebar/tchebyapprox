
#include "double_chebpoly.h"


void double_chebpoly_comp(double_chebpoly_t P, const double_chebpoly_t Q, const double_chebpoly_t R)
{
  long n = Q->degree;

  if (n <= 0)
    double_chebpoly_set(P, Q);

  else {
    
    double_chebpoly_t A, B, Temp;
    double_chebpoly_init(A);
    double_chebpoly_init(B);
    double_chebpoly_init(Temp);
    double_chebpoly_set_degree(A, 0);
    double_set(A->coeffs + 0, Q->coeffs + n - 1, MPFR_RNDN);
    double_chebpoly_set_degree(B, 0);
    double_set(B->coeffs + 0, Q->coeffs + n, MPFR_RNDN);
    long i;
    
    for (i = n-2 ; i >= 0 ; i--) {
      // (A,B) -> (P[i]-B, A+2*R*B)
      // new value for A
      double_chebpoly_swap(Temp, A);
      if (B->degree < 0) {
	double_chebpoly_set_degree(A, 0);
	double_set(A->coeffs + 0, Q->coeffs + i, MPFR_RNDN);
      }
      else {
	double_chebpoly_neg(A, B);
	double_add(A->coeffs + 0, A->coeffs + 0, Q->coeffs + i, MPFR_RNDN);
      }
      // new value for B
      double_chebpoly_mul(B, B, R);
      double_chebpoly_scalar_mul_2si(B, B, 1);
      double_chebpoly_add(B, B, Temp);
    }
    
    // P = A+R*B
    double_chebpoly_mul(P, R, B);
    double_chebpoly_add(P, P, A);
    
    // clear variables
    double_chebpoly_clear(Temp);
    double_chebpoly_clear(A);
    double_chebpoly_clear(B);
  } 

}


