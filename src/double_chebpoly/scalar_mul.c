
#include "double_chebpoly.h"


// set P := c*Q
void double_chebpoly_scalar_mul_si(double_chebpoly_t P, const double_chebpoly_t Q, long c)
{
  if (c == 0)
    double_chebpoly_zero(P);
  else {
    long n = Q->degree;
    double_chebpoly_set_degree(P, n);
    _double_chebpoly_scalar_mul_si(P->coeffs, Q->coeffs, n, c);
  }
}


// set P := c*Q
void double_chebpoly_scalar_mul_z(double_chebpoly_t P, const double_chebpoly_t Q, const mpz_t c)
{
  if (!mpz_sgn(c))
    double_chebpoly_zero(P);
  else {
    long n = Q->degree;
    double_chebpoly_set_degree(P, n);
    _double_chebpoly_scalar_mul_z(P->coeffs, Q->coeffs, n, c);
  }
}


// set P := c*Q
void double_chebpoly_scalar_mul_q(double_chebpoly_t P, const double_chebpoly_t Q, const mpq_t c)
{
  if (!mpq_sgn(c))
    double_chebpoly_zero(P);
  else {
    long n = Q->degree;
    double_chebpoly_set_degree(P, n);
    _double_chebpoly_scalar_mul_q(P->coeffs, Q->coeffs, n, c);
  }
}


// set P := c*Q
void double_chebpoly_scalar_mul_d(double_chebpoly_t P, const double_chebpoly_t Q, const double_t c)
{
  if (double_zero_p(c))
    double_chebpoly_zero(P);
  else {
    long n = Q->degree;
    double_chebpoly_set_degree(P, n);
    _double_chebpoly_scalar_mul_d(P->coeffs, Q->coeffs, n, c);
  }
}

// set P := c*Q
void double_chebpoly_scalar_mul_fr(double_chebpoly_t P, const double_chebpoly_t Q, const mpfr_t c)
{
  if (mpfr_zero_p(c))
    double_chebpoly_zero(P);
  else {
    long n = Q->degree;
    double_chebpoly_set_degree(P, n);
    _double_chebpoly_scalar_mul_fr(P->coeffs, Q->coeffs, n, c);
  }
}



// set P := 2^k*Q
void double_chebpoly_scalar_mul_2si(double_chebpoly_t P, const double_chebpoly_t Q, long k)
{
  long n = Q->degree;

  if (n < 0)
    double_chebpoly_zero(P);

  else {
    double_chebpoly_set_degree(P, n);
    _double_chebpoly_scalar_mul_2si(P->coeffs, Q->coeffs, n, k);
  }
}
