
#include "double_chebpoly.h"


// compute F of degree N st F ~ P/Q and G of degree N st G ~ 1/Q
void double_chebpoly_fract_inverse_approx(double_chebpoly_t F, double_chebpoly_t G, const double_chebpoly_t P, const double_chebpoly_t Q, long N)
{
  long n = P->degree;
  long m = Q->degree;
  long i, j, k;
  double_t temp;
  double_init(temp);

  // compute the values cos(i*pi/(2*N+2)) for 0 <= i < 4*N+4
  double_ptr X = malloc((4*N+4) * sizeof(double_t));
  for (i = 0 ; i < 4*N+4 ; i++) {
    double_init(X + i);
    double_const_pi(temp, MPFR_RNDN);
    double_mul_si(temp, temp, i, MPFR_RNDN);
    double_div_si(temp, temp, 2*N+2, MPFR_RNDN);
    double_cos(X + i, temp, MPFR_RNDN);
  }

  // compute the coefficients of F and G
  double_t P_X_k, Q_X_k, P_Q_X_k;
  double_init(P_X_k);
  double_init(Q_X_k);
  double_init(P_Q_X_k);
  double_ptr Fcoeffs = malloc((N+1) * sizeof(double_t));
  double_ptr Gcoeffs = malloc((N+1) * sizeof(double_t));
  for (i = 0 ; i <= N ; i++) {
    double_init(Fcoeffs + i);
    double_set_si(Fcoeffs + i, 0, MPFR_RNDN);
    double_init(Gcoeffs + i);
    double_set_si(Gcoeffs + i, 0, MPFR_RNDN);
  }

  for (k = 0 ; k <= N ; k++) {

    // compute in P_X_k the value of P(X + (2k+1)*pi/(2*N+2))
    double_set_si(P_X_k, 0, MPFR_RNDN);
    for (j = 0 ; j <= n && j <= N ; j++) {
      double_mul(temp, P->coeffs + j, X + (j * (2*k+1)) % (4*N+4), MPFR_RNDN);
      double_add(P_X_k, P_X_k, temp, MPFR_RNDN);
    }

    // compute in Q_X_k the value of Q(X + (2k+1)*pi/(2*N+2))
    double_set_si(Q_X_k, 0, MPFR_RNDN);
    for (j = 0 ; j <= m && j <= N ; j++) {
      double_mul(temp, Q->coeffs + j, X + (j * (2*k+1)) % (4*N+4), MPFR_RNDN);
      double_add(Q_X_k, Q_X_k, temp, MPFR_RNDN);
    }

    // use these values to compute the coefficients of F and G
    double_div(P_Q_X_k, P_X_k, Q_X_k, MPFR_RNDN);
    for (i = 0 ; i <= N ; i++) {
      double_mul(temp, P_Q_X_k, X + (i * (2*k+1)) % (4*N+4), MPFR_RNDN);
      double_add(Fcoeffs + i, Fcoeffs + i, temp, MPFR_RNDN);
      double_div(temp, X + (i * (2*k+1)) % (4*N+4), Q_X_k, MPFR_RNDN);
      double_add(Gcoeffs + i, Gcoeffs + i, temp, MPFR_RNDN);
    }

  }

  for (i = 0 ; i <= N ; i++) {
    double_div_si(Fcoeffs + i, Fcoeffs + i, N+1, MPFR_RNDN);
    double_div_si(Gcoeffs + i, Gcoeffs + i, N+1, MPFR_RNDN);
    if (i > 0) {
      double_mul_2si(Fcoeffs + i, Fcoeffs + i, 1, MPFR_RNDN);
      double_mul_2si(Gcoeffs + i, Gcoeffs + i, 1, MPFR_RNDN);
    }
  }

  // store them into F and G
  double_chebpoly_clear(F);
  F->degree = N;
  F->coeffs = Fcoeffs;
  double_chebpoly_normalise(F);
  double_chebpoly_clear(G);
  G->degree = N;
  G->coeffs = Gcoeffs;
  double_chebpoly_normalise(G);

  // clear variables
  double_clear(temp);
  double_clear(P_X_k);
  double_clear(Q_X_k);
  double_clear(P_Q_X_k);
  for (i = 0 ; i < 4*N+4 ; i++)
    double_clear(X + i);
  free(X);

}



