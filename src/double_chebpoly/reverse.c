
#include "double_chebpoly.h"


// set P to Q(X^-1)*X^(degree(Q))
void double_chebpoly_reverse(double_chebpoly_t P, const double_chebpoly_t Q)
{
  if (P != Q)
    double_chebpoly_set(P, Q);
  _double_chebpoly_reverse(P->coeffs, P->coeffs, P->degree);
  double_chebpoly_normalise(P);
}
