
#include "double_chebpoly.h"


void _double_chebpoly_antiderivative(double_ptr P, double_srcptr Q, long n)
{
  long i;
  
  double_ptr R = malloc((n+2) * sizeof(double_t));
  for (i = 0 ; i <= n+1 ; i++) {
    double_init(R + i);
    double_set_si(R + i, 0, MPFR_RNDN);
  }
  
  // compute R[1]
  double_mul_2si(R + 1, Q, 1, MPFR_RNDN);
  if (n >= 2)
    double_sub(R + 1, R + 1, Q + 2, MPFR_RNDN);
  double_mul_2si(R + 1, R + 1, -1, MPFR_RNDN);
  
  // compute R[i] for i in [2,n-1]
  for (i = 2 ; i < n ; i++) {
    double_sub(R + i, Q + i - 1, Q + i + 1, MPFR_RNDN);
    double_div_si(R + i, R + i, 2*i, MPFR_RNDN);
  }
  
  // compute R[n] (if n >= 2)
  if (n >= 2)
    double_div_si(R + n, Q + n - 1, 2*n, MPFR_RNDN);
  
  // compute R[n+1] (if n >= 1)
  if (n >= 1)
    double_div_si(R + n + 1, Q + n, 2*(n+1), MPFR_RNDN);
  
  for (i = 0 ; i <= n+1 ; i++) {
    double_swap(P + i, R + i);
    double_clear(R + i);
  }
  free(R);
}


// set P st P' = Q and P(0) = 0
void double_chebpoly_antiderivative(double_chebpoly_t P, const double_chebpoly_t Q)
{
  long n = Q->degree;

  if (n < 0)
    double_chebpoly_zero(P);

  else {
    double_chebpoly_set_degree(P, n+1);
    _double_chebpoly_antiderivative(P->coeffs, Q->coeffs, n);
  }
}


// set P st P' = Q and P(0) = 0
void double_chebpoly_antiderivative0(double_chebpoly_t P, const double_chebpoly_t Q)
{
  double_chebpoly_antiderivative(P, Q);
  long n = P->degree;
  long i;
  for (i = 1 ; 2*i <= n ; i++)
    if (i%2)
      double_add(P->coeffs + 0, P->coeffs + 0, P->coeffs + 2*i, MPFR_RNDN);
    else
      double_sub(P->coeffs + 0, P->coeffs + 0, P->coeffs + 2*i, MPFR_RNDN);
}


// set P st P' = Q and P(-1) = 0
void double_chebpoly_antiderivative_1(double_chebpoly_t P, const double_chebpoly_t Q)
{
  double_chebpoly_antiderivative(P, Q);
  long n = P->degree;
  long i;
  for (i = 1 ; i <= n ; i++)
    if (i%2)
      double_add(P->coeffs + 0, P->coeffs + 0, P->coeffs + i, MPFR_RNDN);
    else
      double_sub(P->coeffs + 0, P->coeffs + 0, P->coeffs + i, MPFR_RNDN);
}


// set P st P' = Q and P(1) = 0
void double_chebpoly_antiderivative1(double_chebpoly_t P, const double_chebpoly_t Q)
{
  double_chebpoly_antiderivative(P, Q);
  long n = P->degree;
  long i;
  for (i = 1 ; i <= n ; i++)
    double_sub(P->coeffs + 0, P->coeffs + 0 , P->coeffs + i, MPFR_RNDN);
}
