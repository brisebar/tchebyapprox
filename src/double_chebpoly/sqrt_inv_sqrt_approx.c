
#include "double_chebpoly.h"


// computes F1 and F2 of degree N st F1 ~ sqrt(G) and F2 ~ 1/sqrt(G) 
void double_chebpoly_sqrt_inv_sqrt_approx(double_chebpoly_t F1, double_chebpoly_t F2, const double_chebpoly_t G, long N)
{
  long m = G->degree;
  long i, j, k;
  double_t temp;
  double_init(temp);

  // compute the values cos(i*pi/(2*N+2)) for 0 <= i < 4*N+4
  double_ptr X = malloc((4*N+4) * sizeof(double_t));
  for (i = 0 ; i < 4*N+4 ; i++) {
    double_init(X + i);
    double_const_pi(temp, MPFR_RNDN);
    double_mul_si(temp, temp, i, MPFR_RNDN);
    double_div_si(temp, temp, 2*N+2, MPFR_RNDN);
    double_cos(X + i, temp, MPFR_RNDN);
  }

  // compute the coefficients of F1 and F2
  double_t G_X_k;
  double_init(G_X_k);
  double_ptr F1coeffs = malloc((N+1) * sizeof(double_t));
  double_ptr F2coeffs = malloc((N+1) * sizeof(double_t));
  for (i = 0 ; i <= N ; i++) {
    double_init(F1coeffs + i);
    double_set_si(F1coeffs + i, 0, MPFR_RNDN);
    double_init(F2coeffs + i);
    double_set_si(F2coeffs + i, 0, MPFR_RNDN);
  }

  for (k = 0 ; k <= N ; k++) {

    // compute in G_X_k the value of G(X + (2k+1)*pi/(2*N+2))
    double_set_si(G_X_k, 0, MPFR_RNDN);
    for (j = 0 ; j <= m && j <= N ; j++) {
      double_mul(temp, G->coeffs + j, X + (j * (2*k+1)) % (4*N+4), MPFR_RNDN);
      double_add(G_X_k, G_X_k, temp, MPFR_RNDN);
    }

    // take the square root
    double_sqrt(G_X_k, G_X_k, MPFR_RNDN);

    // use these values to compute the coefficients of F
    for (i = 0 ; i <= N ; i++) {
      double_mul(temp, X + (i * (2*k+1)) % (4*N+4), G_X_k, MPFR_RNDN);
      double_add(F1coeffs + i, F1coeffs + i, temp, MPFR_RNDN);
      double_div(temp, X + (i * (2*k+1)) % (4*N+4), G_X_k, MPFR_RNDN);
      double_add(F2coeffs + i, F2coeffs + i, temp, MPFR_RNDN);
    }

  }

  for (i = 0 ; i <= N ; i++) {
    double_div_si(F1coeffs + i, F1coeffs + i, N+1, MPFR_RNDN);
    double_div_si(F2coeffs + i, F2coeffs + i, N+1, MPFR_RNDN);
    if (i > 0) {
      double_mul_2si(F1coeffs + i, F1coeffs + i, 1, MPFR_RNDN);
      double_mul_2si(F2coeffs + i, F2coeffs + i, 1, MPFR_RNDN);
    }
  }

  // store them into F1 and F2
  double_chebpoly_clear(F1);
  F1->degree = N;
  F1->coeffs = F1coeffs;
  double_chebpoly_normalise(F1);
  double_chebpoly_clear(F2);
  F2->degree = N;
  F2->coeffs = F2coeffs;
  double_chebpoly_normalise(F2);

  // clear variables
  double_clear(temp);
  double_clear(G_X_k);
  for (i = 0 ; i < 4*N+4 ; i++)
    double_clear(X + i);
  free(X);

}



