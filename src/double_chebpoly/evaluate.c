
#include "double_chebpoly.h"


void _double_chebpoly_evaluate_si(double_t y, double_srcptr P, long n, long x)
{
  double_t t;
  double_init(t);
  double_set_si(t, x, MPFR_RNDN);
  _double_chebpoly_evaluate_d(y, P, n, t);
  double_clear(t);
}


void _double_chebpoly_evaluate_z(double_t y, double_srcptr P, long n, const mpz_t x)
{
  double_t t;
  double_init(t);
  double_set_z(t, x, MPFR_RNDN);
  _double_chebpoly_evaluate_d(y, P, n, t);
  double_clear(t);
}


void _double_chebpoly_evaluate_q(double_t y, double_srcptr P, long n, const mpq_t x)
{
  double_t t;
  double_init(t);
  double_set_q(t, x, MPFR_RNDN);
  _double_chebpoly_evaluate_d(y, P, n, t);
  double_clear(t);
}


void _double_chebpoly_evaluate_d(double_t y, double_srcptr P, long n, const double_t x)
{
  if (n < 0)
    double_set_si(y, 0, MPFR_RNDN);

  else if (n == 0)
    double_set(y, P, MPFR_RNDN);

  else {

    double_t c1, c2, temp;
    double_init(temp);
    double_init(c1);
    double_set(c1, P + n - 1, MPFR_RNDN);
    double_init(c2);
    double_set(c2, P + n, MPFR_RNDN);
    long i;

    for (i = n-2 ; i >= 0 ; i--) {
      // (c1,c2) -> (P[i]-c2, c1+2*x*c2)
      // new value of c1
      double_swap(temp, c1);
      double_sub(c1, P + i, c2, MPFR_RNDN);
      // new value of c2
      double_mul(c2, c2, x, MPFR_RNDN);
      double_mul_2si(c2, c2, 1, MPFR_RNDN);
      double_add(c2, c2, temp, MPFR_RNDN);
    }

    // y = P(x) = c1+x*c2
    double_mul(y, x, c2, MPFR_RNDN);
    double_add(y, y, c1, MPFR_RNDN);
    // clear variables
    double_clear(temp);
    double_clear(c1);
    double_clear(c2);
  }

}


void _double_chebpoly_evaluate_fr(mpfr_t y, double_srcptr P, long n, const mpfr_t x)
{
  if (n < 0)
    mpfr_set_si(y, 0, MPFR_RNDN);

  else if (n == 0)
    mpfr_set_d(y, P[0], MPFR_RNDN);

  else {

    mpfr_t c1, c2, temp;
    mpfr_init(temp);
    mpfr_init(c1);
    mpfr_set_d(c1, P[n - 1], MPFR_RNDN);
    mpfr_init(c2);
    mpfr_set_d(c2, P[n], MPFR_RNDN);
    long i;

    for (i = n-2 ; i >= 0 ; i--) {
      // (c1,c2) -> (P[i]-c2, c1+2*x*c2)
      // new value of c1
      mpfr_swap(temp, c1);
      mpfr_d_sub(c1, P[i], c2, MPFR_RNDN);
      // new value of c2
      mpfr_mul(c2, c2, x, MPFR_RNDN);
      mpfr_mul_2si(c2, c2, 1, MPFR_RNDN);
      mpfr_add(c2, c2, temp, MPFR_RNDN);
    }

    // y = P(x) = c1+x*c2
    mpfr_mul(y, x, c2, MPFR_RNDN);
    mpfr_add(y, y, c1, MPFR_RNDN);
    // clear variables
    mpfr_clear(temp);
    mpfr_clear(c1);
    mpfr_clear(c2);
  }

}


void _double_chebpoly_evaluate_fi(mpfi_t y, double_srcptr P, long n, const mpfi_t x)
{
  if (n < 0)
    mpfi_set_si(y, 0);

  else if (n == 0)
    mpfi_set_d(y, P[0]);

  else {

    mpfi_t c1, c2, temp;
    mpfi_init(temp);
    mpfi_init(c1);
    mpfi_set_d(c1, P[n - 1]);
    mpfi_init(c2);
    mpfi_set_d(c2, P[n]);
    long i;

    for (i = n-2 ; i >= 0 ; i--) {
      // (c1,c2) -> (P[i]-c2, c1+2*x*c2)
      // new value of c1
      mpfi_swap(temp, c1);
      mpfi_d_sub(c1, P[i], c2);
      // new value of c2
      mpfi_mul(c2, c2, x);
      mpfi_mul_2si(c2, c2, 1);
      mpfi_add(c2, c2, temp);
    }

    // y = P(x) = c1+x*c2
    mpfi_mul(y, x, c2);
    mpfi_add(y, y, c1);
    // clear variables
    mpfi_clear(temp);
    mpfi_clear(c1);
    mpfi_clear(c2);
  }

}


// set y to P(x)
void double_chebpoly_evaluate_si(double_t y, const double_chebpoly_t P, long x)
{
  _double_chebpoly_evaluate_si(y, P->coeffs, P->degree, x);
}


// set y to P(x)
void double_chebpoly_evaluate_z(double_t y, const double_chebpoly_t P, const mpz_t x)
{
  _double_chebpoly_evaluate_z(y, P->coeffs, P->degree, x);
}


// set y to P(x)
void double_chebpoly_evaluate_q(double_t y, const double_chebpoly_t P, const mpq_t x)
{
  _double_chebpoly_evaluate_q(y, P->coeffs, P->degree, x);
}


// set y to P(x)
void double_chebpoly_evaluate_d(double_t y, const double_chebpoly_t P, const double_t x)
{
  _double_chebpoly_evaluate_d(y, P->coeffs, P->degree, x);
}


// set y to P(x)
void double_chebpoly_evaluate_fr(mpfr_t y, const double_chebpoly_t P, const mpfr_t x)
{
  _double_chebpoly_evaluate_fr(y, P->coeffs, P->degree, x);
}


// set y to P(x)
void double_chebpoly_evaluate_fi(mpfi_t y, const double_chebpoly_t P, const mpfi_t x)
{
  _double_chebpoly_evaluate_fi(y, P->coeffs, P->degree, x);
}

// set y to P(-1)
void double_chebpoly_evaluate_1(double_t y, const double_chebpoly_t P)
{
  long n = P->degree;
  long i;
  double_set_si(y, 0, MPFR_RNDN);
  for (i = 0 ; i <= n ; i++)
    if (i % 2)
      double_sub(y, y, P->coeffs + i, MPFR_RNDN);
    else
      double_add(y, y, P->coeffs + i, MPFR_RNDN);
}

// set y to P(0)
void double_chebpoly_evaluate0(double_t y, const double_chebpoly_t P)
{
  long n = P->degree;
  long i;
  double_set_si(y, 0, MPFR_RNDN);
  for (i = 0 ; i <= n ; i+=2)
    if (i % 4)
      double_sub(y, y, P->coeffs + i, MPFR_RNDN);
    else
      double_add(y, y, P->coeffs + i, MPFR_RNDN);
}

// set y to P(1)
void double_chebpoly_evaluate1(double_t y, const double_chebpoly_t P)
{
  long n = P->degree;
  long i;
  double_set_si(y, 0, MPFR_RNDN);
  for (i = 0 ; i <= n ; i++)
    double_add(y, y, P->coeffs + i, MPFR_RNDN);
}
