#include "double_chebpoly.h"


// clear P
void double_chebpoly_clear(double_chebpoly_t P)
{
  long n = P->degree;
  long i;
  for (i = 0 ; i <= n ; i++)
    double_clear(P->coeffs + i);
  free(P->coeffs);
}
