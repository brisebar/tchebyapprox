
#include "double_chebpoly.h"


// get the degree of P
long double_chebpoly_degree(const double_chebpoly_t P)
{
  return P->degree;
}
