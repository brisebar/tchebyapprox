
#include "double_chebpoly.h"


// set P to Q*X^k
void double_chebpoly_shift_left(double_chebpoly_t P, const double_chebpoly_t Q, unsigned long k)
{
  long i;
  long n = Q->degree;
  double_ptr Pcoeffs = malloc((n+k+1) * sizeof(double_t));
  for (i = 0 ; i <= n+k ; i++)
    double_init(Pcoeffs + i);
  _double_chebpoly_shift_left(Pcoeffs, Q->coeffs, n, k);
  double_chebpoly_clear(P);
  P->degree = n+k;
  P->coeffs = Pcoeffs;
}


// set P to Q/X^k
void double_chebpoly_shift_right(double_chebpoly_t P, const double_chebpoly_t Q, unsigned long k)
{
  long i;
  long n = Q->degree;
  if (k > n)
    double_chebpoly_zero(P);
  else {
    double_ptr Pcoeffs = malloc((n-k+1) * sizeof(double_t));
    for (i = 0 ; i <= n-k ; i++)
      double_init(Pcoeffs + i);
    _double_chebpoly_shift_right(Pcoeffs, Q->coeffs, n, k);
    double_chebpoly_clear(P);
    P->degree = n-k;
    P->coeffs = Pcoeffs;
  }
}
