
#include "double_chebpoly.h"


int double_chebpoly_equal(const double_chebpoly_t P, const double_chebpoly_t Q)
{
  return (P->degree == Q->degree && _double_chebpoly_equal(P->coeffs, Q->coeffs, P->degree));
}
