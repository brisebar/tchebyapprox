
#include "double_chebpoly.h"


// set P := Q^e
void double_chebpoly_pow(double_chebpoly_t P, const double_chebpoly_t Q, unsigned long e)
{
  double_chebpoly_t Qe;
  double_chebpoly_init(Qe);
  double_chebpoly_set_coeff_si(Qe, 0, 1);
  double_chebpoly_t Qi;
  double_chebpoly_init(Qi);
  double_chebpoly_set_coeff_si(Qi, 0, 1);

  while (e != 0) {
    double_chebpoly_mul(Qi, Qi, Q);
    if (e%2 == 1)
      double_chebpoly_mul(Qe, Qe, Qi);
    e /= 2;
  }

  double_chebpoly_clear(Qi);
  double_chebpoly_clear(P);
  P->degree = Qe->degree;
  P->coeffs = Qe->coeffs;
}
