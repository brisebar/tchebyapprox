
#include "double_chebpoly.h"


void _double_chebpoly_print(double_srcptr P, long n, const char * var)
{
  long i;
  int flag = 0;

  for (i = n ; i >=0 ; i--)
    if (!double_zero_p(P + i)) {
      if (flag)
	  printf("+");
      else
	flag = 1;
      printf("%.10e*%s_%ld", P[i], var, i);	
    }

}


// display the polynomial P with 'var' as indeterminate symbol
// 'var' is a 0-terminated string
void double_chebpoly_print(const double_chebpoly_t P, const char * var)
{
  long n = P->degree;

  if (n < 0)
    printf("0");

  else
    _double_chebpoly_print(P->coeffs, n, var);

}
