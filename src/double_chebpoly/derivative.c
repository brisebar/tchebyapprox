
#include "double_chebpoly.h"


void _double_chebpoly_derivative(double_ptr P, double_srcptr Q, long n)
{
  long i;

  double_ptr R = malloc(n * sizeof(double_t));
  for (i = 0 ; i < n ; i++) {
    double_init(R + i);
    double_set_si(R + i, 0, MPFR_RNDN);
  }
  
  for (i = n-1 ; i >= 0 ; i--) {
    double_mul_si(R + i, Q + i + 1, 2*(i+1), MPFR_RNDN);
    if (i < n-2)
      double_add(R + i, R + i, R + i + 2, MPFR_RNDN);
  }
  
  double_mul_2si(R, R, -1, MPFR_RNDN);

  for (i = 0 ; i < n ; i++) {
    double_swap(P + i, R + i);
    double_clear(R + i);
  }
  
  free(R);
}


// set P := Q'
void double_chebpoly_derivative(double_chebpoly_t P, const double_chebpoly_t Q)
{
  long n = Q->degree;

  if (n <= 0)
    double_chebpoly_zero(P);

  else {
    double_chebpoly_set_degree(P, n);
    _double_chebpoly_derivative(P->coeffs, Q->coeffs, n);
    double_clear(P->coeffs + n);
    P->degree --;
  }
}
