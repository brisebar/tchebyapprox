
#include "double_chebpoly.h"


// init with P = 0
void double_chebpoly_init(double_chebpoly_t P)
{
  P->degree = -1;
  P->coeffs = malloc(0);
}
