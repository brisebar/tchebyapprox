
#include "mpfi_chebpoly_lrintop.h"


void mpfi_chebpoly_lrintop_set_length(mpfi_chebpoly_lrintop_t K, long length)
{
  long r = K->length;
  K->length = length;
  long i;

  if (length < r) {
    for (i = length ; i < r ; i++) {
      mpfi_chebpoly_clear(K->lpoly + i);
      mpfi_chebpoly_clear(K->rpoly + i);
    }
    K->lpoly = realloc(K->lpoly, length * sizeof(mpfi_chebpoly_t));
    K->rpoly = realloc(K->rpoly, length * sizeof(mpfi_chebpoly_t));
  }

  else if (length > r) {
    K->lpoly = realloc(K->lpoly, length * sizeof(mpfi_chebpoly_t));
    K->rpoly = realloc(K->rpoly, length * sizeof(mpfi_chebpoly_t));
    for (i = r ; i < length ; i++) {
      mpfi_chebpoly_init(K->lpoly + i);
      mpfi_chebpoly_init(K->rpoly + i);
    }
  }

}
