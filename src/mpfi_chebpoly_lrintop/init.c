
#include "mpfi_chebpoly_lrintop.h"


void mpfi_chebpoly_lrintop_init(mpfi_chebpoly_lrintop_t K)
{
  K->length = 0;
  K->lpoly = malloc(0);
  K->rpoly = malloc(0);
}
