
#include "mpfi_chebpoly_lrintop.h"


void mpfi_chebpoly_lrintop_swap(mpfi_chebpoly_lrintop_t K, mpfi_chebpoly_lrintop_t L)
{
  long length_buf = K->length;
  mpfi_chebpoly_ptr lpoly_buf = K->lpoly;
  mpfi_chebpoly_ptr rpoly_buf = K->rpoly;

  K->length = L->length;
  K->lpoly = L->lpoly;
  K->rpoly = L->rpoly;

  L->length = length_buf;
  L->lpoly = lpoly_buf;
  L->rpoly = rpoly_buf;
}
