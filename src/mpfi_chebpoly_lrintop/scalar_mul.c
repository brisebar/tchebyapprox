
#include "mpfi_chebpoly_lrintop.h"

void _mpfi_chebpoly_lrintop_scalar_mul_si(mpfi_chebpoly_ptr Llpoly, mpfi_chebpoly_ptr Lrpoly, mpfi_chebpoly_srcptr Klpoly, mpfi_chebpoly_srcptr Krpoly, long r, long c)
{
  long i;
  for (i = 0 ; i < r ; i++) {
    mpfi_chebpoly_scalar_mul_si(Llpoly + i, Klpoly + i, c);
    mpfi_chebpoly_set(Lrpoly + i, Krpoly + i);
  }
}


// set L := c * K
void mpfi_chebpoly_lrintop_scalar_mul_si(mpfi_chebpoly_lrintop_t L, const mpfi_chebpoly_lrintop_t K, long c)
{
  mpfi_chebpoly_lrintop_set_length(L, K->length);
  _mpfi_chebpoly_lrintop_scalar_mul_si(L->lpoly, L->rpoly, K->lpoly, K->rpoly, K->length, c);
}


void _mpfi_chebpoly_lrintop_scalar_mul_z(mpfi_chebpoly_ptr Llpoly, mpfi_chebpoly_ptr Lrpoly, mpfi_chebpoly_srcptr Klpoly, mpfi_chebpoly_srcptr Krpoly, long r, const mpz_t c)
{
  long i;
  for (i = 0 ; i < r ; i++) {
    mpfi_chebpoly_scalar_mul_z(Llpoly + i, Klpoly + i, c);
    mpfi_chebpoly_set(Lrpoly + i, Krpoly + i);
  }
}


// set L := c * K
void mpfi_chebpoly_lrintop_scalar_mul_z(mpfi_chebpoly_lrintop_t L, const mpfi_chebpoly_lrintop_t K, const mpz_t c)
{
  mpfi_chebpoly_lrintop_set_length(L, K->length);
  _mpfi_chebpoly_lrintop_scalar_mul_z(L->lpoly, L->rpoly, K->lpoly, K->rpoly, K->length, c);
}


void _mpfi_chebpoly_lrintop_scalar_mul_q(mpfi_chebpoly_ptr Llpoly, mpfi_chebpoly_ptr Lrpoly, mpfi_chebpoly_srcptr Klpoly, mpfi_chebpoly_srcptr Krpoly, long r, const mpq_t c)
{
  long i;
  for (i = 0 ; i < r ; i++) {
    mpfi_chebpoly_scalar_mul_q(Llpoly + i, Klpoly + i, c);
    mpfi_chebpoly_set(Lrpoly + i, Krpoly + i);
  }
}


// set L := c * K
void mpfi_chebpoly_lrintop_scalar_mul_q(mpfi_chebpoly_lrintop_t L, const mpfi_chebpoly_lrintop_t K, const mpq_t c)
{
  mpfi_chebpoly_lrintop_set_length(L, K->length);
  _mpfi_chebpoly_lrintop_scalar_mul_q(L->lpoly, L->rpoly, K->lpoly, K->rpoly, K->length, c);
}


void _mpfi_chebpoly_lrintop_scalar_mul_d(mpfi_chebpoly_ptr Llpoly, mpfi_chebpoly_ptr Lrpoly, mpfi_chebpoly_srcptr Klpoly, mpfi_chebpoly_srcptr Krpoly, long r, const double_t c)
{
  long i;
  for (i = 0 ; i < r ; i++) {
    mpfi_chebpoly_scalar_mul_d(Llpoly + i, Klpoly + i, c);
    mpfi_chebpoly_set(Lrpoly + i, Krpoly + i);
  }
}


// set L := c * K
void mpfi_chebpoly_lrintop_scalar_mul_d(mpfi_chebpoly_lrintop_t L, const mpfi_chebpoly_lrintop_t K, const double_t c)
{
  mpfi_chebpoly_lrintop_set_length(L, K->length);
  _mpfi_chebpoly_lrintop_scalar_mul_d(L->lpoly, L->rpoly, K->lpoly, K->rpoly, K->length, c);
}


void _mpfi_chebpoly_lrintop_scalar_mul_fr(mpfi_chebpoly_ptr Llpoly, mpfi_chebpoly_ptr Lrpoly, mpfi_chebpoly_srcptr Klpoly, mpfi_chebpoly_srcptr Krpoly, long r, const mpfr_t c)
{
  long i;
  for (i = 0 ; i < r ; i++) {
    mpfi_chebpoly_scalar_mul_fr(Llpoly + i, Klpoly + i, c);
    mpfi_chebpoly_set(Lrpoly + i, Krpoly + i);
  }
}


// set L := c * K
void mpfi_chebpoly_lrintop_scalar_mul_fr(mpfi_chebpoly_lrintop_t L, const mpfi_chebpoly_lrintop_t K, const mpfr_t c)
{
  mpfi_chebpoly_lrintop_set_length(L, K->length);
  _mpfi_chebpoly_lrintop_scalar_mul_fr(L->lpoly, L->rpoly, K->lpoly, K->rpoly, K->length, c);
}


void _mpfi_chebpoly_lrintop_scalar_mul_2si(mpfi_chebpoly_ptr Llpoly, mpfi_chebpoly_ptr Lrpoly, mpfi_chebpoly_srcptr Klpoly, mpfi_chebpoly_srcptr Krpoly, long r, long k)
{
  long i;
  for (i = 0 ; i < r ; i++) {
    mpfi_chebpoly_scalar_mul_2si(Llpoly + i, Klpoly + i, k);
    mpfi_chebpoly_set(Lrpoly + i, Krpoly + i);
  }
}


// set L := 2^k * K
void mpfi_chebpoly_lrintop_scalar_mul_2si(mpfi_chebpoly_lrintop_t L, const mpfi_chebpoly_lrintop_t K, long k)
{
  mpfi_chebpoly_lrintop_set_length(L, K->length);
  _mpfi_chebpoly_lrintop_scalar_mul_2si(L->lpoly, L->rpoly, K->lpoly, K->rpoly, K->length, k);
}
