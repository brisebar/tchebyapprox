
#include "mpfi_chebpoly_lrintop.h"


// Mlpoly, Mrpoly must be freshed allocated arrays of mpfi_chebpoly of size r1+r2
void _mpfi_chebpoly_lrintop_add(mpfi_chebpoly_ptr Mlpoly, mpfi_chebpoly_ptr Mrpoly, mpfi_chebpoly_srcptr Klpoly, mpfi_chebpoly_srcptr Krpoly, long r1, mpfi_chebpoly_srcptr Llpoly, mpfi_chebpoly_srcptr Lrpoly, long r2)
{
  long i;

  for (i = 0 ; i < r1 ; i++) {
    mpfi_chebpoly_set(Mlpoly + i, Klpoly + i);
    mpfi_chebpoly_set(Mrpoly + i, Krpoly + i);
  }

  for (i = 0 ; i < r2 ; i++) {
    mpfi_chebpoly_set(Mlpoly + r1 + i, Llpoly + i);
    mpfi_chebpoly_set(Mrpoly + r1 + i, Lrpoly + i);
  }

}


void mpfi_chebpoly_lrintop_add(mpfi_chebpoly_lrintop_t M, const mpfi_chebpoly_lrintop_t K, const mpfi_chebpoly_lrintop_t L)
{
  mpfi_chebpoly_lrintop_t K_L;
  mpfi_chebpoly_lrintop_init(K_L);

  mpfi_chebpoly_lrintop_set_length(K_L, K->length + L->length);
  _mpfi_chebpoly_lrintop_add(K_L->lpoly, K_L->rpoly, K->lpoly, K->rpoly, K->length, L->lpoly, L->rpoly, L->length);

  mpfi_chebpoly_lrintop_swap(M, K_L);
  mpfi_chebpoly_lrintop_clear(K_L);
}
