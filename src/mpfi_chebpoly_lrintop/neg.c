
#include "mpfi_chebpoly_lrintop.h"


void _mpfi_chebpoly_lrintop_neg(mpfi_chebpoly_ptr Llpoly, mpfi_chebpoly_ptr Lrpoly, mpfi_chebpoly_srcptr Klpoly, mpfi_chebpoly_srcptr Krpoly, long r)
{
  long i;
  for (i = 0 ; i < r ; i++) {
    mpfi_chebpoly_neg(Llpoly + i, Klpoly + i);
    mpfi_chebpoly_set(Lrpoly + i, Krpoly + i);
  }
}

void mpfi_chebpoly_lrintop_neg(mpfi_chebpoly_lrintop_t L, const mpfi_chebpoly_lrintop_t K)
{
  mpfi_chebpoly_lrintop_set_length(L, K->length);
  _mpfi_chebpoly_lrintop_neg(L->lpoly, L->rpoly, K->lpoly, K->rpoly, K->length);
}
