
#include "mpfi_chebpoly_lrintop.h"

/* convert LODE to lrintop */

// to integral equation on last derivative
// from x-D form: y^(r-1) + a[r-1] * y^(r-1) + ... + a[1] * y' + a[0] * y = h
// with initial conditions y^(i)(-1) = v[i] (0 <= i < r)
// to: f + ∫_-1^t K(t,s) * f(s) ds = g

// compute polynomial kernel K(t,s)
void mpfi_chebpoly_lrintop_set_lode_xD(mpfi_chebpoly_lrintop_t K, const mpfi_chebpoly_srcptr a, long r)
{
  long i, j, k;
  mpfi_chebpoly_t Temp, gamma;
  mpfi_chebpoly_init(Temp);
  mpfi_chebpoly_init(gamma);

  mpfi_chebpoly_lrintop_set_length(K, r);
  // rpoly[i](s) = T_i(s)
  for (i = 0 ; i < r ; i++) {
    mpfi_chebpoly_zero(K->lpoly + i);
    mpfi_chebpoly_zero(K->rpoly + i);
    mpfi_chebpoly_set_coeff_si(K->rpoly + i, i, 1);
  }

  // compute lpoly by adding (t-s)^i / i! * a[r-1-i](t)

  // (t-s)^i / i! = Sum_{j=0}^i beta[j](t) * T_j(s)  for i=0..r-1
  mpfi_chebpoly_ptr beta = malloc(r * sizeof(mpfi_chebpoly_t));
  for (j = 0 ; j < r ; j++)
    mpfi_chebpoly_init(beta + j);

  // start with i=0
  if (r > 0) {
    mpfi_chebpoly_set_coeff_si(beta + 0, 0, 1);
    mpfi_chebpoly_add(K->lpoly + 0, K->lpoly + 0, a + r-1);
  }

  for (i = 1 ; i < r ; i++) {

    // update beta[j] s.t. (t-s)^{i-1} / (i-1)! => (t-s)^i / i!
    // by integrating w.r.t t from s to t
    mpfi_chebpoly_zero(gamma);
    for (j = 0 ; j < i ; j++) {
      // primitive along t
      mpfi_chebpoly_antiderivative(beta + j, beta + j);
      // store component at t=s to be subtracted
      mpfi_chebpoly_zero(Temp);
      mpfi_chebpoly_set_coeff_si(Temp, j, 1);
      mpfi_chebpoly_mul(Temp, beta + j, Temp);
      mpfi_chebpoly_add(gamma, gamma, Temp);
    }

    // subtract gamma by distributing over the beta[j]
    // assignes b[i] for the first time
    mpfi_chebpoly_set_degree(Temp, 0);
    for (j = 0 ; j <= i ; j++) {
      mpfi_chebpoly_get_coeff(Temp->coeffs + 0, gamma, j);
      mpfi_chebpoly_sub(beta + j, beta + j, Temp);
    }

    // multiply by a[r-1-i] and add to lpoly
    for (j = 0 ; j <= i ; j++) {
      mpfi_chebpoly_mul(Temp, beta + j, a + r-1-i);
      mpfi_chebpoly_add(K->lpoly + j, K->lpoly + j, Temp);
    }

  }

  // clear variables
  for (i = 0 ; i < r ; i++)
    mpfi_chebpoly_clear(beta + i);
  free(beta);
  mpfi_chebpoly_clear(Temp);
  mpfi_chebpoly_clear(gamma);
}


// compute initial condition contribution to right hand side
void mpfi_chebpoly_lrintop_set_lode_xD_initvals(mpfi_chebpoly_t g, mpfi_chebpoly_srcptr a, long r, mpfi_srcptr v)
{
  long i;
  mpfi_chebpoly_zero(g);

  // P = v[r-1] * (t-1)^(r-1-i) / (r-1-i)! + ... + v[i+1] * (t-1) + v[i]
  // Q = P * a[i]
  mpfi_chebpoly_t P, Q;
  mpfi_chebpoly_init(P);
  mpfi_chebpoly_init(Q);
  for (i = r-1 ; i >= 0 ; i--) {
    mpfi_chebpoly_antiderivative_1(P, P);
    if (P->degree < 0)
      mpfi_chebpoly_set_coeff_fi(P, 0, v + i);
    else
      mpfi_add(P->coeffs + 0, P->coeffs + 0, v + i);
    mpfi_chebpoly_mul(Q, P, a + i);
    mpfi_chebpoly_sub(g, g, Q);
  }

  mpfi_chebpoly_clear(P);
  mpfi_chebpoly_clear(Q);
}


// compute right hand side g
void mpfi_chebpoly_lrintop_set_lode_xD_rhs(mpfi_chebpoly_t g, mpfi_chebpoly_srcptr a, long r, const mpfi_chebpoly_t h, mpfi_srcptr v)
{
  mpfi_chebpoly_t P;
  mpfi_chebpoly_init(P);
  mpfi_chebpoly_lrintop_set_lode_xD_initvals(P, a, r, v);
  mpfi_chebpoly_add(g, h, P);
  mpfi_chebpoly_clear(P);
}


// to integral equation on the same function
// from D-x form: (-D)^(r)y + (-D)^(r-1)(b[r-1] * y) + ... + (-D)(b[1] * y) + b[0] * y = h
// with initial conditions y^[i](-1) = w[i] (0 <= i < r)
// where y^[i] = (-D)^(i)y + (-D)^(i-1)(b[r-1] * y) + ... + b[r-i] * y
// to: y + ∫_-1^t K(t,s) * y(s) ds = g

// compute polynomial kernel K(t,s)
void mpfi_chebpoly_lrintop_set_lode_Dx(mpfi_chebpoly_lrintop_t K, const mpfi_chebpoly_srcptr b, long r)
{
  long i;
  mpfi_chebpoly_lrintop_set_lode_xD(K, b, r);
  for (i = 0 ; i < r ; i++) {
    mpfi_chebpoly_swap(K->lpoly + i, K->rpoly + i);
    mpfi_chebpoly_neg(K->lpoly + i, K->lpoly + i);
  }
}

// compute initial condition contribution to right hand side
void mpfi_chebpoly_lrintop_set_lode_Dx_initvals(mpfi_chebpoly_t g, long r, mpfi_srcptr w)
{
  long i;
  mpfi_chebpoly_zero(g);
  mpfi_chebpoly_t P, Q;
  mpfi_chebpoly_init(P);
  mpfi_chebpoly_init(Q);

  // P = (1-x)^i / i!
  // Q = w[i] * P
  mpfi_chebpoly_set_degree(P, 0);
  mpfi_chebpoly_set_coeff_si(P, 0, 1);
  for (i = 0 ; i < r ; i++) {
    mpfi_chebpoly_scalar_mul_fi(Q, P, w + i);
    mpfi_chebpoly_add(g, g, Q);
    mpfi_chebpoly_antiderivative_1(P, P);
    mpfi_chebpoly_neg(P, P);
  }

  mpfi_chebpoly_clear(P);
  mpfi_chebpoly_clear(Q);
}


// compute right hand side g
void mpfi_chebpoly_lrintop_set_lode_Dx_rhs(mpfi_chebpoly_t g, long r, const mpfi_chebpoly_t h, mpfi_srcptr w)
{
  // integrate h r times and apply sign
  long i;
  mpfi_chebpoly_set(g, h);
  for (i = 0 ; i < r ; i++)
    mpfi_chebpoly_antiderivative_1(g, g);
  if (r % 2)
    mpfi_chebpoly_neg(g, g);

  // add initial condition term
  mpfi_chebpoly_t P;
  mpfi_chebpoly_init(P);
  mpfi_chebpoly_lrintop_set_lode_Dx_initvals(P, r, w);
  mpfi_chebpoly_add(g, g, P);
  mpfi_chebpoly_clear(P);
}
