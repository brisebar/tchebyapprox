
#include "mpfi_chebpoly_lrintop.h"


// Mlpoly, Mrpoly must be freshed allocated arrays of mpfi_chebpoly of size r1+r2
void _mpfi_chebpoly_lrintop_comp(mpfi_chebpoly_ptr Mlpoly, mpfi_chebpoly_ptr Mrpoly, mpfi_chebpoly_srcptr Klpoly, mpfi_chebpoly_srcptr Krpoly, long r1, mpfi_chebpoly_srcptr Llpoly, mpfi_chebpoly_srcptr Lrpoly, long r2)
{
  mpfi_chebpoly_t P, Q;
  mpfi_chebpoly_init(P);
  mpfi_chebpoly_init(Q);
  long i, j;

  // initialization
  for (i = 0 ; i < r1 ; i++) {
    mpfi_chebpoly_set(Mlpoly + i, Klpoly + i);
    mpfi_chebpoly_zero(Mrpoly + i);
  }

  for (j = 0 ; j < r2 ; j++) {
    mpfi_chebpoly_zero(Mlpoly + r1 + j);
    mpfi_chebpoly_set(Mrpoly + r1 + j, Lrpoly + j);
  }

  // composition loop
  for (i = 0 ; i < r1 ; i++)
    for (j = 0 ; j < r2 ; j++) {

      // primitive of middle product
      mpfi_chebpoly_mul(P, Krpoly + i, Llpoly + j);
      mpfi_chebpoly_antiderivative(P, P);

      // distribute to the left
      mpfi_chebpoly_mul(Q, Klpoly + i, P);
      mpfi_chebpoly_add(Mlpoly + r1 + j, Mlpoly + r1 + j, Q);

      // distribute to the right
      mpfi_chebpoly_mul(Q, P, Lrpoly + j);
      mpfi_chebpoly_sub(Mrpoly + i, Mrpoly + i, Q);

    }

  mpfi_chebpoly_clear(P);
  mpfi_chebpoly_clear(Q);

}


// set M := K ∘ L
void mpfi_chebpoly_lrintop_comp(mpfi_chebpoly_lrintop_t M, const mpfi_chebpoly_lrintop_t K, const mpfi_chebpoly_lrintop_t L)
{
  mpfi_chebpoly_lrintop_t K_L;
  mpfi_chebpoly_lrintop_init(K_L);

  mpfi_chebpoly_lrintop_set_length(K_L, K->length + L->length);
  _mpfi_chebpoly_lrintop_comp(K_L->lpoly, K_L->rpoly, K->lpoly, K->rpoly, K->length, L->lpoly, L->rpoly, L->length);

  mpfi_chebpoly_lrintop_swap(M, K_L);
  mpfi_chebpoly_lrintop_clear(K_L);
}
