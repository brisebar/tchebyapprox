
#include "mpfi_chebpoly_lrintop.h"


// get bandmatrix
void mpfi_chebpoly_lrintop_get_bandmatrix(mpfi_bandmatrix_t M, const mpfi_chebpoly_lrintop_t K, long N)
{
  mpfi_chebpoly_intop_t KK;
  mpfi_chebpoly_intop_init(KK);
  mpfi_chebpoly_lrintop_get_intop(KK, K);
  mpfi_chebpoly_intop_get_bandmatrix(M, KK, N);
  mpfi_chebpoly_intop_clear(KK);
}
