
#include "mpfi_chebpoly_lrintop.h"


void mpfi_chebpoly_lrintop_clear(mpfi_chebpoly_lrintop_t K)
{
  long r = K->length;
  long i;
  for (i = 0 ; i < r ; i++) {
    mpfi_chebpoly_clear(K->lpoly + i);
    mpfi_chebpoly_clear(K->rpoly + i);
  }
  free(K->lpoly);
  free(K->rpoly);
}
