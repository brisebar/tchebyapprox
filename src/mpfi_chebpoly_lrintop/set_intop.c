
#include "mpfi_chebpoly_lrintop.h"


/* convert from mpfi_chebpoly_intop_t */
void mpfi_chebpoly_lrintop_set_intop(mpfi_chebpoly_lrintop_t M, const mpfi_chebpoly_intop_t K)
{
  long r = K->order;
  mpfi_chebpoly_lrintop_set_length(M, r);
  long i;
  for (i = 0 ; i < r ; i++) {
    mpfi_chebpoly_set(M->lpoly + i, K->alpha + i);
    mpfi_chebpoly_zero(M->rpoly + i);
    mpfi_chebpoly_set_coeff_si(M->rpoly + i, i, 1);
  }
}


/* convert to mpfi_chebpoly_intop_t */
void mpfi_chebpoly_lrintop_get_intop(mpfi_chebpoly_intop_t M, const mpfi_chebpoly_lrintop_t K)
{
  long r = K->length;
  long i, j;

  // order of M = max degree rpoly in s + 1
  long s = 0;
  for (i = 0 ; i < r ; i++)
    s = s < K->rpoly[i].degree ? K->rpoly[i].degree : s;
  s++;
  mpfi_chebpoly_intop_set_order(M, s);
  for (i = 0 ; i < s ; i++)
    mpfi_chebpoly_zero(M->alpha + i);

  mpfi_chebpoly_t Temp;
  mpfi_chebpoly_init(Temp);
  for (i = 0 ; i < r ; i++)
    for (j = 0 ; j <= K->rpoly[i].degree ; j++) {
      mpfi_chebpoly_scalar_mul_fi(Temp, K->lpoly + i, K->rpoly[i].coeffs + j);
      mpfi_chebpoly_add(M->alpha + j, M->alpha + j, Temp);
    }

  mpfi_chebpoly_clear(Temp);
}
