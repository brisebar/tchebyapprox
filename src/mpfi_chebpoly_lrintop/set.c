
#include "mpfi_chebpoly_lrintop.h"


void mpfi_chebpoly_lrintop_set(mpfi_chebpoly_lrintop_t L, const mpfi_chebpoly_lrintop_t K)
{
  long r = K->length;
  mpfi_chebpoly_lrintop_set_length(L, r);

  long i;
  for (i = 0 ; i < r ; i++) {
    mpfi_chebpoly_set(L->lpoly + i, K->lpoly + i);
    mpfi_chebpoly_set(L->rpoly + i, K->rpoly + i);
  }
}


void mpfi_chebpoly_lrintop_set_double_chebpoly_lrintop(mpfi_chebpoly_lrintop_t L, const double_chebpoly_lrintop_t K)
{
  long r = K->length;
  mpfi_chebpoly_lrintop_set_length(L, r);

  long i;
  for (i = 0 ; i < r ; i++) {
    mpfi_chebpoly_set_double_chebpoly(L->lpoly + i, K->lpoly + i);
    mpfi_chebpoly_set_double_chebpoly(L->rpoly + i, K->rpoly + i);
  }
}


void mpfi_chebpoly_lrintop_get_double_chebpoly_lrintop(double_chebpoly_lrintop_t L, const mpfi_chebpoly_lrintop_t K)
{
  long r = K->length;
  double_chebpoly_lrintop_set_length(L, r);

  long i;
  for (i = 0 ; i < r ; i++) {
    mpfi_chebpoly_get_double_chebpoly(L->lpoly + i, K->lpoly + i);
    mpfi_chebpoly_get_double_chebpoly(L->rpoly + i, K->rpoly + i);
  }
}


void mpfi_chebpoly_lrintop_set_mpfr_chebpoly_lrintop(mpfi_chebpoly_lrintop_t L, const mpfr_chebpoly_lrintop_t K)
{
  long r = K->length;
  mpfi_chebpoly_lrintop_set_length(L, r);

  long i;
  for (i = 0 ; i < r ; i++) {
    mpfi_chebpoly_set_mpfr_chebpoly(L->lpoly + i, K->lpoly + i);
    mpfi_chebpoly_set_mpfr_chebpoly(L->rpoly + i, K->rpoly + i);
  }
}


void mpfi_chebpoly_lrintop_get_mpfr_chebpoly_lrintop(mpfr_chebpoly_lrintop_t L, const mpfi_chebpoly_lrintop_t K)
{
  long r = K->length;
  mpfr_chebpoly_lrintop_set_length(L, r);

  long i;
  for (i = 0 ; i < r ; i++) {
    mpfi_chebpoly_get_mpfr_chebpoly(L->lpoly + i, K->lpoly + i);
    mpfi_chebpoly_get_mpfr_chebpoly(L->rpoly + i, K->rpoly + i);
  }
}
