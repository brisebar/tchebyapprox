
#include "mpfi_chebpoly_lrintop.h"


void mpfi_chebpoly_lrintop_print(const mpfi_chebpoly_lrintop_t K, const char * Cheb_var, size_t digits)
{
  long r = K->length;
  long i;

  for (i = 0 ; i < r ; i++) {
    printf("lpoly[%ld] = ", i);
    mpfi_chebpoly_print(K->lpoly + i, Cheb_var, digits);
    printf("\nrpoly[%ld] = ", i);
    mpfi_chebpoly_print(K->rpoly + i, Cheb_var, digits);
    printf("\n");
  }
}
