
#include "mpfi_bandvec.h"


void _mpfi_bandvec_scalar_div_si(mpfi_ptr VH, mpfi_ptr VD, long VHwidth, long VDwidth, mpfi_srcptr WH, mpfi_srcptr WD, long WHwidth, long WDwidth, long ind, long c)
{
  long i;
  _mpfi_bandvec_set(VH, VD, VHwidth, VDwidth, WH, WD, WHwidth, WDwidth, ind);

  for (i = 0 ; i < VHwidth ; i++)
    mpfi_div_si(VH + i, VH + i, c);

  for (i = 0 ; i <= 2*VDwidth ; i++)
    mpfi_div_si(VD + i, VD + i, c);
}


void mpfi_bandvec_scalar_div_si(mpfi_bandvec_t V, const mpfi_bandvec_t W, long c)
{
  if (V != W)
    mpfi_bandvec_set_params(V, W->Hwidth, W->Dwidth, W->ind);

  _mpfi_bandvec_scalar_div_si(V->H, V->D, V->Hwidth, V->Dwidth, W->H, W->D, W->Hwidth, W->Dwidth, V->ind, c);
}


void _mpfi_bandvec_scalar_div_z(mpfi_ptr VH, mpfi_ptr VD, long VHwidth, long VDwidth, mpfi_srcptr WH, mpfi_srcptr WD, long WHwidth, long WDwidth, long ind, const mpz_t c)
{
  long i;
  _mpfi_bandvec_set(VH, VD, VHwidth, VDwidth, WH, WD, WHwidth, WDwidth, ind);

  for (i = 0 ; i < VHwidth ; i++)
    mpfi_div_z(VH + i, VH + i, c);

  for (i = 0 ; i <= 2*VDwidth ; i++)
    mpfi_div_z(VD + i, VD + i, c);
}


void mpfi_bandvec_scalar_div_z(mpfi_bandvec_t V, const mpfi_bandvec_t W, const mpz_t c)
{
  if (V != W)
    mpfi_bandvec_set_params(V, W->Hwidth, W->Dwidth, W->ind);

  _mpfi_bandvec_scalar_div_z(V->H, V->D, V->Hwidth, V->Dwidth, W->H, W->D, W->Hwidth, W->Dwidth, V->ind, c);
}


void _mpfi_bandvec_scalar_div_q(mpfi_ptr VH, mpfi_ptr VD, long VHwidth, long VDwidth, mpfi_srcptr WH, mpfi_srcptr WD, long WHwidth, long WDwidth, long ind, const mpq_t c)
{
  long i;
  _mpfi_bandvec_set(VH, VD, VHwidth, VDwidth, WH, WD, WHwidth, WDwidth, ind);

  for (i = 0 ; i < VHwidth ; i++)
    mpfi_div_q(VH + i, VH + i, c);

  for (i = 0 ; i <= 2*VDwidth ; i++)
    mpfi_div_q(VD + i, VD + i, c);
}


void mpfi_bandvec_scalar_div_q(mpfi_bandvec_t V, const mpfi_bandvec_t W, const mpq_t c)
{
  if (V != W)
    mpfi_bandvec_set_params(V, W->Hwidth, W->Dwidth, W->ind);

  _mpfi_bandvec_scalar_div_q(V->H, V->D, V->Hwidth, V->Dwidth, W->H, W->D, W->Hwidth, W->Dwidth, V->ind, c);
}


void _mpfi_bandvec_scalar_div_fr(mpfi_ptr VH, mpfi_ptr VD, long VHwidth, long VDwidth, mpfi_srcptr WH, mpfi_srcptr WD, long WHwidth, long WDwidth, long ind, const mpfr_t c)
{
  long i;
  _mpfi_bandvec_set(VH, VD, VHwidth, VDwidth, WH, WD, WHwidth, WDwidth, ind);

  for (i = 0 ; i < VHwidth ; i++)
    mpfi_div_fr(VH + i, VH + i, c);

  for (i = 0 ; i <= 2*VDwidth ; i++)
    mpfi_div_fr(VD + i, VD + i, c);
}


void mpfi_bandvec_scalar_div_fr(mpfi_bandvec_t V, const mpfi_bandvec_t W, const mpfr_t c)
{
  if (V != W)
    mpfi_bandvec_set_params(V, W->Hwidth, W->Dwidth, W->ind);

  _mpfi_bandvec_scalar_div_fr(V->H, V->D, V->Hwidth, V->Dwidth, W->H, W->D, W->Hwidth, W->Dwidth, V->ind, c);
}


void _mpfi_bandvec_scalar_div_fi(mpfi_ptr VH, mpfi_ptr VD, long VHwidth, long VDwidth, mpfi_srcptr WH, mpfi_srcptr WD, long WHwidth, long WDwidth, long ind, const mpfi_t c)
{
  long i;
  _mpfi_bandvec_set(VH, VD, VHwidth, VDwidth, WH, WD, WHwidth, WDwidth, ind);

  for (i = 0 ; i < VHwidth ; i++)
    mpfi_div(VH + i, VH + i, c);

  for (i = 0 ; i <= 2*VDwidth ; i++)
    mpfi_div(VD + i, VD + i, c);
}


void mpfi_bandvec_scalar_div_fi(mpfi_bandvec_t V, const mpfi_bandvec_t W, const mpfi_t c)
{
  if (V != W)
    mpfi_bandvec_set_params(V, W->Hwidth, W->Dwidth, W->ind);

  _mpfi_bandvec_scalar_div_fi(V->H, V->D, V->Hwidth, V->Dwidth, W->H, W->D, W->Hwidth, W->Dwidth, V->ind, c);
}


void _mpfi_bandvec_scalar_div_2si(mpfi_ptr VH, mpfi_ptr VD, long VHwidth, long VDwidth, mpfi_srcptr WH, mpfi_srcptr WD, long WHwidth, long WDwidth, long ind, long k)
{
   _mpfi_bandvec_set(VH, VD, VHwidth, VDwidth, WH, WD, WHwidth, WDwidth, ind);
long i;

for (i = 0 ; i < VHwidth ; i++)
  mpfi_mul_2si(VH + i, VH + i, -k);

for (i = 0 ; i <= 2*VDwidth ; i++)
  mpfi_mul_2si(VD + i, VD + i, -k);
}


void mpfi_bandvec_scalar_div_2si(mpfi_bandvec_t V, const mpfi_bandvec_t W, long k)
{
if (V != W)
    mpfi_bandvec_set_params(V, W->Hwidth, W->Dwidth, W->ind);

  _mpfi_bandvec_scalar_div_2si(V->H, V->D, V->Hwidth, V->Dwidth, W->H, W->D, W->Hwidth, W->Dwidth, V->ind, k);
}




