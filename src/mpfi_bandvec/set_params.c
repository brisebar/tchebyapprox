
#include "mpfi_bandvec.h"


void mpfi_bandvec_set_params(mpfi_bandvec_t V, long Hwidth, long Dwidth, long ind)
{
  V->ind = ind;

  long i;

  if (Hwidth < V->Hwidth) {
    for (i = Hwidth ; i < V->Hwidth ; i++)
      mpfi_clear(V->H + i);
    V->H = realloc(V->H, Hwidth * sizeof(mpfi_t));
  }

  else if (V->Hwidth < Hwidth) {
    V->H = realloc(V->H, Hwidth * sizeof(mpfi_t));
    for (i = V->Hwidth ; i < Hwidth ; i++) {
      mpfi_init(V->H + i);
      mpfi_set_si(V->H + i, 0);
    }
  }

  V->Hwidth = Hwidth;

  if (Dwidth < V->Dwidth) {
    long delta = V->Dwidth - Dwidth;
    for (i = 0 ; i <= 2*Dwidth ; i++)
      mpfi_swap(V->D + i, V->D + delta + i);
    for (i = 2*Dwidth+1 ; i <= 2*V->Dwidth ; i++)
      mpfi_clear(V->D + i);
    V->D = realloc(V->D, (2*Dwidth + 1) * sizeof(mpfi_t));
  }

  else if (V->Dwidth < Dwidth) {
    long delta = Dwidth - V->Dwidth;
    V->D = realloc(V->D, (2*Dwidth+1) * sizeof(mpfi_t));
    for (i = 2*V->Dwidth+1 ; i <= 2*Dwidth ; i++) {
      mpfi_init(V->D + i);
      mpfi_set_si(V->D + i, 0);
    }
    for (i = 2*V->Dwidth ; i >= 0 ; i--)
      mpfi_swap(V->D + i, V->D + delta + i);
  }

  V->Dwidth = Dwidth;

  mpfi_bandvec_normalise(V);
}

