
#include "mpfi_bandvec.h"


void _mpfi_bandvec_get_mpfr_bandvec(mpfr_ptr VH, mpfr_ptr VD, long VHwidth, long VDwidth, mpfi_srcptr WH, mpfi_srcptr WD, long WHwidth, long WDwidth, long ind)
{
  if (VHwidth < VDwidth) {
    fprintf(stderr, "_mpfi_bandvec_get_mpfr_bandvec: error: Hwidth of result too small\n");
    return;
  }

  else if (VDwidth < WDwidth) {
    fprintf(stderr, "_mpfi_bandvec_get_mpfr_bandvec: error: Dwidth of result too small\n");
    return;
  }

  else {
    long i;
    for (i = 0 ; i < WHwidth ; i++)
      mpfi_mid(VH + i, WH + i);
    for (i = WHwidth ; i < VHwidth ; i++)
      mpfr_set_zero(VH + i, 1);
    for (i = -WDwidth ; i <= WDwidth ; i++)
      mpfi_mid(VD + VDwidth + i, WD + WDwidth + i);
    for (i = WDwidth+1 ; i <= VDwidth ; i++) {
      mpfr_set_zero(VD + VDwidth+i, 1);
      mpfr_set_zero(VD + VDwidth-i, 1);
    }
    _mpfr_bandvec_normalise(VH, VD, VHwidth, VDwidth, ind);
  }
}


void mpfi_bandvec_get_mpfr_bandvec(mpfr_bandvec_t V, const mpfi_bandvec_t W)
{
  mpfr_bandvec_set_params(V, W->Hwidth, W->Dwidth, W->ind);
  long i;
  for (i = 0 ; i < W->Hwidth ; i++)
    mpfi_mid(V->H + i, W->H + i);
  for (i = 0 ; i <= 2*W->Dwidth ; i++)
    mpfi_mid(V->D + i, W->D + i);
}
