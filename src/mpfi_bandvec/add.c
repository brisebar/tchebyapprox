
#include "mpfi_bandvec.h"


void _mpfi_bandvec_add(mpfi_ptr VH, mpfi_ptr VD, long VHwidth, long VDwidth, mpfi_srcptr WH, mpfi_srcptr WD, long WHwidth, long WDwidth, mpfi_srcptr ZH, mpfi_srcptr ZD, long ZHwidth, long ZDwidth, long ind)
{
  if (VHwidth < WHwidth || VHwidth < ZHwidth)
    fprintf(stderr, "_mpfi_bandvec_add: error: Hwidth of result too small\n");

  else if (VDwidth < WDwidth || VDwidth < ZDwidth)
    fprintf(stderr, "_mpfi_bandvec_add: error: Dwidth of result too small\n");

  else {

    long i;

    for (i = 0 ; i < VHwidth ; i++) {
      if (i < WHwidth && i < ZHwidth)
	mpfi_add(VH + i, WH + i, ZH + i);
      else if (i < WHwidth)
	mpfi_set(VH + i, WH + i);
      else if (i < ZHwidth)
	mpfi_set(VH + i, ZH + i);
      else
	mpfi_set_si(VH + i, 0);
    }

    for (i = -VDwidth ; i <= VDwidth ; i++) {
      long abs_i = abs(i);
      if (abs_i <= WDwidth && abs_i <= ZDwidth)
	mpfi_add(VD + VDwidth + i, WD + WDwidth + i, ZD + ZDwidth + i);
      else if (abs_i <= WDwidth)
	mpfi_set(VD + VDwidth + i, WD + WDwidth + i);
      else if (abs_i <= ZDwidth)
	mpfi_set(VD + VDwidth + i, ZD + ZDwidth + i);
      else
	mpfi_set_si(VD + VDwidth + i, 0);
    }

    _mpfi_bandvec_normalise(VH, VD, VHwidth, VDwidth, ind);

  }
}


void mpfi_bandvec_add(mpfi_bandvec_t V, const mpfi_bandvec_t W, const mpfi_bandvec_t Z)
{
  if (W->ind != Z->ind)
    fprintf(stderr, "mpfi_bandvec_add: error: incompatible ind\n");

  else {

    long ind = W->ind;
    long VHwidth = W->Hwidth < Z->Hwidth ? Z->Hwidth : W->Hwidth;
    long VDwidth = W->Dwidth < Z->Dwidth ? Z->Dwidth : W->Dwidth;
    
    mpfi_bandvec_set_params(V, VHwidth, VDwidth, ind);

    _mpfi_bandvec_add(V->H, V->D, VHwidth, VDwidth, W->H, W->D, W->Hwidth, W->Dwidth, Z->H, Z->D, Z->Hwidth, Z->Dwidth, ind);

  }
}

