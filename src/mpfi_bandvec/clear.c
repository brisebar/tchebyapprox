
#include "mpfi_bandvec.h"


void mpfi_bandvec_clear(mpfi_bandvec_t V)
{
  long i;
  for (i = 0 ; i < V->Hwidth ; i++)
    mpfi_clear(V->H + i);
  free(V->H);
  for (i = 0 ; i <= 2*V->Dwidth ; i++)
    mpfi_clear(V->D + i);
  free(V->D);
}
