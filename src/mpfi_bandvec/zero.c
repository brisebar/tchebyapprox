
#include "mpfi_bandvec.h"


void _mpfi_bandvec_zero(mpfi_ptr VH, mpfi_ptr VD, long Hwidth, long Dwidth)
{
  long i;
  for (i = 0 ; i < Hwidth ; i++)
    mpfi_set_si(VH + i, 0);
  for (i = 0 ; i <= 2*Dwidth ; i++)
    mpfi_set_si(VD + i, 0);
}


void mpfi_bandvec_zero(mpfi_bandvec_t V)
{
  _mpfi_bandvec_zero(V->H, V->D, V->Hwidth, V->Dwidth);
}



