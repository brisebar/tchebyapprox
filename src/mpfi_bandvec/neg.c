
#include "mpfi_bandvec.h"



void _mpfi_bandvec_neg(mpfi_ptr VH, mpfi_ptr VD, long VHwidth, long VDwidth, mpfi_srcptr WH, mpfi_srcptr WD, long WHwidth, long WDwidth, long ind)
{
  _mpfi_bandvec_set(VH, VD, VHwidth, VDwidth, WH, WD, WHwidth, WDwidth, ind);
  long i;

  for (i = 0 ; i < VHwidth ; i++)
    mpfi_neg(VH + i, VH + i);

  for (i = 0 ; i <= 2*VDwidth ; i++)
    mpfi_neg(VD + i, VD + i);
}


void mpfi_bandvec_neg(mpfi_bandvec_t V, const mpfi_bandvec_t W)
{
  if (V != W)
    mpfi_bandvec_set_params(V, W->Hwidth, W->Dwidth, W->ind);

  _mpfi_bandvec_neg(V->H, V->D, V->Hwidth, V->Dwidth, W->H, W->D, W->Hwidth, W->Dwidth, V->ind);
}

