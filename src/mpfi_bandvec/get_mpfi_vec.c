
#include "mpfi_bandvec.h"


void _mpfi_bandvec_get_mpfi_vec(mpfi_ptr V, long length, mpfi_srcptr WH, mpfi_srcptr WD, long WHwidth, long WDwidth, long ind)
{
  long j, jmin, jmax;

  _mpfi_vec_zero(V, length);

  jmax = WHwidth < ind - WDwidth ? WHwidth : ind - WDwidth;
  jmax = jmax < length ? jmax : length;
  for (j = 0 ; j < jmax ; j++)
    mpfi_set(V + j, WH + j);

  jmin = ind - WDwidth < 0 ? 0 : ind - WDwidth;
  jmax = ind + WDwidth < length ? ind + WDwidth : length - 1;
  for (j = jmin ; j <= jmax ; j++)
    mpfi_set(V + j, WD + WDwidth - ind + j);

  jmax = WHwidth < length ? WHwidth : length;
  for (j = ind + WDwidth + 1 ; j < jmax ; j++)
    mpfi_set(V + j, WH + j);
}


void mpfi_bandvec_get_mpfi_vec(mpfi_vec_t V, const mpfi_bandvec_t W)
{
  _mpfi_bandvec_get_mpfi_vec(V->coeffs, V->length, W->H, W->D, W->Hwidth, W->Dwidth, W->ind);
}
