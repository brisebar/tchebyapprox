
#include "mpfi_bandvec.h"


void _mpfi_bandvec_normalise(mpfi_ptr VH, mpfi_ptr VD, long VHwidth, long VDwidth, long ind)
{
  long i;

  // handle negative index for VD
  for (i = ind - VDwidth ; i < 0 ; i++) {
    mpfi_add(VD + VDwidth - ind - i, VD + VDwidth - ind - i, VD + VDwidth - ind + i);
    mpfi_set_si(VD + VDwidth - ind + i, 0);
  }

  // add to VD the part of VH that overlaps
  if (ind - VDwidth < VHwidth) {

    long imin = ind - VDwidth < 0 ? 0 : ind - VDwidth;
    long imax = ind + VDwidth < VHwidth ? ind + VDwidth : VHwidth - 1;

    for (i = imin ; i <= imax ; i++) {
      mpfi_add(VD + VDwidth - ind + i, VD + VDwidth - ind + i, VH + i);
      mpfi_set_si(VH + i, 0);
    }

  }
}


void mpfi_bandvec_normalise(mpfi_bandvec_t V)
{
  _mpfi_bandvec_normalise(V->H, V->D, V->Hwidth, V->Dwidth, V->ind);
}
