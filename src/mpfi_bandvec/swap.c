
#include "mpfi_bandvec.h"


void mpfi_bandvec_swap(mpfi_bandvec_t V, mpfi_bandvec_t W)
{
  long long_buf = V->Hwidth;
  V->Hwidth = W->Hwidth;
  W->Hwidth = long_buf;

  long_buf = V->Dwidth;
  V->Dwidth = W->Dwidth;
  W->Dwidth = long_buf;

  long_buf = V->ind;
  V->ind = W->ind;
  W->ind = long_buf;

  mpfi_ptr mpfi_ptr_buf = V->H;
  V->H = W->H;
  W->H = mpfi_ptr_buf;

  mpfi_ptr_buf = V->D;
  V->D = W->D;
  W->D = mpfi_ptr_buf;
}

