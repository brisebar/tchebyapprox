
#include "mpfi_bandvec.h"


void _mpfi_bandvec_print(mpfi_srcptr VH, mpfi_srcptr VD, long VHwidth, long VDwidth, long ind, size_t digits)
{
  long i, imin, imax;
  printf("[");

  imax = ind-VDwidth < VHwidth ? ind-VDwidth : VHwidth;
  for (i = 0 ; i < imax ; i++) {
    mpfi_out_str(stdout, 10, digits, VH + i);
    if (i < imax-1)
      printf(", ");
    else
      printf(" || ");
  }

  imin = ind-VDwidth < 0 ? 0 : ind-VDwidth;
  for (i = imin ; i <= ind+VDwidth ; i++) {
    mpfi_out_str(stdout, 10, digits, VD + VDwidth-ind+i);
    if (i < ind+VDwidth)
      printf(", ");
  }

  for (i = ind+VDwidth+1 ; i < VHwidth ; i++) {
    if (i == ind+VDwidth+1)
      printf(" || ");
    mpfi_out_str(stdout, 10, digits, VH + i);
    if (i < VHwidth-1)
      printf(", ");
  }

  printf("]");
}


void mpfi_bandvec_print(const mpfi_bandvec_t V, size_t digits)
{
  _mpfi_bandvec_print(V->H, V->D, V->Hwidth, V->Dwidth, V->ind, digits);
}


