
#include "mpfi_bandvec.h"


void _mpfi_bandvec_set(mpfi_ptr VH, mpfi_ptr VD, long VHwidth, long VDwidth, mpfi_srcptr WH, mpfi_srcptr WD, long WHwidth, long WDwidth, long ind)
{
  if (VHwidth < VDwidth) {
    fprintf(stderr, "_mpfi_bandvec_set: error: Hwidth of result too small\n");
    return;
  }

  else if (VDwidth < WDwidth) {
    fprintf(stderr, "_mpfi_bandvec_set: error: Dwidth of result too small\n");
    return;
  }

  else {
    long i;
    for (i = 0 ; i < WHwidth ; i++)
      mpfi_set(VH + i, WH + i);
    for (i = WHwidth ; i < VHwidth ; i++)
      mpfi_set_si(VH + i, 0);
    for (i = -WDwidth ; i <= WDwidth ; i++)
      mpfi_set(VD + VDwidth+i, WD + WDwidth+i);
    for (i = WDwidth+1 ; i <= VDwidth ; i++) {
      mpfi_set_si(VD + VDwidth+i, 0);
      mpfi_set_si(VD + VDwidth-i, 0);
    }
    _mpfi_bandvec_normalise(VH, VD, VHwidth, VDwidth, ind);
  }
}


void mpfi_bandvec_set(mpfi_bandvec_t V, const mpfi_bandvec_t W)
{
  if (V != W) {
    mpfi_bandvec_set_params(V, W->Hwidth, W->Dwidth, W->ind);
    long i;
    for (i = 0 ; i < W->Hwidth ; i++)
      mpfi_set(V->H + i, W->H + i);
    for (i = 0 ; i <= 2*W->Dwidth ; i++)
      mpfi_set(V->D + i, W->D + i);
  }
}
