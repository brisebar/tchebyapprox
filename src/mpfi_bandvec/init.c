
#include "mpfi_bandvec.h"


void mpfi_bandvec_init(mpfi_bandvec_t V)
{
  V->Hwidth = 0;
  V->Dwidth = 0;
  V->ind = 0;
  V->H = malloc(0);
  V->D = malloc(sizeof(mpfi_t));
  mpfi_init(V->D + 0);
  mpfi_set_si(V->D + 0, 0);
}
