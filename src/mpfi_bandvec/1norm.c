
#include "mpfi_bandvec.h"


void _mpfi_bandvec_1norm_ubound(mpfr_t norm, mpfi_srcptr VH, mpfi_srcptr VD, long Hwidth, long Dwidth, long ind)
{
  mpfr_set_zero(norm, 1);
  long i, imin, imax;
  mpfr_t temp;
  mpfr_init(temp);
  
  imax = ind-Dwidth < Hwidth ? ind-Dwidth : Hwidth;
  for (i = 0 ; i < imax ; i++) {
    mpfi_mag(temp, VH + i);
    mpfr_add(norm, norm, temp, MPFR_RNDU);
  }

  imin = ind-Dwidth < 0 ? 0 : ind-Dwidth;
  for (i = imin ; i <= ind+Dwidth ; i++) {
    mpfi_mag(temp, VD + Dwidth - ind + i);
    mpfr_add(norm, norm, temp, MPFR_RNDU);
  }

  for (i = ind+Dwidth+1 ; i < Hwidth ; i++) {
    mpfi_mag(temp, VH + i);
    mpfr_add(norm, norm, temp, MPFR_RNDU);
  }

  mpfr_clear(temp);
}


void mpfi_bandvec_1norm_ubound(mpfr_t norm, const mpfi_bandvec_t V)
{
  _mpfi_bandvec_1norm_ubound(norm, V->H, V->D, V->Hwidth, V->Dwidth, V->ind);
}


void _mpfi_bandvec_1norm_lbound(mpfr_t norm, mpfi_srcptr VH, mpfi_srcptr VD, long Hwidth, long Dwidth, long ind)
{
  mpfr_set_zero(norm, 1);
  long i, imin, imax;
  mpfr_t temp;
  mpfr_init(temp);
  
  imax = ind-Dwidth < Hwidth ? ind-Dwidth : Hwidth;
  for (i = 0 ; i < imax ; i++) {
    mpfi_mig(temp, VH + i);
    mpfr_add(norm, norm, temp, MPFR_RNDD);
  }

  imin = ind-Dwidth < 0 ? 0 : ind-Dwidth;
  for (i = imin ; i <= ind+Dwidth ; i++) {
    mpfi_mig(temp, VD + Dwidth - ind + i);
    mpfr_add(norm, norm, temp, MPFR_RNDD);
  }

  for (i = ind+Dwidth+1 ; i < Hwidth ; i++) {
    mpfi_mig(temp, VH + i);
    mpfr_add(norm, norm, temp, MPFR_RNDD);
  }

  mpfr_clear(temp);
}


void mpfi_bandvec_1norm_lbound(mpfr_t norm, const mpfi_bandvec_t V)
{
  _mpfi_bandvec_1norm_lbound(norm, V->H, V->D, V->Hwidth, V->Dwidth, V->ind);
}


void _mpfi_bandvec_1norm_fi(mpfi_t norm, mpfi_srcptr VH, mpfi_srcptr VD, long Hwidth, long Dwidth, long ind)
{
  mpfi_set_si(norm, 0);
  long i, imin, imax;
  mpfi_t temp;
  mpfi_init(temp);
  
  imax = ind-Dwidth < Hwidth ? ind-Dwidth : Hwidth;
  for (i = 0 ; i < imax ; i++) {
    mpfi_abs(temp, VH + i);
    mpfi_add(norm, norm, temp);
  }

  imin = ind-Dwidth < 0 ? 0 : ind-Dwidth;
  for (i = imin ; i <= ind+Dwidth ; i++) {
    mpfi_abs(temp, VD + Dwidth - ind + i);
    mpfi_add(norm, norm, temp);
  }

  for (i = ind+Dwidth+1 ; i < Hwidth ; i++) {
    mpfi_abs(temp, VH + i);
    mpfi_add(norm, norm, temp);
  }

  mpfi_clear(temp);
}


void mpfi_bandvec_1norm_fi(mpfi_t norm, const mpfi_bandvec_t V)
{
  _mpfi_bandvec_1norm_fi(norm, V->H, V->D, V->Hwidth, V->Dwidth, V->ind);
}

