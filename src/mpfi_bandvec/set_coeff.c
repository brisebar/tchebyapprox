
#include "mpfi_bandvec.h"


void _mpfi_bandvec_set_coeff_si(mpfi_ptr VH, mpfi_ptr VD, long Hwidth, long Dwidth, long ind, long n, long c)
{
  if (n >= 0 && n >= ind-Dwidth && n <= ind+Dwidth)
    mpfi_set_si(VD + Dwidth-ind+n, c);

  else if (n >= 0 && n < Hwidth)
    mpfi_set_si(VH + n, c);

  else
    fprintf(stderr, "_mpfi_bandvec_set_coeff_si: error: index out of range\n");
}


void mpfi_bandvec_set_coeff_si(mpfi_bandvec_t V, long n, long c)
{
  _mpfi_bandvec_set_coeff_si(V->H, V->D, V->Hwidth, V->Dwidth, V->ind, n, c);
}


void _mpfi_bandvec_set_coeff_z(mpfi_ptr VH, mpfi_ptr VD, long Hwidth, long Dwidth, long ind, long n, const mpz_t c)
{
  if (n >= 0 && n >= ind-Dwidth && n <= ind+Dwidth)
    mpfi_set_z(VD + Dwidth-ind+n, c);

  else if (n >= 0 && n < Hwidth)
    mpfi_set_z(VH + n, c);

  else
    fprintf(stderr, "_mpfi_bandvec_set_coeff_z: error: index out of range\n");
}


void mpfi_bandvec_set_coeff_z(mpfi_bandvec_t V, long n, const mpz_t c)
{
  _mpfi_bandvec_set_coeff_z(V->H, V->D, V->Hwidth, V->Dwidth, V->ind, n, c);
}


void _mpfi_bandvec_set_coeff_q(mpfi_ptr VH, mpfi_ptr VD, long Hwidth, long Dwidth, long ind, long n, const mpq_t c)
{
  if (n >= 0 && n >= ind-Dwidth && n <= ind+Dwidth)
    mpfi_set_q(VD + Dwidth-ind+n, c);

  else if (n >= 0 && n < Hwidth)
    mpfi_set_q(VH + n, c);

  else
    fprintf(stderr, "_mpfi_bandvec_set_coeff_q: error: index out of range\n");
}


void mpfi_bandvec_set_coeff_q(mpfi_bandvec_t V, long n, const mpq_t c)
{
  _mpfi_bandvec_set_coeff_q(V->H, V->D, V->Hwidth, V->Dwidth, V->ind, n, c);
}


void _mpfi_bandvec_set_coeff_fr(mpfi_ptr VH, mpfi_ptr VD, long Hwidth, long Dwidth, long ind, long n, const mpfr_t c)
{
  if (n >= 0 && n >= ind-Dwidth && n <= ind+Dwidth)
    mpfi_set_fr(VD + Dwidth-ind+n, c);

  else if (n >= 0 && n < Hwidth)
    mpfi_set_fr(VH + n, c);

  else
    fprintf(stderr, "_mpfi_bandvec_set_coeff_fr: error: index out of range\n");
}


void mpfi_bandvec_set_coeff_fr(mpfi_bandvec_t V, long n, const mpfr_t c)
{
  _mpfi_bandvec_set_coeff_fr(V->H, V->D, V->Hwidth, V->Dwidth, V->ind, n, c);
}


void _mpfi_bandvec_set_coeff_fi(mpfi_ptr VH, mpfi_ptr VD, long Hwidth, long Dwidth, long ind, long n, const mpfi_t c)
{
  if (n >= 0 && n >= ind-Dwidth && n <= ind+Dwidth)
    mpfi_set(VD + Dwidth-ind+n, c);

  else if (n >= 0 && n < Hwidth)
    mpfi_set(VH + n, c);

  else
    fprintf(stderr, "_mpfi_bandvec_set_coeff_fr: error: index out of range\n");
}


void mpfi_bandvec_set_coeff_fi(mpfi_bandvec_t V, long n, const mpfi_t c)
{
  _mpfi_bandvec_set_coeff_fi(V->H, V->D, V->Hwidth, V->Dwidth, V->ind, n, c);
}


