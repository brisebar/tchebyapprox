
#include "mpfi_vec.h"


void _mpfi_vec_scalar_mul_si(mpfi_ptr V, mpfi_srcptr W, long n, long c)
{
  long i;
  for (i = 0 ; i < n ; i++)
    mpfi_mul_si(V + i, W + i, c);
}


void _mpfi_vec_scalar_mul_z(mpfi_ptr V, mpfi_srcptr W, long n, const mpz_t c)
{
  long i;
  for (i = 0 ; i < n ; i++)
    mpfi_mul_z(V + i, W + i, c);
}


void _mpfi_vec_scalar_mul_q(mpfi_ptr V, mpfi_srcptr W, long n, const mpq_t c)
{
  long i;
  for (i = 0 ; i < n ; i++)
    mpfi_mul_q(V + i, W + i, c);
}


void _mpfi_vec_scalar_mul_fr(mpfi_ptr V, mpfi_srcptr W, long n, const mpfr_t c)
{
  long i;
  for (i = 0 ; i < n ; i++)
    mpfi_mul_fr(V + i, W + i, c);
}


void _mpfi_vec_scalar_mul_fi(mpfi_ptr V, mpfi_srcptr W, long n, const mpfi_t c)
{
  long i;
  for (i = 0 ; i < n ; i++)
    mpfi_mul(V + i, W + i, c);
}


void _mpfi_vec_scalar_mul_2si(mpfi_ptr V, mpfi_srcptr W, long n, long k)
{
  long i;
  for (i = 0 ; i < n ; i++)
    mpfi_mul_2si(V + i, W + i, k);
}



// set V := c*W
void mpfi_vec_scalar_mul_si(mpfi_vec_t V, const mpfi_vec_t W, long c)
{
  long n = W->length;
  mpfi_vec_set_length(V, n);
  _mpfi_vec_scalar_mul_si(V->coeffs, W->coeffs, n, c);
}


// set V := c*W
void mpfi_vec_scalar_mul_z(mpfi_vec_t V, const mpfi_vec_t W, const mpz_t c)
{
  long n = W->length;
  mpfi_vec_set_length(V, n);
  _mpfi_vec_scalar_mul_z(V->coeffs, W->coeffs, n, c);
}


// set V := c*W
void mpfi_vec_scalar_mul_q(mpfi_vec_t V, const mpfi_vec_t W, const mpq_t c)
{
  long n = W->length;
  mpfi_vec_set_length(V, n);
  _mpfi_vec_scalar_mul_q(V->coeffs, W->coeffs, n, c);
}


// set V := c*W
void mpfi_vec_scalar_mul_fr(mpfi_vec_t V, const mpfi_vec_t W, const mpfr_t c)
{
  long n = W->length;
  mpfi_vec_set_length(V, n);
  _mpfi_vec_scalar_mul_fr(V->coeffs, W->coeffs, n, c);
}


// set V := c*W
void mpfi_vec_scalar_mul_fi(mpfi_vec_t V, const mpfi_vec_t W, const mpfi_t c)
{
  long n = W->length;
  mpfi_vec_set_length(V, n);
  _mpfi_vec_scalar_mul_fi(V->coeffs, W->coeffs, n, c);
}


// set V := 2^k*W
void mpfi_vec_scalar_mul_2si(mpfi_vec_t V, const mpfi_vec_t W, long k)
{
  long n = W->length;
  mpfi_vec_set_length(V, n);
  _mpfi_vec_scalar_mul_2si(V->coeffs, W->coeffs, n, k);
}
