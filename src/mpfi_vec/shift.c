
#include "mpfi_vec.h"


void _mpfi_vec_shift_left(mpfi_ptr V, mpfi_srcptr W, long n, unsigned long k)
{
  long i;
  for (i = n-1 ; i >= 0 ; i--)
    mpfi_set(V + i + k, W + i);
  for (i = 0 ; i < k ; i++)
    mpfi_set_si(V + i, 0);
}


void mpfi_vec_shift_left(mpfi_vec_t V, const mpfi_vec_t W, unsigned long k)
{
  long i;
  long n = W->length;
  mpfi_vec_set_length(V, n+k);
  _mpfi_vec_shift_left(V->coeffs, W->coeffs, n, k);
}


void _mpfi_vec_shift_right(mpfi_ptr V, mpfi_srcptr W, long n, unsigned long k)
{
  long i;
  for (i = 0 ; i+k < n ; i++)
    mpfi_set(V + i, W + i + k);
}


void mpfi_vec_shift_right(mpfi_vec_t V, const mpfi_vec_t W, unsigned long k)
{
  long i;
  long n = W->length;
  if (k >= n)
    mpfi_vec_set_length(V, 0);
  else {
    mpfi_ptr Vcoeffs = malloc((n-k) * sizeof(mpfi_t));
    for (i = 0 ; i < n-k ; i++)
      mpfi_init(Vcoeffs + i);
    _mpfi_vec_shift_right(Vcoeffs, W->coeffs, n, k);
    mpfi_vec_clear(V);
    V->length = n-k;
    V->coeffs = Vcoeffs;
  }
}
