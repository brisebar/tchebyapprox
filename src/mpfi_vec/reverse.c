
#include "mpfi_vec.h"


// reverse a list of mpfi of size (n+1)
void _mpfi_vec_reverse(mpfi_ptr V, mpfi_srcptr W, long n)
{
  long i;
  mpfi_ptr R = malloc(n * sizeof(mpfi_t));
  for (i = 0 ; i < n ; i++) {
    mpfi_init(R + i);
    mpfi_set(R + i, W + n-1-i);
  }
  for (i = 0 ; i < n ; i++) {
    mpfi_swap(V + i, R + i);
    mpfi_clear(R + i);
  }
}


void mpfi_vec_reverse(mpfi_vec_t V, const mpfi_vec_t W)
{
  if (V != W)
    mpfi_vec_set(V, W);
  _mpfi_vec_reverse(V->coeffs, V->coeffs, V->length);
}
