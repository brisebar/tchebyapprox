
#include "mpfi_vec.h"




// set the n-th coefficient of V to c
void mpfi_vec_set_coeff_si(mpfi_vec_t V, long n, long c)
{
  if (n < 0 || n >= V->length)
    return;

  else
    mpfi_set_si(V->coeffs + n, c);
}


// set the n-th coefficient of V to c
void mpfi_vec_set_coeff_z(mpfi_vec_t V, long n, const mpz_t c)
{
  if (n < 0 || n >= V->length)
    return;

  else
    mpfi_set_z(V->coeffs + n, c);
}


// set the n-th coefficient of V to c
void mpfi_vec_set_coeff_q(mpfi_vec_t V, long n, const mpq_t c)
{
  if (n < 0 || n >= V->length)
    return;

  else
    mpfi_set_q(V->coeffs + n, c);
} 


// set the n-th coefficient of V to c
void mpfi_vec_set_coeff_fr(mpfi_vec_t V, long n, const mpfr_t c)
{
  if (n < 0 || n >= V->length)
    return;

  else
    mpfi_set_fr(V->coeffs + n, c);
}


// set the n-th coefficient of V to c
void mpfi_vec_set_coeff_fi(mpfi_vec_t V, long n, const mpfi_t c)
{
  if (n < 0 || n >= V->length)
    return;

  else
    mpfi_set(V->coeffs + n, c);
}

