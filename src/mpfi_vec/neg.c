
#include "mpfi_vec.h"


void _mpfi_vec_neg(mpfi_ptr V, mpfi_srcptr W, long n)
{
  long i;
  for (i = 0 ; i < n ; i++)
    mpfi_neg(V + i, W + i);
}


// set V := -W
void mpfi_vec_neg(mpfi_vec_t V, const mpfi_vec_t W)
{
  mpfi_vec_set_length(V, W->length);
  _mpfi_vec_neg(V->coeffs, W->coeffs, W->length);
}
