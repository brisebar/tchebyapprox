
#include "mpfi_vec.h"


// set n as length for V
void mpfi_vec_set_length(mpfi_vec_t V, long n)
{
  long m = V->length;
  long i;

  if (n < m) {
    for (i = n ; i < m ; i++)
      mpfi_clear(V->coeffs + i);
    V->coeffs = realloc(V->coeffs, n * sizeof(mpfi_t));
  }
  else if (n > m) {
    V->coeffs = realloc(V->coeffs, n * sizeof(mpfi_t));
    for (i = m ; i < n ; i++) {
      mpfi_init(V->coeffs + i);
      mpfi_set_si(V->coeffs + i, 0);
    }
  }

  V->length = n;
}
