
#include "mpfi_vec.h"


// swap V and W efficiently
void mpfi_vec_swap(mpfi_vec_t V, mpfi_vec_t W)
{
  long length_buf = V->length;
  mpfi_ptr coeffs_buf = V->coeffs;
  V->length = W->length;
  V->coeffs = W->coeffs;
  W->length = length_buf;
  W->coeffs = coeffs_buf;
}
