#include "mpfi_vec.h"


// clear a vector of n mpfi_t
void _mpfi_vec_clear(mpfi_ptr V, long n)
{
  long i;
  for (i = 0 ; i < n ; i++)
    mpfi_clear(V + i);
  free(V);
}


// clear V
void mpfi_vec_clear(mpfi_vec_t V)
{
  long n = V->length;
  long i;
  for (i = 0 ; i < n ; i++)
    mpfi_clear(V->coeffs + i);
  free(V->coeffs);
}
