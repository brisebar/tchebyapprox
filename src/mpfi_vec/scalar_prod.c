
#include "mpfi_vec.h"


void _mpfi_vec_scalar_prod_fi(mpfi_t prod, mpfi_srcptr V, mpfi_srcptr W, long n)
{
  mpfi_set_si(prod, 0);
  mpfi_t temp;
  mpfi_init(temp);
  long i;
  for (i = 0 ; i < n ; i++) {
    mpfi_mul(temp, V + i, W + i);
    mpfi_add(prod, prod, temp);
  }
  mpfi_clear(temp);
} 


void mpfi_vec_scalar_prod_fi(mpfi_t prod, const mpfi_vec_t V, const mpfi_vec_t W)
{
  if (V->length != W->length)
    fprintf(stderr, "mpfi_vec_scalar_prod_fi: error: incompatible lengths\n");
  else
    _mpfi_vec_scalar_prod_fi(prod, V->coeffs, W->coeffs, V->length);
}
