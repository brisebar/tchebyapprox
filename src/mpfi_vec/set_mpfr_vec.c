
#include "mpfi_vec.h"


void _mpfi_vec_set_mpfr_vec(mpfi_ptr V, mpfr_srcptr W, long length)
{
  long i;
  for (i = 0 ; i < length ; i++)
    mpfi_set_fr(V + i, W + i);
}


void mpfi_vec_set_mpfr_vec(mpfi_vec_t V, const mpfr_vec_t W)
{
  long n = W->length;

  mpfi_vec_set_length(V, n);

  _mpfi_vec_set_mpfr_vec(V->coeffs, W->coeffs, n);
}
