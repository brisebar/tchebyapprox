
#include "mpfi_vec.h"


void _mpfi_vec_sub(mpfi_ptr V, mpfi_srcptr W, mpfi_srcptr Z, long n)
{
  long i;
  for (i = 0 ; i < n ; i++)
    mpfi_sub(V + i, W + i, Z + i);
}


// set V := W + R
void mpfi_vec_sub(mpfi_vec_t V, const mpfi_vec_t W, const mpfi_vec_t Z)
{
  long n = W->length;
  if (n != Z->length) {
    fprintf(stderr, "mpfi_vec_sub: error: incompatible lengths\n");
    return;
  }
  mpfi_vec_set_length(V, n);
  _mpfi_vec_sub(V->coeffs, W->coeffs, Z->coeffs, n);
}

