
#include "mpfi_vec.h"


void _mpfi_vec_print(mpfi_srcptr V, long n, size_t digits)
{
  printf("[");
  long i;
  for (i = 0 ; i < n ; i++) {
    mpfi_out_str(stdout, 10, digits, V + i);
    if (i < n-1)
      printf(", ");
  }
  printf("]");
}


// display the vector V
void mpfi_vec_print(const mpfi_vec_t V, size_t digits)
{
  _mpfi_vec_print(V->coeffs, V->length, digits);
}

