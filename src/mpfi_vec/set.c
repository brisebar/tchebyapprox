
#include "mpfi_vec.h"


void _mpfi_vec_set(mpfi_ptr V, mpfi_srcptr W, long n)
{
  long i ;
  for (i = 0 ; i < n ; i++)
    mpfi_set(V + i, W + i);
}


// copy W into V
void mpfi_vec_set(mpfi_vec_t V, const mpfi_vec_t W)
{
  long n = W->length;
  long i;
  if (V != W) {
    mpfi_vec_set_length(V, n);
    for (i = 0 ; i < n ; i++)
      mpfi_set(V->coeffs + i, W->coeffs + i);
  }
}
