
#include "mpfi_vec.h"


void _mpfi_vec_get_mpfr_vec(mpfr_ptr V, mpfi_srcptr W, long length)
{
  long i;
  for (i = 0 ; i < length ; i++)
    mpfi_mid(V + i, W + i);
}


void mpfi_vec_get_mpfr_vec(mpfr_vec_t V, const mpfi_vec_t W)
{
  long n = W->length;

  mpfr_vec_set_length(V, n);

  _mpfi_vec_get_mpfr_vec(V->coeffs, W->coeffs, n);
}

