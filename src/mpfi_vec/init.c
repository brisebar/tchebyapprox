
#include "mpfi_vec.h"


// init a vector of n mpfi_t
void _mpfi_vec_init(mpfi_ptr V, long n)
{
  long i;
  for (i = 0 ; i < n ; i++)
    mpfi_init(V + i);
}


// init with V = 0
void mpfi_vec_init(mpfi_vec_t V)
{
  V->length = 0;
  V->coeffs = malloc(0);
}


// init V as a zero-vector of size n
void mpfi_vec_init_zero(mpfi_vec_t V, long n)
{
  V->length = n;
  V->coeffs = malloc(n * sizeof(mpfi_t));
  long i;
  for (i = 0 ; i < n ; i++) {
    mpfi_init(V->coeffs + i);
    mpfi_set_si(V->coeffs + i, 0);
  }
}
