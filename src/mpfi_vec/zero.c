
#include "mpfi_vec.h"


void _mpfi_vec_zero(mpfi_ptr V, long n)
{
  long i;
  for (i = 0 ; i < n ; i++)
    mpfi_set_si(V + i, 0);
}


void mpfi_vec_zero(mpfi_vec_t V)
{
_mpfi_vec_zero(V->coeffs, V->length);
}
