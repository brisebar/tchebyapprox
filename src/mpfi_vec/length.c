
#include "mpfi_vec.h"


// get the degree of V
long mpfi_vec_degree(const mpfi_vec_t V)
{
  return V->length;
}
