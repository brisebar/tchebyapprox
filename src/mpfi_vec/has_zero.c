
#include "mpfi_vec.h"


int _mpfi_vec_has_zero(mpfi_srcptr V, long n)
{
  long i;
  for (i = 0 ; i < n ; i++)
    if (!mpfi_has_zero(V + i))
      break;

  return i == n;
}


int mpfi_vec_has_zero(const mpfi_vec_t V)
{
  return _mpfi_vec_has_zero(V->coeffs, V->length);
}

