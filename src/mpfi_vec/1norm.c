
#include "mpfi_vec.h"


void _mpfi_vec_1norm_ubound(mpfr_t norm, mpfi_srcptr V, long n)
{
  long i;
  mpfr_t temp;
  mpfr_init(temp);
  mpfr_set_zero(norm, 1);
  for (i = 0 ; i < n ; i++) {
    mpfi_mag(temp, V + i);
    mpfr_add(norm, norm, temp, MPFR_RNDU);
  }
  mpfr_clear(temp);
}


void mpfi_vec_1norm_ubound(mpfr_t norm, const mpfi_vec_t V)
{
  _mpfi_vec_1norm_ubound(norm, V->coeffs, V->length);
}


void _mpfi_vec_1norm_lbound(mpfr_t norm, mpfi_srcptr V, long n)
{
  long i;
  mpfr_t temp;
  mpfr_init(temp);
  mpfr_set_zero(norm, 1);
  for (i = 0 ; i < n ; i++) {
    mpfi_mig(temp, V + i);
    mpfr_add(norm, norm, temp, MPFR_RNDD);
  }
  mpfr_clear(temp);
}


void mpfi_vec_1norm_lbound(mpfr_t norm, const mpfi_vec_t V)
{
  _mpfi_vec_1norm_lbound(norm, V->coeffs, V->length);
}


void _mpfi_vec_1norm_fi(mpfi_t norm, mpfi_srcptr V, long n)
{
  long i;
  mpfi_t temp;
  mpfi_init(temp);
  mpfi_set_si(norm, 0);
  for (i = 0 ; i < n ; i++) {
    mpfi_abs(temp, V + i);
    mpfi_add(norm, norm, temp);
  }
  mpfi_clear(temp);
}


void mpfi_vec_1norm_fi(mpfi_t norm, const mpfi_vec_t V)
{
  _mpfi_vec_1norm_fi(norm, V->coeffs, V->length);
}
