
#include "mpfi_chebpoly_vec_lode.h"


void mpfi_chebpoly_vec_lode_clear(mpfi_chebpoly_vec_lode_t L)
{
  long r = L->order;
  long n = L->dim;
  long i, j, k;
  for (i = 0 ; i < r ; i++) {
    for (j = 0 ; j < n ; j++) {
      for (k = 0 ; k < n ; k++)
	mpfi_chebpoly_clear(L->A[i][j] + k);
      free(L->A[i][j]);
    }
    free(L->A[i]);
  }
  free(L->A);
}
