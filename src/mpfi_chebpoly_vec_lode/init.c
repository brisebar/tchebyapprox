
#include "mpfi_chebpoly_vec_lode.h"


void mpfi_chebpoly_vec_lode_init(mpfi_chebpoly_vec_lode_t L)
{
  L->order = 0;
  L->dim = 0;
  L->A = malloc(0);
}
