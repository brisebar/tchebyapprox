
#include "mpfi_chebpoly_vec_lode.h"



void mpfi_chebpoly_vec_lode_evaluate_d(mpfi_chebpoly_vec_t P, const mpfi_chebpoly_vec_lode_t L, const double_chebpoly_vec_t Q)
{

  if (L->dim != Q->dim) {
    fprintf(stderr, "mpfi_chebpoly_vec_lode_evaluate_d: error: incompatible dimensions\n");
    return;
  }

  mpfi_chebpoly_vec_t Qfi;
  mpfi_chebpoly_vec_init(Qfi);
  mpfi_chebpoly_vec_set_double_chebpoly_vec(Qfi, Q);
  mpfi_chebpoly_vec_lode_evaluate_fi(P, L, Qfi);
  mpfi_chebpoly_vec_clear(Qfi);
}


void mpfi_chebpoly_vec_lode_evaluate_fr(mpfi_chebpoly_vec_t P, const mpfi_chebpoly_vec_lode_t L, const mpfr_chebpoly_vec_t Q)
{

  if (L->dim != Q->dim) {
    fprintf(stderr, "mpfi_chebpoly_vec_lode_evaluate_fr: error: incompatible dimensions\n");
    return;
  }

  mpfi_chebpoly_vec_t Qfi;
  mpfi_chebpoly_vec_init(Qfi);
  mpfi_chebpoly_vec_set_mpfr_chebpoly_vec(Qfi, Q);
  mpfi_chebpoly_vec_lode_evaluate_fi(P, L, Qfi);
  mpfi_chebpoly_vec_clear(Qfi);
}


void mpfi_chebpoly_vec_lode_evaluate_fi(mpfi_chebpoly_vec_t P, const mpfi_chebpoly_vec_lode_t L, const mpfi_chebpoly_vec_t Q)
{

  if (L->dim != Q->dim) {
    fprintf(stderr, "mpfi_chebpoly_vec_lode_evaluate_fi: error: incompatible dimensions\n");
    return;
  }

  mpfi_chebpoly_vec_t L_Q, d_Q; 
  mpfi_chebpoly_t F;
  mpfi_chebpoly_vec_init(L_Q);
  mpfi_chebpoly_vec_init(d_Q);
  mpfi_chebpoly_init(F);
  mpfi_chebpoly_vec_set(d_Q, Q);

  long r = L->order;
  long n = L->dim;
  mpfi_chebpoly_vec_set_dim(L_Q, n);

  long i, j, k;
  for (i = 0 ; i < r ; i++) {
    
    for (j = 0 ; j < n ; j++)
      for (k = 0 ; k < n ; k++) {
        mpfi_chebpoly_mul(F, L->A[i][j] + k, d_Q->poly + k);
	mpfi_chebpoly_add(L_Q->poly + j, L_Q->poly + j, F);
      }

    mpfi_chebpoly_vec_derivative(d_Q, d_Q);

  }

  mpfi_chebpoly_vec_add(L_Q, L_Q, d_Q);

  mpfi_chebpoly_vec_swap(P, L_Q);
  mpfi_chebpoly_vec_clear(L_Q);
  mpfi_chebpoly_vec_clear(d_Q);
  mpfi_chebpoly_clear(F);
}


