
#include "mpfi_chebpoly_vec_lode.h"


void mpfi_chebpoly_vec_lode_set(mpfi_chebpoly_vec_lode_t L, const mpfi_chebpoly_vec_lode_t M)
{
  if (L != M) {
    long r = M->order;
    long n = M->dim;
    mpfi_chebpoly_vec_lode_set_order_dim(L, r, n);

    long i, j, k;
    for (i = 0 ; i < r ; i++)
      for (j = 0 ; j < n ; j++)
	for (k = 0 ; k < n ; k++)
	  mpfi_chebpoly_set(L->A[i][j] + k, M->A[i][j] + k);
  }
}


void mpfi_chebpoly_vec_lode_set_double_chebpoly_vec_lode(mpfi_chebpoly_vec_lode_t L, const double_chebpoly_vec_lode_t M)
{
  long r = M->order;
  long n = M->dim;
  mpfi_chebpoly_vec_lode_set_order_dim(L, r, n);

  long i, j, k;
  for (i = 0 ; i < r ; i++)
    for (j = 0 ; j < n ; j++)
      for (k = 0 ; k < n ; k++)
	mpfi_chebpoly_set_double_chebpoly(L->A[i][j] + k, M->A[i][j] + k);
}


void mpfi_chebpoly_vec_lode_set_mpfr_chebpoly_vec_lode(mpfi_chebpoly_vec_lode_t L, const mpfr_chebpoly_vec_lode_t M)
{
  long r = M->order;
  long n = M->dim;
  mpfi_chebpoly_vec_lode_set_order_dim(L, r, n);

  long i, j, k;
  for (i = 0 ; i < r ; i++)
    for (j = 0 ; j < n ; j++)
      for (k = 0 ; k < n ; k++)
	mpfi_chebpoly_set_mpfr_chebpoly(L->A[i][j] + k, M->A[i][j] + k);
}


void mpfi_chebpoly_vec_lode_get_double_chebpoly_vec_lode(double_chebpoly_vec_lode_t L, const mpfi_chebpoly_vec_lode_t M)
{
  long r = M->order;
  long n = M->dim;
  double_chebpoly_vec_lode_set_order_dim(L, r, n);

  long i, j, k;
  for (i = 0 ; i < r ; i++)
    for (j = 0 ; j < n ; j++)
      for (k = 0 ; k < n ; k++)
	mpfi_chebpoly_get_double_chebpoly(L->A[i][j] + k, M->A[i][j] + k);
}


void mpfi_chebpoly_vec_lode_get_mpfr_chebpoly_vec_lode(mpfr_chebpoly_vec_lode_t L, const mpfi_chebpoly_vec_lode_t M)
{
  long r = M->order;
  long n = M->dim;
  mpfr_chebpoly_vec_lode_set_order_dim(L, r, n);

  long i, j, k;
  for (i = 0 ; i < r ; i++)
    for (j = 0 ; j < n ; j++)
      for (k = 0 ; k < n ; k++)
	mpfi_chebpoly_get_mpfr_chebpoly(L->A[i][j] + k, M->A[i][j] + k);
}


void mpfi_chebpoly_vec_lode_get_mpfi_chebpoly_lode(mpfi_chebpoly_lode_t Lij, const mpfi_chebpoly_vec_lode_t L, long i, long j)
{
  long r = L->order;
  mpfi_chebpoly_lode_set_order(Lij, r);
  long k;

  for (k = 0 ; k < r ; k++)
    mpfi_chebpoly_set(Lij->a + k, L->A[k][i] + j);
}


