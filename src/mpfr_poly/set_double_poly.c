
#include "mpfr_poly.h"


void _mpfr_poly_set_double_poly(mpfr_ptr P, double_srcptr Q, long n)
{
  long i ;
  for (i = 0 ; i <= n ; i++)
    mpfr_set_d(P + i, Q[i], MPFR_RNDN);
}


// copy Q into P
void mpfr_poly_set_double_poly(mpfr_poly_t P, const double_poly_t Q)
{
  mpfr_poly_set_degree(P, Q->degree);
  _mpfr_poly_set_double_poly(P->coeffs, Q->coeffs, Q->degree);
}
