
#include "mpfr_poly.h"


void _mpfr_poly_scalar_div_si(mpfr_ptr P, mpfr_srcptr Q, long n, long c)
{
  long i;
  for (i = 0 ; i <= n ; i++)
    mpfr_div_si(P + i, Q + i, c, MPFR_RNDN);
}


void _mpfr_poly_scalar_div_z(mpfr_ptr P, mpfr_srcptr Q, long n, const mpz_t c)
{
  long i;
  for (i = 0 ; i <= n ; i++)
    mpfr_div_z(P + i, Q + i, c, MPFR_RNDN);
}


void _mpfr_poly_scalar_div_q(mpfr_ptr P, mpfr_srcptr Q, long n, const mpq_t c)
{
  long i;
  for (i = 0 ; i <= n ; i++)
    mpfr_div_q(P + i, Q + i, c, MPFR_RNDN);
}


void _mpfr_poly_scalar_div_d(mpfr_ptr P, mpfr_srcptr Q, long n, const double_t c)
{
  long i;
  for (i = 0 ; i <= n ; i++)
    mpfr_div_d(P + i, Q + i, *c, MPFR_RNDN);
}


void _mpfr_poly_scalar_div_fr(mpfr_ptr P, mpfr_srcptr Q, long n, const mpfr_t c)
{
  long i;
  for (i = 0 ; i <= n ; i++)
    mpfr_div(P + i, Q + i, c, MPFR_RNDN);
}


void _mpfr_poly_scalar_div_2si(mpfr_ptr P, mpfr_srcptr Q, long n, long k)
{
  long i;
  for (i = 0 ; i <= n ; i++)
    mpfr_mul_2si(P + i, Q + i, -k, MPFR_RNDN);
}



// set P := (1/c)*Q
void mpfr_poly_scalar_div_si(mpfr_poly_t P, const mpfr_poly_t Q, long c)
{
  long n = Q->degree;
  mpfr_poly_set_degree(P, n);
  _mpfr_poly_scalar_div_si(P->coeffs, Q->coeffs, n, c);
}


// set P := (1/c)*Q
void mpfr_poly_scalar_div_z(mpfr_poly_t P, const mpfr_poly_t Q, const mpz_t c)
{
  long n = Q->degree;
  mpfr_poly_set_degree(P, n);
  _mpfr_poly_scalar_div_z(P->coeffs, Q->coeffs, n, c);
}


// set P := (1/c)*Q
void mpfr_poly_scalar_div_q(mpfr_poly_t P, const mpfr_poly_t Q, const mpq_t c)
{
  long n = Q->degree;
  mpfr_poly_set_degree(P, n);
  _mpfr_poly_scalar_div_q(P->coeffs, Q->coeffs, n, c);
}


// set P := (1/c)*Q
void mpfr_poly_scalar_div_d(mpfr_poly_t P, const mpfr_poly_t Q, const double_t c)
{
  long n = Q->degree;
  mpfr_poly_set_degree(P, n);
  _mpfr_poly_scalar_div_d(P->coeffs, Q->coeffs, n, c);
}


// set P := (1/c)*Q
void mpfr_poly_scalar_div_fr(mpfr_poly_t P, const mpfr_poly_t Q, const mpfr_t c)
{
  long n = Q->degree;
  mpfr_poly_set_degree(P, n);
  _mpfr_poly_scalar_div_fr(P->coeffs, Q->coeffs, n, c);
}


// set P := 2^-k*Q
void mpfr_poly_scalar_div_2si(mpfr_poly_t P, const mpfr_poly_t Q, long k)
{
  long n = Q->degree;
  mpfr_poly_set_degree(P, n);
  _mpfr_poly_scalar_div_2si(P->coeffs, Q->coeffs, n, k);
}
