
#include "mpfr_poly.h"


void _mpfr_poly_get_double_poly(double_ptr P, mpfr_srcptr Q, long n)
{
  long i;
  for (i = 0 ; i <= n ; i++)
    double_set_fr(P + i, Q + i, MPFR_RNDN);
}

void mpfr_poly_get_double_poly(double_poly_t P, const mpfr_poly_t Q)
{
  long n = Q->degree;

  if (n < 0)
    double_poly_zero(P);

  else {
    double_poly_set_degree(P, n);
    _mpfr_poly_get_double_poly(P->coeffs, Q->coeffs, n);
  }
}
