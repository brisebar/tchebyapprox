
#include "mpfr_poly.h"


// swap P and Q efficiently
void mpfr_poly_swap(mpfr_poly_t P, mpfr_poly_t Q)
{
  long degree_buf = P->degree;
  mpfr_ptr coeffs_buf = P->coeffs;
  P->degree = Q->degree;
  P->coeffs = Q->coeffs;
  Q->degree = degree_buf;
  Q->coeffs = coeffs_buf;
}
