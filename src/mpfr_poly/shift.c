
#include "mpfr_poly.h"


void _mpfr_poly_shift_left(mpfr_ptr P, mpfr_srcptr Q, long n, unsigned long k)
{
  long i;
  for (i = n ; i >= 0 ; i--)
    mpfr_set(P + i + k, Q + i, MPFR_RNDN);
  for (i = 0 ; i < k ; i++)
    mpfr_set_si(P + i, 0, MPFR_RNDN);
}


// set P to Q*X^k
void mpfr_poly_shift_left(mpfr_poly_t P, const mpfr_poly_t Q, unsigned long k)
{
  long i;
  long n = Q->degree;
  mpfr_ptr Pcoeffs = malloc((n+k+1) * sizeof(mpfr_t));
  for (i = 0 ; i <= n+k ; i++)
    mpfr_init(Pcoeffs + i);
  _mpfr_poly_shift_left(Pcoeffs, Q->coeffs, n, k);
  mpfr_poly_clear(P);
  P->degree = n+k;
  P->coeffs = Pcoeffs;
}


void _mpfr_poly_shift_right(mpfr_ptr P, mpfr_srcptr Q, long n, unsigned long k)
{
  long i;
  for (i = 0 ; i+k <= n ; i++)
    mpfr_set(P + i, Q + i + k, MPFR_RNDN);
}


// set P to Q/X^k
void mpfr_poly_shift_right(mpfr_poly_t P, const mpfr_poly_t Q, unsigned long k)
{
  long i;
  long n = Q->degree;
  if (k > n)
    mpfr_poly_zero(P);
  else {
    mpfr_ptr Pcoeffs = malloc((n-k+1) * sizeof(mpfr_t));
    for (i = 0 ; i <= n-k ; i++)
      mpfr_init(Pcoeffs + i);
    _mpfr_poly_shift_right(Pcoeffs, Q->coeffs, n, k);
    mpfr_poly_clear(P);
    P->degree = n-k;
    P->coeffs = Pcoeffs;
  }
}
