
#include "mpfr_poly.h"


void _mpfr_poly_evaluate_si(mpfr_t y, mpfr_srcptr P, long n, long x)
{
  mpfr_t t;
  mpfr_init(t);
  mpfr_set_si(t, x, MPFR_RNDN);
  _mpfr_poly_evaluate_fr(y, P, n, t);
  mpfr_clear(t);
}


void _mpfr_poly_evaluate_z(mpfr_t y, mpfr_srcptr P, long n, const mpz_t x)
{
  mpfr_t t;
  mpfr_init(t);
  mpfr_set_z(t, x, MPFR_RNDN);
  _mpfr_poly_evaluate_fr(y, P, n, t);
  mpfr_clear(t);
}


void _mpfr_poly_evaluate_q(mpfr_t y, mpfr_srcptr P, long n, const mpq_t x)
{
  mpfr_t t;
  mpfr_init(t);
  mpfr_set_q(t, x, MPFR_RNDN);
  _mpfr_poly_evaluate_fr(y, P, n, t);
  mpfr_clear(t);
}


void _mpfr_poly_evaluate_fr(mpfr_t y, mpfr_srcptr P, long n, const mpfr_t x)
{
  if (n < 0)
    mpfr_set_si(y, 0, MPFR_RNDN);

  else {
    long i;
    mpfr_set(y, P + n, MPFR_RNDN);
    for (i = n-1 ; i >= 0 ; i--) {
      mpfr_mul(y, x, y, MPFR_RNDN);
      mpfr_add(y, y, P + i, MPFR_RNDN);
    }
  }
}



// set y to P(x)
void mpfr_poly_evaluate_si(mpfr_t y, const mpfr_poly_t P, long x)
{
  _mpfr_poly_evaluate_si(y, P->coeffs, P->degree, x);
}


// set y to P(x)
void mpfr_poly_evaluate_z(mpfr_t y, const mpfr_poly_t P, const mpz_t x)
{
  _mpfr_poly_evaluate_z(y, P->coeffs, P->degree, x);
}


// set y to P(x)
void mpfr_poly_evaluate_q(mpfr_t y, const mpfr_poly_t P, const mpq_t x)
{
  _mpfr_poly_evaluate_q(y, P->coeffs, P->degree, x);
}


// set y to P(x)
void mpfr_poly_evaluate_fr(mpfr_t y, const mpfr_poly_t P, const mpfr_t x)
{
  _mpfr_poly_evaluate_fr(y, P->coeffs, P->degree, x);
}

