
#include "mpfr_poly.h"


int _mpfr_poly_equal(mpfr_srcptr P, mpfr_srcptr Q, long n)
{
  long i;
  for (i = 0 ; i <= n ; i++)
    if (!mpfr_equal_p(P + i, Q + i))
      break;
  return (i == n+1);
}


int mpfr_poly_equal(const mpfr_poly_t P, const mpfr_poly_t Q)
{
  return (P->degree == Q->degree && _mpfr_poly_equal(P->coeffs, Q->coeffs, P->degree));
}
