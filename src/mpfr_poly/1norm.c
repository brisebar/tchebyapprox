
#include "mpfr_poly.h"


void _mpfr_poly_1norm(mpfr_t y, mpfr_srcptr P, long n)
{
  long i;
  mpfr_set_si(y, 0, MPFR_RNDU);
  for (i = 0 ; i <= n ; i++)
    if (mpfr_sgn(P + i) >= 0)
      mpfr_add(y, y, P + i, MPFR_RNDU);
    else
      mpfr_sub(y, y, P + i, MPFR_RNDU);
}


void mpfr_poly_1norm(mpfr_t y, const mpfr_poly_t P)
{
  _mpfr_poly_1norm(y, P->coeffs, P->degree);
}
