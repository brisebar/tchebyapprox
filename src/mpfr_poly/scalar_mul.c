
#include "mpfr_poly.h"


void _mpfr_poly_scalar_mul_si(mpfr_ptr P, mpfr_srcptr Q, long n, long c)
{
  long i;
  for (i = 0 ; i <= n ; i++)
    mpfr_mul_si(P + i, Q + i, c, MPFR_RNDN);
}


void _mpfr_poly_scalar_mul_z(mpfr_ptr P, mpfr_srcptr Q, long n, const mpz_t c)
{
  long i;
  for (i = 0 ; i <= n ; i++)
    mpfr_mul_z(P + i, Q + i, c, MPFR_RNDN);
}


void _mpfr_poly_scalar_mul_q(mpfr_ptr P, mpfr_srcptr Q, long n, const mpq_t c)
{
  long i;
  for (i = 0 ; i <= n ; i++)
    mpfr_mul_q(P + i, Q + i, c, MPFR_RNDN);
}


void _mpfr_poly_scalar_mul_d(mpfr_ptr P, mpfr_srcptr Q, long n, const double_t c)
{
  long i;
  for (i = 0 ; i <= n ; i++)
    mpfr_mul_d(P + i, Q + i, *c, MPFR_RNDN);
}


void _mpfr_poly_scalar_mul_fr(mpfr_ptr P, mpfr_srcptr Q, long n, const mpfr_t c)
{
  long i;
  for (i = 0 ; i <= n ; i++)
    mpfr_mul(P + i, Q + i, c, MPFR_RNDN);
}


void _mpfr_poly_scalar_mul_2si(mpfr_ptr P, mpfr_srcptr Q, long n, long k)
{
  long i;
  for (i = 0 ; i <= n ; i++)
    mpfr_mul_2si(P + i, Q + i, k, MPFR_RNDN);
}



// set P := c*Q
void mpfr_poly_scalar_mul_si(mpfr_poly_t P, const mpfr_poly_t Q, long c)
{
  if (c == 0)
    mpfr_poly_zero(P);
  else {
    long n = Q->degree;
    mpfr_poly_set_degree(P, n);
    _mpfr_poly_scalar_mul_si(P->coeffs, Q->coeffs, n, c);
  }
}


// set P := c*Q
void mpfr_poly_scalar_mul_z(mpfr_poly_t P, const mpfr_poly_t Q, const mpz_t c)
{
  if (!mpz_sgn(c))
    mpfr_poly_zero(P);
  else {
    long n = Q->degree;
    mpfr_poly_set_degree(P, n);
    _mpfr_poly_scalar_mul_z(P->coeffs, Q->coeffs, n, c);
  }
}


// set P := c*Q
void mpfr_poly_scalar_mul_q(mpfr_poly_t P, const mpfr_poly_t Q, const mpq_t c)
{
  if (!mpq_sgn(c))
    mpfr_poly_zero(P);
  else {
    long n = Q->degree;
    mpfr_poly_set_degree(P, n);
    _mpfr_poly_scalar_mul_q(P->coeffs, Q->coeffs, n, c);
  }
}


// set P := c*Q
void mpfr_poly_scalar_mul_d(mpfr_poly_t P, const mpfr_poly_t Q, const double_t c)
{
  if (*c == 0)
    mpfr_poly_zero(P);
  else {
    long n = Q->degree;
    mpfr_poly_set_degree(P, n);
    _mpfr_poly_scalar_mul_d(P->coeffs, Q->coeffs, n, c);
  }
}


// set P := c*Q
void mpfr_poly_scalar_mul_fr(mpfr_poly_t P, const mpfr_poly_t Q, const mpfr_t c)
{
  if (mpfr_zero_p(c))
    mpfr_poly_zero(P);
  else {
    long n = Q->degree;
    mpfr_poly_set_degree(P, n);
    _mpfr_poly_scalar_mul_fr(P->coeffs, Q->coeffs, n, c);
  }
}


// set P := 2^k*Q
void mpfr_poly_scalar_mul_2si(mpfr_poly_t P, const mpfr_poly_t Q, long k)
{
  long n = Q->degree;

  if (n < 0)
    mpfr_poly_zero(P);

  else {
    mpfr_poly_set_degree(P, n);
    _mpfr_poly_scalar_mul_2si(P->coeffs, Q->coeffs, n, k);
  }
}
