
#include "mpfr_poly.h"


// init with P = 0
void mpfr_poly_init(mpfr_poly_t P)
{
  P->degree = -1;
  P->coeffs = malloc(0);
}
