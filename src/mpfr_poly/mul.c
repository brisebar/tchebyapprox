
#include "mpfr_poly.h"


void _mpfr_poly_mul(mpfr_ptr P, mpfr_srcptr Q, long n, mpfr_srcptr R, long m)
{
  long i, j;
  mpfr_ptr S = malloc((n+m+1) * sizeof(mpfr_t));
  mpfr_t temp;
  mpfr_init(temp);

  for (i = 0 ; i <= n+m ; i++) {
    mpfr_init(S + i);
    mpfr_set_si(S + i, 0, MPFR_RNDN);
  }

  for (i = 0 ; i <= n ; i++)
    for (j = 0 ; j <= m ; j++) {
      mpfr_mul(temp, Q + i, R + j, MPFR_RNDN);
      mpfr_add(S + i + j, S + i + j, temp, MPFR_RNDN);
    }

  for (i = 0 ; i <= n+m ; i++) {
    mpfr_swap(P + i, S + i);
    mpfr_clear(S + i);
  }

  mpfr_clear(temp);
  free(S);
}


// set P := Q * R
void mpfr_poly_mul(mpfr_poly_t P, const mpfr_poly_t Q, const mpfr_poly_t R)
{
  long n = Q->degree;
  long m = R->degree;

  if (n < 0 || m < 0)
    mpfr_poly_zero(P);

  else {
    mpfr_poly_set_degree(P, n+m);
    _mpfr_poly_mul(P->coeffs, Q->coeffs, n, R->coeffs, m);
  }
}
