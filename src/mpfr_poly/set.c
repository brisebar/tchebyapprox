
#include "mpfr_poly.h"


void _mpfr_poly_set(mpfr_ptr P, mpfr_srcptr Q, long n)
{
  long i ;
  for (i = 0 ; i <= n ; i++)
    mpfr_set(P + i, Q + i, MPFR_RNDN);
}


// copy Q into P
void mpfr_poly_set(mpfr_poly_t P, const mpfr_poly_t Q)
{
  long n = Q->degree;
  long i;
  if (P != Q) {
    mpfr_poly_set_degree(P, n);
    for (i = 0 ; i <= n ; i++)
      mpfr_set(P->coeffs + i, Q->coeffs + i, MPFR_RNDN);
  }
}
