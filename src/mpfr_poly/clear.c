#include "mpfr_poly.h"


// clear P
void mpfr_poly_clear(mpfr_poly_t P)
{
  long n = P->degree;
  long i;
  for (i = 0 ; i <= n ; i++)
    mpfr_clear(P->coeffs + i);
  free(P->coeffs);
}
