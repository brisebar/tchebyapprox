
#include "mpfr_poly.h"


// set P in canonical form (exact degree)
void mpfr_poly_normalise(mpfr_poly_t P)
{
  long n, i; 
  for (n = P->degree ; n >= 0 ; n--) {
    if (!mpfr_zero_p(P->coeffs + n))
      break;
  }
  if (n < P->degree) {
    for (i = n+1 ; i <= P->degree ; i++)
      mpfr_clear(P->coeffs + i);
    P->degree = n;
    P->coeffs = realloc(P->coeffs, (n+1) * sizeof(mpfr_t));
  }
}
