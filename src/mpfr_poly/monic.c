
#include "mpfr_poly.h"


void _mpfr_poly_monic(mpfr_ptr P, mpfr_srcptr Q, long n)
{
  long i;
  for (i = 0 ; i < n ; i++)
    mpfr_div(P + i, Q + i, Q + n, MPFR_RNDN);
  mpfr_set_si(P + i, 1, MPFR_RNDN);
}


// set P to (1/c)*Q where c is the leading coefficient of P
void mpfr_poly_monic(mpfr_poly_t P, const mpfr_poly_t Q)
{
  long n = Q->degree;

  if (n < 0)
    mpfr_poly_zero(P);

  else {
    mpfr_poly_set_degree(P, n);
    _mpfr_poly_monic(P->coeffs, Q->coeffs, n);
  }
}
