
#include "mpfr_poly.h"


// set P to 0
void mpfr_poly_zero(mpfr_poly_t P)
{
  long n = P->degree;
  long i ;
  for (i = 0 ; i <= n ; i++)
    mpfr_clear(P->coeffs + i);
  P->coeffs = realloc(P->coeffs, 0);
  P->degree = -1;
}
