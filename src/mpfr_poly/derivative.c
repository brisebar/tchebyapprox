
#include "mpfr_poly.h"


void _mpfr_poly_derivative(mpfr_ptr P, mpfr_srcptr Q, long n)
{
  long i;
  for (i = 0 ; i < n ; i++)
    mpfr_mul_si(P + i, Q + i + 1, i+1, MPFR_RNDN);
}


// set P := Q'
void mpfr_poly_derivative(mpfr_poly_t P, const mpfr_poly_t Q)
{
  long n = Q->degree;

  if (n <= 0)
    mpfr_poly_zero(P);

  else {
    mpfr_poly_set_degree(P, n);
    _mpfr_poly_derivative(P->coeffs, Q->coeffs, n);
    mpfr_clear(P->coeffs + n);
    P->degree --;
  }
}
