
#include "mpfr_poly.h"


void _mpfr_poly_add(mpfr_ptr P, mpfr_srcptr Q, long n, mpfr_srcptr R, long m)
{
  long min_n_m = n < m ? n : m;
  long i;

  for (i = 0 ; i <= min_n_m ; i++)
    mpfr_add(P + i, Q + i, R + i, MPFR_RNDN);

  if (P != Q)
    for (i = min_n_m+1 ; i <= n ; i++)
      mpfr_set(P + i, Q + i, MPFR_RNDN);

  if (P != R)
    for (i = min_n_m+1 ; i <= m ; i++)
      mpfr_set(P + i, R + i, MPFR_RNDN);
}


// set P := Q + R
void mpfr_poly_add(mpfr_poly_t P, const mpfr_poly_t Q, const mpfr_poly_t R)
{
  long n = Q->degree;
  long m = R->degree;
  long max_n_m = n < m ? m : n;

  mpfr_poly_set_degree(P, max_n_m);
  _mpfr_poly_add(P->coeffs, Q->coeffs, n, R->coeffs, m);
  mpfr_poly_normalise(P);
}
