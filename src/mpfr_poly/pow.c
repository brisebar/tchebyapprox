
#include "mpfr_poly.h"


// set P := Q^e
void mpfr_poly_pow(mpfr_poly_t P, const mpfr_poly_t Q, unsigned long e)
{
  mpfr_poly_t Qe;
  mpfr_poly_init(Qe);
  mpfr_poly_set_coeff_si(Qe, 0, 1);
  mpfr_poly_t Qi;
  mpfr_poly_init(Qi);
  mpfr_poly_set_coeff_si(Qi, 0, 1);

  while (e != 0) {
    mpfr_poly_mul(Qi, Qi, Q);
    if (e%2 == 1)
      mpfr_poly_mul(Qe, Qe, Qi);
    e /= 2;
  }

  mpfr_poly_clear(Qi);
  mpfr_poly_clear(P);
  P->degree = Qe->degree;
  P->coeffs = Qe->coeffs;
}
