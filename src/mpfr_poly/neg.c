
#include "mpfr_poly.h"


void _mpfr_poly_neg(mpfr_ptr P, mpfr_srcptr Q, long n)
{
  long i;
  for (i = 0 ; i <= n ; i++)
    mpfr_neg(P + i, Q + i, MPFR_RNDN);
}


// set P := -Q
void mpfr_poly_neg(mpfr_poly_t P, const mpfr_poly_t Q)
{
  mpfr_poly_set_degree(P, Q->degree);
  _mpfr_poly_neg(P->coeffs, Q->coeffs, Q->degree);
}
