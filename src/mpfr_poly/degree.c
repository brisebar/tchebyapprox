
#include "mpfr_poly.h"


// get the degree of P
long mpfr_poly_degree(const mpfr_poly_t P)
{
  return P->degree;
}
