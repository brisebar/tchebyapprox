
#include "mpfr_poly.h"


// reverse a list of mpfr of size (n+1)
void _mpfr_poly_reverse(mpfr_ptr P, mpfr_srcptr Q, long n)
{
  long i;
  mpfr_ptr R = malloc((n+1) * sizeof(mpfr_t));
  for (i = 0 ; i <= n ; i++) {
    mpfr_init(R + i);
    mpfr_set(R + i, Q + n - i, MPFR_RNDN);
  }
  for (i = 0 ; i <= n ; i++) {
    mpfr_swap(P + i, R + i);
    mpfr_clear(R + i);
  }
}


// set P to Q(X^-1)*X^(degree(Q))
void mpfr_poly_reverse(mpfr_poly_t P, const mpfr_poly_t Q)
{
  if (P != Q)
    mpfr_poly_set(P, Q);
  _mpfr_poly_reverse(P->coeffs, P->coeffs, P->degree);
  mpfr_poly_normalise(P);
}
