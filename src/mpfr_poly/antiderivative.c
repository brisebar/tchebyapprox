
#include "mpfr_poly.h"


void _mpfr_poly_antiderivative(mpfr_ptr P, mpfr_srcptr Q, long n)
{
  long i;
  for (i = n ; i >= 0 ; i--)
    mpfr_div_si(P + i + 1, Q + i, i+1, MPFR_RNDU);
  mpfr_set_si(P, 0, MPFR_RNDN);
}


// set P st P' = Q and P(0) = 0
void mpfr_poly_antiderivative(mpfr_poly_t P, const mpfr_poly_t Q)
{
  long n = Q->degree;

  if (n < 0)
    mpfr_poly_zero(P);

  else {
    mpfr_poly_set_degree(P, n+1);
    _mpfr_poly_antiderivative(P->coeffs, Q->coeffs, n);
  }
}
