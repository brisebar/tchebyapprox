
#include "mpfr_poly.h"


// get the n-th coefficient of P
void mpfr_poly_get_coeff(mpfr_t c, const mpfr_poly_t P, long n)
{
  if (n < 0 || n > P->degree)
    mpfr_set_si(c, 0, MPFR_RNDN);
  else
    mpfr_set(c, P->coeffs + n, MPFR_RNDN);
}
