
#include "chebmodel_lrintop.h"


/* convert from chebmodel_intop_t */
void chebmodel_lrintop_set_intop(chebmodel_lrintop_t M, const chebmodel_intop_t K)
{
  long r = K->order;
  chebmodel_lrintop_set_length(M, r);
  long i;
  for (i = 0 ; i < r ; i++) {
    chebmodel_set(M->lpoly + i, K->alpha + i);
    chebmodel_zero(M->rpoly + i);
    mpfi_chebpoly_set_coeff_si(M->rpoly[i].poly, i, 1);
  }
}
