
#include "chebmodel_lrintop.h"


/* convert LODE to lrintop */

// to integral equation on last derivative
// from x-D form: y^(r-1) + a[r-1] * y^(r-1) + ... + a[1] * y' + a[0] * y = h
// with initial conditions y^(i)(-1) = v[i] (0 <= i < r)
// to: f + ∫_-1^t K(t,s) * f(s) ds = g

// compute polynomial kernel K(t,s)
void chebmodel_lrintop_set_lode_xD(chebmodel_lrintop_t K, const chebmodel_srcptr a, long r)
{
  long i, j, k;
  chebmodel_t Temp;
  chebmodel_init(Temp);
  mpfi_chebpoly_t gamma, Temp2;
  mpfi_chebpoly_init(gamma);
  mpfi_chebpoly_init(Temp2);

  chebmodel_lrintop_set_length(K, r);
  // rpoly[i](s) = T_i(s)
  for (i = 0 ; i < r ; i++) {
    chebmodel_zero(K->lpoly + i);
    chebmodel_zero(K->rpoly + i);
    mpfi_chebpoly_set_coeff_si(K->rpoly[i].poly, i, 1);
  }

  // compute lpoly by adding (t-s)^i / i! * a[r-1-i](t)

  // (t-s)^i / i! = Sum_{j=0}^i beta[j](t) * T_j(s)  for i=0..r-1
  mpfi_chebpoly_ptr beta = malloc(r * sizeof(mpfi_chebpoly_t));
  for (j = 0 ; j < r ; j++)
    mpfi_chebpoly_init(beta + j);

  // start with i=0
  if (r > 0) {
    mpfi_chebpoly_set_coeff_si(beta + 0, 0, 1);
    chebmodel_add(K->lpoly + 0, K->lpoly + 0, a + r-1);
  }

  for (i = 1 ; i < r ; i++) {

    // update beta[j] s.t. (t-s)^{i-1} / (i-1)! => (t-s)^i / i!
    // by integrating w.r.t t from s to t
    mpfi_chebpoly_zero(gamma);
    for (j = 0 ; j < i ; j++) {
      // primitive along t
      mpfi_chebpoly_antiderivative(beta + j, beta + j);
      // store component at t=s to be subtracted
      mpfi_chebpoly_zero(Temp2);
      mpfi_chebpoly_set_coeff_si(Temp2, j, 1);
      mpfi_chebpoly_mul(Temp2, beta + j, Temp2);
      mpfi_chebpoly_add(gamma, gamma, Temp2);
    }

    // subtract gamma by distributing over the beta[j]
    // assignes b[i] for the first time
    mpfi_chebpoly_set_degree(Temp2, 0);
    for (j = 0 ; j <= i ; j++) {
      mpfi_chebpoly_get_coeff(Temp2->coeffs + 0, gamma, j);
      mpfi_chebpoly_sub(beta + j, beta + j, Temp2);
    }

    // multiply by a[r-1-i] and add to lpoly
    for (j = 0 ; j <= i ; j++) {
      chebmodel_set_mpfi_chebpoly(Temp, beta + j);
      chebmodel_mul(Temp, Temp, a + r-1-i);
      chebmodel_add(K->lpoly + j, K->lpoly + j, Temp);
    }

  }

  // clear variables
  for (i = 0 ; i < r ; i++)
    mpfi_chebpoly_clear(beta + i);
  free(beta);
  chebmodel_clear(Temp);
  mpfi_chebpoly_clear(gamma);
  mpfi_chebpoly_clear(Temp2);
}


// compute initial condition contribution to right hand side
void chebmodel_lrintop_set_lode_xD_initvals(chebmodel_t g, chebmodel_srcptr a, long r, mpfi_srcptr v)
{
  long i;
  chebmodel_zero(g);

  // P = v[r-1] * (t-1)^(r-1-i) / (r-1-i)! + ... + v[i+1] * (t-1) + v[i]
  // Q = P * a[i]
  mpfi_chebpoly_t P;
  mpfi_chebpoly_init(P);
  chebmodel_t Q;
  chebmodel_init(Q);
  for (i = r-1 ; i >= 0 ; i--) {
    mpfi_chebpoly_antiderivative_1(P, P);
    if (P->degree < 0)
      mpfi_chebpoly_set_coeff_fi(P, 0, v + i);
    else
      mpfi_add(P->coeffs + 0, P->coeffs + 0, v + i);
    chebmodel_set_mpfi_chebpoly(Q, P);
    chebmodel_mul(Q, Q, a + i);
    chebmodel_sub(g, g, Q);
  }

  mpfi_chebpoly_clear(P);
  chebmodel_clear(Q);
}


// compute right hand side g
void chebmodel_lrintop_set_lode_xD_rhs(chebmodel_t g, chebmodel_srcptr a, long r, const chebmodel_t h, mpfi_srcptr v)
{
  chebmodel_t P;
  chebmodel_init(P);
  chebmodel_lrintop_set_lode_xD_initvals(P, a, r, v);
  chebmodel_add(g, h, P);
  chebmodel_clear(P);
}


// to integral equation on the same function
// from D-x form: (-D)^(r)y + (-D)^(r-1)(b[r-1] * y) + ... + (-D)(b[1] * y) + b[0] * y = h
// with initial conditions y^[i](-1) = w[i] (0 <= i < r)
// where y^[i] = (-D)^(i)y + (-D)^(i-1)(b[r-1] * y) + ... + b[r-i] * y
// to: y + ∫_-1^t K(t,s) * y(s) ds = g

// compute polynomial kernel K(t,s)
void chebmodel_lrintop_set_lode_Dx(chebmodel_lrintop_t K, const chebmodel_srcptr b, long r)
{
  long i;
  chebmodel_lrintop_set_lode_xD(K, b, r);
  for (i = 0 ; i < r ; i++) {
    chebmodel_swap(K->lpoly + i, K->rpoly + i);
    chebmodel_neg(K->lpoly + i, K->lpoly + i);
  }
}


// compute right hand side g
void chebmodel_lrintop_set_lode_Dx_rhs(chebmodel_t g, long r, const chebmodel_t h, mpfi_srcptr w)
{
  // integrate h r times and apply sign
  long i;
  chebmodel_set(g, h);
  for (i = 0 ; i < r ; i++)
    chebmodel_antiderivative_1(g, g);
  if (r % 2)
    chebmodel_neg(g, g);

  // add initial condition term
  chebmodel_t P;
  chebmodel_init(P);
  chebmodel_lrintop_set_lode_Dx_initvals(P->poly, r, w);
  chebmodel_add(g, g, P);
  chebmodel_clear(P);
}
