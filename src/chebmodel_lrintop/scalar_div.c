
#include "chebmodel_lrintop.h"

void _chebmodel_lrintop_scalar_div_si(chebmodel_ptr Llpoly, chebmodel_ptr Lrpoly, chebmodel_srcptr Klpoly, chebmodel_srcptr Krpoly, long r, long c)
{
  long i;
  for (i = 0 ; i < r ; i++) {
    chebmodel_scalar_div_si(Llpoly + i, Klpoly + i, c);
    chebmodel_set(Lrpoly + i, Krpoly + i);
  }
}


// set L := K / c
void chebmodel_lrintop_scalar_div_si(chebmodel_lrintop_t L, const chebmodel_lrintop_t K, long c)
{
  chebmodel_lrintop_set_length(L, K->length);
  _chebmodel_lrintop_scalar_div_si(L->lpoly, L->rpoly, K->lpoly, K->rpoly, K->length, c);
}


void _chebmodel_lrintop_scalar_div_z(chebmodel_ptr Llpoly, chebmodel_ptr Lrpoly, chebmodel_srcptr Klpoly, chebmodel_srcptr Krpoly, long r, const mpz_t c)
{
  long i;
  for (i = 0 ; i < r ; i++) {
    chebmodel_scalar_div_z(Llpoly + i, Klpoly + i, c);
    chebmodel_set(Lrpoly + i, Krpoly + i);
  }
}


// set L := K / c
void chebmodel_lrintop_scalar_div_z(chebmodel_lrintop_t L, const chebmodel_lrintop_t K, const mpz_t c)
{
  chebmodel_lrintop_set_length(L, K->length);
  _chebmodel_lrintop_scalar_div_z(L->lpoly, L->rpoly, K->lpoly, K->rpoly, K->length, c);
}


void _chebmodel_lrintop_scalar_div_q(chebmodel_ptr Llpoly, chebmodel_ptr Lrpoly, chebmodel_srcptr Klpoly, chebmodel_srcptr Krpoly, long r, const mpq_t c)
{
  long i;
  for (i = 0 ; i < r ; i++) {
    chebmodel_scalar_div_q(Llpoly + i, Klpoly + i, c);
    chebmodel_set(Lrpoly + i, Krpoly + i);
  }
}


// set L := K / c
void chebmodel_lrintop_scalar_div_q(chebmodel_lrintop_t L, const chebmodel_lrintop_t K, const mpq_t c)
{
  chebmodel_lrintop_set_length(L, K->length);
  _chebmodel_lrintop_scalar_div_q(L->lpoly, L->rpoly, K->lpoly, K->rpoly, K->length, c);
}


void _chebmodel_lrintop_scalar_div_d(chebmodel_ptr Llpoly, chebmodel_ptr Lrpoly, chebmodel_srcptr Klpoly, chebmodel_srcptr Krpoly, long r, const double_t c)
{
  long i;
  for (i = 0 ; i < r ; i++) {
    chebmodel_scalar_div_d(Llpoly + i, Klpoly + i, c);
    chebmodel_set(Lrpoly + i, Krpoly + i);
  }
}


// set L := K / c
void chebmodel_lrintop_scalar_div_d(chebmodel_lrintop_t L, const chebmodel_lrintop_t K, const double_t c)
{
  chebmodel_lrintop_set_length(L, K->length);
  _chebmodel_lrintop_scalar_div_d(L->lpoly, L->rpoly, K->lpoly, K->rpoly, K->length, c);
}


void _chebmodel_lrintop_scalar_div_fr(chebmodel_ptr Llpoly, chebmodel_ptr Lrpoly, chebmodel_srcptr Klpoly, chebmodel_srcptr Krpoly, long r, const mpfr_t c)
{
  long i;
  for (i = 0 ; i < r ; i++) {
    chebmodel_scalar_div_fr(Llpoly + i, Klpoly + i, c);
    chebmodel_set(Lrpoly + i, Krpoly + i);
  }
}


// set L := K / c
void chebmodel_lrintop_scalar_div_fr(chebmodel_lrintop_t L, const chebmodel_lrintop_t K, const mpfr_t c)
{
  chebmodel_lrintop_set_length(L, K->length);
  _chebmodel_lrintop_scalar_div_fr(L->lpoly, L->rpoly, K->lpoly, K->rpoly, K->length, c);
}


void _chebmodel_lrintop_scalar_div_2si(chebmodel_ptr Llpoly, chebmodel_ptr Lrpoly, chebmodel_srcptr Klpoly, chebmodel_srcptr Krpoly, long r, long k)
{
  long i;
  for (i = 0 ; i < r ; i++) {
    chebmodel_scalar_div_2si(Llpoly + i, Klpoly + i, k);
    chebmodel_set(Lrpoly + i, Krpoly + i);
  }
}


// set L := 2^-k * K
void chebmodel_lrintop_scalar_div_2si(chebmodel_lrintop_t L, const chebmodel_lrintop_t K, long k)
{
  chebmodel_lrintop_set_length(L, K->length);
  _chebmodel_lrintop_scalar_div_2si(L->lpoly, L->rpoly, K->lpoly, K->rpoly, K->length, k);
}
