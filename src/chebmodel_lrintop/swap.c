
#include "chebmodel_lrintop.h"


void chebmodel_lrintop_swap(chebmodel_lrintop_t K, chebmodel_lrintop_t L)
{
  long length_buf = K->length;
  chebmodel_ptr lpoly_buf = K->lpoly;
  chebmodel_ptr rpoly_buf = K->rpoly;

  K->length = L->length;
  K->lpoly = L->lpoly;
  K->rpoly = L->rpoly;

  L->length = length_buf;
  L->lpoly = lpoly_buf;
  L->rpoly = rpoly_buf;
}
