
#include "chebmodel_lrintop.h"


void chebmodel_lrintop_clear(chebmodel_lrintop_t K)
{
  long r = K->length;
  long i;
  for (i = 0 ; i < r ; i++) {
    chebmodel_clear(K->lpoly + i);
    chebmodel_clear(K->rpoly + i);
  }
  free(K->lpoly);
  free(K->rpoly);
}
