
#include "chebmodel_lrintop.h"


// Mlpoly, Mrpoly must be freshed allocated arrays of chebmodel of size r1+r2
void _chebmodel_lrintop_comp(chebmodel_ptr Mlpoly, chebmodel_ptr Mrpoly, chebmodel_srcptr Klpoly, chebmodel_srcptr Krpoly, long r1, chebmodel_srcptr Llpoly, chebmodel_srcptr Lrpoly, long r2)
{
  chebmodel_t P, Q;
  chebmodel_init(P);
  chebmodel_init(Q);
  long i, j;

  // initialization
  for (i = 0 ; i < r1 ; i++) {
    chebmodel_set(Mlpoly + i, Klpoly + i);
    chebmodel_zero(Mrpoly + i);
  }

  for (j = 0 ; j < r2 ; j++) {
    chebmodel_zero(Mlpoly + r1 + j);
    chebmodel_set(Mrpoly + r1 + j, Lrpoly + j);
  }

  // composition loop
  for (i = 0 ; i < r1 ; i++)
    for (j = 0 ; j < r2 ; j++) {

      // primitive of middle product
      chebmodel_mul(P, Krpoly + i, Llpoly + j);
      chebmodel_antiderivative(P, P);

      // distribute to the left
      chebmodel_mul(Q, Klpoly + i, P);
      chebmodel_add(Mlpoly + r1 + j, Mlpoly + r1 + j, Q);

      // distribute to the right
      chebmodel_mul(Q, P, Lrpoly + j);
      chebmodel_sub(Mrpoly + i, Mrpoly + i, Q);

    }

  chebmodel_clear(P);
  chebmodel_clear(Q);

}


// set M := K ∘ L
void chebmodel_lrintop_comp(chebmodel_lrintop_t M, const chebmodel_lrintop_t K, const chebmodel_lrintop_t L)
{
  chebmodel_lrintop_t K_L;
  chebmodel_lrintop_init(K_L);

  chebmodel_lrintop_set_length(K_L, K->length + L->length);
  _chebmodel_lrintop_comp(K_L->lpoly, K_L->rpoly, K->lpoly, K->rpoly, K->length, L->lpoly, L->rpoly, L->length);

  chebmodel_lrintop_swap(M, K_L);
  chebmodel_lrintop_clear(K_L);
}
