
#include "chebmodel_lrintop.h"


void chebmodel_lrintop_evaluate_d(chebmodel_t P, const chebmodel_lrintop_t K, const double_chebpoly_t Q)
{
  chebmodel_t Qcm;
  chebmodel_init(Qcm);
  chebmodel_set_double_chebpoly(Qcm, Q);
  chebmodel_lrintop_evaluate_cm(P, K, Qcm);
  chebmodel_clear(Qcm);
}


void chebmodel_lrintop_evaluate_fr(chebmodel_t P, const chebmodel_lrintop_t K, const mpfr_chebpoly_t Q)
{
  chebmodel_t Qcm;
  chebmodel_init(Qcm);
  chebmodel_set_mpfr_chebpoly(Qcm, Q);
  chebmodel_lrintop_evaluate_cm(P, K, Qcm);
  chebmodel_clear(Qcm);
}


void chebmodel_lrintop_evaluate_fi(chebmodel_t P, const chebmodel_lrintop_t K, const mpfi_chebpoly_t Q)
{
  chebmodel_t Qcm;
  chebmodel_init(Qcm);
  chebmodel_set_mpfi_chebpoly(Qcm, Q);
  chebmodel_lrintop_evaluate_cm(P, K, Qcm);
  chebmodel_clear(Qcm);
}


void chebmodel_lrintop_evaluate_cm(chebmodel_t P, const chebmodel_lrintop_t K, const chebmodel_t Q)
{
  long r = K->length;
  long i;

  chebmodel_t K_Q, K_Q_i;
  chebmodel_init(K_Q);
  chebmodel_init(K_Q_i);

  for (i = 0 ; i < r ; i++) {
    chebmodel_mul(K_Q_i, K->rpoly + i, Q);
    chebmodel_antiderivative_1(K_Q_i, K_Q_i);
    chebmodel_mul(K_Q_i, K->lpoly + i, K_Q_i);
    chebmodel_add(K_Q, K_Q, K_Q_i);
  }

  // clear variables
  chebmodel_clear(K_Q_i);
  chebmodel_swap(P, K_Q);
  chebmodel_clear(K_Q);
}
