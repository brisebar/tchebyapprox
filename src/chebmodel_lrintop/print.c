
#include "chebmodel_lrintop.h"


void chebmodel_lrintop_print(const chebmodel_lrintop_t K, const char * Cheb_var, size_t digits)
{
  long r = K->length;
  long i;

  for (i = 0 ; i < r ; i++) {
    printf("lpoly[%ld] = ", i);
    chebmodel_print(K->lpoly + i, Cheb_var, digits);
    printf("\nrpoly[%ld] = ", i);
    chebmodel_print(K->rpoly + i, Cheb_var, digits);
    printf("\n");
  }
}
