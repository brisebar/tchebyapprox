
#include "chebmodel_lrintop.h"


// Mlpoly, Mrpoly must be freshed allocated arrays of chebmodel of size r1+r2
void _chebmodel_lrintop_add(chebmodel_ptr Mlpoly, chebmodel_ptr Mrpoly, chebmodel_srcptr Klpoly, chebmodel_srcptr Krpoly, long r1, chebmodel_srcptr Llpoly, chebmodel_srcptr Lrpoly, long r2)
{
  long i;

  for (i = 0 ; i < r1 ; i++) {
    chebmodel_set(Mlpoly + i, Klpoly + i);
    chebmodel_set(Mrpoly + i, Krpoly + i);
  }

  for (i = 0 ; i < r2 ; i++) {
    chebmodel_set(Mlpoly + r1 + i, Llpoly + i);
    chebmodel_set(Mrpoly + r1 + i, Lrpoly + i);
  }

}


void chebmodel_lrintop_add(chebmodel_lrintop_t M, const chebmodel_lrintop_t K, const chebmodel_lrintop_t L)
{
  chebmodel_lrintop_t K_L;
  chebmodel_lrintop_init(K_L);

  chebmodel_lrintop_set_length(K_L, K->length + L->length);
  _chebmodel_lrintop_add(K_L->lpoly, K_L->rpoly, K->lpoly, K->rpoly, K->length, L->lpoly, L->rpoly, L->length);

  chebmodel_lrintop_swap(M, K_L);
  chebmodel_lrintop_clear(K_L);
}
