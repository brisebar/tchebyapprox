
#include "chebmodel_lrintop.h"


void _chebmodel_lrintop_neg(chebmodel_ptr Llpoly, chebmodel_ptr Lrpoly, chebmodel_srcptr Klpoly, chebmodel_srcptr Krpoly, long r)
{
  long i;
  for (i = 0 ; i < r ; i++) {
    chebmodel_neg(Llpoly + i, Klpoly + i);
    chebmodel_set(Lrpoly + i, Krpoly + i);
  }
}

void chebmodel_lrintop_neg(chebmodel_lrintop_t L, const chebmodel_lrintop_t K)
{
  chebmodel_lrintop_set_length(L, K->length);
  _chebmodel_lrintop_neg(L->lpoly, L->rpoly, K->lpoly, K->rpoly, K->length);
}
