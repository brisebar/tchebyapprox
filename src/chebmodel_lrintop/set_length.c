
#include "chebmodel_lrintop.h"


void chebmodel_lrintop_set_length(chebmodel_lrintop_t K, long length)
{
  long r = K->length;
  K->length = length;
  long i;

  if (length < r) {
    for (i = length ; i < r ; i++) {
      chebmodel_clear(K->lpoly + i);
      chebmodel_clear(K->rpoly + i);
    }
    K->lpoly = realloc(K->lpoly, length * sizeof(chebmodel_t));
    K->rpoly = realloc(K->rpoly, length * sizeof(chebmodel_t));
  }

  else if (length > r) {
    K->lpoly = realloc(K->lpoly, length * sizeof(chebmodel_t));
    K->rpoly = realloc(K->rpoly, length * sizeof(chebmodel_t));
    for (i = r ; i < length ; i++) {
      chebmodel_init(K->lpoly + i);
      chebmodel_init(K->rpoly + i);
    }
  }

}
