
#include "mpfr_chebpoly_vec_intop.h"


void mpfr_chebpoly_vec_intop_rhs(mpfr_chebpoly_vec_t G, const mpfr_chebpoly_vec_lode_t L, const mpfr_chebpoly_vec_t g, mpfr_ptr_ptr I)
{
  mpfr_chebpoly_vec_intop_initvals(G, L, I);
  mpfr_chebpoly_vec_add(G, G, g);
}

