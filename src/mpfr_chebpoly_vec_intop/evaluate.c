
#include "mpfr_chebpoly_vec_intop.h"


void mpfr_chebpoly_vec_intop_evaluate_fr(mpfr_chebpoly_vec_t P, const mpfr_chebpoly_vec_intop_t K, const mpfr_chebpoly_vec_t Q)
{
  if (K->dim != Q->dim) {
    fprintf(stderr, "mpfr_chebpoly_vec_intop_evaluate_fr: error: incompatible dimensions\n");
    return;
  }

  long n = K->dim;
  long i, j;
  mpfr_chebpoly_vec_t K_Q;
  mpfr_chebpoly_vec_init(K_Q);
  mpfr_chebpoly_vec_set_dim(K_Q, n);
  mpfr_chebpoly_t F;
  mpfr_chebpoly_init(F);

  for (i = 0 ; i < n ; i++)
    for (j = 0 ; j < n ; j++) {
      mpfr_chebpoly_intop_evaluate_fr(F, K->intop[i] + j, Q->poly + j);
      mpfr_chebpoly_add(K_Q->poly + i, K_Q->poly + i, F);
    }

  mpfr_chebpoly_vec_swap(P, K_Q);
  mpfr_chebpoly_vec_clear(K_Q);
  mpfr_chebpoly_clear(F);
}


void mpfr_chebpoly_vec_intop_evaluate_fi(mpfi_chebpoly_vec_t P, const mpfr_chebpoly_vec_intop_t K, const mpfi_chebpoly_vec_t Q)
{
  if (K->dim != Q->dim) {
    fprintf(stderr, "mpfr_chebpoly_vec_intop_evaluate_fi: error: incompatible dimensions\n");
    return;
  }

  long n = K->dim;
  long i, j;
  mpfi_chebpoly_vec_t K_Q;
  mpfi_chebpoly_vec_init(K_Q);
  mpfi_chebpoly_vec_set_dim(K_Q, n);
  mpfi_chebpoly_t F;
  mpfi_chebpoly_init(F);

  for (i = 0 ; i < n ; i++)
    for (j = 0 ; j < n ; j++) {
      mpfr_chebpoly_intop_evaluate_fi(F, K->intop[i] + j, Q->poly + j);
      mpfi_chebpoly_add(K_Q->poly + i, K_Q->poly + i, F);
    }

  mpfi_chebpoly_vec_swap(P, K_Q);
  mpfi_chebpoly_vec_clear(K_Q);
  mpfi_chebpoly_clear(F);
}


