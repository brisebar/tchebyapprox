
#include "mpfr_chebpoly_vec_intop.h"


void mpfr_chebpoly_vec_intop_set_lode(mpfr_chebpoly_vec_intop_t K, const mpfr_chebpoly_vec_lode_t L)
{
  long n = L->dim;
  mpfr_chebpoly_vec_intop_set_dim(K, n);
  long i, j;
  mpfr_chebpoly_lode_t Lij;
  mpfr_chebpoly_lode_init(Lij);
  
  for (i = 0 ; i < n ; i++)
    for (j = 0 ; j < n ; j++) {
      mpfr_chebpoly_vec_lode_get_mpfr_chebpoly_lode(Lij, L, i, j);
      mpfr_chebpoly_intop_set_lode(K->intop[i] + j, Lij);
    }

  mpfr_chebpoly_lode_clear(Lij);
}


