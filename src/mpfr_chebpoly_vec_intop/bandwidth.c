
#include "mpfr_chebpoly_vec_intop.h"


long mpfr_chebpoly_vec_intop_Hwidth(const mpfr_chebpoly_vec_intop_t K)
{
  long n = K->dim;
  long i, j;
  long h = 0;
  long hij;
  
  for (i = 0 ; i < n ; i++)
    for (j = 0 ; j < n ; j++) {
      hij = mpfr_chebpoly_intop_Hwidth(K->intop[i] + j);
      if (hij > h)
	h = hij;
    }

  h *= n;

  return h;
}
      

long mpfr_chebpoly_vec_intop_Dwidth(const mpfr_chebpoly_vec_intop_t K)
{
  long n = K->dim;
  long i, j;
  long d = 0;
  long dij;
  
  for (i = 0 ; i < n ; i++)
    for (j = 0 ; j < n ; j++) {
      dij = mpfr_chebpoly_intop_Dwidth(K->intop[i] + j);
      if (dij > d)
	d = dij;
    }

  d = n*d + n-1;

  return d;
}
 

