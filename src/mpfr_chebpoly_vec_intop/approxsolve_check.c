
#include "mpfr_chebpoly_vec_intop.h"



void mpfr_chebpoly_vec_intop_approxsolve_check(mpfr_ptr bounds, mpfr_chebpoly_vec_srcptr Sols, const mpfr_chebpoly_vec_lode_t L, const mpfr_chebpoly_vec_t g, mpfr_ptr_ptr I)
{
  long r = L->order; 
  long n = L->dim;
  long i, j, k;

  mpfr_chebpoly_t P, Temp;
  mpfr_chebpoly_init(P);
  mpfr_chebpoly_init(Temp);
  mpfr_t t;
  mpfr_init(t);

  for (j = 0 ; j < n ; j++) {

    // error in the differential equation
    mpfr_chebpoly_set(P, Sols[r].poly + j);
    for (i = 0 ; i < r ; i++)
      for (k = 0 ; k < n ; k++) {
        mpfr_chebpoly_mul(Temp, L->A[i][j] + k, Sols[i].poly + k);
        mpfr_chebpoly_add(P, P, Temp);
      }
    mpfr_chebpoly_sub(P, P, g->poly + j);
    mpfr_chebpoly_1norm(bounds + j, P);

    // error in the initial conditions
    for (i = 0 ; i < r ; i++) {
      mpfr_chebpoly_evaluate_si(t, Sols[i].poly + j, -1);
      mpfr_sub(t, t, I[j] + i, MPFR_RNDN);
      mpfr_abs(t, t, MPFR_RNDU);
      mpfr_add(bounds + j, bounds + j, t, MPFR_RNDU);
    }

  }
  
  mpfr_clear(t);
  mpfr_chebpoly_clear(P);
  mpfr_chebpoly_clear(Temp);
}

