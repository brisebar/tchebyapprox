
#include "mpfr_chebpoly_vec_intop.h"


void mpfr_chebpoly_vec_intop_approxsolve(mpfr_chebpoly_vec_t F, const mpfr_chebpoly_vec_intop_t K, const mpfr_chebpoly_vec_t G, long N)
{
  long i, j, imax;
  long n = K->dim;
  long Ntrunc = N;
  long NN = n * (N+1);
  long d = mpfr_chebpoly_vec_intop_Dwidth(K);
  if (NN < 2*d) {
    Ntrunc = 2*d/n;
    NN = n * (Ntrunc+1);
  }

  // matrix associated to K
  mpfr_bandmatrix_t M;
  mpfr_bandmatrix_init(M);
  mpfr_chebpoly_vec_intop_get_bandmatrix(M, K, Ntrunc);
  long h = M->Hwidth;
  d = M->Dwidth;

  mpfr_bandmatrix_neg(M, M);
  for (i = 0 ; i < NN ; i++)
    mpfr_add_si(M->D[i] + d, M->D[i] + d, 1, MPFR_RNDN);

  // QR factorization
  mpfr_bandmatrix_QRdecomp_t MM;
  mpfr_bandmatrix_QRdecomp_init(MM);
  mpfr_bandmatrix_get_QRdecomp(MM, M);

  // creating rhs vector
  mpfr_vec_t V;
  mpfr_vec_init(V);
  mpfr_vec_set_length(V, NN);
  mpfr_vec_zero(V);
  for (j = 0 ; j < n ; j++) {
    imax = G->poly[j].degree < Ntrunc ? G->poly[j].degree : Ntrunc;
    for (i = 0 ; i <= imax; i++)
      mpfr_set(V->coeffs + n*i+j, G->poly[j].coeffs + i, MPFR_RNDN);
  }

  // linear system solving
  mpfr_bandmatrix_QRdecomp_solve_fr(V, MM, V);

  // reconstituing polynomials
  mpfr_chebpoly_vec_set(F, G);
  for (j = 0 ; j < n ; j++) {
    if (F->poly[j].degree < Ntrunc)
      mpfr_chebpoly_set_degree(F->poly + j, Ntrunc);
    for (i = 0 ; i <= Ntrunc ; i++)
      mpfr_set(F->poly[j].coeffs + i, V->coeffs + n*i+j, MPFR_RNDN);
    mpfr_chebpoly_normalise(F->poly + j);
  }

  if (Ntrunc > N)
    for (j = 0 ; j < n ; j++)
      mpfr_chebpoly_set_degree(F->poly + j, N);

  // clear variables
  mpfr_bandmatrix_clear(M);
  mpfr_bandmatrix_QRdecomp_clear(MM);
  mpfr_vec_clear(V);
}


void mpfr_chebpoly_vec_lode_intop_approxsolve(mpfr_chebpoly_vec_ptr Sols, const mpfr_chebpoly_vec_lode_t L, const mpfr_chebpoly_vec_t g, mpfr_ptr_ptr I, long N)
{
  if (L->dim != g->dim) {
    fprintf(stderr, "mpfr_chebpoly_vec_lode_intop_approxsolve: error: incompatible dimensions\n");
    return;
  }

  long r = L->order;
  long n = L->dim;
  long i, j;

  // creating integral operator intop
  mpfr_chebpoly_vec_intop_t K;
  mpfr_chebpoly_vec_intop_init(K);
  mpfr_chebpoly_vec_intop_set_lode(K, L);

  // constructing rhs
  mpfr_chebpoly_vec_t G;
  mpfr_chebpoly_vec_init(G);
  mpfr_chebpoly_vec_intop_rhs(G, L, g, I);

  // solving for the r-th derivative using intop
  mpfr_chebpoly_vec_intop_approxsolve(Sols + r, K, G, N);

  // computing the other derivatives
  mpfr_chebpoly_t C;
  mpfr_chebpoly_init(C);
  mpfr_chebpoly_set_degree(C, 0);
  for (i = r - 1 ; i >= 0 ; i--) {
    mpfr_chebpoly_vec_antiderivative_1(Sols + i, Sols + i + 1);
    for (j = 0 ; j < n ; j++) {
      mpfr_chebpoly_set_coeff_fr(C, 0, I[j] + i);
      mpfr_chebpoly_add(Sols[i].poly + j, Sols[i].poly + j, C);
    }
  }

  // clear variables
  mpfr_chebpoly_vec_intop_clear(K);
  mpfr_chebpoly_vec_clear(G);
  mpfr_chebpoly_clear(C);
}


