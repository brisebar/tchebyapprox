
#include "mpfr_chebpoly_vec_intop.h"


void mpfr_chebpoly_vec_intop_get_bandmatrix(mpfr_bandmatrix_t M, const mpfr_chebpoly_vec_intop_t K, long N)
{
  long n = K->dim;
  long i, j;
  
  // create the matrices for all intops K[i][j]
  mpfr_bandmatrix_struct ** A = malloc(n * sizeof(mpfr_bandmatrix_struct*));
  for (i = 0 ; i < n ; i++) {
    A[i] = malloc(n * sizeof(mpfr_bandmatrix_t));
    for (j = 0 ; j < n ; j++) {
      mpfr_bandmatrix_init(A[i] + j);
      mpfr_chebpoly_intop_get_bandmatrix(A[i] + j, K->intop[i] + j, N);
    }
  }

  // merge in one bandmatrix
  mpfr_bandmatrix_merge(M, A, n);

  // clear the small matrices
  for (i = 0 ; i < n ; i++) {
    for (j = 0 ; j < n ; j++)
      mpfr_bandmatrix_clear(A[i] + j);
    free(A[i]);
  }

}
  
  



/*
void mpfr_chebpoly_vec_intop_get_bandmatrix(mpfr_bandmatrix_t M, const mpfr_chebpoly_vec_intop_t K, long N)
{
  long n = K->dim;
  long h = mpfr_chebpoly_vec_intop_Hwidth(K);
  long d = mpfr_chebpoly_vec_intop_Dwidth(K);
  long i, j, k, l, hij, dij, lmin, lmax;

  mpfr_bandmatrix_set_params(M, n * (N + 1), h, d);
  mpfr_bandmatrix_zero(M);
  mpfr_bandmatrix_t Mij;
  mpfr_bandmatrix_init(Mij);

  for (i = 0 ; i < n ; i++)
    for (j = 0 ; j < n ; j++) {
      mpfr_chebpoly_intop_get_bandmatrix(Mij, K->intop[i] + j, N);
      hij = Mij->Hwidth;
      dij = Mij->Dwidth;
      // initial coefficients
      for (k = 0 ; k <= N ; k++) {
	lmin = 0;
	lmax = hij <= N ? hij-1 : N;
	for (l = lmin ; l <= lmax ; l++)
	  mpfr_set(M->H[n*l+i] + n*k+j, Mij->H[l] + k, MPFR_RNDN);
      }
      // diagonal coefficients
      for (k = 0 ; k <= N ; k++) {
        lmin = k-dij < 0 ? -k : -dij;
	lmax = k+dij > N ? N-k : dij;
	for (l = lmin ; l <= lmax ; l++)
	  mpfr_set(M->D[n*k+i] + d+n*l+j-i, Mij->D[k] + dij+l, MPFR_RNDN);
      }
    }
  
  mpfr_bandmatrix_normalise(M); 

  mpfr_bandmatrix_clear(Mij);
}

*/
