
#include "mpfr_chebpoly_vec_intop.h"


void mpfr_chebpoly_vec_intop_initvals(mpfr_chebpoly_vec_t G, const mpfr_chebpoly_vec_lode_t L, mpfr_ptr_ptr I)
{
  long n = L->dim;
  mpfr_chebpoly_vec_set_dim(G, n);
  mpfr_chebpoly_vec_zero(G);
  long i, j;
  mpfr_chebpoly_lode_t Lij;
  mpfr_chebpoly_lode_init(Lij);
  mpfr_chebpoly_t F;
  mpfr_chebpoly_init(F);

  for (i = 0 ; i < n ; i++)
    for (j = 0 ; j < n ; j++) {
      mpfr_chebpoly_vec_lode_get_mpfr_chebpoly_lode(Lij, L, i, j);
      mpfr_chebpoly_intop_initvals(F, Lij, I[j]);
      mpfr_chebpoly_add(G->poly + i, G->poly + i, F);
    }
  mpfr_chebpoly_lode_clear(Lij);
  mpfr_chebpoly_clear(F);
}


