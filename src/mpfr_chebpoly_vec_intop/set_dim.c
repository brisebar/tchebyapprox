
#include "mpfr_chebpoly_vec_intop.h"


void mpfr_chebpoly_vec_intop_set_dim(mpfr_chebpoly_vec_intop_t K, long dim)
{
  mpfr_chebpoly_vec_intop_clear(K);
  K->dim = dim;
  long i, j;

  K->intop = malloc(dim * sizeof(mpfr_chebpoly_intop_ptr));
  for (i = 0 ; i < dim ; i++) {
    K->intop[i] = malloc(dim * sizeof(mpfr_chebpoly_intop_t));
    for (j = 0 ; j < dim ; j++)
      mpfr_chebpoly_intop_init(K->intop[i] + j);
  }
}

