
#include "double_chebpoly_vec_lode.h"


void double_chebpoly_vec_lode_set_order_dim(double_chebpoly_vec_lode_t L, long order, long dim)
{
  double_chebpoly_vec_lode_clear(L);
  L->order = order;
  L->dim = dim;

  long i, j, k;
  L->A = malloc(order * sizeof(double_chebpoly_ptr_ptr));
  for (i = 0 ; i < order ; i++) {
    L->A[i] = malloc(dim * sizeof(double_chebpoly_ptr));
    for (j = 0 ; j < dim ; j++) {
      L->A[i][j] = malloc(dim * sizeof(double_chebpoly_t));
      for (k = 0 ; k < dim ; k++)
	double_chebpoly_init(L->A[i][j] + k);
    }
  }
}


