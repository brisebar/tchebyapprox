
#include "double_chebpoly_vec_lode.h"


void double_chebpoly_vec_lode_swap(double_chebpoly_vec_lode_t L, double_chebpoly_vec_lode_t M)
{
  long order_buf = L->order;
  long dim_buf = L->dim;
  double_chebpoly_ptr_ptr_ptr A_buf = L->A;

  L->order = M->order;
  L->dim = M->dim;
  L->A = M->A;

  M->order = order_buf;
  M->dim = dim_buf;
  M->A = A_buf;
}
