
#include "double_chebpoly_vec_lode.h"


void double_chebpoly_vec_lode_set(double_chebpoly_vec_lode_t L, const double_chebpoly_vec_lode_t M)
{
  if (L != M) {
    long r = M->order;
    long n = M->dim;
    double_chebpoly_vec_lode_set_order_dim(L, r, n);

    long i, j, k;
    for (i = 0 ; i < r ; i++)
      for (j = 0 ; j < n ; j++)
	for (k = 0 ; k < n ; k++)
	  double_chebpoly_set(L->A[i][j] + k, M->A[i][j] + k);
  }
}


void double_chebpoly_vec_lode_get_double_chebpoly_lode(double_chebpoly_lode_t Lij, const double_chebpoly_vec_lode_t L, long i, long j)
{
  long r = L->order;
  double_chebpoly_lode_set_order(Lij, r);
  long k;

  for (k = 0 ; k < r ; k++)
    double_chebpoly_set(Lij->a + k, L->A[k][i] + j);
}


