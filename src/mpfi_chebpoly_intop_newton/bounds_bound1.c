
#include "mpfi_chebpoly_intop_newton.h"

// bound1: i from N-s+1 to N


void mpfi_chebpoly_intop_newton_bounds_bound1_d(double_t bound1, const mpfi_chebpoly_intop_t K, const mpfi_bandmatrix_t M_K)
{
  long N = M_K->dim - 1;
  long s = M_K->Dwidth;
  long i;

  mpfi_bandvec_t V;
  mpfi_bandvec_init(V);
  mpfr_t temp;
  mpfr_init(temp);

  double_set_zero(bound1, 1);

  for (i = N - s + 1 ; i <= N ; i++) {

    mpfi_chebpoly_intop_evaluate_Ti(V, K, i);
    _mpfi_vec_1norm_ubound(temp, V->D + s + N + 1 - i, i - N + s);
    
    if (mpfr_cmp_d(temp, *bound1) > 0)
      double_set_fr(bound1, temp, MPFR_RNDU);
  }

  mpfi_bandvec_clear(V);
  mpfr_clear(temp);
}




void mpfi_chebpoly_intop_newton_bounds_bound1_fr(mpfr_t bound1, const mpfi_chebpoly_intop_t K, const mpfi_bandmatrix_t M_K)
{
  long N = M_K->dim - 1;
  long s = M_K->Dwidth;
  long i;

  mpfi_bandvec_t V;
  mpfi_bandvec_init(V);
  mpfr_t temp;
  mpfr_init(temp);

  mpfr_set_zero(bound1, 1);

  for (i = N - s + 1 ; i <= N ; i++) {

    mpfi_chebpoly_intop_evaluate_Ti(V, K, i);
    _mpfi_vec_1norm_ubound(temp, V->D + s + N + 1 - i, i - N + s);
    
    if (mpfr_cmp(temp, bound1) > 0)
      mpfr_set(bound1, temp, MPFR_RNDU);
  }

  mpfi_bandvec_clear(V);
  mpfr_clear(temp);
}




