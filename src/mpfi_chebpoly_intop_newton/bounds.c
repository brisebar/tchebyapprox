
#include "mpfi_chebpoly_intop_newton.h"

// compute bound_i for i = 1,...,6

void mpfi_chebpoly_intop_newton_bounds_d(double_t bound1, double_t bound2, double_t bound3, double_t bound4, double_t bound5, const mpfi_chebpoly_intop_t K, const mpfi_bandmatrix_t M_K, const double_bandmatrix_t M_K_inv)
{ 
  mpfi_chebpoly_intop_newton_bounds_bound1_d(bound1, K, M_K);
  printf("[bound1=%f]\n", *bound1);
  mpfi_chebpoly_intop_newton_bounds_bound2_d(bound2, K, M_K, M_K_inv);
  printf("[bound2=%f]\n", *bound2);
  mpfi_chebpoly_intop_newton_bounds_bound34_d(bound3, bound4, K, M_K, M_K_inv);
  printf("[bound3=%f + bound4=%f => %f]\n", *bound3, *bound4, *bound3+*bound4);
  mpfi_chebpoly_intop_newton_bounds_bound5_d(bound5, M_K, M_K_inv);
  printf("[bound5=%f]\n", *bound5);
}
 

void mpfi_chebpoly_intop_newton_bounds_fr(mpfr_t bound1, mpfr_t bound2, mpfr_t bound3, mpfr_t bound4, mpfr_t bound5, const mpfi_chebpoly_intop_t K, const mpfi_bandmatrix_t M_K, const mpfr_bandmatrix_t M_K_inv)
{ 
  mpfi_chebpoly_intop_newton_bounds_bound1_fr(bound1, K, M_K);
  mpfi_chebpoly_intop_newton_bounds_bound2_fr(bound2, K, M_K, M_K_inv);
  mpfi_chebpoly_intop_newton_bounds_bound34_fr(bound3, bound4, K, M_K, M_K_inv);
  mpfi_chebpoly_intop_newton_bounds_bound5_fr(bound5, M_K, M_K_inv);
}
  
