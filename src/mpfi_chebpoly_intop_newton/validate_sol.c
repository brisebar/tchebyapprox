
#include "mpfi_chebpoly_intop_newton.h"


void mpfi_chebpoly_intop_newton_validate_sol_d(mpfr_t bound, const mpfi_chebpoly_intop_t K, const mpfi_chebpoly_t G, const mpfr_chebpoly_t P, long init_N)
{
  double_t T_norm;
  double_init(T_norm);
  double_bandmatrix_t M_K_inv;
  double_bandmatrix_init(M_K_inv);

  // get a contracting operator
  mpfi_chebpoly_intop_newton_contract_d(T_norm, M_K_inv, K, init_N);

  // obtain upper bound
  mpfi_chebpoly_intop_newton_validate_sol_aux_d(bound, K, T_norm, M_K_inv, G, P);

  // clear variables
  double_clear(T_norm);
  double_bandmatrix_clear(M_K_inv);
}



void mpfi_chebpoly_intop_newton_validate_sol_fr(mpfr_t bound, const mpfi_chebpoly_intop_t K, const mpfi_chebpoly_t G, const mpfr_chebpoly_t P, long init_N)
{
  mpfr_t T_norm;
  mpfr_init(T_norm);
  mpfr_bandmatrix_t M_K_inv;
  mpfr_bandmatrix_init(M_K_inv);

  // get a contracting operator
  mpfi_chebpoly_intop_newton_contract_fr(T_norm, M_K_inv, K, init_N);

  // obtain upper bound
  mpfi_chebpoly_intop_newton_validate_sol_aux_fr(bound, K, T_norm, M_K_inv, G, P);

  // clear variables
  mpfr_clear(T_norm);
  mpfr_bandmatrix_clear(M_K_inv);
}


void mpfi_chebpoly_lode_intop_newton_validate_sol_d(mpfr_t bound, const mpfi_chebpoly_lode_t L, const mpfi_chebpoly_t g, mpfi_srcptr I, const mpfr_chebpoly_t P)
{
  mpfi_chebpoly_intop_t K;
  mpfi_chebpoly_intop_init(K);
  mpfi_chebpoly_intop_set_lode(K, L);

  mpfi_chebpoly_t G;
  mpfi_chebpoly_init(G);
  mpfi_chebpoly_intop_rhs(G, L, g, I);

  long init_N = 2 * mpfi_chebpoly_intop_Dwidth(K);

  mpfi_chebpoly_intop_newton_validate_sol_d(bound, K, G, P, init_N);
}




void mpfi_chebpoly_lode_intop_newton_validate_sol_fr(mpfr_t bound, const mpfi_chebpoly_lode_t L, const mpfi_chebpoly_t g, mpfi_srcptr I, const mpfr_chebpoly_t P)
{
  mpfi_chebpoly_intop_t K;
  mpfi_chebpoly_intop_init(K);
  mpfi_chebpoly_intop_set_lode(K, L);

  mpfi_chebpoly_t G;
  mpfi_chebpoly_init(G);
  mpfi_chebpoly_intop_rhs(G, L, g, I);

  long init_N = 2 * mpfi_chebpoly_intop_Dwidth(K);

  mpfi_chebpoly_intop_newton_validate_sol_fr(bound, K, G, P, init_N);

  mpfi_chebpoly_intop_clear(K);
  mpfi_chebpoly_clear(G);
}
