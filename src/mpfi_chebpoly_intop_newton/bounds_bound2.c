
#include "mpfi_chebpoly_intop_newton.h"

// bound2: i from N+1 to N+s


void mpfi_chebpoly_intop_newton_bounds_bound2_d(double_t bound2, const mpfi_chebpoly_intop_t K, const mpfi_bandmatrix_t M_K, const double_bandmatrix_t M_K_inv)
{
  long N = M_K->dim - 1;
  long s = M_K->Dwidth;
  long i;

  mpfi_bandvec_t V;
  mpfi_bandvec_init(V);
  mpfr_t temp, temp2;
  mpfr_inits(temp, temp2, NULL);

  double_set_zero(bound2, 1);

  for (i = N + 1 ; i <= N + s ; i++) {
   
    mpfi_chebpoly_intop_evaluate_Ti(V, K, i);
    _mpfi_vec_1norm_ubound(temp, V->D + s + N + 1 - i, s + i - N);
    _mpfi_vec_zero(V->D + s + N + 1 - i, s + i - N);
  
    double_bandmatrix_evaluate_band_fi(V, M_K_inv, V);
    mpfi_bandvec_1norm_ubound(temp2, V);

    mpfr_add(temp, temp, temp2, MPFR_RNDU);
    if (mpfr_cmp_d(temp, *bound2) > 0)
      double_set_fr(bound2, temp, MPFR_RNDU);
  
  }

  mpfi_bandvec_clear(V);
  mpfr_clears(temp, temp2, NULL);
}

void mpfi_chebpoly_intop_newton_bounds_bound2_fr(mpfr_t bound2, const mpfi_chebpoly_intop_t K, const mpfi_bandmatrix_t M_K, const mpfr_bandmatrix_t M_K_inv)
{
  long N = M_K->dim - 1;
  long s = M_K->Dwidth;
  long i;

  mpfi_bandvec_t V;
  mpfi_bandvec_init(V);
  mpfr_t temp, temp2;
  mpfr_inits(temp, temp2, NULL);

  mpfr_set_zero(bound2, 1);

  for (i = N + 1 ; i <= N + s ; i++) {
   
    mpfi_chebpoly_intop_evaluate_Ti(V, K, i);
    _mpfi_vec_1norm_ubound(temp, V->D + s + N + 1 - i, s + i - N);
    _mpfi_vec_zero(V->D + s + N + 1 - i, s + i - N);
  
    mpfr_bandmatrix_evaluate_band_fi(V, M_K_inv, V);
    mpfi_bandvec_1norm_ubound(temp2, V);

    mpfr_add(temp, temp, temp2, MPFR_RNDU);
    if (mpfr_cmp(temp, bound2) > 0)
      mpfr_set(bound2, temp, MPFR_RNDU);
  
  }

  mpfi_bandvec_clear(V);
  mpfr_clears(temp, temp2, NULL);
}
