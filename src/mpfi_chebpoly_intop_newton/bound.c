
#include "mpfi_chebpoly_intop_newton.h"

// merge all bound_i to get the final bound

int mpfi_chebpoly_intop_newton_bound_d(double_t bound, const mpfi_chebpoly_intop_t K, const mpfi_bandmatrix_t M_K, const double_bandmatrix_t M_K_inv)
{
  int return_flag = 0;

  double_t bound1, bound2, bound3, bound4, bound5;
  double_init(bound1);
  double_init(bound2);
  double_init(bound3);
  double_init(bound4);
  double_init(bound5);
  
  mpfr_t bound_fr;
  mpfr_init(bound_fr);


  // first just compute bounds

  mpfi_chebpoly_intop_newton_bounds_bound1_d(bound1, K, M_K);
  printf("[bound1=%f]\n", *bound1);
  mpfi_chebpoly_intop_newton_bounds_bound2_d(bound2, K, M_K, M_K_inv);
  printf("[bound2=%f]\n", *bound2);
  mpfi_chebpoly_intop_newton_bounds_bound34_d(bound3, bound4, K, M_K, M_K_inv);
  printf("[bound3=%f + bound4=%f => %f]\n", *bound3, *bound4, *bound3+*bound4);
  mpfr_set_d(bound_fr, *bound3, MPFR_RNDU);
  mpfr_add_d(bound_fr, bound_fr, *bound4, MPFR_RNDU);
  double_set_fr(bound, bound_fr, MPFR_RNDU);
  if (double_cmp(bound1, bound) > 0)
    double_set(bound, bound1, MPFR_RNDU);
  if (double_cmp(bound2, bound) > 0)
    double_set(bound, bound2, MPFR_RNDU);

  if (*bound > 0.4)
    return_flag = 1;

  else {

    // bound5: approx error of the inverse

    mpfi_chebpoly_intop_newton_bounds_bound5_d(bound5, M_K, M_K_inv);
    printf("[bound5=%f]\n", *bound5);

    mpfr_set_d(bound_fr, *bound, MPFR_RNDU);
    mpfr_add_d(bound_fr, bound_fr, *bound5, MPFR_RNDU);
    double_set_fr(bound, bound_fr, MPFR_RNDU);

    if (*bound5 > 0.1)
      return_flag = 2;


  }

  double_clear(bound1);
  double_clear(bound2);
  double_clear(bound3);
  double_clear(bound4);
  double_clear(bound5);
  mpfr_clear(bound_fr);

  return return_flag;
}


int mpfi_chebpoly_intop_newton_bound_fr(mpfr_t bound, const mpfi_chebpoly_intop_t K, const mpfi_bandmatrix_t M_K, const mpfr_bandmatrix_t M_K_inv)
{
  int return_flag = 0;

  mpfr_t bound1, bound2, bound3, bound4, bound5;
  mpfr_inits(bound1, bound2, bound3, bound4, bound5, NULL);

  // first just compute bounds

  mpfi_chebpoly_intop_newton_bounds_bound1_fr(bound1, K, M_K);
  mpfi_chebpoly_intop_newton_bounds_bound2_fr(bound2, K, M_K, M_K_inv);
  mpfi_chebpoly_intop_newton_bounds_bound34_fr(bound3, bound4, K, M_K, M_K_inv);

  mpfr_add(bound, bound3, bound4, MPFR_RNDU);
  if (mpfr_cmp(bound1, bound) > 0)
    mpfr_set(bound, bound1, MPFR_RNDU);
  if (mpfr_cmp(bound2, bound) > 0)
    mpfr_set(bound, bound2, MPFR_RNDU);

  if (mpfr_cmp_d(bound, 0.4) > 0)
    return_flag = 1;

  else {

    // bound5: approx error of the inverse

    mpfi_chebpoly_intop_newton_bounds_bound5_fr(bound5, M_K, M_K_inv);

    mpfr_add(bound, bound, bound5, MPFR_RNDU);

    if (mpfr_cmp_d(bound5, 0.1) > 0)
      return_flag = 2;


  }

  mpfr_clears(bound1, bound2, bound3, bound4, bound5, NULL);

  return return_flag;
}

