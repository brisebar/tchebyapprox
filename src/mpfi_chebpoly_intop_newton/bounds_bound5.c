
#include "mpfi_chebpoly_intop_newton.h"


// bound5: numerical error = ||Id - M_K_inv*M_K||


void mpfi_chebpoly_intop_newton_bounds_bound5_d(double_t bound5, const mpfi_bandmatrix_t M_K, const double_bandmatrix_t M_K_inv)
{
  mpfi_bandmatrix_t M_K_inv_fi, A;
  mpfi_bandmatrix_init(M_K_inv_fi);
  mpfi_bandmatrix_set_double_bandmatrix(M_K_inv_fi, M_K_inv);
  mpfi_bandmatrix_init(A);
  mpfr_t bound5_fr;
  mpfr_init(bound5_fr);
  
  mpfi_bandmatrix_mul(A, M_K_inv_fi, M_K);

  long N = M_K->dim - 1;
  long i;
  for (i = 0 ; i <= N ; i++)
    mpfi_sub_si(A->D[i] + A->Dwidth, A->D[i] + A->Dwidth, 1);

  mpfi_bandmatrix_1norm_ubound(bound5_fr, A);
  double_set_fr(bound5, bound5_fr, MPFR_RNDU);

  mpfr_clear(bound5_fr);
  mpfi_bandmatrix_clear(M_K_inv_fi);
  mpfi_bandmatrix_clear(A);
}



void mpfi_chebpoly_intop_newton_bounds_bound5_fr(mpfr_t bound5, const mpfi_bandmatrix_t M_K, const mpfr_bandmatrix_t M_K_inv)
{
  mpfi_bandmatrix_t M_K_inv_fi, A;
  mpfi_bandmatrix_init(M_K_inv_fi);
  mpfi_bandmatrix_set_mpfr_bandmatrix(M_K_inv_fi, M_K_inv);
  mpfi_bandmatrix_init(A);
    
  mpfi_bandmatrix_mul(A, M_K_inv_fi, M_K);
  long N = M_K->dim - 1;
  long i;
  for (i = 0 ; i <= N ; i++)
    mpfi_sub_si(A->D[i] + A->Dwidth, A->D[i] + A->Dwidth, 1);
  mpfi_bandmatrix_1norm_ubound(bound5, A);
  mpfi_bandmatrix_clear(M_K_inv_fi);
  mpfi_bandmatrix_clear(A);
}


