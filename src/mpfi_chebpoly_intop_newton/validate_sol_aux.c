
#include "mpfi_chebpoly_intop_newton.h"


void mpfi_chebpoly_intop_newton_validate_sol_aux_d(mpfr_t bound, const mpfi_chebpoly_intop_t K, const double_t T_norm, const double_bandmatrix_t M_K_inv, const mpfi_chebpoly_t G, const mpfr_chebpoly_t P)
{
  mpfr_t temp;
  mpfr_init(temp);
  long N = M_K_inv->dim - 1;

  // compute P-T(P)
  mpfi_chebpoly_t P_fi;
  mpfi_chebpoly_init(P_fi);
  mpfi_chebpoly_set_mpfr_chebpoly(P_fi, P);

  mpfi_chebpoly_t T_P;
  mpfi_chebpoly_init(T_P);
  mpfi_chebpoly_intop_evaluate_fi(T_P, K, P_fi);
  mpfi_chebpoly_add(T_P, T_P, G);
  mpfi_chebpoly_sub(T_P, P_fi, T_P);
  if (T_P->degree < N)
    mpfi_chebpoly_set_degree(T_P, N);
  _double_bandmatrix_evaluate_fi(T_P->coeffs, M_K_inv, T_P->coeffs);

  // validated bound
  mpfi_chebpoly_1norm_ubound(bound, T_P);
  mpfr_set_d(temp, *T_norm, MPFR_RNDU);
  mpfr_si_sub(temp, 1, temp, MPFR_RNDD);
  mpfr_div(bound, bound, temp, MPFR_RNDU);

  // clear variables
  mpfi_chebpoly_clear(T_P);
  mpfi_chebpoly_clear(P_fi);
  mpfr_clear(temp);
}


void mpfi_chebpoly_intop_newton_validate_sol_aux_fr(mpfr_t bound, const mpfi_chebpoly_intop_t K, const mpfr_t T_norm, const mpfr_bandmatrix_t M_K_inv, const mpfi_chebpoly_t G, const mpfr_chebpoly_t P)
{
  mpfr_t temp;
  mpfr_init(temp);
  long N = M_K_inv->dim - 1;

  // compute P-T(P)
  mpfi_chebpoly_t P_fi;
  mpfi_chebpoly_init(P_fi);
  mpfi_chebpoly_set_mpfr_chebpoly(P_fi, P);

  mpfi_chebpoly_t T_P;
  mpfi_chebpoly_init(T_P);
  mpfi_chebpoly_intop_evaluate_fi(T_P, K, P_fi);
  mpfi_chebpoly_add(T_P, T_P, G);
  mpfi_chebpoly_sub(T_P, P_fi, T_P);
  if (T_P->degree < N)
    mpfi_chebpoly_set_degree(T_P, N);
  _mpfr_bandmatrix_evaluate_fi(T_P->coeffs, M_K_inv, T_P->coeffs);

  // validated bound
  mpfi_chebpoly_1norm_ubound(bound, T_P); // printf("[[~~ %.10e]]\n", mpfr_get_d(bound, MPFR_RNDU));
  mpfr_si_sub(temp, 1, T_norm, MPFR_RNDD);
  mpfr_div(bound, bound, temp, MPFR_RNDU);

  // clear variables
  mpfi_chebpoly_clear(T_P);
  mpfi_chebpoly_clear(P_fi);
  mpfr_clear(temp);
}
