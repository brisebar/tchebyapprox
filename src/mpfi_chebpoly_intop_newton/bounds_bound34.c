
#include "mpfi_chebpoly_intop_newton.h"


// bound3 (diagonal coefficients) and bound4 (N_F^-1 on initial coefficient): i > N+s


void mpfi_chebpoly_intop_newton_bounds_bound34_d(double_t bound3, double_t bound4, const mpfi_chebpoly_intop_t K, const mpfi_bandmatrix_t M_K, const double_bandmatrix_t M_K_inv)
{
  long N = M_K->dim - 1;
  long r = K->order;
  long h = M_K->Hwidth;
  long s = M_K->Dwidth;
  long j, k, l;

  mpfi_t t, u;
  mpfi_init(t);
  mpfi_init(u);

  // interval where the index i is living = [N+s+1, +inf[
  mpfi_t i;
  mpfi_init(i);
  mpfr_set_si(&(i->left), N + s + 1, MPFR_RNDN);
  mpfr_set_inf(&(i->right), 1);

  // first order difference method to compute bound3 and bound4
  mpfr_t bound3_fr, bound4_fr, bound3_alt, bound4_alt, bound3_alt_diff, bound4_alt_diff, bound3_alt_diff_j, bound4_alt_diff_j, temp_fr;
  mpfr_inits(bound3_fr, bound4_fr, bound3_alt, bound4_alt, bound3_alt_diff, bound4_alt_diff, bound3_alt_diff_j, bound4_alt_diff_j, temp_fr, NULL);

  mpfr_set_zero(bound3_alt_diff, 1);
  mpfr_set_zero(bound4_alt_diff, 1);

  mpfi_bandvec_t V, W;
  mpfi_bandvec_init(V);
  mpfi_bandvec_init(W);
  mpfi_bandvec_set_params(V, h, s, N + s + 1);
  mpfi_bandvec_set_params(W, h, s, N + s + 1);

  for (j = 0 ; j < r ; j++) {

    // int_-1^x T_j*T_i = 1/4*( T_{i+j+1}/(i+j+1) - 
    // T_{i+j-1}/(i+j-1) + T_{i-j+1}/(i-j+1) - T_{i-j-1}/(i-j-1) )
    // + (-1)^{i+j}/4 * (-2/((i+j)^2-1) - 2/((i-j)^2-1))

    long d = K->alpha[j].degree;

    long val_k[4];
    val_k[0] = j + 1;
    val_k[1] = j - 1;
    val_k[2] = -j + 1;
    val_k[3] = -j - 1;

    mpfr_set_zero(bound3_alt_diff_j, 1);

    // compute in V->D the sum:
    // 1/4 * ( T_{i+j+1}/(i+j+1) - T_{i+j-1}/(i+j-1) + T_{i-j+1}/(i-j+1) - T_{i-j-1}/(i-j-1) ) * alpha_j
    // and treatment of bound3_alt_diff

    for (k = 0 ; k < 4 ; k++) {
      mpfi_add_si(t, i, val_k[k]);
      mpfi_si_div(t, 1, t);
      mpfi_mul_2si(t, t, -3);
      if (k % 2)
	mpfi_neg(t, t);

      for (l = 0 ; l <= d ; l++) {
	mpfi_mul(u, K->alpha[j].coeffs + l, t);
	mpfi_add(V->D + s + val_k[k] + l, V->D + s + val_k[k] + l, u);
        mpfi_add(V->D + s + val_k[k] - l, V->D + s + val_k[k] - l, u);
      }

      // focus on bound3_alt_diff_j
      mpfi_si_div(t, val_k[k], i);
      mpfi_add_si(t, t, 1);
      mpfi_si_div(t, 1, t);
      mpfi_sub(t, t, t);
      mpfi_mag(temp_fr, t);
      mpfr_add(bound3_alt_diff_j, bound3_alt_diff_j, temp_fr, MPFR_RNDU);
    }

    mpfr_div(bound3_alt_diff_j, bound3_alt_diff_j, &(i->left), MPFR_RNDU);
    mpfr_mul_2si(bound3_alt_diff_j, bound3_alt_diff_j, -2, MPFR_RNDU);
    mpfi_chebpoly_1norm_ubound(temp_fr, K->alpha + j);
    mpfr_mul(bound3_alt_diff_j, bound3_alt_diff_j, temp_fr, MPFR_RNDU);
    mpfr_add(bound3_alt_diff, bound3_alt_diff, bound3_alt_diff_j, MPFR_RNDU);


    // add integration constant (t) * alpha_j into V->H
    // t = 1/2 * (1/((i+j)^2-1) + 1/((i-j)^2-1))
    mpfi_add_si(u, i, j);
    mpfi_sqr(u, u);
    mpfi_sub_si(u, u, 1);
    mpfi_si_div(u, 1, u);
    mpfi_sub_si(t, i, j);
    mpfi_sqr(t, t);
    mpfi_sub_si(t, t, 1);
    mpfi_si_div(t, 1, t);
    mpfi_add(t, u, t);
    mpfi_mul_2si(t, t, -1);

    for (k = 0 ; k <= d ; k++) {
      mpfi_mul(u, K->alpha[j].coeffs + k, t);
      mpfi_add(V->H + k, V->H + k, u);
    }
    
    // focus on bound4_alt_diff_j
    // first part
    k = j + 1 < 0 ? -j - 1 : j + 1;
    mpfi_si_div(t, k, i);
    mpfi_sqr(t, t);
    mpfi_si_sub(t, 1, t);
    mpfi_si_div(t, 1, t);
    mpfi_sub(t, t, t);
    mpfi_mag(bound4_alt_diff_j, t);
    mpfr_mul_si(bound4_alt_diff_j, bound4_alt_diff_j, k, MPFR_RNDU);
    // second part
    k = j - 1 < 0 ? -j + 1 : j - 1;
    mpfi_si_div(t, k, i);
    mpfi_sqr(t, t);
    mpfi_si_sub(t, 1, t);
    mpfi_si_div(t, 1, t);
    mpfi_sub(t, t, t);
    mpfi_mag(temp_fr, t);
    mpfr_mul_si(temp_fr, temp_fr, k, MPFR_RNDU);
    // together
    mpfr_add(bound4_alt_diff_j, bound4_alt_diff_j, temp_fr, MPFR_RNDU);
    mpfr_mul(temp_fr, &(i->left), &(i->left), MPFR_RNDD);
    mpfr_div(bound4_alt_diff_j, bound4_alt_diff_j, temp_fr, MPFR_RNDU);
    mpfr_mul_2si(bound4_alt_diff_j, bound4_alt_diff_j, -1, MPFR_RNDU);
    // norm ||M_K_inv * alpha_j||
    mpfi_bandvec_zero(W);
    for (k = 0 ; k <= d ; k++)
      mpfi_set(W->H + k, K->alpha[j].coeffs + k);
    double_bandmatrix_evaluate_band_fi(W, M_K_inv, W);
    _mpfi_vec_1norm_ubound(temp_fr, W->H, h);
    // correct value of bound4_alt_diff_j
    mpfr_mul(bound4_alt_diff_j, bound4_alt_diff_j, temp_fr, MPFR_RNDU);

    // update bound4_alt_diff
    mpfr_add(bound4_alt_diff, bound4_alt_diff, bound4_alt_diff_j, MPFR_RNDU);

  }

  mpfr_t control_bound;
  mpfr_init(control_bound);

  mpfi_chebpoly_intop_evaluate_Ti(W, K, N + s + 1);

  // compute bound3
  _mpfi_vec_1norm_ubound(bound3_fr, V->D, 2 * s + 1);

  // compute bound3_alt and compare with bound3
  _mpfi_vec_1norm_ubound(bound3_alt, W->D, 2 * s + 1); mpfr_set(control_bound, bound3_alt, MPFR_RNDU);
  mpfr_add(bound3_alt, bound3_alt, bound3_alt_diff, MPFR_RNDU);
  if (mpfr_cmp(bound3_alt, bound3_fr) < 0) {
    mpfr_set(bound3_fr, bound3_alt, MPFR_RNDU);
  }

  // compute bound4
  double_bandmatrix_evaluate_band_fi(V, M_K_inv, V);
  _mpfi_vec_1norm_ubound(bound4_fr, V->H, h);

  // compute bound4_alt
  double_bandmatrix_evaluate_band_fi(W, M_K_inv, W);
  _mpfi_vec_1norm_ubound(bound4_alt, W->H, h); mpfr_add(control_bound, control_bound, bound4_alt, MPFR_RNDU);
  mpfr_add(bound4_alt, bound4_alt, bound4_alt_diff, MPFR_RNDU);
  if (mpfr_cmp(bound4_alt, bound4_fr) < 0) {
    mpfr_set(bound4_fr, bound4_alt, MPFR_RNDU);
  }

  // assign bound3 and bound4
  double_set_fr(bound3, bound3_fr, MPFR_RNDU);
  double_set_fr(bound4, bound4_fr, MPFR_RNDU);

  // clear variables
  mpfi_clear(t);
  mpfi_clear(u);
  mpfi_clear(i);
  mpfi_bandvec_clear(V);
  mpfi_bandvec_clear(W);
  mpfr_clears(bound3_fr, bound4_fr, bound3_alt, bound4_alt, bound3_alt_diff, bound4_alt_diff, bound3_alt_diff_j, bound4_alt_diff_j, temp_fr, NULL);
}



void mpfi_chebpoly_intop_newton_bounds_bound34_fr(mpfr_t bound3, mpfr_t bound4, const mpfi_chebpoly_intop_t K, const mpfi_bandmatrix_t M_K, const mpfr_bandmatrix_t M_K_inv)
{
  long N = M_K->dim - 1;
  long r = K->order;
  long h = M_K->Hwidth;
  long s = M_K->Dwidth;
  long j, k, l;

  mpfi_t t, u;
  mpfi_init(t);
  mpfi_init(u);

  // interval where the index i is living = [N+s+1, +inf[
  mpfi_t i;
  mpfi_init(i);
  mpfr_set_si(&(i->left), N + s + 1, MPFR_RNDN);
  mpfr_set_inf(&(i->right), 1);

  // first order difference method to compute bound3 and bound4
  mpfr_t bound3_alt, bound4_alt, bound3_alt_diff, bound4_alt_diff, bound3_alt_diff_j, bound4_alt_diff_j, temp_fr;
  mpfr_inits(bound3_alt, bound4_alt, bound3_alt_diff, bound4_alt_diff, bound3_alt_diff_j, bound4_alt_diff_j, temp_fr, NULL);

  mpfr_set_zero(bound3_alt_diff, 1);
  mpfr_set_zero(bound4_alt_diff, 1);

  mpfi_bandvec_t V, W;
  mpfi_bandvec_init(V);
  mpfi_bandvec_init(W);
  mpfi_bandvec_set_params(V, h, s, N + s + 1);
  mpfi_bandvec_set_params(W, h, s, N + s + 1);

  for (j = 0 ; j < r ; j++) {

    // int_-1^x T_j*T_i = 1/4*( T_{i+j+1}/(i+j+1) - 
    // T_{i+j-1}/(i+j-1) + T_{i-j+1}/(i-j+1) - T_{i-j-1}/(i-j-1) )
    // + (-1)^{i+j}/4 * (-2/((i+j)^2-1) - 2/((i-j)^2-1))

    long d = K->alpha[j].degree;

    long val_k[4];
    val_k[0] = j + 1;
    val_k[1] = j - 1;
    val_k[2] = -j + 1;
    val_k[3] = -j - 1;

    mpfr_set_zero(bound3_alt_diff_j, 1);

    // compute in V->D the sum:
    // 1/4 * ( T_{i+j+1}/(i+j+1) - T_{i+j-1}/(i+j-1) + T_{i-j+1}/(i-j+1) - T_{i-j-1}/(i-j-1) ) * alpha_j
    // and treatment of bound3_alt_diff

    for (k = 0 ; k < 4 ; k++) {
      mpfi_add_si(t, i, val_k[k]);
      mpfi_si_div(t, 1, t);
      mpfi_mul_2si(t, t, -3);
      if (k % 2)
	mpfi_neg(t, t);

      for (l = 0 ; l <= d ; l++) {
	mpfi_mul(u, K->alpha[j].coeffs + l, t);
	mpfi_add(V->D + s + val_k[k] + l, V->D + s + val_k[k] + l, u);
        mpfi_add(V->D + s + val_k[k] - l, V->D + s + val_k[k] - l, u);
      }

      // focus on bound3_alt_diff_j
      mpfi_si_div(t, val_k[k], i);
      mpfi_add_si(t, t, 1);
      mpfi_si_div(t, 1, t);
      mpfi_sub(t, t, t);
      mpfi_mag(temp_fr, t);
      mpfr_add(bound3_alt_diff_j, bound3_alt_diff_j, temp_fr, MPFR_RNDU);
    }

    mpfr_div(bound3_alt_diff_j, bound3_alt_diff_j, &(i->left), MPFR_RNDU);
    mpfr_mul_2si(bound3_alt_diff_j, bound3_alt_diff_j, -2, MPFR_RNDU);
    mpfi_chebpoly_1norm_ubound(temp_fr, K->alpha + j);
    mpfr_mul(bound3_alt_diff_j, bound3_alt_diff_j, temp_fr, MPFR_RNDU);
    mpfr_add(bound3_alt_diff, bound3_alt_diff, bound3_alt_diff_j, MPFR_RNDU);


    // add integration constant (t) * alpha_j into V->H
    // t = 1/2 * (1/((i+j)^2-1) + 1/((i-j)^2-1))
    mpfi_add_si(u, i, j);
    mpfi_sqr(u, u);
    mpfi_sub_si(u, u, 1);
    mpfi_si_div(u, 1, u);
    mpfi_sub_si(t, i, j);
    mpfi_sqr(t, t);
    mpfi_sub_si(t, t, 1);
    mpfi_si_div(t, 1, t);
    mpfi_add(t, u, t);
    mpfi_mul_2si(t, t, -1);

    for (k = 0 ; k <= d ; k++) {
      mpfi_mul(u, K->alpha[j].coeffs + k, t);
      mpfi_add(V->H + k, V->H + k, u);
    }
    
    // focus on bound4_alt_diff_j
    // first part
    k = j + 1 < 0 ? -j - 1 : j + 1;
    mpfi_si_div(t, k, i);
    mpfi_sqr(t, t);
    mpfi_si_sub(t, 1, t);
    mpfi_si_div(t, 1, t);
    mpfi_sub(t, t, t);
    mpfi_mag(bound4_alt_diff_j, t);
    mpfr_mul_si(bound4_alt_diff_j, bound4_alt_diff_j, k, MPFR_RNDU);
    // second part
    k = j - 1 < 0 ? -j + 1 : j - 1;
    mpfi_si_div(t, k, i);
    mpfi_sqr(t, t);
    mpfi_si_sub(t, 1, t);
    mpfi_si_div(t, 1, t);
    mpfi_sub(t, t, t);
    mpfi_mag(temp_fr, t);
    mpfr_mul_si(temp_fr, temp_fr, k, MPFR_RNDU);
    // together
    mpfr_add(bound4_alt_diff_j, bound4_alt_diff_j, temp_fr, MPFR_RNDU);
    mpfr_mul(temp_fr, &(i->left), &(i->left), MPFR_RNDD);
    mpfr_div(bound4_alt_diff_j, bound4_alt_diff_j, temp_fr, MPFR_RNDU);
    mpfr_mul_2si(bound4_alt_diff_j, bound4_alt_diff_j, -1, MPFR_RNDU);
    // norm ||M_K_inv * alpha_j||
    mpfi_bandvec_zero(W);
    for (k = 0 ; k <= d ; k++)
      mpfi_set(W->H + k, K->alpha[j].coeffs + k);
    mpfr_bandmatrix_evaluate_band_fi(W, M_K_inv, W);
    _mpfi_vec_1norm_ubound(temp_fr, W->H, h);
    // correct value of bound4_alt_diff_j
    mpfr_mul(bound4_alt_diff_j, bound4_alt_diff_j, temp_fr, MPFR_RNDU);

    // update bound4_alt_diff
    mpfr_add(bound4_alt_diff, bound4_alt_diff, bound4_alt_diff_j, MPFR_RNDU);

  }

  mpfr_t control_bound;
  mpfr_init(control_bound);

  mpfi_chebpoly_intop_evaluate_Ti(W, K, N + s + 1);

  // compute bound3
  _mpfi_vec_1norm_ubound(bound3, V->D, 2 * s + 1);

  // compute bound3_alt and compare with bound3
  _mpfi_vec_1norm_ubound(bound3_alt, W->D, 2 * s + 1); mpfr_set(control_bound, bound3_alt, MPFR_RNDU);
  mpfr_add(bound3_alt, bound3_alt, bound3_alt_diff, MPFR_RNDU);
  if (mpfr_cmp(bound3_alt, bound3) < 0) {
    mpfr_set(bound3, bound3_alt, MPFR_RNDU);
  }

  // compute bound4
  mpfr_bandmatrix_evaluate_band_fi(V, M_K_inv, V);
  _mpfi_vec_1norm_ubound(bound4, V->H, h);

  // compute bound4_alt
  mpfr_bandmatrix_evaluate_band_fi(W, M_K_inv, W);
  _mpfi_vec_1norm_ubound(bound4_alt, W->H, h); mpfr_add(control_bound, control_bound, bound4_alt, MPFR_RNDU);
  mpfr_add(bound4_alt, bound4_alt, bound4_alt_diff, MPFR_RNDU);
  if (mpfr_cmp(bound4_alt, bound4) < 0) {
    mpfr_set(bound4, bound4_alt, MPFR_RNDU);
  }

  // clear variables
  mpfi_clear(t);
  mpfi_clear(u);
  mpfi_clear(i);
  mpfi_bandvec_clear(V);
  mpfi_bandvec_clear(W);
  mpfr_clears(bound3_alt, bound4_alt, bound3_alt_diff, bound4_alt_diff, bound3_alt_diff_j, bound4_alt_diff_j, temp_fr, NULL);
}


