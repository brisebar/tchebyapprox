
#include "mpfr_chebpoly_intop.h"


void mpfr_chebpoly_intop_initvals(mpfr_chebpoly_t G, const mpfr_chebpoly_lode_t L, mpfr_srcptr I)
{
  long r = L->order;
  long i;

  mpfr_chebpoly_t P, Q;
  mpfr_chebpoly_init(P);
  mpfr_chebpoly_init(Q);

  mpfr_chebpoly_zero(G);

  for (i = r - 1 ; i >= 0 ; i--) {
    
    // update P
    mpfr_chebpoly_antiderivative_1(P, P);
    mpfr_chebpoly_set_degree(Q, 0);
    mpfr_chebpoly_set_coeff_fr(Q, 0, I + i);
    mpfr_chebpoly_add(P, P, Q);

    // add L->a + i * P to G
    mpfr_chebpoly_mul(Q, L->a + i, P);
    mpfr_chebpoly_sub(G, G, Q);

  }

  // clear variables
  mpfr_chebpoly_clear(P);
  mpfr_chebpoly_clear(Q);
}

