
#include "mpfr_chebpoly_intop.h"


void mpfr_chebpoly_intop_evaluate_fr(mpfr_chebpoly_t P, const mpfr_chebpoly_intop_t K, const mpfr_chebpoly_t Q)
{
  long r = K->order;
  long i;

  mpfr_chebpoly_t K_Q, K_Q_i;
  mpfr_chebpoly_init(K_Q);
  mpfr_chebpoly_init(K_Q_i);

  for (i = 0 ; i < r ; i++) {
    
    mpfr_chebpoly_zero(K_Q_i);
    mpfr_chebpoly_set_coeff_si(K_Q_i, i, 1);
    mpfr_chebpoly_mul(K_Q_i, K_Q_i, Q);

    mpfr_chebpoly_antiderivative_1(K_Q_i, K_Q_i);

    mpfr_chebpoly_mul(K_Q_i, K->alpha + i, K_Q_i);

    mpfr_chebpoly_add(K_Q, K_Q, K_Q_i);

  }

  // clear variables
  mpfr_chebpoly_clear(K_Q_i);
  mpfr_chebpoly_swap(P, K_Q);
  mpfr_chebpoly_clear(K_Q);
}


void mpfr_chebpoly_intop_evaluate_fi(mpfi_chebpoly_t P, const mpfr_chebpoly_intop_t K, const mpfi_chebpoly_t Q)
{
  long r = K->order;
  long i;

  mpfi_chebpoly_t K_Q, K_Q_i, Temp;
  mpfi_chebpoly_init(K_Q);
  mpfi_chebpoly_init(K_Q_i);
  mpfi_chebpoly_init(Temp);

  for (i = 0 ; i < r ; i++) {
    
    mpfi_chebpoly_zero(K_Q_i);
    mpfi_chebpoly_set_coeff_si(K_Q_i, i, 1);
    mpfi_chebpoly_mul(K_Q_i, K_Q_i, Q);

    mpfi_chebpoly_antiderivative_1(K_Q_i, K_Q_i);

    mpfi_chebpoly_set_mpfr_chebpoly(Temp, K->alpha + i);
    mpfi_chebpoly_mul(K_Q_i, Temp, K_Q_i);

    mpfi_chebpoly_add(K_Q, K_Q, K_Q_i);

  }

  // clear variables
  mpfi_chebpoly_clear(K_Q_i);
  mpfi_chebpoly_swap(P, K_Q);
  mpfi_chebpoly_clear(K_Q);
  mpfi_chebpoly_clear(Temp);
}

