
#include "mpfr_chebpoly_intop.h"


void mpfr_chebpoly_intop_set(mpfr_chebpoly_intop_t K, const mpfr_chebpoly_intop_t N)
{
  long r = N->order;
  mpfr_chebpoly_intop_set_order(K, r);

  long i;
  for (i = 0 ; i < r ; i++)
    mpfr_chebpoly_set(K->alpha + i, N->alpha + i);
}


void mpfr_chebpoly_intop_set_double_chebpoly_intop(mpfr_chebpoly_intop_t K, const double_chebpoly_intop_t N)
{
  long r = N->order;
  mpfr_chebpoly_intop_set_order(K, r);

  long i;
  for (i = 0 ; i < r ; i++)
    mpfr_chebpoly_set_double_chebpoly(K->alpha + i, N->alpha + i);
}


void mpfr_chebpoly_intop_get_double_chebpoly_intop(double_chebpoly_intop_t K, const mpfr_chebpoly_intop_t N)
{
  long r = N->order;
  double_chebpoly_intop_set_order(K, r);

  long i;
  for (i = 0 ; i < r ; i++)
    mpfr_chebpoly_get_double_chebpoly(K->alpha + i, N->alpha + i);
}
