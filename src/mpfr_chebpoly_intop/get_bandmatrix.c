
#include "mpfr_chebpoly_intop.h"


void mpfr_chebpoly_intop_get_bandmatrix(mpfr_bandmatrix_t M, const mpfr_chebpoly_intop_t K, long N)
{
  long h = mpfr_chebpoly_intop_Hwidth(K);
  long s = mpfr_chebpoly_intop_Dwidth(K);
  long i;

  mpfr_bandmatrix_set_params(M, N + 1, h, s);
  mpfr_bandmatrix_zero(M);

  mpfr_bandvec_t V;
  mpfr_bandvec_init(V);

  for (i = 0 ; i <= N ; i++) {
    mpfr_chebpoly_intop_evaluate_Ti(V, K, i);
    mpfr_bandmatrix_set_column(M, V);
  }

  mpfr_bandvec_clear(V);
}



