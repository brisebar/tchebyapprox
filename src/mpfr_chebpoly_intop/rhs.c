
#include "mpfr_chebpoly_intop.h"


void mpfr_chebpoly_intop_rhs(mpfr_chebpoly_t G, const mpfr_chebpoly_lode_t L, const mpfr_chebpoly_t g, mpfr_srcptr I)
{
  mpfr_chebpoly_intop_initvals(G, L, I);
  mpfr_chebpoly_add(G, G, g);
}

