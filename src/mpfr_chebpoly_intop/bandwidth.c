
#include "mpfr_chebpoly_intop.h"


long mpfr_chebpoly_intop_Hwidth(const mpfr_chebpoly_intop_t K)
{
  long r = K->order;
  long h = 0;
  long i;

  for (i = 0 ; i < r ; i++)
    if (K->alpha[i].degree > h)
      h = K->alpha[i].degree;
  
  h++;

  return h;
}


long mpfr_chebpoly_intop_Dwidth(const mpfr_chebpoly_intop_t K)
{
  long r = K->order;
  long s = 0;
  long temp;
  long i;

  for (i = 0 ; i < r ; i++) {
    temp = (K->alpha[i]).degree + 1 + i;
    if (temp > s)
      s = temp;
  }

  return s;
}
