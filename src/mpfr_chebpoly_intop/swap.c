
#include "mpfr_chebpoly_intop.h"


void mpfr_chebpoly_intop_swap(mpfr_chebpoly_intop_t K, mpfr_chebpoly_intop_t N)
{
  long order_buf = K->order;
  mpfr_chebpoly_ptr alpha_buf = K->alpha;

  K->order = N->order;
  K->alpha = N->alpha;

  N->order = order_buf;
  N->alpha = alpha_buf;
}
