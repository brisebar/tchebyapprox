
#include "mpfr_chebpoly_intop.h"


void mpfr_chebpoly_intop_print(const mpfr_chebpoly_intop_t K, const char * Cheb_var, size_t digits)
{
  long r = K->order;
  long i;

  for (i = 0 ; i < r ; i++) {
    printf("alpha[%ld] = ", i);
    mpfr_chebpoly_print(K->alpha + i, Cheb_var, digits);
    printf("\n");
  }
}
