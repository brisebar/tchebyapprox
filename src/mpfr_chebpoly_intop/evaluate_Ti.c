
#include "mpfr_chebpoly_intop.h"


void mpfr_chebpoly_intop_evaluate_Ti(mpfr_bandvec_t V, const mpfr_chebpoly_intop_t K, long i)
{
  long r = K->order;
  long h = mpfr_chebpoly_intop_Hwidth(K);
  long s = mpfr_chebpoly_intop_Dwidth(K);
  long j, k;

  mpfr_bandvec_set_params(V, h, s, i);
  mpfr_bandvec_zero(V);

  mpfr_t temp, int_const;
  mpfr_inits(temp, int_const, NULL);


  for (j = 0 ; j < r ; j++) {

    mpfr_set_zero(int_const, 1);

    // T_{i-j-1}
    if (i - j - 1) {

      for (k = 0 ; k <= K->alpha[j].degree ; k++) {
	mpfr_div_si(temp, K->alpha[j].coeffs + k, i - j - 1, MPFR_RNDN);
	mpfr_sub(V->D + s - j - 1 - k, V->D + s - j - 1 - k, temp, MPFR_RNDN);
	mpfr_sub(V->D + s - j - 1 + k, V->D + s - j - 1 + k, temp, MPFR_RNDN);
      }

      if ((i + j) % 2)
	mpfr_set_si(temp, 1, MPFR_RNDN);
      else
	mpfr_set_si(temp, -1, MPFR_RNDN);
      mpfr_div_si(temp, temp, i - j - 1, MPFR_RNDN);
      mpfr_add(int_const, int_const, temp, MPFR_RNDN);

    }

    // T_{i-j+1}
    if (i - j + 1) {

      for (k = 0 ; k <= K->alpha[j].degree ; k++) {
	mpfr_div_si(temp, K->alpha[j].coeffs + k, i - j + 1, MPFR_RNDN);
	mpfr_add(V->D + s - j + 1 - k, V->D + s - j + 1 - k, temp, MPFR_RNDN);
	mpfr_add(V->D + s - j + 1 + k, V->D + s - j + 1 + k, temp, MPFR_RNDN);
      }

      if ((i + j) % 2)
	mpfr_set_si(temp, -1, MPFR_RNDN);
      else
	mpfr_set_si(temp, 1, MPFR_RNDN);
      mpfr_div_si(temp, temp, i - j + 1, MPFR_RNDN);
      mpfr_add(int_const, int_const, temp, MPFR_RNDN);

    }

    // T_{i+j-1}
    if (i + j - 1) {

      for (k = 0 ; k <= K->alpha[j].degree ; k++) {
	mpfr_div_si(temp, K->alpha[j].coeffs + k, i + j - 1, MPFR_RNDN);
	mpfr_sub(V->D + s + j - 1 - k, V->D + s + j - 1 - k, temp, MPFR_RNDN);
	mpfr_sub(V->D + s + j - 1 + k, V->D + s + j - 1 + k, temp, MPFR_RNDN);
      }

      if ((i + j) % 2)
	mpfr_set_si(temp, 1, MPFR_RNDN);
      else
	mpfr_set_si(temp, -1, MPFR_RNDN);
      mpfr_div_si(temp, temp, i + j - 1, MPFR_RNDN);
      mpfr_add(int_const, int_const, temp, MPFR_RNDN);

    }

    // T_{i+j+1}
    if (i + j + 1) {

      for (k = 0 ; k <= K->alpha[j].degree ; k++) {
	mpfr_div_si(temp, K->alpha[j].coeffs + k, i + j + 1, MPFR_RNDN);
	mpfr_add(V->D + s + j + 1 - k, V->D + s + j + 1 - k, temp, MPFR_RNDN);
	mpfr_add(V->D + s + j + 1 + k, V->D + s + j + 1 + k, temp, MPFR_RNDN);
      }

      if ((i + j) % 2)
	mpfr_set_si(temp, -1, MPFR_RNDN);
      else
	mpfr_set_si(temp, 1, MPFR_RNDN);
      mpfr_div_si(temp, temp, i + j + 1, MPFR_RNDN);
      mpfr_add(int_const, int_const, temp, MPFR_RNDN);

    }

    // contribution of the integration constant
    for (k = 0 ; k <= K->alpha[j].degree ; k++) {
      mpfr_mul(temp, int_const, K->alpha[j].coeffs + k, MPFR_RNDN);
      mpfr_add(V->H + k, V->H + k, temp, MPFR_RNDN);
    }

  }

  // normalisation
  for (j = 0 ; j < h ; j++)
    mpfr_mul_2si(V->H + j, V->H + j, -2, MPFR_RNDN);
  
  for (j = 0 ; j <= 2 * s ; j++)
    mpfr_mul_2si(V->D + j, V->D + j, -3, MPFR_RNDN);

  mpfr_bandvec_normalise(V);

  // clear variables
  mpfr_clears(temp, int_const, NULL);
}


void mpfr_chebpoly_intop_evaluate_Ti_fi(mpfi_bandvec_t V, const mpfr_chebpoly_intop_t K, long i)
{
  long r = K->order;
  long h = mpfr_chebpoly_intop_Hwidth(K);
  long s = mpfr_chebpoly_intop_Dwidth(K);
  long j, k;

  mpfi_bandvec_set_params(V, h, s, i);
  mpfi_bandvec_zero(V);

  mpfi_t temp, int_const;
  mpfi_init(temp);
  mpfi_init(int_const);


  for (j = 0 ; j < r ; j++) {

    mpfi_set_si(int_const, 0);

    // T_{i-j-1}
    if (i - j - 1) {

      for (k = 0 ; k <= K->alpha[j].degree ; k++) {
	mpfi_set_fr(temp, K->alpha[j].coeffs + k);
	mpfi_div_si(temp, temp, i - j - 1);
	mpfi_sub(V->D + s - j - 1 - k, V->D + s - j - 1 - k, temp);
	mpfi_sub(V->D + s - j - 1 + k, V->D + s - j - 1 + k, temp);
      }

      if ((i + j) % 2)
	mpfi_set_si(temp, 1);
      else
	mpfi_set_si(temp, -1);
      mpfi_div_si(temp, temp, i - j - 1);
      mpfi_add(int_const, int_const, temp);

    }

    // T_{i-j+1}
    if (i - j + 1) {

      for (k = 0 ; k <= K->alpha[j].degree ; k++) {
	mpfi_set_fr(temp, K->alpha[j].coeffs + k);
	mpfi_div_si(temp, temp, i - j + 1);
	mpfi_add(V->D + s - j + 1 - k, V->D + s - j + 1 - k, temp);
	mpfi_add(V->D + s - j + 1 + k, V->D + s - j + 1 + k, temp);
      }

      if ((i + j) % 2)
	mpfi_set_si(temp, -1);
      else
	mpfi_set_si(temp, 1);
      mpfi_div_si(temp, temp, i - j + 1);
      mpfi_add(int_const, int_const, temp);

    }

    // T_{i+j-1}
    if (i + j - 1) {

      for (k = 0 ; k <= K->alpha[j].degree ; k++) {
	mpfi_set_fr(temp, K->alpha[j].coeffs + k);
	mpfi_div_si(temp, temp, i + j - 1);
	mpfi_sub(V->D + s + j - 1 - k, V->D + s + j - 1 - k, temp);
	mpfi_sub(V->D + s + j - 1 + k, V->D + s + j - 1 + k, temp);
      }

      if ((i + j) % 2)
	mpfi_set_si(temp, 1);
      else
	mpfi_set_si(temp, -1);
      mpfi_div_si(temp, temp, i + j - 1);
      mpfi_add(int_const, int_const, temp);

    }

    // T_{i+j+1}
    if (i + j + 1) {

      for (k = 0 ; k <= K->alpha[j].degree ; k++) {
	mpfi_set_fr(temp, K->alpha[j].coeffs + k);
	mpfi_div_si(temp, temp, i + j + 1);
	mpfi_add(V->D + s + j + 1 - k, V->D + s + j + 1 - k, temp);
	mpfi_add(V->D + s + j + 1 + k, V->D + s + j + 1 + k, temp);
      }

      if ((i + j) % 2)
	mpfi_set_si(temp, -1);
      else
	mpfi_set_si(temp, 1);
      mpfi_div_si(temp, temp, i + j + 1);
      mpfi_add(int_const, int_const, temp);

    }

    // contribution of the integration constant
    for (k = 0 ; k <= K->alpha[j].degree ; k++) {
      mpfi_mul_fr(temp, int_const, K->alpha[j].coeffs + k);
      mpfi_add(V->H + k, V->H + k, temp);
    }

  }

  // normalisation
  for (j = 0 ; j < h ; j++)
    mpfi_mul_2si(V->H + j, V->H + j, -2);
  
  for (j = 0 ; j <= 2 * s ; j++)
    mpfi_mul_2si(V->D + j, V->D + j, -3);

  mpfi_bandvec_normalise(V);

  // clear variables
  mpfi_clear(temp);
  mpfi_clear(int_const);
}

