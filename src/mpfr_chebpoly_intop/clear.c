
#include "mpfr_chebpoly_intop.h"


void mpfr_chebpoly_intop_clear(mpfr_chebpoly_intop_t K)
{
  long r = K->order;
  long i;
  for (i = 0 ; i < r ; i++)
    mpfr_chebpoly_clear(K->alpha + i);
  free(K->alpha);
}
