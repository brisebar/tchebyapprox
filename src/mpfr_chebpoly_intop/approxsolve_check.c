
#include "mpfr_chebpoly_intop.h"



void mpfr_chebpoly_intop_approxsolve_check(mpfr_t bound, mpfr_chebpoly_srcptr Sols, const mpfr_chebpoly_lode_t L, const mpfr_chebpoly_t g, mpfr_srcptr I)
{
  long r = L->order; 
  long i;

  // error in the differential equation
  mpfr_chebpoly_t P, Temp;
  mpfr_chebpoly_init(P);
  mpfr_chebpoly_init(Temp);
 
  mpfr_chebpoly_set(P, Sols + r);

  for (i = 0 ; i < r ; i++) {
    mpfr_chebpoly_mul(Temp, L->a + i, Sols + i);
    mpfr_chebpoly_add(P, P, Temp);
  }

  mpfr_chebpoly_sub(P, P, g);

  mpfr_chebpoly_1norm(bound, P);

  // error in the initial conditions
  mpfr_t t;
  mpfr_init(t);

  for (i = 0 ; i < r ; i++) {
    mpfr_chebpoly_evaluate_si(t, Sols + i, -1);
    mpfr_sub(t, t, I + i, MPFR_RNDN);
    mpfr_abs(t, t, MPFR_RNDU);
    mpfr_add(bound, bound, t, MPFR_RNDU);
  }

  mpfr_clear(t);
  mpfr_chebpoly_clear(P);
  mpfr_chebpoly_clear(Temp);
}

