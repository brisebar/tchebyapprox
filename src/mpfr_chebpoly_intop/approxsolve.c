
#include "mpfr_chebpoly_intop.h"


void mpfr_chebpoly_intop_approxsolve(mpfr_chebpoly_t F, const mpfr_chebpoly_intop_t K, const mpfr_chebpoly_t G, long N)
{
  long i;
  long d = mpfr_chebpoly_intop_Dwidth(K);
  long Ntrunc = N < 2*d ? 2*d : N;

  // matrix associated to K
  mpfr_bandmatrix_t M;
  mpfr_bandmatrix_init(M);
  mpfr_chebpoly_intop_get_bandmatrix(M, K, Ntrunc);
  mpfr_bandmatrix_neg(M, M);
  for (i = 0 ; i <= Ntrunc ; i++)
    mpfr_add_si(M->D[i] + M->Dwidth, M->D[i] + M->Dwidth, 1, MPFR_RNDN);

  // QR factorization
  mpfr_bandmatrix_QRdecomp_t MM;
  mpfr_bandmatrix_QRdecomp_init(MM);
  mpfr_bandmatrix_get_QRdecomp(MM, M);

  // linear system solving
  mpfr_chebpoly_t Sol;
  mpfr_chebpoly_init(Sol);
  mpfr_chebpoly_set(Sol, G);
  if (Sol->degree < Ntrunc)
    mpfr_chebpoly_set_degree(Sol, Ntrunc);
  _mpfr_bandmatrix_QRdecomp_solve_fr(Sol->coeffs, MM, Sol->coeffs);
  mpfr_chebpoly_normalise(Sol);

  if (Ntrunc > N)
    mpfr_chebpoly_set_degree(Sol, N);

  // clear variables
  mpfr_chebpoly_swap(F, Sol);
  mpfr_chebpoly_clear(Sol);
  mpfr_bandmatrix_clear(M);
  mpfr_bandmatrix_QRdecomp_clear(MM);
}


void mpfr_chebpoly_lode_intop_approxsolve(mpfr_chebpoly_ptr Sols, const mpfr_chebpoly_lode_t L, const mpfr_chebpoly_t g, mpfr_srcptr I, long N)
{
  long r = L->order;
  long i;

  // creating integral operator intop
  mpfr_chebpoly_intop_t K;
  mpfr_chebpoly_intop_init(K);
  mpfr_chebpoly_intop_set_lode(K, L);

  // constructing rhs
  mpfr_chebpoly_t G;
  mpfr_chebpoly_init(G);
  mpfr_chebpoly_intop_rhs(G, L, g, I);

  // solving for the r-th derivative using intop
  mpfr_chebpoly_intop_approxsolve(Sols + r, K, G, N);

  // computing the other derivatives
  mpfr_chebpoly_set_degree(G, 0);
  for (i = r - 1 ; i >= 0 ; i--) {
    mpfr_chebpoly_antiderivative_1(Sols + i, Sols + i + 1);
    mpfr_chebpoly_set_coeff_fr(G, 0, I + i);
    mpfr_chebpoly_add(Sols + i, Sols + i, G);
  }

  // clear variables
  mpfr_chebpoly_intop_clear(K);
  mpfr_chebpoly_clear(G);
}


 
