
#include "chebmodel_vec_lode.h"


void chebmodel_vec_lode_set_order_dim(chebmodel_vec_lode_t L, long order, long dim)
{
  chebmodel_vec_lode_clear(L);
  L->order = order;
  L->dim = dim;

  long i, j, k;
  L->A = malloc(order * sizeof(chebmodel_ptr_ptr));
  for (i = 0 ; i < order ; i++) {
    L->A[i] = malloc(dim * sizeof(chebmodel_ptr));
    for (j = 0 ; j < dim ; j++) {
      L->A[i][j] = malloc(dim * sizeof(chebmodel_t));
      for (k = 0 ; k < dim ; k++)
	chebmodel_init(L->A[i][j] + k);
    }
  }
}


