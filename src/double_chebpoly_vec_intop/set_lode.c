
#include "double_chebpoly_vec_intop.h"


void double_chebpoly_vec_intop_set_lode(double_chebpoly_vec_intop_t K, const double_chebpoly_vec_lode_t L)
{
  long n = L->dim;
  double_chebpoly_vec_intop_set_dim(K, n);
  long i, j;
  double_chebpoly_lode_t Lij;
  double_chebpoly_lode_init(Lij);
  
  for (i = 0 ; i < n ; i++)
    for (j = 0 ; j < n ; j++) {
      double_chebpoly_vec_lode_get_double_chebpoly_lode(Lij, L, i, j);
      double_chebpoly_intop_set_lode(K->intop[i] + j, Lij);
    }

  double_chebpoly_lode_clear(Lij);
}


