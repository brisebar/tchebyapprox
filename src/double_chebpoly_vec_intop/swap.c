
#include "double_chebpoly_vec_intop.h"


void double_chebpoly_vec_intop_swap(double_chebpoly_vec_intop_t K, double_chebpoly_vec_intop_t N)
{
  long dim_buf = K->dim;
  double_chebpoly_intop_struct ** intop_buf = K->intop;

  K->dim = N->dim;
  K->intop = N->intop;

  N->dim = dim_buf;
  N->intop = intop_buf;
}
