
#include "double_chebpoly_vec_intop.h"



void double_chebpoly_vec_intop_approxsolve_check(double_ptr bounds, double_chebpoly_vec_srcptr Sols, const double_chebpoly_vec_lode_t L, const double_chebpoly_vec_t g, double_ptr_ptr I)
{
  long r = L->order; 
  long n = L->dim;
  long i, j, k;

  double_chebpoly_t P, Temp;
  double_chebpoly_init(P);
  double_chebpoly_init(Temp);
  double_t t;
  double_init(t);

  for (j = 0 ; j < n ; j++) {

    // error in the differential equation
    double_chebpoly_set(P, Sols[r].poly + j);
    for (i = 0 ; i < r ; i++)
      for (k = 0 ; k < n ; k++) {
        double_chebpoly_mul(Temp, L->A[i][j] + k, Sols[i].poly + k);
        double_chebpoly_add(P, P, Temp);
      }
    double_chebpoly_sub(P, P, g->poly + j);
    double_chebpoly_1norm(bounds + j, P);

    // error in the initial conditions
    for (i = 0 ; i < r ; i++) {
      double_chebpoly_evaluate_si(t, Sols[i].poly + j, -1);
      double_sub(t, t, I[j] + i, MPFR_RNDN);
      double_abs(t, t, MPFR_RNDU);
      double_add(bounds + j, bounds + j, t, MPFR_RNDU);
    }

  }
  
  double_clear(t);
  double_chebpoly_clear(P);
  double_chebpoly_clear(Temp);
}

