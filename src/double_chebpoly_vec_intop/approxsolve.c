
#include "double_chebpoly_vec_intop.h"


void double_chebpoly_vec_intop_approxsolve(double_chebpoly_vec_t F, const double_chebpoly_vec_intop_t K, const double_chebpoly_vec_t G, long N)
{
  long i, j, imax;
  long n = K->dim;
  long NN = n * (N+1);

  // matrix associated to K
  double_bandmatrix_t M;
  double_bandmatrix_init(M);
  double_chebpoly_vec_intop_get_bandmatrix(M, K, N);
  long h = M->Hwidth;
  long d = M->Dwidth;
  double_bandmatrix_neg(M, M);
  for (i = 0 ; i < NN ; i++)
    double_add_si(M->D[i] + d, M->D[i] + d, 1, MPFR_RNDN);

  // QR factorization
  double_bandmatrix_QRdecomp_t MM;
  double_bandmatrix_QRdecomp_init(MM);
  double_bandmatrix_get_QRdecomp(MM, M);

  // creating rhs vector
  double_vec_t V;
  double_vec_init(V);
  double_vec_set_length(V, NN);
  double_vec_zero(V);
  for (j = 0 ; j < n ; j++) {
    imax = G->poly[j].degree < N ? G->poly[j].degree : N;
    for (i = 0 ; i <= imax; i++)
      double_set(V->coeffs + n*i+j, G->poly[j].coeffs + i, MPFR_RNDN);
  }

  // linear system solving
  double_bandmatrix_QRdecomp_solve_d(V, MM, V);

  // reconstituing polynomials
  double_chebpoly_vec_set(F, G);
  for (j = 0 ; j < n ; j++) {
    if (F->poly[j].degree < N)
      double_chebpoly_set_degree(F->poly + j, N);
    for (i = 0 ; i <= N ; i++)
      double_set(F->poly[j].coeffs + i, V->coeffs + n*i+j, MPFR_RNDN);
    double_chebpoly_normalise(F->poly + j);
  }

  // clear variables
  double_bandmatrix_clear(M);
  double_bandmatrix_QRdecomp_clear(MM);
  double_vec_clear(V);
}


void double_chebpoly_vec_lode_intop_approxsolve(double_chebpoly_vec_ptr Sols, const double_chebpoly_vec_lode_t L, const double_chebpoly_vec_t g, double_ptr_ptr I, long N)
{
  if (L->dim != g->dim) {
    fprintf(stderr, "double_chebpoly_vec_lode_intop_approxsolve: error: incompatible dimensions\n");
    return;
  }

  long r = L->order;
  long n = L->dim;
  long i, j;

  // creating integral operator intop
  double_chebpoly_vec_intop_t K;
  double_chebpoly_vec_intop_init(K);
  double_chebpoly_vec_intop_set_lode(K, L);

  // constructing rhs
  double_chebpoly_vec_t G;
  double_chebpoly_vec_init(G);
  double_chebpoly_vec_intop_rhs(G, L, g, I);

  // solving for the r-th derivative using intop
  double_chebpoly_vec_intop_approxsolve(Sols + r, K, G, N);

  // computing the other derivatives
  double_chebpoly_t C;
  double_chebpoly_init(C);
  double_chebpoly_set_degree(C, 0);
  for (i = r - 1 ; i >= 0 ; i--) {
    double_chebpoly_vec_antiderivative_1(Sols + i, Sols + i + 1);
    for (j = 0 ; j < n ; j++) {
      double_chebpoly_set_coeff_d(C, 0, I[j] + i);
      double_chebpoly_add(Sols[i].poly + j, Sols[i].poly + j, C);
    }
  }

  // clear variables
  double_chebpoly_vec_intop_clear(K);
  double_chebpoly_vec_clear(G);
  double_chebpoly_clear(C);
}


