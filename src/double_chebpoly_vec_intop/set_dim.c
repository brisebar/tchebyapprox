
#include "double_chebpoly_vec_intop.h"


void double_chebpoly_vec_intop_set_dim(double_chebpoly_vec_intop_t K, long dim)
{
  double_chebpoly_vec_intop_clear(K);
  K->dim = dim;
  long i, j;

  K->intop = malloc(dim * sizeof(double_chebpoly_intop_ptr));
  for (i = 0 ; i < dim ; i++) {
    K->intop[i] = malloc(dim * sizeof(double_chebpoly_intop_t));
    for (j = 0 ; j < dim ; j++)
      double_chebpoly_intop_init(K->intop[i] + j);
  }
}

