
#include "double_chebpoly_vec_intop.h"


void double_chebpoly_vec_intop_initvals(double_chebpoly_vec_t G, const double_chebpoly_vec_lode_t L, double_ptr_ptr I)
{
  long n = L->dim;
  double_chebpoly_vec_set_dim(G, n);
  double_chebpoly_vec_zero(G);
  long i, j;
  double_chebpoly_lode_t Lij;
  double_chebpoly_lode_init(Lij);
  double_chebpoly_t F;
  double_chebpoly_init(F);

  for (i = 0 ; i < n ; i++)
    for (j = 0 ; j < n ; j++) {
      double_chebpoly_vec_lode_get_double_chebpoly_lode(Lij, L, i, j);
      double_chebpoly_intop_initvals(F, Lij, I[j]);
      double_chebpoly_add(G->poly + i, G->poly + i, F);
    }
  double_chebpoly_lode_clear(Lij);
  double_chebpoly_clear(F);
}


