
#include "double_chebpoly_vec_intop.h"


void double_chebpoly_vec_intop_get_bandmatrix(double_bandmatrix_t M, const double_chebpoly_vec_intop_t K, long N)
{
  long n = K->dim;
  long h = double_chebpoly_vec_intop_Hwidth(K);
  long d = double_chebpoly_vec_intop_Dwidth(K);
  long i, j, k, l, hij, dij, lmin, lmax;

  double_bandmatrix_set_params(M, n * (N + 1), h, d);
  double_bandmatrix_zero(M);
  double_bandmatrix_t Mij;
  double_bandmatrix_init(Mij);

  for (i = 0 ; i < n ; i++)
    for (j = 0 ; j < n ; j++) {
      double_chebpoly_intop_get_bandmatrix(Mij, K->intop[i] + j, N);
      hij = Mij->Hwidth;
      dij = Mij->Dwidth;
      // initial coefficients
      for (k = 0 ; k <= N ; k++) {
	lmin = 0;
	lmax = hij <= N ? hij-1 : N;
	for (l = lmin ; l <= lmax ; l++)
	  double_set(M->H[n*l+i] + n*k+j, Mij->H[l] + k, MPFR_RNDN);
      }
      // diagonal coefficients
      for (k = 0 ; k <= N ; k++) {
        lmin = k-dij < 0 ? -k : -dij;
	lmax = k+dij > N ? N-k : dij;
	for (l = lmin ; l <= lmax ; l++)
	  double_set(M->D[n*k+i] + d+n*l+j-i, Mij->D[k] + dij+l, MPFR_RNDN);
      }
    }
  
  double_bandmatrix_normalise(M); 

  double_bandmatrix_clear(Mij);
}


