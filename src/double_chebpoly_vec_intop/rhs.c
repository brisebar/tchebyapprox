
#include "double_chebpoly_vec_intop.h"


void double_chebpoly_vec_intop_rhs(double_chebpoly_vec_t G, const double_chebpoly_vec_lode_t L, const double_chebpoly_vec_t g, double_ptr_ptr I)
{
  double_chebpoly_vec_intop_initvals(G, L, I);
  double_chebpoly_vec_add(G, G, g);
}

