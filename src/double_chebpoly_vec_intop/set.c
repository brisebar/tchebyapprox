
#include "double_chebpoly_vec_intop.h"


void double_chebpoly_vec_intop_set(double_chebpoly_vec_intop_t K, const double_chebpoly_vec_intop_t N)
{
  if (K != N) {
    long n = N->dim;
    double_chebpoly_vec_intop_set_dim(K, n);
    long i, j;
    for (i = 0 ; i < n ; i++)
      for (j = 0 ; j < n ; j++)
	double_chebpoly_intop_set(K->intop[i] + j, N->intop[i] + j);
  }
}


