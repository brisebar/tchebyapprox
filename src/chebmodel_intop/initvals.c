
#include "chebmodel_intop.h"


void chebmodel_intop_initvals(chebmodel_t G, const chebmodel_lode_t L, mpfi_srcptr I)
{
  long r = L->order;
  long i;

  chebmodel_t P, Q;
  chebmodel_init(P);
  chebmodel_init(Q);

  chebmodel_zero(G);

  for (i = r - 1 ; i >= 0 ; i--) {
    
    // update P
    chebmodel_antiderivative_1(P, P);
    chebmodel_set_degree(Q, 0);
    mpfi_chebpoly_set_coeff_fi(Q->poly, 0, I + i);
    chebmodel_add(P, P, Q);

    // add L->a + i * P to G
    chebmodel_mul(Q, L->a + i, P);
    chebmodel_sub(G, G, Q);

  }

  // clear variables
  chebmodel_clear(P);
  chebmodel_clear(Q);
}

