
#include "chebmodel_intop.h"


void chebmodel_intop_set(chebmodel_intop_t K, const chebmodel_intop_t N)
{
  long r = N->order;
  chebmodel_intop_set_order(K, r);

  long i;
  for (i = 0 ; i < r ; i++)
    chebmodel_set(K->alpha + i, N->alpha + i);
}

void chebmodel_intop_set_double_chebpoly_intop(chebmodel_intop_t K, const double_chebpoly_intop_t N)
{
  long r = N->order;
  chebmodel_intop_set_order(K, r);

  long i;
  for (i = 0 ; i < r ; i++)
    chebmodel_set_double_chebpoly(K->alpha + i, N->alpha + i);
}

void chebmodel_intop_set_mpfr_chebpoly_intop(chebmodel_intop_t K, const mpfr_chebpoly_intop_t N)
{
  long r = N->order;
  chebmodel_intop_set_order(K, r);

  long i;
  for (i = 0 ; i < r ; i++)
    chebmodel_set_mpfr_chebpoly(K->alpha + i, N->alpha + i);
}


void chebmodel_intop_set_mpfi_chebpoly_intop(chebmodel_intop_t K, const mpfi_chebpoly_intop_t N)
{
  long r = N->order;
  chebmodel_intop_set_order(K, r);

  long i;
  for (i = 0 ; i < r ; i++)
    chebmodel_set_mpfi_chebpoly(K->alpha + i, N->alpha + i);
}

void chebmodel_intop_get_double_chebpoly_intop(double_chebpoly_intop_t K, const chebmodel_intop_t N)
{
  long r = N->order;
  double_chebpoly_intop_set_order(K, r);

  long i;
  for (i = 0 ; i < r ; i++)
    chebmodel_get_double_chebpoly(K->alpha + i, N->alpha + i);
}

void chebmodel_intop_get_mpfr_chebpoly_intop(mpfr_chebpoly_intop_t K, const chebmodel_intop_t N)
{
  long r = N->order;
  mpfr_chebpoly_intop_set_order(K, r);

  long i;
  for (i = 0 ; i < r ; i++)
    chebmodel_get_mpfr_chebpoly(K->alpha + i, N->alpha + i);
}


void chebmodel_intop_get_mpfi_chebpoly_intop(mpfi_chebpoly_intop_t K, const chebmodel_intop_t N)
{
  long r = N->order;
  mpfi_chebpoly_intop_set_order(K, r);

  long i;
  for (i = 0 ; i < r ; i++)
    chebmodel_get_mpfi_chebpoly(K->alpha + i, N->alpha + i);
}

