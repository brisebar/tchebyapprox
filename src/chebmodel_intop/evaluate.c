
#include "chebmodel_intop.h"


void chebmodel_intop_evaluate_chebmodel(chebmodel_t P, const chebmodel_intop_t K, const chebmodel_t Q)
{
  long r = K->order;
  long i;

  chebmodel_t K_Q, K_Q_i;
  chebmodel_init(K_Q);
  chebmodel_init(K_Q_i);

  for (i = 0 ; i < r ; i++) {
    
    chebmodel_zero(K_Q_i);
    mpfi_chebpoly_set_coeff_si(K_Q_i->poly, i, 1);
    chebmodel_mul(K_Q_i, K_Q_i, Q);

    chebmodel_antiderivative_1(K_Q_i, K_Q_i);

    chebmodel_mul(K_Q_i, K->alpha + i, K_Q_i);

    chebmodel_add(K_Q, K_Q, K_Q_i);

  }

  // clear variables
  chebmodel_clear(K_Q_i);
  chebmodel_swap(P, K_Q);
  chebmodel_clear(K_Q);
}


void chebmodel_intop_evaluate_mpfr_chebpoly(chebmodel_t P, const chebmodel_intop_t K, const mpfr_chebpoly_t Q)
{
  chebmodel_t Q2;
  chebmodel_init(Q2);
  chebmodel_set_mpfr_chebpoly(Q2, Q);
  chebmodel_intop_evaluate_chebmodel(P, K, Q2);
  chebmodel_clear(Q2);
}


void chebmodel_intop_evaluate_mpfi_chebpoly(chebmodel_t P, const chebmodel_intop_t K, const mpfi_chebpoly_t Q)
{
  chebmodel_t Q2;
  chebmodel_init(Q2);
  chebmodel_set_mpfi_chebpoly(Q2, Q);
  chebmodel_intop_evaluate_chebmodel(P, K, Q2);
  chebmodel_clear(Q2);
}



