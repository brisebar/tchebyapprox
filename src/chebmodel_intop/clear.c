
#include "chebmodel_intop.h"


void chebmodel_intop_clear(chebmodel_intop_t K)
{
  long r = K->order;
  long i;
  for (i = 0 ; i < r ; i++)
    chebmodel_clear(K->alpha + i);
  free(K->alpha);
}
