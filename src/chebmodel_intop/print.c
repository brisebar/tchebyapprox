
#include "chebmodel_intop.h"


void chebmodel_intop_print(const chebmodel_intop_t K, const char * Cheb_var, size_t digits)
{
  long r = K->order;
  long i;

  for (i = 0 ; i < r ; i++) {
    printf("alpha[%ld] = ", i);
    chebmodel_print(K->alpha + i, Cheb_var, digits);
    printf("\n");
  }
}
