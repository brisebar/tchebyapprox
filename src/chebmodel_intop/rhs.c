
#include "chebmodel_intop.h"


void chebmodel_intop_rhs(chebmodel_t G, const chebmodel_lode_t L, const chebmodel_t g, mpfi_srcptr I)
{
  chebmodel_intop_initvals(G, L, I);
  chebmodel_add(G, G, g);
}

