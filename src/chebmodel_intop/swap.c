
#include "chebmodel_intop.h"


void chebmodel_intop_swap(chebmodel_intop_t K, chebmodel_intop_t N)
{
  long order_buf = K->order;
  chebmodel_ptr alpha_buf = K->alpha;

  K->order = N->order;
  K->alpha = N->alpha;

  N->order = order_buf;
  N->alpha = alpha_buf;
}
