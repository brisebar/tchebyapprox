
#include "chebmodel_intop.h"


long chebmodel_intop_Hwidth(const chebmodel_intop_t K)
{
  long r = K->order;
  long h = 0;
  long i;

  for (i = 0 ; i < r ; i++)
    if (K->alpha[i].poly->degree > h)
      h = K->alpha[i].poly->degree;
  
  h++;

  return h;
}


long chebmodel_intop_Dwidth(const chebmodel_intop_t K)
{
  long r = K->order;
  long s = 0;
  long temp;
  long i;

  for (i = 0 ; i < r ; i++) {
    temp = (K->alpha[i]).poly->degree + 1 + i;
    if (temp > s)
      s = temp;
  }

  return s;
}
