
#include "chebmodel_intop.h"


void chebmodel_intop_set_order(chebmodel_intop_t K, long order)
{
  long r = K->order;
  K->order = order;
  long i;

  if (order < r) {
    for (i = order ; i < r ; i++)
      chebmodel_clear(K->alpha + i);
    K->alpha = realloc(K->alpha, order * sizeof(chebmodel_t));
  }

  else if (order > r) {
    K->alpha = realloc(K->alpha, order * sizeof(chebmodel_t));
    for (i = r ; i < order ; i++)
      chebmodel_init(K->alpha + i);
  }

  else
    return;
}
