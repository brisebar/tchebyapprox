
#include "mpfr_chebpoly.h"


// init with P = 0
void mpfr_chebpoly_init(mpfr_chebpoly_t P)
{
  P->degree = -1;
  P->coeffs = malloc(0);
}
