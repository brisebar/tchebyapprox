
#include "mpfr_chebpoly.h"


void _mpfr_chebpoly_print(mpfr_srcptr P, long n, const char * var, size_t digits)
{
  long i;
  int flag = 0;

  for (i = n ; i >=0 ; i--)
    if (!mpfr_zero_p(P + i)) {
      if (flag)
	  printf("+");
      else
	flag = 1;
      mpfr_out_str(stdout, 10, digits, P + i, MPFR_RNDN);
      printf("*%s_%ld", var, i);
    }

}


// display the polynomial P with 'var' as indeterminate symbol
// 'var' is a 0-terminated string
void mpfr_chebpoly_print(const mpfr_chebpoly_t P, const char * var, size_t digits)
{
  long n = P->degree;

  if (n < 0)
    printf("0");

  else
    _mpfr_chebpoly_print(P->coeffs, n, var, digits);

}
