
#include "mpfr_chebpoly.h"


void _mpfr_chebpoly_set_mpfr_poly(mpfr_ptr P, mpfr_srcptr Q, long n)
{
  long i, j;
  mpfr_t prev, temp;
  mpfr_init(prev);
  mpfr_init(temp);

  mpfr_ptr R = malloc((n+1) * sizeof(mpfr_t));
  for (i = 0 ; i <= n ; i++) {
    mpfr_init(R + i);
    mpfr_set_si(R + i, 0, MPFR_RNDN);
  }
  
  mpfr_set(R + 0, Q + n, MPFR_RNDN); // R = Q_n

  for (i = 1 ; i <= n ; i++) {

    // multiplication by X
    for (j = 1 ; j < i ; j++)
      mpfr_mul_2si(R + j, R + j, -1, MPFR_RNDN);
    mpfr_set(prev, R + 0, MPFR_RNDN);
    mpfr_set_si(R + 0, 0, MPFR_RNDN);
    for (j = 1 ; j < i ; j++) {
      mpfr_add(R + j - 1, R + j - 1, R + j, MPFR_RNDN);
      mpfr_set(temp, R + j, MPFR_RNDN);
      mpfr_set(R + j, prev, MPFR_RNDN);
      mpfr_set(prev, temp, MPFR_RNDN);
    }
    mpfr_set(R + i, prev, MPFR_RNDN);

    // add Q[n-i]
    mpfr_add(R, R, Q + n - i, MPFR_RNDN);

  }

  for (i = 0 ; i <= n ; i++) {
    mpfr_swap(P + i, R + i);
    mpfr_clear(R + i);
  }
  free(R);
  mpfr_clear(prev);
  mpfr_clear(temp);
}


// set in P the conversion of Q (in the monomial basis) into the Chebyshev basis
void mpfr_chebpoly_set_mpfr_poly(mpfr_chebpoly_t P, const mpfr_poly_t Q)
{
  long n = Q->degree;

  if (n < 0)
    mpfr_chebpoly_zero(P);

  else {
    mpfr_chebpoly_set_degree(P, n);
    _mpfr_chebpoly_set_mpfr_poly(P->coeffs, Q->coeffs, n);
  }
}
