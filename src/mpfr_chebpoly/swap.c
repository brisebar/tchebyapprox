
#include "mpfr_chebpoly.h"


// swap P and Q efficiently
void mpfr_chebpoly_swap(mpfr_chebpoly_t P, mpfr_chebpoly_t Q)
{
  long degree_buf = P->degree;
  mpfr_ptr coeffs_buf = P->coeffs;
  P->degree = Q->degree;
  P->coeffs = Q->coeffs;
  Q->degree = degree_buf;
  Q->coeffs = coeffs_buf;
}
