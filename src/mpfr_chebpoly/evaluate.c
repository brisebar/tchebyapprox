
#include "mpfr_chebpoly.h"


void _mpfr_chebpoly_evaluate_si(mpfr_t y, mpfr_srcptr P, long n, long x)
{
  mpfr_t t;
  mpfr_init(t);
  mpfr_set_si(t, x, MPFR_RNDN);
  _mpfr_chebpoly_evaluate_fr(y, P, n, t);
  mpfr_clear(t);
}


void _mpfr_chebpoly_evaluate_z(mpfr_t y, mpfr_srcptr P, long n, const mpz_t x)
{
  mpfr_t t;
  mpfr_init(t);
  mpfr_set_z(t, x, MPFR_RNDN);
  _mpfr_chebpoly_evaluate_fr(y, P, n, t);
  mpfr_clear(t);
}


void _mpfr_chebpoly_evaluate_q(mpfr_t y, mpfr_srcptr P, long n, const mpq_t x)
{
  mpfr_t t;
  mpfr_init(t);
  mpfr_set_q(t, x, MPFR_RNDN);
  _mpfr_chebpoly_evaluate_fr(y, P, n, t);
  mpfr_clear(t);
}


void _mpfr_chebpoly_evaluate_d(mpfr_t y, mpfr_srcptr P, long n, const double_t x)
{
  mpfr_t t;
  mpfr_init(t);
  mpfr_set_d(t, *x, MPFR_RNDN);
  _mpfr_chebpoly_evaluate_fr(y, P, n, t);
  mpfr_clear(t);
}


void _mpfr_chebpoly_evaluate_fr(mpfr_t y, mpfr_srcptr P, long n, const mpfr_t x)
{
  if (n < 0)
    mpfr_set_si(y, 0, MPFR_RNDN);

  else if (n == 0)
    mpfr_set(y, P, MPFR_RNDN);

  else {

    mpfr_t c1, c2, temp;
    mpfr_init(temp);
    mpfr_init(c1);
    mpfr_set(c1, P + n - 1, MPFR_RNDN);
    mpfr_init(c2);
    mpfr_set(c2, P + n, MPFR_RNDN);
    long i;

    for (i = n-2 ; i >= 0 ; i--) {
      // (c1,c2) -> (P[i]-c2, c1+2*x*c2)
      // new value of c1
      mpfr_swap(temp, c1);
      mpfr_sub(c1, P + i, c2, MPFR_RNDN);
      // new value of c2
      mpfr_mul(c2, c2, x, MPFR_RNDN);
      mpfr_mul_2si(c2, c2, 1, MPFR_RNDN);
      mpfr_add(c2, c2, temp, MPFR_RNDN);
    }

    // y = P(x) = c1+x*c2
    mpfr_mul(y, x, c2, MPFR_RNDN);
    mpfr_add(y, y, c1, MPFR_RNDN);
    // clear variables
    mpfr_clear(temp);
    mpfr_clear(c1);
    mpfr_clear(c2);
  }

}


void _mpfr_chebpoly_evaluate_fi(mpfi_t y, mpfr_srcptr P, long n, const mpfi_t x)
{
  if (n < 0)
    mpfi_set_si(y, 0);

  else if (n == 0)
    mpfi_set_fr(y, P);

  else {

    mpfi_t c1, c2, temp;
    mpfi_init(temp);
    mpfi_init(c1);
    mpfi_set_fr(c1, P + n - 1);
    mpfi_init(c2);
    mpfi_set_fr(c2, P + n);
    long i;

    for (i = n-2 ; i >= 0 ; i--) {
      // (c1,c2) -> (P[i]-c2, c1+2*x*c2)
      // new value of c1
      mpfi_swap(temp, c1);
      mpfi_fr_sub(c1, P + i, c2);
      // new value of c2
      mpfi_mul(c2, c2, x);
      mpfi_mul_2si(c2, c2, 1);
      mpfi_add(c2, c2, temp);
    }

    // y = P(x) = c1+x*c2
    mpfi_mul(y, x, c2);
    mpfi_add(y, y, c1);
    // clear variables
    mpfi_clear(temp);
    mpfi_clear(c1);
    mpfi_clear(c2);
  }

}

// set y to P(x)
void mpfr_chebpoly_evaluate_si(mpfr_t y, const mpfr_chebpoly_t P, long x)
{
  _mpfr_chebpoly_evaluate_si(y, P->coeffs, P->degree, x);
}


// set y to P(x)
void mpfr_chebpoly_evaluate_z(mpfr_t y, const mpfr_chebpoly_t P, const mpz_t x)
{
  _mpfr_chebpoly_evaluate_z(y, P->coeffs, P->degree, x);
}


// set y to P(x)
void mpfr_chebpoly_evaluate_q(mpfr_t y, const mpfr_chebpoly_t P, const mpq_t x)
{
  _mpfr_chebpoly_evaluate_q(y, P->coeffs, P->degree, x);
}


// set y to P(x)
void mpfr_chebpoly_evaluate_d(mpfr_t y, const mpfr_chebpoly_t P, const double_t x)
{
  _mpfr_chebpoly_evaluate_d(y, P->coeffs, P->degree, x);
}


// set y to P(x)
void mpfr_chebpoly_evaluate_fr(mpfr_t y, const mpfr_chebpoly_t P, const mpfr_t x)
{
  _mpfr_chebpoly_evaluate_fr(y, P->coeffs, P->degree, x);
}


// set y to P(x)
void mpfr_chebpoly_evaluate_fi(mpfi_t y, const mpfr_chebpoly_t P, const mpfi_t x)
{
  _mpfr_chebpoly_evaluate_fi(y, P->coeffs, P->degree, x);
}

// set y to P(-1)
void mpfr_chebpoly_evaluate_1(mpfr_t y, const mpfr_chebpoly_t P)
{
  long n = P->degree;
  long i;
  mpfr_set_si(y, 0, MPFR_RNDN);
  for (i = 0 ; i <= n ; i++)
    if (i % 2)
      mpfr_sub(y, y, P->coeffs + i, MPFR_RNDN);
    else
      mpfr_add(y, y, P->coeffs + i, MPFR_RNDN);
}

// set y to P(0)
void mpfr_chebpoly_evaluate0(mpfr_t y, const mpfr_chebpoly_t P)
{
  long n = P->degree;
  long i;
  mpfr_set_si(y, 0, MPFR_RNDN);
  for (i = 0 ; i <= n ; i+=2)
    if (i % 4)
      mpfr_sub(y, y, P->coeffs + i, MPFR_RNDN);
    else
      mpfr_add(y, y, P->coeffs + i, MPFR_RNDN);
}

// set y to P(1)
void mpfr_chebpoly_evaluate1(mpfr_t y, const mpfr_chebpoly_t P)
{
  long n = P->degree;
  long i;
  mpfr_set_si(y, 0, MPFR_RNDN);
  for (i = 0 ; i <= n ; i++)
    mpfr_add(y, y, P->coeffs + i, MPFR_RNDN);
}
