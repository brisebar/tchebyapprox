
#include "mpfr_chebpoly.h"




// set the n-th coefficient of P to c
void mpfr_chebpoly_set_coeff_si(mpfr_chebpoly_t P, long n, long c)
{
  if (n < 0)
    return;

  else if (n <= P->degree) {
    mpfr_set_si(P->coeffs + n, c, MPFR_RNDN);
    if (n == P->degree && c == 0)
      mpfr_chebpoly_normalise(P);
  }

  else if (c != 0) {
    P->coeffs = realloc(P->coeffs, (n+1) * sizeof(mpfr_t));
    long i;
    for (i = P->degree + 1 ; i <= n ; i++) {
      mpfr_init(P->coeffs + i);
      mpfr_set_si(P->coeffs + i, 0, MPFR_RNDN);
    }
    mpfr_set_si(P->coeffs + n, c, MPFR_RNDN);
    P->degree = n;
  }

  else
    return;
}


// set the n-th coefficient of P to c
void mpfr_chebpoly_set_coeff_z(mpfr_chebpoly_t P, long n, const mpz_t c)
{
  if (n < 0)
    return;

  else if (n <= P->degree) {
    mpfr_set_z(P->coeffs + n, c, MPFR_RNDN);
    if (n == P->degree && !mpz_sgn(c))
      mpfr_chebpoly_normalise(P);
  }

  else if (mpz_sgn(c)) {
    P->coeffs = realloc(P->coeffs, (n+1) * sizeof(mpfr_t));
    long i;
    for (i = P->degree + 1 ; i <= n ; i++) {
      mpfr_init(P->coeffs + i);
      mpfr_set_si(P->coeffs + i, 0, MPFR_RNDN);
    }
    mpfr_set_z(P->coeffs + n, c, MPFR_RNDN);
    P->degree = n;
  }

  else
    return;
}


// set the n-th coefficient of P to c
void mpfr_chebpoly_set_coeff_q(mpfr_chebpoly_t P, long n, const mpq_t c)
{
  if (n < 0)
    return;

  else if (n <= P->degree) {
    mpfr_set_q(P->coeffs + n, c, MPFR_RNDN);
    if (n == P->degree && !mpq_sgn(c))
      mpfr_chebpoly_normalise(P);
  }

  else if (mpq_sgn(c)) {
    P->coeffs = realloc(P->coeffs, (n+1) * sizeof(mpfr_t));
    long i;
    for (i = P->degree + 1 ; i <= n ; i++) {
      mpfr_init(P->coeffs + i);
      mpfr_set_si(P->coeffs + i, 0, MPFR_RNDN);
    }
    mpfr_set_q(P->coeffs + n, c, MPFR_RNDN);
    P->degree = n;
  }

  else
    return;
}


// set the n-th coefficient of P to c
void mpfr_chebpoly_set_coeff_fr(mpfr_chebpoly_t P, long n, const mpfr_t c)
{
  if (n < 0)
    return;

  else if (n <= P->degree) {
    mpfr_set(P->coeffs + n, c, MPFR_RNDN);
    if (n == P->degree && mpfr_zero_p(c))
      mpfr_chebpoly_normalise(P);
  }

  else if (!mpfr_zero_p(c)) {
    P->coeffs = realloc(P->coeffs, (n+1) * sizeof(mpfr_t));
    long i;
    for (i = P->degree + 1 ; i <= n ; i++) {
      mpfr_init(P->coeffs + i);
      mpfr_set_si(P->coeffs + i, 0, MPFR_RNDN);
    }
    mpfr_set(P->coeffs + n, c, MPFR_RNDN);
    P->degree = n;
  }

  else
    return;
}



