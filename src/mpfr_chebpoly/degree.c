
#include "mpfr_chebpoly.h"


// get the degree of P
long mpfr_chebpoly_degree(const mpfr_chebpoly_t P)
{
  return P->degree;
}
