
#include "mpfr_chebpoly.h"


void _mpfr_chebpoly_mul(mpfr_ptr P, mpfr_srcptr Q, long n, mpfr_srcptr R, long m)
{
  long i, j;
  mpfr_ptr S = malloc((n+m+1) * sizeof(mpfr_t));
  mpfr_t temp;
  mpfr_init(temp);

  for (i = 0 ; i <= n+m ; i++) {
    mpfr_init(S + i);
    mpfr_set_si(S + i, 0, MPFR_RNDN);
  }

  for (i = 0 ; i <= n ; i++)
    for (j = 0 ; j <= m ; j++) {
      mpfr_mul(temp, Q + i, R + j, MPFR_RNDN);
      mpfr_mul_2si(temp, temp, -1, MPFR_RNDN);
      mpfr_add(S + i + j, S + i + j, temp, MPFR_RNDN);
      if (i > j)
	mpfr_add(S + i - j, S + i - j, temp, MPFR_RNDN);
      else
	mpfr_add(S + j - i, S + j - i, temp, MPFR_RNDN);
    }

  for (i = 0 ; i <= n+m ; i++) {
    mpfr_swap(P + i, S + i);
    mpfr_clear(S + i);
  }

  mpfr_clear(temp);
  free(S);
}

// set P := Q * R
void mpfr_chebpoly_mul(mpfr_chebpoly_t P, const mpfr_chebpoly_t Q, const mpfr_chebpoly_t R)
{
  long n = Q->degree;
  long m = R->degree;

  if (n < 0 || m < 0)
    mpfr_chebpoly_zero(P);

  else {
    mpfr_chebpoly_set_degree(P, n+m);
    _mpfr_chebpoly_mul(P->coeffs, Q->coeffs, n, R->coeffs, m);
  }
}
