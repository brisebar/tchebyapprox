
#include "mpfr_chebpoly.h"


// computes F1 and F2 of degree N st F1 ~ sqrt(G) and F2 ~ 1/sqrt(G) 
void mpfr_chebpoly_sqrt_inv_sqrt_approx(mpfr_chebpoly_t F1, mpfr_chebpoly_t F2, const mpfr_chebpoly_t G, long N)
{
  long m = G->degree;
  long i, j, k;
  mpfr_t temp;
  mpfr_init(temp);

  // compute the values cos(i*pi/(2*N+2)) for 0 <= i < 4*N+4
  mpfr_ptr X = malloc((4*N+4) * sizeof(mpfr_t));
  for (i = 0 ; i < 4*N+4 ; i++) {
    mpfr_init(X + i);
    mpfr_const_pi(temp, MPFR_RNDN);
    mpfr_mul_si(temp, temp, i, MPFR_RNDN);
    mpfr_div_si(temp, temp, 2*N+2, MPFR_RNDN);
    mpfr_cos(X + i, temp, MPFR_RNDN);
  }

  // compute the coefficients of F1 and F2
  mpfr_t G_X_k;
  mpfr_init(G_X_k);
  mpfr_ptr F1coeffs = malloc((N+1) * sizeof(mpfr_t));
  mpfr_ptr F2coeffs = malloc((N+1) * sizeof(mpfr_t));
  for (i = 0 ; i <= N ; i++) {
    mpfr_init(F1coeffs + i);
    mpfr_set_si(F1coeffs + i, 0, MPFR_RNDN);
    mpfr_init(F2coeffs + i);
    mpfr_set_si(F2coeffs + i, 0, MPFR_RNDN);
  }

  for (k = 0 ; k <= N ; k++) {

    // compute in G_X_k the value of G(X + (2k+1)*pi/(2*N+2))
    mpfr_set_si(G_X_k, 0, MPFR_RNDN);
    for (j = 0 ; j <= m && j <= N ; j++) {
      mpfr_mul(temp, G->coeffs + j, X + (j * (2*k+1)) % (4*N+4), MPFR_RNDN);
      mpfr_add(G_X_k, G_X_k, temp, MPFR_RNDN);
    }

    // take the square root
    mpfr_sqrt(G_X_k, G_X_k, MPFR_RNDN);

    // use these values to compute the coefficients of F
    for (i = 0 ; i <= N ; i++) {
      mpfr_mul(temp, X + (i * (2*k+1)) % (4*N+4), G_X_k, MPFR_RNDN);
      mpfr_add(F1coeffs + i, F1coeffs + i, temp, MPFR_RNDN);
      mpfr_div(temp, X + (i * (2*k+1)) % (4*N+4), G_X_k, MPFR_RNDN);
      mpfr_add(F2coeffs + i, F2coeffs + i, temp, MPFR_RNDN);
    }

  }

  for (i = 0 ; i <= N ; i++) {
    mpfr_div_si(F1coeffs + i, F1coeffs + i, N+1, MPFR_RNDN);
    mpfr_div_si(F2coeffs + i, F2coeffs + i, N+1, MPFR_RNDN);
    if (i > 0) {
      mpfr_mul_2si(F1coeffs + i, F1coeffs + i, 1, MPFR_RNDN);
      mpfr_mul_2si(F2coeffs + i, F2coeffs + i, 1, MPFR_RNDN);
    }
  }

  // store them into F1 and F2
  mpfr_chebpoly_clear(F1);
  F1->degree = N;
  F1->coeffs = F1coeffs;
  mpfr_chebpoly_normalise(F1);
  mpfr_chebpoly_clear(F2);
  F2->degree = N;
  F2->coeffs = F2coeffs;
  mpfr_chebpoly_normalise(F2);

  // clear variables
  mpfr_clear(temp);
  mpfr_clear(G_X_k);
  for (i = 0 ; i < 4*N+4 ; i++)
    mpfr_clear(X + i);
  free(X);

}



