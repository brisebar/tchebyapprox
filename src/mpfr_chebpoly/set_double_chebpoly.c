
#include "mpfr_chebpoly.h"


// copy Q into P
void mpfr_chebpoly_set_double_chebpoly(mpfr_chebpoly_t P, const double_chebpoly_t Q)
{
  mpfr_chebpoly_set_degree(P, Q->degree);
  _mpfr_chebpoly_set_double_chebpoly(P->coeffs, Q->coeffs, Q->degree);
}
