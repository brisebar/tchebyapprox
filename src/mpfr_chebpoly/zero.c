
#include "mpfr_chebpoly.h"


// set P to 0
void mpfr_chebpoly_zero(mpfr_chebpoly_t P)
{
  long n = P->degree;
  long i ;
  for (i = 0 ; i <= n ; i++)
    mpfr_clear(P->coeffs + i);
  P->coeffs = realloc(P->coeffs, 0);
  P->degree = -1;
}
