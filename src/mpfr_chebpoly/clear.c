#include "mpfr_chebpoly.h"


// clear P
void mpfr_chebpoly_clear(mpfr_chebpoly_t P)
{
  long n = P->degree;
  long i;
  for (i = 0 ; i <= n ; i++)
    mpfr_clear(P->coeffs + i);
  free(P->coeffs);
}
