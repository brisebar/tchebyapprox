
#include "mpfr_chebpoly.h"


int mpfr_chebpoly_equal(const mpfr_chebpoly_t P, const mpfr_chebpoly_t Q)
{
  return (P->degree == Q->degree && _mpfr_chebpoly_equal(P->coeffs, Q->coeffs, P->degree));
}
