
#include "mpfr_chebpoly.h"


void mpfr_chebpoly_get_double_chebpoly(double_chebpoly_t P, const mpfr_chebpoly_t Q)
{
  long n = Q->degree;

  if (n < 0)
    double_chebpoly_zero(P);

  else {
    double_chebpoly_set_degree(P, n);
    _mpfr_chebpoly_get_double_chebpoly(P->coeffs, Q->coeffs, n);
  }
}
