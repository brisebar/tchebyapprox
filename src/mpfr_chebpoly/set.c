
#include "mpfr_chebpoly.h"


// copy Q into P
void mpfr_chebpoly_set(mpfr_chebpoly_t P, const mpfr_chebpoly_t Q)
{
  long n = Q->degree;
  long i;
  if (P != Q) {
    mpfr_chebpoly_set_degree(P, n);
    for (i = 0 ; i <= n ; i++)
      mpfr_set(P->coeffs + i, Q->coeffs + i, MPFR_RNDN);
  }
}
