
#include "mpfr_chebpoly.h"


void _mpfr_chebpoly_get_mpfr_poly(mpfr_ptr P, mpfr_srcptr Q, long n)
{
  mpfr_set(P + 0, Q + 0, MPFR_RNDN); // P = Q_0

  long i;
  mpfr_ptr Ti = malloc((n+2) * sizeof(mpfr_t));
  mpfr_ptr Tip1 = malloc((n+2) * sizeof(mpfr_t));
  mpfr_ptr Temp = malloc((n+2) * sizeof(mpfr_t));
  for (i = 0 ; i <= n+1 ; i++) {
    mpfr_init(Ti + i);
    mpfr_set_si(Ti + i, 0, MPFR_RNDN);
    mpfr_init(Tip1 + i);
    mpfr_set_si(Tip1 + i, 0, MPFR_RNDN);
    mpfr_init(Temp + i);
    mpfr_set_si(Temp + i, 0, MPFR_RNDN);
  }
  mpfr_set_si(Ti, 1, MPFR_RNDN); // T0 = 1
  mpfr_set_si(Tip1 + 1, 1, MPFR_RNDN); // T1 = X
  
  for (i = 0 ; i < n ; i++) {

    // P += Q_{i+1} * T_{i+1}
    _mpfr_poly_scalar_mul_fr(Temp, Tip1, i+1, Q + i + 1);
    mpfr_set_si(P + i + 1, 0, MPFR_RNDN);
    _mpfr_poly_add(P, P, i+1, Temp, i+1);
      
    // (Ti, Tip1) -> (Tip1, 2*X*Tip1-Ti)
    _mpfr_poly_scalar_mul_2si(Temp, Tip1, i+1, 1);
    _mpfr_poly_shift_left(Temp, Temp, i+1, 1);
    _mpfr_poly_sub(Temp, Temp, i+2, Ti, i);
    _mpfr_poly_set(Ti, Tip1, i+1);
    _mpfr_poly_set(Tip1, Temp, i+2);

  }

  // clear variables
  for (i = 0 ; i <= n+1 ; i++) {
    mpfr_clear(Ti + i);
    mpfr_clear(Tip1 + i);
    mpfr_clear(Temp + i);
  }
}


// set in P the conversion of Q (in the Chebyshev basis) into the monomial basis
void mpfr_chebpoly_get_mpfr_poly(mpfr_poly_t P, const mpfr_chebpoly_t Q)
{
  long n = Q->degree;

  if (n < 0)
    mpfr_poly_zero(P);

  else {
    mpfr_poly_set_degree(P, n);
    _mpfr_chebpoly_get_mpfr_poly(P->coeffs, Q->coeffs, n);
  }
}
