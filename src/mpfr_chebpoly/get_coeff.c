
#include "mpfr_chebpoly.h"


// get the n-th coefficient of P
void mpfr_chebpoly_get_coeff(mpfr_t c, const mpfr_chebpoly_t P, long n)
{
  if (n < 0 || n > P->degree)
    mpfr_set_si(c, 0, MPFR_RNDN);
  else
    mpfr_set(c, P->coeffs + n, MPFR_RNDN);
}
