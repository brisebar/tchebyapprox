
#include "mpfr_chebpoly.h"


void mpfr_chebpoly_1norm(mpfr_t y, const mpfr_chebpoly_t P)
{
  _mpfr_chebpoly_1norm(y, P->coeffs, P->degree);
}

