
#include "mpfr_chebpoly.h"


// set P to Q*X^k
void mpfr_chebpoly_shift_left(mpfr_chebpoly_t P, const mpfr_chebpoly_t Q, unsigned long k)
{
  long i;
  long n = Q->degree;
  mpfr_ptr Pcoeffs = malloc((n+k+1) * sizeof(mpfr_t));
  for (i = 0 ; i <= n+k ; i++)
    mpfr_init(Pcoeffs + i);
  _mpfr_chebpoly_shift_left(Pcoeffs, Q->coeffs, n, k);
  mpfr_chebpoly_clear(P);
  P->degree = n+k;
  P->coeffs = Pcoeffs;
}


// set P to Q/X^k
void mpfr_chebpoly_shift_right(mpfr_chebpoly_t P, const mpfr_chebpoly_t Q, unsigned long k)
{
  long i;
  long n = Q->degree;
  if (k > n)
    mpfr_chebpoly_zero(P);
  else {
    mpfr_ptr Pcoeffs = malloc((n-k+1) * sizeof(mpfr_t));
    for (i = 0 ; i <= n-k ; i++)
      mpfr_init(Pcoeffs + i);
    _mpfr_chebpoly_shift_right(Pcoeffs, Q->coeffs, n, k);
    mpfr_chebpoly_clear(P);
    P->degree = n-k;
    P->coeffs = Pcoeffs;
  }
}
