
#include "mpfr_chebpoly.h"


void mpfr_chebpoly_comp(mpfr_chebpoly_t P, const mpfr_chebpoly_t Q, const mpfr_chebpoly_t R)
{
  long n = Q->degree;

  if (n <= 0)
    mpfr_chebpoly_set(P, Q);

  else {
    
    mpfr_chebpoly_t A, B, Temp;
    mpfr_chebpoly_init(A);
    mpfr_chebpoly_init(B);
    mpfr_chebpoly_init(Temp);
    mpfr_chebpoly_set_degree(A, 0);
    mpfr_set(A->coeffs + 0, Q->coeffs + n - 1, MPFR_RNDN);
    mpfr_chebpoly_set_degree(B, 0);
    mpfr_set(B->coeffs + 0, Q->coeffs + n, MPFR_RNDN);
    long i;
    
    for (i = n-2 ; i >= 0 ; i--) {
      // (A,B) -> (P[i]-B, A+2*R*B)
      // new value for A
      mpfr_chebpoly_swap(Temp, A);
      if (B->degree < 0) {
	mpfr_chebpoly_set_degree(A, 0);
	mpfr_set(A->coeffs + 0, Q->coeffs + i, MPFR_RNDN);
      }
      else {
	mpfr_chebpoly_neg(A, B);
	mpfr_add(A->coeffs + 0, A->coeffs + 0, Q->coeffs + i, MPFR_RNDN);
      }
      // new value for B
      mpfr_chebpoly_mul(B, B, R);
      mpfr_chebpoly_scalar_mul_2si(B, B, 1);
      mpfr_chebpoly_add(B, B, Temp);
    }
    
    // P = A+R*B
    mpfr_chebpoly_mul(P, R, B);
    mpfr_chebpoly_add(P, P, A);
    
    // clear variables
    mpfr_chebpoly_clear(Temp);
    mpfr_chebpoly_clear(A);
    mpfr_chebpoly_clear(B);
  } 

}


