
#include "mpfr_chebpoly.h"


// computes F of degree N st F ~ 1/sqrt(G) 
void mpfr_chebpoly_inv_sqrt_approx(mpfr_chebpoly_t F, const mpfr_chebpoly_t G, long N)
{
  long m = G->degree;
  long i, j, k;
  mpfr_t temp;
  mpfr_init(temp);

  // compute the values cos(i*pi/(2*N+2)) for 0 <= i < 4*N+4
  mpfr_ptr X = malloc((4*N+4) * sizeof(mpfr_t));
  for (i = 0 ; i < 4*N+4 ; i++) {
    mpfr_init(X + i);
    mpfr_const_pi(temp, MPFR_RNDN);
    mpfr_mul_si(temp, temp, i, MPFR_RNDN);
    mpfr_div_si(temp, temp, 2*N+2, MPFR_RNDN);
    mpfr_cos(X + i, temp, MPFR_RNDN);
  }

  // compute the coefficients of F
  mpfr_t G_X_k;
  mpfr_init(G_X_k);
  mpfr_ptr Fcoeffs = malloc((N+1) * sizeof(mpfr_t));
  for (i = 0 ; i <= N ; i++) {
    mpfr_init(Fcoeffs + i);
    mpfr_set_si(Fcoeffs + i, 0, MPFR_RNDN);
  }

  for (k = 0 ; k <= N ; k++) {

    // compute in G_X_k the value of G(X + (2k+1)*pi/(2*N+2))
    mpfr_set_si(G_X_k, 0, MPFR_RNDN);
    for (j = 0 ; j <= m && j <= N ; j++) {
      mpfr_mul(temp, G->coeffs + j, X + (j * (2*k+1)) % (4*N+4), MPFR_RNDN);
      mpfr_add(G_X_k, G_X_k, temp, MPFR_RNDN);
    }

    // take the square root
    mpfr_sqrt(G_X_k, G_X_k, MPFR_RNDN);

    // use these values to compute the coefficients of F
    for (i = 0 ; i <= N ; i++) {
      mpfr_div(temp, X + (i * (2*k+1)) % (4*N+4), G_X_k, MPFR_RNDN);
      mpfr_add(Fcoeffs + i, Fcoeffs + i, temp, MPFR_RNDN);
    }

  }

  for (i = 0 ; i <= N ; i++) {
    mpfr_div_si(Fcoeffs + i, Fcoeffs + i, N+1, MPFR_RNDN);
    if (i > 0)
      mpfr_mul_2si(Fcoeffs + i, Fcoeffs + i, 1, MPFR_RNDN);
  }

  // store them into F
  mpfr_chebpoly_clear(F);
  F->degree = N;
  F->coeffs = Fcoeffs;
  mpfr_chebpoly_normalise(F);

  // clear variables
  mpfr_clear(temp);
  mpfr_clear(G_X_k);
  for (i = 0 ; i < 4*N+4 ; i++)
    mpfr_clear(X + i);
  free(X);

}



