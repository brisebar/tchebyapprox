
#include "mpfr_chebpoly.h"


// set P to Q(X^-1)*X^(degree(Q))
void mpfr_chebpoly_reverse(mpfr_chebpoly_t P, const mpfr_chebpoly_t Q)
{
  if (P != Q)
    mpfr_chebpoly_set(P, Q);
  _mpfr_chebpoly_reverse(P->coeffs, P->coeffs, P->degree);
  mpfr_chebpoly_normalise(P);
}
