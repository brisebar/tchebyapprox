
#include "mpfr_chebpoly.h"


// set P := Q^e
void mpfr_chebpoly_pow(mpfr_chebpoly_t P, const mpfr_chebpoly_t Q, unsigned long e)
{
  mpfr_chebpoly_t Qe;
  mpfr_chebpoly_init(Qe);
  mpfr_chebpoly_set_coeff_si(Qe, 0, 1);
  mpfr_chebpoly_t Qi;
  mpfr_chebpoly_init(Qi);
  mpfr_chebpoly_set(Qi, Q);

  while (e != 0) {
    if (e%2 == 1)
      mpfr_chebpoly_mul(Qe, Qe, Qi);
    mpfr_chebpoly_mul(Qi, Qi, Qi);
    e /= 2;
  }

  mpfr_chebpoly_clear(Qi);
  mpfr_chebpoly_clear(P);
  P->degree = Qe->degree;
  P->coeffs = Qe->coeffs;
}
