
#include "mpfr_chebpoly.h"


// set P := -Q
void mpfr_chebpoly_neg(mpfr_chebpoly_t P, const mpfr_chebpoly_t Q)
{
  mpfr_chebpoly_set_degree(P, Q->degree);
  _mpfr_chebpoly_neg(P->coeffs, Q->coeffs, Q->degree);
}
