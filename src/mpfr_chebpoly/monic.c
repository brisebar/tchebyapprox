
#include "mpfr_chebpoly.h"


// set P to (1/c)*Q where c is the leading coefficient of P
void mpfr_chebpoly_monic(mpfr_chebpoly_t P, const mpfr_chebpoly_t Q)
{
  long n = Q->degree;

  if (n < 0)
    mpfr_chebpoly_zero(P);

  else {
    mpfr_chebpoly_set_degree(P, n);
    _mpfr_chebpoly_monic(P->coeffs, Q->coeffs, n);
  }
}
