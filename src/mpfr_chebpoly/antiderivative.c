
#include "mpfr_chebpoly.h"


void _mpfr_chebpoly_antiderivative(mpfr_ptr P, mpfr_srcptr Q, long n)
{
  long i;
  
  mpfr_ptr R = malloc((n+2) * sizeof(mpfr_t));
  for (i = 0 ; i <= n+1 ; i++) {
    mpfr_init(R + i);
    mpfr_set_si(R + i, 0, MPFR_RNDN);
  }
  
  // compute R[1]
  mpfr_mul_2si(R + 1, Q, 1, MPFR_RNDN);
  if (n >= 2)
    mpfr_sub(R + 1, R + 1, Q + 2, MPFR_RNDN);
  mpfr_mul_2si(R + 1, R + 1, -1, MPFR_RNDN);
  
  // compute R[i] for i in [2,n-1]
  for (i = 2 ; i < n ; i++) {
    mpfr_sub(R + i, Q + i - 1, Q + i + 1, MPFR_RNDN);
    mpfr_div_si(R + i, R + i, 2*i, MPFR_RNDN);
  }
  
  // compute R[n] (if n >= 2)
  if (n >= 2)
    mpfr_div_si(R + n, Q + n - 1, 2*n, MPFR_RNDN);
  
  // compute R[n+1] (if n >= 1)
  if (n >= 1)
    mpfr_div_si(R + n + 1, Q + n, 2*(n+1), MPFR_RNDN);
  
  for (i = 0 ; i <= n+1 ; i++) {
    mpfr_swap(P + i, R + i);
    mpfr_clear(R + i);
  }
  free(R);
}


// set P st P' = Q and P(0) = 0
void mpfr_chebpoly_antiderivative(mpfr_chebpoly_t P, const mpfr_chebpoly_t Q)
{
  long n = Q->degree;

  if (n < 0)
    mpfr_chebpoly_zero(P);

  else {
    mpfr_chebpoly_set_degree(P, n+1);
    _mpfr_chebpoly_antiderivative(P->coeffs, Q->coeffs, n);
  }
}


// set P st P' = Q and P(0) = 0
void mpfr_chebpoly_antiderivative0(mpfr_chebpoly_t P, const mpfr_chebpoly_t Q)
{
  mpfr_chebpoly_antiderivative(P, Q);
  long n = P->degree;
  long i;
  for (i = 1 ; 2*i <= n ; i++)
    if (i%2)
      mpfr_add(P->coeffs + 0, P->coeffs + 0, P->coeffs + 2*i, MPFR_RNDN);
    else
      mpfr_sub(P->coeffs + 0, P->coeffs + 0, P->coeffs + 2*i, MPFR_RNDN);
}


// set P st P' = Q and P(-1) = 0
void mpfr_chebpoly_antiderivative_1(mpfr_chebpoly_t P, const mpfr_chebpoly_t Q)
{
  mpfr_chebpoly_antiderivative(P, Q);
  long n = P->degree;
  long i;
  for (i = 1 ; i <= n ; i++)
    if (i%2)
      mpfr_add(P->coeffs + 0, P->coeffs + 0, P->coeffs + i, MPFR_RNDN);
    else
      mpfr_sub(P->coeffs + 0, P->coeffs + 0, P->coeffs + i, MPFR_RNDN);
}


// set P st P' = Q and P(1) = 0
void mpfr_chebpoly_antiderivative1(mpfr_chebpoly_t P, const mpfr_chebpoly_t Q)
{
  mpfr_chebpoly_antiderivative(P, Q);
  long n = P->degree;
  long i;
  for (i = 1 ; i <= n ; i++)
    mpfr_sub(P->coeffs + 0, P->coeffs + 0 , P->coeffs + i, MPFR_RNDN);
}
