
#include "mpfr_chebpoly.h"


void _mpfr_chebpoly_interpolate(mpfr_ptr P, mpfr_srcptr F, long n)
{
  long i, j, k;
  mpfr_t temp;
  mpfr_init(temp);

  // X[j] = cos(pi*j/(2n+2)) for 0 <= j < 4n+4 
  mpfr_ptr X = malloc((4*n+4) * sizeof(mpfr_t));
  for (j = 0 ; j < 4*n+4 ; j++) {
    mpfr_init(X + j);
    mpfr_const_pi(X + j, MPFR_RNDN);
    mpfr_mul_si(X + j, X + j, j, MPFR_RNDN);
    mpfr_div_si(X + j, X + j, 2*n+2, MPFR_RNDN);
    mpfr_cos(X + j, X + j, MPFR_RNDN);
  }

  // FF is a copy of F
  mpfr_ptr FF = malloc((n+1) * sizeof(mpfr_t));
  for (k = 0 ; k <= n ; k++) {
    mpfr_init(FF + k);
    mpfr_set(FF + k, F + k, MPFR_RNDN);
  }

  // P[i] = (2* if i>0) 1/(n+1) sum_{0 <= k <= n} F[k] X[(2k+1)i mod (4n+4)]
  for (i = 0 ; i <= n ; i++) {
    mpfr_set_zero(P + i, 1);
    for (k = 0 ; k <= n ; k++) {
      mpfr_mul(temp, FF + k, X + (((2*k+1)*i) % (4*n+4)), MPFR_RNDN);
      mpfr_add(P + i, P + i, temp, MPFR_RNDN);
    }
    mpfr_div_si(P + i, P + i, n+1, MPFR_RNDN);
    if (i > 0)
      mpfr_mul_2si(P + i, P + i, 1, MPFR_RNDN);
  }

  mpfr_clear(temp);
  for (j = 0 ; j < 4*n+4 ; j++)
    mpfr_clear(X + j);
  free(X);
  for (k = 0 ; k <= n ; k++)
    mpfr_clear(FF + k);
  free(FF);
}


void mpfr_chebpoly_interpolate(mpfr_chebpoly_t P, const mpfr_vec_t F)
{
  long n = F->length;
  
  if (n == 0)
    mpfr_chebpoly_zero(P);

  else {
    mpfr_chebpoly_set_degree(P, n-1);
    _mpfr_chebpoly_interpolate(P->coeffs, F->coeffs, n-1);
  } 
}

