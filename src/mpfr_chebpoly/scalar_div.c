
#include "mpfr_chebpoly.h"


// set P := (1/c)*Q
void mpfr_chebpoly_scalar_div_si(mpfr_chebpoly_t P, const mpfr_chebpoly_t Q, long c)
{
  long n = Q->degree;
  mpfr_chebpoly_set_degree(P, n);
  _mpfr_chebpoly_scalar_div_si(P->coeffs, Q->coeffs, n, c);
}


// set P := (1/c)*Q
void mpfr_chebpoly_scalar_div_z(mpfr_chebpoly_t P, const mpfr_chebpoly_t Q, const mpz_t c)
{
  long n = Q->degree;
  mpfr_chebpoly_set_degree(P, n);
  _mpfr_chebpoly_scalar_div_z(P->coeffs, Q->coeffs, n, c);
}


// set P := (1/c)*Q
void mpfr_chebpoly_scalar_div_q(mpfr_chebpoly_t P, const mpfr_chebpoly_t Q, const mpq_t c)
{
  long n = Q->degree;
  mpfr_chebpoly_set_degree(P, n);
  _mpfr_chebpoly_scalar_div_q(P->coeffs, Q->coeffs, n, c);
}


// set P := (1/c)*Q
void mpfr_chebpoly_scalar_div_d(mpfr_chebpoly_t P, const mpfr_chebpoly_t Q, const double_t c)
{
  long n = Q->degree;
  mpfr_chebpoly_set_degree(P, n);
  _mpfr_chebpoly_scalar_div_d(P->coeffs, Q->coeffs, n, c);
}


// set P := (1/c)*Q
void mpfr_chebpoly_scalar_div_fr(mpfr_chebpoly_t P, const mpfr_chebpoly_t Q, const mpfr_t c)
{
  long n = Q->degree;
  mpfr_chebpoly_set_degree(P, n);
  _mpfr_chebpoly_scalar_div_fr(P->coeffs, Q->coeffs, n, c);
}


// set P := 2^-k*Q
void mpfr_chebpoly_scalar_div_2si(mpfr_chebpoly_t P, const mpfr_chebpoly_t Q, long k)
{
  long n = Q->degree;
  mpfr_chebpoly_set_degree(P, n);
  _mpfr_chebpoly_scalar_div_2si(P->coeffs, Q->coeffs, n, k);
}
