
#include "mpfr_chebpoly.h"


// set n as degree for P
void mpfr_chebpoly_set_degree(mpfr_chebpoly_t P, long n)
{
  if (n < -1)
    n = -1;
  long m = P->degree;
  long i;

  if (n < m) {
    for (i = n+1 ; i <= m ; i++)
      mpfr_clear(P->coeffs + i);
    P->coeffs = realloc(P->coeffs, (n+1) * sizeof(mpfr_t));
  }
  else if (n > m) {
    P->coeffs = realloc(P->coeffs, (n+1) * sizeof(mpfr_t));
    for (i = m+1 ; i <= n ; i++) {
      mpfr_init(P->coeffs + i);
      mpfr_set_si(P->coeffs + i, 0, MPFR_RNDN);
    }
  }

  P->degree = n;
}
