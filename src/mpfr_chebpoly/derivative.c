
#include "mpfr_chebpoly.h"


void _mpfr_chebpoly_derivative(mpfr_ptr P, mpfr_srcptr Q, long n)
{
  long i;

  mpfr_ptr R = malloc(n * sizeof(mpfr_t));
  for (i = 0 ; i < n ; i++) {
    mpfr_init(R + i);
    mpfr_set_si(R + i, 0, MPFR_RNDN);
  }
  
  for (i = n-1 ; i >= 0 ; i--) {
    mpfr_mul_si(R + i, Q + i + 1, 2*(i+1), MPFR_RNDN);
    if (i < n-2)
      mpfr_add(R + i, R + i, R + i + 2, MPFR_RNDN);
  }
  
  mpfr_mul_2si(R, R, -1, MPFR_RNDN);

  for (i = 0 ; i < n ; i++) {
    mpfr_swap(P + i, R + i);
    mpfr_clear(R + i);
  }
  
  free(R);
}


// set P := Q'
void mpfr_chebpoly_derivative(mpfr_chebpoly_t P, const mpfr_chebpoly_t Q)
{
  long n = Q->degree;

  if (n <= 0)
    mpfr_chebpoly_zero(P);

  else {
    mpfr_chebpoly_set_degree(P, n);
    _mpfr_chebpoly_derivative(P->coeffs, Q->coeffs, n);
    mpfr_clear(P->coeffs + n);
    P->degree --;
  }
}
