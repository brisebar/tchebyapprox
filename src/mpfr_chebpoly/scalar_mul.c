
#include "mpfr_chebpoly.h"


// set P := c*Q
void mpfr_chebpoly_scalar_mul_si(mpfr_chebpoly_t P, const mpfr_chebpoly_t Q, long c)
{
  if (c == 0)
    mpfr_chebpoly_zero(P);
  else {
    long n = Q->degree;
    mpfr_chebpoly_set_degree(P, n);
    _mpfr_chebpoly_scalar_mul_si(P->coeffs, Q->coeffs, n, c);
  }
}


// set P := c*Q
void mpfr_chebpoly_scalar_mul_z(mpfr_chebpoly_t P, const mpfr_chebpoly_t Q, const mpz_t c)
{
  if (!mpz_sgn(c))
    mpfr_chebpoly_zero(P);
  else {
    long n = Q->degree;
    mpfr_chebpoly_set_degree(P, n);
    _mpfr_chebpoly_scalar_mul_z(P->coeffs, Q->coeffs, n, c);
  }
}


// set P := c*Q
void mpfr_chebpoly_scalar_mul_q(mpfr_chebpoly_t P, const mpfr_chebpoly_t Q, const mpq_t c)
{
  if (!mpq_sgn(c))
    mpfr_chebpoly_zero(P);
  else {
    long n = Q->degree;
    mpfr_chebpoly_set_degree(P, n);
    _mpfr_chebpoly_scalar_mul_q(P->coeffs, Q->coeffs, n, c);
  }
}


// set P := c*Q
void mpfr_chebpoly_scalar_mul_d(mpfr_chebpoly_t P, const mpfr_chebpoly_t Q, const double_t c)
{
  if (*c == 0)
    mpfr_chebpoly_zero(P);
  else {
    long n = Q->degree;
    mpfr_chebpoly_set_degree(P, n);
    _mpfr_chebpoly_scalar_mul_d(P->coeffs, Q->coeffs, n, c);
  }
}


// set P := c*Q
void mpfr_chebpoly_scalar_mul_fr(mpfr_chebpoly_t P, const mpfr_chebpoly_t Q, const mpfr_t c)
{
  if (mpfr_zero_p(c))
    mpfr_chebpoly_zero(P);
  else {
    long n = Q->degree;
    mpfr_chebpoly_set_degree(P, n);
    _mpfr_chebpoly_scalar_mul_fr(P->coeffs, Q->coeffs, n, c);
  }
}


// set P := 2^k*Q
void mpfr_chebpoly_scalar_mul_2si(mpfr_chebpoly_t P, const mpfr_chebpoly_t Q, long k)
{
  long n = Q->degree;

  if (n < 0)
    mpfr_chebpoly_zero(P);

  else {
    mpfr_chebpoly_set_degree(P, n);
    _mpfr_chebpoly_scalar_mul_2si(P->coeffs, Q->coeffs, n, k);
  }
}
