
#include "mpfr_chebpoly.h"


// set P := Q + R
void mpfr_chebpoly_sub(mpfr_chebpoly_t P, const mpfr_chebpoly_t Q, const mpfr_chebpoly_t R)
{
  long n = Q->degree;
  long m = R->degree;
  long max_n_m = n < m ? m : n;

  mpfr_chebpoly_set_degree(P, max_n_m);
  _mpfr_chebpoly_sub(P->coeffs, Q->coeffs, n, R->coeffs, m);
  mpfr_chebpoly_normalise(P);
}
