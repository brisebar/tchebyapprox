
#include "mpfi_bandmatrix.h"


void mpfi_bandmatrix_set_params(mpfi_bandmatrix_t M, long dim, long Hwidth, long Dwidth)
{
  mpfi_bandmatrix_clear(M);

  M->dim = dim;
  M->Hwidth = Hwidth;
  M->Dwidth = Dwidth;

  long i, j;

  M->H = malloc(Hwidth * sizeof(mpfi_ptr));
  for (j = 0 ; j < Hwidth ; j++) {
    M->H[j] = malloc(dim * sizeof(mpfi_t));
    for (i = 0 ; i < dim ; i++) {
      mpfi_init(M->H[j] + i);
      mpfi_set_si(M->H[j] + i, 0);
    }
  }

  M->D = malloc(dim * sizeof(mpfi_ptr));
  for (i = 0 ; i < dim ; i++) {
    M->D[i] = malloc((2*Dwidth+1) * sizeof(mpfi_t));
    for (j = 0 ; j <= 2*Dwidth ; j++) {
      mpfi_init(M->D[i] + j);
      mpfi_set_si(M->D[i] + j, 0);
    }
  }

}
