
#include "mpfi_bandmatrix.h"


void mpfi_bandmatrix_normalise(mpfi_bandmatrix_t M)
{
  long n = M->dim;
  long r = M->Hwidth;
  long s = M->Dwidth;
  long i, j;

  for (i = 0 ; i < r ; i++) {

    long jmin = i-s < 0 ? 0 : i-s;
    long jmax = i+s < n ? i+s : n-1;

    for (j = jmin ; j <= jmax ; j++) {
      mpfi_add(M->D[i] + s-i+j, M->D[i] + s-i+j, M->H[i] + j);
      mpfi_set_si(M->H[i] + j, 0);
    }

  }
}
