
#include "mpfi_bandmatrix.h"


void mpfi_bandmatrix_neg(mpfi_bandmatrix_t M, const mpfi_bandmatrix_t N)
{
  long n = N->dim;
  long r = N->Hwidth;
  long s = N->Dwidth;
  long i, j;

  if (M != N)
    mpfi_bandmatrix_set_params(M, n, r, s);

  for (i = 0 ; i < r ; i++)
    for (j = 0 ; j < n ; j++)
      mpfi_neg(M->H[i] + j, N->H[i] + j);

  for (i = 0 ; i < n ; i++)
    for (j = 0 ; j <= 2*s ; j++)
      mpfi_neg(M->D[i] + j, N->D[i] + j);
}
