
#include "mpfi_bandmatrix.h"


void mpfi_bandmatrix_set_double_bandmatrix(mpfi_bandmatrix_t M, const double_bandmatrix_t N)
{
  long n = N->dim;
  long r = N->Hwidth;
  long s = N->Dwidth;
  long i, j;

  mpfi_bandmatrix_set_params(M, n, r, s);

  for (i = 0 ; i < n ; i++) {

    for (j = 0 ; j < r ; j++)
      mpfi_set_d(M->H[j] + i, N->H[j][i]);

    for (j = 0 ; j <= 2*s ; j++)
      mpfi_set_d(M->D[i] + j, N->D[i][j]);

  }
}

void mpfi_bandmatrix_set_mpfr_bandmatrix(mpfi_bandmatrix_t M, const mpfr_bandmatrix_t N)
{
  long n = N->dim;
  long r = N->Hwidth;
  long s = N->Dwidth;
  long i, j;

  mpfi_bandmatrix_set_params(M, n, r, s);

  for (i = 0 ; i < n ; i++) {

    for (j = 0 ; j < r ; j++)
      mpfi_set_fr(M->H[j] + i, N->H[j] + i);

    for (j = 0 ; j <= 2*s ; j++)
      mpfi_set_fr(M->D[i] + j, N->D[i] + j);

  }
}
