
#include "mpfi_bandmatrix.h"


// all mpfi_ptr must be distinct !
void _mpfi_bandmatrix_evaluate_band_fr(mpfi_ptr VH, mpfi_ptr VD, long VHwidth, long VDwidth, const mpfi_bandmatrix_t M, mpfr_srcptr WH, mpfr_srcptr WD, long WHwidth, long WDwidth, long ind)
{
  long n = M->dim;
  long r = M->Hwidth;
  long s = M->Dwidth;

  long R = r < s + WHwidth ? s + WHwidth : r;
  long S = s + WDwidth;

  long j, jmin, jmax, k, kmin, kmax;

  if (VHwidth < R)
    fprintf(stderr, "_mpfi_bandmatrix_evaluate_band_fr: error: Hwidth of result too small\n");

  else if (VDwidth < S)
    fprintf(stderr, "_mpfi_bandmatrix_evaluate_band_fr: error: Dwidth of result too small\n");

  else {

    mpfi_t temp;
    mpfi_init(temp);

    _mpfi_bandvec_zero(VH, VD, VHwidth, VDwidth);

    // image of WH (first part)
    jmax = WHwidth < ind - WDwidth ? WHwidth : ind - WDwidth;
    jmax = jmax < n ? jmax : n;
    for (j = 0 ; j < jmax ; j++) {

      kmax = r < j-s ? r : j-s;
      kmax = kmax < n ? kmax : n;
      for (k = 0 ; k < kmax ; k++) {
	mpfi_mul_fr(temp, M->H[k] + j, WH + j);
	mpfi_add(VH + k, VH + k, temp);
      }

      kmin = j-s < 0 ? 0 : j-s;
      kmax = j+s < n ? j+s : n-1;
      for (k = kmin ; k <= kmax ; k++) {
	mpfi_mul_fr(temp, M->D[k] + s + j - k, WH + j);
	mpfi_add(VH + k, VH + k, temp);
      }

      kmax = r < n ? r : n;
      for (k = j+s+1 ; k < kmax ; k++) {
	mpfi_mul_fr(temp, M->H[k] + j, WH + j);
	mpfi_add(VH + k, VH + k, temp);
      }

    }

    // image of WD
    jmin = ind - WDwidth < 0 ? 0 : ind - WDwidth;
    jmax = ind + WDwidth < n ? ind + WDwidth : n-1;
    for (j = jmin ; j <= jmax ; j++) {

      kmax = r < j-s ? r : j-s;
      kmax = kmax < n ? kmax : n;
      for (k = 0 ; k < kmax ; k++) {
	mpfi_mul_fr(temp, M->H[k] + j, WD + WDwidth - ind + j);
	mpfi_add(VH + k, VH + k, temp);
      }

      kmin = j-s < 0 ? 0 : j-s;
      kmax = j+s < n ? j+s : n-1;
      for (k = kmin ; k <= kmax ; k++) {
	mpfi_mul_fr(temp, M->D[k] + s + j - k, WD + WDwidth - ind + j);
	mpfi_add(VD + VDwidth - ind + k, VD + VDwidth - ind + k, temp);
      }

      kmax = r < n ? r : n;
      for (k = j+s+1 ; k < kmax ; k++) {
	mpfi_mul_fr(temp, M->H[k] + j, WD + WDwidth - ind + j);
	mpfi_add(VH + k, VH + k, temp);
      }

    }

    // image of WH (second part)
    jmax = WHwidth < n ? WHwidth : n;
    for (j = ind + WDwidth + 1 ; j < jmax ; j++) {

      kmax = r < j-s ? r : j-s;
      kmax = kmax < n ? kmax : n;
      for (k = 0 ; k < kmax ; k++) {
	mpfi_mul_fr(temp, M->H[k] + j, WH + j);
	mpfi_add(VH + k, VH + k, temp);
      }

      kmin = j-s < 0 ? 0 : j-s;
      kmax = j+s < n ? j+s : n-1;
      for (k = kmin ; k <= kmax ; k++) {
	mpfi_mul_fr(temp, M->D[k] + s + j - k, WH + j);
	mpfi_add(VH + k, VH + k, temp);
      }

      kmax = r < n ? r : n;
      for (k = j+s+1 ; k < kmax ; k++) {
	mpfi_mul_fr(temp, M->H[k] + j, WH + j);
	mpfi_add(VH + k, VH + k, temp);
      }

    }

    _mpfi_bandvec_normalise(VH, VD, VHwidth, VDwidth, ind);

    mpfi_clear(temp);

  }
}


void mpfi_bandmatrix_evaluate_band_fr(mpfi_bandvec_t V, const mpfi_bandmatrix_t M, const mpfr_bandvec_t W)
{
  long n = M->dim;
  long r = M->Hwidth;
  long s = M->Dwidth;

  long R = r < s + W->Hwidth ? s + W->Hwidth : r;
  long S = s + W->Dwidth;

  mpfi_bandvec_t Z;
  mpfi_bandvec_init(Z);
  mpfi_bandvec_set_params(Z, R, S, W->ind);

  _mpfi_bandmatrix_evaluate_band_fr(Z->H, Z->D, R, S, M, W->H, W->D, W->Hwidth, W->Dwidth, W->ind);

  mpfi_bandvec_swap(V, Z);
  mpfi_bandvec_clear(Z);
}


// all mpfi_ptr must be distinct !
void _mpfi_bandmatrix_evaluate_band_fi(mpfi_ptr VH, mpfi_ptr VD, long VHwidth, long VDwidth, const mpfi_bandmatrix_t M, mpfi_srcptr WH, mpfi_srcptr WD, long WHwidth, long WDwidth, long ind)
{
  long n = M->dim;
  long r = M->Hwidth;
  long s = M->Dwidth;

  long R = r < s + WHwidth ? s + WHwidth : r;
  long S = s + WDwidth;

  long j, jmin, jmax, k, kmin, kmax;

  if (VHwidth < R)
    fprintf(stderr, "_mpfi_bandmatrix_evaluate_band_fi: error: Hwidth of result too small\n");

  else if (VDwidth < S)
    fprintf(stderr, "_mpfi_bandmatrix_evaluate_band_fi: error: Dwidth of result too small\n");

  else {

    mpfi_t temp;
    mpfi_init(temp);

    _mpfi_bandvec_zero(VH, VD, VHwidth, VDwidth);

    // image of WH (first part)
    jmax = WHwidth < ind - WDwidth ? WHwidth : ind - WDwidth;
    jmax = jmax < n ? jmax : n;
    for (j = 0 ; j < jmax ; j++) {

      kmax = r < j-s ? r : j-s;
      kmax = kmax < n ? kmax : n;
      for (k = 0 ; k < kmax ; k++) {
	mpfi_mul(temp, WH + j, M->H[k] + j);
	mpfi_add(VH + k, VH + k, temp);
      }

      kmin = j-s < 0 ? 0 : j-s;
      kmax = j+s < n ? j+s : n-1;
      for (k = kmin ; k <= kmax ; k++) {
	mpfi_mul(temp, WH + j, M->D[k] + s + j - k);
	mpfi_add(VH + k, VH + k, temp);
      }

      kmax = r < n ? r : n;
      for (k = j+s+1 ; k < kmax ; k++) {
	mpfi_mul(temp, WH + j, M->H[k] + j);
	mpfi_add(VH + k, VH + k, temp);
      }

    }

    // image of WD
    jmin = ind - WDwidth < 0 ? 0 : ind - WDwidth;
    jmax = ind + WDwidth < n ? ind + WDwidth : n-1;
    for (j = jmin ; j <= jmax ; j++) {

      kmax = r < j-s ? r : j-s;
      kmax = kmax < n ? kmax : n;
      for (k = 0 ; k < kmax ; k++) {
	mpfi_mul(temp, WD + WDwidth - ind + j, M->H[k] + j);
	mpfi_add(VH + k, VH + k, temp);
      }

      kmin = j-s < 0 ? 0 : j-s;
      kmax = j+s < n ? j+s : n-1;
      for (k = kmin ; k <= kmax ; k++) {
	mpfi_mul(temp, WD + WDwidth - ind + j, M->D[k] + s + j - k);
	mpfi_add(VD + VDwidth - ind + k, VD + VDwidth - ind + k, temp);
      }

      kmax = r < n ? r : n;
      for (k = j+s+1 ; k < kmax ; k++) {
	mpfi_mul(temp, WD + WDwidth - ind + j, M->H[k] + j);
	mpfi_add(VH + k, VH + k, temp);
      }

    }

    // image of WH (second part)
    jmax = WHwidth < n ? WHwidth : n;
    for (j = ind + WDwidth + 1 ; j < jmax ; j++) {

      kmax = r < j-s ? r : j-s;
      kmax = kmax < n ? kmax : n;
      for (k = 0 ; k < kmax ; k++) {
	mpfi_mul(temp, WH + j, M->H[k] + j);
	mpfi_add(VH + k, VH + k, temp);
      }

      kmin = j-s < 0 ? 0 : j-s;
      kmax = j+s < n ? j+s : n-1;
      for (k = kmin ; k <= kmax ; k++) {
	mpfi_mul(temp, WH + j, M->D[k] + s + j - k);
	mpfi_add(VH + k, VH + k, temp);
      }

      kmax = r < n ? r : n;
      for (k = j+s+1 ; k < kmax ; k++) {
	mpfi_mul(temp, WH + j, M->H[k] + j);
	mpfi_add(VH + k, VH + k, temp);
      }

    }

    _mpfi_bandvec_normalise(VH, VD, VHwidth, VDwidth, ind);

    mpfi_clear(temp);

  }
}


void mpfi_bandmatrix_evaluate_band_fi(mpfi_bandvec_t V, const mpfi_bandmatrix_t M, const mpfi_bandvec_t W)
{
  long n = M->dim;
  long r = M->Hwidth;
  long s = M->Dwidth;

  long R = r < s + W->Hwidth ? s + W->Hwidth : r;
  long S = s + W->Dwidth;

  mpfi_bandvec_t Z;
  mpfi_bandvec_init(Z);
  mpfi_bandvec_set_params(Z, R, S, W->ind);

  _mpfi_bandmatrix_evaluate_band_fi(Z->H, Z->D, R, S, M, W->H, W->D, W->Hwidth, W->Dwidth, W->ind);

  mpfi_bandvec_swap(Z, V);
  mpfi_bandvec_clear(Z);
}

