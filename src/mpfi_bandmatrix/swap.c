
#include "mpfi_bandmatrix.h"


void mpfi_bandmatrix_swap(mpfi_bandmatrix_t M, mpfi_bandmatrix_t N)
{
  long long_buf;
  mpfi_ptr_ptr mpfi_ptr_ptr_buf;

  long_buf = M->dim;
  M->dim = N->dim;
  N->dim = long_buf;

  long_buf = M->Hwidth;
  M->Hwidth = N->Hwidth;
  N->Hwidth = long_buf;

  long_buf = M->Dwidth;
  M->Dwidth = N->Dwidth;
  N->Dwidth = long_buf;

  mpfi_ptr_ptr_buf = M->H;
  M->H = N->H;
  N->H = mpfi_ptr_ptr_buf;

  mpfi_ptr_ptr_buf = M->D;
  M->D = N->D;
  N->D = mpfi_ptr_ptr_buf;
}
