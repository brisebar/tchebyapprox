
#include "mpfi_bandmatrix.h"


void mpfi_bandmatrix_get_double_bandmatrix(double_bandmatrix_t N, const mpfi_bandmatrix_t M)
{
  long n = M->dim;
  long r = M->Hwidth;
  long s = M->Dwidth;
  long i, j;
  mpfr_t temp;
  mpfr_init(temp);

  double_bandmatrix_set_params(N, n, r, s);

  for (i = 0 ; i < n ; i++) {

    for (j = 0 ; j < r ; j++) {
      mpfi_mid(temp, M->H[j] + i);
      double_set_fr(N->H[j] + i, temp, MPFR_RNDN);
    }

    for (j = 0 ; j <= 2*s ; j++) {
      mpfi_mid(temp, M->D[i] + j);
      double_set_fr(N->D[i] + j, temp, MPFR_RNDN);
    }
  
  }

  mpfr_clear(temp);
}


void mpfi_bandmatrix_get_mpfr_bandmatrix(mpfr_bandmatrix_t N, const mpfi_bandmatrix_t M)
{
  long n = M->dim;
  long r = M->Hwidth;
  long s = M->Dwidth;
  long i, j;

  mpfr_bandmatrix_set_params(N, n, r, s);

  for (i = 0 ; i < n ; i++) {

    for (j = 0 ; j < r ; j++)
      mpfi_mid(N->H[j] + i, M->H[j] + i);
    
    for (j = 0 ; j <= 2*s ; j++)
      mpfi_mid(N->D[i] + j, M->D[i] + j);
  
  }
}
