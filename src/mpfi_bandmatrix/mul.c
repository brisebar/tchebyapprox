
#include "mpfi_bandmatrix.h"


void mpfi_bandmatrix_mul(mpfi_bandmatrix_t M, const mpfi_bandmatrix_t N, const mpfi_bandmatrix_t P)
{
  long n = N->dim;

  if (n != P->dim)
    fprintf(stderr, "mpfi_bandmatrix_mul: error: operands 1 and 2 have different dimensions (%ld != %ld)\n", n, P->dim);

  else {

    long r1 = N->Hwidth;
    long s1 = N->Dwidth;
    long r2 = P->Hwidth;
    long s2 = P->Dwidth;
    long i, j;

    // if R or S > n, resize at the end -- experimental
    long R = r1 < s1 + r2 ? s1 + r2 : r1;
    long S = s1 + s2;

    mpfi_bandmatrix_t Q;
    mpfi_bandmatrix_init(Q);
    mpfi_bandmatrix_set_params(Q, n, R, S);
    mpfi_bandmatrix_zero(Q);

    mpfi_bandvec_t V;
    mpfi_bandvec_init(V);

    for (i = 0 ; i < n ; i++) {

      mpfi_bandmatrix_get_column(V, P, i);

      mpfi_bandmatrix_evaluate_band_fi(V, N, V);

      mpfi_bandmatrix_set_column(Q, V);

    }

    // experimental!!
   /*
    // diagonal band too large
    if (S >= n) {
      for (i = 0 ; i < n ; i++) {
        for (j = 0 ; j <= 2*n-2 ; j++)
          mpfi_swap(Q->D[i] + j, Q->D[i] + i + S-n+1);
        for (j = 2*n-1 ; j <= 2*S ; j++)
       	  mpfi_clear(Q->D[i] + j);
        Q->D[i] = realloc(Q->D[i], (2*n-1) * sizeof(mpfi_t));
      }
      Q->Dwidth = n-1;
    }

    // horizontal band too large
    if (R > n) {
      for (i = n ; i < R ; i++) {
        for (j = 0 ; j < n ; j++)
	  mpfi_clear(Q->H[i] + j);
	free(Q->H[i]);
      }
      Q->H = realloc(Q->H, n * sizeof(mpfi_ptr));
      Q->Hwidth = n-1;
    }
   */

    mpfi_bandmatrix_swap(Q, M);
    mpfi_bandmatrix_clear(Q);

  }
}


