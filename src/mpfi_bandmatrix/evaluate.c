
#include "mpfi_bandmatrix.h"


void _mpfi_bandmatrix_evaluate_fr(mpfi_ptr V, const mpfi_bandmatrix_t M, mpfr_srcptr W)
{
  long n = M->dim;
  long r = M->Hwidth;
  long s = M->Dwidth;
  long i, j, jmin, jmax;
 
  mpfi_t temp;
  mpfi_init(temp);
 
  mpfi_ptr Z = malloc(n * sizeof(mpfi_t));
  for (i = 0 ; i < n ; i++) {
    mpfi_init(Z + i);
    mpfi_set_si(Z + i, 0);
  }

  for (i = 0 ; i < n ; i++) {

    jmax = i-s < r ? i-s : r;
    jmax = jmax < n ? jmax : n;
    for (j = 0 ; j < jmax ; j++) {
      mpfi_mul_fr(temp, M->H[j] + i, W + i);
      mpfi_add(Z + j, Z + j, temp);
    }

    jmin = i-s < 0 ? 0 : i-s;
    jmax = i+s < n ? i+s : n-1;
    for (j = jmin ; j <= jmax ; j++) {
      mpfi_mul_fr(temp, M->D[j] + s + i - j, W + i);
      mpfi_add(Z + j, Z + j, temp);
    }

    jmax = r < n ? r : n;
    for (j = i+s+1 ; j < jmax ; j++) {
      mpfi_mul_fr(temp, M->H[j] + i, W + i);
      mpfi_add(Z + j, Z + j, temp);
    }

  }

  for (i = 0 ; i < n ; i++) {
    mpfi_swap(V + i, Z + i);
    mpfi_clear(Z + i);
  }
  free(Z);
  mpfi_clear(temp);
}


void mpfi_bandmatrix_evaluate_fr(mpfi_vec_t V, const mpfi_bandmatrix_t M, const mpfr_vec_t W)
{
  long n = M->dim;

  if (n != W->length)
    fprintf(stderr, "mpfi_bandmatrix_evaluate_fr: error: matrix and vector have uncompatible dimensions\n");

  else {
    mpfi_vec_set_length(V, n);
    _mpfi_bandmatrix_evaluate_fr(V->coeffs, M, W->coeffs);
  }
}


void _mpfi_bandmatrix_evaluate_fi(mpfi_ptr V, const mpfi_bandmatrix_t M, mpfi_srcptr W)
{
  long n = M->dim;
  long r = M->Hwidth;
  long s = M->Dwidth;
  long i, j, jmin, jmax;
 
  mpfi_t temp;
  mpfi_init(temp);
 
  mpfi_ptr Z = malloc(n * sizeof(mpfi_t));
  for (i = 0 ; i < n ; i++) {
    mpfi_init(Z + i);
    mpfi_set_si(Z + i, 0);
  }

  for (i = 0 ; i < n ; i++) {

    jmax = i-s < r ? i-s : r;
    jmax = jmax < n ? jmax : n;
    for (j = 0 ; j < jmax ; j++) {
      mpfi_mul(temp, W + i, M->H[j] + i);
      mpfi_add(Z + j, Z + j, temp);
    }

    jmin = i-s < 0 ? 0 : i-s;
    jmax = i+s < n ? i+s : n-1;
    for (j = jmin ; j <= jmax ; j++) {
      mpfi_mul(temp, W + i, M->D[j] + s + i - j);
      mpfi_add(Z + j, Z + j, temp);
    }

    jmax = r < n ? r : n;
    for (j = i+s+1 ; j < jmax ; j++) {
      mpfi_mul(temp, W + i, M->H[j] + i);
      mpfi_add(Z + j, Z + j, temp);
    }

  }

  for (i = 0 ; i < n ; i++) {
    mpfi_swap(V + i, Z + i);
    mpfi_clear(Z + i);
  }
  free(Z);
  mpfi_clear(temp);
}


void mpfi_bandmatrix_evaluate_fi(mpfi_vec_t V, const mpfi_bandmatrix_t M, const mpfi_vec_t W)
{
  long n = M->dim;

  if (n != W->length)
    fprintf(stderr, "mpfi_bandmatrix_evaluate_fi: error: matrix and vector have uncompatible dimensions\n");

  else {
    mpfi_vec_set_length(V, n);
    _mpfi_bandmatrix_evaluate_fi(V->coeffs, M, W->coeffs);
  }
}



