
#include "mpfi_bandmatrix.h"


void mpfi_bandmatrix_add(mpfi_bandmatrix_t M, const mpfi_bandmatrix_t N, const mpfi_bandmatrix_t P)
{
  long n = N->dim;
 
  if (n != P->dim) 
    fprintf(stderr, "mpfi_bandmatrix_add: error: operands 1 and 2 have different dimensions.\n");

  else {

    long r1 = N->Hwidth;
    long s1 = N->Dwidth;
    long r2 = P->Hwidth;
    long s2 = P->Dwidth;
    long i, j;

    long R = r1 < r2 ? r2 : r1;
    long S = s1 < s2 ? s2 : s1;

    mpfi_bandmatrix_t Q;
    mpfi_bandmatrix_init(Q);
    mpfi_bandmatrix_set_params(Q, n, R, S);
    mpfi_bandmatrix_zero(Q);

    // horizontal band
    for (i = 0 ; i < r1 ; i++)
      for (j = 0 ; j < n ; j++)
	mpfi_set(Q->H[i] + j, N->H[i] + j);
    for (i = 0 ; i < r2 ; i++)
      for (j = 0 ; j < n ; j++)
	mpfi_add(Q->H[i] + j, Q->H[i] + j, P->H[i] + j);

    // diagonal band
    for (i = 0 ; i < n ; i++) {
      for (j = -s1 ; j <= s1 ; j++)
	mpfi_set(Q->D[i] + S - j, N->D[i] + s1 - j);
      for (j = -s2 ; j <= s2 ; j++)
	mpfi_add(Q->D[i] + S - j, Q->D[i] + S - j, P->D[i] + s2 - j);
    }

    mpfi_bandmatrix_normalise(Q);

    mpfi_bandmatrix_swap(M, Q);
    mpfi_bandmatrix_clear(Q);
  }
}
