
#include "mpfi_bandmatrix.h"


void mpfr_bandmatrix_QRdecomp_inverse_certif(mpfr_t bound, const mpfi_bandmatrix_t M, const mpfr_bandmatrix_QRdecomp_t N)
{
  long n = M->dim;
  long r = M->Hwidth;
  long s = N->Dwidth;
  long i, j, k, l, max_index1, max_index2, max_index3;
  mpfr_t temp_fr, temp2_fr;
  mpfr_init(temp_fr);
  mpfr_init(temp2_fr);
  mpfi_t temp_fi, temp2_fi;
  mpfi_init(temp_fi);
  mpfi_init(temp2_fi);
  mpfi_ptr row_buf;

  // copy M into (D, B)
  mpfi_ptr_ptr D = malloc(n * sizeof(mpfi_ptr));
  mpfi_ptr_ptr B = malloc(n * sizeof(mpfi_ptr));
  for (i = 0 ; i < n ; i++) {
    D[i] = malloc((2*s+1) * sizeof(mpfi_t));
    for (j = 0 ; j <= 2*s ; j++) {
      mpfi_init(D[i] + j);
      mpfi_set(D[i] + j, M->D[i] + j);
    }
    B[i] = malloc(r * sizeof(mpfi_t));
    for (l = 0 ; l < r ; l++) {
      mpfi_init(B[i] + l);
      mpfi_set_si(B[i] + l, 0);
    }
    if (i < r)
      mpfi_set_si(B[i] + i, 1);
  }

  // error bounds for the "nearly zero" coefficients
  mpfr_ptr NZ = malloc(n * sizeof(mpfr_t));
  for (i = 0 ; i < n ; i++) {
    mpfr_init(NZ + i);
    mpfr_set_si(NZ + i, 0, MPFR_RNDU);
  }

  mpfr_t GR_bound;
  mpfr_init(GR_bound);
  mpfr_set_si(GR_bound, 1, MPFR_RNDU);
  
  // shift the s first lines on the left to align them
  max_index1 = s > n ? n : s; // = min(s, n)
  for (j = 0 ; j < max_index1 ; j++) {
    for (k = 0 ; k <= s+j ; k++) {
      mpfi_set_si(D[j] + k, 0);
      mpfi_swap(D[j] + k, D[j] + k+s-j);
    }
    max_index2 = 2*s < n ? 2*s : n-1; // = min(2*s, n-1)
    for (k = s+j+1 ; k <= max_index2 ; k++)
      for (l = 0 ; l < r ; l++) {
	mpfi_mul(temp_fi, B[j] + l, M->H[l] + k);
	mpfi_add(D[j] + k, D[j] + k, temp_fi);
      }
  }
  
  // main loop to compute GR*M into (D, B)
  for (i = 0 ; i < n ; i++) {

    max_index1 = i+s >= n ? n-1-i : s; // = min(s, n-1-i)
    max_index2 = i+2*s >= n ? n-1-i : 2*s; // min(2*s, n-1-i)

    // exchange lines according to XCH
    if (N->XCH[i] != 0) {
      j = N->XCH[i];
      row_buf = D[i];
      D[i] = D[i+j];
      D[i+j] = row_buf;
      row_buf = B[i];
      B[i] = B[i+j];
      B[i+j] = row_buf;
    }
    
    // apply Givens rotations
    for (j = 1 ; j <= max_index1 ; j++) {

      // on the diagonal coefficients
      for (k = 0 ; k <= max_index2 ; k++) {
	mpfi_swap(temp_fi, D[i+j] + k);
	mpfi_mul_fr(D[i+j] + k, D[i] + k, N->GR[i][j-1] + 1);
	mpfi_mul_fr(D[i] + k, D[i] + k, N->GR[i][j-1] + 0);
	mpfi_mul_fr(temp2_fi, temp_fi, N->GR[i][j-1] + 1);
	mpfi_sub(D[i] + k, D[i] + k, temp2_fi);
	mpfi_mul_fr(temp2_fi, temp_fi, N->GR[i][j-1] + 0);
	mpfi_add(D[i+j] + k, D[i+j] + k, temp2_fi);
      }
      
      // on B
      for (l = 0 ; l < r ; l++) {
	mpfi_swap(temp_fi, B[i+j] + l);
	mpfi_mul_fr(B[i+j] + l, B[i] + l, N->GR[i][j-1] + 1);
	mpfi_mul_fr(B[i] + l, B[i] + l, N->GR[i][j-1] + 0);
	mpfi_mul_fr(temp2_fi, temp_fi, N->GR[i][j-1] + 1);
	mpfi_sub(B[i] + l, B[i] + l, temp2_fi);
	mpfi_mul_fr(temp2_fi, temp_fi, N->GR[i][j-1] + 0);
	mpfi_add(B[i+j] + l, B[i+j] + l, temp2_fi);
      }
      
      // stores the norme of the rotation
      mpfr_abs(temp_fr, N->GR[i][j-1] + 0, MPFR_RNDU);
      mpfr_abs(temp2_fr, N->GR[i][j-1] + 1, MPFR_RNDU);
      mpfr_add(temp_fr, temp_fr, temp2_fr, MPFR_RNDU);
      mpfr_mul(GR_bound, GR_bound, temp_fr, MPFR_RNDU);
      
      // stores the bound on the "nearly-zero" obtained coefficient
      mpfi_mag(temp_fr, D[i+j] + 0);
      mpfr_add(NZ + i, NZ + i, temp_fr, MPFR_RNDU);

    }
    
    // shift the next s rows by 1 on the left (erase the first coeff, counted as "nearly zero")
    for (j = 1 ; j <= max_index1 ; j++) {
      for (k = 0 ; k < 2*s ; k++)
	mpfi_swap(D[i+j] + k, D[i+j] + k+1);
      if (i+1+2*s < n)
	for (l = 0 ; l < r ; l++) {
	  mpfi_mul(temp_fi, B[i+j] + l, M->H[l] + i+1+2*s);
	  mpfi_add(D[i+j] + 2*s, D[i+j] + 2*s, temp_fi);
	}
    }

  }

  // apply the GR norm on the "nearly zero" coefficients
  for (i = 0 ; i < n ; i++)
    mpfr_mul(NZ + i, NZ + i, GR_bound, MPFR_RNDU);

  // compare (D, B) = GR*M and (N->D, N->B)
  mpfr_set_si(bound, 0, MPFR_RNDU);

  // the error due to the lines represented by B 
  mpfr_t H_bound, H_err_bound;
  mpfr_init(H_bound);
  mpfr_init(H_err_bound);

  for (l = 0 ; l < r ; l++) {
    mpfr_set_si(H_bound, 0, MPFR_RNDU);
    mpfr_set_si(H_err_bound, 0, MPFR_RNDU);
    for (j = 0 ; j < n ; j++) {
      mpfr_abs(temp_fr, N->H[l] + j, MPFR_RNDU);
      if (mpfr_cmp(temp_fr, H_bound) > 0)
	mpfr_set(H_bound, temp_fr, MPFR_RNDU);
      mpfi_sub_fr(temp_fi, M->H[l] + j, N->H[l] + j);
      mpfi_mag(temp_fr, temp_fi);
      if (mpfr_cmp(temp_fr, H_err_bound) > 0)
	mpfr_set(H_err_bound, temp_fr, MPFR_RNDU);
    }
    mpfr_set_si(temp_fr, 0, MPFR_RNDU);
    for (i = 0 ; i < n ; i++) {
      mpfi_mag(temp2_fr, B[i] + l);
      mpfr_add(temp_fr, temp_fr, temp2_fr, MPFR_RNDU);
    }
    mpfr_mul(temp_fr, temp_fr, H_err_bound, MPFR_RNDU);
    mpfr_add(bound, bound, temp_fr, MPFR_RNDU);
    mpfr_set_si(temp_fr, 0, MPFR_RNDU);
    for (i = 0 ; i < n ; i++) {
      mpfi_sub_fr(temp_fi, B[i] + l, N->B[i] + l);
      mpfi_mag(temp2_fr, temp_fi);
      mpfr_add(temp_fr, temp_fr, temp2_fr, MPFR_RNDU);
    }
    mpfr_mul(temp_fr, temp_fr, H_bound, MPFR_RNDU);
    mpfr_add(bound, bound, temp_fr, MPFR_RNDU);
  }

  // the error due to the diagonal coefficients and the "nearly zero" coefficients
  for (i = 0 ; i < n ; i++) {
    long k_min = i-2*s < 0 ? 0 : i-2*s; // = min(i-2*s,0)
    for (k = k_min ; k <= i ; k++) {
      mpfi_sub_fr(temp_fi, D[k] + i-k, N->D[k] + i-k);
      mpfi_mag(temp_fr, temp_fi);
      mpfr_add(NZ + i, NZ + i, temp_fr, MPFR_RNDU);
    }
  }

  // computing the bound of (D, B) - (N->D, N->B)
  mpfr_set_si(temp_fr, 0, MPFR_RNDU);
  for (i = 0 ; i < n ; i++)
    if (mpfr_cmp(NZ + i, temp_fr) > 0)
      mpfr_set(temp_fr, NZ + i, MPFR_RNDU);
  mpfr_add(bound, bound, temp_fr, MPFR_RNDU);

  // overestimate the norm of (N->D, N->B)^-1
  mpfr_bandmatrix_QRdecomp_inv1norm_overestimate(temp_fr, N);
  printf("(inverse-norm: ");
  mpfr_out_str(stdout, 10, 10, temp_fr, MPFR_RNDU);
  printf(")\n");

  // final bound
  mpfr_mul(bound, bound, temp_fr, MPFR_RNDU);

  // clear variables
  mpfr_clear(temp_fr);
  mpfr_clear(temp2_fr);
  mpfi_clear(temp_fi);
  mpfi_clear(temp2_fi);
  for (i = 0 ; i < n ; i++) {
    for (j = 0 ; j <= 2*s ; j++)
      mpfi_clear(D[i] + j);
    free(D[i]);
    for (l = 0 ; l < r ; l++)
      mpfi_clear(B[i] + l);
    free(B[i]);
    mpfr_clear(NZ + i);
  }
  free(D);
  free(B);
  free(NZ);
  mpfr_clear(GR_bound);
  mpfr_clear(H_bound);
  mpfr_clear(H_err_bound);

}










