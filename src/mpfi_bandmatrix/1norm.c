
#include "mpfi_bandmatrix.h"


void mpfi_bandmatrix_1norm_ubound(mpfr_t norm, const mpfi_bandmatrix_t M)
{
  long n = M->dim;
  long r = M->Hwidth;
  long s = M->Dwidth;
  long i, j, jmin, jmax;

  mpfr_t temp, temp2;
  mpfr_inits(temp, temp2, NULL);

  mpfr_set_zero(norm, 1);

  for (i = 0 ; i < n ; i++) {
  
    mpfr_set_zero(temp, 1);

    jmax = r < i-s ? r : i-s;
    jmax = jmax < n ? jmax : n;
    for (j = 0 ; j < jmax ; j++) {
      mpfi_mag(temp2, M->H[j] + i);
      mpfr_add(temp, temp, temp2, MPFR_RNDU);
    }
      
    jmin = i-s < 0 ? 0 : i-s;
    jmax = i+s < n ? i+s : n-1;
    for (j = jmin ; j <= jmax ; j++) {
      mpfi_mag(temp2, M->D[j] + s - j + i);
      mpfr_add(temp, temp, temp2, MPFR_RNDU);
    }

    jmax = r < n ? r : n;
    for (j = i+s+1 ; j < jmax ; j++) {
      mpfi_mag(temp2, M->H[j] + i);
      mpfr_add(temp, temp, temp2, MPFR_RNDU);
    }
    
    if (mpfr_cmp(temp, norm) > 0)
      mpfr_set(norm, temp, MPFR_RNDU);
  
  }

  mpfr_clears(temp, temp2, NULL);
}


void mpfi_bandmatrix_1norm_lbound(mpfr_t norm, const mpfi_bandmatrix_t M)
{
  long n = M->dim;
  long r = M->Hwidth;
  long s = M->Dwidth;
  long i, j, jmin, jmax;

  mpfr_t temp, temp2;
  mpfr_inits(temp, temp2, NULL);

  mpfr_set_zero(norm, 1);

  for (i = 0 ; i < n ; i++) {
  
    mpfr_set_zero(temp, 1);

    jmax = r < i-s ? r : i-s;
    jmax = jmax < n ? jmax : n;
    for (j = 0 ; j < jmax ; j++) {
      mpfi_mig(temp2, M->H[j] + i);
      mpfr_add(temp, temp, temp2, MPFR_RNDD);
    }
      
    jmin = i-s < 0 ? 0 : i-s;
    jmax = i+s < n ? i+s : n-1;
    for (j = jmin ; j <= jmax ; j++) {
      mpfi_mig(temp2, M->D[j] + s - j + i);
      mpfr_add(temp, temp, temp2, MPFR_RNDD);
    }

    jmax = r < n ? r : n;
    for (j = i+s+1 ; j < jmax ; j++) {
      mpfi_mig(temp2, M->H[j] + i);
      mpfr_add(temp, temp, temp2, MPFR_RNDD);
    }
    
    if (mpfr_cmp(temp, norm) > 0)
      mpfr_set(norm, temp, MPFR_RNDD);
  
  }

  mpfr_clears(temp, temp2, NULL);
}


void mpfi_bandmatrix_1norm_fi(mpfi_t norm, const mpfi_bandmatrix_t M)
{
  long n = M->dim;
  long r = M->Hwidth;
  long s = M->Dwidth;
  long i, j, jmin, jmax;

  mpfi_t temp, temp2;
  mpfi_init(temp);
  mpfi_init(temp2);

  mpfi_set_si(norm, 0);

  for (i = 0 ; i < n ; i++) {
  
    mpfi_set_si(temp, 0);

    jmax = r < i-s ? r : i-s;
    jmax = jmax < n ? jmax : n;
    for (j = 0 ; j < jmax ; j++) {
      mpfi_abs(temp2, M->H[j] + i);
      mpfi_add(temp, temp, temp2);
    }
      
    jmin = i-s < 0 ? 0 : i-s;
    jmax = i+s < n ? i+s : n-1;
    for (j = jmin ; j <= jmax ; j++) {
      mpfi_abs(temp2, M->D[j] + s - j + i);
      mpfi_add(temp, temp, temp2);
    }

    jmax = r < n ? r : n;
    for (j = i+s+1 ; j < jmax ; j++) {
      mpfi_abs(temp2, M->H[j] + i);
      mpfi_add(temp, temp, temp2);
    }

    mpfr_max(&(norm->left), &(norm->left), &(temp->left), MPFR_RNDD);
    mpfr_max(&(norm->right), &(norm->right), &(temp->right), MPFR_RNDU);
  
  }

  mpfi_clear(temp);
  mpfi_clear(temp2);
}

