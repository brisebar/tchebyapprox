
#include "mpfi_bandmatrix.h"


void mpfi_bandmatrix_set_column(mpfi_bandmatrix_t M, const mpfi_bandvec_t V)
{
  long n = M->dim;
  long r = M->Hwidth;
  long s = M->Dwidth;
  long i = V->ind;
  long j, jmin, jmax;

  if (r != V->Hwidth)
    fprintf(stderr, "mpfi_bandmatrix_set_column: error: incompatible Hwidth\n");

  else if (s != V->Dwidth)
    fprintf(stderr, "mpfi_bandmatrix_set_column: error: incompatible Dwidth\n");

  else {

    jmax = r < i-s ? r : i-s;
    jmax = jmax < n ? jmax : n;
    for (j = 0 ; j < jmax ; j++)
      mpfi_set(M->H[j] + i, V->H + j);

    jmin = i-s < 0 ? 0 : i-s;
    jmax = i+s < n ? i+s : n-1;
    for (j = jmin ; j <= jmax ; j++)
      mpfi_set(M->D[j] + s + i - j, V->D + s - i + j);

    jmax = r < n ? r : n;
    for (j = i+s+1 ; j < jmax ; j++)
      mpfi_set(M->H[j] + i, V->H + j);

  }
}


void mpfi_bandmatrix_set_column_fr(mpfi_bandmatrix_t M, const mpfr_bandvec_t V)
{
  long n = M->dim;
  long r = M->Hwidth;
  long s = M->Dwidth;
  long i = V->ind;
  long j, jmin, jmax;

  if (r != V->Hwidth)
    fprintf(stderr, "mpfi_bandmatrix_set_column: error: incompatible Hwidth\n");

  else if (s != V->Dwidth)
    fprintf(stderr, "mpfi_bandmatrix_set_column: error: incompatible Dwidth\n");

  else {

    jmax = r < i-s ? r : i-s;
    jmax = jmax < n ? jmax : n;
    for (j = 0 ; j < jmax ; j++)
      mpfi_set_fr(M->H[j] + i, V->H + j);

    jmin = i-s < 0 ? 0 : i-s;
    jmax = i+s < n ? i+s : n-1;
    for (j = jmin ; j <= jmax ; j++)
      mpfi_set_fr(M->D[j] + s + i - j, V->D + s - i + j);

    jmax = r < n ? r : n;
    for (j = i+s+1 ; j < jmax ; j++)
      mpfi_set_fr(M->H[j] + i, V->H + j);

  }
}
