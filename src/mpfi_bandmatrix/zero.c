
#include "mpfi_bandmatrix.h"


void mpfi_bandmatrix_zero(mpfi_bandmatrix_t M)
{
  long n = M->dim;
  long r = M->Hwidth;
  long s = M->Dwidth;
  long i, j;

  for (i = 0 ; i < n ; i++) {

    for (j = 0 ; j < r ; j++)
      mpfi_set_si(M->H[j] + i, 0);

    for (j = 0 ; j <= 2*s ; j++)
      mpfi_set_si(M->D[i] + j, 0);
  
  }
}
