
#include "mpfi_bandmatrix.h"


void mpfi_bandmatrix_set(mpfi_bandmatrix_t M, const mpfi_bandmatrix_t N)
{
  long n = N->dim;
  long r = N->Hwidth;
  long s = N->Dwidth;
  long i, j;

  mpfi_bandmatrix_set_params(M, n, r, s);

  for (i = 0 ; i < n ; i++) {

    for (j = 0 ; j < r ; j++)
      mpfi_set(M->H[j] + i, N->H[j] + i);

    for (j = 0 ; j <= 2*s ; j++)
      mpfi_set(M->D[i] + j, N->D[i] + j);
  
  }
}
